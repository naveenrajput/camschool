-- phpMyAdmin SQL Dump
-- version 5.0.0
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Nov 22, 2020 at 09:56 AM
-- Server version: 10.4.11-MariaDB-1:10.4.11+maria~bionic
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `camschool`
--

-- --------------------------------------------------------

--
-- Table structure for table `action_log`
--

CREATE TABLE `action_log` (
  `action_id` int(11) NOT NULL,
  `table_name` varchar(50) NOT NULL,
  `record_id` int(11) NOT NULL,
  `action` varchar(50) NOT NULL,
  `description` varchar(250) CHARACTER SET utf8 NOT NULL,
  `perform_by` varchar(50) NOT NULL,
  `performer_id` int(11) NOT NULL,
  `ip_address` varchar(60) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `action_log`
--

INSERT INTO `action_log` (`action_id`, `table_name`, `record_id`, `action`, `description`, `perform_by`, `performer_id`, `ip_address`, `created`) VALUES
(1, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.172', '2020-11-03 13:47:07'),
(2, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '::1', '2020-11-03 13:50:35'),
(3, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.182', '2020-11-04 04:34:27'),
(4, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '::1', '2020-11-04 08:26:18'),
(5, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '::1', '2020-11-04 09:41:42'),
(6, 'pages', 3, 'update', 'cam_school updated \'About Us\' page.', 'Admin', 1, '::1', '2020-11-04 09:42:48'),
(7, 'pages', 3, 'update', 'cam_school updated \'About Us\' page.', 'Admin', 1, '::1', '2020-11-04 09:43:24'),
(8, 'pages', 3, 'update', 'cam_school updated \'About Us\' page.', 'Admin', 1, '::1', '2020-11-04 09:45:13'),
(9, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '::1', '2020-11-04 10:49:04'),
(10, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '::1', '2020-11-04 12:57:03'),
(11, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '::1', '2020-11-04 13:56:00'),
(12, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.172', '2020-11-05 05:00:42'),
(13, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '::1', '2020-11-05 05:12:49'),
(14, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '::1', '2020-11-05 13:07:01'),
(15, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.172', '2020-11-05 13:54:07'),
(16, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '::1', '2020-11-06 05:09:17'),
(17, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.172', '2020-11-08 12:01:45'),
(18, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '::1', '2020-11-08 13:35:39'),
(19, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.172', '2020-11-09 05:23:56'),
(20, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.172', '2020-11-10 09:19:51'),
(21, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '::1', '2020-11-10 14:08:47'),
(22, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '::1', '2020-11-10 14:10:30'),
(23, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '::1', '2020-11-10 14:11:29'),
(24, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '::1', '2020-11-10 14:12:51'),
(25, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '::1', '2020-11-12 07:31:15'),
(26, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '::1', '2020-11-16 05:44:03'),
(27, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.172', '2020-11-17 05:26:38'),
(28, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.194', '2020-11-17 11:54:53'),
(29, 'pages', 7, 'update', 'cam_school updated \'Chat At Me! Schools Information - Parent\' page.', 'Admin', 1, '192.168.2.194', '2020-11-17 13:52:32'),
(30, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.194', '2020-11-18 05:26:18'),
(31, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.172', '2020-11-18 05:41:11'),
(32, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.194', '2020-11-18 13:37:13'),
(33, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.194', '2020-11-18 13:40:32'),
(34, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.194', '2020-11-18 13:54:03'),
(35, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.194', '2020-11-18 13:59:30'),
(36, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.194', '2020-11-18 14:00:17'),
(37, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.194', '2020-11-18 14:01:34'),
(38, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.194', '2020-11-19 05:19:11'),
(39, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.194', '2020-11-19 07:11:47'),
(40, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.194', '2020-11-19 10:09:02'),
(41, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.194', '2020-11-19 11:28:14'),
(42, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.194', '2020-11-20 05:38:50'),
(43, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '::1', '2020-11-20 06:11:30'),
(44, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.172', '2020-11-20 08:08:16'),
(45, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.172', '2020-11-20 10:23:37'),
(46, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.194', '2020-11-21 05:54:35'),
(47, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.194', '2020-11-21 07:27:20'),
(48, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.172', '2020-11-21 12:13:37'),
(49, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '192.168.2.228', '2020-11-21 12:31:38'),
(50, 'admin', 1, 'Login', 'cam_school login in admin.', 'Admin', 1, '::1', '2020-11-22 05:46:16'),
(51, 'pages', 1, 'update', 'cam_school updated \'Terms of Service\' page.', 'Admin', 1, '::1', '2020-11-22 05:47:13'),
(52, 'pages', 2, 'update', 'cam_school updated \'Privacy Policy\' page.', 'Admin', 1, '::1', '2020-11-22 05:47:36');

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `type` varchar(50) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text DEFAULT NULL,
  `record_id` int(11) NOT NULL DEFAULT 0,
  `office_id` int(11) NOT NULL DEFAULT 0,
  `room_id` int(11) NOT NULL DEFAULT 0,
  `product_id` int(11) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` smallint(6) NOT NULL DEFAULT 1,
  `deleted` smallint(6) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `user_id`, `type`, `title`, `description`, `record_id`, `office_id`, `room_id`, `product_id`, `created_by`, `created_at`, `updated_at`, `is_active`, `deleted`) VALUES
(1, 0, 'new_group', 'New Group', 'District2@mailinator.com created a new group testChatGroup1', 0, 0, 0, 0, 22, '2020-11-07 11:52:40', NULL, 1, 0),
(2, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group Hhhgh', 0, 0, 0, 0, 11, '2020-11-07 15:24:52', NULL, 1, 0),
(3, 0, 'new_group', 'New Group', 'District2@mailinator.com created a new group hellodes', 0, 0, 0, 0, 22, '2020-11-07 16:03:34', NULL, 1, 0),
(4, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group New Grp', 0, 0, 0, 0, 11, '2020-11-07 18:00:55', NULL, 1, 0),
(5, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group Grp1', 0, 0, 0, 0, 11, '2020-11-09 15:06:33', NULL, 1, 0),
(6, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group Grp2', 0, 0, 0, 0, 11, '2020-11-09 17:14:26', NULL, 1, 0),
(7, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group Grp3', 0, 0, 0, 0, 11, '2020-11-09 17:17:51', NULL, 1, 0),
(8, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group Grp5', 0, 0, 0, 0, 11, '2020-11-09 17:37:54', NULL, 1, 0),
(9, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group Thdnd', 0, 0, 0, 0, 11, '2020-11-09 18:11:22', NULL, 1, 0),
(10, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group Group 1', 0, 0, 0, 0, 11, '2020-11-18 19:20:44', NULL, 1, 0),
(11, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group Group 2', 0, 0, 0, 0, 11, '2020-11-19 16:15:51', NULL, 1, 0),
(12, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group group 3', 0, 0, 0, 0, 11, '2020-11-19 16:16:55', NULL, 1, 0),
(13, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group group 4', 0, 0, 0, 0, 11, '2020-11-19 16:20:20', NULL, 1, 0),
(14, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group group 6', 0, 0, 0, 0, 11, '2020-11-19 16:21:29', NULL, 1, 0),
(15, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group Group 100', 0, 0, 0, 0, 11, '2020-11-19 20:01:09', NULL, 1, 0),
(16, 0, 'new_group', 'New Group', 'Student@mailinator.com created a new group test group', 0, 0, 0, 0, 20, '2020-11-20 13:34:59', NULL, 1, 0),
(17, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group Grp22', 0, 0, 0, 0, 11, '2020-11-20 15:46:50', NULL, 1, 0),
(18, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group Group 4', 0, 0, 0, 0, 11, '2020-11-20 16:11:16', NULL, 1, 0),
(19, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group Group 4', 0, 0, 0, 0, 11, '2020-11-20 16:13:53', NULL, 1, 0),
(20, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group Group 55', 0, 0, 0, 0, 11, '2020-11-20 16:19:02', NULL, 1, 0),
(21, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group hellodes', 0, 0, 0, 0, 11, '2020-11-20 17:05:49', NULL, 1, 0),
(22, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group test group test ', 0, 0, 0, 0, 11, '2020-11-20 18:57:02', NULL, 1, 0),
(23, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group tie ', 0, 0, 0, 0, 11, '2020-11-21 11:18:30', NULL, 1, 0),
(24, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group Vjvjgi', 0, 0, 0, 0, 11, '2020-11-21 12:03:02', NULL, 1, 0),
(25, 0, 'new_group', 'New Group', 'teacher@mailinator.com created a new group Testing great', 0, 0, 0, 0, 11, '2020-11-21 12:49:14', NULL, 1, 0),
(26, 0, 'new_group', 'New Group', 'teacher55@mailinator.com created a new group Ttt11', 0, 0, 0, 0, 190, '2020-11-21 16:36:04', NULL, 1, 0),
(27, 0, 'new_group', 'New Group', 'std99@yopmail.com created a new group testing ', 0, 0, 0, 0, 194, '2020-11-21 18:58:45', NULL, 1, 0),
(28, 0, 'new_group', 'New Group', 'std99@yopmail.com created a new group test group 2', 0, 0, 0, 0, 194, '2020-11-21 19:06:49', NULL, 1, 0),
(29, 0, 'new_group', 'New Group', 'std99@yopmail.com created a new group test group 3', 0, 0, 0, 0, 194, '2020-11-21 19:09:34', NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `user_type` enum('Super Admin','Admin','Facility','User') NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `profile_pic` varchar(200) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `zipcode` varchar(6) NOT NULL,
  `otp` int(8) NOT NULL,
  `otp_date` datetime NOT NULL,
  `reset_token_date` datetime DEFAULT NULL,
  `reset_token` varchar(100) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `device_id` varchar(200) NOT NULL,
  `device_type` varchar(10) NOT NULL,
  `mobile_auth_token` varchar(20) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL COMMENT '0 for not deleed 1 for deleted',
  `deleted_date` datetime NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime NOT NULL,
  `modified_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `role_id`, `parent_id`, `user_type`, `fullname`, `username`, `email`, `mobile`, `status`, `profile_pic`, `state_id`, `city_id`, `address`, `zipcode`, `otp`, `otp_date`, `reset_token_date`, `reset_token`, `password`, `device_id`, `device_type`, `mobile_auth_token`, `is_deleted`, `deleted_date`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 0, 0, 'Super Admin', 'Richard Soesman', 'cam_school', 'cam@mailinator.com', '5656565656', 'Active', 'resources/images/profile/15865022945e901a9639c9c.png', 0, 0, '56 Basant vihar', '452001', 0, '2019-07-04 00:00:00', '2020-02-05 05:59:28', '43973367c73155358b892944441c9e8a', 'MTIzNDU2', '', '', '', 0, '2019-07-11 00:00:00', '2019-07-15 00:00:00', 1, '2020-10-07 11:37:33', 1),
(2, 1, 1, 'Admin', 'Micheal', 'micheal', 'micheal@mailinator.com', '8541274596', 'Active', 'resources/images/profile/15888602355eb4154bdf022.jpg', 0, 0, '36, park avenue', '', 0, '0000-00-00 00:00:00', NULL, NULL, 'MTIzNDU2Nzg5', '', '', '', 0, '0000-00-00 00:00:00', '2019-11-07 18:39:50', 1, '2020-05-08 06:43:14', 1),
(3, 2, 1, 'Admin', 'new role user test', 'Testusr1', 'feda@getnada.com', '1234567890', 'Active', 'resources/images/profile/15888602815eb41579813b8.jpg', 0, 0, '', '', 0, '0000-00-00 00:00:00', NULL, NULL, 'MTIzNDU2Nzg5', '', '', '', 0, '0000-00-00 00:00:00', '2020-01-30 13:29:38', 1, '2020-05-07 13:49:57', 1),
(4, 2, 1, 'Admin', 'Jenifer', 'jeni007', 'jeny@mailinator.com', '8471236555', 'Active', 'resources/images/profile/e5bb0b02188e2500e74d5ec30c380c46.jpeg', 0, 0, 'test', '', 0, '0000-00-00 00:00:00', NULL, NULL, 'MTIzNDU2Nzg5', '', '', '', 1, '0000-00-00 00:00:00', '2020-05-07 12:02:37', 1, '2020-05-07 12:27:19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `app_first_time_visitor`
--

CREATE TABLE `app_first_time_visitor` (
  `id` int(11) NOT NULL,
  `media_type` tinyint(1) DEFAULT 1 COMMENT '1=Video, 2=Audio',
  `format` varchar(50) DEFAULT NULL,
  `thumb_image` text DEFAULT NULL,
  `large_image` text DEFAULT NULL,
  `media_name` varchar(100) DEFAULT NULL,
  `duration` time DEFAULT '00:00:00',
  `status` tinyint(1) DEFAULT 2 COMMENT '1=Active, 2=Deactive,3=Hidden',
  `views` int(11) NOT NULL,
  `created` datetime DEFAULT '0000-00-00 00:00:00',
  `modified` datetime DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `broadcast_emails`
--

CREATE TABLE `broadcast_emails` (
  `id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `broadcast_emails`
--

INSERT INTO `broadcast_emails` (`id`, `notification_id`, `email`, `user_id`, `created`) VALUES
(1, 1, 'teacher@yopmail.com', 8, '2020-11-03 14:03:32'),
(2, 2, 'School@mailinator.com', 10, '2020-11-04 05:38:38'),
(3, 3, 'School@mailinator.com', 10, '2020-11-04 05:47:13'),
(4, 4, 'bharattea@yopmail.com', 32, '2020-11-05 14:57:46'),
(5, 5, 'dyna@mailinator.com', 29, '2020-11-06 06:38:36'),
(6, 5, 'test@mailinator.com', 35, '2020-11-06 06:38:36'),
(7, 6, 'dyna@mailinator.com', 29, '2020-11-06 06:44:23'),
(8, 6, 'test@mailinator.com', 35, '2020-11-06 06:44:23'),
(9, 7, 'dyna@mailinator.com', 29, '2020-11-06 06:44:57'),
(10, 7, 'test@mailinator.com', 35, '2020-11-06 06:44:57'),
(11, 8, 'dyna@mailinator.com', 29, '2020-11-06 06:51:50'),
(12, 8, 'test@mailinator.com', 35, '2020-11-06 06:51:50'),
(13, 9, 'dyna@mailinator.com', 29, '2020-11-06 06:53:49'),
(14, 9, 'test@mailinator.com', 35, '2020-11-06 06:53:49'),
(15, 10, 'dyna@mailinator.com', 29, '2020-11-06 06:56:10'),
(16, 10, 'test@mailinator.com', 35, '2020-11-06 06:56:10'),
(17, 11, 'dyna@mailinator.com', 29, '2020-11-06 06:56:46'),
(18, 11, 'test@mailinator.com', 35, '2020-11-06 06:56:46'),
(19, 12, 'dyna@mailinator.com', 29, '2020-11-06 06:57:55'),
(20, 12, 'test@mailinator.com', 35, '2020-11-06 06:57:55'),
(21, 13, 'dyna@mailinator.com', 29, '2020-11-06 13:16:36'),
(22, 13, 'test@mailinator.com', 35, '2020-11-06 13:16:36'),
(23, 13, 'test1@mailinator.com', 48, '2020-11-06 13:16:36'),
(24, 13, 'test2@mailinator.com', 49, '2020-11-06 13:16:36'),
(25, 13, 'test3@mailinator.com', 50, '2020-11-06 13:16:36'),
(26, 13, 'test4@mailinator.com', 51, '2020-11-06 13:16:36'),
(27, 13, 'test8@mailinator.com', 55, '2020-11-06 13:16:36'),
(28, 14, 'juniour@mailinator.com', 78, '2020-11-09 13:39:37'),
(29, 15, 'teacher30@mailinator.com', 79, '2020-11-09 13:47:55'),
(30, 16, 'teacher30@mailinator.com', 79, '2020-11-09 13:54:48'),
(31, 17, 'teacher1@mailinator.com', 47, '2020-11-10 05:30:54'),
(32, 18, 'btea123@yopmail.com', 88, '2020-11-10 10:27:12'),
(33, 19, 'btea123@yopmail.com', 88, '2020-11-10 10:29:48'),
(34, 20, 'widow@mailinator.com', 142, '2020-11-16 08:07:56'),
(35, 21, 'jaxi@mailinator.com', 150, '2020-11-16 09:37:18'),
(36, 22, 'tereso@mailinator.com', 148, '2020-11-16 09:38:36'),
(37, 23, 'dummyschool123@mailinator.com', 27, '2020-11-16 10:26:16'),
(38, 24, 'teacher30@mailinator.com', 79, '2020-11-16 10:45:43'),
(39, 25, 'trisha@mailinator.com', 124, '2020-11-17 05:57:26'),
(40, 26, 'trisha@mailinator.com', 124, '2020-11-17 06:03:13'),
(41, 27, 'trisha@mailinator.com', 124, '2020-11-17 06:09:29'),
(42, 28, 'trisha@mailinator.com', 124, '2020-11-17 06:13:26'),
(43, 29, 'trio@mailinator.com', 146, '2020-11-17 06:28:01'),
(44, 30, 'trio@mailinator.com', 146, '2020-11-17 06:30:49'),
(45, 31, 'trio@mailinator.com', 146, '2020-11-17 06:31:15'),
(46, 32, 'widow@mailinator.com', 142, '2020-11-17 09:39:08'),
(47, 33, 'widow@mailinator.com', 142, '2020-11-17 09:41:41'),
(48, 34, 'ngawade@hitaishin.com', 27, '2020-11-17 09:42:22'),
(49, 35, 'ngawade@hitaishin.com', 27, '2020-11-17 09:42:54'),
(50, 36, 'bpri33@yopmail.com', 158, '2020-11-18 06:38:22'),
(51, 37, 'teacher@mailinator.com', 11, '2020-11-18 07:06:42'),
(52, 38, 'ocean@mailinator.com', 159, '2020-11-18 13:18:20'),
(53, 39, 'dino@mailinator.com', 153, '2020-11-18 13:19:15'),
(54, 40, 'dummyschool123@mailinator.com', 27, '2020-11-18 13:19:32'),
(55, 40, 'kingjunior@mailinator.com', 40, '2020-11-18 13:19:32'),
(56, 40, 'test11@mailinator.com', 67, '2020-11-18 13:19:32'),
(57, 40, 'juniour@mailinator.com', 78, '2020-11-18 13:19:32'),
(58, 40, 'donald@mailinator.com', 132, '2020-11-18 13:19:32'),
(59, 40, 'dino@mailinator.com', 153, '2020-11-18 13:19:32'),
(60, 40, 'harry@mailinator.com', 155, '2020-11-18 13:19:32'),
(61, 41, 'checkteacher@mailinator.com', 56, '2020-11-19 07:07:28'),
(62, 42, 'bdis123@yopmail.com', 82, '2020-11-19 10:09:16'),
(63, 42, 'fridas@mailinator.com', 143, '2020-11-19 10:09:16'),
(64, 43, 'bdis123@yopmail.com', 82, '2020-11-19 10:14:24'),
(65, 43, 'fridas@mailinator.com', 143, '2020-11-19 10:14:24'),
(66, 44, 'bdis123@yopmail.com', 82, '2020-11-19 10:15:06'),
(67, 45, 'dist@mailinator.com', 68, '2020-11-19 10:43:20'),
(68, 46, 'bharatdis@yopmail.com', 30, '2020-11-19 10:54:29'),
(69, 47, 'tri110@mailinator.com', 184, '2020-11-20 06:02:53'),
(70, 48, 'trisha@mailinator.com', 124, '2020-11-20 06:03:29'),
(71, 49, 'checkteacher@mailinator.com', 56, '2020-11-20 07:42:22'),
(72, 50, 'checkteacher@mailinator.com', 56, '2020-11-20 07:46:50'),
(73, 51, 'tri110@mailinator.com', 184, '2020-11-20 09:18:03'),
(74, 52, 'masa11@mailinator.com', 183, '2020-11-20 09:21:00'),
(75, 53, 'masa11@mailinator.com', 183, '2020-11-20 09:23:22'),
(76, 54, 'nehagawade741@gmail.com', 29, '2020-11-20 09:45:54'),
(77, 55, 'ngawade@hitaishin.com', 29, '2020-11-20 09:48:37'),
(78, 56, 'dino@mailinator.com', 153, '2020-11-20 12:27:36'),
(79, 57, 'trisha@mailinator.com', 124, '2020-11-21 07:53:40'),
(80, 58, 'trisha@mailinator.com', 124, '2020-11-21 09:26:53'),
(81, 59, 'trisha@mailinator.com', 124, '2020-11-21 09:28:58'),
(82, 60, 'trisha@mailinator.com', 124, '2020-11-21 09:30:44'),
(83, 61, 'trisha@mailinator.com', 124, '2020-11-21 09:32:48'),
(84, 62, 'principal1@yopmail.com', 7, '2020-11-21 09:44:19'),
(85, 63, 'pri99@yopmail.com', 192, '2020-11-21 12:46:46'),
(86, 64, 'dummyschool123@mailinator.com', 27, '2020-11-21 12:47:13'),
(87, 64, 'kingjunior@mailinator.com', 40, '2020-11-21 12:47:13'),
(88, 64, 'test11@mailinator.com', 67, '2020-11-21 12:47:13'),
(89, 64, 'juniour@mailinator.com', 78, '2020-11-21 12:47:13'),
(90, 64, 'donald@mailinator.com', 132, '2020-11-21 12:47:13'),
(91, 64, 'dino@mailinator.com', 153, '2020-11-21 12:47:13'),
(92, 64, 'harry@mailinator.com', 155, '2020-11-21 12:47:13'),
(93, 65, 'trisha@mailinator.com', 124, '2020-11-22 05:45:27'),
(94, 66, 'trisha@mailinator.com', 124, '2020-11-22 05:45:28'),
(95, 67, 'checkteacher@mailinator.com', 56, '2020-11-22 05:45:38');

-- --------------------------------------------------------

--
-- Table structure for table `broadcast_notification`
--

CREATE TABLE `broadcast_notification` (
  `notification_id` int(11) NOT NULL,
  `notification_type` varchar(20) NOT NULL,
  `subject` varchar(150) NOT NULL,
  `message` text NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0= Not delete , 1=delete',
  `created_by` int(11) NOT NULL,
  `sent_by` tinyint(1) NOT NULL COMMENT '0=User,\r\n1=Admin',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `broadcast_notification`
--

INSERT INTO `broadcast_notification` (`notification_id`, `notification_type`, `subject`, `message`, `is_deleted`, `created_by`, `sent_by`, `created`, `modified`) VALUES
(1, 'email', 'test', 'Hellooooo', 0, 7, 0, '2020-11-03 14:03:32', '0000-00-00 00:00:00'),
(2, 'email', 'New messahe', 'Testing message 1', 0, 9, 0, '2020-11-04 05:38:38', '0000-00-00 00:00:00'),
(3, 'email', 'Tedting2', 'Testing2 Message2.2     New one', 0, 9, 0, '2020-11-04 05:47:13', '0000-00-00 00:00:00'),
(4, 'email', 'cucù fu', 'Cucù cucù cucù', 0, 31, 0, '2020-11-05 14:57:46', '0000-00-00 00:00:00'),
(5, 'email', 'hello', 'this is a test message', 1, 27, 0, '2020-11-06 06:38:35', '2020-11-06 13:51:48'),
(6, 'email', 'hello', 'fgdsxfg sdsfdsf sfdsf', 0, 27, 0, '2020-11-06 06:44:23', '0000-00-00 00:00:00'),
(7, 'email', 'tjtyu', 'utyuyt', 0, 27, 0, '2020-11-06 06:44:57', '0000-00-00 00:00:00'),
(8, 'email', 'hello', 'y ouigb e wqeq ewqe', 0, 27, 0, '2020-11-06 06:51:50', '0000-00-00 00:00:00'),
(9, 'email', 'hello', 'this is atest mesga', 0, 27, 0, '2020-11-06 06:53:49', '0000-00-00 00:00:00'),
(10, 'email', 'HJGHJH', 'dsad sadasw', 0, 27, 0, '2020-11-06 06:56:10', '0000-00-00 00:00:00'),
(11, 'email', 'hjghj', 'jhj hj', 0, 27, 0, '2020-11-06 06:56:46', '0000-00-00 00:00:00'),
(12, 'email', 'HJGHJH', 'fghjfghj', 1, 27, 0, '2020-11-06 06:57:55', '2020-11-07 12:01:48'),
(13, 'email', 'jghjfghj hh', 'hgdft trytr', 1, 27, 0, '2020-11-06 13:16:36', '2020-11-07 05:46:24'),
(14, 'email', 'Test teacher', 'test message teacher', 0, 5, 0, '2020-11-09 13:39:37', '0000-00-00 00:00:00'),
(15, 'email', 'Test teacher', 'test message teacher', 0, 5, 0, '2020-11-09 13:47:55', '0000-00-00 00:00:00'),
(16, 'email', 'New subject', 'New description', 0, 78, 0, '2020-11-09 13:54:48', '0000-00-00 00:00:00'),
(17, 'email', 'dsf', 'dsv', 0, 27, 0, '2020-11-10 05:30:54', '0000-00-00 00:00:00'),
(18, 'email', 'Hiiii', 'Hello there hope your day was fun to see it all', 0, 83, 0, '2020-11-10 10:27:12', '0000-00-00 00:00:00'),
(19, 'email', 'Gigoucoyxitd', 'Dkydkydoydoyf hlotdoydyo holdup', 0, 83, 0, '2020-11-10 10:29:48', '0000-00-00 00:00:00'),
(20, 'email', 'test', 'test', 1, 1, 0, '2020-11-16 08:07:56', '2020-11-17 09:44:27'),
(21, 'email', 'test', 'ds', 1, 1, 0, '2020-11-16 09:37:18', '2020-11-17 09:44:29'),
(22, 'email', 're', '55', 1, 1, 0, '2020-11-16 09:38:36', '2020-11-17 09:44:30'),
(23, 'email', 'my test cont', 'my test conts', 1, 1, 0, '2020-11-16 10:26:16', '2020-11-17 09:44:30'),
(24, 'email', 'dd', 'dd', 0, 78, 0, '2020-11-16 10:45:43', '0000-00-00 00:00:00'),
(25, 'email', 'test', 'test', 0, 27, 0, '2020-11-17 05:57:26', '0000-00-00 00:00:00'),
(26, 'email', 'teerrwrewrw', 'eewrwer', 1, 27, 0, '2020-11-17 06:03:13', '2020-11-20 07:44:49'),
(27, 'email', 'wewew', 'ewewe', 1, 27, 0, '2020-11-17 06:09:29', '2020-11-20 07:44:48'),
(28, 'email', 'ddd', 'ddd', 1, 27, 0, '2020-11-17 06:13:26', '2020-11-20 07:44:48'),
(29, 'email', 'adf', 'asfasfsafsd', 1, 1, 0, '2020-11-17 06:28:01', '2020-11-17 09:44:30'),
(30, 'email', 'tasdf', 'afsdff', 1, 1, 0, '2020-11-17 06:30:49', '2020-11-17 09:44:31'),
(31, 'email', 'asdf', 'asffd', 1, 1, 0, '2020-11-17 06:31:15', '2020-11-17 09:44:32'),
(32, 'email', 'sss', 'ssss', 1, 1, 0, '2020-11-17 09:39:08', '2020-11-17 09:44:33'),
(33, 'email', 'ddd', 'sdsdsdsd', 0, 1, 0, '2020-11-17 09:41:41', '0000-00-00 00:00:00'),
(34, 'email', 'ddd', 'dssdsd', 0, 1, 0, '2020-11-17 09:42:22', '0000-00-00 00:00:00'),
(35, 'email', 'ss', 'ss', 0, 1, 0, '2020-11-17 09:42:54', '0000-00-00 00:00:00'),
(36, 'email', 'Test', 'Hiii', 0, 82, 0, '2020-11-18 06:38:22', '0000-00-00 00:00:00'),
(37, 'email', 'Tygc', 'Ethfsw', 0, 10, 0, '2020-11-18 07:06:42', '0000-00-00 00:00:00'),
(38, 'email', 'wer', 'er', 1, 1, 0, '2020-11-18 13:18:20', '2020-11-18 13:19:39'),
(39, 'email', 'ff', 'fdf', 1, 1, 0, '2020-11-18 13:19:15', '2020-11-18 13:19:41'),
(40, 'email', 'dddfd', 'dfdf', 1, 1, 0, '2020-11-18 13:19:32', '2020-11-18 13:19:36'),
(41, 'email', 'erer', 'ere', 1, 27, 0, '2020-11-19 07:07:28', '2020-11-20 07:44:47'),
(42, 'email', 'test', '<p>\r\n	testadfsfffdsf</p>', 0, 1, 1, '2020-11-19 10:09:16', '0000-00-00 00:00:00'),
(43, 'email', 'test', 'adfasdfsdfsf', 0, 1, 1, '2020-11-19 10:14:24', '0000-00-00 00:00:00'),
(44, 'email', 'test', 'asdfsadf', 0, 1, 1, '2020-11-19 10:15:06', '0000-00-00 00:00:00'),
(45, 'email', 'Broadcast Message to Only one user', 'Hello,\r\n\r\nHow are you ! here is you content\r\n\r\nBest Regards,\r\nRashique Khan', 0, 1, 1, '2020-11-19 10:43:20', '0000-00-00 00:00:00'),
(46, 'email', 'test', 'sdf sdfdf', 0, 1, 1, '2020-11-19 10:54:29', '0000-00-00 00:00:00'),
(47, 'email', 'test', 'et', 0, 1, 0, '2020-11-20 06:02:53', '0000-00-00 00:00:00'),
(48, 'email', 'my test', 'mytest', 1, 27, 0, '2020-11-20 06:03:29', '2020-11-20 07:44:47'),
(49, 'email', 'w', 's', 1, 27, 0, '2020-11-20 07:42:22', '2020-11-20 07:44:46'),
(50, 'email', 'e', 'ee', 0, 27, 0, '2020-11-20 07:46:50', '0000-00-00 00:00:00'),
(51, 'email', 'tets', 'ett', 0, 1, 0, '2020-11-20 09:18:03', '0000-00-00 00:00:00'),
(52, 'email', 'rrt', 'trt', 0, 1, 0, '2020-11-20 09:21:00', '0000-00-00 00:00:00'),
(53, 'email', 'dfdf', 'df', 0, 1, 0, '2020-11-20 09:23:22', '0000-00-00 00:00:00'),
(54, 'email', 'test email', 'test email from cam school', 0, 27, 0, '2020-11-20 09:45:54', '0000-00-00 00:00:00'),
(55, 'email', 'test mail', 'test mail test mail', 1, 27, 0, '2020-11-20 09:48:37', '2020-11-20 12:31:59'),
(56, 'email', 'dssd', 'dsfdsf', 0, 1, 0, '2020-11-20 12:27:36', '0000-00-00 00:00:00'),
(57, 'email', 'Test', 'Test Message for Trisha', 0, 27, 0, '2020-11-21 07:53:40', '0000-00-00 00:00:00'),
(58, 'email', 'Test', 'TEst One two Three', 0, 27, 0, '2020-11-21 09:26:53', '0000-00-00 00:00:00'),
(59, 'email', 'test', 'sdfsadfdsfsdf', 0, 27, 0, '2020-11-21 09:28:58', '0000-00-00 00:00:00'),
(60, 'email', 'testsadf', 'adsf afdsaf', 0, 27, 0, '2020-11-21 09:30:44', '0000-00-00 00:00:00'),
(61, 'email', 'Test', 'Test Message For test', 0, 27, 0, '2020-11-21 09:32:48', '0000-00-00 00:00:00'),
(62, 'email', 'hello Subject Bharat', 'Hello Message Bharat', 0, 3, 0, '2020-11-21 09:44:19', '0000-00-00 00:00:00'),
(63, 'email', 'Hiii', 'Hi allll', 0, 191, 0, '2020-11-21 12:46:46', '0000-00-00 00:00:00'),
(64, 'email', 'test', 'test', 0, 1, 0, '2020-11-21 12:47:13', '0000-00-00 00:00:00'),
(65, 'email', 'et', 'fhfgh', 0, 27, 0, '2020-11-22 05:45:27', '0000-00-00 00:00:00'),
(66, 'email', 'et', 'fhfgh', 0, 27, 0, '2020-11-22 05:45:28', '0000-00-00 00:00:00'),
(67, 'email', 'try', 'rty', 0, 27, 0, '2020-11-22 05:45:38', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `broadcast_notifications_old`
--

CREATE TABLE `broadcast_notifications_old` (
  `id` int(11) NOT NULL,
  `message` text DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `room_id` varchar(11) NOT NULL,
  `job_id` varchar(11) NOT NULL,
  `sender_id` varchar(11) NOT NULL,
  `sender_id_delete` int(11) NOT NULL DEFAULT 0,
  `receiver_id` varchar(11) NOT NULL,
  `receiver_id_delete` int(11) NOT NULL DEFAULT 0,
  `message` text NOT NULL,
  `type` enum('Image','Video','Document','Text') NOT NULL DEFAULT 'Text',
  `file_format` varchar(10) NOT NULL DEFAULT '0',
  `original_file_name` varchar(255) NOT NULL DEFAULT '0',
  `is_read` int(11) NOT NULL DEFAULT 0 COMMENT '0 for read 1 for unread',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chat_active_users`
--

CREATE TABLE `chat_active_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_active_user` int(11) NOT NULL DEFAULT 0,
  `first_socket_id` varchar(100) NOT NULL DEFAULT '',
  `second_active_user` int(11) NOT NULL DEFAULT 0,
  `second_socket_id` varchar(100) NOT NULL DEFAULT '',
  `active_group_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` smallint(6) NOT NULL DEFAULT 1,
  `deleted` smallint(6) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `chat_groups`
--

CREATE TABLE `chat_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `course_id` int(11) NOT NULL DEFAULT 0,
  `group_image` varchar(200) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `is_teacher` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1 = Teacher, 0 = other',
  `grade` tinyint(2) NOT NULL,
  `school_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `is_active` smallint(6) NOT NULL DEFAULT 1,
  `deleted` smallint(6) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chat_groups`
--

INSERT INTO `chat_groups` (`id`, `course_id`, `group_image`, `name`, `description`, `created_at`, `is_teacher`, `grade`, `school_id`, `created_by`, `is_active`, `deleted`) VALUES
(1, 0, '', 'Grop 112bjgh', 'Hello Group 11 this is new Description hfhfhf', '2020-11-18 13:50:44', 0, 0, 0, 11, 1, 0),
(6, 0, '', 'Group 100', 'Test group 100', '2020-11-19 14:31:09', 0, 0, 0, 11, 1, 0),
(7, 0, '', 'test group', 'test descriptiion', '2020-11-20 08:04:59', 0, 0, 0, 20, 1, 0),
(8, 0, '', 'Dummy Group', 'test descriptiion Dummy', '2020-11-20 08:04:59', 0, 0, 0, 11, 1, 0),
(9, 0, 'resources/images/profile/file-1605867410763.jpeg', 'Grp22', 'New GRP description.', '2020-11-20 10:16:50', 0, 0, 0, 11, 1, 0),
(10, 0, 'resources/images/profile/message-1605868876172.png', 'Group 4', 'Test Description', '2020-11-20 10:41:16', 0, 0, 0, 11, 1, 0),
(11, 0, 'resources/images/profile/message-1605869033568.png', 'Group 4', 'Test Description', '2020-11-20 10:43:53', 0, 0, 0, 11, 1, 0),
(12, 0, '/resources/images/profile/message-1605869342452.png', 'Group 55', 'Test Description', '2020-11-20 10:49:02', 0, 0, 0, 11, 1, 0),
(13, 0, '', 'Teacher 11th Grade group', '', '2020-11-20 11:15:30', 1, 11, 3, 11, 1, 1),
(14, 0, '', 'Teacher 1st Grade group', '', '2020-11-20 11:24:31', 1, 1, 3, 0, 1, 0),
(15, 0, '', 'Teacher 2nd Grade group', '', '2020-11-20 11:24:31', 1, 2, 3, 0, 1, 0),
(16, 0, '/resources/images/profile/message-1605872149577.PNG', 'hellodes', 'descriptiondescriptiondescriptiondescriptiondescriptiondescription', '2020-11-20 11:35:49', 0, 0, 0, 11, 1, 0),
(17, 0, '', 'Teacher 7th Grade group', '', '2020-11-20 12:34:17', 1, 7, 3, 11, 1, 1),
(18, 0, '', 'Teacher 10th Grade group', '', '2020-11-20 12:34:17', 1, 10, 3, 11, 1, 1),
(19, 0, '', 'Teacher 1st Grade group', '', '2020-11-20 12:34:47', 1, 1, 3, 11, 1, 0),
(20, 0, '', 'Teacher 2nd Grade group', '', '2020-11-20 12:34:47', 1, 2, 3, 11, 1, 0),
(21, 0, '/resources/images/profile/message-1605878822087.jpg', 'test group test ', 'His name was the best thing I have to say in a while he has ', '2020-11-20 13:27:02', 0, 0, 0, 11, 1, 0),
(22, 0, '/resources/images/profile/message-1605937709606.jpg', 'tie ', 'We ', '2020-11-21 05:48:30', 0, 0, 0, 11, 1, 0),
(23, 0, '', 'Vjvjgi', '', '2020-11-21 06:33:02', 0, 0, 0, 11, 1, 0),
(24, 0, '/resources/images/profile/file-1605943154272.jpeg', 'Testing great', 'New grp created.', '2020-11-21 07:19:14', 0, 0, 0, 11, 1, 0),
(25, 0, '', 'peter parera 1st Grade group', '', '2020-11-21 10:38:37', 1, 1, 1, 12, 1, 0),
(40, 0, '', 'Teachernn Kindergarten group', '', '2020-11-21 11:03:18', 1, 0, 3, 190, 1, 0),
(41, 0, '', 'Teachernn 1st Grade group', '', '2020-11-21 11:03:18', 1, 1, 3, 190, 1, 0),
(42, 0, '', 'Teachernn 2nd Grade group', '', '2020-11-21 11:03:18', 1, 2, 3, 190, 1, 0),
(43, 0, '', 'Teachernn 3rd Grade group', '', '2020-11-21 11:03:18', 1, 3, 3, 190, 1, 0),
(44, 0, '', 'Teachernn 4th Grade group', '', '2020-11-21 11:03:18', 1, 4, 3, 190, 1, 0),
(45, 0, '', 'Teachernn 5th Grade group', '', '2020-11-21 11:03:18', 1, 5, 3, 190, 1, 0),
(46, 0, '', 'Teachernn 6th Grade group', 'Hello new image.', '2020-11-21 11:03:18', 1, 6, 3, 190, 1, 0),
(47, 0, '', 'Teachernn 7th Grade group', 'This is auto created grp.\nHellos', '2020-11-21 11:03:18', 1, 7, 3, 190, 1, 0),
(48, 0, '', 'Ttt11', 'Gehhbd', '2020-11-21 11:06:04', 0, 0, 0, 190, 1, 0),
(49, 0, '', 'Teacher 3rd Grade group', '', '2020-11-21 12:47:52', 1, 3, 3, 11, 1, 0),
(50, 0, '', 'Teacher 6th Grade group', '', '2020-11-21 12:47:59', 1, 6, 3, 11, 1, 0),
(51, 0, '', 'Teacher 4th Grade group', '', '2020-11-21 12:55:48', 1, 4, 3, 11, 1, 0),
(52, 0, '/resources/images/profile/message-1605965324987.jpg', 'testing ', 'Hello to your new video ', '2020-11-21 13:28:45', 0, 0, 0, 194, 1, 0),
(53, 0, '/resources/images/profile/message-1605965809122.jpg', 'test group 2', 'Testing ', '2020-11-21 13:36:49', 0, 0, 0, 194, 1, 0),
(54, 0, '/resources/images/profile/message-1605965974791.jpg', 'test group 3', 'Test des', '2020-11-21 13:39:34', 0, 0, 0, 194, 1, 0),
(56, 0, '', 'Ms. jeera 1st Grade', '', '2020-11-22 07:09:40', 1, 1, 7, 196, 1, 1),
(57, 0, '', 'Ms. jeera 2nd Grade', '', '2020-11-22 07:10:35', 1, 2, 7, 196, 1, 0),
(58, 0, '', 'Ms. jeera 4th Grade', '', '2020-11-22 07:10:35', 1, 4, 7, 196, 1, 0),
(59, 0, '', 'Ms. jeera 1st Grade', '', '2020-11-22 07:11:49', 1, 1, 7, 196, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `chat_message`
--

CREATE TABLE `chat_message` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `message` text DEFAULT NULL,
  `type` enum('image','video','document','text') NOT NULL DEFAULT 'text',
  `file_format` varchar(20) NOT NULL DEFAULT '',
  `original_filename` varchar(255) NOT NULL DEFAULT '',
  `mimetype` varchar(50) NOT NULL DEFAULT '',
  `thumbnail` varchar(100) NOT NULL DEFAULT '',
  `parent_message_id` int(11) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `is_active` smallint(6) NOT NULL DEFAULT 1,
  `deleted` smallint(6) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chat_message`
--

INSERT INTO `chat_message` (`id`, `message`, `type`, `file_format`, `original_filename`, `mimetype`, `thumbnail`, `parent_message_id`, `created_by`, `created_at`, `is_active`, `deleted`) VALUES
(1, 'Hello 1', 'text', '', '', '', '', 0, 11, '2020-11-18 13:39:07', 1, 1),
(2, 'Hello 2', 'text', '', '', '', '', 0, 11, '2020-11-17 13:39:07', 1, 1),
(3, 'HEllo', 'text', '', '', '', '', 0, 11, '2020-11-18 13:51:21', 1, 1),
(26, '0m02mx1z50', 'text', '', '', '', '', 0, 11, '2020-11-19 12:33:48', 1, 1),
(27, '0m02mx1z50', 'text', '', '', '', '', 0, 11, '2020-11-19 12:39:43', 1, 1),
(28, '0m02mx1z50', 'text', '', '', '', '', 0, 11, '2020-11-19 12:40:50', 1, 1),
(29, '0m02mx1z50', 'text', '', '', '', '', 0, 11, '2020-11-19 12:41:32', 1, 1),
(30, '0m02mx1z50', 'text', '', '', '', '', 0, 11, '2020-11-19 12:41:42', 1, 1),
(31, '0m02mx1z50', 'text', '', '', '', '', 0, 11, '2020-11-19 12:42:56', 1, 1),
(32, '0m02mx1z50', 'text', '', '', '', '', 0, 11, '2020-11-19 12:43:51', 1, 1),
(33, '0m02mx1z50', 'text', '', '', '', '', 0, 11, '2020-11-19 12:44:15', 1, 1),
(34, '0m02mx1z50', 'text', '', '', '', '', 0, 11, '2020-11-19 14:26:19', 1, 1),
(35, 'hii 4654654', 'text', '', '', '', '', 0, 11, '2020-11-11 14:30:04', 1, 1),
(36, 'Group chat ', 'text', '', '', '', '', 0, 11, '2020-11-19 14:30:17', 1, 1),
(37, '10000', 'text', '', '', '', '', 0, 11, '2020-11-20 05:10:50', 1, 1),
(38, '5002', 'text', '', '', '', '', 0, 11, '2020-11-20 05:26:44', 1, 1),
(39, '898989898', 'text', '', '', '', '', 0, 11, '2020-11-21 05:10:50', 1, 1),
(40, '1546', 'text', '', '', '', '', 0, 11, '2020-11-20 06:28:22', 1, 1),
(41, '0m02mx1z50', 'text', '', '', '', '', 0, 11, '2020-11-20 07:05:30', 1, 1),
(42, '0m02mx1z50', 'text', '', '', '', '', 0, 11, '2020-11-20 07:05:41', 1, 1),
(43, 'ghjghj.', 'text', '', '', '', '', 0, 11, '2020-11-20 07:42:36', 1, 1),
(44, 'fghfgh', 'text', '', '', '', '', 0, 11, '2020-11-20 07:42:47', 1, 1),
(45, '0m02mx1z50', 'text', '', '', '', '', 0, 20, '2020-11-20 07:56:51', 1, 1),
(46, '0m02mx1z50', 'text', '', '', '', '', 0, 20, '2020-11-20 07:57:13', 1, 1),
(47, '0m02mx1z50', 'text', '', '', '', '', 0, 20, '2020-11-20 07:57:59', 1, 1),
(48, '0m02mx1z50', 'text', '', '', '', '', 0, 20, '2020-11-20 07:58:31', 1, 1),
(49, '0m02mx1z50', 'text', '', '', '', '', 0, 20, '2020-11-20 09:12:16', 1, 0),
(51, 'Hello', 'text', '', '', '', '', 0, 11, '2020-11-20 10:18:07', 1, 0),
(52, 'Hi', 'text', '', '', '', '', 0, 20, '2020-11-20 10:18:30', 1, 0),
(55, 'dfdfgdfg', 'text', '', '', '', '', 0, 11, '2020-11-20 12:48:28', 1, 0),
(57, 'Hi', 'text', '', '', '', '', 0, 11, '2020-11-21 06:04:55', 1, 0),
(58, 'Gh', 'text', '', '', '', '', 0, 11, '2020-11-21 06:26:30', 1, 0),
(59, 'Vjvjgig', 'text', '', '', '', '', 0, 11, '2020-11-21 08:11:01', 1, 0),
(60, 'Hi', 'text', '', '', '', '', 0, 190, '2020-11-21 11:04:31', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `chat_message_recipients`
--

CREATE TABLE `chat_message_recipients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `recipient_id` int(11) NOT NULL DEFAULT 0,
  `recipient_group_id` int(11) NOT NULL DEFAULT 0,
  `message_id` int(11) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `is_read` smallint(6) NOT NULL DEFAULT 0,
  `is_active` smallint(6) NOT NULL DEFAULT 0,
  `deleted` smallint(6) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chat_message_recipients`
--

INSERT INTO `chat_message_recipients` (`id`, `recipient_id`, `recipient_group_id`, `message_id`, `created_by`, `is_read`, `is_active`, `deleted`) VALUES
(1, 20, 0, 1, 11, 1, 0, 0),
(2, 158, 1, 4, 11, 0, 1, 0),
(3, 159, 1, 4, 11, 0, 1, 0),
(4, 158, 1, 5, 11, 0, 1, 0),
(5, 159, 1, 5, 11, 0, 1, 0),
(6, 20, 0, 6, 11, 1, 1, 0),
(7, 158, 1, 7, 11, 0, 1, 0),
(8, 159, 1, 7, 11, 0, 1, 0),
(9, 158, 1, 8, 11, 0, 1, 0),
(10, 159, 1, 8, 11, 0, 1, 0),
(11, 20, 0, 9, 11, 1, 1, 0),
(12, 66, 0, 10, 74, 0, 1, 0),
(13, 74, 5, 11, 11, 0, 1, 0),
(14, 20, 0, 12, 11, 1, 0, 0),
(15, 20, 0, 13, 11, 1, 0, 0),
(16, 20, 0, 14, 11, 1, 0, 0),
(17, 20, 0, 15, 11, 1, 0, 0),
(18, 20, 0, 16, 11, 1, 0, 0),
(19, 20, 0, 17, 11, 1, 0, 0),
(20, 20, 0, 18, 11, 1, 0, 0),
(21, 20, 0, 19, 11, 1, 0, 0),
(22, 20, 0, 20, 11, 1, 0, 0),
(23, 20, 0, 21, 11, 1, 0, 0),
(24, 20, 0, 22, 11, 1, 0, 0),
(25, 20, 0, 23, 11, 1, 0, 0),
(26, 20, 0, 24, 11, 1, 0, 0),
(27, 74, 0, 25, 11, 0, 0, 0),
(28, 74, 0, 26, 11, 0, 0, 0),
(29, 20, 0, 27, 11, 1, 0, 0),
(30, 20, 0, 28, 11, 1, 0, 0),
(31, 20, 0, 29, 11, 1, 0, 0),
(32, 74, 0, 30, 11, 0, 0, 0),
(33, 74, 0, 31, 11, 0, 0, 0),
(34, 20, 0, 32, 11, 1, 0, 0),
(35, 20, 0, 33, 11, 1, 0, 0),
(36, 74, 0, 34, 11, 0, 0, 0),
(37, 74, 0, 35, 11, 0, 1, 0),
(38, 158, 1, 36, 11, 0, 1, 0),
(39, 159, 1, 36, 11, 0, 1, 0),
(40, 74, 6, 37, 11, 0, 1, 0),
(41, 158, 1, 38, 11, 0, 1, 0),
(42, 159, 1, 38, 11, 0, 1, 0),
(43, 159, 1, 39, 11, 0, 1, 0),
(44, 74, 6, 40, 11, 0, 1, 0),
(45, 20, 0, 41, 11, 1, 0, 0),
(46, 20, 0, 42, 11, 1, 0, 0),
(47, 158, 1, 43, 11, 0, 1, 0),
(48, 159, 1, 43, 11, 0, 1, 0),
(49, 20, 0, 44, 11, 1, 1, 0),
(50, 11, 0, 45, 20, 1, 0, 0),
(51, 11, 0, 46, 20, 1, 0, 0),
(52, 11, 0, 47, 20, 1, 0, 0),
(53, 11, 0, 48, 20, 1, 0, 0),
(54, 11, 0, 49, 20, 1, 0, 0),
(55, 20, 9, 51, 11, 1, 1, 0),
(56, 11, 9, 52, 20, 1, 1, 0),
(57, 1, 16, 55, 11, 0, 1, 0),
(58, 2, 16, 55, 11, 0, 1, 0),
(59, 3, 16, 55, 11, 1, 1, 0),
(60, 158, 1, 57, 11, 0, 1, 0),
(61, 159, 1, 57, 11, 0, 1, 0),
(62, 20, 1, 57, 11, 0, 1, 0),
(63, 20, 0, 58, 11, 0, 1, 0),
(64, 20, 24, 59, 11, 1, 1, 0),
(65, 11, 0, 60, 190, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `chat_room`
--

CREATE TABLE `chat_room` (
  `id` int(11) NOT NULL,
  `first_user` varchar(11) NOT NULL,
  `first_user_chat_id` varchar(100) NOT NULL DEFAULT '0',
  `first_user_archive` int(11) NOT NULL DEFAULT 0,
  `first_user_delete` int(11) NOT NULL DEFAULT 0,
  `second_user` varchar(11) NOT NULL,
  `second_user_chat_id` varchar(100) NOT NULL DEFAULT '0',
  `second_user_archive` int(11) NOT NULL DEFAULT 0,
  `second_user_delete` int(11) NOT NULL DEFAULT 0,
  `job_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chat_user_group_assoc`
--

CREATE TABLE `chat_user_group_assoc` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `group_id` int(11) NOT NULL DEFAULT 0,
  `is_active` smallint(6) NOT NULL DEFAULT 1,
  `created` datetime NOT NULL,
  `deleted` smallint(6) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chat_user_group_assoc`
--

INSERT INTO `chat_user_group_assoc` (`id`, `user_id`, `group_id`, `is_active`, `created`, `deleted`) VALUES
(15, 11, 6, 1, '2020-11-19 14:31:09', 0),
(16, 74, 6, 1, '2020-11-19 14:31:09', 0),
(17, 20, 7, 1, '2020-11-20 08:04:59', 0),
(18, 11, 7, 1, '2020-11-20 08:04:59', 0),
(19, 11, 8, 1, '2020-11-20 08:04:59', 0),
(22, 11, 9, 1, '2020-11-20 10:17:47', 0),
(23, 20, 9, 0, '2020-11-20 10:17:47', 1),
(24, 11, 10, 1, '2020-11-20 10:41:16', 0),
(25, 11, 10, 1, '2020-11-20 10:41:16', 0),
(26, 11, 11, 1, '2020-11-20 10:43:53', 0),
(27, 12, 11, 1, '2020-11-20 10:43:53', 0),
(28, 11, 12, 1, '2020-11-20 10:49:02', 0),
(29, 12, 12, 1, '2020-11-20 10:49:02', 0),
(30, 11, 13, 1, '0000-00-00 00:00:00', 0),
(31, 11, 14, 1, '0000-00-00 00:00:00', 0),
(32, 11, 15, 1, '0000-00-00 00:00:00', 0),
(33, 11, 16, 1, '2020-11-20 11:35:49', 0),
(34, 1, 16, 1, '2020-11-20 11:35:49', 0),
(35, 2, 16, 1, '2020-11-20 11:35:49', 0),
(36, 3, 16, 1, '2020-11-20 11:35:49', 0),
(37, 11, 17, 1, '2020-11-20 12:34:17', 0),
(38, 11, 18, 1, '2020-11-20 12:34:17', 0),
(39, 11, 19, 1, '2020-11-20 12:34:47', 1),
(40, 11, 20, 1, '2020-11-20 12:34:47', 1),
(42, 20, 2, 1, '2020-11-20 12:51:38', 0),
(43, 20, 19, 1, '2020-11-20 12:54:38', 1),
(44, 20, 20, 1, '2020-11-20 12:54:38', 1),
(45, 11, 21, 1, '2020-11-20 13:27:02', 0),
(46, 0, 21, 1, '2020-11-20 13:27:02', 0),
(47, 11, 22, 1, '2020-11-21 05:48:30', 0),
(48, 0, 22, 1, '2020-11-21 05:48:30', 0),
(49, 11, 23, 1, '2020-11-21 06:33:02', 0),
(50, 20, 23, 1, '2020-11-21 06:33:02', 0),
(53, 11, 24, 1, '2020-11-21 08:13:04', 0),
(54, 20, 24, 1, '2020-11-21 08:13:04', 0),
(55, 12, 25, 1, '2020-11-21 10:38:37', 0),
(70, 190, 40, 1, '2020-11-21 11:03:18', 0),
(71, 190, 41, 1, '2020-11-21 11:03:18', 0),
(72, 190, 42, 1, '2020-11-21 11:03:18', 0),
(73, 190, 43, 1, '2020-11-21 11:03:18', 0),
(74, 190, 44, 1, '2020-11-21 11:03:18', 0),
(75, 190, 45, 1, '2020-11-21 11:03:18', 0),
(76, 190, 46, 1, '2020-11-21 11:03:18', 0),
(83, 11, 49, 1, '2020-11-21 12:47:52', 0),
(84, 11, 50, 1, '2020-11-21 12:47:59', 0),
(87, 11, 51, 1, '2020-11-21 12:55:48', 0),
(88, 194, 52, 1, '2020-11-21 13:28:45', 0),
(89, 0, 52, 1, '2020-11-21 13:28:45', 0),
(90, 194, 53, 1, '2020-11-21 13:36:49', 0),
(91, 0, 53, 1, '2020-11-21 13:36:49', 0),
(92, 194, 54, 1, '2020-11-21 13:39:34', 0),
(93, 0, 54, 1, '2020-11-21 13:39:34', 0),
(94, 190, 48, 1, '2020-11-21 14:20:16', 0),
(95, 11, 48, 1, '2020-11-21 14:20:16', 0),
(96, 11, 1, 1, '2020-11-21 14:37:31', 0),
(97, 1, 1, 1, '2020-11-21 14:37:31', 0),
(98, 2, 1, 1, '2020-11-21 14:37:31', 0),
(99, 3, 1, 1, '2020-11-21 14:37:31', 0),
(100, 4, 1, 1, '2020-11-21 14:37:31', 0),
(101, 20, 19, 1, '2020-11-22 06:26:28', 0),
(102, 196, 56, 1, '2020-11-22 07:09:40', 0),
(103, 196, 57, 1, '2020-11-22 07:10:35', 0),
(104, 196, 58, 1, '2020-11-22 07:10:35', 0),
(105, 196, 59, 1, '2020-11-22 07:11:49', 0);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT 0,
  `media_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment_type` tinyint(1) DEFAULT NULL COMMENT '1=Media, 2 = Text',
  `comment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `parent_id`, `media_id`, `user_id`, `comment_type`, `comment`, `created`) VALUES
(1, 0, 1, 4, 2, 'NEw comment', '2020-11-06 12:57:32'),
(2, 0, 1, 4, 2, 'NEw comment ggterttet', '2020-11-06 12:57:45'),
(3, 13, 1, 4, 2, 'Reply of new comment', '2020-11-06 12:59:13'),
(4, 0, 17, 4, 2, 'NEw comment ggterttet', '2020-11-06 13:04:52'),
(5, 0, 17, 4, 2, 'NEw comment ggterttet', '2020-11-06 13:07:19'),
(6, 0, 6, 20, 2, 'Hi', '2020-11-07 05:42:20'),
(7, 0, 6, 20, 2, 'Hi1', '2020-11-07 05:42:26'),
(8, 0, 6, 11, 2, 'Hello Dlast', '2020-11-07 05:43:41'),
(9, 6, 6, 11, 2, 'Hello test', '2020-11-07 05:44:11'),
(10, 6, 6, 11, 2, 'Thanks for commenting 100', '2020-11-07 05:44:25'),
(19, 6, 6, 11, 2, 'Thanks for commenting 200', '2020-11-07 05:44:25'),
(20, 6, 6, 11, 2, 'Thanks for commenting 300', '2020-11-07 05:44:25'),
(21, 0, 2, 11, 1, '1', '2020-11-08 15:21:31'),
(22, 0, 2, 11, 1, '1', '2020-11-08 15:21:40'),
(23, 0, 30, 73, 2, 'test', '2020-11-09 06:22:00'),
(24, 0, 30, 73, 2, 'Hello', '2020-11-09 06:22:12'),
(25, 23, 30, 73, 2, 'hiiii', '2020-11-09 06:22:35'),
(26, 0, 30, 73, 2, 'test user', '2020-11-09 06:27:42'),
(27, 25, 30, 73, 2, 'testtttttt', '2020-11-09 06:36:04'),
(28, 0, 31, 72, 2, 'hiiii', '2020-11-09 06:48:32'),
(29, 0, 31, 72, 2, 'hellooo', '2020-11-09 06:48:38'),
(30, 0, 31, 72, 1, '1', '2020-11-09 07:11:45'),
(31, 28, 31, 72, 2, 'hiiii', '2020-11-09 07:14:21'),
(32, 0, 6, 11, 1, '1', '2020-11-09 07:15:29'),
(33, 0, 31, 72, 1, '1', '2020-11-09 07:15:39'),
(34, 33, 31, 72, 2, 'hiiiii', '2020-11-09 07:17:44'),
(35, 8, 6, 11, 2, 'test', '2020-11-09 07:25:13'),
(36, 8, 6, 11, 2, 'test20', '2020-11-09 07:25:30'),
(37, 33, 31, 72, 2, 'hiiiiiiii', '2020-11-09 07:30:19'),
(38, 34, 31, 72, 2, 'ggggggg', '2020-11-09 07:32:57'),
(39, 30, 31, 72, 2, 'tttttt', '2020-11-09 07:33:29'),
(40, 31, 31, 72, 2, 'hiiiiiiiiiiiiiiiiii reply', '2020-11-09 07:55:04'),
(41, 28, 31, 72, 2, 'yyyyyyyyy makes', '2020-11-09 07:56:20'),
(42, 0, 31, 72, 2, 'test user interface is a', '2020-11-09 07:57:41'),
(43, 0, 31, 72, 2, 'trehhtdd', '2020-11-09 08:00:35'),
(44, 0, 6, 11, 2, 'mertu', '2020-11-09 08:01:50'),
(45, 0, 6, 11, 2, 'rtyyy', '2020-11-09 08:03:43'),
(46, 0, 6, 11, 2, 'teeeeeeeeeeeet', '2020-11-09 08:04:45'),
(47, 0, 6, 11, 2, 'rety', '2020-11-09 08:05:57'),
(48, 28, 31, 72, 2, 'tubfstknvfty', '2020-11-09 08:17:28'),
(49, 0, 6, 11, 1, '36', '2020-11-09 09:30:56'),
(51, 0, 6, 11, 1, '38', '2020-11-09 09:47:48'),
(52, 0, 3, 11, 2, 'test', '2020-11-09 09:54:08'),
(53, 0, 3, 11, 1, '39', '2020-11-09 09:54:29'),
(54, 0, 6, 11, 1, '40', '2020-11-09 10:13:34'),
(55, 0, 39, 11, 2, 'test video comment', '2020-11-09 10:17:28'),
(56, 54, 6, 11, 2, 'test', '2020-11-09 11:41:23'),
(57, 56, 6, 11, 2, 'test', '2020-11-09 11:41:37'),
(58, 57, 6, 11, 2, 'testtttt', '2020-11-09 11:42:52'),
(59, 43, 31, 72, 2, 'dsfsdfsdf kjklajsd klajsdklasd', '2020-11-09 11:49:51'),
(60, 43, 31, 72, 2, 'hi alll test user data with new', '2020-11-09 11:50:56'),
(61, 42, 31, 72, 2, 'hi allll jhasjdh aksdjasdkl', '2020-11-09 11:51:19'),
(62, 53, 3, 11, 2, 'test', '2020-11-09 11:54:24'),
(63, 62, 3, 11, 2, 'test', '2020-11-09 11:54:30'),
(64, 30, 31, 72, 2, 'jhjkhjk highly jkhjk', '2020-11-09 12:13:22'),
(65, 26, 30, 73, 2, 'hiiiiiii', '2020-11-10 05:05:17'),
(66, 26, 30, 73, 2, 'hellllooooooooo', '2020-11-10 05:05:27'),
(67, 26, 30, 73, 2, 'bharat rawallllllll', '2020-11-10 05:05:37'),
(68, 0, 30, 73, 2, 'dsfsdfsdf dsfsdfsdf dsfsdfsdf sadfdsfds', '2020-11-10 05:06:18'),
(69, 0, 30, 73, 2, 'dsfsdfsdf saddest', '2020-11-10 05:06:23'),
(70, 59, 31, 72, 2, 'hiiiiii', '2020-11-10 06:06:18'),
(71, 59, 31, 72, 2, 'hiiiiiiii reply', '2020-11-10 06:12:53'),
(72, 0, 31, 72, 1, '41', '2020-11-10 06:19:08'),
(73, 28, 31, 72, 2, 'fghjfghfghfgh', '2020-11-10 06:40:23'),
(74, 28, 31, 72, 2, 'dsfsdfsdf sdfsdf', '2020-11-10 06:40:57'),
(75, 22, 2, 11, 2, 'fgdfgdfgd', '2020-11-10 07:02:57'),
(76, 59, 31, 72, 2, 'fgvrgrgf', '2020-11-10 07:05:55'),
(77, 74, 31, 72, 2, 'dfgdfgdfg', '2020-11-10 07:07:22'),
(78, 72, 31, 72, 2, 'ok iPad stop asdfiop oadisfopisdfop', '2020-11-10 07:10:06'),
(79, 72, 31, 72, 2, 'staff sdfsdf sdfsadfas assessed sdfsd', '2020-11-10 07:20:34'),
(80, 72, 31, 72, 2, 'dsfsdfsdf Arafat?s asdfadsf', '2020-11-10 07:20:41'),
(81, 0, 6, 11, 1, '46', '2020-11-10 07:50:16'),
(82, 0, 6, 11, 1, '47', '2020-11-10 07:51:00'),
(83, 0, 9, 75, 2, 'hii', '2020-11-10 07:54:03'),
(84, 0, 9, 75, 2, 'fusjtstjstjsy', '2020-11-10 07:54:19'),
(85, 69, 30, 73, 2, 'dyktodkydkydlydykd', '2020-11-10 07:55:42'),
(86, 0, 30, 73, 2, 'gkxkyxkyxkyx dkydy', '2020-11-10 07:56:49'),
(87, 0, 30, 73, 2, 'cigifi', '2020-11-10 07:58:56'),
(88, 0, 30, 73, 2, 'Gigi?s', '2020-11-10 08:00:06'),
(89, 0, 30, 73, 2, 'hiii', '2020-11-10 08:03:30'),
(90, 26, 30, 73, 2, 'ggd', '2020-11-10 08:04:20'),
(91, 26, 30, 73, 2, 'hdjajaj', '2020-11-10 08:04:33'),
(92, 0, 6, 11, 1, '57', '2020-11-10 08:06:23'),
(93, 0, 30, 73, 2, 'fugitive', '2020-11-10 08:07:19'),
(94, 0, 30, 73, 2, 'chchc', '2020-11-10 08:17:18'),
(95, 0, 30, 73, 2, 'jfkfifo', '2020-11-10 08:18:02'),
(96, 0, 30, 73, 2, 'fugitive', '2020-11-10 08:24:12'),
(97, 0, 9, 75, 2, 'haiwjeje', '2020-11-10 09:19:25'),
(98, 0, 30, 73, 2, 'hands', '2020-11-10 09:21:08'),
(99, 0, 30, 73, 2, 'judicial', '2020-11-10 09:21:43'),
(100, 0, 30, 73, 2, 'fugitive', '2020-11-10 09:22:36'),
(101, 0, 30, 73, 2, 'hi', '2020-11-10 09:23:36'),
(102, 0, 30, 73, 2, 'hello', '2020-11-10 09:24:22'),
(103, 0, 30, 73, 2, 'hello', '2020-11-10 09:26:02'),
(104, 0, 30, 73, 2, 'fugitive', '2020-11-10 09:26:22'),
(105, 0, 6, 11, 1, '61', '2020-11-10 10:10:12'),
(106, 0, 63, 91, 2, 'hiii', '2020-11-10 10:46:40'),
(107, 106, 63, 91, 2, 'ghlcoyxy', '2020-11-10 11:01:40'),
(108, 0, 58, 11, 1, '65', '2020-11-10 11:09:48'),
(109, 106, 63, 88, 2, 'hdhd end', '2020-11-10 11:11:44'),
(110, 106, 63, 88, 2, 'bei end e', '2020-11-10 11:12:11'),
(111, 106, 63, 88, 2, 'bsbsbs', '2020-11-10 11:12:20'),
(112, 0, 63, 88, 1, '66', '2020-11-10 11:13:10'),
(113, 0, 63, 88, 2, 'brushed', '2020-11-10 11:13:23'),
(114, 0, 63, 88, 2, 'Hansberry', '2020-11-10 11:13:30'),
(115, 0, 63, 88, 2, 'rhrhtb', '2020-11-10 11:15:32'),
(116, 0, 63, 88, 2, 'Sydney', '2020-11-10 11:16:56'),
(117, 0, 63, 91, 2, 'oyxoycyoc', '2020-11-10 11:18:27'),
(118, 0, 63, 88, 1, '67', '2020-11-10 11:26:57'),
(119, 0, 63, 88, 2, 'NEw comment', '2020-11-10 11:29:38'),
(120, 0, 63, 88, 2, 'NEw comment', '2020-11-10 11:30:25'),
(121, 0, 63, 88, 2, '\\U263a\\Ufe0f\\U263a\\Ufe0f\\U263a\\Ufe0f\\U263a\\Ufe0f\\U263a\\Ufe0f\\U263a\\Ufe0f\\U263a\\Ufe0f\\U263a\\Ufe0f\\U263a\\Ufe0f\\U263a\\Ufe0f\\U263a\\Ufe0f\\U263a\\Ufe0f\\U263a\\Ufe0f\\Ud83d\\Udc4c\\Ud83d\\Ude0d\\U2764\\Ufe0f\\U2764\\Ufe0f\\Ud83d\\Ude01\\U2764\\Ufe0f\\Ud83d\\Ude0d\\Ud83d\\Ude29\\Ud83d\\Ude0d\\Ud83d\\Ude0d\\Ud83d\\Ude29\\Ud83d\\Ude0d\\Ud83d\\Ude33\\Ud83d\\Ude33\\Ud83d\\Ude0d\\Ud83d\\Ude0d', '2020-11-10 11:48:42'),
(122, 0, 63, 88, 2, '☺️✌️☺️✌️', '2020-11-10 11:54:31'),
(123, 0, 60, 11, 2, 'test', '2020-11-10 12:56:35'),
(124, 0, 60, 11, 2, 'errere', '2020-11-10 12:58:10'),
(125, 0, 60, 11, 2, 'test3', '2020-11-10 12:59:08'),
(126, 0, 60, 11, 2, 'test4', '2020-11-10 12:59:13'),
(127, 0, 60, 11, 2, 'tet5', '2020-11-10 12:59:18'),
(128, 0, 60, 11, 2, 'test6', '2020-11-10 12:59:21'),
(129, 0, 60, 11, 2, 'tet7', '2020-11-10 12:59:27'),
(130, 0, 60, 11, 2, 'test8', '2020-11-10 12:59:33'),
(131, 0, 60, 11, 2, 'test9', '2020-11-10 13:01:08'),
(132, 0, 60, 11, 2, 'test10', '2020-11-10 13:01:15'),
(133, 0, 60, 11, 2, 'test12', '2020-11-10 13:01:21'),
(134, 0, 63, 91, 2, '????????????????', '2020-11-10 14:03:49'),
(135, 0, 63, 91, 2, 'ffg', '2020-11-10 14:16:15'),
(136, 112, 63, 91, 2, 'yes but', '2020-11-11 06:10:50'),
(137, 0, 63, 91, 2, 'CGI it’d make my life so bad again when you’re trying something different from others than what they were looking like and now', '2020-11-11 07:58:04'),
(138, 111, 63, 91, 2, 'really great game with friends that can get better graphics would recommend', '2020-11-11 08:01:47'),
(139, 113, 63, 91, 2, 'hello to our beautiful girl who will have an amazing birthday present today thank goodness she had her surgery for', '2020-11-11 08:02:17'),
(140, 113, 63, 91, 2, 'hello this morning we you need some new work done', '2020-11-11 08:05:44'),
(141, 137, 63, 91, 2, 'hi honey you know what you want your name out of', '2020-11-11 08:06:16'),
(142, 0, 32, 81, 2, 'test', '2020-11-11 08:52:48'),
(143, 142, 32, 81, 2, 'test2', '2020-11-11 08:54:09'),
(144, 0, 68, 88, 2, 'main comment', '2020-11-11 10:37:52'),
(145, 144, 68, 88, 2, 'hello sub', '2020-11-11 10:41:55'),
(146, 0, 4, 11, 2, 'tets', '2020-11-11 10:57:20'),
(147, 0, 32, 81, 2, 'New 1', '2020-11-11 12:53:24'),
(148, 142, 32, 81, 2, 'New 2', '2020-11-11 12:53:32'),
(149, 142, 32, 81, 2, 'New 3', '2020-11-11 12:53:41'),
(150, 0, 70, 91, 2, 'main', '2020-11-11 14:05:43'),
(151, 150, 70, 91, 2, 'sub com', '2020-11-11 14:06:01'),
(152, 0, 70, 91, 2, 'guvuvu', '2020-11-11 14:11:42'),
(153, 0, 70, 91, 2, 'cbbgckhg', '2020-11-11 14:13:38'),
(154, 0, 70, 91, 2, 'gjhgj', '2020-11-11 14:13:56'),
(155, 154, 70, 91, 2, 'gigiifjchcjchchxjxhcjcjccxh bfjcjjj', '2020-11-11 14:14:17'),
(156, 154, 70, 91, 2, 'fuguufufhcjchchxjxhcj', '2020-11-11 14:14:29'),
(157, 0, 70, 91, 2, 'u hv hcucuchcjfjfjjtuuhy', '2020-11-11 14:15:45'),
(158, 0, 70, 91, 2, 'invigorate', '2020-11-11 14:16:03'),
(159, 0, 71, 91, 2, 'Hiiii', '2020-11-12 05:37:50'),
(160, 0, 71, 91, 2, 'Hiiii syb', '2020-11-12 05:38:05'),
(161, 159, 71, 91, 2, 'Hi sub mau', '2020-11-12 05:38:19'),
(162, 161, 71, 91, 2, 'Hsihsihsi', '2020-11-12 05:38:33'),
(163, 161, 71, 91, 2, 'Sub to sub', '2020-11-12 05:39:02'),
(164, 0, 71, 91, 2, 'Hi', '2020-11-12 05:39:18'),
(165, 164, 71, 91, 2, 'Hiii', '2020-11-12 05:39:26'),
(166, 164, 71, 91, 2, 'Vuvuv', '2020-11-12 05:41:05'),
(167, 164, 71, 91, 2, 'Ivuvuvuv', '2020-11-12 05:41:18'),
(168, 0, 71, 91, 2, 'cylcotdotdtod kcitxktxktxktxktxkyxyl', '2020-11-12 05:51:57'),
(169, 158, 70, 91, 2, 'Hii vjsisgigeis hdihodhdohod', '2020-11-12 05:58:36'),
(170, 158, 70, 91, 2, 'Hisbosbkdvodvidb', '2020-11-12 05:59:39'),
(171, 0, 70, 88, 2, 'hello to', '2020-11-12 06:00:58'),
(172, 171, 70, 91, 2, 'Bdjbidbdivodb', '2020-11-12 06:03:30'),
(173, 172, 70, 91, 2, 'Ihdhdihd', '2020-11-12 06:03:45'),
(174, 0, 63, 88, 2, 'hello', '2020-11-12 06:14:54'),
(175, 0, 62, 88, 2, 'hello main', '2020-11-12 06:16:56'),
(176, 175, 62, 88, 2, 'hello to your', '2020-11-12 06:17:10'),
(177, 0, 62, 88, 2, 'hello to your house', '2020-11-12 06:17:20'),
(178, 176, 62, 88, 2, 'sub to sub', '2020-11-12 06:17:47'),
(179, 175, 62, 88, 2, 'sub main', '2020-11-12 06:18:16'),
(180, 0, 62, 88, 1, '72', '2020-11-12 06:20:43'),
(181, 176, 62, 91, 2, 'Vsivsivsivso', '2020-11-12 06:48:30'),
(182, 176, 62, 91, 2, 'Tes', '2020-11-12 06:48:46'),
(183, 182, 62, 91, 2, 'Hiiii', '2020-11-12 06:48:56'),
(184, 0, 62, 91, 2, 'Fbdg', '2020-11-12 06:49:49'),
(185, 0, 62, 91, 2, 'Zvdvs', '2020-11-12 06:56:20'),
(186, 0, 62, 91, 2, 'Vsvdvd', '2020-11-12 06:57:10'),
(187, 0, 62, 91, 2, 'Sbsthstn', '2020-11-12 06:58:07'),
(188, 0, 62, 91, 2, 'Bsbsfhrs', '2020-11-12 06:59:15'),
(189, 0, 62, 91, 2, 'Sgjehst', '2020-11-12 07:00:43'),
(190, 0, 62, 91, 2, 'Sfhstj', '2020-11-12 07:00:55'),
(191, 0, 62, 88, 2, 'hello to your mother', '2020-11-12 07:12:24'),
(192, 0, 71, 88, 2, 'hello to you', '2020-11-12 07:28:56'),
(193, 0, 62, 88, 2, 'you will never forget about', '2020-11-12 07:35:50'),
(194, 0, 70, 88, 2, 'test', '2020-11-12 07:39:23'),
(195, 0, 71, 88, 2, 'hi', '2020-11-12 07:42:01'),
(196, 0, 77, 91, 2, 'hi', '2020-11-12 08:01:15'),
(197, 0, 78, 88, 2, 'Ckyckyxkgxkyx', '2020-11-12 08:04:03'),
(198, 0, 59, 11, 2, 'test', '2020-11-12 08:43:28'),
(199, 0, 87, 11, 2, 'yrt', '2020-11-19 05:53:14'),
(200, 0, 87, 11, 2, 'my comment', '2020-11-19 05:55:23'),
(201, 200, 87, 11, 2, 'rtrt', '2020-11-19 05:56:55'),
(202, 0, 83, 11, 2, 'www', '2020-11-19 11:34:33'),
(203, 202, 83, 11, 2, 'dd', '2020-11-19 11:35:11'),
(204, 0, 87, 11, 2, 'hhfghfghfgh', '2020-11-19 13:00:16'),
(205, 0, 87, 11, 2, 'fghdfghfghfghdfghfg', '2020-11-19 13:00:20'),
(206, 0, 87, 11, 2, 'gdfgdfgdsfgdfg', '2020-11-19 13:01:06'),
(207, 204, 87, 11, 2, 'adsfasfsf', '2020-11-19 13:16:37'),
(208, 200, 87, 11, 2, 'ert', '2020-11-19 13:23:35'),
(209, 0, 90, 11, 2, 'dsfsdfsdfdsf', '2020-11-20 13:25:41'),
(210, 209, 90, 11, 2, 'sdfsdfsf', '2020-11-21 09:29:28'),
(211, 0, 93, 11, 2, 'hui', '2020-11-21 12:19:11'),
(212, 0, 13, 194, 2, 'hiii', '2020-11-21 13:04:50');

-- --------------------------------------------------------

--
-- Table structure for table `comments_deleted`
--

CREATE TABLE `comments_deleted` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT 0,
  `media_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment_type` tinyint(1) DEFAULT NULL COMMENT '1=Media, 2 = Text',
  `comment` text DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comment_dislike`
--

CREATE TABLE `comment_dislike` (
  `id` int(11) NOT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comment_dislike_deleted`
--

CREATE TABLE `comment_dislike_deleted` (
  `id` int(11) NOT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT '0000-00-00 00:00:00',
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comment_like`
--

CREATE TABLE `comment_like` (
  `id` int(11) NOT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment_like`
--

INSERT INTO `comment_like` (`id`, `comment_id`, `user_id`, `created`) VALUES
(3, 6, 11, '2020-11-07 05:43:53'),
(4, 8, 11, '2020-11-07 05:47:25'),
(5, 15, 11, '2020-11-07 05:49:25'),
(7, 16, 20, '2020-11-07 05:49:50'),
(8, 16, 11, '2020-11-07 05:50:04'),
(9, 18, 11, '2020-11-07 05:50:38'),
(10, 1, 20, '2020-11-07 09:11:19'),
(23, 6, 20, '2020-11-07 09:30:32'),
(36, 10, 20, '2020-11-07 09:49:39'),
(37, 18, 20, '2020-11-07 09:49:54'),
(39, 11, 20, '2020-11-07 09:50:04'),
(57, 21, 11, '2020-11-09 06:06:47'),
(64, 25, 73, '2020-11-09 06:22:50'),
(71, 22, 11, '2020-11-09 06:28:50'),
(72, 24, 73, '2020-11-09 06:33:16'),
(74, 27, 73, '2020-11-09 06:38:50'),
(76, 32, 11, '2020-11-09 07:18:06'),
(89, 45, 11, '2020-11-09 08:22:30'),
(100, 47, 11, '2020-11-09 08:56:32'),
(105, 20, 10, '2020-11-09 12:07:10'),
(108, 9, 10, '2020-11-09 12:07:14'),
(110, 39, 72, '2020-11-10 06:07:07'),
(112, 64, 72, '2020-11-10 06:07:12'),
(116, 61, 72, '2020-11-10 06:08:37'),
(117, 43, 72, '2020-11-10 06:09:24'),
(132, 51, 11, '2020-11-10 06:46:59'),
(142, 59, 72, '2020-11-10 06:58:12'),
(143, 34, 72, '2020-11-10 06:58:18'),
(146, 75, 11, '2020-11-10 07:03:06'),
(148, 60, 72, '2020-11-10 07:05:13'),
(151, 69, 73, '2020-11-10 07:56:42'),
(152, 112, 88, '2020-11-10 11:14:07'),
(159, 117, 88, '2020-11-10 11:23:15'),
(161, 118, 91, '2020-11-10 12:03:39'),
(162, 106, 88, '2020-11-10 12:27:26'),
(168, 109, 88, '2020-11-10 12:46:23'),
(169, 110, 88, '2020-11-10 12:47:19'),
(170, 111, 91, '2020-11-10 14:02:06'),
(176, 135, 91, '2020-11-11 06:28:54'),
(179, 106, 91, '2020-11-11 06:32:01'),
(180, 109, 91, '2020-11-11 07:28:46'),
(181, 171, 88, '2020-11-12 06:01:17'),
(182, 171, 91, '2020-11-12 06:01:26'),
(183, 197, 91, '2020-11-12 08:04:21');

-- --------------------------------------------------------

--
-- Table structure for table `comment_like_deleted`
--

CREATE TABLE `comment_like_deleted` (
  `id` int(11) NOT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT '0000-00-00 00:00:00',
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comment_report`
--

CREATE TABLE `comment_report` (
  `id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(200) NOT NULL,
  `reason` text NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment_report`
--

INSERT INTO `comment_report` (`id`, `comment_id`, `user_id`, `type`, `reason`, `created`) VALUES
(1, 1, 20, '', '', '2020-11-06 13:01:49'),
(2, 25, 73, '', '', '2020-11-09 06:35:52'),
(3, 54, 0, '', '', '2020-11-09 11:36:01'),
(4, 51, 0, '', '', '2020-11-09 11:39:26'),
(5, 49, 0, '', '', '2020-11-09 11:40:01'),
(6, 47, 0, '', '', '2020-11-09 11:40:21'),
(7, 46, 0, '', '', '2020-11-09 11:40:58'),
(8, 43, 72, '', '', '2020-11-09 11:49:28'),
(9, 53, 0, '', '', '2020-11-09 11:51:09'),
(10, 9, 0, '', '', '2020-11-09 12:07:02'),
(11, 20, 0, '', '', '2020-11-09 12:07:06'),
(12, 59, 72, '', '', '2020-11-10 06:06:30'),
(13, 48, 72, '', '', '2020-11-10 06:40:15'),
(14, 31, 72, '', '', '2020-11-10 06:40:18'),
(15, 75, 0, '', '', '2020-11-10 07:03:08'),
(16, 135, 91, '', '', '2020-11-11 06:24:18'),
(17, 146, 0, '', '', '2020-11-11 10:57:23'),
(18, 133, 0, '', '', '2020-11-11 11:30:19'),
(20, 132, 20, 'Abusive', 'test', '2020-11-11 12:01:03'),
(21, 143, 81, 'Abusive/Offensive Content', 'hello.', '2020-11-11 12:50:38'),
(22, 137, 91, 'Adult/violent content', 'Busy h k knob', '2020-11-11 13:32:04'),
(27, 125, 11, 'Abusive/Offensive Content', 'mty', '2020-11-12 10:21:26');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT 0,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1 COMMENT '1=Pending, 2 = Replied',
  `is_deleted` tinyint(1) DEFAULT 2 COMMENT '1=Yes, 2 = No',
  `deleted_by` varchar(50) DEFAULT '0',
  `deleted_date` datetime DEFAULT '0000-00-00 00:00:00',
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `parent_id`, `name`, `email`, `subject`, `message`, `status`, `is_deleted`, `deleted_by`, `deleted_date`, `created`) VALUES
(1, 0, 'Dis Ghj', 'district@mailinator.com', 'Subject%201', 'Message%201', 1, 2, '0', '0000-00-00 00:00:00', '2020-11-04 04:44:49'),
(2, 0, 'Dis Ghj', 'district@mailinator.com', 'Message%202', 'New%20message%202%0ANew%201', 2, 2, '0', '0000-00-00 00:00:00', '2020-11-04 04:45:11'),
(3, 0, 'Sneha Sharma', 'sneha@mailinator.com', 'test', 'test message', 2, 2, '0', '0000-00-00 00:00:00', '2020-11-05 08:00:09'),
(4, 2, 'Admin', NULL, 'Message%202', '<p>\r\n	test</p>', 2, 2, '0', '0000-00-00 00:00:00', '2020-11-05 13:56:55'),
(5, 3, 'Admin', NULL, 'test', 'test', 2, 2, '0', '0000-00-00 00:00:00', '2020-11-05 14:07:33'),
(6, 0, 'Merry Tylor', 'merry@mailinator.com', 'test', 'test', 1, 2, '0', '0000-00-00 00:00:00', '2020-11-12 06:14:43'),
(7, 0, 'Merry Tylor', 'merry@mailinator.com', 'test', 'tes', 2, 2, '0', '0000-00-00 00:00:00', '2020-11-12 06:17:17'),
(8, 7, 'Admin', NULL, 'test', 'hell o, we look forward to you', 2, 2, '0', '0000-00-00 00:00:00', '2020-11-12 07:35:44'),
(9, 0, 'Student Dlast', 'Student@mailinator.com', 'ert', 'ret', 1, 2, '0', '0000-00-00 00:00:00', '2020-11-19 06:37:49'),
(10, 0, 'Student Dlast', 'Student@mailinator.com', 'uty', 'tyu', 1, 2, '0', '0000-00-00 00:00:00', '2020-11-20 13:41:57'),
(11, 0, 'Merry Tylor', 'merry@mailinator.com', 'test', 'test', 1, 2, '0', '0000-00-00 00:00:00', '2020-11-21 12:23:19'),
(12, 0, 'Merry Tylor', 'merry@mailinator.com', 'test', 'test', 1, 2, '0', '0000-00-00 00:00:00', '2020-11-21 12:23:34'),
(13, 0, 'Merry Tylor', 'merry@mailinator.com', 'test99', 'test99', 1, 2, '0', '0000-00-00 00:00:00', '2020-11-21 12:39:18'),
(14, 0, 'Merry Tylor', 'merry@mailinator.com', 'test', 'test', 1, 2, '0', '0000-00-00 00:00:00', '2020-11-21 13:28:42');

-- --------------------------------------------------------

--
-- Table structure for table `continue_watch_media`
--

CREATE TABLE `continue_watch_media` (
  `id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `duration` time NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `continue_watch_media`
--

INSERT INTO `continue_watch_media` (`id`, `media_id`, `user_id`, `duration`, `created`, `modified`) VALUES
(3, 18, 33, '00:00:00', '2020-11-06 13:19:10', '2020-11-06 13:19:10'),
(4, 9, 73, '00:01:00', '2020-11-09 06:02:36', '0000-00-00 00:00:00'),
(5, 30, 73, '00:01:00', '2020-11-09 06:03:50', '0000-00-00 00:00:00'),
(6, 25, 64, '00:01:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 30, 64, '00:01:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 6, 11, '00:00:03', '2020-11-10 07:39:08', '0000-00-00 00:00:00'),
(13, 6, 20, '00:00:05', '2020-11-12 05:20:08', '2020-11-12 05:20:08'),
(16, 63, 91, '00:00:05', '2020-11-11 11:24:40', '2020-11-11 11:24:40'),
(17, 69, 88, '00:00:13', '2020-11-11 11:29:53', '2020-11-11 11:29:53'),
(18, 70, 88, '00:01:05', '2020-11-11 12:05:21', '2020-11-11 12:05:21'),
(19, 32, 81, '00:00:12', '2020-11-11 11:59:20', '0000-00-00 00:00:00'),
(20, 71, 88, '00:01:06', '2020-11-11 12:09:17', '0000-00-00 00:00:00'),
(21, 71, 91, '00:00:00', '2020-11-17 13:35:32', '2020-11-17 13:35:32'),
(22, 60, 20, '00:00:19', '2020-11-18 07:00:30', '2020-11-18 07:00:30'),
(23, 4, 20, '00:00:02', '2020-11-12 05:20:25', '0000-00-00 00:00:00'),
(24, 59, 20, '00:00:05', '2020-11-12 05:27:38', '0000-00-00 00:00:00'),
(25, 70, 91, '00:00:02', '2020-11-12 06:22:19', '0000-00-00 00:00:00'),
(26, 95, 193, '00:00:00', '2020-11-21 13:17:06', '2020-11-21 13:17:06'),
(27, 96, 193, '00:00:00', '2020-11-21 13:19:40', '0000-00-00 00:00:00'),
(31, 18, 91, '00:40:08', '2020-11-21 14:19:42', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `csv_uploads`
--

CREATE TABLE `csv_uploads` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0=pending 1 = completed\r\n2=error',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `csv_uploads`
--

INSERT INTO `csv_uploads` (`id`, `user_id`, `district_id`, `state_id`, `file_name`, `status`, `created`) VALUES
(1, 14, 7, 2, 'resources/district_csv/1604480664_1604183352_Copy_of_Sample_File.csv', 1, '2020-11-04 09:04:25'),
(14, 1, 1, 2, 'resources/district_csv/1605091130_1605089315_1604994429_Sample_File_(5)_10_nov_(1)_(2).xlsx', 1, '2020-11-11 10:38:50'),
(15, 1, 1, 2, 'resources/district_csv/1605091518_1605089315_1604994429_Sample_File_(5)_10_nov_(1)_(2).xlsx', 1, '2020-11-11 10:45:18'),
(16, 1, 1, 2, 'resources/district_csv/1605103319_1605044744_Sample_File_1_11.10.20_(1).xlsx', 2, '2020-11-11 14:01:59'),
(17, 1, 1, 2, 'resources/district_csv/1605103781_1605044744_Sample_File_1_11.10.20.xlsx', 1, '2020-11-11 14:09:41'),
(18, 1, 1, 2, 'resources/district_csv/1605518355_1605514467_District_Sample_File_(7)_16_nov_(1).xlsx', 2, '2020-11-16 09:19:16'),
(19, 1, 1, 2, 'resources/district_csv/1605518426_1605514467_District_Sample_File_(7)_16_nov_(1).xlsx', 1, '2020-11-16 09:20:26'),
(20, 9, 5, 1, 'resources/district_csv/1605687551_Sample_File_old.xlsx', 2, '2020-11-18 08:19:11'),
(21, 1, 1, 2, 'resources/district_csv/1605697578_1605514467_District_Sample_File_(7)_16_nov_(1).xlsx', 1, '2020-11-18 11:06:18'),
(22, 1, 1, 2, 'resources/district_csv/1605698459_1605514467_District_Sample_File_(7)_16_nov_(1).xlsx', 1, '2020-11-18 11:20:59'),
(23, 1, 1, 2, 'resources/district_csv/1605699881_1605514467_District_Sample_File_(7)_16_nov_(1).xlsx', 1, '2020-11-18 11:44:41'),
(24, 1, 1, 2, 'resources/district_csv/1605708786_IPS_Template.xlsx', 1, '2020-11-18 14:13:06'),
(25, 1, 1, 2, 'resources/district_csv/1605767238_Lincoln_ISD_123.xlsx', 1, '2020-11-19 06:27:18'),
(26, 1, 1, 2, 'resources/district_csv/1605783180_Lincoln_ISD_123.xlsx', 1, '2020-11-19 10:53:00'),
(27, 1, 1, 2, 'resources/district_csv/1605783300_Lincoln_ISD_123.xlsx', 1, '2020-11-19 10:55:00'),
(28, 1, 1, 2, 'resources/district_csv/1605785236_Lincoln_ISD_123.xlsx', 1, '2020-11-19 11:27:16'),
(29, 1, 1, 2, 'resources/district_csv/1605785476_Lincoln_ISD_123.xlsx', 1, '2020-11-19 11:31:16'),
(30, 1, 1, 2, 'resources/district_csv/1605785568_Lincoln_ISD_123.xlsx', 1, '2020-11-19 11:32:48'),
(31, 1, 1, 2, 'resources/district_csv/1605785807_Lincoln_ISD_123.xlsx', 1, '2020-11-19 11:36:47'),
(32, 1, 1, 2, 'resources/district_csv/1605785873_Lincoln_ISD_123.xlsx', 1, '2020-11-19 11:37:53'),
(33, 1, 1, 2, 'resources/district_csv/1605785926_Lincoln_ISD_123.xlsx', 1, '2020-11-19 11:38:46'),
(34, 1, 1, 2, 'resources/district_csv/1605786110_1605776565_Sample_File_(12)_19_nov_(1).xlsx', 1, '2020-11-19 11:41:50'),
(35, 1, 1, 2, 'resources/district_csv/1605788776_1605518898_1605514467_District_Sample_File_(7)_16_nov_(1).xlsx', 1, '2020-11-19 12:26:16'),
(36, 1, 1, 2, 'resources/district_csv/1605789513_1605518898_1605514467_District_Sample_File_(7)_16_nov_(1).xlsx', 1, '2020-11-19 12:38:33'),
(37, 1, 1, 2, 'resources/district_csv/1605794277_1605518898_1605514467_District_Sample_File_(7)_16_nov_(1).xlsx', 1, '2020-11-19 13:57:57'),
(38, 191, 19, 14, 'resources/district_csv/1605960900_Sample_File.xlsx', 1, '2020-11-21 12:15:00'),
(39, 1, 1, 2, 'resources/district_csv/1605965903_locallllllllllllllllllLOCALLLSample_File_(16).xlsx', 1, '2020-11-21 13:38:23'),
(40, 1, 1, 2, 'resources/district_csv/1606034920_1606026616_Sample_File_23.xlsx', 1, '2020-11-22 08:48:40');

-- --------------------------------------------------------

--
-- Table structure for table `dislikes`
--

CREATE TABLE `dislikes` (
  `id` int(11) NOT NULL,
  `media_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_mac_address` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dislikes_deleted`
--

CREATE TABLE `dislikes_deleted` (
  `id` int(11) NOT NULL,
  `media_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_mac_address` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE `district` (
  `id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `ask_question` text NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`id`, `state_id`, `name`, `ask_question`, `created`) VALUES
(1, 2, 'Meri Island', 'tes', '2020-11-03 13:30:44'),
(2, 1, 'Bharatt DIST', 'Dfgdfgdf', '2020-11-03 13:45:16'),
(3, 1, 'Bharatt DIST12', 'Dfgdfgdf', '2020-11-03 13:45:41'),
(4, 3, 'Arizona city', 'No question', '2020-11-03 13:50:10'),
(5, 1, 'District', 'Tftvtvgt', '2020-11-04 04:21:47'),
(6, 2, 'Devil city', 'No question mine', '2020-11-04 08:22:38'),
(7, 2, 'Temple city', '', '2020-11-04 08:24:50'),
(8, 5, 'Crown city', 'What is Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has?', '2020-11-04 10:48:50'),
(9, 4, 'Arkan city', 'test', '2020-11-04 12:47:01'),
(10, 1, 'District2', 'Bxbxbd', '2020-11-04 13:41:06'),
(11, 1, 'Gshz', 'Xbbxbx', '2020-11-04 13:41:37'),
(12, 2, '', '', '2020-11-05 07:21:22'),
(13, 1, 'bharat district 123', 'Test', '2020-11-05 14:36:29'),
(14, 2, 'NEw district', 'test', '2020-11-08 13:35:02'),
(15, 9, 'Bharat Dis', 'Test desc', '2020-11-09 05:23:40'),
(16, 2, 'Bharat district', 'Vigicicic', '2020-11-10 09:30:02'),
(17, 2, 'crystal Lake', 'no question', '2020-11-16 07:46:36'),
(18, 3, 'dww', 'dd', '2020-11-19 06:51:14'),
(19, 14, 'Bharat dis928', 'Bznsnsbs', '2020-11-21 12:13:24');

-- --------------------------------------------------------

--
-- Table structure for table `email_notification`
--

CREATE TABLE `email_notification` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `recipient_name` varchar(70) NOT NULL,
  `type` varchar(100) NOT NULL,
  `user_type` tinyint(1) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `sender_email` varchar(100) DEFAULT NULL,
  `sender_name` varchar(70) DEFAULT NULL COMMENT 'sender name used only for when we need to send sender name insetead of chat at me school team',
  `subject` varchar(150) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0 = pending,  1= completed',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_notification`
--

INSERT INTO `email_notification` (`id`, `user_id`, `email`, `recipient_name`, `type`, `user_type`, `sender_id`, `sender_email`, `sender_name`, `subject`, `message`, `status`, `created`, `modified`) VALUES
(1, 7, 'principal1@yopmail.com', 'bharat rawal', 'Distrct_approved_school', 2, 3, NULL, NULL, NULL, NULL, 1, '2020-11-03 13:59:43', '0000-00-00 00:00:00'),
(2, 8, 'teacher@yopmail.com', 'bharat rawal', 'School_approved_teacher', 3, 7, NULL, NULL, NULL, NULL, 1, '2020-11-03 14:03:07', '0000-00-00 00:00:00'),
(3, 8, 'teacher@yopmail.com', '', 'Broadcast_notification', 2, 7, NULL, NULL, 'test', 'Hellooooo', 1, '2020-11-03 14:03:32', '0000-00-00 00:00:00'),
(4, 10, 'School@mailinator.com', 'SchoolQ SLast', 'Distrct_approved_school', 2, 9, NULL, NULL, NULL, NULL, 1, '2020-11-04 05:01:55', '0000-00-00 00:00:00'),
(5, 10, 'School@mailinator.com', 'SchoolQ SLast', 'Broadcast_notification', 1, 9, NULL, NULL, 'New messahe', 'Testing message 1', 1, '2020-11-04 05:38:38', '0000-00-00 00:00:00'),
(6, 10, 'School@mailinator.com', 'SchoolQ SLast', 'Broadcast_notification', 1, 9, NULL, NULL, 'Tedting2', 'Testing2 Message2.2     New one', 1, '2020-11-04 05:47:13', '0000-00-00 00:00:00'),
(7, 11, 'teacher@mailinator.com', 'Teacher NewTech', 'School_approved_teacher', 3, 10, NULL, NULL, NULL, NULL, 1, '2020-11-04 06:06:27', '0000-00-00 00:00:00'),
(8, 12, 'teacherT@yopmail.com', 'teacher T', 'School_approved_teacher', 3, 7, NULL, NULL, NULL, NULL, 1, '2020-11-04 08:45:05', '0000-00-00 00:00:00'),
(9, 15, 'Teacher12@yopmail.com', 'Teacher T', 'School_approved_teacher', 3, 7, NULL, NULL, NULL, NULL, 1, '2020-11-04 08:56:58', '0000-00-00 00:00:00'),
(10, 20, '', '', 'Teacher_approved_student', 4, 11, NULL, NULL, NULL, 'Student \'s request approved by Teacher for First Grade', 1, '2020-11-04 12:06:02', '0000-00-00 00:00:00'),
(11, 20, '', '', 'Teacher_approved_student', 4, 11, NULL, NULL, NULL, 'Ss \'s request approved by Teacher for Kindergarten', 1, '2020-11-04 12:07:18', '0000-00-00 00:00:00'),
(12, 25, 'mydummyschool@mailinator.com', 'dummy school', 'Distrct_approved_school', 2, 1, NULL, NULL, NULL, NULL, 1, '2020-11-05 07:43:39', '0000-00-00 00:00:00'),
(13, 26, 'mydummyschool@mailinator.com', 'dummy school', 'Distrct_approved_school', 2, 1, NULL, NULL, NULL, NULL, 1, '2020-11-05 07:50:53', '0000-00-00 00:00:00'),
(14, 27, 'dummyschool123@mailinator.com', 'dummy school 123', 'Distrct_approved_school', 2, 1, NULL, NULL, NULL, NULL, 1, '2020-11-05 08:05:27', '0000-00-00 00:00:00'),
(15, 31, 'bharatpri@yopmail.com', 'bharat principal', 'Distrct_approved_school', 2, 30, NULL, NULL, NULL, NULL, 1, '2020-11-05 14:42:57', '0000-00-00 00:00:00'),
(16, 32, 'bharattea@yopmail.com', 'bharat teacher', 'School_approved_teacher', 3, 31, NULL, NULL, NULL, NULL, 1, '2020-11-05 14:46:34', '0000-00-00 00:00:00'),
(17, 32, 'bharattea@yopmail.com', '', 'Broadcast_notification', 2, 31, NULL, NULL, 'cucù fu', 'Cucù cucù cucù', 1, '2020-11-05 14:57:46', '0000-00-00 00:00:00'),
(18, 34, 'bharattea2@yopmail.com', 'bharat teacher2', 'School_approved_teacher', 3, 31, NULL, NULL, NULL, NULL, 1, '2020-11-06 05:00:25', '0000-00-00 00:00:00'),
(19, 29, 'dyna@mailinator.com', 'Dyna Lee', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-06 05:18:15', '0000-00-00 00:00:00'),
(20, 29, 'dyna@mailinator.com', 'Dyna Lee', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-06 05:22:29', '0000-00-00 00:00:00'),
(21, 29, 'dyna@mailinator.com', 'Dyna Lee', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-06 05:24:37', '0000-00-00 00:00:00'),
(22, 35, 'test@mailinator.com', 'test tttt', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-06 05:25:23', '0000-00-00 00:00:00'),
(23, 29, 'dyna@mailinator.com', 'Dyna Lee', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-06 05:25:23', '0000-00-00 00:00:00'),
(24, 35, 'test@mailinator.com', 'test tttt', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-06 05:30:03', '0000-00-00 00:00:00'),
(25, 29, 'dyna@mailinator.com', 'Dyna Lee', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-06 05:30:03', '0000-00-00 00:00:00'),
(26, 29, 'dyna@mailinator.com', 'Dyna Lee', 'Broadcast_notification', 2, 27, NULL, NULL, 'hello', 'this is a test message', 1, '2020-11-06 06:38:36', '0000-00-00 00:00:00'),
(27, 35, 'test@mailinator.com', 'test tttt', 'Broadcast_notification', 2, 27, NULL, NULL, 'hello', 'this is a test message', 1, '2020-11-06 06:38:36', '0000-00-00 00:00:00'),
(28, 29, 'dyna@mailinator.com', 'Dyna Lee', 'Broadcast_notification', 2, 27, NULL, NULL, 'hello', 'fgdsxfg sdsfdsf sfdsf', 1, '2020-11-06 06:44:23', '0000-00-00 00:00:00'),
(29, 35, 'test@mailinator.com', 'test tttt', 'Broadcast_notification', 2, 27, NULL, NULL, 'hello', 'fgdsxfg sdsfdsf sfdsf', 1, '2020-11-06 06:44:23', '0000-00-00 00:00:00'),
(30, 29, 'dyna@mailinator.com', 'Dyna Lee', 'Broadcast_notification', 2, 27, NULL, NULL, 'tjtyu', 'utyuyt', 1, '2020-11-06 06:44:57', '0000-00-00 00:00:00'),
(31, 35, 'test@mailinator.com', 'test tttt', 'Broadcast_notification', 2, 27, NULL, NULL, 'tjtyu', 'utyuyt', 1, '2020-11-06 06:44:57', '0000-00-00 00:00:00'),
(32, 29, 'dyna@mailinator.com', 'Dyna Lee', 'Broadcast_notification', 2, 27, NULL, NULL, 'hello', 'y ouigb e wqeq ewqe', 1, '2020-11-06 06:51:50', '0000-00-00 00:00:00'),
(33, 35, 'test@mailinator.com', 'test tttt', 'Broadcast_notification', 2, 27, NULL, NULL, 'hello', 'y ouigb e wqeq ewqe', 1, '2020-11-06 06:51:50', '0000-00-00 00:00:00'),
(34, 29, 'dyna@mailinator.com', 'Dyna Lee', 'Broadcast_notification', 2, 27, NULL, NULL, 'hello', 'this is atest mesga', 1, '2020-11-06 06:53:49', '0000-00-00 00:00:00'),
(35, 35, 'test@mailinator.com', 'test tttt', 'Broadcast_notification', 2, 27, NULL, NULL, 'hello', 'this is atest mesga', 1, '2020-11-06 06:53:49', '0000-00-00 00:00:00'),
(36, 29, 'dyna@mailinator.com', 'Dyna Lee', 'Broadcast_notification', 2, 27, NULL, NULL, 'HJGHJH', 'dsad sadasw', 1, '2020-11-06 06:56:10', '0000-00-00 00:00:00'),
(37, 35, 'test@mailinator.com', 'test tttt', 'Broadcast_notification', 2, 27, NULL, NULL, 'HJGHJH', 'dsad sadasw', 1, '2020-11-06 06:56:10', '0000-00-00 00:00:00'),
(38, 29, 'dyna@mailinator.com', 'Dyna Lee', 'Broadcast_notification', 2, 27, NULL, NULL, 'hjghj', 'jhj hj', 1, '2020-11-06 06:56:46', '0000-00-00 00:00:00'),
(39, 35, 'test@mailinator.com', 'test tttt', 'Broadcast_notification', 2, 27, NULL, NULL, 'hjghj', 'jhj hj', 1, '2020-11-06 06:56:46', '0000-00-00 00:00:00'),
(40, 29, 'dyna@mailinator.com', 'Dyna Lee', 'Broadcast_notification', 2, 27, NULL, NULL, 'HJGHJH', 'fghjfghj', 1, '2020-11-06 06:57:55', '0000-00-00 00:00:00'),
(41, 35, 'test@mailinator.com', 'test tttt', 'Broadcast_notification', 2, 27, NULL, NULL, 'HJGHJH', 'fghjfghj', 1, '2020-11-06 06:57:55', '0000-00-00 00:00:00'),
(42, 36, 'rrr@mailinator.com', 'eee rrrr', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-06 09:07:26', '0000-00-00 00:00:00'),
(43, 46, 'robert@mailinator.com', 'Robert D\'souza', 'Distrct_approved_school', 2, 1, NULL, NULL, NULL, NULL, 1, '2020-11-06 09:19:06', '0000-00-00 00:00:00'),
(44, 47, 'teacher1@mailinator.com', 'Teacher II', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-06 10:24:42', '0000-00-00 00:00:00'),
(45, 49, 'test2@mailinator.com', 'test tttt', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-06 11:51:42', '0000-00-00 00:00:00'),
(46, 50, 'test3@mailinator.com', 'test tttt', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-06 11:51:42', '0000-00-00 00:00:00'),
(47, 51, 'test4@mailinator.com', 'test tttt', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-06 11:51:42', '0000-00-00 00:00:00'),
(48, 52, 'test5@mailinator.com', 'test tttt', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-06 11:51:42', '0000-00-00 00:00:00'),
(49, 53, 'test6@mailinator.com', 'test tttt', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-06 11:51:42', '0000-00-00 00:00:00'),
(50, 54, 'test7@mailinator.com', 'test tttt', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-06 11:51:42', '0000-00-00 00:00:00'),
(51, 55, 'test8@mailinator.com', 'test tttt', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-06 11:51:42', '0000-00-00 00:00:00'),
(52, 29, 'dyna@mailinator.com', 'Dyna Lee', 'Broadcast_notification', 2, 27, NULL, NULL, 'jghjfghj hh', 'hgdft trytr', 1, '2020-11-06 13:16:36', '0000-00-00 00:00:00'),
(53, 35, 'test@mailinator.com', 'test tttt', 'Broadcast_notification', 2, 27, NULL, NULL, 'jghjfghj hh', 'hgdft trytr', 1, '2020-11-06 13:16:36', '0000-00-00 00:00:00'),
(54, 48, 'test1@mailinator.com', 'test tttt', 'Broadcast_notification', 2, 27, NULL, NULL, 'jghjfghj hh', 'hgdft trytr', 1, '2020-11-06 13:16:36', '0000-00-00 00:00:00'),
(55, 49, 'test2@mailinator.com', 'test tttt', 'Broadcast_notification', 2, 27, NULL, NULL, 'jghjfghj hh', 'hgdft trytr', 1, '2020-11-06 13:16:36', '0000-00-00 00:00:00'),
(56, 50, 'test3@mailinator.com', 'test tttt', 'Broadcast_notification', 2, 27, NULL, NULL, 'jghjfghj hh', 'hgdft trytr', 1, '2020-11-06 13:16:36', '0000-00-00 00:00:00'),
(57, 51, 'test4@mailinator.com', 'test tttt', 'Broadcast_notification', 2, 27, NULL, NULL, 'jghjfghj hh', 'hgdft trytr', 1, '2020-11-06 13:16:36', '0000-00-00 00:00:00'),
(58, 55, 'test8@mailinator.com', 'test tttt', 'Broadcast_notification', 2, 27, NULL, NULL, 'jghjfghj hh', 'hgdft trytr', 1, '2020-11-06 13:16:36', '0000-00-00 00:00:00'),
(59, 57, 'School2@mailinator.com', 'PrincipalNew LadtNee', 'Distrct_approved_school', 2, 30, NULL, NULL, NULL, NULL, 1, '2020-11-07 06:04:53', '0000-00-00 00:00:00'),
(60, 56, 'checkteacher@mailinator.com', 'check teacher', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-07 06:08:54', '0000-00-00 00:00:00'),
(61, 61, 'checkteacher4@mailinator.com', 'check2 teacher', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-07 06:23:58', '0000-00-00 00:00:00'),
(62, 62, 'checkteacher5@mailinator.com', 'check1 teacher', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-07 06:24:24', '0000-00-00 00:00:00'),
(63, 58, 'checkteacher1@mailinator.com', 'check5 teacher', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-07 06:25:18', '0000-00-00 00:00:00'),
(64, 60, 'checkteacher3@mailinator.com', 'check3 teacher', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-07 06:25:18', '0000-00-00 00:00:00'),
(65, 64, '', '', 'Teacher_approved_student', 4, 62, NULL, NULL, NULL, 'Deny Parkar3 \'s request approved by sdghj kddddd1234 for First Grade', 1, '2020-11-07 10:44:12', '0000-00-00 00:00:00'),
(66, 64, '', '', 'Teacher_approved_student', 4, 62, NULL, NULL, NULL, 'Deny Parkar3 \'s request approved by sdghj kddddd1234 for First Grade', 1, '2020-11-07 10:44:44', '0000-00-00 00:00:00'),
(67, 64, '', '', 'Teacher_approved_student', 4, 62, NULL, NULL, NULL, 'Deny Parkar3 \'s request approved by sdghj kddddd1234 for Second Grade', 1, '2020-11-07 10:44:44', '0000-00-00 00:00:00'),
(68, 64, '', '', 'Teacher_approved_student', 4, 62, NULL, NULL, NULL, 'Deny Parkar3 \'s request approved by sdghj kddddd1234 for Second Grade', 1, '2020-11-07 10:59:19', '0000-00-00 00:00:00'),
(69, 64, '', '', 'Teacher_approved_student', 4, 62, NULL, NULL, NULL, 'Deny Parkar3 \'s request approved by sdghj kddddd1234 for First Grade', 1, '2020-11-07 10:59:47', '0000-00-00 00:00:00'),
(70, 64, '', '', 'Teacher_approved_student', 4, 62, NULL, NULL, NULL, 'Deny Parkar3 \'s request approved by sdghj kddddd1234 for Second Grade', 1, '2020-11-07 10:59:48', '0000-00-00 00:00:00'),
(71, 59, 'checkteacher2@mailinator.com', 'check4 teacher', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 1, '2020-11-07 12:03:21', '0000-00-00 00:00:00'),
(72, 64, '', '', 'Teacher_approved_student', 4, 62, NULL, NULL, NULL, 'Dummy user \'s request approved by sdghj kddddd1234 for Fourth Grade', 1, '2020-11-08 07:20:21', '0000-00-00 00:00:00'),
(73, 64, '', '', 'Teacher_approved_student', 4, 62, NULL, NULL, NULL, 'Deny Parkar3 \'s request approved by sdghj kddddd1234 for First Grade', 1, '2020-11-08 07:20:21', '0000-00-00 00:00:00'),
(74, 64, '', '', 'Teacher_approved_student', 4, 62, NULL, NULL, NULL, 'Deny Parkar3 \'s request approved by sdghj kddddd1234 for First Grade', 1, '2020-11-08 07:20:21', '0000-00-00 00:00:00'),
(75, 64, '', '', 'Teacher_approved_student', 4, 62, NULL, NULL, NULL, 'Deny Parkar3 \'s request approved by sdghj kddddd1234 for First Grade', 1, '2020-11-08 07:20:21', '0000-00-00 00:00:00'),
(76, 64, '', '', 'Teacher_approved_student', 4, 62, NULL, NULL, NULL, 'Deny Parkar3 \'s request approved by sdghj kddddd1234 for Second Grade', 1, '2020-11-08 07:20:21', '0000-00-00 00:00:00'),
(77, 40, 'kingjunior@mailinator.com', 'King junior Luthor', 'Distrct_approved_school', 2, 1, NULL, NULL, NULL, NULL, 1, '2020-11-08 11:55:27', '0000-00-00 00:00:00'),
(78, 64, '', '', 'Teacher_approved_student', 4, 62, NULL, NULL, NULL, 'Dummy user \'s request approved by sdghj kddddd1234 for Fourth Grade', 1, '2020-11-08 12:18:58', '0000-00-00 00:00:00'),
(79, 64, '', '', 'Teacher_approved_student', 4, 62, NULL, NULL, NULL, 'vishal \'s request approved by sdghj kddddd1234 for Fourth Grade', 1, '2020-11-08 12:18:58', '0000-00-00 00:00:00'),
(80, 64, '', '', 'Teacher_approved_student', 4, 62, NULL, NULL, NULL, 'vijendra \'s request approved by sdghj kddddd1234 for Fourth Grade', 1, '2020-11-08 12:18:58', '0000-00-00 00:00:00'),
(81, 64, '', '', 'Teacher_approved_student', 4, 62, NULL, NULL, NULL, 'vijendra \'s request approved by sdghj kddddd1234 for Second Grade', 1, '2020-11-08 12:18:58', '0000-00-00 00:00:00'),
(82, 64, '', '', 'Teacher_approved_student', 4, 62, NULL, NULL, NULL, 'Dummy user \'s request approved by sdghj kddddd1234 for Fourth Grade', 1, '2020-11-08 12:18:58', '0000-00-00 00:00:00'),
(83, 71, 'bpri@yopmail.com', 'Bharat Teacher', 'Distrct_approved_school', 2, 69, NULL, NULL, NULL, NULL, 1, '2020-11-09 05:28:49', '0000-00-00 00:00:00'),
(84, 72, 'btea1@yopmail.com', 'Bharat TeacherT', 'School_approved_teacher', 3, 71, NULL, NULL, NULL, NULL, 1, '2020-11-09 05:35:05', '0000-00-00 00:00:00'),
(85, 73, '', '', 'Teacher_approved_student', 4, 72, NULL, NULL, NULL, 'Bhart \'s request approved by Rawal Display Name for Tenth Grade', 1, '2020-11-09 06:03:08', '0000-00-00 00:00:00'),
(86, 73, '', '', 'Teacher_approved_student', 4, 72, NULL, NULL, NULL, 'Bhart \'s request approved by Rawal Display Name for First Grade', 1, '2020-11-09 06:03:08', '0000-00-00 00:00:00'),
(87, 20, '', '', 'Teacher_approved_student', 4, 11, NULL, NULL, NULL, 'New student \'s request approved by Teacher for Second Grade', 1, '2020-11-09 11:58:52', '0000-00-00 00:00:00'),
(88, 20, '', '', 'Teacher_approved_student', 4, 11, NULL, NULL, NULL, 'New student \'s request approved by Teacher for First Grade', 1, '2020-11-09 11:58:52', '0000-00-00 00:00:00'),
(89, 74, '', '', 'Teacher_approved_student', 4, 11, NULL, NULL, NULL, 'Asd \'s request approved by Teacher for First Grade', 1, '2020-11-09 12:07:09', '0000-00-00 00:00:00'),
(90, 74, '', '', 'Teacher_approved_student', 4, 11, NULL, NULL, NULL, 'Asd \'s request approved by Teacher for Kindergarten', 1, '2020-11-09 12:07:09', '0000-00-00 00:00:00'),
(91, 76, 'bpri1@yopmail.com', 'Bharat New', 'Distrct_approved_school', 2, 69, NULL, NULL, NULL, NULL, 1, '2020-11-09 12:51:07', '0000-00-00 00:00:00'),
(92, 78, 'juniour@mailinator.com', 'junior king', 'Distrct_approved_school', 2, 1, NULL, NULL, NULL, NULL, 1, '2020-11-09 13:38:01', '0000-00-00 00:00:00'),
(93, 78, 'juniour@mailinator.com', 'Juniour', 'Broadcast_notification', 3, 5, NULL, NULL, 'Test teacher', 'test message teacher', 1, '2020-11-09 13:39:37', '0000-00-00 00:00:00'),
(94, 79, 'teacher30@mailinator.com', 'TeacherII teacher III', 'School_approved_teacher', 3, 78, NULL, NULL, NULL, NULL, 1, '2020-11-09 13:45:29', '0000-00-00 00:00:00'),
(95, 79, 'teacher30@mailinator.com', 'teacher', 'Broadcast_notification', 3, 5, NULL, NULL, 'Test teacher', 'test message teacher', 1, '2020-11-09 13:47:55', '0000-00-00 00:00:00'),
(96, 79, 'teacher30@mailinator.com', 'Teeeeeeeeeee', 'Broadcast_notification', 2, 78, NULL, NULL, 'New subject', 'New description', 1, '2020-11-09 13:54:48', '0000-00-00 00:00:00'),
(97, 64, '', '', 'Teacher_approved_student', 4, 62, NULL, NULL, NULL, 'demo demo \'s request approved by sdghj kddddd1234 for Fourth Grade', 1, '2020-11-10 05:14:19', '0000-00-00 00:00:00'),
(98, 64, '', '', 'Teacher_approved_student', 4, 62, NULL, NULL, NULL, 'accele \'s request approved by sdghj kddddd1234 for Fourth Grade', 1, '2020-11-10 05:14:19', '0000-00-00 00:00:00'),
(99, 64, '', '', 'Teacher_approved_student', 4, 62, NULL, NULL, NULL, 'accele \'s request approved by sdghj kddddd1234 for Fourth Grade', 1, '2020-11-10 05:14:19', '0000-00-00 00:00:00'),
(100, 64, '', '', 'Teacher_approved_student', 4, 62, NULL, NULL, NULL, 'accele \'s request approved by sdghj kddddd1234 for Fourth Grade', 1, '2020-11-10 05:14:19', '0000-00-00 00:00:00'),
(101, 64, '', '', 'Teacher_approved_student', 4, 62, NULL, NULL, NULL, 'accele \'s request approved by sdghj kddddd1234 for Fourth Grade', 1, '2020-11-10 05:14:19', '0000-00-00 00:00:00'),
(102, 47, 'teacher1@mailinator.com', 'Teacher II', 'Broadcast_notification', 2, 27, NULL, NULL, 'dsf', 'dsv', 1, '2020-11-10 05:30:54', '0000-00-00 00:00:00'),
(103, 20, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '42', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:30:11', '0000-00-00 00:00:00'),
(104, 74, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '42', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:30:11', '0000-00-00 00:00:00'),
(105, 20, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '43', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:31:04', '0000-00-00 00:00:00'),
(106, 74, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '43', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:31:04', '0000-00-00 00:00:00'),
(107, 20, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '44', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:40:51', '0000-00-00 00:00:00'),
(108, 74, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '44', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:40:51', '0000-00-00 00:00:00'),
(109, 20, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '45', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:44:35', '0000-00-00 00:00:00'),
(110, 74, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '45', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:44:35', '0000-00-00 00:00:00'),
(111, 73, '', '', 'Teacher_approved_student', 4, 72, NULL, NULL, NULL, 'Dsfsdfsdf dsfsdfsdf dsfsdf \'s request approved by Rawal Display Name for First Grade', 1, '2020-11-10 07:51:00', '0000-00-00 00:00:00'),
(112, 20, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '48', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:52:00', '0000-00-00 00:00:00'),
(113, 74, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '48', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:52:00', '0000-00-00 00:00:00'),
(114, 20, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '49', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:52:05', '0000-00-00 00:00:00'),
(115, 74, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '49', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:52:05', '0000-00-00 00:00:00'),
(116, 20, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '50', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:52:06', '0000-00-00 00:00:00'),
(117, 74, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '50', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:52:06', '0000-00-00 00:00:00'),
(118, 20, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '51', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:52:07', '0000-00-00 00:00:00'),
(119, 74, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '51', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:52:07', '0000-00-00 00:00:00'),
(120, 20, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '52', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:53:56', '0000-00-00 00:00:00'),
(121, 74, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '52', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:53:56', '0000-00-00 00:00:00'),
(122, 20, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '53', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:53:58', '0000-00-00 00:00:00'),
(123, 74, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '53', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:53:58', '0000-00-00 00:00:00'),
(124, 20, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '54', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:53:58', '0000-00-00 00:00:00'),
(125, 74, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '54', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:53:58', '0000-00-00 00:00:00'),
(126, 20, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '55', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:53:58', '0000-00-00 00:00:00'),
(127, 74, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '55', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:53:58', '0000-00-00 00:00:00'),
(128, 20, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '56', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:53:59', '0000-00-00 00:00:00'),
(129, 74, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '56', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 07:53:59', '0000-00-00 00:00:00'),
(130, 20, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '59', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 09:06:02', '0000-00-00 00:00:00'),
(131, 74, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '59', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 09:06:02', '0000-00-00 00:00:00'),
(132, 81, '', '', 'Teacher_approved_student', 4, 72, NULL, NULL, NULL, 'Hello \'s request approved by Rawal Display Name for First Grade', 1, '2020-11-10 09:12:32', '0000-00-00 00:00:00'),
(133, 20, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '60', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 09:17:07', '0000-00-00 00:00:00'),
(134, 74, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '60', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-10 09:17:07', '0000-00-00 00:00:00'),
(135, 83, 'bpri123@yopmail.com', 'Bharat PRI', 'Distrct_approved_school', 2, 82, NULL, NULL, NULL, NULL, 1, '2020-11-10 10:03:00', '0000-00-00 00:00:00'),
(136, 88, 'btea123@yopmail.com', 'Bharat Teach teach', 'School_approved_teacher', 3, 83, NULL, NULL, NULL, NULL, 1, '2020-11-10 10:15:39', '0000-00-00 00:00:00'),
(137, 91, '', '', 'Teacher_approved_student', 4, 88, NULL, NULL, NULL, 'Hiii \'s request approved by Hiii for Third Grade', 1, '2020-11-10 10:26:46', '0000-00-00 00:00:00'),
(138, 91, '', '', 'Teacher_approved_student', 4, 88, NULL, NULL, NULL, 'Hiii \'s request approved by Hiii for First Grade', 1, '2020-11-10 10:26:46', '0000-00-00 00:00:00'),
(139, 88, 'btea123@yopmail.com', '', 'Broadcast_notification', 2, 83, NULL, NULL, 'Hiiii', 'Hello there hope your day was fun to see it all', 1, '2020-11-10 10:27:12', '0000-00-00 00:00:00'),
(140, 88, 'btea123@yopmail.com', '', 'Broadcast_notification', 2, 83, NULL, NULL, 'Gigoucoyxitd', 'Dkydkydoydoyf hlotdoydyo holdup', 1, '2020-11-10 10:29:48', '0000-00-00 00:00:00'),
(141, 91, '', '', 'Teacher_upload_video', 4, 88, NULL, NULL, '62', 'Hiii uploaded new video for First Grade', 1, '2020-11-10 10:37:04', '0000-00-00 00:00:00'),
(142, 91, '', '', 'Teacher_upload_video', 4, 88, NULL, NULL, '63', 'Hiii uploaded new video for First Grade', 1, '2020-11-10 10:44:08', '0000-00-00 00:00:00'),
(143, 20, '', '', 'Teacher_approved_student', 4, 11, NULL, NULL, NULL, 'krish \'s request approved by Teacher for First Grade', 1, '2020-11-10 11:01:06', '0000-00-00 00:00:00'),
(144, 91, '', '', 'Teacher_upload_video', 4, 88, NULL, NULL, '68', 'Hiii uploaded new video for First Grade', 1, '2020-11-11 10:02:32', '0000-00-00 00:00:00'),
(145, 91, '', '', 'Teacher_upload_video', 4, 88, NULL, NULL, '69', 'Hiii uploaded new video for First Grade', 1, '2020-11-11 11:28:49', '0000-00-00 00:00:00'),
(146, 91, '', '', 'Teacher_upload_video', 4, 88, NULL, NULL, '70', 'Hiii uploaded new video for First Grade', 1, '2020-11-11 11:32:05', '0000-00-00 00:00:00'),
(147, 91, '', '', 'Teacher_upload_video', 4, 88, NULL, NULL, '71', 'Hiii uploaded new video for First Grade', 1, '2020-11-11 12:08:45', '0000-00-00 00:00:00'),
(148, 64, '', '', 'Teacher_approved_student', 4, 61, NULL, NULL, NULL, 'demo demo \'s request approved by vghfgh for Second Grade', 1, '2020-11-12 07:43:10', '0000-00-00 00:00:00'),
(149, 64, '', '', 'Teacher_upload_video', 4, 61, NULL, NULL, '73', 'Vghfgh uploaded new video for Second Grade', 1, '2020-11-12 07:43:33', '0000-00-00 00:00:00'),
(150, 64, '', '', 'Teacher_upload_video', 4, 61, NULL, NULL, '74', 'Vghfgh uploaded new video for Second Grade', 1, '2020-11-12 07:43:38', '0000-00-00 00:00:00'),
(151, 64, '', '', 'Teacher_upload_video', 4, 61, NULL, NULL, '75', 'Vghfgh uploaded new video for Second Grade', 1, '2020-11-12 07:47:35', '0000-00-00 00:00:00'),
(152, 91, '', '', 'Teacher_upload_video', 4, 88, NULL, NULL, '77', 'Hiii uploaded new video for Third Grade', 1, '2020-11-12 08:00:17', '0000-00-00 00:00:00'),
(153, 91, '', '', 'Teacher_upload_video', 4, 88, NULL, NULL, '78', 'Hiii uploaded new video for First Grade', 1, '2020-11-12 08:02:20', '0000-00-00 00:00:00'),
(154, 91, '', '', 'Teacher_upload_video', 4, 88, NULL, NULL, '79', 'Hiii uploaded new video for First Grade', 1, '2020-11-12 08:16:00', '0000-00-00 00:00:00'),
(155, 91, '', '', 'Teacher_upload_video', 4, 88, NULL, NULL, '80', 'Hiii uploaded new video for First Grade', 1, '2020-11-12 08:20:48', '0000-00-00 00:00:00'),
(156, 91, '', '', 'Teacher_upload_video', 4, 88, NULL, NULL, '81', 'Hiii uploaded new video for First Grade', 1, '2020-11-12 10:17:05', '0000-00-00 00:00:00'),
(157, 91, '', '', 'Teacher_upload_video', 4, 88, NULL, NULL, '82', 'Hiii uploaded new video for First Grade', 1, '2020-11-12 10:28:34', '0000-00-00 00:00:00'),
(158, 142, 'widow@mailinator.com', 'Black Widow', 'Broadcast_notification', 1, 1, NULL, NULL, 'test', 'test', 1, '2020-11-16 08:07:56', '0000-00-00 00:00:00'),
(159, 150, 'jaxi@mailinator.com', ' ', 'Broadcast_notification', 1, 1, NULL, NULL, 'test', 'ds', 1, '2020-11-16 09:37:18', '0000-00-00 00:00:00'),
(160, 148, 'tereso@mailinator.com', ' ', 'Broadcast_notification', 1, 1, NULL, NULL, 're', '55', 1, '2020-11-16 09:38:36', '0000-00-00 00:00:00'),
(161, 27, 'dummyschool123@mailinator.com', 'dummy school 123d', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'my test cont', 'my test conts', 1, '2020-11-16 10:26:16', '0000-00-00 00:00:00'),
(162, 79, 'teacher30@mailinator.com', 'TeacherII teacher III', 'Broadcast_notification', 2, 78, 'juniour@mailinator.com', 'Juniour test', 'dd', 'dd', 1, '2020-11-16 10:45:43', '0000-00-00 00:00:00'),
(163, 124, 'trisha@mailinator.com', 'Trisha shah', 'Broadcast_notification', 2, 27, 'dummyschool123@mailinator.com', 'dummy school 123d', 'test', 'test', 1, '2020-11-17 05:57:26', '0000-00-00 00:00:00'),
(164, 124, 'trisha@mailinator.com', 'Trisha shah', 'Broadcast_notification', 2, 27, 'dummyschool123@mailinator.com', 'dummy school 123d', 'teerrwrewrw', 'eewrwer', 1, '2020-11-17 06:03:13', '0000-00-00 00:00:00'),
(165, 124, 'trisha@mailinator.com', 'Trisha shah', 'Broadcast_notification', 2, 27, 'ngawade@hitaishin.com', 'dummy school 123d', 'wewew', 'ewewe', 1, '2020-11-17 06:09:29', '0000-00-00 00:00:00'),
(166, 124, 'trisha@mailinator.com', 'Trisha shah', 'Broadcast_notification', 2, 27, 'ngawade@hitaishin.com', 'dummy school 123d', 'ddd', 'ddd', 1, '2020-11-17 06:13:26', '0000-00-00 00:00:00'),
(167, 146, 'trio@mailinator.com', ' ', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'adf', 'asfasfsafsd', 1, '2020-11-17 06:28:01', '0000-00-00 00:00:00'),
(168, 146, 'trio@mailinator.com', ' ', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'tasdf', 'afsdff', 1, '2020-11-17 06:30:49', '0000-00-00 00:00:00'),
(169, 146, 'trio@mailinator.com', ' ', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'asdf', 'asffd', 1, '2020-11-17 06:31:15', '0000-00-00 00:00:00'),
(170, 142, 'widow@mailinator.com', 'Black Widow', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'sss', 'ssss', 1, '2020-11-17 09:39:08', '0000-00-00 00:00:00'),
(171, 142, 'widow@mailinator.com', 'Black Widow', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'ddd', 'sdsdsdsd', 1, '2020-11-17 09:41:41', '0000-00-00 00:00:00'),
(172, 27, 'ngawade@hitaishin.com', 'dummy school 123d', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'ddd', 'dssdsd', 1, '2020-11-17 09:42:22', '0000-00-00 00:00:00'),
(173, 27, 'ngawade@hitaishin.com', 'dummy school 123d', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'ss', 'ss', 1, '2020-11-17 09:42:54', '0000-00-00 00:00:00'),
(174, 20, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '83', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-17 12:06:36', '0000-00-00 00:00:00'),
(175, 74, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '83', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-17 12:06:36', '0000-00-00 00:00:00'),
(176, 20, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '84', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-17 13:08:41', '0000-00-00 00:00:00'),
(177, 74, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '84', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-17 13:08:41', '0000-00-00 00:00:00'),
(178, 20, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '85', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-17 13:09:06', '0000-00-00 00:00:00'),
(179, 74, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '85', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-17 13:09:06', '0000-00-00 00:00:00'),
(180, 20, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '86', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-17 13:09:27', '0000-00-00 00:00:00'),
(181, 74, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '86', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-17 13:09:27', '0000-00-00 00:00:00'),
(182, 85, 'bpri12345@yopmail.com', 'Bharat Prima', 'Distrct_approved_school', 2, 82, NULL, NULL, NULL, NULL, 1, '2020-11-18 05:58:18', '0000-00-00 00:00:00'),
(183, 158, 'bpri33@yopmail.com', 'Bharat Schooollll', 'Distrct_approved_school', 2, 82, NULL, NULL, NULL, NULL, 1, '2020-11-18 05:58:18', '0000-00-00 00:00:00'),
(184, 158, 'bpri33@yopmail.com', 'Bharat SC', 'Broadcast_notification', 1, 82, 'bdis123@yopmail.com', 'Bharat Dis', 'Test', 'Hiii', 1, '2020-11-18 06:38:22', '0000-00-00 00:00:00'),
(185, 11, 'teacher@mailinator.com', 'Teacher', 'Broadcast_notification', 2, 10, 'School@mailinator.com', 'SchoolQ SLast', 'Tygc', 'Ethfsw', 1, '2020-11-18 07:06:42', '0000-00-00 00:00:00'),
(186, 159, 'ocean@mailinator.com', 'ocean last', 'Distrct_approved_school', 2, 1, NULL, NULL, NULL, NULL, 1, '2020-11-18 12:54:02', '0000-00-00 00:00:00'),
(187, 159, 'ocean@mailinator.com', 'ocean last', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'wer', 'er', 1, '2020-11-18 13:18:20', '0000-00-00 00:00:00'),
(188, 153, 'dino@mailinator.com', 'Dino mie', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'ff', 'fdf', 1, '2020-11-18 13:19:15', '0000-00-00 00:00:00'),
(189, 27, 'dummyschool123@mailinator.com', 'dummy school 123d', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'dddfd', 'dfdf', 1, '2020-11-18 13:19:32', '0000-00-00 00:00:00'),
(190, 40, 'kingjunior@mailinator.com', 'King junior Luthor', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'dddfd', 'dfdf', 1, '2020-11-18 13:19:32', '0000-00-00 00:00:00'),
(191, 67, 'test11@mailinator.com', 'testtesttetest test1', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'dddfd', 'dfdf', 1, '2020-11-18 13:19:32', '0000-00-00 00:00:00'),
(192, 78, 'juniour@mailinator.com', 'Juniour test', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'dddfd', 'dfdf', 1, '2020-11-18 13:19:32', '0000-00-00 00:00:00'),
(193, 132, 'donald@mailinator.com', 'Robin Donald', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'dddfd', 'dfdf', 1, '2020-11-18 13:19:32', '0000-00-00 00:00:00'),
(194, 153, 'dino@mailinator.com', 'Dino mie', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'dddfd', 'dfdf', 1, '2020-11-18 13:19:32', '0000-00-00 00:00:00'),
(195, 155, 'harry@mailinator.com', 'Harry Ben', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'dddfd', 'dfdf', 1, '2020-11-18 13:19:32', '0000-00-00 00:00:00'),
(196, 159, 'ocean@mailinator.com', 'ocean last', 'Distrct_approved_school', 2, 1, NULL, NULL, NULL, NULL, 1, '2020-11-18 13:32:19', '0000-00-00 00:00:00'),
(197, 159, 'ocean@mailinator.com', 'ocean last', 'Distrct_approved_school', 2, 1, NULL, NULL, NULL, NULL, 1, '2020-11-18 13:33:25', '0000-00-00 00:00:00'),
(198, 20, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '87', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-19 05:43:29', '0000-00-00 00:00:00'),
(199, 74, '', '', 'Teacher_upload_video', 4, 11, NULL, NULL, '87', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-19 05:43:29', '0000-00-00 00:00:00'),
(200, 20, '', '', 'Teacher_approved_student', 4, 11, NULL, NULL, NULL, 'mike \'s request approved by Teacher for 1st Grade', 1, '2020-11-19 05:49:52', '0000-00-00 00:00:00'),
(201, 56, 'checkteacher@mailinator.com', 'check teacher', 'Broadcast_notification', 2, 27, 'dummyschool123@mailinator.com', 'dummy school 123d', 'erer', 'ere', 1, '2020-11-19 07:07:28', '0000-00-00 00:00:00'),
(202, 82, 'bdis123@yopmail.com', 'Bharat Dis', 'Broadcast_notification', 0, 1, 'cam@mailinator.com', 'cam_school', 'test', '<p>\r\n	testadfsfffdsf</p>', 1, '2020-11-19 10:09:16', '0000-00-00 00:00:00'),
(203, 143, 'fridas@mailinator.com', 'Frida peter', 'Broadcast_notification', 0, 1, 'cam@mailinator.com', 'cam_school', 'test', '<p>\r\n	testadfsfffdsf</p>', 1, '2020-11-19 10:09:16', '0000-00-00 00:00:00'),
(204, 82, 'bdis123@yopmail.com', 'Bharat Dis', 'Broadcast_notification', 0, 1, 'cam@mailinator.com', 'cam_school', 'test', 'adfasdfsdfsf', 1, '2020-11-19 10:14:24', '0000-00-00 00:00:00'),
(205, 143, 'fridas@mailinator.com', 'Frida peter', 'Broadcast_notification', 0, 1, 'cam@mailinator.com', 'cam_school', 'test', 'adfasdfsdfsf', 1, '2020-11-19 10:14:24', '0000-00-00 00:00:00'),
(206, 82, 'bdis123@yopmail.com', 'Bharat Dis', 'Broadcast_notification', 0, 1, 'cam@mailinator.com', 'cam_school', 'test', 'asdfsadf', 1, '2020-11-19 10:15:06', '0000-00-00 00:00:00'),
(207, 68, 'dist@mailinator.com', 'dist last dist', 'Broadcast_notification', 0, 1, 'cam@mailinator.com', 'Cam School Admin', 'Broadcast Message to Only one user', 'Hello,\r\n\r\nHow are you ! here is you content\r\n\r\nBest Regards,\r\nRashique Khan', 1, '2020-11-19 10:43:20', '0000-00-00 00:00:00'),
(208, 30, 'bharatdis@yopmail.com', 'bharat district', 'Broadcast_notification', 0, 1, 'cam@mailinator.com', 'Cam School Admin', 'test', 'sdf sdfdf', 1, '2020-11-19 10:54:29', '0000-00-00 00:00:00'),
(209, 184, 'tri110@mailinator.com', ' ', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'test', 'et', 1, '2020-11-20 06:02:53', '0000-00-00 00:00:00'),
(210, 124, 'trisha@mailinator.com', 'Trisha shah', 'Broadcast_notification', 2, 27, 'dummyschool123@mailinator.com', 'dummy school 123d', 'my test', 'mytest', 1, '2020-11-20 06:03:29', '0000-00-00 00:00:00'),
(211, 56, 'checkteacher@mailinator.com', 'check teacher', 'Broadcast_notification', 2, 27, 'dummyschool123@mailinator.com', 'dummy school 123d', 'w', 's', 1, '2020-11-20 07:42:22', '0000-00-00 00:00:00'),
(212, 56, 'checkteacher@mailinator.com', 'check teacher', 'Broadcast_notification', 2, 27, 'dummyschool123@mailinator.com', 'dummy school 123d', 'e', 'ee', 1, '2020-11-20 07:46:50', '0000-00-00 00:00:00'),
(213, 20, 'Student@mailinator.com', 'Student Dlast', 'Teacher_upload_video', 4, 11, NULL, NULL, '88', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-20 08:14:52', '0000-00-00 00:00:00'),
(214, 74, 'student1@mailinator.com', 'Student nnee New', 'Teacher_upload_video', 4, 11, NULL, NULL, '88', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-20 08:14:52', '0000-00-00 00:00:00'),
(215, 20, 'Student@mailinator.com', 'Student Dlast', 'Teacher_upload_video', 4, 11, 'teacher@mailinator.com', NULL, '89', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-20 08:19:08', '0000-00-00 00:00:00'),
(216, 74, 'student1@mailinator.com', 'Student nnee New', 'Teacher_upload_video', 4, 11, 'teacher@mailinator.com', NULL, '89', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-20 08:19:08', '0000-00-00 00:00:00'),
(217, 20, 'Student@mailinator.com', 'Student Dlast', 'Teacher_upload_video', 4, 11, 'teacher@mailinator.com', 'Teacher', '90', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-20 08:21:12', '0000-00-00 00:00:00'),
(218, 74, 'student1@mailinator.com', 'Student nnee New', 'Teacher_upload_video', 4, 11, 'teacher@mailinator.com', 'Teacher', '90', 'Teacher uploaded new video for Kindergarten', 1, '2020-11-20 08:21:12', '0000-00-00 00:00:00'),
(219, 184, 'tri110@mailinator.com', ' ', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'tets', 'ett', 1, '2020-11-20 09:18:03', '0000-00-00 00:00:00'),
(220, 183, 'masa11@mailinator.com', ' ', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'rrt', 'trt', 1, '2020-11-20 09:21:00', '0000-00-00 00:00:00'),
(221, 183, 'masa11@mailinator.com', ' ', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'dfdf', 'df', 1, '2020-11-20 09:23:22', '0000-00-00 00:00:00'),
(222, 29, 'nehagawade741@gmail.com', 'Dyna Lee', 'Broadcast_notification', 2, 27, 'dummyschool123@mailinator.com', 'dummy school 123d', 'test email', 'test email from cam school', 1, '2020-11-20 09:45:54', '0000-00-00 00:00:00'),
(223, 29, 'ngawade@hitaishin.com', 'Dyna Lee', 'Broadcast_notification', 2, 27, 'dummyschool123@mailinator.com', 'dummy school 123d', 'test mail', 'test mail test mail', 1, '2020-11-20 09:48:37', '0000-00-00 00:00:00'),
(224, 20, '', '', 'Teacher_approved_student', 4, 11, NULL, NULL, NULL, 'd\'s request approved by Teacher for 2nd Grade', 1, '2020-11-20 10:15:31', '0000-00-00 00:00:00'),
(225, 20, '', '', 'Teacher_approved_student', 4, 11, NULL, NULL, NULL, 'orio\'s request approved by Teacher for 1st Grade', 1, '2020-11-20 10:15:31', '0000-00-00 00:00:00'),
(226, 20, '', '', 'Teacher_approved_student', 4, 11, NULL, NULL, NULL, 'Keety\'s request approved by Teacher for 2nd Grade', 1, '2020-11-20 10:15:31', '0000-00-00 00:00:00'),
(227, 20, '', '', 'Teacher_approved_student', 4, 11, NULL, NULL, NULL, 'Keety\'s request approved by Teacher for 2nd Grade', 1, '2020-11-20 10:15:31', '0000-00-00 00:00:00'),
(228, 20, '', '', 'Teacher_approved_student', 4, 11, NULL, NULL, NULL, 'erer\'s request approved by Teacher for 1st Grade', 1, '2020-11-20 12:16:32', '0000-00-00 00:00:00'),
(229, 20, '', '', 'Teacher_approved_student', 4, 11, NULL, NULL, NULL, 'martin\'s request approved by Teacher for 1st Grade', 1, '2020-11-20 12:23:50', '0000-00-00 00:00:00'),
(230, 153, 'dino@mailinator.com', 'Dino mie', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'dssd', 'dsfdsf', 1, '2020-11-20 12:27:36', '0000-00-00 00:00:00'),
(231, 20, '', '', 'Teacher_approved_student', 4, 11, NULL, NULL, NULL, 'meenu\'s request approved by Teacher for 1st Grade', 1, '2020-11-20 12:47:57', '0000-00-00 00:00:00'),
(232, 20, '', '', 'Teacher_approved_student', 4, 11, NULL, NULL, NULL, 'jeera\'s request approved by Teacher for 1st Grade', 1, '2020-11-20 12:51:38', '0000-00-00 00:00:00'),
(233, 20, '', '', 'Teacher_approved_student', 4, 11, NULL, NULL, NULL, 'jeera\'s request approved by Teacher for 2nd Grade', 1, '2020-11-20 12:51:38', '0000-00-00 00:00:00'),
(234, 20, '', '', 'Teacher_approved_student', 4, 11, NULL, NULL, NULL, 'nisha\'s request approved by Teacher for 1st Grade', 1, '2020-11-20 12:54:38', '0000-00-00 00:00:00'),
(235, 20, '', '', 'Teacher_approved_student', 4, 11, NULL, NULL, NULL, 'nisha\'s request approved by Teacher for 2nd Grade', 1, '2020-11-20 12:54:38', '0000-00-00 00:00:00'),
(236, 20, 'Student@mailinator.com', 'Student Dlast', 'Teacher_upload_video', 4, 11, 'teacher@mailinator.com', 'Teacher', '91', 'Teacher uploaded new video for 1st Grade', 1, '2020-11-21 07:45:00', '0000-00-00 00:00:00'),
(237, 124, 'trisha@mailinator.com', 'Trisha shah', 'Broadcast_notification', 2, 27, 'dummyschool123@mailinator.com', 'dummy school 123d', 'Test', 'Test Message for Trisha', 1, '2020-11-21 07:53:40', '0000-00-00 00:00:00'),
(238, 124, 'trisha@mailinator.com', 'Trisha shah', 'Broadcast_notification', 2, 27, 'dummyschool123@mailinator.com', 'dummy school 123d', 'Test', 'TEst One two Three', 1, '2020-11-21 09:26:53', '0000-00-00 00:00:00'),
(239, 124, 'trisha@mailinator.com', 'Trisha shah', 'Broadcast_notification', 2, 27, 'dummyschool123@mailinator.com', 'dummy school 123d', 'test', 'sdfsadfdsfsdf', 1, '2020-11-21 09:28:58', '0000-00-00 00:00:00'),
(240, 124, 'trisha@mailinator.com', 'Trisha shah', 'Broadcast_notification', 2, 27, 'dummyschool123@mailinator.com', 'dummy school 123d', 'testsadf', 'adsf afdsaf', 1, '2020-11-21 09:30:44', '0000-00-00 00:00:00'),
(241, 124, 'trisha@mailinator.com', 'Trisha shah', 'Broadcast_notification', 2, 27, 'dummyschool123@mailinator.com', 'dummy school 123d', 'Test', 'Test Message For test', 1, '2020-11-21 09:32:48', '0000-00-00 00:00:00'),
(242, 7, 'principal1@yopmail.com', 'bharat rawal', 'Broadcast_notification', 1, 3, 'district@yopmail.com', 'bharat bharat', 'hello Subject Bharat', 'Hello Message Bharat', 1, '2020-11-21 09:44:19', '0000-00-00 00:00:00'),
(243, 190, 'teacher55@mailinator.com', 'TeacherNn Tlast', 'School_approved_teacher', 3, 10, NULL, NULL, NULL, NULL, 1, '2020-11-21 10:32:26', '0000-00-00 00:00:00'),
(244, 20, 'Student@mailinator.com', 'Student Dlast', 'Teacher_upload_video', 4, 11, 'teacher@mailinator.com', 'Teacher', '92', 'Teacher uploaded new video for 2nd Grade', 1, '2020-11-21 11:53:49', '0000-00-00 00:00:00'),
(245, 20, 'Student@mailinator.com', 'Student Dlast', 'Teacher_upload_video', 4, 11, 'teacher@mailinator.com', 'Teacher', '93', 'Teacher uploaded new video for 2nd Grade', 1, '2020-11-21 12:05:50', '0000-00-00 00:00:00'),
(246, 192, 'pri99@yopmail.com', 'Bharat Principal', 'Distrct_approved_school', 2, 191, NULL, NULL, NULL, NULL, 1, '2020-11-21 12:27:47', '0000-00-00 00:00:00'),
(247, 192, 'pri99@yopmail.com', 'Bharat st. School', 'Broadcast_notification', 1, 191, 'dis99@yopmail.com', 'Bharat District', 'Hiii', 'Hi allll', 1, '2020-11-21 12:46:46', '0000-00-00 00:00:00'),
(248, 27, 'dummyschool123@mailinator.com', 'dummy school 123d', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'test', 'test', 1, '2020-11-21 12:47:13', '0000-00-00 00:00:00'),
(249, 40, 'kingjunior@mailinator.com', 'King junior Luthor', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'test', 'test', 1, '2020-11-21 12:47:13', '0000-00-00 00:00:00'),
(250, 67, 'test11@mailinator.com', 'testtesttetest test1', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'test', 'test', 1, '2020-11-21 12:47:13', '0000-00-00 00:00:00'),
(251, 78, 'juniour@mailinator.com', 'Juniour test', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'test', 'test', 1, '2020-11-21 12:47:13', '0000-00-00 00:00:00'),
(252, 132, 'donald@mailinator.com', 'Robin Donald', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'test', 'test', 1, '2020-11-21 12:47:13', '0000-00-00 00:00:00'),
(253, 153, 'dino@mailinator.com', 'Dino mie', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'test', 'test', 1, '2020-11-21 12:47:13', '0000-00-00 00:00:00'),
(254, 155, 'harry@mailinator.com', 'Harry Ben', 'Broadcast_notification', 1, 1, 'merry@mailinator.com', 'Merry Tylor', 'test', 'test', 1, '2020-11-21 12:47:13', '0000-00-00 00:00:00'),
(255, 193, 'tea99@yopmail.com', 'Bharat Teacher', 'School_approved_teacher', 3, 192, NULL, NULL, NULL, NULL, 1, '2020-11-21 12:48:51', '0000-00-00 00:00:00'),
(256, 194, '', '', 'Teacher_approved_student', 4, 193, NULL, NULL, NULL, 'Bharat\'s request approved by BharatTea for 6th Grade', 1, '2020-11-21 13:14:35', '0000-00-00 00:00:00'),
(257, 194, '', '', 'Teacher_approved_student', 4, 193, NULL, NULL, NULL, 'Bharat\'s request approved by BharatTea for Kindergarten', 1, '2020-11-21 13:14:35', '0000-00-00 00:00:00'),
(258, 194, 'std99@yopmail.com', 'Bharat Parent', 'Teacher_upload_video', 4, 193, 'tea99@yopmail.com', 'BharatTea', '94', 'BharatTea uploaded new video for Kindergarten', 1, '2020-11-21 13:15:31', '0000-00-00 00:00:00'),
(259, 124, 'trisha@mailinator.com', 'Trisha shah', 'Broadcast_notification', 2, 27, 'dummyschool123@mailinator.com', 'dummy school 123d', 'et', 'fhfgh', 1, '2020-11-22 05:45:27', '0000-00-00 00:00:00'),
(260, 124, 'trisha@mailinator.com', 'Trisha shah', 'Broadcast_notification', 2, 27, 'dummyschool123@mailinator.com', 'dummy school 123d', 'et', 'fhfgh', 1, '2020-11-22 05:45:28', '0000-00-00 00:00:00'),
(261, 56, 'checkteacher@mailinator.com', 'check teacher', 'Broadcast_notification', 2, 27, 'dummyschool123@mailinator.com', 'dummy school 123d', 'try', 'rty', 1, '2020-11-22 05:45:38', '0000-00-00 00:00:00'),
(262, 20, 'Student@mailinator.com', 'Student Dlast', 'Teacher_approved_student', 4, 11, NULL, NULL, NULL, 'test\'s request approved by Teacher for 1st Grade', 1, '2020-11-22 06:26:28', '0000-00-00 00:00:00'),
(263, 196, 'jeera@mailinator.com', 'jerra sevy', 'School_approved_teacher', 3, 27, NULL, NULL, NULL, NULL, 0, '2020-11-22 07:07:31', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `first_time_visitor`
--

CREATE TABLE `first_time_visitor` (
  `id` int(11) NOT NULL,
  `media_type` tinyint(1) DEFAULT 1 COMMENT '1=Video, 2=Audio',
  `format` varchar(50) DEFAULT NULL,
  `thumb_image` text DEFAULT NULL,
  `large_image` text DEFAULT NULL,
  `media_name` varchar(100) DEFAULT NULL,
  `duration` time DEFAULT '00:00:00',
  `status` tinyint(1) DEFAULT 2 COMMENT '1=Active, 2=Deactive,3=Uncomplete',
  `views` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `follow_users`
--

CREATE TABLE `follow_users` (
  `id` int(11) NOT NULL,
  `follow_to` int(11) NOT NULL,
  `followed_by` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

CREATE TABLE `friends` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `status` tinyint(5) NOT NULL COMMENT '0=pending,1=Accepted',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `friends_logs`
--

CREATE TABLE `friends_logs` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `status` tinyint(5) NOT NULL COMMENT '0=pending,1=Accepted,2=Rejected, 3=Unfriend, 4= Canceled',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `media_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_mac_address` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `media_id`, `user_id`, `user_mac_address`, `created`) VALUES
(1, 14, 15, NULL, '2020-11-05 10:56:42'),
(12, 1, 64, NULL, '2020-11-08 11:44:27'),
(13, 6, 20, NULL, '2020-11-09 10:11:25'),
(16, 3, 11, NULL, '2020-11-09 10:55:42'),
(17, 1, 11, NULL, '2020-11-10 07:03:17'),
(24, 71, 91, NULL, '2020-11-20 07:41:42');

-- --------------------------------------------------------

--
-- Table structure for table `likes_deleted`
--

CREATE TABLE `likes_deleted` (
  `id` int(11) NOT NULL,
  `media_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_mac_address` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `teacher_grade_id` int(11) NOT NULL,
  `grade` tinyint(3) NOT NULL,
  `grade_display_name` varchar(20) NOT NULL,
  `school_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `media_type` tinyint(1) DEFAULT 1 COMMENT '1=Video, 2=Audio',
  `format` varchar(50) DEFAULT NULL,
  `title` varchar(60) DEFAULT NULL,
  `media_slug` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `thumb_image` text DEFAULT NULL,
  `large_image` text DEFAULT NULL,
  `media_name` varchar(100) DEFAULT NULL,
  `duration` time DEFAULT NULL,
  `tags` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT 2 COMMENT '1=Active, 2=Deactive,3=Uncomplete',
  `comments` int(11) DEFAULT 0,
  `created_as` tinyint(1) DEFAULT 1 COMMENT '1=Media, 2 = Comment',
  `views` int(11) DEFAULT 0,
  `likes` int(11) DEFAULT 0,
  `dislikes` int(11) DEFAULT 0,
  `is_deleted` tinyint(1) NOT NULL COMMENT '0= not deleted 1 =deleted',
  `deleted_date` datetime NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `user_id`, `teacher_grade_id`, `grade`, `grade_display_name`, `school_id`, `district_id`, `media_type`, `format`, `title`, `media_slug`, `description`, `thumb_image`, `large_image`, `media_name`, `duration`, `tags`, `status`, `comments`, `created_as`, `views`, `likes`, `dislikes`, `is_deleted`, `deleted_date`, `deleted_by`, `created`, `modified`) VALUES
(1, 11, 16, 0, 'Kindergarten', 3, 5, 1, '3', 'Testing1', 'testing1-NTdfMQ==', 'NewTesting.', 'https://cam-school-staging.s3.amazonaws.com/school/1604470593_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604470593_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com//media_1604470583913_video.mp4', '00:00:05', NULL, 1, 3, 1, 0, 2, 0, 0, '2020-11-18 10:20:24', 1, '2020-11-04 06:16:45', NULL),
(2, 11, 16, 0, 'Kindergarten', 3, 5, 1, '3', 'Video23dsd', 'video23ds-NTdfMg==', 'New description 2 Check Now.5dsd', 'https://cam-school-staging.s3.amazonaws.com/school/1604471870_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604471870_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com//media_1604471857277_video.mp4', '00:00:06', NULL, 1, 3, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-08 11:06:40', NULL),
(3, 11, 17, 1, 'First Grade', 3, 5, 1, '3', 'Title video 35', 'title-video-3-NTdfMw==', 'WDiscription.', 'https://cam-school-staging.s3.amazonaws.com/school/1604472694_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604472694_800x450.jpg', 'https://cam-school-staging.s3-us-west-2.amazonaws.com/aws.mp4', '00:00:06', NULL, 1, 4, 1, 0, 1, 0, 1, '2020-11-18 10:20:28', 1, '2020-11-04 06:51:59', NULL),
(4, 11, 18, 2, 'Second Grade', 3, 5, 1, '3', 'Ti', 'ti-NTdfNA==', 'Jdjdj', 'https://cam-school-staging.s3.amazonaws.com/school/1604473305_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604473305_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com//media_1604473293916_video.mp4', '00:00:06', NULL, 1, 1, 1, 0, 0, 0, 1, '2020-11-18 10:20:31', 1, '2020-11-04 07:01:56', NULL),
(5, 11, 19, 3, 'Third Grade', 3, 5, 1, '3', 'Vid1', 'vid1-NTdfNQ==', 'Vid1 description.', 'https://cam-school-staging.s3.amazonaws.com/school/1604474995_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604474995_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com//media_1604474838925_video.mp4', '00:00:06', NULL, 1, 0, 1, 0, 0, 0, 1, '2020-11-18 10:20:34', 1, '2020-11-04 07:30:21', NULL),
(6, 11, 16, 0, 'Kindergarten', 3, 5, 1, '3', 'Vid2', 'vid2-NTdfNg==', 'Vid2 description.', 'https://cam-school-staging.s3.amazonaws.com/school/1604475156_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604475156_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com//media_1604475142265_video.mp4', '00:00:05', NULL, 1, 24, 1, 0, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 06:44:27', NULL),
(7, 15, 33, 0, 'Kindergarten', 2, 3, 1, '2', 'test video teacher', 'test-video-teacher-NTdfNw==', 'My Video', 'https://cam-school-staging.s3.amazonaws.com/00000015/1604562520_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000015/1604562520_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/userttttt15/media_1604562509_video.mp4', '00:00:03', NULL, 1, 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-05 07:49:11', NULL),
(8, 15, 38, 1, 'First Grade', 2, 3, 1, '2', 'First Video test', 'first-video-test-NTdfOA==', 'Testtttt description was the first time that we had to share the story of a few of them and then the first time we', 'https://cam-school-staging.s3.amazonaws.com/00000015/1604562847_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000015/1604562847_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/userttttt15/media_1604562844_video.mp4', '00:00:03', NULL, 1, 0, 1, 0, 0, 0, 1, '2020-11-05 10:35:14', 15, '2020-11-05 10:10:56', NULL),
(9, 15, 38, 1, 'First Grade', 2, 3, 1, '2', 'second video', 'second-video-NTdfOQ==', 'Test video 2 tests and then get to test test tomorrow morning to test it', 'https://cam-school-staging.s3.amazonaws.com/00000015/1604572306_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000015/1604572306_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/userttttt15/media_1604572233_video.mp4', '00:00:10', NULL, 1, 3, 1, 0, 0, 0, 1, '2020-11-05 10:36:40', 15, '2020-11-05 10:32:36', NULL),
(10, 15, 38, 1, 'First Grade', 2, 3, 1, '2', 'test', 'test-NTdfMTA=', 'User test and then I have the same problem with this app', 'https://cam-school-staging.s3.amazonaws.com/00000015/1604572699_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000015/1604572699_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/userttttt15/media_1604572691_video.mp4', '00:00:18', NULL, 1, 0, 1, 0, 0, 0, 1, '2020-11-05 10:41:54', 15, '2020-11-05 10:39:22', NULL),
(11, 15, 38, 1, 'First Grade', 2, 3, 1, '2', 'my video', 'my-video-NTdfMTE=', 'User test', 'https://cam-school-staging.s3.amazonaws.com/00000015/1604572863_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000015/1604572863_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/userttttt15/media_1604572860_video.mp4', '00:00:06', NULL, 1, 0, 1, 0, 0, 0, 1, '2020-11-05 10:42:52', 15, '2020-11-05 10:41:21', NULL),
(12, 15, 38, 1, 'First Grade', 2, 3, 1, '2', 'tesyyy', 'tesyyy-NTdfMTI=', 'Hshs hshs', 'https://cam-school-staging.s3.amazonaws.com/00000015/1604573092_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000015/1604573092_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/userttttt15/media_1604573089_video.mp4', '00:00:05', NULL, 1, 0, 1, 0, 0, 0, 1, '2020-11-05 10:46:50', 15, '2020-11-05 10:45:55', NULL),
(13, 15, 38, 1, 'First Grade', 2, 3, 1, '2', 'sheeghr', 'sheeghr-NTdfMTM=', 'Gdjgsnsgnzgnzgngzgz', 'https://cam-school-staging.s3.amazonaws.com/00000015/1604573329_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000015/1604573329_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/userttttt15/media_1604573325_video.mp4', '00:00:08', NULL, 1, 1, 1, 0, 0, 0, 1, '2020-11-05 10:51:32', 15, '2020-11-05 10:49:12', NULL),
(14, 15, 38, 1, 'First Grade', 2, 3, 1, '2', 'sfndgnsgngs', 'sfndgnsgngs-NTdfMTQ=', 'S dgnsgndgdgndng', 'https://cam-school-staging.s3.amazonaws.com/00000015/1604573532_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000015/1604573532_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/userttttt15/media_1604573528_video.mp4', '00:00:10', NULL, 1, 0, 1, 0, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-05 10:52:26', NULL),
(15, 32, 40, 0, 'Kindergarten', 8, 13, 1, '8', 'test', 'test-NTdfMTU=', 'Bsbsbs bsbsbs', 'https://cam-school-staging.s3.amazonaws.com/00000032/1604588373_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000032/1604588373_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/32/media_1604588366_video.mp4', '00:00:05', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-05 14:59:53', NULL),
(16, 32, 40, 0, 'Kindergarten', 8, 13, 1, '8', 'test 2', 'test-2-NTdfMTY=', 'Test audio is', 'https://cam-school-staging.s3.amazonaws.com/00000032/1604588408_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000032/1604588408_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/32/media_1604588403_video.mp4', '00:00:04', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-05 15:00:27', NULL),
(17, 32, 41, 1, 'First Grade', 8, 13, 1, '8', 'bsbsbs dB', 'bsbsbs-db-NTdfMTc=', 'Bsushsbsjdjdbdj hshs s s hsbs', 'https://cam-school-staging.s3.amazonaws.com/00000032/1604588449_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000032/1604588449_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/32/media_1604588442_video.mp4', '00:00:03', NULL, 1, 2, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-05 15:01:05', NULL),
(18, 32, 41, 1, 'First Grade', 8, 13, 1, '8', 'ho appena completato la missione', 'ho-appena-completato-la-missione-NTdfMTg=', 'Vivuvjvjsvivaisv', 'https://cam-school-staging.s3.amazonaws.com/00000032/1604588482_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000032/1604588482_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/32/media_1604588475_video.mp4', '00:00:05', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-05 15:01:39', NULL),
(19, 32, 42, 2, 'Second Grade', 8, 13, 1, '8', 'user interface can use', 'user-interface-can-use-NTdfMTk=', 'Test test', 'https://cam-school-staging.s3.amazonaws.com/00000032/1604588516_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000032/1604588516_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/32/media_1604588512_video.mp4', '00:00:04', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-05 15:02:15', NULL),
(20, 34, 43, 2, 'Second Grade', 8, 13, 1, '8', 'texttt', 'texttt-NTdfMjA=', 'Vivjcjcjcjcicjcuc', 'https://cam-school-staging.s3.amazonaws.com/00000034/1604652471_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000034/1604652471_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/34/media_1604652463_video.mp4', '00:00:05', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-06 08:48:04', NULL),
(21, 34, 43, 2, 'Second Grade', 8, 13, 1, '8', 'hiiii', 'hiiii-NTdfMjE=', 'Bksbisvidvivdivd', 'https://cam-school-staging.s3.amazonaws.com/00000034/1604652504_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000034/1604652504_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/34/media_1604652500_video.mp4', '00:00:05', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-06 08:48:40', NULL),
(22, 34, 44, 1, 'First Grade', 8, 13, 1, '8', 'hdhd hdhd', 'hdhd-hdhd-NTdfMjI=', 'Hdjdjdhdjdjhdhdbdbd', 'https://cam-school-staging.s3.amazonaws.com/00000034/1604652544_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000034/1604652544_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/34/media_1604652542_video.mp4', '00:00:04', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-06 08:49:20', NULL),
(23, 11, 16, 8, 'Eight Grade', 7, 13, 1, '3', 'Comment video1', 'comment-video1-NTdfMjM=', 'New comment video 1', 'https://cam-school-staging.s3.amazonaws.com/school/1604728118_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604728118_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com//media_1604728108609_video.mp4', '00:00:03', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-07 05:48:52', NULL),
(24, 11, 17, 8, 'Eight Grade', 7, 13, 1, '3', 'Title video 35-01', 'title-video-3-NTdfMw===', 'WDiscription.', 'https://cam-school-staging.s3.amazonaws.com/school/1604472694_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604472694_800x450.jpg', 'https://cam-school-staging.s3-us-west-2.amazonaws.com/aws.mp4', '00:00:06', NULL, 1, 0, 1, 0, 0, 0, 1, '2020-11-18 10:20:38', 1, '2020-11-04 06:51:59', NULL),
(25, 11, 17, 8, 'Eight Grade', 7, 13, 1, '3', 'Title video 35-02', 'title-video-3-NTdfMw====', 'WDiscription.', 'https://cam-school-staging.s3.amazonaws.com/school/1604472694_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604472694_800x450.jpg', 'https://cam-school-staging.s3-us-west-2.amazonaws.com/aws.mp4', '00:00:06', NULL, 1, 0, 1, 0, 0, 0, 1, '2020-11-18 10:20:41', 1, '2020-11-04 06:51:59', NULL),
(26, 15, 33, 0, 'Kindergarten', 2, 3, 1, 'video/mp4', 'huhj', 'huhj-NTdfMjY=', 'huhuh', 'https://cam-school-staging.s3.amazonaws.com/school/1604826062_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604826062_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/video_1604826062_v.mp4', '00:00:05', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-08 09:01:10', NULL),
(27, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'comment video', 'comment-video-NTdfMjc=', 'video comment', 'https://cam-school-staging.s3.amazonaws.com/school/1604848883_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604848883_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/video_1604848883_v.mp4', '00:00:05', NULL, 1, 0, 2, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-08 15:21:31', NULL),
(28, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'comment video', 'comment-video-NTdfMjg=', 'video comment', 'https://cam-school-staging.s3.amazonaws.com/school/1604848891_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604848891_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/video_1604848891_v.mp4', '00:00:05', NULL, 1, 0, 2, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-08 15:21:40', NULL),
(29, 72, 66, 0, 'Kindergarten', 18, 15, 1, '18', 'Teacher Post', 'teacher-post-NTdfMjk=', 'Teacher DFescr', 'https://cam-school-staging.s3.amazonaws.com/00000072/1604900192_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000072/1604900192_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/72/media_1604900189_video.mp4', '00:00:04', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 05:37:16', NULL),
(30, 72, 68, 10, 'Tenth Grade', 18, 15, 1, '18', 'Dum just Video', 'dum-just-video-NTdfMzA=', 'Bymmmyyyyyyyy', 'https://cam-school-staging.s3.amazonaws.com/00000072/1604900320_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000072/1604900320_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/72/media_1604900317_video.mp4', '00:00:03', NULL, 1, 28, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 05:39:20', NULL),
(31, 72, 66, 0, 'Kindergarten', 18, 15, 1, '18', 'Hiiii', 'hiiii-NTdfMzE=', 'Hi alll Helloooo', 'https://cam-school-staging.s3.amazonaws.com/00000072/1604900413_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000072/1604900413_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/72/media_1604900398_video.mp4', '00:00:37', NULL, 1, 28, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 05:41:26', NULL),
(32, 72, 69, 11, 'Eleventh Grade', 18, 15, 1, '18', 'Hi Test', 'hi-test-NTdfMzI=', 'Description video', 'https://cam-school-staging.s3.amazonaws.com/00000072/1604901550_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000072/1604901550_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/72/media_1604901530_video.mp4', '00:00:00', NULL, 1, 5, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 05:59:34', NULL),
(33, 72, 66, 0, 'Kindergarten', 18, 15, 1, '18', 'Testtt', 'testtt-NTdfMzM=', 'Ghoulishvdsfsdfdsf', 'https://cam-school-staging.s3.amazonaws.com/00000072/1604905866_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000072/1604905866_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/Rawal%20Display%20Name72/media_1604905860_video.mp4', '00:00:05', NULL, 1, 0, 2, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 07:11:45', NULL),
(34, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'my comment', 'my-comment-NTdfMzQ=', 'test', 'https://cam-school-staging.s3.amazonaws.com/school/1604906121_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604906121_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/video_1604906121_v.mp4', '00:00:05', NULL, 1, 0, 2, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 07:15:29', NULL),
(35, 72, 66, 0, 'Kindergarten', 18, 15, 1, '18', 'Hiiiii', 'hiiiii-NTdfMzU=', 'Testttttt', 'https://cam-school-staging.s3.amazonaws.com/00000072/1604906091_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000072/1604906091_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/Rawal%20Display%20Name72/media_1604906087_video.mp4', '00:00:05', NULL, 1, 0, 2, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 07:15:39', NULL),
(36, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'my video comment', 'my-video-comment-NTdfMzY=', 'video', 'https://cam-school-staging.s3.amazonaws.com/school/1604914247_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604914247_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/video_1604914247_v.mp4', '00:00:05', NULL, 1, 0, 2, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 09:30:56', NULL),
(38, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'new test videos', 'new-test-videos-NTdfMzg=', 'new test videos', 'https://cam-school-staging.s3.amazonaws.com/school/1604915261_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604915261_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/video_1604915261_v.mp4', '00:00:05', NULL, 1, 0, 2, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 09:47:48', NULL),
(39, 11, 17, 1, 'First Grade', 3, 5, 1, 'video/mp4', 'test2', 'test2-NTdfMzk=', 'test2', 'https://cam-school-staging.s3.amazonaws.com/school/1604915661_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604915661_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/video_1604915661_v.mp4', '00:00:05', NULL, 1, 1, 2, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 09:54:29', NULL),
(40, 11, 16, 0, 'Kindergarten', 3, 5, 1, '3', 'Vid comment 1', 'vid-comment-1-NTdfNDA=', 'Vid comment 1', 'https://cam-school-staging.s3.amazonaws.com/school/1604916806_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604916806_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com//media_1604916791002_video.mp4', '00:00:01', NULL, 1, 0, 2, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:13:34', NULL),
(41, 72, 66, 0, 'Kindergarten', 18, 15, 1, '18', 'My comment video', 'my-comment-video-NTdfNDE=', 'Hi all test data', 'https://cam-school-staging.s3.amazonaws.com/00000072/1604989122_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000072/1604989122_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/Rawal%20Display%20Name72/media_1604989115_video.mp4', '00:00:05', NULL, 1, 0, 2, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 06:19:08', NULL),
(42, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'test', 'test-NTdfNDI=', 'test', 'https://cam-school-staging.s3.amazonaws.com/school/1604993183_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604993183_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/video_1604993183_v.mp4', '00:02:38', NULL, 1, 0, 1, 0, 0, 0, 1, '2020-11-10 07:30:25', 11, '2020-11-10 07:30:11', NULL),
(43, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'test', 'test-NTdfNDM=', 'test', 'https://cam-school-staging.s3.amazonaws.com/school/1604993453_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604993453_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/video_1604993453_v.mp4', '00:00:05', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 07:31:03', NULL),
(44, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'testtttt', 'testtttt-NTdfNDQ=', 'test', 'https://cam-school-staging.s3.amazonaws.com/00000011/1604994041_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/1604994041_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/video_1604994041_v.mp4', '00:00:05', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 07:40:51', NULL),
(45, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'test', 'test-NTdfNDU=', 'test', NULL, NULL, 'video_1604994275_v.mp4', '00:02:30', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 07:44:35', NULL),
(46, 11, 16, 0, 'Kindergarten', 3, 5, 1, '3', 'Test1', 'test1-NTdfNDY=', 'Message description.', 'https://cam-school-staging.s3.amazonaws.com/school/1604994609_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604994609_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com//media_1604994564283_video.mp4', '00:00:03', NULL, 1, 0, 2, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 07:50:16', NULL),
(47, 11, 16, 0, 'Kindergarten', 3, 5, 1, '3', 'Test1', 'test1-NTdfNDc=', 'Message description.', 'https://cam-school-staging.s3.amazonaws.com/school/1604994655_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604994655_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com//media_1604994564283_video.mp4', '00:00:03', NULL, 1, 0, 2, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 07:51:00', NULL),
(48, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'test', 'test-NTdfNDg=', 'test', NULL, NULL, 'video_1604994719_v.mp4', '00:02:30', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 07:52:00', NULL),
(49, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'test', 'test-NTdfNDk=', 'test', NULL, NULL, 'video_1604994725_v.mp4', '00:02:30', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 07:52:05', NULL),
(50, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'test', 'test-NTdfNTA=', 'test', NULL, NULL, 'video_1604994726_v.mp4', '00:02:30', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 07:52:06', NULL),
(51, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'test', 'test-NTdfNTE=', 'test', NULL, NULL, 'video_1604994727_v.mp4', '00:02:30', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 07:52:07', NULL),
(52, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'eee', 'eee-NTdfNTI=', 'eee', NULL, NULL, 'video_1604994836_v.mp4', '00:02:30', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 07:53:56', NULL),
(53, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'eee', 'eee-NTdfNTM=', 'eee', NULL, NULL, 'video_1604994837_v.mp4', '00:02:30', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 07:53:57', NULL),
(54, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'eee', 'eee-NTdfNTQ=', 'eee', NULL, NULL, 'video_1604994838_v.mp4', '00:02:30', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 07:53:58', NULL),
(55, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'eee', 'eee-NTdfNTU=', 'eee', NULL, NULL, 'video_1604994838_v.mp4', '00:02:30', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 07:53:58', NULL),
(56, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'eee', 'eee-NTdfNTY=', 'eee', NULL, NULL, 'video_1604994839_v.mp4', '00:02:30', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 07:53:59', NULL),
(57, 11, 16, 0, 'Kindergarten', 3, 5, 1, '3', 'Test1', 'test1-NTdfNTc=', 'Message description.', 'https://cam-school-staging.s3.amazonaws.com/school/1604995579_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604995579_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com//media_1604994564283_video.mp4', '00:00:03', NULL, 1, 0, 2, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 08:06:23', NULL),
(58, 11, 21, 5, 'Fifth Grade', 3, 5, 1, 'video/mp4', 'test', 'test-NTdfNTg=', 'test', 'https://cam-school-staging.s3.amazonaws.com/00000011/1604999107_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/1604999107_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/video_1604999107_v.mp4', '00:00:05', NULL, 1, 1, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 09:05:15', NULL),
(59, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'testttes', 'testttes-NTdfNTk=', 'tettt', 'https://cam-school-staging.s3.amazonaws.com/00000011/1604999154_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/1604999154_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/video_1604999154_v.mp4', '00:00:05', NULL, 1, 1, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 09:06:02', NULL),
(60, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'testttest', 'testttest-NTdfNjA=', 'tet', 'https://cam-school-staging.s3.amazonaws.com/00000011/1604999772_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/1604999772_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/video_1604999772_v.mp4', '00:02:30', NULL, 1, 11, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 09:17:07', NULL),
(61, 11, 16, 0, 'Kindergarten', 3, 5, 1, '3', 'Test1', 'test1-NTdfNjE=', 'Message description.', 'https://cam-school-staging.s3.amazonaws.com/school/1605003002_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1605003002_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com//media_1604994564283_video.mp4', '00:00:03', NULL, 1, 0, 2, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 10:10:12', NULL),
(62, 88, 72, 1, 'First Grade', 22, 16, 1, '22', 'Gsushsbshd', 'gsushsbshd-NTdfNjI=', 'Ixeigxeivxekbebxobex', 'https://cam-school-staging.s3.amazonaws.com/00000088/1605004369_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000088/1605004369_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/Hiii88/media_1605004300_video.mp4', '00:00:05', NULL, 1, 18, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 10:37:04', NULL),
(63, 88, 72, 1, 'First Grade', 22, 16, 1, '22', 'XVI civic', 'xvi-civic-NTdfNjM=', 'Civici frizzante', 'https://cam-school-staging.s3.amazonaws.com/00000088/1605005019_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000088/1605005019_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/Hiii88/media_1605005013_video.mp4', '00:00:12', NULL, 1, 25, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 10:44:08', NULL),
(64, 88, 74, 4, 'Fourth Grade', 22, 16, 1, '22', 'Clydkyxkyxkyx', 'clydkyxkyxkyx-NTdfNjQ=', 'Lucky dud', 'https://cam-school-staging.s3.amazonaws.com/00000088/1605005092_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000088/1605005092_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/Hiii88/media_1605005088_video.mp4', '00:00:05', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 10:45:46', NULL),
(65, 11, 21, 5, 'Fifth Grade', 3, 5, 1, 'video/mp4', 'tes', 'tes-NTdfNjU=', 'tst', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605006508_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605006508_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/video_1605006508_v.mp4', '00:02:30', NULL, 1, 0, 2, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 11:09:48', NULL),
(66, 88, 72, 1, 'First Grade', 22, 16, 1, '22', 'Gags igfoyyocyod', 'gags-igfoyyocyod-NTdfNjY=', 'Tiutsitsitstiz', 'https://cam-school-staging.s3.amazonaws.com/00000088/1605006779_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000088/1605006779_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/Hiii88/media_1605006776_video.mp4', '00:00:05', NULL, 1, 0, 2, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 11:13:10', NULL),
(67, 88, 72, 1, 'First Grade', 22, 16, 1, '22', 'Ditditsotdylkyx ditditdtid codyid????????????????', 'ditditsotdylkyx-ditditdtid-codyid-NTdfNjc=', '????????????', 'https://cam-school-staging.s3.amazonaws.com/00000088/1605007528_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000088/1605007528_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/Hiii88/media_1605007459_video.mp4', '00:00:09', NULL, 1, 0, 2, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 11:26:57', NULL),
(68, 88, 72, 1, 'First Grade', 22, 16, 1, '22', 'Dneyntej', 'dneyntej-NTdfNjg=', 'Dyndynsynus', 'https://cam-school-staging.s3.amazonaws.com/00000088/1605088936_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000088/1605088936_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/Hiii88/media_1605088933_video.mp4', '00:00:01', NULL, 1, 2, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-11 10:02:32', NULL),
(69, 88, 72, 1, 'First Grade', 22, 16, 1, '22', 'Hello to you', 'hello-to-you-NTdfNjk=', 'Hi am sorry to bother anyone else at', 'https://cam-school-staging.s3.amazonaws.com/00000088/1605094102_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000088/1605094102_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/Hiii88/media_1605094026_video.mp4', '00:00:27', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-11 11:28:49', NULL),
(70, 88, 72, 1, 'First Grade', 22, 16, 1, '22', 'Test 1457', 'test-1457-NTdfNzA=', '25899534fyyhfuf fufucufufugi', 'https://cam-school-staging.s3.amazonaws.com/00000088/1605094301_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000088/1605094301_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/Hiii88/media_1605094278_video.mp4', '00:01:10', NULL, 1, 15, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-11 11:32:05', NULL),
(71, 88, 72, 1, 'First Grade', 22, 16, 1, '22', 'Yeast cookie cutter', 'yeast-cookie-cutter-NTdfNzE=', 'Hello to you your friend you got your email', 'https://cam-school-staging.s3.amazonaws.com/00000088/1605096491_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000088/1605096491_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/Hiii88/media_1605096472_video.mp4', '00:01:20', NULL, 1, 12, 1, 0, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-11 12:08:45', NULL),
(72, 88, 72, 1, 'First Grade', 22, 16, 1, '22', 'Testing your new phone number denotes you guys come back', 'testing-your-new-phone-number-denotes-you-guys-come-back-NTdfNzI=', 'High maintenance', 'https://cam-school-staging.s3.amazonaws.com/00000088/1605162008_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000088/1605162008_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/Hiii88/media_1605161933_video.mp4', '00:00:12', NULL, 1, 0, 2, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-12 06:20:43', NULL),
(73, 61, 62, 2, 'Second Grade', 7, 1, 1, 'video/mp4', 'check', 'check-NTdfNzM=', 'sdasd sddsad sadsadsad', NULL, NULL, 'video_1605167013_v.mp4', '00:00:00', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-12 07:43:33', NULL),
(74, 61, 62, 2, 'Second Grade', 7, 1, 1, 'video/mp4', 'check', 'check-NTdfNzQ=', 'sdasd sddsad sadsadsad', NULL, NULL, 'video_1605167018_v.mp4', '00:00:00', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-12 07:43:38', NULL),
(75, 61, 62, 2, 'Second Grade', 7, 1, 1, 'video/mp4', 'tryhy', 'tryhy-NTdfNzU=', 'ttry', NULL, NULL, 'video_1605167255_v.mp4', '00:00:00', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-12 07:47:35', NULL),
(76, 88, 75, 10, 'Tenth Grade', 22, 16, 1, '22', 'Hiii test', 'hiii-test-NTdfNzY=', 'Hiisvsbs', 'https://cam-school-staging.s3.amazonaws.com/school/1605167908_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1605167908_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com//media_1605167897528_video.mp4', '00:00:06', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-12 07:58:35', NULL),
(77, 88, 73, 3, 'Third Grade', 22, 16, 1, '22', 'Hiiiii update', 'hiiiii-update-NTdfNzc=', 'Description added to the next few days ago', 'https://cam-school-staging.s3.amazonaws.com/school/1605168008_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1605168008_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com//media_1605167997645_video.mp4', '00:00:05', NULL, 1, 1, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-12 08:00:17', NULL),
(78, 88, 72, 1, 'First Grade', 22, 16, 1, '22', 'Doyfkyflycyl', 'doyfkyflycyl-NTdfNzg=', 'Lcylhclyxoyxyofoyxkyxkyxkgxktxkgxkyxkydkyxkyxkyxkyx', 'https://cam-school-staging.s3.amazonaws.com/school/1605168132_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1605168132_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com//media_1605168121635_video.mp4', '00:00:05', NULL, 1, 1, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-12 08:02:20', NULL),
(79, 88, 72, 1, 'First Grade', 22, 16, 1, '22', 'Tedt', 'tedt-NTdfNzk=', 'The next few weeks ago but have r the', 'https://cam-school-staging.s3.amazonaws.com/school/1605168953_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1605168953_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com//media_1605168827756_video.mp4', '00:00:06', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-12 08:16:00', NULL),
(80, 88, 72, 1, 'First Grade', 22, 16, 1, '22', 'Sgbsgbsgbsg', 'sgbsgbsgbsg-NTdfODA=', 'At be afbsggs', 'https://cam-school-staging.s3.amazonaws.com/school/1605169242_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1605169242_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com//media_1605169233607_video.mp4', '00:00:04', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-12 08:20:48', NULL),
(81, 88, 72, 1, 'First Grade', 22, 16, 1, '22', 'Hiiii', 'hiiii-NTdfODE=', 'Hiiiiii', 'https://cam-school-staging.s3.amazonaws.com/school/1605176216_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1605176216_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com//media_1605176205831_video.mp4', '00:00:03', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-12 10:17:05', NULL),
(82, 88, 72, 1, 'First Grade', 22, 16, 1, '22', 'Ddffffx', 'ddffffx-NTdfODI=', 'Erdvdbsbstt', 'https://cam-school-staging.s3.amazonaws.com/school/1605176907_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1605176907_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com//media_1605176888417_video.mp4', '00:00:03', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-12 10:28:33', NULL),
(83, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'my test', 'my-test-NTdfODM=', 'mytest', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605614788_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605614788_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/video_1605614788_v.mp4', '00:00:05', NULL, 1, 2, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-17 12:06:36', NULL),
(84, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'trte', 'trte-NTdfODQ=', 'ert', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605618512_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605618512_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/video_1605618512_v.mp4', '00:00:05', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-17 13:08:41', NULL),
(85, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'trte', 'trte-NTdfODU=', 'ert', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605618538_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605618538_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/video_1605618538_v.mp4', '00:00:05', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-17 13:09:06', NULL),
(86, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'fhfgh', 'fhfgh-NTdfODY=', 'fghfgh', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605618559_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605618559_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/video_1605618559_v.mp4', '00:00:05', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-17 13:09:27', NULL),
(87, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'ss', 'ss-NTdfODc=', 'ss', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605764601_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605764601_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/video_1605764601_v.mp4', '00:00:05', NULL, 1, 8, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-19 05:43:29', NULL),
(88, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'test', 'test-NTdfODg=', 'test', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605860084_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605860084_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/video_1605860084_v.mp4', '00:00:05', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-20 08:14:52', NULL),
(89, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'my video', 'my-video-NTdfODk=', 'my video 2', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605860340_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605860340_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/video_1605860340_v.mp4', '00:00:05', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-20 08:19:08', NULL),
(90, 11, 16, 0, 'Kindergarten', 3, 5, 1, 'video/mp4', 'k video', 'k-video-NTdfOTA=', 'k video', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605860463_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605860463_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/video_1605860463_v.mp4', '00:00:05', NULL, 1, 2, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-20 08:21:12', NULL),
(91, 11, 203, 1, '1st Grade', 3, 5, 1, 'video/mp4', 'testtttttttttttttttttt testttttttttttttttttt', 'testtttttttttttttttttt-testttttttttttttttttt-NTdfOTE=', 'eeeeeeeeeeeeeeeeeeeeeeee er', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605944692_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605944692_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/video_1605944692_v.mp4', '00:00:05', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-21 07:45:00', NULL),
(92, 11, 204, 2, '2nd Grade', 3, 5, 1, 'video/mp4', 'Media is the communication outlets or tools used to store an', 'media-is-the-communication-outlets-or-tools-used-to-store-an-NTdfOTI=', 'Media is the communication outlets or tools used to store and deliver information or data. The term refers to components of the mass media communications industry, such as print media, publishing, the news media, photography, cinema, broadcasting (radio and television), and advertising.', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605959600_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605959600_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/video_1605959600_v.mp4', '00:00:00', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-21 11:53:49', NULL),
(93, 11, 204, 2, '2nd Grade', 3, 5, 1, 'video/mp4', 'Media is the communication outlets or tools used to store an', 'media-is-the-communication-outlets-or-tools-used-to-store-an-NTdfOTM=', 'Media is the communication outlets or tools used to store and deliver information or data. The term refers to components of the mass media communications industry, such as print media, publishing, the news media, photography, cinema, broadcasting (radio and television), and advertising.\r\n\r\n\r\nMedia is the communication outlets or tools used to store and deliver information or data. The term refers to components of the mass media communications industry, such as print media, publishing, the news media, photography, cinema, broadcasting (radio and television), and advertising.', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605960338_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/1605960338_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000011/video_1605960338_v.mp4', '00:00:13', NULL, 1, 1, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-21 12:05:50', NULL),
(94, 193, 242, 0, 'Kindergarten', 79, 19, 1, '79', 'Test video', 'test-video-NTdfOTQ=', 'My videotape shows you', 'https://cam-school-staging.s3.amazonaws.com/00000193/1605964509_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000193/1605964509_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/BharatTea193/media_1605964503_video.mp4', '00:00:11', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-21 13:15:31', NULL),
(95, 193, 244, 2, '2nd Grade', 79, 19, 1, '79', 'Test video 2', 'test-video-2-NTdfOTU=', 'Bharat district officials say hi to you hello your new video', 'https://cam-school-staging.s3.amazonaws.com/00000193/1605964560_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000193/1605964560_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/BharatTea193/media_1605964556_video.mp4', '00:00:07', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-21 13:16:43', NULL),
(96, 193, 247, 9, '9th Grade', 79, 19, 1, '79', 'Test video 3', 'test-video-3-NTdfOTY=', 'Hello to your new followers to you tested positive feedback to', 'https://cam-school-staging.s3.amazonaws.com/00000193/1605964691_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/00000193/1605964691_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/BharatTea193/media_1605964687_video.mp4', '00:00:09', NULL, 1, 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-21 13:19:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `media_block`
--

CREATE TABLE `media_block` (
  `block_id` int(11) NOT NULL,
  `blocked_by` int(11) DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media_deleted`
--

CREATE TABLE `media_deleted` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `outlet_id` int(11) DEFAULT NULL,
  `sub_outlet_id` int(11) DEFAULT NULL,
  `other_sub_outlet` varchar(100) DEFAULT NULL,
  `media_type` tinyint(1) DEFAULT 1 COMMENT '1=Video, 2=Audio',
  `format` varchar(50) DEFAULT NULL,
  `title` varchar(60) DEFAULT NULL,
  `media_slug` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `thumb_image` text DEFAULT NULL,
  `large_image` text DEFAULT NULL,
  `media_name` varchar(100) DEFAULT NULL,
  `duration` time DEFAULT NULL,
  `tags` varchar(50) DEFAULT NULL,
  `stance` tinyint(1) DEFAULT NULL COMMENT '1=For, 2=Against,3=Neutral',
  `status` tinyint(1) DEFAULT 2 COMMENT '1=Active, 2=Deactive,3=Uncomplete',
  `approved_date` datetime DEFAULT '0000-00-00 00:00:00',
  `comments` int(11) DEFAULT 0,
  `created_as` tinyint(1) DEFAULT 1 COMMENT '1=Media, 2 = Comment',
  `views` int(11) DEFAULT 0,
  `likes` int(11) DEFAULT 0,
  `dislikes` int(11) DEFAULT 0,
  `created` datetime DEFAULT '0000-00-00 00:00:00',
  `modified` datetime DEFAULT '0000-00-00 00:00:00',
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `media_report`
--

CREATE TABLE `media_report` (
  `report_id` int(11) NOT NULL,
  `reported_by` int(11) DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `other` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `subject` varchar(100) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `record_id` int(11) NOT NULL,
  `main_media_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `message` mediumtext NOT NULL,
  `to_user` int(11) NOT NULL,
  `read` enum('0','1') NOT NULL DEFAULT '0',
  `created_by` varchar(100) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(4) DEFAULT 0 COMMENT '0-not_deleted,1-deleted',
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_datetime` datetime DEFAULT '0000-00-00 00:00:00',
  `modified_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `record_id`, `main_media_id`, `type`, `message`, `to_user`, `read`, `created_by`, `is_deleted`, `created_datetime`, `deleted_datetime`, `modified_datetime`) VALUES
(1, 10, 0, 'Request', 'School registered in your district. Please check his request.', 9, '0', '10', 0, '2020-11-04 04:52:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 11, 0, 'Request', 'Teacher NewTech registered in your school. Please check his request.', 10, '0', '11', 0, '2020-11-04 05:43:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 12, 0, 'Request', 'Teacher T registered in your school. Please check his request.', 7, '0', '12', 0, '2020-11-04 08:07:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 15, 0, 'Request', 'Teacher T registered in your school. Please check his request.', 7, '0', '15', 0, '2020-11-04 08:55:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 18, 0, 'Request', 'Sdfsdfsdf requested to join you. Please check his request.', 8, '0', '18', 0, '2020-11-04 10:11:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 18, 0, 'Request', 'Sdfsdfsdf requested to join you. Please check his request.', 12, '0', '18', 0, '2020-11-04 10:11:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 18, 0, 'Request', 'Sdfsdfsdf requested to join you. Please check his request.', 15, '1', '18', 0, '2020-11-04 10:11:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 20, 0, 'Request', 'S requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-04 11:44:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 20, 0, 'Request', 'Student requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-04 11:48:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 20, 0, 'Request', 'Ss requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-04 12:07:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 25, 0, 'Request', 'My dummy school registered in your district. Please check his request.', 1, '1', '25', 0, '2020-11-05 07:33:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 26, 0, 'Request', 'My dummy school registered in your district. Please check his request.', 1, '1', '26', 0, '2020-11-05 07:50:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 27, 0, 'Request', 'Dummy school 123 registered in your district. Please check his request.', 1, '1', '27', 0, '2020-11-05 08:04:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 28, 0, 'Request', 'Bharat Rawal requested to join you. Please check his request.', 8, '0', '28', 0, '2020-11-05 11:05:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 28, 0, 'Request', 'Bharat Rawal requested to join you. Please check his request.', 12, '0', '28', 0, '2020-11-05 11:05:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 28, 0, 'Request', 'Bharat Rawal requested to join you. Please check his request.', 15, '1', '28', 0, '2020-11-05 11:05:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 29, 0, 'Request', 'Dyna Lee registered in your school. Please check his request.', 27, '0', '29', 0, '2020-11-05 12:51:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 31, 0, 'Request', 'Bharat school registered in your district. Please check his request.', 30, '0', '31', 0, '2020-11-05 14:42:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 32, 0, 'Request', 'Bharat teacher registered in your school. Please check his request.', 31, '0', '32', 0, '2020-11-05 14:45:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 33, 0, 'Request', 'Test requested to join you. Please check his request.', 32, '0', '33', 0, '2020-11-05 15:05:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 34, 0, 'Request', 'Bharat teacher2 registered in your school. Please check his request.', 31, '0', '34', 0, '2020-11-06 04:59:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 35, 0, 'Request', 'Test tttt registered in your school. Please check his request.', 27, '0', '35', 0, '2020-11-06 05:18:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 36, 0, 'Request', 'Eee rrrr registered in your school. Please check his request.', 27, '0', '36', 0, '2020-11-06 05:34:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 39, 0, 'Request', 'IPS  Acadmy registered in your district. Please check his request.', 1, '1', '39', 0, '2020-11-06 07:46:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 40, 0, 'Request', 'IPS Acadmy registered in your district. Please check his request.', 1, '1', '40', 0, '2020-11-06 07:54:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 41, 0, 'Request', 'New Era public school registered in your district. Please check his request.', 1, '1', '41', 0, '2020-11-06 07:57:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 46, 0, 'Request', 'St. xeviour registered in your district. Please check his request.', 1, '1', '46', 0, '2020-11-06 08:58:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 47, 0, 'Request', 'Teacher II registered in your school. Please check his request.', 27, '0', '47', 0, '2020-11-06 09:45:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 18, 18, 'Like', 'Bharat student liked your video titled \'ho appena completato la missione\'.', 32, '0', '33', 1, '2020-11-06 12:41:55', '2020-11-06 13:19:05', '0000-00-00 00:00:00'),
(30, 33, 0, 'Request', 'Bharat Rawal Rawal requested to join you. Please check his request.', 32, '0', '33', 0, '2020-11-06 12:42:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 33, 0, 'Request', 'Bharat b requested to join you. Please check his request.', 32, '0', '33', 0, '2020-11-06 12:42:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 1, 1, 'Comment', 'Denmark Lee commented on your video titled \'Testing1\'.', 11, '1', '4', 0, '2020-11-06 12:57:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 2, 1, 'Comment', 'Denmark Lee commented on your video titled \'Testing1\'.', 11, '1', '4', 0, '2020-11-06 12:57:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 4, 17, 'Comment', 'Denmark Lee commented on your video titled \'bsbsbs dB\'.', 32, '0', '4', 0, '2020-11-06 13:04:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 5, 17, 'Comment', 'Denmark Lee commented on your video titled \'bsbsbs dB\'.', 32, '0', '4', 0, '2020-11-06 13:07:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 6, 6, 'Comment', 'Student Dlast commented on your video titled \'Vid2\'.', 11, '1', '20', 0, '2020-11-07 05:42:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 7, 6, 'Comment', 'Student Dlast commented on your video titled \'Vid2\'.', 11, '1', '20', 0, '2020-11-07 05:42:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 8, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 05:43:49', '2020-11-07 12:36:56', '0000-00-00 00:00:00'),
(39, 6, 6, 'Comment like', 'Teacher liked your comment on video titled \'Vid2\'.', 20, '1', '11', 0, '2020-11-07 05:43:53', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 9, 6, 'Comment', 'Teacher replied to your comment on video titled \'Vid2\'.', 20, '1', '11', 0, '2020-11-07 05:44:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 10, 6, 'Comment', 'Teacher replied to your comment on video titled \'Vid2\'.', 20, '1', '11', 0, '2020-11-07 05:44:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 11, 6, 'Comment', 'Teacher replied to your comment on video titled \'Vid2\'.', 20, '1', '11', 0, '2020-11-07 05:44:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 12, 6, 'Comment', 'Student Dlast replied to Teacher\'s comment on your video titled \'Vid2\'.', 11, '1', '20', 0, '2020-11-07 05:45:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 12, 6, 'Comment', 'Student Dlast replied to your comment on video titled \'Vid2\'.', 11, '1', '20', 0, '2020-11-07 05:45:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 14, 6, 'Comment', 'Teacher replied to your comment on video titled \'Vid2\'.', 20, '1', '11', 0, '2020-11-07 05:47:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 56, 0, 'Request', 'Check teacher registered in your school. Please check his request.', 27, '0', '56', 0, '2020-11-07 05:48:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 15, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 05:49:31', '2020-11-07 09:44:09', '0000-00-00 00:00:00'),
(48, 16, 6, 'Comment', 'Student Dlast replied to Teacher\'s comment on your video titled \'Vid2\'.', 11, '1', '20', 0, '2020-11-07 05:49:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 16, 6, 'Comment', 'Student Dlast replied to your comment on video titled \'Vid2\'.', 11, '1', '20', 0, '2020-11-07 05:49:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 16, 6, 'Comment like', 'Teacher liked your comment on video titled \'Vid2\'.', 20, '1', '11', 0, '2020-11-07 05:50:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 17, 6, 'Comment', 'Teacher replied to your comment on video titled \'Vid2\'.', 20, '1', '11', 0, '2020-11-07 05:50:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 57, 0, 'Request', 'Schoole1 registered in your district. Please check his request.', 30, '0', '57', 0, '2020-11-07 06:02:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 0, 0, 'new_group', 'you have been added to  group testChatGroup1', 1, '1', '22', 0, '2020-11-07 11:52:40', '2020-11-07 11:52:40', '2020-11-07 11:52:40'),
(54, 0, 0, 'new_group', 'you have been added to  group testChatGroup1', 2, '0', '22', 0, '2020-11-07 11:52:40', '2020-11-07 11:52:40', '2020-11-07 11:52:40'),
(55, 0, 0, 'new_group', 'you have been added to  group testChatGroup1', 3, '0', '22', 0, '2020-11-07 11:52:40', '2020-11-07 11:52:40', '2020-11-07 11:52:40'),
(56, 1, 1, 'Comment like', 'Student Dlast liked your comment on video titled \'Testing1\'.', 4, '0', '20', 0, '2020-11-07 09:11:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 15, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 09:26:48', '2020-11-07 09:44:09', '0000-00-00 00:00:00'),
(58, 15, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 09:26:53', '2020-11-07 09:44:09', '0000-00-00 00:00:00'),
(59, 15, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 09:26:59', '2020-11-07 09:44:09', '0000-00-00 00:00:00'),
(60, 15, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 09:27:02', '2020-11-07 09:44:09', '0000-00-00 00:00:00'),
(61, 15, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 09:27:07', '2020-11-07 09:44:09', '0000-00-00 00:00:00'),
(62, 15, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 09:27:14', '2020-11-07 09:44:09', '0000-00-00 00:00:00'),
(63, 15, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 09:27:18', '2020-11-07 09:44:09', '0000-00-00 00:00:00'),
(64, 15, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 09:29:09', '2020-11-07 09:44:09', '0000-00-00 00:00:00'),
(65, 15, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 09:30:15', '2020-11-07 09:44:09', '0000-00-00 00:00:00'),
(66, 10, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 09:32:17', '2020-11-07 09:49:07', '0000-00-00 00:00:00'),
(67, 14, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 09:32:26', '2020-11-07 09:49:42', '0000-00-00 00:00:00'),
(68, 11, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 09:34:28', '2020-11-07 09:50:01', '0000-00-00 00:00:00'),
(69, 18, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 09:43:43', '2020-11-07 09:49:50', '0000-00-00 00:00:00'),
(70, 10, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 09:43:45', '2020-11-07 09:49:07', '0000-00-00 00:00:00'),
(71, 8, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 09:43:50', '2020-11-07 12:36:56', '0000-00-00 00:00:00'),
(72, 8, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 09:43:54', '2020-11-07 12:36:56', '0000-00-00 00:00:00'),
(73, 15, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 09:43:58', '2020-11-07 09:44:09', '0000-00-00 00:00:00'),
(74, 11, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 09:48:58', '2020-11-07 09:50:01', '0000-00-00 00:00:00'),
(75, 11, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 09:49:35', '2020-11-07 09:50:01', '0000-00-00 00:00:00'),
(76, 10, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 0, '2020-11-07 09:49:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 18, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 0, '2020-11-07 09:49:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 11, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 09:49:58', '2020-11-07 09:50:01', '0000-00-00 00:00:00'),
(79, 11, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 0, '2020-11-07 09:50:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 0, 0, 'new_group', 'you have been added to  group Hhhgh', 20, '1', '11', 0, '2020-11-07 15:24:52', '2020-11-07 15:24:52', '2020-11-07 15:24:52'),
(81, 0, 0, 'new_group', 'you have been added to  group Hhhgh', 11, '1', '11', 0, '2020-11-07 15:24:52', '2020-11-07 15:24:52', '2020-11-07 15:24:52'),
(82, 64, 0, 'Request', 'Deny Parkar1 requested to join you. Please check his request.', 6, '0', '64', 0, '2020-11-07 10:21:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 64, 0, 'Request', 'Deny Parkar1 requested to join you. Please check his request.', 11, '1', '64', 0, '2020-11-07 10:21:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 64, 0, 'Request', 'Deny Parkar2 requested to join you. Please check his request.', 6, '0', '64', 0, '2020-11-07 10:21:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 64, 0, 'Request', 'Deny Parkar2 requested to join you. Please check his request.', 11, '1', '64', 0, '2020-11-07 10:21:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 64, 0, 'Request', 'Deny Parkar3 requested to join you. Please check his request.', 62, '1', '64', 0, '2020-11-07 10:26:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 0, 0, 'new_group', 'you have been added to  group hellodes', 1, '1', '22', 0, '2020-11-07 16:03:34', '2020-11-07 16:03:34', '2020-11-07 16:03:34'),
(88, 0, 0, 'new_group', 'you have been added to  group hellodes', 2, '0', '22', 0, '2020-11-07 16:03:34', '2020-11-07 16:03:34', '2020-11-07 16:03:34'),
(89, 0, 0, 'new_group', 'you have been added to  group hellodes', 3, '0', '22', 0, '2020-11-07 16:03:34', '2020-11-07 16:03:34', '2020-11-07 16:03:34'),
(90, 8, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 11:44:09', '2020-11-07 12:36:56', '0000-00-00 00:00:00'),
(91, 8, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 11:44:15', '2020-11-07 12:36:56', '0000-00-00 00:00:00'),
(92, 8, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 11:44:18', '2020-11-07 12:36:56', '0000-00-00 00:00:00'),
(93, 8, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 11:44:22', '2020-11-07 12:36:56', '0000-00-00 00:00:00'),
(94, 8, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 12:06:44', '2020-11-07 12:36:56', '0000-00-00 00:00:00'),
(95, 8, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 12:06:48', '2020-11-07 12:36:56', '0000-00-00 00:00:00'),
(96, 8, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 12:06:51', '2020-11-07 12:36:56', '0000-00-00 00:00:00'),
(97, 0, 0, 'new_group', 'you have been added to  group New Grp', 20, '1', '11', 0, '2020-11-07 18:00:55', '2020-11-07 18:00:55', '2020-11-07 18:00:55'),
(98, 8, 6, 'Comment like', 'Student Dlast liked your comment on video titled \'Vid2\'.', 11, '1', '20', 1, '2020-11-07 12:36:54', '2020-11-07 12:36:56', '0000-00-00 00:00:00'),
(99, 65, 0, 'Request', 'Primary school registered in your district. Please check his request.', 3, '0', '65', 0, '2020-11-08 05:55:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 64, 0, 'Request', 'Deny Parkar3 requested to join you. Please check his request.', 62, '1', '64', 0, '2020-11-08 06:36:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 64, 0, 'Request', 'Dummy user requested to join you. Please check his request.', 36, '0', '64', 0, '2020-11-08 07:18:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 64, 0, 'Request', 'Dummy user requested to join you. Please check his request.', 47, '0', '64', 0, '2020-11-08 07:18:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 64, 0, 'Request', 'Dummy user requested to join you. Please check his request.', 36, '0', '64', 0, '2020-11-08 07:19:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 64, 0, 'Request', 'Dummy user requested to join you. Please check his request.', 47, '0', '64', 0, '2020-11-08 07:19:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 64, 0, 'Request', 'Dummy user requested to join you. Please check his request.', 62, '1', '64', 0, '2020-11-08 07:19:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 64, 0, 'Request', 'Dummy user requested to join you. Please check his request.', 36, '0', '64', 0, '2020-11-08 07:20:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 64, 0, 'Request', 'Dummy user requested to join you. Please check his request.', 47, '0', '64', 0, '2020-11-08 07:20:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 64, 0, 'Request', 'Dummy user requested to join you. Please check his request.', 62, '1', '64', 0, '2020-11-08 07:20:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 64, 0, 'Request', 'Vijendra requested to join you. Please check his request.', 29, '0', '64', 0, '2020-11-08 07:23:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, 64, 0, 'Request', 'Vijendra requested to join you. Please check his request.', 62, '1', '64', 0, '2020-11-08 07:23:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, 1, 1, 'Like', 'Newstudent1 student145 liked your video titled \'Testing1\'.', 11, '1', '64', 1, '2020-11-08 08:40:21', '2020-11-08 11:44:26', '0000-00-00 00:00:00'),
(112, 1, 1, 'Like', 'Newstudent1 student145 liked your video titled \'Testing1\'.', 11, '1', '64', 1, '2020-11-08 08:44:29', '2020-11-08 11:44:26', '0000-00-00 00:00:00'),
(113, 1, 1, 'Like', 'Newstudent1 student145 liked your video titled \'Testing1\'.', 11, '1', '64', 1, '2020-11-08 08:44:49', '2020-11-08 11:44:26', '0000-00-00 00:00:00'),
(114, 1, 1, 'Like', 'Newstudent1 student145 liked your video titled \'Testing1\'.', 11, '1', '64', 1, '2020-11-08 11:41:57', '2020-11-08 11:44:26', '0000-00-00 00:00:00'),
(115, 1, 1, 'Like', 'Newstudent1 student145 liked your video titled \'Testing1\'.', 11, '1', '64', 1, '2020-11-08 11:42:02', '2020-11-08 11:44:26', '0000-00-00 00:00:00'),
(116, 1, 1, 'Like', 'Newstudent1 student145 liked your video titled \'Testing1\'.', 11, '1', '64', 0, '2020-11-08 11:44:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, 66, 0, 'Request', 'Bings convent registered in your district. Please check his request.', 14, '0', '66', 0, '2020-11-08 11:48:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, 64, 0, 'Request', 'Vishal requested to join you. Please check his request.', 62, '1', '64', 0, '2020-11-08 12:01:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, 64, 0, 'Request', 'Dummy user requested to join you. Please check his request.', 62, '1', '64', 0, '2020-11-08 12:02:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, 67, 0, 'Request', 'Test user registered in your district. Please check his request.', 1, '1', '67', 0, '2020-11-08 13:28:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 62, '1', '64', 0, '2020-11-09 05:08:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 36, '0', '64', 0, '2020-11-09 05:12:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, 64, 0, 'Request', 'Vijendra requested to join you. Please check his request.', 35, '0', '64', 0, '2020-11-09 05:13:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, 64, 0, 'Request', 'Kljkl requested to join you. Please check his request.', 35, '0', '64', 0, '2020-11-09 05:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(125, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 36, '0', '64', 0, '2020-11-09 05:17:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, 64, 0, 'Request', 'Lk;\'kllk requested to join you. Please check his request.', 35, '0', '64', 0, '2020-11-09 05:17:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, 70, 0, 'Request', 'Bharat School registered in your district. Please check his request.', 69, '0', '70', 0, '2020-11-09 05:27:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(128, 71, 0, 'Request', 'Bharat School 123 registered in your district. Please check his request.', 69, '0', '71', 0, '2020-11-09 05:28:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(129, 72, 0, 'Request', 'Bharat TeacherT registered in your school. Please check his request.', 71, '0', '72', 0, '2020-11-09 05:34:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(130, 73, 0, 'Request', 'Bhart requested to join you. Please check his request.', 72, '1', '73', 0, '2020-11-09 06:02:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(131, 23, 30, 'Comment', 'Bharat Student commented on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-09 06:22:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(132, 24, 30, 'Comment', 'Bharat Student commented on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-09 06:22:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(133, 25, 30, 'Comment', 'Bharat Student replied to Bharat Student\'s comment on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-09 06:22:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(134, 26, 30, 'Comment', 'Bharat Student commented on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-09 06:27:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(135, 27, 30, 'Comment', 'Bharat Student replied to Bharat Student\'s comment on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-09 06:36:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(136, 0, 0, 'new_group', 'you have been added to  group Grp1', 20, '1', '11', 0, '2020-11-09 15:06:33', '2020-11-09 15:06:33', '2020-11-09 15:06:33'),
(137, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 62, '1', '64', 0, '2020-11-09 10:07:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(138, 64, 0, 'Request', 'Dummy user requested to join you. Please check his request.', 47, '0', '64', 0, '2020-11-09 10:11:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(139, 6, 6, 'Like', 'Student Dlast liked your video titled \'Vid2\'.', 11, '1', '20', 0, '2020-11-09 10:11:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(140, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 35, '0', '64', 0, '2020-11-09 10:13:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(141, 64, 0, 'Request', 'Dummy user requested to join you. Please check his request.', 36, '0', '64', 0, '2020-11-09 10:14:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(142, 64, 0, 'Request', 'Dummy user requested to join you. Please check his request.', 35, '0', '64', 0, '2020-11-09 10:15:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(143, 64, 0, 'Request', 'Dummy user requested to join you. Please check his request.', 35, '0', '64', 0, '2020-11-09 10:15:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(144, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 35, '0', '64', 0, '2020-11-09 10:15:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(145, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 35, '0', '64', 0, '2020-11-09 10:17:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(146, 64, 0, 'Request', 'Dummy user requested to join you. Please check his request.', 47, '0', '64', 0, '2020-11-09 10:21:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(147, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 35, '0', '64', 0, '2020-11-09 10:21:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(148, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 35, '0', '64', 0, '2020-11-09 10:23:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(149, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 35, '0', '64', 0, '2020-11-09 10:23:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(150, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 62, '1', '64', 0, '2020-11-09 10:24:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(151, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 62, '1', '64', 0, '2020-11-09 10:25:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(152, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 62, '1', '64', 0, '2020-11-09 10:26:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(153, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 35, '0', '64', 0, '2020-11-09 10:26:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(154, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 35, '0', '64', 0, '2020-11-09 10:28:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(155, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 47, '0', '64', 0, '2020-11-09 10:35:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(156, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 58, '0', '64', 0, '2020-11-09 10:35:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(157, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 62, '1', '64', 0, '2020-11-09 10:35:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(158, 64, 0, 'Request', 'Demo demo requested to join you. Please check his request.', 61, '1', '64', 0, '2020-11-09 10:36:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(159, 64, 0, 'Request', 'Demo demo requested to join you. Please check his request.', 62, '1', '64', 0, '2020-11-09 10:36:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(160, 64, 0, 'Request', 'Dummy user requested to join you. Please check his request.', 35, '0', '64', 0, '2020-11-09 11:20:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(161, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 62, '1', '64', 0, '2020-11-09 11:24:56', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(162, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 35, '0', '64', 0, '2020-11-09 11:27:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(163, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 35, '0', '64', 0, '2020-11-09 11:27:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(164, 64, 0, 'Request', 'Dummy user requested to join you. Please check his request.', 35, '0', '64', 0, '2020-11-09 11:29:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(165, 64, 0, 'Request', 'Dummy user requested to join you. Please check his request.', 47, '0', '64', 0, '2020-11-09 11:29:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(166, 64, 0, 'Request', 'Dummy user requested to join you. Please check his request.', 59, '0', '64', 0, '2020-11-09 11:29:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(167, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 47, '0', '64', 0, '2020-11-09 11:29:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(168, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 35, '0', '64', 0, '2020-11-09 11:30:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(169, 64, 0, 'Request', 'Accele requested to join you. Please check his request.', 35, '0', '64', 0, '2020-11-09 11:31:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(170, 64, 0, 'Request', 'Dummy user requested to join you. Please check his request.', 36, '0', '64', 0, '2020-11-09 11:31:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(171, 11, 0, 'Request Approved', 'Student \'s request approved by Teacher for First Grade', 20, '1', '11', 0, '2020-11-09 11:31:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(172, 11, 0, 'Request Approved', 'Ss \'s request approved by Teacher for Kindergarten', 20, '1', '11', 0, '2020-11-09 11:31:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(173, 64, 0, 'Request', 'DEMO DEMO  requested to join you. Please check his request.', 35, '0', '64', 0, '2020-11-09 11:33:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(174, 64, 0, 'Request', 'DEMO DEMO  requested to join you. Please check his request.', 36, '0', '64', 0, '2020-11-09 11:33:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(175, 64, 0, 'Request', 'DEMO DEMO  requested to join you. Please check his request.', 62, '1', '64', 0, '2020-11-09 11:33:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(176, 62, 0, 'Request Approved', 'Deny Parkar3 \'s request approved by sdghj kddddd1234 for First Grade', 64, '1', '62', 0, '2020-11-09 11:35:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(177, 62, 0, 'Request Approved', 'Deny Parkar3 \'s request approved by sdghj kddddd1234 for First Grade', 64, '1', '62', 0, '2020-11-09 11:35:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(178, 62, 0, 'Request Approved', 'Deny Parkar3 \'s request approved by sdghj kddddd1234 for Second Grade', 64, '1', '62', 0, '2020-11-09 11:35:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(179, 62, 0, 'Request Approved', 'Deny Parkar3 \'s request approved by sdghj kddddd1234 for Second Grade', 64, '1', '62', 0, '2020-11-09 11:35:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(180, 62, 0, 'Request Approved', 'Deny Parkar3 \'s request approved by sdghj kddddd1234 for First Grade', 64, '1', '62', 0, '2020-11-09 11:35:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(181, 62, 0, 'Request Approved', 'Deny Parkar3 \'s request approved by sdghj kddddd1234 for Second Grade', 64, '1', '62', 0, '2020-11-09 11:35:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(182, 62, 0, 'Request Approved', 'Dummy user \'s request approved by sdghj kddddd1234 for Fourth Grade', 64, '1', '62', 0, '2020-11-09 11:35:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(183, 62, 0, 'Request Approved', 'Deny Parkar3 \'s request approved by sdghj kddddd1234 for First Grade', 64, '1', '62', 0, '2020-11-09 11:35:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(184, 62, 0, 'Request Approved', 'Deny Parkar3 \'s request approved by sdghj kddddd1234 for First Grade', 64, '1', '62', 0, '2020-11-09 11:35:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(185, 62, 0, 'Request Approved', 'Deny Parkar3 \'s request approved by sdghj kddddd1234 for First Grade', 64, '1', '62', 0, '2020-11-09 11:35:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(186, 62, 0, 'Request Approved', 'Deny Parkar3 \'s request approved by sdghj kddddd1234 for Second Grade', 64, '1', '62', 0, '2020-11-09 11:35:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(187, 62, 0, 'Request Approved', 'Dummy user \'s request approved by sdghj kddddd1234 for Fourth Grade', 64, '1', '62', 0, '2020-11-09 11:35:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(188, 62, 0, 'Request Approved', 'vishal \'s request approved by sdghj kddddd1234 for Fourth Grade', 64, '1', '62', 0, '2020-11-09 11:35:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(189, 62, 0, 'Request Approved', 'vijendra \'s request approved by sdghj kddddd1234 for Fourth Grade', 64, '1', '62', 0, '2020-11-09 11:35:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(190, 62, 0, 'Request Approved', 'vijendra \'s request approved by sdghj kddddd1234 for Second Grade', 64, '1', '62', 0, '2020-11-09 11:35:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(191, 62, 0, 'Request Approved', 'Dummy user \'s request approved by sdghj kddddd1234 for Fourth Grade', 64, '1', '62', 0, '2020-11-09 11:35:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(192, 72, 0, 'Request Approved', 'Bhart \'s request approved by Rawal Display Name for Tenth Grade', 73, '0', '72', 0, '2020-11-09 11:35:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(193, 72, 0, 'Request Approved', 'Bhart \'s request approved by Rawal Display Name for First Grade', 73, '0', '72', 0, '2020-11-09 11:35:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(194, 0, 0, 'new_group', 'you have been added to  group Grp2', 20, '1', '11', 0, '2020-11-09 17:14:26', '2020-11-09 17:14:26', '2020-11-09 17:14:26'),
(195, 0, 0, 'new_group', 'you have been added to  group Grp3', 20, '1', '11', 0, '2020-11-09 17:17:51', '2020-11-09 17:17:51', '2020-11-09 17:17:51'),
(196, 64, 0, 'Request', 'Dummy user requested to join you. Please check his request.', 47, '0', '64', 0, '2020-11-09 11:52:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(197, 20, 0, 'Request', 'New student requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-09 11:58:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(198, 74, 0, 'Request', 'Asd requested to join you. Please check his request.', 11, '1', '74', 0, '2020-11-09 12:07:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(199, 20, 6, 'Comment like', 'DisName liked your comment on video titled \'Vid2\'.', 11, '1', '10', 1, '2020-11-09 12:07:09', '2020-11-09 12:07:10', '0000-00-00 00:00:00'),
(200, 20, 6, 'Comment like', 'DisName liked your comment on video titled \'Vid2\'.', 11, '1', '10', 1, '2020-11-09 12:07:10', '2020-11-09 12:07:10', '0000-00-00 00:00:00'),
(201, 20, 6, 'Comment like', 'DisName liked your comment on video titled \'Vid2\'.', 11, '1', '10', 0, '2020-11-09 12:07:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(202, 9, 6, 'Comment like', 'DisName liked your comment on video titled \'Vid2\'.', 11, '1', '10', 1, '2020-11-09 12:07:12', '2020-11-09 12:07:12', '0000-00-00 00:00:00'),
(203, 9, 6, 'Comment like', 'DisName liked your comment on video titled \'Vid2\'.', 11, '1', '10', 1, '2020-11-09 12:07:12', '2020-11-09 12:07:12', '0000-00-00 00:00:00'),
(204, 9, 6, 'Comment like', 'DisName liked your comment on video titled \'Vid2\'.', 11, '1', '10', 0, '2020-11-09 12:07:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(205, 0, 0, 'new_group', 'you have been added to  group Grp5', 20, '1', '11', 0, '2020-11-09 17:37:54', '2020-11-09 17:37:54', '2020-11-09 17:37:54'),
(206, 0, 0, 'new_group', 'you have been added to  group Grp5', 74, '0', '11', 0, '2020-11-09 17:37:54', '2020-11-09 17:37:54', '2020-11-09 17:37:54'),
(207, 56, 6, 'Comment like', 'DisName liked your comment on video titled \'Vid2\'.', 11, '1', '10', 1, '2020-11-09 12:14:02', '2020-11-09 12:14:03', '0000-00-00 00:00:00'),
(208, 73, 0, 'Request', 'Dsfsdfsdf dsfsdfsdf dsfsdf requested to join you. Please check his request.', 72, '1', '73', 0, '2020-11-09 12:34:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(209, 0, 0, 'new_group', 'you have been added to  group Thdnd', 20, '1', '11', 0, '2020-11-09 18:11:22', '2020-11-09 18:11:22', '2020-11-09 18:11:22'),
(210, 0, 0, 'new_group', 'you have been added to  group Thdnd', 74, '0', '11', 0, '2020-11-09 18:11:22', '2020-11-09 18:11:22', '2020-11-09 18:11:22'),
(211, 75, 0, 'Request', 'Bharat rawal rawal requested to join you. Please check his request.', 72, '1', '75', 0, '2020-11-09 12:47:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(212, 76, 0, 'Request', 'Bhart School New123 registered in your district. Please check his request.', 69, '0', '76', 0, '2020-11-09 12:50:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(213, 77, 0, 'Request', 'PRe primery registered in your district. Please check his request.', 3, '0', '77', 0, '2020-11-09 13:18:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(214, 78, 0, 'Request', 'Juniour KG registered in your district. Please check his request.', 1, '1', '78', 0, '2020-11-09 13:20:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(215, 11, 0, 'Request Approved', 'New student \'s request approved by Teacher for Second Grade', 20, '1', '11', 0, '2020-11-09 13:39:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(216, 11, 0, 'Request Approved', 'New student \'s request approved by Teacher for First Grade', 20, '1', '11', 0, '2020-11-09 13:39:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(217, 11, 0, 'Request Approved', 'Asd \'s request approved by Teacher for First Grade', 74, '0', '11', 0, '2020-11-09 13:39:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(218, 11, 0, 'Request Approved', 'Asd \'s request approved by Teacher for Kindergarten', 74, '0', '11', 0, '2020-11-09 13:39:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(219, 79, 0, 'Request', 'TeacherII teacher III registered in your school. Please check his request.', 78, '0', '79', 0, '2020-11-09 13:44:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(220, 65, 30, 'Comment', 'Bharat Student replied to Bharat Student\'s comment on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 05:05:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(221, 66, 30, 'Comment', 'Bharat Student replied to Bharat Student\'s comment on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 05:05:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(222, 67, 30, 'Comment', 'Bharat Student replied to Bharat Student\'s comment on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 05:05:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(223, 68, 30, 'Comment', 'Bharat Student commented on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 05:06:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(224, 69, 30, 'Comment', 'Bharat Student commented on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 05:06:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(225, 83, 9, 'Comment', 'Bharat New commented on your video titled \'second video\'.', 15, '0', '75', 0, '2020-11-10 07:54:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(226, 84, 9, 'Comment', 'Bharat New commented on your video titled \'second video\'.', 15, '0', '75', 0, '2020-11-10 07:54:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(227, 85, 30, 'Comment', 'Bharat Student replied to Bharat Student\'s comment on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 07:55:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(228, 86, 30, 'Comment', 'Bharat Student commented on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 07:56:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(229, 87, 30, 'Comment', 'Bharat Student commented on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 07:58:56', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(230, 88, 30, 'Comment', 'Bharat Student commented on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 08:00:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(231, 89, 30, 'Comment', 'Bharat Student commented on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 08:03:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(232, 90, 30, 'Comment', 'Bharat Student replied to Bharat Student\'s comment on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 08:04:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(233, 91, 30, 'Comment', 'Bharat Student replied to Bharat Student\'s comment on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 08:04:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(234, 93, 30, 'Comment', 'Bharat Student commented on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 08:07:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(235, 94, 30, 'Comment', 'Bharat Student commented on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 08:17:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(236, 95, 30, 'Comment', 'Bharat Student commented on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 08:18:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(237, 96, 30, 'Comment', 'Bharat Student commented on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 08:24:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(238, 81, 0, 'Request', 'Hello requested to join you. Please check his request.', 72, '1', '81', 0, '2020-11-10 09:05:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(239, 97, 9, 'Comment', 'Bharat New commented on your video titled \'second video\'.', 15, '0', '75', 0, '2020-11-10 09:19:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(240, 98, 30, 'Comment', 'Bharat Student commented on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 09:21:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(241, 99, 30, 'Comment', 'Bharat Student commented on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 09:21:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(242, 100, 30, 'Comment', 'Bharat Student commented on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 09:22:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(243, 101, 30, 'Comment', 'Bharat Student commented on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 09:23:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(244, 102, 30, 'Comment', 'Bharat Student commented on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 09:24:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(245, 103, 30, 'Comment', 'Bharat Student commented on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 09:26:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(246, 104, 30, 'Comment', 'Bharat Student commented on your video titled \'Dum just Video\'.', 72, '1', '73', 0, '2020-11-10 09:26:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(247, 83, 0, 'Request', 'Bharat new school 1 registered in your district. Please check his request.', 82, '0', '83', 0, '2020-11-10 09:39:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(248, 84, 0, 'Request', 'Bharat school 2 registered in your district. Please check his request.', 82, '0', '84', 0, '2020-11-10 09:43:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(249, 85, 0, 'Request', 'Bharat schofjfifificuc registered in your district. Please check his request.', 82, '0', '85', 0, '2020-11-10 09:52:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(250, 86, 0, 'Request', 'Cuccoycyocu registered in your district. Please check his request.', 82, '0', '86', 0, '2020-11-10 09:55:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(251, 87, 0, 'Request', 'Cuccoycyocufufufu registered in your district. Please check his request.', 82, '0', '87', 0, '2020-11-10 09:58:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(252, 88, 0, 'Request', 'Bharat Teach teach registered in your school. Please check his request.', 83, '0', '88', 0, '2020-11-10 10:06:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(253, 90, 0, 'Request', 'Vuvuvuv Vivuvu registered in your school. Please check his request.', 83, '0', '90', 0, '2020-11-10 10:14:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(254, 91, 0, 'Request', 'Hiii requested to join you. Please check his request.', 88, '1', '91', 0, '2020-11-10 10:19:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(255, 62, 0, 'Request Approved', 'demo demo \'s request approved by sdghj kddddd1234 for Fourth Grade', 64, '0', '62', 0, '2020-11-10 10:28:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(256, 62, 0, 'Request Approved', 'accele \'s request approved by sdghj kddddd1234 for Fourth Grade', 64, '0', '62', 0, '2020-11-10 10:28:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(257, 62, 0, 'Request Approved', 'accele \'s request approved by sdghj kddddd1234 for Fourth Grade', 64, '0', '62', 0, '2020-11-10 10:28:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(258, 62, 0, 'Request Approved', 'accele \'s request approved by sdghj kddddd1234 for Fourth Grade', 64, '0', '62', 0, '2020-11-10 10:28:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(259, 62, 0, 'Request Approved', 'accele \'s request approved by sdghj kddddd1234 for Fourth Grade', 64, '0', '62', 0, '2020-11-10 10:28:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(260, 42, 42, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-10 10:28:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(261, 42, 42, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-10 10:28:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(262, 43, 43, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-10 10:28:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(263, 43, 43, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-10 10:28:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(264, 44, 44, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-10 10:28:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(265, 44, 44, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-10 10:28:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(266, 45, 45, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-10 10:28:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(267, 45, 45, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-10 10:28:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(268, 72, 0, 'Request Approved', 'Dsfsdfsdf dsfsdfsdf dsfsdf \'s request approved by Rawal Display Name for First Grade', 73, '0', '72', 0, '2020-11-10 10:28:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(269, 48, 48, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-10 10:28:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(270, 48, 48, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-10 10:28:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(271, 49, 49, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-10 10:28:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(272, 49, 49, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-10 10:28:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(273, 50, 50, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-10 10:28:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(274, 50, 50, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-10 10:28:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(275, 51, 51, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-10 10:28:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(276, 51, 51, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-10 10:28:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(277, 52, 52, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-10 10:28:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(278, 52, 52, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-10 10:28:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(279, 53, 53, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-10 10:28:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(280, 53, 53, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-10 10:28:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(281, 54, 54, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-10 10:28:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(282, 54, 54, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-10 10:28:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(283, 55, 55, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-10 10:28:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(284, 55, 55, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-10 10:28:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `notification` (`id`, `record_id`, `main_media_id`, `type`, `message`, `to_user`, `read`, `created_by`, `is_deleted`, `created_datetime`, `deleted_datetime`, `modified_datetime`) VALUES
(285, 56, 56, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-10 10:28:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(286, 56, 56, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-10 10:28:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(287, 59, 59, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-10 10:28:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(288, 59, 59, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-10 10:28:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(289, 72, 0, 'Request Approved', 'Hello \'s request approved by Rawal Display Name for First Grade', 81, '1', '72', 0, '2020-11-10 10:28:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(290, 60, 60, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-10 10:28:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(291, 60, 60, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-10 10:28:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(292, 88, 0, 'Request Approved', 'Hiii \'s request approved by Hiii for Third Grade', 91, '1', '88', 0, '2020-11-10 10:28:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(293, 88, 0, 'Request Approved', 'Hiii \'s request approved by Hiii for First Grade', 91, '1', '88', 0, '2020-11-10 10:28:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(294, 62, 62, 'Teacher Upload Media', 'Hiii uploaded new video for First Grade', 91, '1', '88', 0, '2020-11-10 10:45:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(295, 63, 63, 'Teacher Upload Media', 'Hiii uploaded new video for First Grade', 91, '1', '88', 0, '2020-11-10 10:45:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(296, 106, 63, 'Comment', 'Bharat STD commented on your video titled \'XVI civic\'.', 88, '1', '91', 0, '2020-11-10 10:46:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(297, 20, 0, 'Request', 'Krish requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-10 11:00:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(298, 107, 63, 'Comment', 'Bharat STD replied to Bharat STD\'s comment on your video titled \'XVI civic\'.', 88, '1', '91', 0, '2020-11-10 11:01:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(299, 109, 63, 'Comment', 'Hiii replied to your comment on video titled \'XVI civic\'.', 91, '1', '88', 0, '2020-11-10 11:11:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(300, 110, 63, 'Comment', 'Hiii replied to your comment on video titled \'XVI civic\'.', 91, '1', '88', 0, '2020-11-10 11:12:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(301, 111, 63, 'Comment', 'Hiii replied to your comment on video titled \'XVI civic\'.', 91, '1', '88', 0, '2020-11-10 11:12:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(302, 106, 63, 'Comment like', 'Hiii liked your comment on video titled \'XVI civic\'.', 91, '1', '88', 1, '2020-11-10 11:14:08', '2020-11-10 12:18:18', '0000-00-00 00:00:00'),
(303, 117, 63, 'Comment', 'Bharat STD commented on your video titled \'XVI civic\'.', 88, '1', '91', 0, '2020-11-10 11:18:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(304, 117, 63, 'Comment like', 'Hiii liked your comment on video titled \'XVI civic\'.', 91, '1', '88', 1, '2020-11-10 11:19:23', '2020-11-10 11:23:14', '0000-00-00 00:00:00'),
(305, 117, 63, 'Comment like', 'Hiii liked your comment on video titled \'XVI civic\'.', 91, '1', '88', 0, '2020-11-10 11:23:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(306, 63, 63, 'Comment like', 'Bharat STD liked your comment on video titled \'XVI civic\'.', 88, '1', '91', 0, '2020-11-10 12:03:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(307, 63, 63, 'Comment like', 'Bharat STD liked your comment on video titled \'XVI civic\'.', 88, '1', '91', 0, '2020-11-10 12:03:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(308, 63, 63, 'Comment like', 'Hiii liked your comment on video titled \'XVI civic\'.', 91, '1', '88', 0, '2020-11-10 12:27:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(309, 63, 63, 'Comment like', 'Bharat STD liked your comment on video titled \'XVI civic\'.', 88, '1', '91', 0, '2020-11-10 14:02:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(310, 63, 63, 'Comment', 'Bharat STD commented on your video titled \'XVI civic\'.', 88, '1', '91', 0, '2020-11-10 14:03:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(311, 63, 63, 'Comment', 'Bharat STD commented on your video titled \'XVI civic\'.', 88, '1', '91', 0, '2020-11-10 14:16:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(312, 63, 63, 'Comment', 'Bharat STD replied to Hiii\'s comment on your video titled \'XVI civic\'.', 88, '1', '91', 0, '2020-11-11 06:10:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(313, 63, 63, 'Comment', 'Bharat STD replied to your comment on video titled \'XVI civic\'.', 88, '1', '91', 0, '2020-11-11 06:10:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(314, 91, 0, 'Request', 'Bharat requested to join you. Please check his request.', 88, '1', '91', 0, '2020-11-11 06:34:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(315, 11, 0, 'Request Approved', 'krish \'s request approved by Teacher for First Grade', 20, '1', '11', 0, '2020-11-11 07:02:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(316, 63, 63, 'Comment like', 'Bharat STD liked your comment on video titled \'XVI civic\'.', 88, '1', '91', 0, '2020-11-11 07:28:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(317, 81, 0, 'Request', 'Junior requested to join you. Please check his request.', 72, '0', '81', 0, '2020-11-11 07:54:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(318, 63, 63, 'Comment', 'Bharat STD commented on your video titled \'XVI civic\'.', 88, '1', '91', 0, '2020-11-11 07:58:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(319, 63, 63, 'Comment', 'Bharat STD replied to Hiii\'s comment on your video titled \'XVI civic\'.', 88, '1', '91', 0, '2020-11-11 08:01:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(320, 63, 63, 'Comment', 'Bharat STD replied to your comment on video titled \'XVI civic\'.', 88, '1', '91', 0, '2020-11-11 08:01:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(321, 63, 63, 'Comment', 'Bharat STD replied to Hiii\'s comment on your video titled \'XVI civic\'.', 88, '1', '91', 0, '2020-11-11 08:02:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(322, 63, 63, 'Comment', 'Bharat STD replied to your comment on video titled \'XVI civic\'.', 88, '1', '91', 0, '2020-11-11 08:02:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(323, 63, 63, 'Comment', 'Bharat STD replied to Hiii\'s comment on your video titled \'XVI civic\'.', 88, '1', '91', 0, '2020-11-11 08:05:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(324, 63, 63, 'Comment', 'Bharat STD replied to your comment on video titled \'XVI civic\'.', 88, '1', '91', 0, '2020-11-11 08:05:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(325, 63, 63, 'Comment', 'Bharat STD replied to Bharat STD\'s comment on your video titled \'XVI civic\'.', 88, '1', '91', 0, '2020-11-11 08:06:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(326, 32, 32, 'Comment', 'Bharat School commented on your video titled \'Hi Test\'.', 72, '0', '81', 0, '2020-11-11 08:52:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(327, 32, 32, 'Comment', 'Bharat School replied to Bharat School\'s comment on your video titled \'Hi Test\'.', 72, '0', '81', 0, '2020-11-11 08:54:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(328, 32, 32, 'Comment', 'Bharat School commented on your video titled \'Hi Test\'.', 72, '0', '81', 0, '2020-11-11 12:53:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(329, 32, 32, 'Comment', 'Bharat School replied to Bharat School\'s comment on your video titled \'Hi Test\'.', 72, '0', '81', 0, '2020-11-11 12:53:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(330, 32, 32, 'Comment', 'Bharat School replied to Bharat School\'s comment on your video titled \'Hi Test\'.', 72, '0', '81', 0, '2020-11-11 12:53:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(331, 70, 70, 'Comment', 'Bharat STD commented on your video titled \'Test 1457\'.', 88, '1', '91', 0, '2020-11-11 14:05:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(332, 70, 70, 'Comment', 'Bharat STD replied to Bharat STD\'s comment on your video titled \'Test 1457\'.', 88, '1', '91', 0, '2020-11-11 14:06:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(333, 70, 70, 'Comment', 'Bharat STD commented on your video titled \'Test 1457\'.', 88, '1', '91', 0, '2020-11-11 14:11:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(334, 70, 70, 'Comment', 'Bharat STD commented on your video titled \'Test 1457\'.', 88, '1', '91', 0, '2020-11-11 14:13:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(335, 70, 70, 'Comment', 'Bharat STD commented on your video titled \'Test 1457\'.', 88, '1', '91', 0, '2020-11-11 14:13:56', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(336, 70, 70, 'Comment', 'Bharat STD replied to Bharat STD\'s comment on your video titled \'Test 1457\'.', 88, '1', '91', 0, '2020-11-11 14:14:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(337, 70, 70, 'Comment', 'Bharat STD replied to Bharat STD\'s comment on your video titled \'Test 1457\'.', 88, '1', '91', 0, '2020-11-11 14:14:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(338, 70, 70, 'Comment', 'Bharat STD commented on your video titled \'Test 1457\'.', 88, '1', '91', 0, '2020-11-11 14:15:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(339, 70, 70, 'Comment', 'Bharat STD commented on your video titled \'Test 1457\'.', 88, '1', '91', 0, '2020-11-11 14:16:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(340, 68, 68, 'Teacher Upload Media', 'Hiii uploaded new video for First Grade', 91, '1', '88', 0, '2020-11-12 05:07:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(341, 69, 69, 'Teacher Upload Media', 'Hiii uploaded new video for First Grade', 91, '1', '88', 0, '2020-11-12 05:07:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(342, 70, 70, 'Teacher Upload Media', 'Hiii uploaded new video for First Grade', 91, '1', '88', 0, '2020-11-12 05:07:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(343, 71, 71, 'Teacher Upload Media', 'Hiii uploaded new video for First Grade', 91, '1', '88', 0, '2020-11-12 05:07:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(344, 71, 71, 'Comment', 'Bharat STD commented on your video titled \'Yeast cookie cutter\'.', 88, '1', '91', 0, '2020-11-12 05:37:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(345, 71, 71, 'Comment', 'Bharat STD commented on your video titled \'Yeast cookie cutter\'.', 88, '1', '91', 0, '2020-11-12 05:38:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(346, 71, 71, 'Comment', 'Bharat STD replied to Bharat STD\'s comment on your video titled \'Yeast cookie cutter\'.', 88, '1', '91', 0, '2020-11-12 05:38:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(347, 71, 71, 'Comment', 'Bharat STD replied to Bharat STD\'s comment on your video titled \'Yeast cookie cutter\'.', 88, '1', '91', 0, '2020-11-12 05:38:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(348, 71, 71, 'Comment', 'Bharat STD replied to Bharat STD\'s comment on your video titled \'Yeast cookie cutter\'.', 88, '1', '91', 0, '2020-11-12 05:39:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(349, 71, 71, 'Comment', 'Bharat STD commented on your video titled \'Yeast cookie cutter\'.', 88, '1', '91', 0, '2020-11-12 05:39:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(350, 71, 71, 'Comment', 'Bharat STD replied to Bharat STD\'s comment on your video titled \'Yeast cookie cutter\'.', 88, '1', '91', 0, '2020-11-12 05:39:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(351, 71, 71, 'Comment', 'Bharat STD replied to Bharat STD\'s comment on your video titled \'Yeast cookie cutter\'.', 88, '1', '91', 0, '2020-11-12 05:41:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(352, 71, 71, 'Comment', 'Bharat STD replied to Bharat STD\'s comment on your video titled \'Yeast cookie cutter\'.', 88, '1', '91', 0, '2020-11-12 05:41:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(353, 71, 71, 'Comment', 'Bharat STD commented on your video titled \'Yeast cookie cutter\'.', 88, '1', '91', 0, '2020-11-12 05:51:57', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(354, 70, 70, 'Comment', 'Bharat STD replied to Bharat STD\'s comment on your video titled \'Test 1457\'.', 88, '1', '91', 0, '2020-11-12 05:58:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(355, 70, 70, 'Comment', 'Bharat STD replied to Bharat STD\'s comment on your video titled \'Test 1457\'.', 88, '1', '91', 0, '2020-11-12 05:59:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(356, 70, 70, 'Comment like', 'Bharat STD liked your comment on video titled \'Test 1457\'.', 88, '1', '91', 0, '2020-11-12 06:01:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(357, 70, 70, 'Comment', 'Bharat STD replied to Hiii\'s comment on your video titled \'Test 1457\'.', 88, '1', '91', 0, '2020-11-12 06:03:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(358, 70, 70, 'Comment', 'Bharat STD replied to your comment on video titled \'Test 1457\'.', 88, '1', '91', 0, '2020-11-12 06:03:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(359, 70, 70, 'Comment', 'Bharat STD replied to Bharat STD\'s comment on your video titled \'Test 1457\'.', 88, '1', '91', 0, '2020-11-12 06:03:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(360, 62, 62, 'Comment', 'Bharat STD replied to Hiii\'s comment on your video titled \'Gsushsbshd\'.', 88, '1', '91', 0, '2020-11-12 06:48:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(361, 62, 62, 'Comment', 'Bharat STD replied to your comment on video titled \'Gsushsbshd\'.', 88, '1', '91', 0, '2020-11-12 06:48:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(362, 62, 62, 'Comment', 'Bharat STD replied to Hiii\'s comment on your video titled \'Gsushsbshd\'.', 88, '1', '91', 0, '2020-11-12 06:48:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(363, 62, 62, 'Comment', 'Bharat STD replied to your comment on video titled \'Gsushsbshd\'.', 88, '1', '91', 0, '2020-11-12 06:48:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(364, 62, 62, 'Comment', 'Bharat STD replied to Bharat STD\'s comment on your video titled \'Gsushsbshd\'.', 88, '1', '91', 0, '2020-11-12 06:48:57', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(365, 62, 62, 'Comment', 'Bharat STD commented on your video titled \'Gsushsbshd\'.', 88, '1', '91', 0, '2020-11-12 06:49:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(366, 62, 62, 'Comment', 'Bharat STD commented on your video titled \'Gsushsbshd\'.', 88, '1', '91', 0, '2020-11-12 06:56:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(367, 62, 62, 'Comment', 'Bharat STD commented on your video titled \'Gsushsbshd\'.', 88, '1', '91', 0, '2020-11-12 06:57:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(368, 62, 62, 'Comment', 'Bharat STD commented on your video titled \'Gsushsbshd\'.', 88, '1', '91', 0, '2020-11-12 06:58:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(369, 62, 62, 'Comment', 'Bharat STD commented on your video titled \'Gsushsbshd\'.', 88, '1', '91', 0, '2020-11-12 06:59:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(370, 62, 62, 'Comment', 'Bharat STD commented on your video titled \'Gsushsbshd\'.', 88, '1', '91', 0, '2020-11-12 07:00:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(371, 62, 62, 'Comment', 'Bharat STD commented on your video titled \'Gsushsbshd\'.', 88, '1', '91', 0, '2020-11-12 07:00:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(372, 77, 77, 'Comment', 'Bharat STD commented on your video titled \'Hiiiii update\'.', 88, '1', '91', 0, '2020-11-12 08:01:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(373, 78, 78, 'Comment like', 'Bharat STD liked your comment on video titled \'Doyfkyflycyl\'.', 88, '1', '91', 0, '2020-11-12 08:04:21', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(374, 61, 0, 'Request Approved', 'demo demo \'s request approved by vghfgh for Second Grade', 64, '0', '61', 0, '2020-11-12 08:05:21', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(375, 73, 73, 'Teacher Upload Media', 'Vghfgh uploaded new video for Second Grade', 64, '0', '61', 0, '2020-11-12 08:05:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(376, 74, 74, 'Teacher Upload Media', 'Vghfgh uploaded new video for Second Grade', 64, '0', '61', 0, '2020-11-12 08:05:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(377, 75, 75, 'Teacher Upload Media', 'Vghfgh uploaded new video for Second Grade', 64, '0', '61', 0, '2020-11-12 08:05:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(378, 77, 77, 'Teacher Upload Media', 'Hiii uploaded new video for Third Grade', 91, '1', '88', 0, '2020-11-12 08:05:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(379, 78, 78, 'Teacher Upload Media', 'Hiii uploaded new video for First Grade', 91, '1', '88', 0, '2020-11-12 08:05:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(380, 79, 79, 'Teacher Upload Media', 'Hiii uploaded new video for First Grade', 91, '1', '88', 0, '2020-11-12 08:16:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(381, 80, 80, 'Teacher Upload Media', 'Hiii uploaded new video for First Grade', 91, '1', '88', 0, '2020-11-12 08:20:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(382, 81, 81, 'Teacher Upload Media', 'Hiii uploaded new video for First Grade', 91, '1', '88', 0, '2020-11-12 10:17:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(383, 82, 82, 'Teacher Upload Media', 'Hiii uploaded new video for First Grade', 91, '1', '88', 0, '2020-11-12 10:28:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(384, 152, 0, 'Request', 'Peter requested to join you. Please check his request.', 88, '1', '152', 0, '2020-11-16 12:39:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(385, 153, 0, 'Request', ' registered in your district. Please check his request.', 1, '0', '153', 0, '2020-11-17 10:31:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(386, 153, 0, 'Request', ' registered in your district. Please check his request.', 1, '0', '153', 0, '2020-11-17 10:34:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(387, 153, 0, 'Request', ' registered in your district. Please check his request.', 1, '0', '153', 0, '2020-11-17 10:35:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(388, 153, 0, 'Request', 'New eras public school registered in your district. Please check his request.', 1, '0', '153', 0, '2020-11-17 10:37:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(389, 154, 0, 'Request', '  registered in your school. Please check his request.', 153, '0', '154', 0, '2020-11-17 10:48:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(390, 154, 0, 'Request', 'Myra pilate registered in your school. Please check his request.', 153, '0', '154', 0, '2020-11-17 10:50:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(391, 155, 0, 'Request', 'Kids zone registered in your district. Please check his request.', 1, '0', '155', 0, '2020-11-17 10:59:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(392, 156, 0, 'Request', '  registered in your school. Please check his request.', 155, '0', '156', 0, '2020-11-17 11:02:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(393, 157, 0, 'Request', 'Michael John registered in your school. Please check his request.', 155, '0', '157', 0, '2020-11-17 11:06:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(394, 20, 0, 'Request', 'Keety requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-17 11:39:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(395, 20, 0, 'Request', 'Orio requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-17 12:31:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(396, 20, 0, 'Request', 'Mike requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-17 12:32:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(397, 158, 0, 'Request', 'Bharat SC registered in your district. Please check his request.', 82, '0', '158', 0, '2020-11-18 05:57:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(398, 159, 0, 'Request', 'Occians high school 2 registered in your district. Please check his request.', 1, '0', '159', 0, '2020-11-18 12:51:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(399, 0, 0, 'new_group', 'you have been added to  group Group 1', 158, '0', '11', 0, '2020-11-18 19:20:44', '2020-11-18 19:20:44', '2020-11-18 19:20:44'),
(400, 0, 0, 'new_group', 'you have been added to  group Group 1', 159, '0', '11', 0, '2020-11-18 19:20:44', '2020-11-18 19:20:44', '2020-11-18 19:20:44'),
(401, 83, 83, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-18 13:55:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(402, 83, 83, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-18 13:55:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(403, 84, 84, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-18 13:55:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(404, 84, 84, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-18 13:55:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(405, 85, 85, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-18 13:55:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(406, 85, 85, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-18 13:55:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(407, 86, 86, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-18 13:55:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(408, 86, 86, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-18 13:55:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(409, 20, 0, 'Request', 'Dg requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-19 06:07:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(410, 20, 0, 'Request', 'Ggg requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-19 06:10:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(411, 20, 0, 'Request', 'Ere requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-19 06:20:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(412, 20, 0, 'Request', 'Ee requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-19 06:20:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(413, 20, 0, 'Request', 'E requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-19 06:22:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(414, 20, 0, 'Request', 'W requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-19 06:22:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(415, 20, 0, 'Request', 'Fdg requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-19 06:32:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(416, 20, 0, 'Request', 'D requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-19 06:34:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(417, 87, 87, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-19 06:51:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(418, 87, 87, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-19 06:51:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(419, 11, 0, 'Request Approved', 'mike \'s request approved by Teacher for 1st Grade', 20, '1', '11', 0, '2020-11-19 06:51:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(420, 27, 0, 'Broadcast Notification', 'New broadcast message.', 56, '1', '27', 0, '2020-11-19 07:08:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(421, 0, 0, 'new_group', 'you have been added to  group Group 2', 158, '0', '11', 0, '2020-11-19 16:15:51', '2020-11-19 16:15:51', '2020-11-19 16:15:51'),
(422, 0, 0, 'new_group', 'you have been added to  group Group 2', 159, '0', '11', 0, '2020-11-19 16:15:51', '2020-11-19 16:15:51', '2020-11-19 16:15:51'),
(423, 0, 0, 'new_group', 'you have been added to  group group 3', 20, '1', '11', 0, '2020-11-19 16:16:55', '2020-11-19 16:16:55', '2020-11-19 16:16:55'),
(424, 0, 0, 'new_group', 'you have been added to  group group 3', 74, '0', '11', 0, '2020-11-19 16:16:55', '2020-11-19 16:16:55', '2020-11-19 16:16:55'),
(425, 0, 0, 'new_group', 'you have been added to  group group 4', 20, '1', '11', 0, '2020-11-19 16:20:21', '2020-11-19 16:20:21', '2020-11-19 16:20:21'),
(426, 0, 0, 'new_group', 'you have been added to  group group 4', 74, '0', '11', 0, '2020-11-19 16:20:21', '2020-11-19 16:20:21', '2020-11-19 16:20:21'),
(427, 0, 0, 'new_group', 'you have been added to  group group 6', 74, '0', '11', 0, '2020-11-19 16:21:29', '2020-11-19 16:21:29', '2020-11-19 16:21:29'),
(428, 0, 0, 'new_group', 'you have been added to  group Group 100', 74, '0', '11', 0, '2020-11-19 20:01:10', '2020-11-19 20:01:10', '2020-11-19 20:01:10'),
(429, 75, 75, 'Like', 'Merry Tylor liked your video titled \'tryhy\'.', 61, '0', '1', 1, '2020-11-20 05:23:23', '2020-11-20 05:23:37', '0000-00-00 00:00:00'),
(430, 75, 75, 'Like', 'Merry Tylor liked your video titled \'tryhy\'.', 61, '0', '1', 1, '2020-11-20 05:23:25', '2020-11-20 05:23:37', '0000-00-00 00:00:00'),
(431, 75, 75, 'Like', 'Merry Tylor liked your video titled \'tryhy\'.', 61, '0', '1', 1, '2020-11-20 05:23:27', '2020-11-20 05:23:37', '0000-00-00 00:00:00'),
(432, 1, 0, 'Broadcast Notification', 'New broadcast message.', 82, '0', '1', 0, '2020-11-20 05:48:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(433, 1, 0, 'Broadcast Notification', 'New broadcast message.', 143, '0', '1', 0, '2020-11-20 05:48:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(434, 1, 0, 'Broadcast Notification', 'New broadcast message.', 82, '0', '1', 0, '2020-11-20 05:48:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(435, 1, 0, 'Broadcast Notification', 'New broadcast message.', 143, '0', '1', 0, '2020-11-20 05:48:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(436, 1, 0, 'Broadcast Notification', 'New broadcast message.', 82, '0', '1', 0, '2020-11-20 05:48:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(437, 1, 0, 'Broadcast Notification', 'New broadcast message.', 68, '0', '1', 0, '2020-11-20 05:48:21', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(438, 1, 0, 'Broadcast Notification', 'New broadcast message.', 30, '0', '1', 0, '2020-11-20 05:48:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(439, 71, 71, 'Like', 'Bharat STD liked your video titled \'Yeast cookie cutter\'.', 88, '0', '91', 0, '2020-11-20 07:41:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(440, 0, 0, 'new_group', 'you have been added to  group test group', 11, '1', '20', 0, '2020-11-20 13:34:59', '2020-11-20 13:34:59', '2020-11-20 13:34:59'),
(441, 1, 0, 'Broadcast Notification', 'New broadcast message.', 184, '0', '1', 0, '2020-11-20 08:47:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(442, 27, 0, 'Broadcast Notification', 'New broadcast message.', 124, '0', '27', 0, '2020-11-20 08:47:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(443, 27, 0, 'Broadcast Notification', 'New broadcast message.', 56, '0', '27', 0, '2020-11-20 08:47:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(444, 27, 0, 'Broadcast Notification', 'New broadcast message.', 56, '0', '27', 0, '2020-11-20 08:47:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(445, 88, 88, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-20 08:47:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(446, 88, 88, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-20 08:47:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(447, 89, 89, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-20 08:47:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(448, 89, 89, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-20 08:47:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(449, 90, 90, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 20, '1', '11', 0, '2020-11-20 08:47:57', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(450, 90, 90, 'Teacher Upload Media', 'Teacher uploaded new video for Kindergarten', 74, '0', '11', 0, '2020-11-20 08:48:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(451, 1, 0, 'Broadcast Notification', 'New broadcast message.', 184, '0', '1', 0, '2020-11-20 09:21:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(452, 1, 0, 'Broadcast Notification', 'New broadcast message.', 183, '0', '1', 0, '2020-11-20 09:21:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(453, 1, 0, 'Broadcast Notification', 'New broadcast message.', 183, '0', '1', 0, '2020-11-20 09:23:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(454, 27, 0, 'Broadcast Notification', 'New broadcast message.', 29, '0', '27', 0, '2020-11-20 09:46:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(455, 27, 0, 'Broadcast Notification', 'New broadcast message.', 29, '0', '27', 0, '2020-11-20 09:48:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(456, 0, 0, 'new_group', 'you have been added to  group Grp22', 20, '1', '11', 0, '2020-11-20 15:46:50', '2020-11-20 15:46:50', '2020-11-20 15:46:50'),
(457, 0, 0, 'new_group', 'you have been added to  group Group 4', 11, '1', '11', 0, '2020-11-20 16:11:16', '2020-11-20 16:11:16', '2020-11-20 16:11:16'),
(458, 0, 0, 'new_group', 'you have been added to  group Group 4', 12, '0', '11', 0, '2020-11-20 16:13:53', '2020-11-20 16:13:53', '2020-11-20 16:13:53'),
(459, 0, 0, 'new_group', 'you have been added to  group Group 55', 12, '0', '11', 0, '2020-11-20 16:19:02', '2020-11-20 16:19:02', '2020-11-20 16:19:02'),
(460, 0, 0, 'new_group', 'you have been added to  group hellodes', 1, '0', '11', 0, '2020-11-20 17:05:49', '2020-11-20 17:05:49', '2020-11-20 17:05:49'),
(461, 0, 0, 'new_group', 'you have been added to  group hellodes', 2, '0', '11', 0, '2020-11-20 17:05:49', '2020-11-20 17:05:49', '2020-11-20 17:05:49'),
(462, 0, 0, 'new_group', 'you have been added to  group hellodes', 3, '0', '11', 0, '2020-11-20 17:05:49', '2020-11-20 17:05:49', '2020-11-20 17:05:49'),
(463, 20, 0, 'Request', 'Erer requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-20 12:18:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(464, 20, 0, 'Request', 'Martin requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-20 12:22:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(465, 20, 0, 'Request', 'Merry requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-20 12:25:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(466, 20, 0, 'Request', 'Meenu requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-20 12:44:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(467, 20, 0, 'Request', 'Jeera requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-20 12:51:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(468, 20, 0, 'Request', 'Nisha requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-20 12:53:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(469, 0, 0, 'new_group', 'you have been added to  group test group test ', 0, '0', '11', 0, '2020-11-20 18:57:02', '2020-11-20 18:57:02', '2020-11-20 18:57:02'),
(470, 0, 0, 'new_group', 'you have been added to  group tie ', 0, '0', '11', 0, '2020-11-21 11:18:30', '2020-11-21 11:18:30', '2020-11-21 11:18:30'),
(471, 0, 0, 'new_group', 'you have been added to  group Vjvjgi', 20, '1', '11', 0, '2020-11-21 12:03:03', '2020-11-21 12:03:03', '2020-11-21 12:03:03'),
(472, 0, 0, 'new_group', 'you have been added to  group Testing great', 20, '1', '11', 0, '2020-11-21 12:49:14', '2020-11-21 12:49:14', '2020-11-21 12:49:14'),
(473, 190, 0, 'Request', 'TeacherNn Tlast registered in your school. Please check his request.', 10, '0', '190', 0, '2020-11-21 10:31:57', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(474, 0, 0, 'new_group', 'you have been added to  group Ttt11', 11, '1', '190', 0, '2020-11-21 16:36:04', '2020-11-21 16:36:04', '2020-11-21 16:36:04'),
(475, 192, 0, 'Request', 'Bharat st. School registered in your district. Please check his request.', 191, '0', '192', 0, '2020-11-21 12:20:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(476, 20, 0, 'Request', 'Test requested to join you. Please check his request.', 11, '1', '20', 0, '2020-11-21 12:38:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(477, 20, 0, 'Request', 'Test requested to join you. Please check his request.', 190, '0', '20', 0, '2020-11-21 12:38:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(478, 193, 0, 'Request', 'Bharat Teacher registered in your school. Please check his request.', 192, '0', '193', 0, '2020-11-21 12:48:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(479, 194, 0, 'Request', 'Bharat requested to join you. Please check his request.', 193, '1', '194', 0, '2020-11-21 13:00:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(480, 13, 13, 'Comment', 'Bharat Parent commented on your video titled \'sheeghr\'.', 15, '0', '194', 0, '2020-11-21 13:04:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(481, 194, 0, 'Request', 'Rawal requested to join you. Please check his request.', 193, '1', '194', 0, '2020-11-21 13:11:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(482, 0, 0, 'new_group', 'you have been added to  group testing ', 0, '0', '194', 0, '2020-11-21 18:58:45', '2020-11-21 18:58:45', '2020-11-21 18:58:45'),
(483, 0, 0, 'new_group', 'you have been added to  group test group 2', 0, '0', '194', 0, '2020-11-21 19:06:49', '2020-11-21 19:06:49', '2020-11-21 19:06:49'),
(484, 0, 0, 'new_group', 'you have been added to  group test group 3', 0, '0', '194', 0, '2020-11-21 19:09:34', '2020-11-21 19:09:34', '2020-11-21 19:09:34'),
(485, 194, 0, 'Request', 'Hitaishin requested to join you. Please check his request.', 193, '0', '194', 0, '2020-11-21 14:06:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(486, 11, 0, 'Request Approved', 'd\'s request approved by Teacher for 2nd Grade', 20, '1', '11', 0, '2020-11-22 06:26:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(487, 11, 0, 'Request Approved', 'orio\'s request approved by Teacher for 1st Grade', 20, '1', '11', 0, '2020-11-22 06:26:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(488, 11, 0, 'Request Approved', 'Keety\'s request approved by Teacher for 2nd Grade', 20, '1', '11', 0, '2020-11-22 06:26:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(489, 11, 0, 'Request Approved', 'Keety\'s request approved by Teacher for 2nd Grade', 20, '1', '11', 0, '2020-11-22 06:26:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(490, 11, 0, 'Request Approved', 'erer\'s request approved by Teacher for 1st Grade', 20, '1', '11', 0, '2020-11-22 06:26:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(491, 11, 0, 'Request Approved', 'martin\'s request approved by Teacher for 1st Grade', 20, '1', '11', 0, '2020-11-22 06:26:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(492, 1, 0, 'Broadcast Notification', 'New broadcast message.', 153, '0', '1', 0, '2020-11-22 06:26:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(493, 11, 0, 'Request Approved', 'meenu\'s request approved by Teacher for 1st Grade', 20, '1', '11', 0, '2020-11-22 06:26:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(494, 11, 0, 'Request Approved', 'jeera\'s request approved by Teacher for 1st Grade', 20, '1', '11', 0, '2020-11-22 06:26:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(495, 11, 0, 'Request Approved', 'jeera\'s request approved by Teacher for 2nd Grade', 20, '1', '11', 0, '2020-11-22 06:26:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(496, 11, 0, 'Request Approved', 'nisha\'s request approved by Teacher for 1st Grade', 20, '1', '11', 0, '2020-11-22 06:26:53', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(497, 11, 0, 'Request Approved', 'nisha\'s request approved by Teacher for 2nd Grade', 20, '1', '11', 0, '2020-11-22 06:26:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(498, 91, 91, 'Teacher Upload Media', 'Teacher uploaded new video for 1st Grade', 20, '1', '11', 0, '2020-11-22 06:26:56', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(499, 27, 0, 'Broadcast Notification', 'New broadcast message.', 124, '0', '27', 0, '2020-11-22 06:26:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(500, 27, 0, 'Broadcast Notification', 'New broadcast message.', 124, '0', '27', 0, '2020-11-22 06:27:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(501, 27, 0, 'Broadcast Notification', 'New broadcast message.', 124, '0', '27', 0, '2020-11-22 06:27:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(502, 27, 0, 'Broadcast Notification', 'New broadcast message.', 124, '0', '27', 0, '2020-11-22 06:27:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(503, 27, 0, 'Broadcast Notification', 'New broadcast message.', 124, '0', '27', 0, '2020-11-22 06:27:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(504, 3, 0, 'Broadcast Notification', 'New broadcast message.', 7, '0', '3', 0, '2020-11-22 06:27:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(505, 92, 92, 'Teacher Upload Media', 'Teacher uploaded new video for 2nd Grade', 20, '1', '11', 0, '2020-11-22 06:27:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(506, 93, 93, 'Teacher Upload Media', 'Teacher uploaded new video for 2nd Grade', 20, '1', '11', 0, '2020-11-22 06:27:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(507, 191, 0, 'Broadcast Notification', 'New broadcast message.', 192, '0', '191', 0, '2020-11-22 06:27:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(508, 1, 0, 'Broadcast Notification', 'New broadcast message.', 27, '0', '1', 0, '2020-11-22 06:27:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(509, 1, 0, 'Broadcast Notification', 'New broadcast message.', 40, '0', '1', 0, '2020-11-22 06:27:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(510, 1, 0, 'Broadcast Notification', 'New broadcast message.', 67, '0', '1', 0, '2020-11-22 06:27:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(511, 1, 0, 'Broadcast Notification', 'New broadcast message.', 78, '0', '1', 0, '2020-11-22 06:27:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(512, 1, 0, 'Broadcast Notification', 'New broadcast message.', 132, '0', '1', 0, '2020-11-22 06:27:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(513, 1, 0, 'Broadcast Notification', 'New broadcast message.', 153, '0', '1', 0, '2020-11-22 06:28:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(514, 1, 0, 'Broadcast Notification', 'New broadcast message.', 155, '0', '1', 0, '2020-11-22 06:28:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(515, 193, 0, 'Request Approved', 'Bharat\'s request approved by BharatTea for 6th Grade', 194, '0', '193', 0, '2020-11-22 06:28:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(516, 193, 0, 'Request Approved', 'Bharat\'s request approved by BharatTea for Kindergarten', 194, '0', '193', 0, '2020-11-22 06:28:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(517, 94, 94, 'Teacher Upload Media', 'BharatTea uploaded new video for Kindergarten', 194, '0', '193', 0, '2020-11-22 06:28:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(518, 27, 0, 'Broadcast Notification', 'New broadcast message.', 124, '0', '27', 0, '2020-11-22 06:28:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(519, 27, 0, 'Broadcast Notification', 'New broadcast message.', 124, '0', '27', 0, '2020-11-22 06:28:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(520, 27, 0, 'Broadcast Notification', 'New broadcast message.', 56, '0', '27', 0, '2020-11-22 06:28:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(521, 11, 0, 'Request Approved', 'test\'s request approved by Teacher for 1st Grade', 20, '1', '11', 0, '2020-11-22 06:28:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(522, 196, 0, 'Request', 'Jerra sevy registered in your school. Please check his request.', 27, '0', '196', 0, '2020-11-22 07:06:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(523, 20, 0, 'Request', 'Test requested to join you. Please check his request.', 190, '0', '20', 0, '2020-11-22 09:52:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `page_slug` varchar(100) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `meta_keyword` varchar(250) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `title`, `page_slug`, `content`, `meta_title`, `meta_description`, `meta_keyword`, `created`, `modified`) VALUES
(1, 'Terms of Service', 'terms_of_service', '<p>\r\n	<br />\r\n	<strong>Date Posted:</strong></p>\r\n<p>\r\n	PLEASE READ THIS DOCUMENT CAREFULLY. The Truth Debate (&quot;The Truth Debate,&quot; &quot;we,&quot; or &quot;us&quot;) offers an online audio and/or video sharing platform and community through its website and other The Truth Debate-operated sites (collectively, &quot;The Truth Debate website&quot;), mobile applications, and other online services (collectively and including The Truth Debate website, &quot;The Truth Debate&quot;). By registering as a member or by using The Truth Debate in any way, you accept these Terms of Service (&quot;Agreement&quot;), which forms a binding agreement between you and The Truth Debate. If you do not wish to be bound by this Agreement, do not use The Truth Debate.</p>\r\n<p>\r\n	<br />\r\n	<strong>1. Who May Use The Truth Debate</strong></p>\r\n<p>\r\n	<strong>AGE REQUIREMENT:</strong> The Truth Debate is recommended for users age 16 and older. Anyone considered a minor (dependent on where you live), must have your parent&rsquo;s or legal guardian&#39;s permission to use The Truth Debate.&nbsp; Please have him or her read this Agreement with you.</p>\r\n<p>\r\n	<strong>NOTICE TO PARENTS AND GUARDIANS:</strong> By granting your child permission to use The Truth Debate, you agree to the terms of this Agreement on behalf of your child. You are responsible for monitoring and supervising your child&#39;s use of The Truth Debate. If your child is using The Truth Debate and is a minor or does not have your permission, please us the Contact Us form under the gear icon and we will cancel his or her account within 24 hours. If you have questions about whether Truth Debate is appropriate for your child, please contact us.</p>\r\n<p>\r\n	<strong>WARNING:</strong> Even if you have your parent&#39;s or guardian&#39;s permission, some of the content available within The Truth Debate may not be appropriate for you. Some content may contain &quot;R-rated&quot; material, profanity, and mature subject matter. If you are under 18, do not view such content.<br />\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>2. License to Use The Truth Debate</strong></p>\r\n<p>\r\n	<strong>LICENSE:</strong> The Truth Debate grants you a limited, non-exclusive license to access and use The Truth Debate your own personal, non-commercial purposes. This includes the right to view content available on The Truth Debate website and/or mobile apps. This license is personal to you and may not be assigned or sublicensed to anyone else.</p>\r\n<p>\r\n	<strong>COMMERCIAL USE:</strong> You may not use The Truth Debate for commercial purposes.</p>\r\n<p>\r\n	<strong>RESTRICTIONS:</strong> Except as expressly permitted by The Truth Debate in writing, you will not scrape, reproduce, redistribute, sell, create derivative works from, decompile, reverse engineer, or disassemble The Truth Debate or any source code therein. Nor will you attempt to circumvent any of The Truth Debate&#39;s technical measures or take any measures to interfere with or damage The Truth Debate. All rights not expressly granted by The Truth Debate are reserved.<br />\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>3. Privacy</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		What information we may collect about you;</li>\r\n	<li>\r\n		What we use that information for; and</li>\r\n	<li>\r\n		With whom we share that information.<br />\r\n		&nbsp;</li>\r\n</ul>\r\n<p>\r\n	<strong>4. Membership</strong></p>\r\n<p>\r\n	<strong>REGISTRATION:</strong> To fully use The Truth Debate, you must register as a member by providing a user name, password, valid email address, etc. You must provide complete and accurate registration information to The Truth Debate and notify us if your information changes. If you are a business, government, or non-profit entity, the person whose email address is associated with the account must have the authority to bind the entity to this Agreement.</p>\r\n<p>\r\n	<strong>USER NAME:</strong>&nbsp;If you select a user name offensive to anyone, yoiu risk your uploads being blocked by those individuals and not showing up on their feeds.&nbsp; If you are a business, government, or non-profit entity, you must use the actual name of your organization. You may not use someone else&#39;s name, a name that violates any third party right, or a name that is obscene or otherwise objectionable.</p>\r\n<p>\r\n	<strong>ACCOUNT SECURITY:</strong> You are responsible for all activity that occurs under your account, including any activity by authorized or unauthorized users. You must not allow others to use your account credentials and you must safeguard the confidentiality of those credentials. If you are using a computer that others have access to, you must log out of your account after using The Truth Debate. If you become aware of an unauthorized access to your account, you must change your password.<br />\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>5. Term and Termination; Account Deletion</strong></p>\r\n<p>\r\n	<strong>TERM:</strong> This Agreement begins on the date you first use The Truth Debate and continues as long as you have an account with us.</p>\r\n<p>\r\n	<strong>ACCOUNT DELETION:</strong> You may delete your account at any time. &nbsp;Accounts may be deleted from The Truth Debate if they remain inactive (i.e.., the user fails to log in) for a continuous period of at least six (6) months.</p>\r\n<p>\r\n	<strong>TERMINATION FOR BREACH:</strong> The Truth Debate may suspend, disable, or delete your account (or any part thereof) or block or remove any content you submitted if The Truth Debate determines that you have violated any provision of this Agreement or that your conduct or content would tend to damage The Truth Debate&#39;s reputation and goodwill. If The Truth Debate deletes your account for the foregoing reasons, you may not re-register for The Truth Debate. The Truth Debate may block your email address and Internet protocol address to prevent further registration.</p>\r\n<p>\r\n	<strong>EFFECT OF TERMINATION/ACCOUNT DELETION:</strong> Upon termination, all licenses granted by The Truth Debate will terminate. Sections 6 and 10 though 15 shall survive termination. In the event of account deletion for any reason, content that you submitted may no longer be available. The Truth Debate shall not be responsible for the loss of such content.<br />\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>6. Content Restrictions</strong></p>\r\n<p>\r\n	You may not upload, post, or transmit (collectively, &quot;submit&quot;) any video, image, text, audio recording, or other work (collectively, &quot;content&quot;) that:</p>\r\n<ul>\r\n	<li>\r\n		Infringes any third party&#39;s copyrights or other rights (e.g.., trademark, privacy rights, etc.);</li>\r\n	<li>\r\n		Contains sexually explicit content or pornography;</li>\r\n	<li>\r\n		Contains hateful, defamatory, or discriminatory content or incites hatred against any individual or group;</li>\r\n	<li>\r\n		Exploits minors;</li>\r\n	<li>\r\n		Depicts unlawful acts or extreme violence;</li>\r\n	<li>\r\n		Depicts animal cruelty or extreme violence towards animals;</li>\r\n	<li>\r\n		Promotes fraudulent or dubious business schemes; or</li>\r\n	<li>\r\n		Violates any law.<br />\r\n		&nbsp;</li>\r\n</ul>\r\n<p>\r\n	<strong>7. Code of Conduct</strong></p>\r\n<p>\r\n	In using The Truth Debate, you must behave in a civil and respectful manner at all times. Further, you will not:</p>\r\n<ul>\r\n	<li>\r\n		Act in a deceptive manner by, among other things, impersonating any person;</li>\r\n	<li>\r\n		Harass or stalk any other person;</li>\r\n	<li>\r\n		Harm or exploit minors;</li>\r\n	<li>\r\n		Distribute &quot;spam&quot;;</li>\r\n	<li>\r\n		Collect information about others; or</li>\r\n	<li>\r\n		Advertise or solicit others to purchase any product or service within The Truth Debate Website (unless you are an official The Truth Debate partner or advertiser and have a written agreement with The Truth Debate).</li>\r\n</ul>\r\n<p>\r\n	The Truth Debate has the right, but not the obligation, to monitor all conduct on and content submitted to The Truth Debate.<br />\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>8. Licenses Granted by You</strong></p>\r\n<p>\r\n	<strong>8.1 Audios and/or Videos</strong></p>\r\n<p>\r\n	<strong>LICENSE TO THE TRUTH DEBATE:</strong> As between you and The Truth Debate, you own the audio and/or video content (&quot;audios and/or videos&quot;) that you submit to The Truth Debate. By submitting an audio and/or video, you grant The Truth Debate and its affiliates a limited, worldwide, non-exclusive, royalty-free license and right to copy, transmit, distribute, publicly perform, and display (through all media now known or hereafter created), and make derivative works from your audio and/or video for the purpose of (i) displaying the audio and/or video within The Truth Debate; (ii) displaying the audio and/or video on third party websites and applications through an audio and/or video embed or The Truth Debate&#39;s API subject to your audio and/or video privacy choices; (iii) allowing other users to play, download, and embed on third party websites the audio and/or video, subject to your audio and/or video privacy choices; (iii) promoting The Truth Debate, provided that you have made the audio and/or video publicly available; and (iv) archiving or preserving the audio and/or video for disputes, legal proceedings, or investigations.</p>\r\n<p>\r\n	<strong>LICENSE TO OTHER USERS:</strong> You further grant all users of The Truth Debate permission to view your audios and/or videos for their personal, non-commercial purposes. This includes the right to copy and make derivative works from the audios and/or videos solely to the extent necessary to view the audios and/or videos. The foregoing licenses are in addition to any license you may decide to grant (e.g.., a Creative Commons license).</p>\r\n<p>\r\n	<strong>DURATION OF LICENSES:</strong> The above licenses will continue unless and until you remove your audios and/or videos from The Truth Debate, in which case the licenses will terminate within a commercially reasonable period of time. Notwithstanding the foregoing, the license for legal archival/preservation purposes will continue indefinitely. Please note that removed audios and/or videos may be cached in search engine indices after removal and that The Truth Debate has no control over such caching.</p>\r\n<p>\r\n	<strong>8.2 Non-audio and/or non-video Content</strong></p>\r\n<p>\r\n	As between you and The Truth Debate, you own all non-audio and/or non-video content that you submit to The Truth Debate. You grant The Truth Debate and its affiliates a worldwide, perpetual, irrevocable, non-exclusive, royalty-free license and right to copy, transmit, distribute, publicly perform, and display (through all media now known or hereafter created), and make derivative works from your non-audio and/or non-video content. In addition, you waive any so-called &quot;moral rights&quot; in your non-audio and/or non-video content. You further grant all users of The Truth Debate permission to view your non-audio and/or non-video content for their personal, non-commercial purposes. If you make suggestions to The Truth Debate on improving or adding new features to The Truth Debate, The Truth Debate shall have the right to use your suggestions without any compensation to you.<br />\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>9. Your Representations and Warranties</strong></p>\r\n<p>\r\n	For each piece of content that you submit, you represent and warrant that: (i) you have the right to submit the content to The Truth Debate and grant the licenses set forth above; (ii) The Truth Debate will not need to obtain licenses from any third party or pay royalties to any third party; (iii) the content does not infringe any third party&#39;s rights, including intellectual property rights and privacy rights; and (iv) the content complies with this Agreement and all applicable laws.<br />\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>10. Indemnification</strong></p>\r\n<p>\r\n	You will indemnify, defend, and hold harmless The Truth Debate and its affiliates, directors, officers, employees, and agents, from and against all third party actions that: (i) arise from your activities on The Truth Debate; (ii) assert a violation by you of any term of this Agreement; or (iii) assert that any content you submitted to The Truth Debate violates any law or infringes any third party right, including any intellectual property or privacy right.<br />\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>11. Third Party Copyrights and Other Rights</strong></p>\r\n<p>\r\n	The Truth Debate respects the intellectual property rights of others. If you believe that your copyright has been infringed, please send us a notice as set forth in our copyright and DMCA policy below. For other intellectual property claims, please send us a notice at legal@thetruthdebate.com.</p>\r\n<p>\r\n	The Truth Debate respects the intellectual property of others, and we ask our users to do the same. Each user is responsible for ensuring that the materials they upload to The Truth Debate website do not infringe any third party copyright.</p>\r\n<p>\r\n	The Truth Debate will promptly remove materials from The Truth Debate website in accordance with the Digital Millennium Copyright Act (&quot;DMCA&quot;) if properly notified that the materials infringe a third party&#39;s copyright. In addition, The Truth Debate may, in appropriate circumstances, terminate the accounts of repeat copyright infringers.</p>\r\n<p>\r\n	Filing a DMCA Notice to Remove Copyrighted Content-for Copyright Holders</p>\r\n<p>\r\n	If you believe that your work has been copied in a way that constitutes copyright infringement, please provide us with a written notice containing the following information:</p>\r\n<p>\r\n	1.&nbsp;&nbsp; Your name, addresses, telephone number, and email address;</p>\r\n<p>\r\n	2.&nbsp;&nbsp; A description of the copyrighted work that you claim has been infringed;</p>\r\n<p>\r\n	3.&nbsp;&nbsp; A description of where on The Truth Debate website the material that you claim is infringing may be found, sufficient for The Truth Debate to locate the material (e.g., the URL);</p>\r\n<p>\r\n	4.&nbsp;&nbsp; A statement that you have a good faith belief that the use of the copyrighted work is not authorized by the copyright owner, its agent, or the law;</p>\r\n<p>\r\n	5.&nbsp;&nbsp; A statement by you UNDER PENALTY OF PERJURY that the information in your notice is accurate and that you are the copyright owner or authorized to act on the copyright owner&#39;s behalf;</p>\r\n<p>\r\n	6.&nbsp;&nbsp; Your electronic or physical signature.</p>\r\n<p>\r\n	<br />\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>12. Disclaimers</strong></p>\r\n<p>\r\n	The Truth Debate reserves the right to modify The Truth Debate. You are responsible for providing your own access (e.g.., computer, mobile device, Internet connection, etc.) to The Truth Debate. The Truth Debate has no obligation to screen or monitor any content and does not guarantee that any content available on The Truth Debate complies with this Agreement or is suitable for all users.</p>\r\n<p>\r\n	The Truth Debate provides The Truth Debate on an &quot;as is&quot; and &quot;as available&quot; basis. You therefore use The Truth Debate at your own risk. The Truth Debate expressly disclaims any and all warranties of any kind, whether express or implied, including, but not limited to the implied warranties of merchantability, fitness for a particular purpose, non-infringement, and any other warranty that might arise under any law. Without limiting the foregoing, The Truth Debate makes no representations or warranties:</p>\r\n<ul>\r\n	<li>\r\n		That The Truth Debate will be permitted in your jurisdiction;</li>\r\n	<li>\r\n		That The Truth Debate will be uninterrupted or error-free;</li>\r\n	<li>\r\n		Concerning any content submitted by any member;</li>\r\n	<li>\r\n		Concerning any third party&#39;s use of content that you submit;</li>\r\n	<li>\r\n		That any content you submit will be made available on The Truth Debate or will be stored by The Truth Debate;</li>\r\n	<li>\r\n		That The Truth Debate will meet your business or professional needs;</li>\r\n	<li>\r\n		That The Truth Debate will continue to support any particular feature of The Truth Debate; or</li>\r\n	<li>\r\n		Concerning sites and resources outside of The Truth Debate, even if linked to from The Truth Debate.</li>\r\n</ul>\r\n<p>\r\n	To the extent any disclaimer or limitation of liability does not apply, all applicable express, implied, and statutory warranties will be limited in duration to a period of thirty (30) days after the date on which you first used The Truth Debate, and no warranties shall apply after such period.<br />\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>13. Limitation of Liability</strong></p>\r\n<p>\r\n	To the fullest extent permitted by law: (i) The Truth Debate shall not be liable for any direct, indirect, incidental, special, consequential, or exemplary damages, including but not limited to damages for loss of profits, goodwill, use, data, or other intangible losses; and (ii) The Truth Debate&#39;s total liability to you shall not exceed the amounts paid by you to The Truth Debate over the twelve (12) months preceding your claim(s).<br />\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>14. Compliance Notice Pursuant to 18 U.S.C. &sect; 2257</strong></p>\r\n<p>\r\n	All pictures, graphics, audios and/or videos, and other visual media displayed on The Truth Debate are exempt from 18 U.S.C. &sect; 2257 and 28 C.F.R. 75 because they do not consist of depictions of conduct as specifically listed in 18 U.S.C. &sect; 2256 (2) (A) - (D)or are otherwise exempt because the visual depictions were created prior to July 3, 1995. The Truth Debate is not the primary producer of the audio and/or visual content contained on The Truth Debate.<br />\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>15. General Provisions</strong></p>\r\n<p>\r\n	<strong>GOVERNING LAW:</strong> This Agreement shall be governed by the laws of the State of California, United States of America, without regard to principles of conflicts of law. The Uniform Commercial Code, the Uniform Computer Information Transaction Act, and the United Nations Convention of Controls for International Sale of Goods shall not apply.</p>\r\n<p>\r\n	<strong>DISPUTES:</strong> Any action arising out of or relating to this Agreement or your use of The Truth Debate must be commenced in the state or federal courts located in Fresno County, California, United States of America (and you consent to the jurisdiction of those courts). In any such action, The Truth Debate and you irrevocably waive any right to a trial by jury.</p>\r\n<p>\r\n	<strong>INTERPRETATION; SEVERABILITY; WAIVER; REMEDIES:</strong> Headings are for convenience only and shall not be used to construe the terms of this Agreement. If any term of this Agreement is found invalid or unenforceable by any court of competent jurisdiction, that term will be severed from this Agreement. No failure or delays by The Truth Debate in exercising any right hereunder will waive any further exercise of that right. The Truth Debate&#39;s rights and remedies hereunder are cumulative and not exclusive.</p>\r\n<p>\r\n	<strong>SUCCESSORS; ASSIGNMENT; NO THIRD PARTY BENEFICIARIES:</strong> This Agreement is binding upon and shall inure to the benefit of both parties and their respective successors, heirs, executors, administrators, personal representatives, and permitted assigns. You may not assign this Agreement without The Truth Debate&#39;s prior written consent. No third party shall have any rights hereunder.</p>\r\n<p>\r\n	Nothing herein shall limit The Truth Debate&#39;s right to object to subpoenas, claims, or other demands.</p>\r\n<p>\r\n	<strong>MODIFICATION:</strong> This Agreement may not be modified except by a revised Terms of Service posted by The Truth Debate on The Truth Debate website or a written amendment signed by an authorized representative of The Truth Debate. A revised Terms of Service will be effective as of the date it is posted on The Truth Debate website.</p>\r\n<p>\r\n	<strong>ENTIRE AGREEMENT:</strong> This Agreement supersedes all prior agreements and understandings regarding the same.<br />\r\n	<br />\r\n	&nbsp;</p>', '', '', '', '2020-11-22 05:47:12', '2018-04-15 13:27:52'),
(2, 'Privacy Policy', 'privacy_policy', '<p>\r\n	<br />\r\n	<strong>What information do we collect?</strong><br />\r\n	When registering with The Truth Debate, as appropriate, you will be asked to enter your name, e-mail address, date of birth, and other pertinent information. You may, however, visit our site anonymously.<br />\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>How do we use your information?</strong><br />\r\n	Any information we collect from you may be used in one of the following ways:</p>\r\n<ul>\r\n	<li>\r\n		To personalize your experience (your information helps us to more effectively respond to your customer service requests and support needs).</li>\r\n	<li>\r\n		To improve our mobile applications and website (we continually strive to improve our platforms based on the information and feedback we receive from you).</li>\r\n	<li>\r\n		The email address you provide will only be used to send you information and updates pertaining to your subscribed outlets and general site information.</li>\r\n</ul>\r\n<p>\r\n	If you decide to opt-in to our mailing list, you will receive emails that may include company news, updates, related product, or service information, etc.</p>\r\n<p>\r\n	Note: If at any time you would like to unsubscribe from receiving future emails, we include unsubscribe instructions at the bottom of each email.</p>\r\n<p>\r\n	<br />\r\n	<strong>How do we protect your information?</strong><br />\r\n	We implement a variety of security measures to maintain the safety of your personal information. Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access to such systems and are required to keep the information confidential.</p>\r\n<p>\r\n	<br />\r\n	<strong>Do we use cookies?</strong><br />\r\n	Yes, like most websites, we use &lsquo;cookies&rsquo; to enhance your experience, gather general visitor information, and track visits to our website.</p>\r\n<p>\r\n	Cookies are small files that a site or its service provider transfers to your computer&rsquo;s hard drive through your Internet browser (if you allow) that enables the website&rsquo;s or service provider&rsquo;s systems to recognize your browser and capture and remember certain information.</p>\r\n<p>\r\n	We use cookies to compile aggregate data about site traffic and site interaction so that we can offer better website experiences and tools in the future.</p>\r\n<p>\r\n	<br />\r\n	<strong>Do we disclose any information to outside parties?</strong><br />\r\n	We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information, unless we provide you with advanced notice. This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our website policies, or protect ours or others&rsquo; rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>\r\n<p>\r\n	<br />\r\n	<strong>Third party links</strong><br />\r\n	In an attempt to provide you with increased value, we may include third party links on our website. These linked sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our website and welcome any feedback about these sites.</p>\r\n<p>\r\n	<br />\r\n	<strong>Online Privacy Protection Act Compliance</strong><br />\r\n	Because we value your privacy we have taken the necessary precautions to be in compliance with the Online Privacy Protection Act. We therefore will not distribute your personal information to outside parties without your consent.</p>\r\n<p>\r\n	<br />\r\n	<strong>Children&rsquo;s Online Privacy Protection Act Compliance</strong><br />\r\n	We are in compliance with the requirements of COPPA (Children&rsquo;s Online Privacy Protection Act); we do not collect any information from anyone under 13 years of age. Due to the nature of some uploads, The Truth Debate is recommended for people who are at least 16 years old or older.</p>\r\n<p>\r\n	<br />\r\n	<strong>Online Privacy Policy Only</strong><br />\r\n	This online privacy policy applies only to information collected through our website and not to information collected offline.</p>\r\n<p>\r\n	<br />\r\n	<strong>Your Consent</strong><br />\r\n	By using The Truth Debate and all associated properties, you consent to our privacy policy.</p>\r\n<p>\r\n	<br />\r\n	<strong>Changes to our Privacy Policy</strong><br />\r\n	If we decide to change our privacy policy, we will post those changes on this page.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>', '', '', '', '2020-11-22 05:47:36', '2018-04-15 13:29:26'),
(3, 'About Us', 'about_us', '<p dir=\"ltr\" style=\"line-height:1.2;margin-top:6pt;margin-bottom:0pt;\">\r\n	<span id=\"docs-internal-guid-84c7a677-7fff-6297-507b-75a53f2d8c9e\"><span style=\"font-size: 24pt; font-family: Calibri, sans-serif; color: rgb(0, 0, 0); background-color: transparent; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">About</span></span></p>\r\n<p dir=\"ltr\" style=\"line-height:1.2;margin-top:6pt;margin-bottom:0pt;\">\r\n	<span id=\"docs-internal-guid-84c7a677-7fff-6297-507b-75a53f2d8c9e\"><span style=\"font-size: 12pt; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">As parents and caregivers struggle with educational challenges created by COVID-19, Chat At Me! Schools enhances the communication of parents and caregivers with their child&rsquo;s teacher(s) and, possibly more important, other classroom parents and caregivers feeling isolated by the new learning format.&nbsp;</span></span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:0pt;\">\r\n	<span id=\"docs-internal-guid-84c7a677-7fff-6297-507b-75a53f2d8c9e\"><span style=\"font-size: 12pt; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Teachers can record lesson plans, homework help and inspirational videos for their students. Parents and caregivers can then post questions and comments to the videos and the teacher, along with other classroom parents and caregivers, can interact with each other.&nbsp;</span></span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p dir=\"ltr\" style=\"line-height:1.2;margin-top:0pt;margin-bottom:0pt;\">\r\n	<span id=\"docs-internal-guid-84c7a677-7fff-6297-507b-75a53f2d8c9e\"><span style=\"font-size: 12pt; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Parents and caregivers who elect to have their profile public, can chat 1:1 with each other and create/join classroom discussion groups for</span><span style=\"font-size: 13pt; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\"> </span><span style=\"font-size: 12pt; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">homework help, encouragement, and to share best practices. Chats can be via text or video.</span></span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p dir=\"ltr\" style=\"line-height:1.2;margin-top:0pt;margin-bottom:0pt;\">\r\n	<span id=\"docs-internal-guid-84c7a677-7fff-6297-507b-75a53f2d8c9e\"><span style=\"font-size: 12pt; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Chat At Me! Schools asks for the following guidelines to be observed.</span></span></p>\r\n<ul style=\"margin-top:0;margin-bottom:0;\">\r\n	<li background-color:=\"\" color:=\"\" dir=\"ltr\" font-variant-east-asian:=\"\" font-variant-numeric:=\"\" noto=\"\" sans=\"\" style=\"list-style-type: disc; font-size: 12pt; font-family: \" vertical-align:=\"\" white-space:=\"\">\r\n		<p dir=\"ltr\" role=\"presentation\" style=\"line-height:1.2;margin-top:6pt;margin-bottom:0pt;\">\r\n			<span id=\"docs-internal-guid-84c7a677-7fff-6297-507b-75a53f2d8c9e\"><span style=\"font-size: 12pt; font-family: Arial; background-color: transparent; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Hate Speech: </span><span style=\"font-size: 12pt; font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Chat At Me! Schools believes in everyone&rsquo;s right to express their opinion but hate speech will not be tolerated. Hate speech is any content promoting hatred or violence against individuals or groups based on: race or ethnicity, sexual orientation or gender identity, religious belief, age, gender, or veteran status. Hate speech of any kind will result in the account being terminated.</span></span></p>\r\n	</li>\r\n	<li background-color:=\"\" color:=\"\" dir=\"ltr\" font-variant-east-asian:=\"\" font-variant-numeric:=\"\" noto=\"\" sans=\"\" style=\"list-style-type: disc; font-size: 12pt; font-family: \" vertical-align:=\"\" white-space:=\"\">\r\n		<p dir=\"ltr\" role=\"presentation\" style=\"line-height:1.2;margin-top:6pt;margin-bottom:0pt;\">\r\n			<span id=\"docs-internal-guid-84c7a677-7fff-6297-507b-75a53f2d8c9e\"><span style=\"font-size: 12pt; font-family: Arial; background-color: transparent; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Threats, Harassment, Defamation, and Cyberbullying: </span><span style=\"font-size: 12pt; font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Chat At Me! Schools will not tolerate:</span></span></p>\r\n		<ul style=\"margin-top:0;margin-bottom:0;\">\r\n			<li background-color:=\"\" courier=\"\" dir=\"ltr\" font-variant-east-asian:=\"\" font-variant-numeric:=\"\" style=\"list-style-type: disc; font-size: 12pt; font-family: \" vertical-align:=\"\">\r\n				<p dir=\"ltr\" role=\"presentation\" style=\"line-height:1.2;margin-top:0pt;margin-bottom:0pt;\">\r\n					<span id=\"docs-internal-guid-84c7a677-7fff-6297-507b-75a53f2d8c9e\"><span style=\"font-size: 12pt; font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Threatening, stalking, or intimidating comments to individuals or groups;</span></span></p>\r\n			</li>\r\n			<li background-color:=\"\" courier=\"\" dir=\"ltr\" font-variant-east-asian:=\"\" font-variant-numeric:=\"\" style=\"list-style-type: disc; font-size: 12pt; font-family: \" vertical-align:=\"\">\r\n				<p dir=\"ltr\" role=\"presentation\" style=\"line-height:1.2;margin-top:0pt;margin-bottom:0pt;\">\r\n					<span id=\"docs-internal-guid-84c7a677-7fff-6297-507b-75a53f2d8c9e\"><span style=\"font-size: 12pt; font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Abusive comments;</span></span></p>\r\n			</li>\r\n			<li background-color:=\"\" courier=\"\" dir=\"ltr\" font-variant-east-asian:=\"\" font-variant-numeric:=\"\" style=\"list-style-type: disc; font-size: 12pt; font-family: \" vertical-align:=\"\">\r\n				<p dir=\"ltr\" role=\"presentation\" style=\"line-height:1.2;margin-top:0pt;margin-bottom:0pt;\">\r\n					<span id=\"docs-internal-guid-84c7a677-7fff-6297-507b-75a53f2d8c9e\"><span style=\"font-size: 12pt; font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Posts to deliberately humiliate an individual or group of people in any manner;</span></span></p>\r\n			</li>\r\n			<li background-color:=\"\" courier=\"\" dir=\"ltr\" font-variant-east-asian:=\"\" font-variant-numeric:=\"\" style=\"list-style-type: disc; font-size: 12pt; font-family: \" vertical-align:=\"\">\r\n				<p dir=\"ltr\" role=\"presentation\" style=\"line-height:1.2;margin-top:0pt;margin-bottom:0pt;\">\r\n					<span id=\"docs-internal-guid-84c7a677-7fff-6297-507b-75a53f2d8c9e\"><span style=\"font-size: 12pt; font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Sexual references, advances, harassment of any individual or group of people;</span></span></p>\r\n			</li>\r\n			<li background-color:=\"\" courier=\"\" dir=\"ltr\" font-variant-east-asian:=\"\" font-variant-numeric:=\"\" style=\"list-style-type: disc; font-size: 12pt; font-family: \" vertical-align:=\"\">\r\n				<p dir=\"ltr\" role=\"presentation\" style=\"line-height:1.2;margin-top:0pt;margin-bottom:0pt;\">\r\n					<span id=\"docs-internal-guid-84c7a677-7fff-6297-507b-75a53f2d8c9e\"><span style=\"font-size: 12pt; font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Comments designed to incite others to harass, threaten, intimidate, or commit violent acts against any individual or group of people;</span></span></p>\r\n			</li>\r\n			<li background-color:=\"\" courier=\"\" dir=\"ltr\" font-variant-east-asian:=\"\" font-variant-numeric:=\"\" style=\"list-style-type: disc; font-size: 12pt; font-family: \" vertical-align:=\"\">\r\n				<p dir=\"ltr\" role=\"presentation\" style=\"line-height:1.2;margin-top:0pt;margin-bottom:0pt;\">\r\n					<span id=\"docs-internal-guid-84c7a677-7fff-6297-507b-75a53f2d8c9e\"><span style=\"font-size: 12pt; font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Content containing false statements designed to harm someone&rsquo;s reputation. Do not abuse the system with comments intended to defame someone.</span></span></p>\r\n			</li>\r\n		</ul>\r\n	</li>\r\n	<li background-color:=\"\" color:=\"\" dir=\"ltr\" font-variant-east-asian:=\"\" font-variant-numeric:=\"\" noto=\"\" sans=\"\" style=\"list-style-type: disc; font-size: 12pt; font-family: \" vertical-align:=\"\" white-space:=\"\">\r\n		<p dir=\"ltr\" role=\"presentation\" style=\"line-height:1.2;margin-top:6pt;margin-bottom:0pt;\">\r\n			<span id=\"docs-internal-guid-84c7a677-7fff-6297-507b-75a53f2d8c9e\"><span style=\"font-size: 12pt; font-family: Arial; background-color: transparent; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Graphic, Violent, Dangerous, Illegal, or Terrorist Related Content: </span><span style=\"font-size: 12pt; font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Comments graphic or violent in nature to express a viewpoint and comments encouraging dangerous or illegal activities will not be tolerated. Chat At Me! Schools prohibits any commnets designed to promote or celebrate terrorism and terrorist activities, recruit for terrorist activities or organizations, or incite violence against any individual or group of people.</span></span></p>\r\n	</li>\r\n</ul>\r\n<p dir=\"ltr\" style=\"line-height:1.2;margin-top:6pt;margin-bottom:0pt;\">\r\n	<span id=\"docs-internal-guid-84c7a677-7fff-6297-507b-75a53f2d8c9e\"><span style=\"font-size: 12pt; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Comments violating any of the above conditions will result in the account permanently deleted from Chat At Me! Schools and the appropriate authorities notified where warranted. If you feel a comment violates the above conditions, please notify your school as soon as possible and report any inappropriate comment you feel violates the above terms to </span><a href=\"mailto:abuse@chatatme.net\" style=\"text-decoration-line: none;\"><span style=\"font-size: 12pt; font-family: Arial; color: rgb(17, 85, 204); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: underline; text-decoration-skip-ink: none; vertical-align: baseline; white-space: pre-wrap;\">abuse@chatatme.net</span></a><span style=\"font-size: 12pt; font-family: Calibri, sans-serif; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">.</span></span></p>\r\n<div>\r\n	&nbsp;</div>', '', '', '', '2020-11-04 09:45:13', '2018-04-15 13:20:24'),
(4, 'Chat At Me! Schools Information - District', 'Chat_at_me_school_information_district', '<p>\r\n	The Truth&nbsp;Debate was created to provide a forum for all people to discuss, share viewpoints, and defend opinions.&nbsp; The Truth Debate encourages people around the world to interact with each other to stimulate a better understanding of each other&rsquo;s point of view.</p>\r\n<p>\r\n	As users have their own unique beliefs, we ask everyone respect each opinion posted on the website.&nbsp; You can vehemently disagree but do so in a respectful manner and please do not cross the line.&nbsp; The Truth Debate strongly believes in everyone&rsquo;s First Amendment right to free speech but there are common sense guidelines for everyone to follow.</p>\r\n<ul>\r\n	<li>\r\n		<strong>Hate Speech:</strong>&nbsp; We encourage anyone to share their opinion regardless of how unpopular it may be perceived.&nbsp; Hate speech is any content promoting hatred or violence against individuals or groups based on:&nbsp; race or ethnicity, sexual orientation or gender identity, religious belief, age, gender, or veteran status. We encourage people to post if they vehemently disagree with a topic or opinion, just do so respectively.&nbsp; Hate speech of any kind will not be tolerated.&nbsp; Posting will be deleted and account subject to termination.</li>\r\n</ul>\r\n<ul>\r\n	<li>\r\n		<strong>Threats, Harassment, Defamation, and Cyberbullying:</strong>&nbsp; Passionate and opinionated responses to content on the site are encouraged.&nbsp; Content viewed as personally offensive naturally occurs with very diverse viewpoints.&nbsp; Free speech allows us to hold these opinions and will be supported.&nbsp; The Truth Debate will not tolerate any material containing:\r\n		<ul>\r\n			<li>\r\n				Threatening, stalking, or intimidating content to individuals or groups;</li>\r\n			<li>\r\n				Abusive comments;</li>\r\n			<li>\r\n				Posts to deliberately humiliate an individual or group of people in any manner;</li>\r\n			<li>\r\n				Sexual references, advances, harassment of any individual or group of people;</li>\r\n			<li>\r\n				Comments designed to incite others to harass, threaten, intimidate, or commit violent acts against any individual or group of people;</li>\r\n			<li>\r\n				Content containing false statements designed to harm someone&rsquo;s reputation.&nbsp; The Truth Debate exists to promote the free interchange of opinions.&nbsp; Do not abuse the system with comments intended to defame someone.</li>\r\n		</ul>\r\n	</li>\r\n</ul>\r\n<ul>\r\n	<li>\r\n		<strong>Graphic, Violent, Dangerous, Illegal, or Terrorist Related Content:</strong>&nbsp; The Truth Debate is a forum for people to share and discuss opinions.&nbsp; Current news events, political, and social issues, etc. will be posted for debate.&nbsp; Content containing graphic or violent material to express a viewpoint and material encouraging dangerous or illegal activities will not be tolerated.&nbsp; The Truth Debate prohibits any content designed to promote or celebrate terrorism and terrorist activities, recruit for terrorist activities or organizations, or incite violence against any individual or group of people.</li>\r\n</ul>\r\n<ul>\r\n	<li>\r\n		<strong>Nudity and Sexual Content:</strong>&nbsp; Discussing pornography, sexually related issues, addictions, or abuses on The Truth Debate is encouraged.&nbsp; Posting content containing pornography or sexually explicit material is strictly prohibited.&nbsp; Any material exploiting children will be reported to the appropriate law enforcement agencies.</li>\r\n</ul>\r\n<ul>\r\n	<li>\r\n		<strong>Age Appropriate Content:</strong>&nbsp; The Truth Debate is designed all users 16 years of age and older.&nbsp; Parental or guardian oversight should be enacted with all users under 18 years of age as some content may not be appropriate for younger viewers.&nbsp; If a parent or guardian views or listens to a post without a warning label you believe warrants one, please click on the three vertical dots in the upper right hand corner of the upload to report it or use the Contact Us form under the gear icon in the bottom right hand corner; reference the post in question, and detail why you feel a label is justified.&nbsp; A response will be provided within 24 hours.</li>\r\n</ul>\r\n<p>\r\n	Posted material violating any of the above conditions will be removed and the account subject to permanently deletion from The Truth Debate.&nbsp; Report any content you find violating the above terms using the Contact Us form or emailing to <a href=\"mailto:abuse@thetruthdebate.com\">abuse@thetruthdebate.com</a>.</p>\r\n<p>\r\n	&nbsp;</p>', '', '', '', '2020-11-03 09:32:57', '2018-04-15 13:20:24'),
(5, 'Chat At Me! Schools Information - School', 'Chat_at_me_school_information_school', '<p>\r\n	The Truth&nbsp;Debate was created to provide a forum for all people to discuss, share viewpoints, and defend opinions.&nbsp; The Truth Debate encourages people around the world to interact with each other to stimulate a better understanding of each other&rsquo;s point of view.</p>\r\n<p>\r\n	As users have their own unique beliefs, we ask everyone respect each opinion posted on the website.&nbsp; You can vehemently disagree but do so in a respectful manner and please do not cross the line.&nbsp; The Truth Debate strongly believes in everyone&rsquo;s First Amendment right to free speech but there are common sense guidelines for everyone to follow.</p>\r\n<ul>\r\n	<li>\r\n		<strong>Hate Speech:</strong>&nbsp; We encourage anyone to share their opinion regardless of how unpopular it may be perceived.&nbsp; Hate speech is any content promoting hatred or violence against individuals or groups based on:&nbsp; race or ethnicity, sexual orientation or gender identity, religious belief, age, gender, or veteran status. We encourage people to post if they vehemently disagree with a topic or opinion, just do so respectively.&nbsp; Hate speech of any kind will not be tolerated.&nbsp; Posting will be deleted and account subject to termination.</li>\r\n</ul>\r\n<ul>\r\n	<li>\r\n		<strong>Threats, Harassment, Defamation, and Cyberbullying:</strong>&nbsp; Passionate and opinionated responses to content on the site are encouraged.&nbsp; Content viewed as personally offensive naturally occurs with very diverse viewpoints.&nbsp; Free speech allows us to hold these opinions and will be supported.&nbsp; The Truth Debate will not tolerate any material containing:\r\n		<ul>\r\n			<li>\r\n				Threatening, stalking, or intimidating content to individuals or groups;</li>\r\n			<li>\r\n				Abusive comments;</li>\r\n			<li>\r\n				Posts to deliberately humiliate an individual or group of people in any manner;</li>\r\n			<li>\r\n				Sexual references, advances, harassment of any individual or group of people;</li>\r\n			<li>\r\n				Comments designed to incite others to harass, threaten, intimidate, or commit violent acts against any individual or group of people;</li>\r\n			<li>\r\n				Content containing false statements designed to harm someone&rsquo;s reputation.&nbsp; The Truth Debate exists to promote the free interchange of opinions.&nbsp; Do not abuse the system with comments intended to defame someone.</li>\r\n		</ul>\r\n	</li>\r\n</ul>\r\n<ul>\r\n	<li>\r\n		<strong>Graphic, Violent, Dangerous, Illegal, or Terrorist Related Content:</strong>&nbsp; The Truth Debate is a forum for people to share and discuss opinions.&nbsp; Current news events, political, and social issues, etc. will be posted for debate.&nbsp; Content containing graphic or violent material to express a viewpoint and material encouraging dangerous or illegal activities will not be tolerated.&nbsp; The Truth Debate prohibits any content designed to promote or celebrate terrorism and terrorist activities, recruit for terrorist activities or organizations, or incite violence against any individual or group of people.</li>\r\n</ul>\r\n<ul>\r\n	<li>\r\n		<strong>Nudity and Sexual Content:</strong>&nbsp; Discussing pornography, sexually related issues, addictions, or abuses on The Truth Debate is encouraged.&nbsp; Posting content containing pornography or sexually explicit material is strictly prohibited.&nbsp; Any material exploiting children will be reported to the appropriate law enforcement agencies.</li>\r\n</ul>\r\n<ul>\r\n	<li>\r\n		<strong>Age Appropriate Content:</strong>&nbsp; The Truth Debate is designed all users 16 years of age and older.&nbsp; Parental or guardian oversight should be enacted with all users under 18 years of age as some content may not be appropriate for younger viewers.&nbsp; If a parent or guardian views or listens to a post without a warning label you believe warrants one, please click on the three vertical dots in the upper right hand corner of the upload to report it or use the Contact Us form under the gear icon in the bottom right hand corner; reference the post in question, and detail why you feel a label is justified.&nbsp; A response will be provided within 24 hours.</li>\r\n</ul>\r\n<p>\r\n	Posted material violating any of the above conditions will be removed and the account subject to permanently deletion from The Truth Debate.&nbsp; Report any content you find violating the above terms using the Contact Us form or emailing to <a href=\"mailto:abuse@thetruthdebate.com\">abuse@thetruthdebate.com</a>.</p>\r\n<p>\r\n	&nbsp;</p>', '', '', '', '2020-11-03 09:32:57', '2018-04-15 13:20:24');
INSERT INTO `pages` (`page_id`, `title`, `page_slug`, `content`, `meta_title`, `meta_description`, `meta_keyword`, `created`, `modified`) VALUES
(6, 'Chat At Me! Schools Information - Teacher', 'Chat_at_me_school_information_teacher', '<p>\r\n	The Truth&nbsp;Debate was created to provide a forum for all people to discuss, share viewpoints, and defend opinions.&nbsp; The Truth Debate encourages people around the world to interact with each other to stimulate a better understanding of each other&rsquo;s point of view.</p>\r\n<p>\r\n	As users have their own unique beliefs, we ask everyone respect each opinion posted on the website.&nbsp; You can vehemently disagree but do so in a respectful manner and please do not cross the line.&nbsp; The Truth Debate strongly believes in everyone&rsquo;s First Amendment right to free speech but there are common sense guidelines for everyone to follow.</p>\r\n<ul>\r\n	<li>\r\n		<strong>Hate Speech:</strong>&nbsp; We encourage anyone to share their opinion regardless of how unpopular it may be perceived.&nbsp; Hate speech is any content promoting hatred or violence against individuals or groups based on:&nbsp; race or ethnicity, sexual orientation or gender identity, religious belief, age, gender, or veteran status. We encourage people to post if they vehemently disagree with a topic or opinion, just do so respectively.&nbsp; Hate speech of any kind will not be tolerated.&nbsp; Posting will be deleted and account subject to termination.</li>\r\n</ul>\r\n<ul>\r\n	<li>\r\n		<strong>Threats, Harassment, Defamation, and Cyberbullying:</strong>&nbsp; Passionate and opinionated responses to content on the site are encouraged.&nbsp; Content viewed as personally offensive naturally occurs with very diverse viewpoints.&nbsp; Free speech allows us to hold these opinions and will be supported.&nbsp; The Truth Debate will not tolerate any material containing:\r\n		<ul>\r\n			<li>\r\n				Threatening, stalking, or intimidating content to individuals or groups;</li>\r\n			<li>\r\n				Abusive comments;</li>\r\n			<li>\r\n				Posts to deliberately humiliate an individual or group of people in any manner;</li>\r\n			<li>\r\n				Sexual references, advances, harassment of any individual or group of people;</li>\r\n			<li>\r\n				Comments designed to incite others to harass, threaten, intimidate, or commit violent acts against any individual or group of people;</li>\r\n			<li>\r\n				Content containing false statements designed to harm someone&rsquo;s reputation.&nbsp; The Truth Debate exists to promote the free interchange of opinions.&nbsp; Do not abuse the system with comments intended to defame someone.</li>\r\n		</ul>\r\n	</li>\r\n</ul>\r\n<ul>\r\n	<li>\r\n		<strong>Graphic, Violent, Dangerous, Illegal, or Terrorist Related Content:</strong>&nbsp; The Truth Debate is a forum for people to share and discuss opinions.&nbsp; Current news events, political, and social issues, etc. will be posted for debate.&nbsp; Content containing graphic or violent material to express a viewpoint and material encouraging dangerous or illegal activities will not be tolerated.&nbsp; The Truth Debate prohibits any content designed to promote or celebrate terrorism and terrorist activities, recruit for terrorist activities or organizations, or incite violence against any individual or group of people.</li>\r\n</ul>\r\n<ul>\r\n	<li>\r\n		<strong>Nudity and Sexual Content:</strong>&nbsp; Discussing pornography, sexually related issues, addictions, or abuses on The Truth Debate is encouraged.&nbsp; Posting content containing pornography or sexually explicit material is strictly prohibited.&nbsp; Any material exploiting children will be reported to the appropriate law enforcement agencies.</li>\r\n</ul>\r\n<ul>\r\n	<li>\r\n		<strong>Age Appropriate Content:</strong>&nbsp; The Truth Debate is designed all users 16 years of age and older.&nbsp; Parental or guardian oversight should be enacted with all users under 18 years of age as some content may not be appropriate for younger viewers.&nbsp; If a parent or guardian views or listens to a post without a warning label you believe warrants one, please click on the three vertical dots in the upper right hand corner of the upload to report it or use the Contact Us form under the gear icon in the bottom right hand corner; reference the post in question, and detail why you feel a label is justified.&nbsp; A response will be provided within 24 hours.</li>\r\n</ul>\r\n<p>\r\n	Posted material violating any of the above conditions will be removed and the account subject to permanently deletion from The Truth Debate.&nbsp; Report any content you find violating the above terms using the Contact Us form or emailing to <a href=\"mailto:abuse@thetruthdebate.com\">abuse@thetruthdebate.com</a>.</p>\r\n<p>\r\n	&nbsp;</p>', '', '', '', '2020-11-03 09:32:57', '2018-04-15 13:20:24'),
(7, 'Chat At Me! Schools Information - Parent', 'Chat_at_me_school_information_parent', '<p>\r\n	The Truth&nbsp;Debate was created to provide a forum for all people to discuss, share viewpoints, and defend opinions.&nbsp; The Truth Debate encourages people around the world to interact with each other to stimulate a better understanding of each other&rsquo;s point of view.</p>\r\n<p>\r\n	As users have their own unique beliefs, we ask everyone respect each opinion posted on the website.&nbsp; You can vehemently disagree but do so in a respectful manner and please do not cross the line.&nbsp; The Truth Debate strongly believes in everyone&rsquo;s First Amendment right to free speech but there are common sense guidelines for everyone to follow.</p>\r\n<ul>\r\n	<li>\r\n		<strong>Hate Speech:</strong>&nbsp; We encourage anyone to share their opinion regardless of how unpopular it may be perceived.&nbsp; Hate speech is any content promoting hatred or violence against individuals or groups based on:&nbsp; race or ethnicity, sexual orientation or gender identity, religious belief, age, gender, or veteran status. We encourage people to post if they vehemently disagree with a topic or opinion, just do so respectively.&nbsp; Hate speech of any kind will not be tolerated.&nbsp; Posting will be deleted and account subject to termination.</li>\r\n</ul>\r\n<ul>\r\n	<li>\r\n		<strong>Threats, Harassment, Defamation, and Cyberbullying:</strong>&nbsp; Passionate and opinionated responses to content on the site are encouraged.&nbsp; Content viewed as personally offensive naturally occurs with very diverse viewpoints.&nbsp; Free speech allows us to hold these opinions and will be supported.&nbsp; The Truth Debate will not tolerate any material containing:\r\n		<ul>\r\n			<li>\r\n				Threatening, stalking, or intimidating content to individuals or groups;</li>\r\n			<li>\r\n				Abusive comments;</li>\r\n			<li>\r\n				Posts to deliberately humiliate an individual or group of people in any manner;</li>\r\n			<li>\r\n				Sexual references, advances, harassment of any individual or group of people;</li>\r\n			<li>\r\n				Comments designed to incite others to harass, threaten, intimidate, or commit violent acts against any individual or group of people;</li>\r\n			<li>\r\n				Content containing false statements designed to harm someone&rsquo;s reputation.&nbsp; The Truth Debate exists to promote the free interchange of opinions.&nbsp; Do not abuse the system with comments intended to defame someone.</li>\r\n		</ul>\r\n	</li>\r\n</ul>\r\n<ul>\r\n	<li>\r\n		<strong>Graphic, Violent, Dangerous, Illegal, or Terrorist Related Content:</strong>&nbsp; The Truth Debate is a forum for people to share and discuss opinions.&nbsp; Current news events, political, and social issues, etc. will be posted for debate.&nbsp; Content containing graphic or violent material to express a viewpoint and material encouraging dangerous or illegal activities will not be tolerated.&nbsp; The Truth Debate prohibits any content designed to promote or celebrate terrorism and terrorist activities, recruit for terrorist activities or organizations, or incite violence against any individual or group of people.</li>\r\n</ul>\r\n<ul>\r\n	<li>\r\n		<strong>Nudity and Sexual Content:</strong>&nbsp; Discussing pornography, sexually related issues, addictions, or abuses on The Truth Debate is encouraged.&nbsp; Posting content containing pornography or sexually explicit material is strictly prohibited.&nbsp; Any material exploiting children will be reported to the appropriate law enforcement agencies.</li>\r\n</ul>\r\n<ul>\r\n	<li>\r\n		<strong>Age Appropriate Content:</strong>&nbsp; The Truth Debate is designed all users 16 years of age and older.&nbsp; Parental or guardian oversight should be enacted with all users under 18 years of age as some content may not be appropriate for younger viewers.&nbsp; If a parent or guardian views or listens to a post without a warning label you believe warrants one, please click on the three vertical dots in the upper right hand corner of the upload to report it or use the Contact Us form under the gear icon in the bottom right hand corner; reference the post in question, and detail why you feel a label is justified.&nbsp; A response will be provided within 24 hours.</li>\r\n</ul>\r\n<p>\r\n	Posted material violating any of the above conditions will be removed and the account subject to permanently deletion from The Truth Debate.&nbsp; Report any content you find violating the above terms using the Contact Us form or emailing to <a href=\"mailto:abuse@thetruthdebate.com\">abuse@thetruthdebate.com</a>.</p>\r\n<p>\r\n	&nbsp;</p>', '', '', '', '2020-11-17 13:52:32', '2018-04-15 13:20:24');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `hide` tinyint(4) NOT NULL COMMENT '1=hide for parent roles not edited by any user',
  `type` enum('Admin','Facility','Agency','User') NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime NOT NULL,
  `modified_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `parent_id`, `hide`, `type`, `name`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 0, 0, 'Admin', 'Manager', 'Active', '2019-11-07 18:38:39', 1, '2020-05-08 06:41:01', 1),
(2, 0, 0, 'Admin', 'Test Role live', 'Active', '2020-01-30 11:49:17', 1, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `add` tinyint(4) NOT NULL,
  `edit` tinyint(4) NOT NULL,
  `delete` tinyint(4) NOT NULL,
  `view` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`id`, `role_id`, `section_id`, `add`, `edit`, `delete`, `view`, `created`, `modified`) VALUES
(1, 1, 1, 0, 0, 0, 1, '2020-08-26 07:15:18', '2020-08-26 07:15:18'),
(2, 1, 2, 0, 0, 0, 1, '2020-08-26 07:15:19', '2020-08-26 07:15:19'),
(3, 1, 3, 0, 0, 0, 1, '2020-08-26 07:15:19', '2020-08-26 07:15:19'),
(4, 1, 4, 0, 0, 0, 1, '2020-08-26 07:15:19', '2020-08-26 07:15:19'),
(5, 1, 5, 0, 0, 0, 0, '2020-08-26 07:15:19', '2020-08-26 07:15:19'),
(6, 1, 6, 0, 0, 0, 0, '2020-08-26 07:15:19', '2020-08-26 07:15:19'),
(7, 1, 7, 0, 0, 0, 0, '2020-08-26 07:15:19', '2020-08-26 07:15:19'),
(8, 1, 8, 0, 0, 0, 1, '2020-08-26 07:15:19', '2020-08-26 07:15:19'),
(9, 1, 9, 0, 0, 0, 1, '2020-08-26 07:15:19', '2020-08-26 07:15:19'),
(10, 1, 10, 0, 0, 0, 0, '2020-08-26 07:15:19', '2020-08-26 07:15:19'),
(11, 1, 11, 0, 0, 0, 0, '2020-08-26 07:15:19', '2020-08-26 07:15:19'),
(12, 1, 12, 0, 0, 0, 0, '2020-08-26 07:15:19', '2020-08-26 07:15:19'),
(13, 1, 13, 0, 0, 0, 0, '2020-08-26 07:15:19', '2020-08-26 07:15:19'),
(14, 1, 14, 0, 0, 0, 0, '2020-08-26 07:15:19', '2020-08-26 07:15:19'),
(15, 1, 15, 0, 0, 0, 0, '2020-08-26 07:15:19', '2020-08-26 07:15:19'),
(16, 1, 16, 0, 0, 0, 0, '2020-08-26 07:15:19', '2020-08-26 07:15:19');

-- --------------------------------------------------------

--
-- Table structure for table `school`
--

CREATE TABLE `school` (
  `id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `school`
--

INSERT INTO `school` (`id`, `state_id`, `district_id`, `name`, `address`, `logo`, `created`) VALUES
(1, 1, 3, 'Bharat 123', '', '', '2020-11-03 13:50:32'),
(2, 1, 3, 'Bharat 1234', 'Sdfsdfdsfsdf sdfsdfdsfas adsjfhjkadsf jkahdfjkdhsaf kjasdhfhjksd ajkdhfjksd', 'resources/images/school/9a7bef8aaf00e4d68c38712a59f7b338.jpg', '2020-11-03 13:58:12'),
(3, 1, 5, 'School', 'School Address.', 'resources/images/school/f411dff0ea9d1af1342c36c82f147286.jpg', '2020-11-04 04:52:19'),
(4, 2, 7, 'Martin king school', '3988 S Hobart Blvd, Los Angeles, CA 90062', '', '2020-11-04 09:04:53'),
(6, 2, 1, 'my dummy school', '', '', '2020-11-05 07:50:22'),
(7, 2, 1, 'dummy school 123', 'Vijay nager indored', 'resources/images/school/f4f91e37fb8760cd7c102fd67f0484a1.svg', '2020-11-05 08:04:40'),
(8, 1, 13, 'bharat school', 'Chchchcjc', 'resources/images/school/7bd16834954a4fa945c9404460f2cff1.jpg', '2020-11-05 14:42:09'),
(9, 2, 1, 'IPS  Acadmy', '', '', '2020-11-06 07:46:29'),
(10, 2, 1, 'IPS Acadmy', '', '', '2020-11-06 07:54:34'),
(11, 2, 1, 'New Era public school', '', '', '2020-11-06 07:56:58'),
(12, 2, 1, 'St. xeviour', 'test', '', '2020-11-06 08:58:37'),
(13, 1, 13, 'Schoole1', 'Schoole', 'resources/images/school/efa7cdfebb79296880c84704acaeba26.jpg', '2020-11-07 06:02:04'),
(14, 2, 3, 'primary school', 'test', '', '2020-11-08 05:54:55'),
(15, 2, 7, 'Bings convent', '', '', '2020-11-08 11:48:50'),
(16, 2, 1, 'test user', 'test', '', '2020-11-08 13:28:20'),
(17, 9, 15, 'Bharat School', '', '', '2020-11-09 05:27:21'),
(18, 9, 15, 'Bharat School 123', 'Indirect Bypasss', 'resources/images/school/a08a7fff3b4b19a6eb620d734db172ed.jpg', '2020-11-09 05:28:01'),
(19, 9, 15, 'Bhart School New123', 'Bharat indorse Vijay nagar', 'resources/images/school/101da09cf1fc7bf605f135ed9bffc854.jpg', '2020-11-09 12:50:07'),
(20, 1, 3, 'PRe primery', '', '', '2020-11-09 13:18:35'),
(21, 2, 1, 'Juniour KG', 'Address', 'resources/images/school/bfe4a3fd2ae8d1c99cd5116ec52b6d17.jpg', '2020-11-09 13:20:20'),
(22, 2, 16, 'Bharat new school 1', 'Cucù cucù', 'resources/images/school/5fa0a96bd19477a05f17d59bff046b9a.jpg', '2020-11-10 09:39:13'),
(23, 2, 16, 'Bharat school 2', '', '', '2020-11-10 09:43:25'),
(24, 2, 16, 'Bharat schofjfifificuc', '', '', '2020-11-10 09:52:46'),
(25, 2, 16, 'Cuccoycyocu', '', '', '2020-11-10 09:55:42'),
(26, 2, 16, 'Cuccoycyocufufufu', '', '', '2020-11-10 09:58:37'),
(27, 2, 1, 'Merry st. School', 'Xxxxx, xxxxxxxx, 243345', 'resources/images/school/16050097725faa816cac9211373636839.png', '2020-11-10 12:02:52'),
(28, 2, 1, 'Maxi Primary School', 'Yyyyy, yyyyyyy, 1232434', 'resources/images/school/16050097915faa817f8e43f881743211.png', '2020-11-10 12:03:11'),
(29, 2, 1, 'Merry st. School4', '1234 Main Street, Anycity, CA 99999', 'resources/images/school/16050108845faa85c48d9f526364251.png', '2020-11-10 12:21:24'),
(30, 2, 1, 'Merry st. School5', '1234 Main Street, Anycity, CA 99999', 'resources/images/school/16050108925faa85cc58af31077943331.png', '2020-11-10 12:21:32'),
(31, 2, 1, 'Merry st. School6', '1234 Main Street, Anycity, CA 99999', 'resources/images/school/16050109005faa85d464b17219110212.png', '2020-11-10 12:21:40'),
(32, 2, 1, 'Merry st. School7', '1234 Main Street, Anycity, CA 99999', 'resources/images/school/16050109085faa85dc0ddb6424188807.png', '2020-11-10 12:21:48'),
(33, 2, 1, 'Maxi Primary School2', '1234 Main Street, Anycity, CA 99999', 'resources/images/school/16050109155faa85e3cb772262148363.png', '2020-11-10 12:21:55'),
(34, 2, 1, 'Maxi Primary School3', '1234 Main Street, Anycity, CA 99999', 'resources/images/school/16050109205faa85e8476a2520358492.png', '2020-11-10 12:22:00'),
(35, 2, 1, 'Sample1', '', '', '2020-11-10 12:23:31'),
(36, 2, 1, 'Merry st. School41', '1234 Main Street, Anycity, CA 99999', 'resources/images/school/16050120955faa8a7f8f75b866066479.png', '2020-11-10 12:41:35'),
(37, 2, 1, 'Merry st. School51', '1234 Main Street, Anycity, CA 99999', 'resources/images/school/16050121065faa8a8a46f50417419750.png', '2020-11-10 12:41:46'),
(38, 2, 1, 'Merry st. School61', '1234 Main Street, Anycity, CA 99999', 'resources/images/school/16050121115faa8a8f5dec2448084541.png', '2020-11-10 12:41:51'),
(39, 2, 1, 'Merry st. School71', '1234 Main Street, Anycity, CA 99999', 'resources/images/school/16050121215faa8a994873c1696094260.png', '2020-11-10 12:42:01'),
(40, 2, 1, 'Maxi Primary School21', '1234 Main Street, Anycity, CA 99999', 'resources/images/school/16050121305faa8aa2aa9f71319333817.png', '2020-11-10 12:42:10'),
(41, 2, 1, 'Maxi Primary School31', '1234 Main Street, Anycity, CA 99999', 'resources/images/school/16050121365faa8aa86363b2120940862.png', '2020-11-10 12:42:16'),
(42, 2, 1, 'Sample11', '', '', '2020-11-10 12:42:24'),
(43, 2, 1, 'childhood school', 'zexa square', 'resources/images/school/16050899395fabba933398d249665189.png', '2020-11-11 10:18:59'),
(44, 2, 1, 'Teen school', 'zexa square', '', '2020-11-11 10:45:25'),
(45, 2, 1, 'Boris Elementary', '1234 Main Street, Anytown, USA 12345', '', '2020-11-11 14:02:21'),
(46, 2, 1, 'Alta Sierra Middle School', '1235 Main Street, Anytown, USA 12345', '', '2020-11-11 14:09:50'),
(47, 2, 1, 'Clovis East HS', '1236 Main Street, Anytown, USA 12345', '', '2020-11-11 14:09:59'),
(48, 2, 1, 'Addicott Elementary', '1237 Main Street, Anytown, USA 12345', 'resources/images/school/16051038165fabf0c88fcb41031716297.png', '2020-11-11 14:10:16'),
(49, 2, 1, 'Baird Middle School', '1238 Main Street, Anytown, USA 12345', 'resources/images/school/16051038335fabf0d9d5de41073809456.png', '2020-11-11 14:10:33'),
(50, 2, 1, 'Cambridge High School', '1239 Main Street, Anytown, USA 12345', 'resources/images/school/16051038505fabf0ea366171311077702.png', '2020-11-11 14:10:50'),
(51, 2, 1, 'ABC 6', '', '', '2020-11-11 14:10:57'),
(52, 2, 1, 'shama mie', '', 'resources/images/school/16055183705fb24422eeb731219854116.png', '2020-11-16 09:19:30'),
(53, 2, 1, 'masaba fine', '', '', '2020-11-16 09:19:39'),
(54, 2, 1, 'st treoso high ', '', 'resources/images/school/16055183895fb244353b824206952641.png', '2020-11-16 09:19:49'),
(55, 2, 1, 'occians high school ', '', 'resources/images/school/16055183985fb2443ec6ee5642728787.png', '2020-11-16 09:19:58'),
(56, 2, 1, 'new eras public school', 'etst', '', '2020-11-17 10:10:50'),
(57, 2, 1, 'Kids zone', 'er', '', '2020-11-17 10:58:57'),
(58, 2, 16, 'Bharat SC', '', '', '2020-11-18 05:57:15'),
(59, 2, 1, 'shama mie1', '', 'resources/images/school/16056984645fb503a00e29d1213775637.png', '2020-11-18 11:21:04'),
(60, 2, 1, 'masaba fine1', '', '', '2020-11-18 11:21:04'),
(61, 2, 1, 'st treoso high 1', '', 'resources/images/school/16056984675fb503a308c3c1585697714.png', '2020-11-18 11:21:07'),
(62, 2, 1, 'occians high school 1', '', 'resources/images/school/16056984675fb503a3dd57b2118530585.png', '2020-11-18 11:21:07'),
(63, 2, 1, 'occians high school 2', '', 'resources/images/school/16056998985fb5093a04933394848777.png', '2020-11-18 11:44:58'),
(64, 2, 1, 'IPS', '12th n broad st', '', '2020-11-18 14:13:12'),
(65, 2, 1, 'Lincoln ISD 123', '12th north broad st', 'resources/images/school/16057672485fb610500a9d3450588260.png', '2020-11-19 06:27:28'),
(66, 2, 1, 'Franklin', '', 'resources/images/school/16057672525fb61054527a2832429976.png', '2020-11-19 06:27:32'),
(67, 2, 1, 'Lincoln ISD 1234', '12th north broad st', 'resources/images/school/16057858115fb658d33475f722166925.png', '2020-11-19 11:36:51'),
(68, 2, 1, 'Lincoln ISD 12349', '12th north broad st', 'resources/images/school/16057859295fb65949b61ef383171494.png', '2020-11-19 11:38:49'),
(69, 2, 1, 'Manas convent', 'nirmala colony', 'resources/images/school/16057861145fb65a0205e6f1021524045.png', '2020-11-19 11:41:54'),
(70, 2, 1, 'Gurger  st. School', 'manas villa building 102', 'resources/images/school/16057861215fb65a09d8f7b833763193.png', '2020-11-19 11:42:01'),
(71, 2, 1, 'Nirmala convent ', 'Aranya nagar', 'resources/images/school/16057861315fb65a1376ddd34050166.png', '2020-11-19 11:42:11'),
(72, 2, 1, 'shama mie23', '', 'error: Could not resolve host: www.fusd.net', '2020-11-19 12:26:37'),
(73, 2, 1, 'masaba fine20', '', 'error: Illegal characters found in URL', '2020-11-19 12:26:41'),
(74, 2, 1, 'shama mie231', '', 'resources/images/school/16057895155fb6674beb17b1141679859.png', '2020-11-19 12:38:36'),
(75, 2, 1, 'masaba fine201', '', 'resources/images/school/16057895245fb66754054e2537253079.png', '2020-11-19 12:38:44'),
(76, 2, 1, 'shama mie2311', '', 'resources/images/school/16057942805fb679e804cca1085301446.png', '2020-11-19 13:58:00'),
(77, 2, 1, 'masaba fine2011', '', 'resources/images/school/16057942845fb679ec9a8c51837110259.png', '2020-11-19 13:58:04'),
(78, 2, 1, 'total new', '', 'resources/images/school/16057942845fb679ec9a8c51837110259.png', '2020-11-19 13:58:04'),
(79, 14, 19, 'Bharat st. School', 'Indore new', '', '2020-11-21 12:15:42'),
(80, 2, 1, 'Juneja high school', 'Vijaynagar', 'resources/images/school/16060352305fba271ed44221825452859.png', '2020-11-22 08:53:51'),
(81, 2, 1, 'Progressive high school', 'Satysai', 'resources/images/school/16060355315fba284bbb7fa1168174077.png', '2020-11-22 08:58:53'),
(82, 2, 1, 'Ninja high school', '', 'resources/images/school/16060355335fba284dbc7271599295920.png', '2020-11-22 08:58:54'),
(83, 2, 1, 'Shiukunj High school', '78 square', '', '2020-11-22 09:08:55'),
(84, 2, 1, 'Greenfield school', 'Near Satysai', 'resources/images/school/16060361355fba2aa7363241854081022.png', '2020-11-22 09:08:55');

-- --------------------------------------------------------

--
-- Table structure for table `school_grade`
--

CREATE TABLE `school_grade` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `grade` varchar(10) NOT NULL,
  `grade_display_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `school_grade`
--

INSERT INTO `school_grade` (`id`, `school_id`, `district_id`, `grade`, `grade_display_name`) VALUES
(1, 1, 3, '0', 'Kindergarten'),
(2, 1, 3, '1', 'First Grade'),
(3, 1, 3, '2', 'Second Grade'),
(4, 2, 3, '0', 'Kindergarten'),
(5, 2, 3, '1', 'First Grade'),
(6, 2, 3, '2', 'Second Grade'),
(7, 3, 5, '0', 'Kindergarten'),
(8, 3, 5, '1', 'First Grade'),
(9, 3, 5, '2', 'Second Grade'),
(10, 3, 5, '3', 'Third Grade'),
(11, 3, 5, '4', 'Fourth Grade'),
(12, 3, 5, '5', 'Fifth Grade'),
(13, 3, 5, '6', 'Six Grade'),
(14, 3, 5, '7', 'Seventh Grade'),
(15, 3, 5, '8', 'Eighth Grade'),
(16, 3, 5, '9', 'Ninth Grade'),
(17, 3, 5, '10', 'Tenth Grade'),
(18, 3, 5, '11', 'Eleventh Grade'),
(19, 4, 7, '0', 'Kindergarten'),
(20, 4, 7, '1', 'First Grade'),
(21, 4, 7, '2', 'Second Grade'),
(22, 4, 7, '3', 'Third Grade'),
(23, 4, 7, '4', 'Fourth Grade'),
(24, 4, 7, '5', 'Fifth Grade'),
(25, 4, 7, '6', 'Six Grade'),
(26, 4, 7, '7', 'Seventh Grade'),
(27, 4, 7, '8', 'Eighth Grade'),
(28, 4, 7, '9', 'Ninth Grade'),
(29, 5, 1, '1', 'First Grade'),
(30, 6, 1, '1', 'First Grade'),
(31, 7, 1, '1', 'First Grade'),
(32, 7, 1, '2', 'Second Grade'),
(33, 8, 13, '0', 'Kindergarten'),
(34, 8, 13, '1', 'First Grade'),
(35, 8, 13, '2', 'Second Grade'),
(36, 10, 1, '0', 'Kindergarten'),
(37, 10, 1, '1', 'First Grade'),
(38, 10, 1, '2', 'Second Grade'),
(39, 11, 1, '0', 'Kindergarten'),
(40, 11, 1, '1', 'First Grade'),
(41, 11, 1, '2', 'Second Grade'),
(42, 11, 1, '3', 'Third Grade'),
(43, 12, 1, '1', 'First Grade'),
(44, 12, 1, '2', 'Second Grade'),
(45, 12, 1, '3', 'Third Grade'),
(46, 13, 13, '0', 'Kindergarten'),
(47, 13, 13, '1', 'First Grade'),
(48, 13, 13, '2', 'Second Grade'),
(49, 13, 13, '3', 'Third Grade'),
(50, 13, 13, '4', 'Fourth Grade'),
(51, 13, 13, '5', 'Fifth Grade'),
(52, 13, 13, '6', 'Six Grade'),
(53, 7, 1, '4', 'Fourth Grade'),
(54, 14, 3, '0', 'Kindergarten'),
(55, 14, 3, '1', 'First Grade'),
(56, 14, 3, '2', 'Second Grade'),
(57, 14, 3, '3', 'Third Grade'),
(58, 14, 3, '4', 'Fourth Grade'),
(59, 14, 3, '5', 'Fifth Grade'),
(60, 14, 3, '6', 'Six Grade'),
(61, 14, 3, '7', 'Seventh Grade'),
(62, 14, 3, '8', 'Eighth Grade'),
(63, 14, 3, '9', 'Ninth Grade'),
(64, 14, 3, '10', 'Tenth Grade'),
(65, 15, 7, '1', 'First Grade'),
(66, 15, 7, '2', 'Second Grade'),
(67, 16, 1, '1', 'First Grade'),
(68, 16, 1, '2', 'Second Grade'),
(69, 17, 15, '0', 'Kindergarten'),
(70, 17, 15, '1', 'First Grade'),
(71, 17, 15, '2', 'Second Grade'),
(72, 17, 15, '10', 'Tenth Grade'),
(73, 17, 15, '11', 'Eleventh Grade'),
(74, 17, 15, '12', 'Twelfth Grade'),
(75, 18, 15, '0', 'Kindergarten'),
(76, 18, 15, '1', 'First Grade'),
(77, 18, 15, '2', 'Second Grade'),
(78, 18, 15, '10', 'Tenth Grade'),
(79, 18, 15, '11', 'Eleventh Grade'),
(80, 18, 15, '12', 'Twelfth Grade'),
(81, 19, 15, '2', 'Second Grade'),
(82, 19, 15, '0', 'Kindergarten'),
(83, 19, 15, '1', 'First Grade'),
(84, 19, 15, '6', 'Six Grade'),
(85, 20, 3, '0', 'Kindergarten'),
(86, 20, 3, '1', 'First Grade'),
(87, 20, 3, '2', 'Second Grade'),
(88, 21, 1, '0', 'Kindergarten'),
(89, 21, 1, '1', 'First Grade'),
(90, 22, 16, '8', 'Eighth Grade'),
(91, 22, 16, '9', 'Ninth Grade'),
(92, 22, 16, '0', 'Kindergarten'),
(93, 22, 16, '1', 'First Grade'),
(94, 22, 16, '2', 'Second Grade'),
(95, 22, 16, '3', 'Third Grade'),
(96, 22, 16, '4', 'Fourth Grade'),
(97, 22, 16, '10', 'Tenth Grade'),
(98, 23, 16, '3', 'Third Grade'),
(99, 23, 16, '2', 'Second Grade'),
(100, 23, 16, '0', 'Kindergarten'),
(101, 23, 16, '9', 'Ninth Grade'),
(102, 23, 16, '10', 'Tenth Grade'),
(103, 23, 16, '11', 'Eleventh Grade'),
(104, 24, 16, '3', 'Third Grade'),
(105, 24, 16, '2', 'Second Grade'),
(106, 24, 16, '0', 'Kindergarten'),
(107, 24, 16, '9', 'Ninth Grade'),
(108, 24, 16, '10', 'Tenth Grade'),
(109, 24, 16, '11', 'Eleventh Grade'),
(110, 25, 16, '4', 'Fourth Grade'),
(111, 25, 16, '6', 'Six Grade'),
(112, 25, 16, '8', 'Eighth Grade'),
(113, 25, 16, '10', 'Tenth Grade'),
(114, 25, 16, '3', 'Third Grade'),
(115, 25, 16, '1', 'First Grade'),
(116, 26, 16, '4', 'Fourth Grade'),
(117, 26, 16, '6', 'Six Grade'),
(118, 26, 16, '8', 'Eighth Grade'),
(119, 26, 16, '10', 'Tenth Grade'),
(120, 26, 16, '3', 'Third Grade'),
(121, 26, 16, '1', 'First Grade'),
(122, 27, 1, '0', 'Kindergarten'),
(123, 27, 1, '1', 'First Grade'),
(124, 27, 1, '2', 'Second Grade'),
(125, 27, 1, '3', 'Third Grade'),
(126, 27, 1, '4', 'Fourth Grade'),
(127, 27, 1, '5', 'Fifth Grade'),
(128, 27, 1, '6', 'Six Grade'),
(129, 27, 1, '7', 'Seventh Grade'),
(130, 27, 1, '8', 'Eighth Grade'),
(131, 27, 1, '9', 'Ninth Grade'),
(132, 27, 1, '10', 'Tenth Grade'),
(133, 27, 1, '11', 'Eleventh Grade'),
(134, 27, 1, '12', 'Twelfth Grade'),
(135, 28, 1, '0', 'Kindergarten'),
(136, 28, 1, '1', 'First Grade'),
(137, 28, 1, '2', 'Second Grade'),
(138, 28, 1, '3', 'Third Grade'),
(139, 28, 1, '4', 'Fourth Grade'),
(140, 28, 1, '5', 'Fifth Grade'),
(141, 28, 1, '6', 'Six Grade'),
(142, 28, 1, '7', 'Seventh Grade'),
(143, 28, 1, '8', 'Eighth Grade'),
(144, 28, 1, '9', 'Ninth Grade'),
(145, 28, 1, '10', 'Tenth Grade'),
(146, 29, 1, '0', 'Kindergarten'),
(147, 29, 1, '1', 'First Grade'),
(148, 29, 1, '2', 'Second Grade'),
(149, 29, 1, '3', 'Third Grade'),
(150, 29, 1, '4', 'Fourth Grade'),
(151, 29, 1, '5', 'Fifth Grade'),
(152, 29, 1, '6', 'Six Grade'),
(153, 29, 1, '7', 'Seventh Grade'),
(154, 29, 1, '8', 'Eighth Grade'),
(155, 29, 1, '9', 'Ninth Grade'),
(156, 29, 1, '10', 'Tenth Grade'),
(157, 29, 1, '11', 'Eleventh Grade'),
(158, 29, 1, '12', 'Twelfth Grade'),
(159, 30, 1, '0', 'Kindergarten'),
(160, 30, 1, '1', 'First Grade'),
(161, 30, 1, '2', 'Second Grade'),
(162, 30, 1, '3', 'Third Grade'),
(163, 30, 1, '4', 'Fourth Grade'),
(164, 30, 1, '5', 'Fifth Grade'),
(165, 30, 1, '6', 'Six Grade'),
(166, 30, 1, '7', 'Seventh Grade'),
(167, 30, 1, '8', 'Eighth Grade'),
(168, 30, 1, '9', 'Ninth Grade'),
(169, 30, 1, '10', 'Tenth Grade'),
(170, 30, 1, '11', 'Eleventh Grade'),
(171, 30, 1, '12', 'Twelfth Grade'),
(172, 31, 1, '0', 'Kindergarten'),
(173, 31, 1, '1', 'First Grade'),
(174, 31, 1, '2', 'Second Grade'),
(175, 31, 1, '3', 'Third Grade'),
(176, 31, 1, '4', 'Fourth Grade'),
(177, 31, 1, '5', 'Fifth Grade'),
(178, 31, 1, '6', 'Six Grade'),
(179, 31, 1, '7', 'Seventh Grade'),
(180, 31, 1, '8', 'Eighth Grade'),
(181, 31, 1, '9', 'Ninth Grade'),
(182, 31, 1, '10', 'Tenth Grade'),
(183, 31, 1, '11', 'Eleventh Grade'),
(184, 31, 1, '12', 'Twelfth Grade'),
(185, 32, 1, '0', 'Kindergarten'),
(186, 32, 1, '1', 'First Grade'),
(187, 32, 1, '2', 'Second Grade'),
(188, 32, 1, '3', 'Third Grade'),
(189, 32, 1, '4', 'Fourth Grade'),
(190, 32, 1, '5', 'Fifth Grade'),
(191, 32, 1, '6', 'Six Grade'),
(192, 32, 1, '7', 'Seventh Grade'),
(193, 32, 1, '8', 'Eighth Grade'),
(194, 32, 1, '9', 'Ninth Grade'),
(195, 32, 1, '10', 'Tenth Grade'),
(196, 32, 1, '11', 'Eleventh Grade'),
(197, 32, 1, '12', 'Twelfth Grade'),
(198, 33, 1, '0', 'Kindergarten'),
(199, 33, 1, '1', 'First Grade'),
(200, 33, 1, '2', 'Second Grade'),
(201, 33, 1, '3', 'Third Grade'),
(202, 33, 1, '4', 'Fourth Grade'),
(203, 33, 1, '5', 'Fifth Grade'),
(204, 33, 1, '6', 'Six Grade'),
(205, 33, 1, '7', 'Seventh Grade'),
(206, 33, 1, '8', 'Eighth Grade'),
(207, 33, 1, '9', 'Ninth Grade'),
(208, 33, 1, '10', 'Tenth Grade'),
(209, 34, 1, '0', 'Kindergarten'),
(210, 34, 1, '1', 'First Grade'),
(211, 34, 1, '2', 'Second Grade'),
(212, 34, 1, '3', 'Third Grade'),
(213, 34, 1, '4', 'Fourth Grade'),
(214, 34, 1, '5', 'Fifth Grade'),
(215, 34, 1, '6', 'Six Grade'),
(216, 34, 1, '7', 'Seventh Grade'),
(217, 34, 1, '8', 'Eighth Grade'),
(218, 34, 1, '9', 'Ninth Grade'),
(219, 34, 1, '10', 'Tenth Grade'),
(220, 35, 1, '0', 'Kindergarten'),
(221, 35, 1, '1', 'First Grade'),
(222, 35, 1, '2', 'Second Grade'),
(223, 35, 1, '3', 'Third Grade'),
(224, 35, 1, '4', 'Fourth Grade'),
(225, 35, 1, '5', 'Fifth Grade'),
(226, 35, 1, '6', 'Six Grade'),
(227, 35, 1, '7', 'Seventh Grade'),
(228, 35, 1, '8', 'Eighth Grade'),
(229, 35, 1, '9', 'Ninth Grade'),
(230, 35, 1, '10', 'Tenth Grade'),
(231, 36, 1, '0', 'Kindergarten'),
(232, 36, 1, '1', 'First Grade'),
(233, 36, 1, '2', 'Second Grade'),
(234, 36, 1, '3', 'Third Grade'),
(235, 36, 1, '4', 'Fourth Grade'),
(236, 36, 1, '5', 'Fifth Grade'),
(237, 36, 1, '6', 'Six Grade'),
(238, 36, 1, '7', 'Seventh Grade'),
(239, 36, 1, '8', 'Eighth Grade'),
(240, 36, 1, '9', 'Ninth Grade'),
(241, 36, 1, '10', 'Tenth Grade'),
(242, 36, 1, '11', 'Eleventh Grade'),
(243, 36, 1, '12', 'Twelfth Grade'),
(244, 37, 1, '0', 'Kindergarten'),
(245, 37, 1, '1', 'First Grade'),
(246, 37, 1, '2', 'Second Grade'),
(247, 37, 1, '3', 'Third Grade'),
(248, 37, 1, '4', 'Fourth Grade'),
(249, 37, 1, '5', 'Fifth Grade'),
(250, 37, 1, '6', 'Six Grade'),
(251, 37, 1, '7', 'Seventh Grade'),
(252, 37, 1, '8', 'Eighth Grade'),
(253, 37, 1, '9', 'Ninth Grade'),
(254, 37, 1, '10', 'Tenth Grade'),
(255, 37, 1, '11', 'Eleventh Grade'),
(256, 37, 1, '12', 'Twelfth Grade'),
(257, 38, 1, '0', 'Kindergarten'),
(258, 38, 1, '1', 'First Grade'),
(259, 38, 1, '2', 'Second Grade'),
(260, 38, 1, '3', 'Third Grade'),
(261, 38, 1, '4', 'Fourth Grade'),
(262, 38, 1, '5', 'Fifth Grade'),
(263, 38, 1, '6', 'Six Grade'),
(264, 38, 1, '7', 'Seventh Grade'),
(265, 38, 1, '8', 'Eighth Grade'),
(266, 38, 1, '9', 'Ninth Grade'),
(267, 38, 1, '10', 'Tenth Grade'),
(268, 38, 1, '11', 'Eleventh Grade'),
(269, 38, 1, '12', 'Twelfth Grade'),
(270, 39, 1, '0', 'Kindergarten'),
(271, 39, 1, '1', 'First Grade'),
(272, 39, 1, '2', 'Second Grade'),
(273, 39, 1, '3', 'Third Grade'),
(274, 39, 1, '4', 'Fourth Grade'),
(275, 39, 1, '5', 'Fifth Grade'),
(276, 39, 1, '6', 'Six Grade'),
(277, 39, 1, '7', 'Seventh Grade'),
(278, 39, 1, '8', 'Eighth Grade'),
(279, 39, 1, '9', 'Ninth Grade'),
(280, 39, 1, '10', 'Tenth Grade'),
(281, 39, 1, '11', 'Eleventh Grade'),
(282, 39, 1, '12', 'Twelfth Grade'),
(283, 40, 1, '0', 'Kindergarten'),
(284, 40, 1, '1', 'First Grade'),
(285, 40, 1, '2', 'Second Grade'),
(286, 40, 1, '3', 'Third Grade'),
(287, 40, 1, '4', 'Fourth Grade'),
(288, 40, 1, '5', 'Fifth Grade'),
(289, 40, 1, '6', 'Six Grade'),
(290, 40, 1, '7', 'Seventh Grade'),
(291, 40, 1, '8', 'Eighth Grade'),
(292, 40, 1, '9', 'Ninth Grade'),
(293, 40, 1, '10', 'Tenth Grade'),
(294, 41, 1, '0', 'Kindergarten'),
(295, 41, 1, '1', 'First Grade'),
(296, 41, 1, '2', 'Second Grade'),
(297, 41, 1, '3', 'Third Grade'),
(298, 41, 1, '4', 'Fourth Grade'),
(299, 41, 1, '5', 'Fifth Grade'),
(300, 41, 1, '6', 'Six Grade'),
(301, 41, 1, '7', 'Seventh Grade'),
(302, 41, 1, '8', 'Eighth Grade'),
(303, 41, 1, '9', 'Ninth Grade'),
(304, 41, 1, '10', 'Tenth Grade'),
(305, 42, 1, '0', 'Kindergarten'),
(306, 42, 1, '1', 'First Grade'),
(307, 42, 1, '2', 'Second Grade'),
(308, 42, 1, '3', 'Third Grade'),
(309, 42, 1, '4', 'Fourth Grade'),
(310, 42, 1, '5', 'Fifth Grade'),
(311, 42, 1, '6', 'Six Grade'),
(312, 42, 1, '7', 'Seventh Grade'),
(313, 42, 1, '8', 'Eighth Grade'),
(314, 42, 1, '9', 'Ninth Grade'),
(315, 42, 1, '10', 'Tenth Grade'),
(316, 43, 1, '0', 'Kindergarten'),
(317, 43, 1, '1', 'First Grade'),
(318, 43, 1, '2', 'Second Grade'),
(319, 43, 1, '3', 'Third Grade'),
(320, 43, 1, '4', 'Fourth Grade'),
(321, 43, 1, '5', 'Fifth Grade'),
(322, 43, 1, '6', 'Six Grade'),
(323, 43, 1, '7', 'Seventh Grade'),
(324, 43, 1, '8', 'Eighth Grade'),
(325, 43, 1, '9', 'Ninth Grade'),
(326, 43, 1, '10', 'Tenth Grade'),
(327, 43, 1, '11', 'Eleventh Grade'),
(328, 43, 1, '12', 'Twelfth Grade'),
(329, 44, 1, '0', 'Kindergarten'),
(330, 44, 1, '1', 'First Grade'),
(331, 44, 1, '2', 'Second Grade'),
(332, 44, 1, '3', 'Third Grade'),
(333, 44, 1, '4', 'Fourth Grade'),
(334, 44, 1, '5', 'Fifth Grade'),
(335, 44, 1, '6', 'Six Grade'),
(336, 44, 1, '7', 'Seventh Grade'),
(337, 44, 1, '8', 'Eighth Grade'),
(338, 44, 1, '9', 'Ninth Grade'),
(339, 44, 1, '10', 'Tenth Grade'),
(340, 44, 1, '11', 'Eleventh Grade'),
(341, 44, 1, '12', 'Twelfth Grade'),
(342, 45, 1, '0', 'Kindergarten'),
(343, 45, 1, '1', 'First Grade'),
(344, 45, 1, '2', 'Second Grade'),
(345, 45, 1, '3', 'Third Grade'),
(346, 45, 1, '4', 'Fourth Grade'),
(347, 45, 1, '5', 'Fifth Grade'),
(348, 45, 1, '6', 'Six Grade'),
(349, 46, 1, '7', 'Seventh Grade'),
(350, 46, 1, '8', 'Eighth Grade'),
(351, 47, 1, '9', 'Ninth Grade'),
(352, 47, 1, '10', 'Tenth Grade'),
(353, 47, 1, '11', 'Eleventh Grade'),
(354, 47, 1, '12', 'Twelfth Grade'),
(355, 48, 1, '0', 'Kindergarten'),
(356, 48, 1, '1', 'First Grade'),
(357, 48, 1, '2', 'Second Grade'),
(358, 48, 1, '3', 'Third Grade'),
(359, 48, 1, '4', 'Fourth Grade'),
(360, 48, 1, '5', 'Fifth Grade'),
(361, 48, 1, '6', 'Six Grade'),
(362, 49, 1, '6', 'Six Grade'),
(363, 49, 1, '7', 'Seventh Grade'),
(364, 49, 1, '8', 'Eighth Grade'),
(365, 50, 1, '9', 'Ninth Grade'),
(366, 50, 1, '10', 'Tenth Grade'),
(367, 50, 1, '11', 'Eleventh Grade'),
(368, 50, 1, '12', 'Twelfth Grade'),
(369, 51, 1, '6', 'Six Grade'),
(370, 51, 1, '7', 'Seventh Grade'),
(371, 51, 1, '8', 'Eighth Grade'),
(372, 52, 1, '0', 'Kindergarten'),
(373, 52, 1, '1', 'First Grade'),
(374, 52, 1, '2', 'Second Grade'),
(375, 52, 1, '3', 'Third Grade'),
(376, 52, 1, '4', 'Fourth Grade'),
(377, 52, 1, '5', 'Fifth Grade'),
(378, 52, 1, '6', 'Six Grade'),
(379, 52, 1, '7', 'Seventh Grade'),
(380, 52, 1, '8', 'Eighth Grade'),
(381, 52, 1, '9', 'Ninth Grade'),
(382, 52, 1, '10', 'Tenth Grade'),
(383, 52, 1, '11', 'Eleventh Grade'),
(384, 52, 1, '12', 'Twelfth Grade'),
(385, 53, 1, '0', 'Kindergarten'),
(386, 53, 1, '1', 'First Grade'),
(387, 53, 1, '2', 'Second Grade'),
(388, 53, 1, '3', 'Third Grade'),
(389, 53, 1, '4', 'Fourth Grade'),
(390, 53, 1, '5', 'Fifth Grade'),
(391, 53, 1, '6', 'Six Grade'),
(392, 53, 1, '7', 'Seventh Grade'),
(393, 53, 1, '8', 'Eighth Grade'),
(394, 53, 1, '9', 'Ninth Grade'),
(395, 53, 1, '10', 'Tenth Grade'),
(396, 53, 1, '11', 'Eleventh Grade'),
(397, 53, 1, '12', 'Twelfth Grade'),
(398, 54, 1, '0', 'Kindergarten'),
(399, 54, 1, '1', 'First Grade'),
(400, 54, 1, '2', 'Second Grade'),
(401, 54, 1, '3', 'Third Grade'),
(402, 54, 1, '4', 'Fourth Grade'),
(403, 54, 1, '5', 'Fifth Grade'),
(404, 54, 1, '6', 'Six Grade'),
(405, 54, 1, '7', 'Seventh Grade'),
(406, 54, 1, '8', 'Eighth Grade'),
(407, 54, 1, '9', 'Ninth Grade'),
(408, 54, 1, '10', 'Tenth Grade'),
(409, 54, 1, '11', 'Eleventh Grade'),
(410, 54, 1, '12', 'Twelfth Grade'),
(411, 55, 1, '0', 'Kindergarten'),
(412, 55, 1, '1', 'First Grade'),
(413, 55, 1, '2', 'Second Grade'),
(414, 55, 1, '3', 'Third Grade'),
(415, 55, 1, '4', 'Fourth Grade'),
(416, 55, 1, '5', 'Fifth Grade'),
(417, 55, 1, '6', 'Six Grade'),
(418, 55, 1, '7', 'Seventh Grade'),
(419, 55, 1, '8', 'Eighth Grade'),
(420, 55, 1, '9', 'Ninth Grade'),
(421, 55, 1, '10', 'Tenth Grade'),
(422, 55, 1, '11', 'Eleventh Grade'),
(423, 55, 1, '12', 'Twelfth Grade'),
(424, 56, 1, '1', '1st Grade'),
(425, 57, 1, '0', 'Kindergarten'),
(426, 57, 1, '1', '1st Grade'),
(427, 57, 1, '2', '2nd Grade'),
(428, 58, 16, '0', 'Kindergarten'),
(429, 58, 16, '1', '1st Grade'),
(430, 58, 16, '8', '8th Grade'),
(431, 58, 16, '7', '7th Grade'),
(432, 58, 16, '6', '6th Grade'),
(433, 58, 16, '5', '5th Grade'),
(434, 59, 1, '0', 'Kindergarten'),
(435, 59, 1, '1', '1st Grade'),
(436, 59, 1, '2', '2nd Grade'),
(437, 59, 1, '3', '3rd Grade'),
(438, 59, 1, '4', '4th Grade'),
(439, 59, 1, '5', '5th Grade'),
(440, 59, 1, '6', '6th Grade'),
(441, 59, 1, '7', '7th Grade'),
(442, 59, 1, '8', '8th Grade'),
(443, 59, 1, '9', '9th Grade'),
(444, 59, 1, '10', '10th Grade'),
(445, 59, 1, '11', '11th Grade'),
(446, 59, 1, '12', '12th Grade'),
(447, 60, 1, '0', 'Kindergarten'),
(448, 60, 1, '1', '1st Grade'),
(449, 60, 1, '2', '2nd Grade'),
(450, 60, 1, '3', '3rd Grade'),
(451, 60, 1, '4', '4th Grade'),
(452, 60, 1, '5', '5th Grade'),
(453, 60, 1, '6', '6th Grade'),
(454, 60, 1, '7', '7th Grade'),
(455, 60, 1, '8', '8th Grade'),
(456, 60, 1, '9', '9th Grade'),
(457, 60, 1, '10', '10th Grade'),
(458, 60, 1, '11', '11th Grade'),
(459, 60, 1, '12', '12th Grade'),
(460, 61, 1, '0', 'Kindergarten'),
(461, 61, 1, '1', '1st Grade'),
(462, 61, 1, '2', '2nd Grade'),
(463, 61, 1, '3', '3rd Grade'),
(464, 61, 1, '4', '4th Grade'),
(465, 61, 1, '5', '5th Grade'),
(466, 61, 1, '6', '6th Grade'),
(467, 61, 1, '7', '7th Grade'),
(468, 61, 1, '8', '8th Grade'),
(469, 61, 1, '9', '9th Grade'),
(470, 61, 1, '10', '10th Grade'),
(471, 61, 1, '11', '11th Grade'),
(472, 61, 1, '12', '12th Grade'),
(473, 62, 1, '0', 'Kindergarten'),
(474, 62, 1, '1', '1st Grade'),
(475, 62, 1, '2', '2nd Grade'),
(476, 62, 1, '3', '3rd Grade'),
(477, 62, 1, '4', '4th Grade'),
(478, 62, 1, '5', '5th Grade'),
(479, 62, 1, '6', '6th Grade'),
(480, 62, 1, '7', '7th Grade'),
(481, 62, 1, '8', '8th Grade'),
(482, 62, 1, '9', '9th Grade'),
(483, 62, 1, '10', '10th Grade'),
(484, 62, 1, '11', '11th Grade'),
(485, 62, 1, '12', '12th Grade'),
(486, 63, 1, '0', 'Kindergarten'),
(487, 63, 1, '1', '1st Grade'),
(488, 63, 1, '2', '2nd Grade'),
(489, 63, 1, '3', '3rd Grade'),
(490, 63, 1, '4', '4th Grade'),
(491, 63, 1, '5', '5th Grade'),
(492, 63, 1, '6', '6th Grade'),
(493, 63, 1, '7', '7th Grade'),
(494, 63, 1, '8', '8th Grade'),
(495, 63, 1, '9', '9th Grade'),
(496, 63, 1, '10', '10th Grade'),
(497, 63, 1, '11', '11th Grade'),
(498, 63, 1, '12', '12th Grade'),
(499, 63, 1, '1', '1st Grade'),
(500, 63, 1, '2', '2nd Grade'),
(501, 64, 1, '0', 'Kindergarten'),
(502, 64, 1, '1', '1st Grade'),
(503, 64, 1, '2', '2nd Grade'),
(504, 64, 1, '3', '3rd Grade'),
(505, 64, 1, '4', '4th Grade'),
(506, 64, 1, '5', '5th Grade'),
(507, 64, 1, '6', '6th Grade'),
(508, 64, 1, '7', '7th Grade'),
(509, 64, 1, '8', '8th Grade'),
(510, 64, 1, '9', '9th Grade'),
(511, 64, 1, '10', '10th Grade'),
(512, 64, 1, '11', '11th Grade'),
(513, 64, 1, '12', '12th Grade'),
(514, 65, 1, '0', 'Kindergarten'),
(515, 66, 1, '7', '7th Grade'),
(516, 66, 1, '8', '8th Grade'),
(517, 66, 1, '9', '9th Grade'),
(518, 67, 1, '0', 'Kindergarten'),
(519, 67, 1, '1', '1st Grade'),
(520, 67, 1, '2', '2nd Grade'),
(521, 67, 1, '3', '3rd Grade'),
(522, 67, 1, '4', '4th Grade'),
(523, 67, 1, '5', '5th Grade'),
(524, 68, 1, '0', 'Kindergarten'),
(525, 68, 1, '1', '1st Grade'),
(526, 68, 1, '2', '2nd Grade'),
(527, 68, 1, '3', '3rd Grade'),
(528, 68, 1, '4', '4th Grade'),
(529, 68, 1, '5', '5th Grade'),
(530, 69, 1, '0', 'Kindergarten'),
(531, 69, 1, '1', '1st Grade'),
(532, 69, 1, '2', '2nd Grade'),
(533, 69, 1, '3', '3rd Grade'),
(534, 69, 1, '4', '4th Grade'),
(535, 69, 1, '5', '5th Grade'),
(536, 69, 1, '6', '6th Grade'),
(537, 69, 1, '7', '7th Grade'),
(538, 69, 1, '8', '8th Grade'),
(539, 69, 1, '9', '9th Grade'),
(540, 69, 1, '10', '10th Grade'),
(541, 69, 1, '11', '11th Grade'),
(542, 69, 1, '12', '12th Grade'),
(543, 70, 1, '0', 'Kindergarten'),
(544, 70, 1, '1', '1st Grade'),
(545, 70, 1, '2', '2nd Grade'),
(546, 70, 1, '3', '3rd Grade'),
(547, 70, 1, '4', '4th Grade'),
(548, 70, 1, '5', '5th Grade'),
(549, 70, 1, '6', '6th Grade'),
(550, 70, 1, '7', '7th Grade'),
(551, 70, 1, '8', '8th Grade'),
(552, 70, 1, '9', '9th Grade'),
(553, 70, 1, '10', '10th Grade'),
(554, 70, 1, '11', '11th Grade'),
(555, 70, 1, '12', '12th Grade'),
(556, 71, 1, '0', 'Kindergarten'),
(557, 71, 1, '1', '1st Grade'),
(558, 71, 1, '2', '2nd Grade'),
(559, 71, 1, '3', '3rd Grade'),
(560, 71, 1, '4', '4th Grade'),
(561, 71, 1, '5', '5th Grade'),
(562, 71, 1, '6', '6th Grade'),
(563, 71, 1, '7', '7th Grade'),
(564, 71, 1, '8', '8th Grade'),
(565, 71, 1, '9', '9th Grade'),
(566, 71, 1, '10', '10th Grade'),
(567, 71, 1, '11', '11th Grade'),
(568, 71, 1, '12', '12th Grade'),
(569, 72, 1, '0', 'Kindergarten'),
(570, 72, 1, '1', '1st Grade'),
(571, 72, 1, '2', '2nd Grade'),
(572, 72, 1, '3', '3rd Grade'),
(573, 72, 1, '4', '4th Grade'),
(574, 72, 1, '5', '5th Grade'),
(575, 72, 1, '6', '6th Grade'),
(576, 72, 1, '7', '7th Grade'),
(577, 72, 1, '8', '8th Grade'),
(578, 72, 1, '9', '9th Grade'),
(579, 72, 1, '10', '10th Grade'),
(580, 72, 1, '11', '11th Grade'),
(581, 72, 1, '12', '12th Grade'),
(582, 73, 1, '0', 'Kindergarten'),
(583, 73, 1, '1', '1st Grade'),
(584, 73, 1, '2', '2nd Grade'),
(585, 73, 1, '3', '3rd Grade'),
(586, 73, 1, '4', '4th Grade'),
(587, 73, 1, '5', '5th Grade'),
(588, 73, 1, '6', '6th Grade'),
(589, 73, 1, '7', '7th Grade'),
(590, 73, 1, '8', '8th Grade'),
(591, 73, 1, '9', '9th Grade'),
(592, 73, 1, '10', '10th Grade'),
(593, 73, 1, '11', '11th Grade'),
(594, 73, 1, '12', '12th Grade'),
(595, 74, 1, '0', 'Kindergarten'),
(596, 74, 1, '1', '1st Grade'),
(597, 74, 1, '2', '2nd Grade'),
(598, 74, 1, '3', '3rd Grade'),
(599, 74, 1, '4', '4th Grade'),
(600, 74, 1, '5', '5th Grade'),
(601, 74, 1, '6', '6th Grade'),
(602, 74, 1, '7', '7th Grade'),
(603, 74, 1, '8', '8th Grade'),
(604, 74, 1, '9', '9th Grade'),
(605, 74, 1, '10', '10th Grade'),
(606, 74, 1, '11', '11th Grade'),
(607, 74, 1, '12', '12th Grade'),
(608, 75, 1, '0', 'Kindergarten'),
(609, 75, 1, '1', '1st Grade'),
(610, 75, 1, '2', '2nd Grade'),
(611, 75, 1, '3', '3rd Grade'),
(612, 75, 1, '4', '4th Grade'),
(613, 75, 1, '5', '5th Grade'),
(614, 75, 1, '6', '6th Grade'),
(615, 75, 1, '7', '7th Grade'),
(616, 75, 1, '8', '8th Grade'),
(617, 75, 1, '9', '9th Grade'),
(618, 75, 1, '10', '10th Grade'),
(619, 75, 1, '11', '11th Grade'),
(620, 75, 1, '12', '12th Grade'),
(621, 76, 1, '0', 'Kindergarten'),
(622, 76, 1, '1', '1st Grade'),
(623, 76, 1, '2', '2nd Grade'),
(624, 76, 1, '3', '3rd Grade'),
(625, 76, 1, '4', '4th Grade'),
(626, 76, 1, '5', '5th Grade'),
(627, 76, 1, '6', '6th Grade'),
(628, 76, 1, '7', '7th Grade'),
(629, 76, 1, '8', '8th Grade'),
(630, 76, 1, '9', '9th Grade'),
(631, 76, 1, '10', '10th Grade'),
(632, 76, 1, '11', '11th Grade'),
(633, 76, 1, '12', '12th Grade'),
(634, 77, 1, '0', 'Kindergarten'),
(635, 77, 1, '1', '1st Grade'),
(636, 77, 1, '2', '2nd Grade'),
(637, 77, 1, '3', '3rd Grade'),
(638, 77, 1, '4', '4th Grade'),
(639, 77, 1, '5', '5th Grade'),
(640, 77, 1, '6', '6th Grade'),
(641, 77, 1, '7', '7th Grade'),
(642, 77, 1, '8', '8th Grade'),
(643, 77, 1, '9', '9th Grade'),
(644, 77, 1, '10', '10th Grade'),
(645, 77, 1, '11', '11th Grade'),
(646, 77, 1, '12', '12th Grade'),
(647, 77, 1, '0', 'Kindergarten'),
(648, 77, 1, '1', '1st Grade'),
(649, 79, 19, '0', 'Kindergarten'),
(650, 79, 19, '1', '1st Grade'),
(651, 79, 19, '2', '2nd Grade'),
(652, 79, 19, '3', '3rd Grade'),
(653, 79, 19, '4', '4th Grade'),
(654, 79, 19, '5', '5th Grade'),
(655, 79, 19, '6', '6th Grade'),
(656, 79, 19, '7', '7th Grade'),
(657, 79, 19, '8', '8th Grade'),
(658, 79, 19, '9', '9th Grade'),
(659, 79, 19, '10', '10th Grade'),
(660, 79, 19, '11', '11th Grade'),
(661, 79, 19, '12', '12th Grade'),
(662, 80, 1, '0', 'Kindergarten'),
(663, 80, 1, '1', '1st Grade'),
(664, 80, 1, '2', '2nd Grade'),
(665, 80, 1, '3', '3rd Grade'),
(666, 80, 1, '4', '4th Grade'),
(667, 80, 1, '5', '5th Grade'),
(668, 80, 1, '6', '6th Grade'),
(669, 80, 1, '7', '7th Grade'),
(670, 80, 1, '8', '8th Grade'),
(671, 80, 1, '9', '9th Grade'),
(672, 80, 1, '10', '10th Grade'),
(673, 80, 1, '11', '11th Grade'),
(674, 80, 1, '12', '12th Grade'),
(675, 81, 1, '0', 'Kindergarten'),
(676, 81, 1, '1', '1st Grade'),
(677, 81, 1, '2', '2nd Grade'),
(678, 81, 1, '3', '3rd Grade'),
(679, 81, 1, '4', '4th Grade'),
(680, 81, 1, '5', '5th Grade'),
(681, 81, 1, '6', '6th Grade'),
(682, 81, 1, '7', '7th Grade'),
(683, 81, 1, '8', '8th Grade'),
(684, 81, 1, '9', '9th Grade'),
(685, 81, 1, '10', '10th Grade'),
(686, 81, 1, '11', '11th Grade'),
(687, 81, 1, '12', '12th Grade'),
(688, 82, 1, '0', 'Kindergarten'),
(689, 82, 1, '1', '1st Grade'),
(690, 82, 1, '2', '2nd Grade'),
(691, 82, 1, '3', '3rd Grade'),
(692, 82, 1, '4', '4th Grade'),
(693, 82, 1, '5', '5th Grade'),
(694, 82, 1, '6', '6th Grade'),
(695, 82, 1, '7', '7th Grade'),
(696, 82, 1, '8', '8th Grade'),
(697, 82, 1, '9', '9th Grade'),
(698, 82, 1, '10', '10th Grade'),
(699, 82, 1, '11', '11th Grade'),
(700, 82, 1, '12', '12th Grade'),
(701, 83, 1, '0', 'Kindergarten'),
(702, 83, 1, '1', '1st Grade'),
(703, 83, 1, '2', '2nd Grade'),
(704, 83, 1, '3', '3rd Grade'),
(705, 83, 1, '4', '4th Grade'),
(706, 83, 1, '5', '5th Grade'),
(707, 83, 1, '6', '6th Grade'),
(708, 83, 1, '7', '7th Grade'),
(709, 83, 1, '8', '8th Grade'),
(710, 83, 1, '9', '9th Grade'),
(711, 83, 1, '10', '10th Grade'),
(712, 83, 1, '11', '11th Grade'),
(713, 83, 1, '12', '12th Grade'),
(714, 84, 1, '0', 'Kindergarten'),
(715, 84, 1, '1', '1st Grade'),
(716, 84, 1, '2', '2nd Grade'),
(717, 84, 1, '3', '3rd Grade'),
(718, 84, 1, '4', '4th Grade'),
(719, 84, 1, '5', '5th Grade'),
(720, 84, 1, '6', '6th Grade'),
(721, 84, 1, '7', '7th Grade'),
(722, 84, 1, '8', '8th Grade'),
(723, 84, 1, '9', '9th Grade'),
(724, 84, 1, '10', '10th Grade'),
(725, 84, 1, '11', '11th Grade'),
(726, 84, 1, '12', '12th Grade');

-- --------------------------------------------------------

--
-- Table structure for table `school_introductory_video`
--

CREATE TABLE `school_introductory_video` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `title` varchar(60) NOT NULL,
  `description` varchar(250) NOT NULL,
  `media_type` tinyint(1) NOT NULL COMMENT '1= video 2 = Audio',
  `media_slug` varchar(150) NOT NULL,
  `format` varchar(30) NOT NULL,
  `thumb_image` varchar(255) NOT NULL,
  `large_image` varchar(255) NOT NULL,
  `media_name` varchar(255) NOT NULL,
  `duration` time NOT NULL,
  `status` tinyint(1) NOT NULL,
  `views` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `school_introductory_video`
--

INSERT INTO `school_introductory_video` (`id`, `user_id`, `school_id`, `title`, `description`, `media_type`, `media_slug`, `format`, `thumb_image`, `large_image`, `media_name`, `duration`, `status`, `views`, `is_deleted`, `created`, `modified`) VALUES
(2, 10, 3, 'UploadTitl1', 'New Video Uploded.', 1, 'uploadtitl1-NTdfMw==', '3', 'https://cam-school-staging.s3.amazonaws.com/school/1604467005_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604467005_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com//media_1604466956462_video.mp4', '00:00:03', 1, 0, 0, '2020-11-04 05:17:02', '0000-00-00 00:00:00'),
(4, 31, 8, 'testtt', 'Helllooo', 1, 'testtt-NTdfOA==', '8', 'https://cam-school-staging.s3.amazonaws.com/school/1604587618_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604587618_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/31/media_1604587609_video.mp4', '00:00:05', 1, 0, 0, '2020-11-05 14:47:15', '0000-00-00 00:00:00'),
(8, 5, 1, 'my introductory video', 'This is a rule book of all our members', 1, 'my-introductory-video-NTdfMQ==', '1', 'https://cam-school-staging.s3.amazonaws.com/school/1604751610_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604751610_800x450.jpg', 'http://192.168.2.153/cam-school/resources/video_1495283238_v..mp4', '00:00:05', 1, 0, 0, '2020-11-07 12:20:14', '0000-00-00 00:00:00'),
(9, 71, 18, 'Test Title', 'Test Descrpipation', 1, 'test-title-NTdfMTg=', '18', 'https://cam-school-staging.s3.amazonaws.com/school/1604899895_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604899895_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/71/media_1604899886_video.mp4', '00:00:07', 1, 0, 0, '2020-11-09 05:32:06', '0000-00-00 00:00:00'),
(10, 78, 21, 'my introductory video', 'This is a rule book of all our members', 1, 'my-introductory-video-NTdfMjE=', '21', 'https://cam-school-staging.s3.amazonaws.com/school/1604930835_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1604930835_800x450.jpg', 'http://192.168.2.153/cam-school/resources/video_1495283238_v..mp4', '00:00:05', 1, 0, 0, '2020-11-09 14:07:21', '0000-00-00 00:00:00'),
(11, 83, 22, 'R r', 'Do fry', 1, 'r-r-NTdfMjI=', '22', 'https://cam-school-staging.s3.amazonaws.com/school/1605004305_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1605004305_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/Display83/media_1605004267_video.mp4', '00:00:05', 1, 0, 0, '2020-11-10 10:32:11', '2020-11-11 09:59:07'),
(12, 27, 7, 'my video title', 'description', 1, 'my-video-title-NTdfNw==', 'video/mp4', 'https://cam-school-staging.s3.amazonaws.com/school/1605772173_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1605772173_800x450.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/video_1605772173_v.mp4', '00:00:05', 1, 0, 0, '2020-11-19 07:49:41', '2020-11-21 13:11:23'),
(13, 192, 79, 'Test video', 'Test description', 1, 'test-video-NTdfNzk=', '79', 'https://cam-school-staging.s3.amazonaws.com/school/1605962174_300x171.jpg', 'https://cam-school-staging.s3.amazonaws.com/school/1605962174_800x450.jpg', 'http://cam-school-staging.s3.amazonaws.com/192/media_1605962159_video.mp4', '00:00:10', 1, 0, 0, '2020-11-21 12:36:36', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(11) NOT NULL,
  `type` enum('Admin') NOT NULL DEFAULT 'Admin',
  `name` varchar(100) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `link` varchar(100) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `type`, `name`, `parent_id`, `icon`, `link`, `sort_order`, `status`) VALUES
(1, 'Admin', 'Dashboard', 0, 'fa fa-dashboard', 'admin/dashboard', 1, 'Active'),
(2, 'Admin', 'CMS Content', 0, 'fa fa-folder-open', 'javascript:void(0);', 4, 'Active'),
(3, 'Admin', 'Pages', 2, 'fa fa-circle', 'admin/pages/list', 1, 'Active'),
(4, 'Admin', 'Support Center', 0, 'fa fa-envelope-o', 'admin/contact/list', 4, 'Inactive'),
(11, 'Admin', 'User Management', 0, 'fa fa-users', 'javascript:void(0);', 2, 'Active'),
(13, 'Admin', 'User List', 11, 'fa fa-circle', 'admin/user/list', 2, 'Active'),
(14, 'Admin', 'Settings', 0, 'fa fa-cog', 'javascript:void(0);', 1000, 'Active'),
(15, 'Admin', 'Manage Roles', 14, 'fa fa-circle', 'admin/role_list', 1, 'Active'),
(16, 'Admin', 'Manage Admin', 14, 'fa fa-circle', 'admin/subadmin/list', 2, 'Active'),
(44, 'Admin', 'Advertisement', 2, 'fa fa-circle', 'admin/advertisement/list', 2, 'Inactive'),
(46, 'Admin', 'Broadcast Notification', 14, 'fa fa-circle', 'admin/broadcast_notification/list', 3, 'Inactive'),
(47, 'Admin', 'System Settings', 14, 'fa fa-circle', 'admin/settings/edit/1', 4, 'Inactive'),
(68, 'Admin', 'Support Form', 14, 'fa fa-circle', 'admin/contact_us/list', 5, 'Active'),
(69, 'Admin', 'Action Log', 0, 'fa fa-circle', 'admin/action_log/list', 20, 'Inactive'),
(70, 'Admin', 'Prospect List', 11, 'fa fa-circle', 'admin/prospect/list', 1, 'Active'),
(71, 'Admin', 'Broadcast List', 11, 'fa fa-circle', 'admin/broadcast/list', 3, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `site_information`
--

CREATE TABLE `site_information` (
  `id` int(11) NOT NULL,
  `question` varchar(255) DEFAULT NULL,
  `answer` text DEFAULT NULL,
  `orders` tinyint(4) DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1 COMMENT '1=Active, 2 = Deactive',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `state_id` int(11) NOT NULL,
  `state_name` varchar(15) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`state_id`, `state_name`) VALUES
(1, 'Alabama'),
(2, 'Alaska'),
(3, 'Arizona'),
(4, 'Arkansas'),
(5, 'California'),
(6, 'Colorado'),
(7, 'Connecticut'),
(8, 'Delaware'),
(9, 'Florida'),
(10, 'Georgia'),
(11, 'Hawaii'),
(12, 'Idaho'),
(13, 'Illinois'),
(14, 'Indiana'),
(15, 'Iowa'),
(16, 'Kansas'),
(17, 'Kentucky'),
(18, 'Louisiana'),
(19, 'Maine'),
(20, 'Maryland'),
(21, 'Massachusetts'),
(22, 'Michigan'),
(23, 'Minnesota'),
(24, 'Mississippi'),
(25, 'Missouri'),
(26, 'Montana'),
(27, 'Nebraska'),
(28, 'Nevada'),
(29, 'New Hampshire'),
(30, 'New Jersey'),
(31, 'New Mexico'),
(32, 'New York'),
(33, 'North Carolina'),
(34, 'North Dakota'),
(35, 'Ohio'),
(36, 'Oklahoma'),
(37, 'Oregon'),
(38, 'Pennsylvania'),
(39, 'Rhode Island'),
(40, 'South Carolina'),
(41, 'South Dakota'),
(42, 'Tennessee'),
(43, 'Texas'),
(44, 'Utah'),
(45, 'Vermont'),
(46, 'Virginia'),
(47, 'Washington'),
(48, 'West Virginia'),
(49, 'Wisconsin'),
(50, 'Wyoming');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `student_name` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 = Active , 2 Inactive',
  `is_deleted` tinyint(1) NOT NULL COMMENT '0 =not deleted 1= deleted',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `parent_id`, `student_name`, `status`, `is_deleted`, `created`, `modified`) VALUES
(2, 18, 'sdfsdfsdf', 1, 0, '2020-11-04 10:11:51', '0000-00-00 00:00:00'),
(4, 20, 'Student', 1, 0, '2020-11-04 11:48:17', '0000-00-00 00:00:00'),
(5, 20, 'Ss', 1, 0, '2020-11-04 12:07:05', '0000-00-00 00:00:00'),
(6, 28, 'bharat Rawal', 1, 0, '2020-11-05 11:05:03', '0000-00-00 00:00:00'),
(7, 33, 'test', 1, 0, '2020-11-05 15:05:50', '0000-00-00 00:00:00'),
(8, 33, 'bharat Rawal Rawal', 1, 0, '2020-11-06 12:42:06', '0000-00-00 00:00:00'),
(9, 33, 'bharat b', 1, 0, '2020-11-06 12:42:33', '0000-00-00 00:00:00'),
(28, 73, 'Bhart', 1, 0, '2020-11-09 06:02:08', '0000-00-00 00:00:00'),
(30, 64, 'Dummy user', 1, 0, '2020-11-09 10:11:23', '0000-00-00 00:00:00'),
(31, 64, 'accele', 1, 0, '2020-11-09 10:13:49', '0000-00-00 00:00:00'),
(32, 64, 'Dummy user', 1, 0, '2020-11-09 10:14:40', '0000-00-00 00:00:00'),
(33, 64, 'Dummy user', 1, 0, '2020-11-09 10:15:28', '0000-00-00 00:00:00'),
(34, 64, 'Dummy user', 1, 0, '2020-11-09 10:15:40', '0000-00-00 00:00:00'),
(35, 64, 'accele', 1, 0, '2020-11-09 10:15:59', '0000-00-00 00:00:00'),
(36, 64, 'accele', 1, 0, '2020-11-09 10:17:39', '0000-00-00 00:00:00'),
(37, 64, 'Dummy user', 1, 0, '2020-11-09 10:21:16', '0000-00-00 00:00:00'),
(38, 64, 'accele', 1, 0, '2020-11-09 10:21:49', '0000-00-00 00:00:00'),
(39, 64, 'accele', 1, 0, '2020-11-09 10:23:03', '0000-00-00 00:00:00'),
(40, 64, 'accele', 1, 0, '2020-11-09 10:23:22', '0000-00-00 00:00:00'),
(41, 64, 'accele', 1, 0, '2020-11-09 10:24:58', '0000-00-00 00:00:00'),
(42, 64, 'accele', 1, 0, '2020-11-09 10:25:35', '0000-00-00 00:00:00'),
(43, 64, 'accele', 1, 0, '2020-11-09 10:26:30', '0000-00-00 00:00:00'),
(44, 64, 'accele', 1, 0, '2020-11-09 10:26:48', '0000-00-00 00:00:00'),
(45, 64, 'accele', 1, 0, '2020-11-09 10:28:11', '0000-00-00 00:00:00'),
(46, 64, 'accele', 1, 0, '2020-11-09 10:35:05', '0000-00-00 00:00:00'),
(47, 64, 'demo demo', 1, 0, '2020-11-09 10:36:40', '0000-00-00 00:00:00'),
(50, 64, 'accele', 1, 0, '2020-11-09 11:27:01', '0000-00-00 00:00:00'),
(51, 64, 'accele', 1, 0, '2020-11-09 11:27:41', '0000-00-00 00:00:00'),
(59, 20, 'New student', 1, 0, '2020-11-09 11:58:38', '0000-00-00 00:00:00'),
(60, 74, 'Asd', 1, 0, '2020-11-09 12:07:00', '0000-00-00 00:00:00'),
(61, 73, 'Dsfsdfsdf dsfsdfsdf dsfsdf', 1, 0, '2020-11-09 12:34:10', '0000-00-00 00:00:00'),
(62, 75, 'Bharat rawal rawal', 1, 0, '2020-11-09 12:47:38', '0000-00-00 00:00:00'),
(63, 81, 'Hello', 1, 0, '2020-11-10 09:05:26', '0000-00-00 00:00:00'),
(64, 91, 'Hiii', 1, 0, '2020-11-10 10:19:27', '0000-00-00 00:00:00'),
(65, 20, 'krish', 1, 0, '2020-11-10 11:00:24', '0000-00-00 00:00:00'),
(66, 91, 'Bharat', 1, 0, '2020-11-11 06:34:32', '0000-00-00 00:00:00'),
(67, 81, 'Junior', 1, 0, '2020-11-11 07:54:34', '0000-00-00 00:00:00'),
(68, 152, 'peter', 1, 0, '2020-11-16 12:39:11', '2020-11-18 12:53:05'),
(71, 20, 'Keety', 1, 0, '2020-11-17 11:38:47', '0000-00-00 00:00:00'),
(72, 20, 'Keety', 1, 0, '2020-11-17 11:39:19', '0000-00-00 00:00:00'),
(73, 20, 'orio', 1, 0, '2020-11-17 12:31:22', '0000-00-00 00:00:00'),
(74, 20, 'mike', 1, 0, '2020-11-17 12:32:44', '0000-00-00 00:00:00'),
(82, 20, 'd', 1, 0, '2020-11-19 06:34:28', '0000-00-00 00:00:00'),
(84, 20, 'merry', 1, 0, '2020-11-20 12:20:41', '0000-00-00 00:00:00'),
(85, 20, 'martin', 1, 0, '2020-11-20 12:22:36', '0000-00-00 00:00:00'),
(86, 20, 'meenu', 1, 0, '2020-11-20 12:44:24', '0000-00-00 00:00:00'),
(87, 20, 'jeera', 1, 0, '2020-11-20 12:50:57', '0000-00-00 00:00:00'),
(88, 20, 'nisha', 1, 0, '2020-11-20 12:53:44', '0000-00-00 00:00:00'),
(89, 20, 'test', 1, 0, '2020-11-21 12:38:15', '0000-00-00 00:00:00'),
(90, 194, 'Bharat', 1, 0, '2020-11-21 13:00:51', '0000-00-00 00:00:00'),
(91, 194, 'Rawal', 1, 0, '2020-11-21 13:11:39', '0000-00-00 00:00:00'),
(92, 194, 'Hitaishin', 1, 0, '2020-11-21 14:06:18', '0000-00-00 00:00:00'),
(93, 20, 'test', 1, 0, '2020-11-22 09:47:07', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `student_grade`
--

CREATE TABLE `student_grade` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `grade` varchar(10) NOT NULL,
  `teacher_grade_id` int(11) NOT NULL,
  `is_approve` tinyint(1) NOT NULL COMMENT '1= Not approve 2= Accepted 3= Rejected',
  `approved_by` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL COMMENT 'status for delete request',
  `deleted_date` datetime NOT NULL,
  `deleted_by` int(11) NOT NULL COMMENT 'pass user id (parent_id)',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_grade`
--

INSERT INTO `student_grade` (`id`, `student_id`, `grade`, `teacher_grade_id`, `is_approve`, `approved_by`, `is_deleted`, `deleted_date`, `deleted_by`, `created`, `modified`) VALUES
(1, 2, '0', 33, 2, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-04 10:11:51', '0000-00-00 00:00:00'),
(2, 2, '0', 27, 2, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-04 10:11:51', '0000-00-00 00:00:00'),
(3, 2, '2', 14, 2, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-04 10:11:51', '0000-00-00 00:00:00'),
(8, 6, '1', 38, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-05 11:05:03', '0000-00-00 00:00:00'),
(9, 6, '0', 27, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-05 11:05:03', '0000-00-00 00:00:00'),
(10, 6, '2', 14, 2, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-05 11:05:03', '0000-00-00 00:00:00'),
(11, 7, '1', 41, 2, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-05 15:05:50', '0000-00-00 00:00:00'),
(12, 7, '0', 40, 2, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-05 15:05:50', '0000-00-00 00:00:00'),
(13, 8, '1', 41, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-06 12:42:06', '0000-00-00 00:00:00'),
(14, 9, '1', 41, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-06 12:42:33', '0000-00-00 00:00:00'),
(15, 9, '0', 40, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-06 12:42:33', '0000-00-00 00:00:00'),
(17, 10, '9', 10, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-07 10:21:34', '0000-00-00 00:00:00'),
(18, 10, '8', 9, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-07 10:21:34', '0000-00-00 00:00:00'),
(20, 11, '9', 10, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-07 10:21:46', '0000-00-00 00:00:00'),
(21, 11, '8', 9, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-07 10:21:46', '0000-00-00 00:00:00'),
(22, 14, '2', 63, 2, 62, 0, '0000-00-00 00:00:00', 0, '2020-11-07 10:26:25', '2020-11-08 07:20:21'),
(23, 14, '1', 64, 2, 62, 0, '0000-00-00 00:00:00', 0, '2020-11-07 10:26:25', '2020-11-08 07:20:21'),
(24, 15, '1', 63, 2, 62, 0, '0000-00-00 00:00:00', 0, '2020-11-08 06:36:08', '2020-11-08 07:20:21'),
(25, 15, '1', 64, 2, 62, 0, '0000-00-00 00:00:00', 0, '2020-11-08 06:36:08', '2020-11-08 07:20:21'),
(26, 16, '1', 46, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-08 07:18:39', '0000-00-00 00:00:00'),
(27, 16, '1', 48, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-08 07:18:39', '0000-00-00 00:00:00'),
(28, 17, '1', 48, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-08 07:19:35', '0000-00-00 00:00:00'),
(29, 17, '2', 47, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-08 07:19:35', '0000-00-00 00:00:00'),
(30, 17, '2', 49, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-08 07:19:35', '0000-00-00 00:00:00'),
(31, 17, '4', 65, 2, 62, 0, '0000-00-00 00:00:00', 0, '2020-11-08 07:19:35', '2020-11-08 07:20:21'),
(32, 18, '1', 48, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-08 07:20:34', '0000-00-00 00:00:00'),
(33, 18, '2', 47, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-08 07:20:34', '0000-00-00 00:00:00'),
(51, 30, '2', 49, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:11:23', '0000-00-00 00:00:00'),
(52, 31, '2', 45, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:13:49', '0000-00-00 00:00:00'),
(53, 32, '2', 47, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:14:40', '0000-00-00 00:00:00'),
(54, 33, '2', 45, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:15:28', '0000-00-00 00:00:00'),
(55, 34, '2', 45, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:15:40', '0000-00-00 00:00:00'),
(56, 35, '2', 45, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:15:59', '0000-00-00 00:00:00'),
(57, 36, '2', 45, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:17:39', '0000-00-00 00:00:00'),
(58, 37, '2', 49, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:21:16', '0000-00-00 00:00:00'),
(59, 38, '2', 45, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:21:49', '0000-00-00 00:00:00'),
(60, 39, '2', 45, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:23:03', '0000-00-00 00:00:00'),
(61, 40, '2', 45, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:23:22', '0000-00-00 00:00:00'),
(62, 41, '4', 65, 2, 62, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:24:58', '2020-11-10 05:14:18'),
(63, 42, '4', 65, 2, 62, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:25:35', '2020-11-10 05:14:18'),
(64, 43, '4', 65, 2, 62, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:26:30', '2020-11-10 05:14:18'),
(65, 44, '2', 45, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:26:48', '0000-00-00 00:00:00'),
(66, 45, '2', 45, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:28:11', '0000-00-00 00:00:00'),
(67, 46, '2', 49, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:35:05', '0000-00-00 00:00:00'),
(68, 46, '2', 59, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:35:05', '0000-00-00 00:00:00'),
(69, 46, '4', 65, 2, 62, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:35:05', '2020-11-10 05:14:18'),
(70, 47, '2', 62, 2, 61, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:36:40', '2020-11-12 07:43:10'),
(72, 47, '4', 65, 2, 62, 0, '0000-00-00 00:00:00', 0, '2020-11-09 10:36:40', '2020-11-10 05:14:18'),
(75, 50, '2', 45, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 11:27:01', '0000-00-00 00:00:00'),
(76, 51, '2', 45, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 11:27:41', '0000-00-00 00:00:00'),
(92, 61, '1', 67, 2, 72, 0, '0000-00-00 00:00:00', 0, '2020-11-09 12:34:10', '2020-11-10 07:51:00'),
(93, 62, '10', 68, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 12:47:38', '0000-00-00 00:00:00'),
(94, 62, '1', 67, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-09 12:47:38', '0000-00-00 00:00:00'),
(95, 63, '1', 67, 2, 72, 0, '0000-00-00 00:00:00', 0, '2020-11-10 09:05:26', '2020-11-10 09:12:32'),
(96, 64, '1', 72, 2, 88, 0, '0000-00-00 00:00:00', 0, '2020-11-10 10:19:27', '2020-11-10 10:26:46'),
(97, 64, '3', 73, 2, 88, 0, '0000-00-00 00:00:00', 0, '2020-11-10 10:19:27', '2020-11-10 10:26:46'),
(98, 64, '0', 77, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-10 10:19:27', '0000-00-00 00:00:00'),
(100, 66, '0', 77, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-11 06:34:32', '0000-00-00 00:00:00'),
(101, 67, '11', 69, 2, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-11 07:54:34', '0000-00-00 00:00:00'),
(102, 68, '4', 74, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-16 12:39:11', '0000-00-00 00:00:00'),
(118, 86, '1', 203, 2, 11, 0, '0000-00-00 00:00:00', 0, '2020-11-20 12:44:24', '2020-11-20 12:47:57'),
(119, 87, '1', 203, 2, 11, 0, '0000-00-00 00:00:00', 0, '2020-11-20 12:50:57', '2020-11-20 12:51:38'),
(120, 87, '2', 204, 2, 11, 0, '0000-00-00 00:00:00', 0, '2020-11-20 12:50:57', '2020-11-20 12:51:38'),
(122, 88, '2', 204, 2, 11, 0, '0000-00-00 00:00:00', 0, '2020-11-20 12:53:44', '2020-11-20 12:54:38'),
(123, 89, '1', 203, 2, 11, 0, '0000-00-00 00:00:00', 0, '2020-11-21 12:38:15', '2020-11-22 06:26:28'),
(125, 89, '2', 220, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-21 12:38:15', '0000-00-00 00:00:00'),
(126, 90, '1', 243, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-21 13:00:51', '0000-00-00 00:00:00'),
(127, 90, '0', 242, 2, 193, 0, '0000-00-00 00:00:00', 0, '2020-11-21 13:00:51', '2020-11-21 13:14:35'),
(128, 90, '6', 245, 2, 193, 0, '0000-00-00 00:00:00', 0, '2020-11-21 13:00:51', '2020-11-21 13:14:35'),
(129, 91, '1', 243, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-21 13:11:39', '0000-00-00 00:00:00'),
(131, 92, '2', 244, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-21 14:06:18', '0000-00-00 00:00:00'),
(132, 92, '8', 246, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-21 14:06:18', '0000-00-00 00:00:00'),
(133, 93, '1', 219, 1, 0, 0, '0000-00-00 00:00:00', 0, '2020-11-22 09:47:07', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_grade`
--

CREATE TABLE `teacher_grade` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL COMMENT 'User Id',
  `school_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `grade` tinyint(3) NOT NULL,
  `grade_display_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_grade`
--

INSERT INTO `teacher_grade` (`id`, `teacher_id`, `school_id`, `district_id`, `grade`, `grade_display_name`) VALUES
(1, 6, 1, 3, 0, 'Kindergarten'),
(2, 6, 1, 3, 1, 'First Grade'),
(3, 6, 1, 3, 2, 'Second Grade'),
(4, 6, 1, 3, 3, 'Third Grade'),
(5, 6, 1, 3, 4, 'Fourth Grade'),
(6, 6, 1, 3, 5, 'Fifth Grade'),
(7, 6, 1, 3, 6, 'Six Grade'),
(8, 6, 1, 3, 7, 'Seventh Grade'),
(9, 6, 1, 3, 8, 'Eighth Grade'),
(10, 6, 1, 3, 9, 'Ninth Grade'),
(11, 6, 1, 3, 10, 'Tenth Grade'),
(12, 8, 2, 3, 0, 'Kindergarten'),
(13, 8, 2, 3, 1, 'First Grade'),
(14, 8, 2, 3, 2, 'Second Grade'),
(15, 8, 2, 3, 4, 'Fourth Grade'),
(27, 12, 2, 3, 0, 'Kindergarten'),
(28, 12, 2, 3, 1, 'First Grade'),
(29, 12, 2, 3, 6, 'Six Grade'),
(30, 12, 2, 3, 7, 'Seventh Grade'),
(31, 12, 2, 3, 8, 'Eighth Grade'),
(33, 15, 2, 3, 0, 'Kindergarten'),
(34, 17, 4, 7, 5, 'Fifth Grade'),
(35, 17, 4, 7, 6, 'Six Grade'),
(36, 17, 4, 7, 7, 'Seventh Grade'),
(37, 17, 4, 7, 8, 'Eighth Grade'),
(38, 15, 2, 3, 1, 'First Grade'),
(39, 29, 7, 1, 2, 'Second Grade'),
(40, 32, 8, 13, 0, 'Kindergarten'),
(41, 32, 8, 13, 1, 'First Grade'),
(42, 32, 8, 13, 2, 'Second Grade'),
(43, 34, 8, 13, 2, 'Second Grade'),
(44, 34, 8, 13, 1, 'First Grade'),
(45, 35, 7, 1, 2, 'Second Grade'),
(46, 36, 7, 1, 1, 'First Grade'),
(47, 36, 7, 1, 2, 'Second Grade'),
(48, 47, 7, 1, 1, 'First Grade'),
(49, 47, 7, 1, 2, 'Second Grade'),
(50, 55, 7, 1, 2, 'Second Grade'),
(51, 54, 7, 1, 2, 'Second Grade'),
(52, 53, 7, 1, 2, 'Second Grade'),
(53, 52, 7, 1, 2, 'Second Grade'),
(54, 51, 7, 1, 2, 'Second Grade'),
(55, 50, 7, 1, 2, 'Second Grade'),
(56, 49, 7, 1, 2, 'Second Grade'),
(57, 48, 7, 1, 2, 'Second Grade'),
(58, 56, 7, 1, 2, 'Second Grade'),
(59, 58, 7, 1, 2, 'Second Grade'),
(60, 59, 7, 1, 2, 'Second Grade'),
(61, 60, 7, 1, 2, 'Second Grade'),
(62, 61, 7, 1, 2, 'Second Grade'),
(63, 62, 7, 1, 2, 'Second Grade'),
(64, 62, 7, 1, 1, 'First Grade'),
(65, 62, 7, 1, 4, 'Fourth Grade'),
(66, 72, 18, 15, 0, 'Kindergarten'),
(67, 72, 18, 15, 1, 'First Grade'),
(68, 72, 18, 15, 10, 'Tenth Grade'),
(69, 72, 18, 15, 11, 'Eleventh Grade'),
(70, 79, 21, 1, 0, 'Kindergarten'),
(71, 79, 21, 1, 1, 'First Grade'),
(72, 88, 22, 16, 1, 'First Grade'),
(73, 88, 22, 16, 3, 'Third Grade'),
(74, 88, 22, 16, 4, 'Fourth Grade'),
(75, 88, 22, 16, 10, 'Tenth Grade'),
(76, 88, 22, 16, 2, 'Second Grade'),
(77, 88, 22, 16, 0, 'Kindergarten'),
(78, 88, 22, 16, 9, 'Ninth Grade'),
(79, 90, 22, 16, 1, 'First Grade'),
(80, 90, 22, 16, 3, 'Third Grade'),
(81, 90, 22, 16, 10, 'Tenth Grade'),
(82, 90, 22, 16, 2, 'Second Grade'),
(83, 90, 22, 16, 0, 'Kindergarten'),
(84, 90, 22, 16, 9, 'Ninth Grade'),
(85, 93, 27, 1, 5, 'Fifth Grade'),
(86, 93, 27, 1, 6, 'Six Grade'),
(87, 93, 27, 1, 7, 'Seventh Grade'),
(88, 93, 27, 1, 8, 'Eighth Grade'),
(89, 94, 27, 1, 4, 'Fourth Grade'),
(90, 94, 27, 1, 5, 'Fifth Grade'),
(91, 94, 27, 1, 6, 'Six Grade'),
(92, 95, 27, 1, 5, 'Fifth Grade'),
(93, 95, 27, 1, 7, 'Seventh Grade'),
(94, 96, 27, 1, 0, 'Kindergarten'),
(95, 96, 27, 1, 1, 'First Grade'),
(96, 96, 27, 1, 2, 'Second Grade'),
(97, 96, 27, 1, 3, 'Third Grade'),
(98, 98, 28, 1, 4, 'Fourth Grade'),
(99, 98, 28, 1, 5, 'Fifth Grade'),
(100, 99, 28, 1, 1, 'First Grade'),
(101, 99, 28, 1, 2, 'Second Grade'),
(102, 99, 28, 1, 3, 'Third Grade'),
(103, 101, 29, 1, 5, 'Fifth Grade'),
(104, 101, 29, 1, 6, 'Six Grade'),
(105, 101, 29, 1, 7, 'Seventh Grade'),
(106, 101, 29, 1, 8, 'Eighth Grade'),
(107, 103, 30, 1, 4, 'Fourth Grade'),
(108, 103, 30, 1, 5, 'Fifth Grade'),
(109, 103, 30, 1, 6, 'Six Grade'),
(110, 105, 31, 1, 5, 'Fifth Grade'),
(111, 105, 31, 1, 7, 'Seventh Grade'),
(112, 107, 32, 1, 0, 'Kindergarten'),
(113, 107, 32, 1, 1, 'First Grade'),
(114, 107, 32, 1, 2, 'Second Grade'),
(115, 107, 32, 1, 3, 'Third Grade'),
(116, 111, 35, 1, 5, 'Fifth Grade'),
(117, 113, 36, 1, 5, 'Fifth Grade'),
(118, 113, 36, 1, 6, 'Six Grade'),
(119, 113, 36, 1, 7, 'Seventh Grade'),
(120, 113, 36, 1, 8, 'Eighth Grade'),
(121, 116, 38, 1, 5, 'Fifth Grade'),
(122, 116, 38, 1, 7, 'Seventh Grade'),
(123, 118, 39, 1, 0, 'Kindergarten'),
(124, 118, 39, 1, 1, 'First Grade'),
(125, 118, 39, 1, 2, 'Second Grade'),
(126, 118, 39, 1, 3, 'Third Grade'),
(127, 121, 41, 1, 1, 'First Grade'),
(128, 121, 41, 1, 2, 'Second Grade'),
(129, 121, 41, 1, 3, 'Third Grade'),
(130, 123, 42, 1, 5, 'Fifth Grade'),
(131, 124, 7, 1, 1, 'First Grade'),
(132, 124, 7, 1, 2, 'Second Grade'),
(133, 124, 7, 1, 4, 'Fourth Grade'),
(134, 126, 43, 1, 5, 'Fifth Grade'),
(135, 126, 43, 1, 6, 'Six Grade'),
(136, 126, 43, 1, 7, 'Seventh Grade'),
(137, 126, 43, 1, 8, 'Eighth Grade'),
(138, 128, 44, 1, 5, 'Fifth Grade'),
(139, 128, 44, 1, 6, 'Six Grade'),
(140, 128, 44, 1, 7, 'Seventh Grade'),
(141, 128, 44, 1, 8, 'Eighth Grade'),
(142, 131, 45, 1, 0, 'Kindergarten'),
(143, 133, 46, 1, 7, 'Seventh Grade'),
(144, 135, 47, 1, 9, 'Ninth Grade'),
(145, 135, 47, 1, 10, 'Tenth Grade'),
(146, 137, 48, 1, 1, 'First Grade'),
(147, 137, 48, 1, 2, 'Second Grade'),
(148, 139, 49, 1, 8, 'Eighth Grade'),
(149, 141, 50, 1, 11, 'Eleventh Grade'),
(150, 145, 52, 1, 5, 'Fifth Grade'),
(151, 145, 52, 1, 6, 'Six Grade'),
(152, 145, 52, 1, 7, 'Seventh Grade'),
(153, 145, 52, 1, 8, 'Eighth Grade'),
(154, 147, 53, 1, 4, 'Fourth Grade'),
(155, 147, 53, 1, 5, 'Fifth Grade'),
(156, 147, 53, 1, 6, 'Six Grade'),
(157, 149, 54, 1, 5, 'Fifth Grade'),
(158, 149, 54, 1, 7, 'Seventh Grade'),
(159, 151, 55, 1, 0, 'Kindergarten'),
(160, 151, 55, 1, 1, 'First Grade'),
(161, 151, 55, 1, 2, 'Second Grade'),
(162, 151, 55, 1, 3, 'Third Grade'),
(163, 154, 56, 1, 1, '1st Grade'),
(164, 156, 57, 1, 0, 'Kindergarten'),
(165, 156, 57, 1, 1, '1st Grade'),
(166, 156, 57, 1, 2, '2nd Grade'),
(167, 157, 57, 1, 0, 'Kindergarten'),
(168, 157, 57, 1, 1, '1st Grade'),
(169, 161, 64, 1, 2, '2nd Grade'),
(170, 161, 64, 1, 3, '3rd Grade'),
(171, 164, 66, 1, 8, '8th Grade'),
(172, 164, 66, 1, 9, '9th Grade'),
(173, 167, 67, 1, 4, '4th Grade'),
(174, 167, 67, 1, 5, '5th Grade'),
(175, 168, 67, 1, 1, '1st Grade'),
(176, 168, 67, 1, 2, '2nd Grade'),
(177, 170, 68, 1, 4, '4th Grade'),
(178, 170, 68, 1, 5, '5th Grade'),
(179, 172, 69, 1, 5, '5th Grade'),
(180, 172, 69, 1, 6, '6th Grade'),
(181, 172, 69, 1, 7, '7th Grade'),
(182, 172, 69, 1, 8, '8th Grade'),
(183, 174, 70, 1, 4, '4th Grade'),
(184, 174, 70, 1, 5, '5th Grade'),
(185, 174, 70, 1, 6, '6th Grade'),
(186, 176, 71, 1, 5, '5th Grade'),
(187, 176, 71, 1, 7, '7th Grade'),
(188, 180, 74, 1, 5, '5th Grade'),
(189, 180, 74, 1, 6, '6th Grade'),
(190, 180, 74, 1, 7, '7th Grade'),
(191, 180, 74, 1, 8, '8th Grade'),
(192, 182, 75, 1, 4, '4th Grade'),
(193, 182, 75, 1, 5, '5th Grade'),
(194, 182, 75, 1, 6, '6th Grade'),
(195, 185, 77, 1, 4, '4th Grade'),
(196, 185, 77, 1, 5, '5th Grade'),
(197, 185, 77, 1, 6, '6th Grade'),
(203, 11, 3, 5, 1, '1st Grade'),
(204, 11, 3, 5, 2, '2nd Grade'),
(205, 187, 7, 1, 2, '2nd Grade'),
(206, 188, 3, 5, 0, 'Kindergarten'),
(207, 188, 3, 5, 1, '1st Grade'),
(208, 188, 3, 5, 2, '2nd Grade'),
(209, 188, 3, 5, 3, '3rd Grade'),
(210, 188, 3, 5, 4, '4th Grade'),
(211, 188, 3, 5, 5, '5th Grade'),
(218, 190, 3, 5, 0, 'Kindergarten'),
(219, 190, 3, 5, 1, '1st Grade'),
(220, 190, 3, 5, 2, '2nd Grade'),
(221, 190, 3, 5, 3, '3rd Grade'),
(222, 190, 3, 5, 4, '4th Grade'),
(223, 190, 3, 5, 5, '5th Grade'),
(224, 190, 3, 5, 6, '6th Grade'),
(225, 190, 3, 5, 7, '7th Grade'),
(226, 12, 1, 3, 1, '1st Grade'),
(241, 11, 3, 5, 3, '3rd Grade'),
(242, 193, 79, 19, 0, 'Kindergarten'),
(243, 193, 79, 19, 1, '1st Grade'),
(244, 193, 79, 19, 2, '2nd Grade'),
(245, 193, 79, 19, 6, '6th Grade'),
(246, 193, 79, 19, 8, '8th Grade'),
(247, 193, 79, 19, 9, '9th Grade'),
(248, 11, 3, 5, 6, '6th Grade'),
(249, 11, 3, 5, 4, '4th Grade'),
(252, 196, 7, 1, 2, '2nd Grade'),
(253, 196, 7, 1, 4, '4th Grade'),
(254, 196, 7, 1, 1, '1st Grade'),
(255, 199, 82, 1, 6, '6th Grade'),
(256, 199, 82, 1, 7, '7th Grade'),
(257, 199, 82, 1, 8, '8th Grade'),
(258, 199, 82, 1, 9, '9th Grade');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_number` varchar(30) NOT NULL,
  `user_type` tinyint(1) NOT NULL COMMENT '1=District, 2=Principal, 3=Teacher, 4=Student',
  `title` varchar(10) DEFAULT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `display_name` varchar(30) NOT NULL,
  `prospect_status` tinyint(1) NOT NULL COMMENT '1= pending 2 =sold  3 = Lost 4 =Requested Info 5=Follow Up',
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(20) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `profile_image` varchar(255) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `district_id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `is_approve` tinyint(1) DEFAULT 1 COMMENT '1=NotApprove, 2=Approve, 3=Complete, 4=Reject',
  `approved_by` int(11) NOT NULL,
  `email_verified` tinyint(1) NOT NULL DEFAULT 2 COMMENT '1=Verified, 2=Unverified',
  `verification_token` varchar(10) DEFAULT NULL,
  `verification_token_date` datetime DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0=No, 1=Yes (only for parent)',
  `status` tinyint(1) DEFAULT 3 COMMENT '1=Active, 2=Inactive,\r\n3=Pending',
  `is_deleted` tinyint(1) DEFAULT 0 COMMENT '0=Not delete, 1=Deleted',
  `deleted_by` int(11) DEFAULT 0,
  `deleted_date` datetime DEFAULT NULL,
  `reset_token` varchar(10) DEFAULT NULL,
  `reset_token_date` datetime DEFAULT NULL,
  `mobile_auth_token` varchar(50) DEFAULT NULL,
  `device_id` varchar(255) DEFAULT NULL,
  `device_type` varchar(10) DEFAULT NULL,
  `unread_notification` int(11) DEFAULT 0,
  `notification_flag` tinyint(1) NOT NULL COMMENT '0= notification on 1 =for off',
  `created` datetime DEFAULT '0000-00-00 00:00:00',
  `modified` datetime DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_number`, `user_type`, `title`, `first_name`, `last_name`, `display_name`, `prospect_status`, `email`, `phone`, `password`, `profile_image`, `state_id`, `district_id`, `school_id`, `is_approve`, `approved_by`, `email_verified`, `verification_token`, `verification_token_date`, `is_public`, `status`, `is_deleted`, `deleted_by`, `deleted_date`, `reset_token`, `reset_token_date`, `mobile_auth_token`, `device_id`, `device_type`, `unread_notification`, `notification_flag`, `created`, `modified`, `modified_by`) VALUES
(1, '00000001', 1, '', 'Merry', 'Tylor', '', 2, 'merry@mailinator.com', '231-313-1231', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/b6bde0776e0e2061571ebae174426974.png', 2, 1, 0, 3, 1, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, '372799', '2020-11-21 09:57:19', 'FzWROcN3lJfv', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 6, 0, '2020-11-03 13:30:44', '2020-11-18 12:50:46', 0),
(2, '00000002', 1, NULL, 'bharat', 'bharat', '', 2, 'district@topsail.com', '', '70b4269b412a8af42b1f7b0d26eceff2', '', 1, 2, 0, 3, 1, 1, '791333', '2020-11-05 08:20:03', 0, 1, 0, 0, NULL, NULL, NULL, 'S0mCaqAaOWCu', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-03 13:45:16', '2020-11-18 12:49:42', 0),
(3, '00000003', 1, '', 'bharat', 'bharat', '', 2, 'district@yopmail.com', '9874563210', '7b8f5a7ac8a6906eadf20709992c00a0', 'resources/images/profile/6a36aa3d52ffd599d6880710410ff6ae.jpg', 1, 3, 0, 3, 1, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 4, 0, '2020-11-03 13:45:41', '2020-11-03 13:49:11', 0),
(4, '00000004', 1, NULL, 'Denmark', 'Lee', '', 2, 'denmark@mailinator.com', '1245789632', '3e0cd7dbf477f6da9831acd7c1d617bc', '', 3, 4, 0, 2, 1, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 'DgS9Wsy0pGnt', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 1, 0, '2020-11-03 13:50:10', '2020-11-03 13:50:46', 0),
(5, '00000005', 2, NULL, 'bharat', 'rawal', '', 0, 'principal@yopmail.com', '9784563214', '7b8f5a7ac8a6906eadf20709992c00a0', 'resources/images/profile/54857c831ff5f289b9e5499356d81072.jpg', 1, 3, 1, 1, 0, 2, '473704', '2020-11-03 13:51:33', 0, 1, 0, 0, NULL, NULL, NULL, 'AQw3YUBUO7Fe', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 1, 0, '2020-11-03 13:50:32', '0000-00-00 00:00:00', 0),
(6, '00000006', 1, NULL, 'Monalisa', 'Thomas', '', 2, 'monalisat@mailinator.com', '', 'e10adc3949ba59abbe56e057f20f883e', '', 2, 3, 1, 2, 0, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 'AC0WlfrDkUkL', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 2, 0, '2020-11-03 13:53:05', '0000-00-00 00:00:00', 0),
(7, '00000007', 2, NULL, 'bharat', 'rawal', '', 2, 'principal1@yopmail.com', '9784563214', '7b8f5a7ac8a6906eadf20709992c00a0', 'resources/images/profile/45c327d81af53155dcd892625715bcf0.jpg', 1, 3, 2, 3, 3, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 4, 0, '2020-11-03 13:58:12', '2020-11-03 13:59:43', 0),
(8, '00000008', 3, NULL, 'bharat', 'rawal', '', 2, 'teacher@yopmail.com', '', '7b8f5a7ac8a6906eadf20709992c00a0', 'resources/images/profile/9bf099348436469941c3b17b3d4d27c7.jpg', 1, 3, 2, 3, 7, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 2, 0, '2020-11-03 14:00:49', '2020-11-03 14:03:07', 0),
(9, '00000009', 1, 'null', 'Dis', 'Ghj', '', 2, 'district@mailinator.com', '5858508050', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/0f9fdc2eddb7350865a40a2a06a139f0.jpg', 1, 5, 0, 3, 1, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 1, 0, '2020-11-04 04:21:47', '2020-11-04 04:44:15', 0),
(10, '00000010', 2, '', 'SchoolQ', 'SLast', 'DisName', 2, 'School@mailinator.com', '848-888-9798', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/f6ccd21598bcaaa03338a6cfa7b53ba7.jpg', 1, 5, 3, 3, 9, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '', '', '', 2, 0, '2020-11-04 04:52:19', '2020-11-18 12:20:45', 1),
(11, '00000011', 3, '', 'Teacher', 'NewTech', 'Teacher', 2, 'teacher@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', '', 1, 5, 3, 3, 10, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '', '', '', 0, 0, '2020-11-04 05:43:05', '2020-11-21 12:55:48', 0),
(12, '00000012', 3, 'Mr.', 'Peterr', 'parera', 'peter parera', 2, 'teacherT@yopmail.com', '', '7b8f5a7ac8a6906eadf20709992c00a0', 'resources/images/profile/0248379e76584b89c20e69728cde30e6.jpg', 1, 3, 2, 3, 7, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '1234', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 2, 0, '2020-11-04 08:07:33', '2020-11-21 10:38:37', 0),
(13, '00000013', 1, NULL, 'Deny', 'Merchant', '', 2, 'deny@mailinator.com', '2342342344', '9927014230e4c4526d376fa3ddefbb6b', '', 2, 6, 0, 2, 1, 1, '170005', '2020-11-04 08:22:38', 0, 3, 0, 0, NULL, NULL, NULL, 'oU6SJB5caXCU', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-04 08:22:38', '2020-11-17 14:16:22', 1),
(14, '00000014', 1, NULL, 'John', 'Thomas', '', 2, 'john@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', '', 2, 7, 0, 3, 1, 1, '117088', '2020-11-04 08:24:50', 0, 1, 0, 0, NULL, NULL, NULL, 'YvP2fKqMvS8G', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 1, 0, '2020-11-04 08:24:50', '2020-11-04 08:59:58', 0),
(15, '00000015', 3, '', 'Teachert', 'T', 'userttttt', 2, 'Teacher12@yopmail.com', '', '7b8f5a7ac8a6906eadf20709992c00a0', 'resources/images/profile/2681a3302c6b8ffc67413e160e804f37.jpg', 1, 3, 2, 3, 7, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 4, 0, '2020-11-04 08:55:45', '2020-11-05 05:42:09', 0),
(16, '', 2, NULL, 'Tiya', 'Tylor', '', 2, 'tiya@mailinator.com', '1234567890', 'c92c52ea462f43ed34125582fd566154', NULL, 2, 7, 4, 2, 14, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-04 09:04:53', '0000-00-00 00:00:00', 0),
(17, '', 3, NULL, 'Homer', 'Simpson', '', 2, 'simp@mailinator.com', '', '92c2a8f67b7de6feed4a22bfbff739a0', NULL, 2, 7, 4, 2, 14, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-04 09:04:57', '0000-00-00 00:00:00', 0),
(18, '00000018', 4, NULL, 'STD', 'STD', '', 2, 'student@yopmail.com', '', '7b8f5a7ac8a6906eadf20709992c00a0', 'resources/images/profile/fede984f9fd4908b9989ca5a25c9c5c8.jpg', 1, 3, 2, 3, 0, 1, '', '0000-00-00 00:00:00', 1, 1, 0, 0, NULL, NULL, NULL, '8oUZKuJvrOJJ', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-04 09:27:59', '0000-00-00 00:00:00', 0),
(19, '00000019', 1, NULL, 'George', 'Thomas', '', 1, 'george@mailinator.com', '1245789563', '', '', 5, 8, 0, 1, 0, 1, '675259', '2020-11-04 10:48:50', 0, 3, 0, 0, NULL, NULL, NULL, 'lDYsFRK0fFLu', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-04 10:48:50', '2020-11-04 12:16:35', 0),
(20, '00000020', 4, '', 'Student', 'Dlast', '', 1, 'Student@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/581af5b537079b12aaab9ca1a3ffe665.jpg', 1, 5, 3, 3, 0, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, '288752', '2020-11-21 11:45:32', '', '', '', 0, 0, '2020-11-04 11:39:10', '2020-11-19 06:53:17', 0),
(21, '00000021', 1, NULL, 'Stuart', 'Lee', '', 5, 'stuart@mailinator.com', '1234567894', '', '', 4, 9, 0, 1, 0, 1, '481412', '2020-11-04 12:47:01', 0, 3, 0, 0, NULL, NULL, NULL, 'p0I99XZRKNMO', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-04 12:47:01', '2020-11-04 13:09:03', 0),
(22, '00000022', 1, NULL, 'Dis', 'Jdj', '', 1, 'District2@mailinator.com', '9898978888', '', '', 1, 10, 0, 1, 0, 1, '802088', '2020-11-04 13:41:06', 0, 3, 0, 0, NULL, NULL, NULL, 'iWLRYrodjQR0', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-04 13:41:06', '0000-00-00 00:00:00', 0),
(23, '00000023', 1, NULL, 'Xbbx', 'Xhhc', '', 1, 'Diss@mailinator.com', '9798989899', '', '', 1, 11, 0, 1, 0, 1, '649471', '2020-11-04 13:41:37', 0, 3, 0, 0, NULL, NULL, NULL, 'rIZQJgwTeivp', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-04 13:41:37', '0000-00-00 00:00:00', 0),
(24, '00000024', 2, NULL, 'dummy', 'school', '', 1, 'dummyschool@mailinator.com', '5646415666', '', '', 2, 12, 0, 1, 0, 1, '405993', '2020-11-05 07:21:22', 0, 3, 0, 0, NULL, NULL, NULL, 'Txmy7zx4B1Ib', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-05 07:21:22', '0000-00-00 00:00:00', 0),
(26, '00000026', 2, NULL, 'dummy', 'school', '', 1, 'mydummyschool@mailinator.com', '9584747545', '70b4269b412a8af42b1f7b0d26eceff2', '', 2, 1, 6, 3, 1, 2, '874559', '2020-11-05 07:51:34', 0, 1, 0, 0, NULL, NULL, NULL, 'of08p2sGu7A8', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-05 07:50:22', '2020-11-05 07:50:53', 0),
(27, '00000027', 2, '', 'dummy', 'school 123d', 'my dummy school 1', 1, 'dummyschool123@mailinator.com', '123-456-7888', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/3f825b6e954c914ebaf45b0c73719663.png', 2, 1, 7, 3, 1, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, '979666', '2020-11-16 13:30:46', 'WYtnatCBB4WD', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 7, 0, '2020-11-05 08:04:40', '2020-11-21 12:09:20', 0),
(28, '00000028', 4, NULL, 'bharat', 'Rawal', '', 1, 'student12@yopmail.com', '', '7b8f5a7ac8a6906eadf20709992c00a0', 'resources/images/profile/369f2ed9d39a01a8842bbc6f12b5a445.jpg', 1, 3, 2, 3, 0, 1, '', '0000-00-00 00:00:00', 1, 1, 0, 0, NULL, NULL, NULL, 'YTPUx4OGOtFt', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-05 11:03:48', '0000-00-00 00:00:00', 0),
(29, '00000029', 3, NULL, 'Dyna', 'Lee', '', 1, 'ngawade@hitaishin.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/3af02fe8ba6316b9c696a7c409c1ec4c.jpeg', 2, 1, 7, 3, 27, 2, '190919', '2020-11-21 07:26:56', 0, 1, 0, 0, NULL, NULL, NULL, 'kdieIABEP72t', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 3, 0, '2020-11-05 12:51:35', '2020-11-06 05:30:03', 0),
(30, '00000030', 1, '', 'bharat', 'district', '', 2, 'bharatdis@yopmail.com', '9875463128', '7b8f5a7ac8a6906eadf20709992c00a0', 'resources/images/profile/abe4d0262d493a714f4d1a12d3b63ced.jpg', 1, 13, 0, 3, 1, 1, '413684', '2020-11-05 14:36:29', 0, 1, 0, 0, NULL, NULL, NULL, '', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 3, 0, '2020-11-05 14:36:29', '2020-11-05 14:40:36', 0),
(31, '00000031', 2, '', 'bharat', 'principal', 'bharat display name', 1, 'bharatpri@yopmail.com', '686-486-4686', '7b8f5a7ac8a6906eadf20709992c00a0', 'resources/images/profile/f05a191c0f792bf0d4cd6dbe40d29d83.jpg', 1, 13, 8, 3, 30, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 2, 0, '2020-11-05 14:42:09', '2020-11-06 14:13:19', 0),
(32, '00000032', 3, NULL, 'bharat', 'teacher', '', 1, 'bharattea@yopmail.com', '', '7b8f5a7ac8a6906eadf20709992c00a0', 'resources/images/profile/2e854a2ff3698b17c9c0d216a590b7c2.jpg', 1, 13, 8, 3, 31, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 5, 0, '2020-11-05 14:45:36', '2020-11-05 14:46:34', 0),
(33, '00000033', 4, '', 'bharat', 'student', '', 1, 'bharatstd@yopmail.com', '', '7b8f5a7ac8a6906eadf20709992c00a0', 'resources/images/profile/ab661aaa95a50d015fe67240ab68fce2.jpg', 1, 13, 8, 3, 0, 1, '', '0000-00-00 00:00:00', 1, 1, 0, 0, NULL, '', '0000-00-00 00:00:00', '', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-05 15:04:49', '2020-11-06 13:20:40', 0),
(34, '00000034', 3, '', 'bharat', 'teacher2', 'bharat tea', 1, 'bharattea2@yopmail.com', '', '7b8f5a7ac8a6906eadf20709992c00a0', 'resources/images/profile/16be963aed7334e8e88ab13bb2a851f5.jpg', 1, 13, 8, 3, 31, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-06 04:59:29', '2020-11-06 08:50:01', 0),
(35, '00000035', 3, NULL, 'test', 'tttt', '', 1, 'test@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/1707158840a3a5ac1e56c02dadc5c5f3.jpeg', 2, 1, 7, 3, 27, 2, '998304', '2020-11-06 05:18:44', 0, 1, 0, 0, NULL, NULL, NULL, 'Swo0T0c7SCb3', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 20, 0, '2020-11-06 05:18:44', '2020-11-06 05:30:03', 0),
(36, '00000036', 3, NULL, 'eee', 'rrrr', 'EEE_RRR', 1, 'rrr@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/d3ed5502dee32683423db58a6424db54.jpeg', 2, 1, 7, 2, 27, 2, '479723', '2020-11-06 05:34:30', 0, 1, 0, 0, NULL, NULL, NULL, '4DkUiNTXZI1W', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 8, 0, '2020-11-06 05:34:30', '2020-11-06 09:07:26', 0),
(37, '00000037', 4, NULL, 'John', 'Thomas', '', 1, 'johnf@mailinator.com', '', 'e10adc3949ba59abbe56e057f20f883e', '', 2, 3, 1, 3, 0, 1, '', '0000-00-00 00:00:00', 1, 1, 0, 0, NULL, NULL, NULL, 'jHXoyjDE5FNf', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-06 07:27:20', '0000-00-00 00:00:00', 0),
(38, '00000038', 4, NULL, 'John', 'Thomas', '', 1, 'john2@mailinator.com', '', 'e10adc3949ba59abbe56e057f20f883e', '', 2, 3, 1, 3, 0, 1, '', '0000-00-00 00:00:00', 1, 1, 0, 0, NULL, NULL, NULL, 'hext61jwF5IM', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-06 07:33:30', '0000-00-00 00:00:00', 0),
(39, '00000039', 2, NULL, 'King junior', 'Luther', '', 1, 'king@mailinator.com', '744558888', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/6c70c08b7c261af7d450c8d0ca8904e1.jpeg', 2, 1, 9, 1, 0, 2, '831566', '2020-11-06 07:46:29', 0, 1, 0, 0, NULL, NULL, NULL, 'hmDUPxZsSUKG', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-06 07:46:29', '0000-00-00 00:00:00', 0),
(40, '00000040', 2, NULL, 'King junior', 'Luthor', '', 1, 'kingjunior@mailinator.com', '744558888', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/8586c9e4f7b659f16ece9d188d11ba2c.jpeg', 2, 1, 10, 3, 1, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '82V1F75LKx5r', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 1, 0, '2020-11-06 07:54:34', '2020-11-08 11:55:27', 0),
(41, '00000041', 2, NULL, 'King junior 2', 'Luthor', '', 1, 'junior2@mailinator.com', '5142369587', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/f6a6e4118f25023e862558a98551726e.jpeg', 2, 1, 11, 1, 0, 2, '412953', '2020-11-06 07:56:58', 0, 1, 0, 0, NULL, NULL, NULL, '7M19cniRslaS', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-06 07:56:58', '0000-00-00 00:00:00', 0),
(42, '00000042', 4, NULL, 'Jeviour', 'Second', '', 1, 'jeviour@mailibnator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/c35f1c23dc18d5165997794cb1dddfba.jpeg', 2, 1, 7, 3, 0, 2, '467942', '2020-11-06 08:10:28', 0, 1, 0, 0, NULL, NULL, NULL, '2AkFnZBkjTFp', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-06 08:10:28', '0000-00-00 00:00:00', 0),
(43, '00000043', 4, NULL, 'test', 'tet', '', 1, 'tet@ma.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/d4cf095fc99c2d73b4025b044ca3f39a.jpeg', 2, 1, 7, 3, 0, 2, '691217', '2020-11-06 08:18:00', 0, 1, 0, 0, NULL, NULL, NULL, 'lbmITXNnjG1Y', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-06 08:18:00', '0000-00-00 00:00:00', 0),
(44, '00000044', 4, NULL, 'Ketty', 'Tylor', '', 1, 'ketty@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/418ca4f070f389d0127533cb2163c961.jpeg', 2, 1, 7, 3, 0, 2, '267065', '2020-11-06 08:19:59', 0, 1, 0, 0, NULL, NULL, NULL, 'UxZQSJe84ALU', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-06 08:19:59', '0000-00-00 00:00:00', 0),
(45, '00000045', 4, NULL, 'Disney', 'Mark', '', 1, 'disney@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/cba15d8a964def58b7999d059fb044a9.jpeg', 2, 1, 7, 3, 0, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 'ymKCPFwelm1s', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-06 08:43:05', '0000-00-00 00:00:00', 0),
(46, '00000046', 2, NULL, 'Robert', 'D\'souza', 'Robert', 1, 'robert@mailinator.com', '7412589632', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/f711a3dbbcc6a62d95082dc2ca62a494.jpeg', 2, 1, 12, 2, 1, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 'YhOapqraeTJ4', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-06 08:58:37', '2020-11-06 09:43:55', 0),
(47, '00000047', 3, NULL, 'Teacher', 'II', 'TEST147', 1, 'teacher1@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/6e675728a76cd5e34daacd8b10d94763.jpeg', 2, 1, 7, 3, 27, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 'OfXLqUpD1ckm', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 9, 0, '2020-11-06 09:45:08', '2020-11-06 10:24:42', 0),
(48, '00000035', 3, NULL, 'test', 'tttt', 'teach_new', 1, 'test1@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/1707158840a3a5ac1e56c02dadc5c5f3.jpeg', 2, 1, 7, 3, 0, 2, '998304', '2020-11-06 05:18:44', 0, 1, 0, 0, NULL, NULL, NULL, 'Swo0T0c7SCb3', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-06 05:18:44', '2020-11-06 05:30:03', 0),
(49, '00000035', 3, NULL, 'test', 'tttt', '', 1, 'test2@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/1707158840a3a5ac1e56c02dadc5c5f3.jpeg', 2, 1, 7, 3, 27, 2, '998304', '2020-11-06 05:18:44', 0, 1, 0, 0, NULL, NULL, NULL, 'Swo0T0c7SCb3', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-06 05:18:44', '2020-11-06 11:51:42', 0),
(50, '00000035', 3, NULL, 'test', 'tttt', '', 1, 'test3@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/1707158840a3a5ac1e56c02dadc5c5f3.jpeg', 2, 1, 7, 3, 27, 2, '998304', '2020-11-06 05:18:44', 0, 1, 0, 0, NULL, NULL, NULL, 'Swo0T0c7SCb3', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-06 05:18:44', '2020-11-06 11:51:42', 0),
(51, '00000035', 3, NULL, 'test', 'tttt', '', 1, 'test4@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/1707158840a3a5ac1e56c02dadc5c5f3.jpeg', 2, 1, 7, 3, 27, 2, '998304', '2020-11-06 05:18:44', 0, 1, 0, 0, NULL, NULL, NULL, 'Swo0T0c7SCb3', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-06 05:18:44', '2020-11-06 11:51:42', 0),
(52, '00000035', 3, NULL, 'test', 'tttt', '', 1, 'test5@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/1707158840a3a5ac1e56c02dadc5c5f3.jpeg', 2, 1, 7, 3, 27, 2, '998304', '2020-11-06 05:18:44', 0, 1, 0, 0, NULL, NULL, NULL, 'Swo0T0c7SCb3', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-06 05:18:44', '2020-11-06 11:51:42', 0),
(53, '00000035', 3, NULL, 'test', 'tttt', '', 1, 'test6@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/1707158840a3a5ac1e56c02dadc5c5f3.jpeg', 2, 1, 7, 3, 27, 2, '998304', '2020-11-06 05:18:44', 0, 1, 0, 0, NULL, NULL, NULL, 'Swo0T0c7SCb3', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-06 05:18:44', '2020-11-06 11:51:42', 0),
(54, '00000035', 3, NULL, 'test', 'tttt', '', 1, 'test7@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/1707158840a3a5ac1e56c02dadc5c5f3.jpeg', 2, 1, 7, 3, 27, 1, '998304', '2020-11-06 05:18:44', 0, 1, 0, 0, NULL, NULL, NULL, 'Swo0T0c7SCb3', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-06 05:18:44', '2020-11-06 11:51:42', 0),
(55, '00000035', 3, NULL, 'test', 'tttt', '', 1, 'test8@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/1707158840a3a5ac1e56c02dadc5c5f3.jpeg', 2, 1, 7, 3, 27, 2, '998304', '2020-11-06 05:18:44', 0, 1, 0, 0, NULL, NULL, NULL, 'Swo0T0c7SCb3', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-06 05:18:44', '2020-11-06 11:51:42', 0),
(56, '00000056', 3, '', 'check', 'teacher', 'myname', 1, 'checkteacher@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/ffe2a4544442462a039e31136c965d22.jpeg', 2, 1, 7, 3, 27, 1, '860360', '2020-11-07 05:48:32', 0, 1, 0, 0, NULL, NULL, NULL, 'bd1qdmHII2RZ', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 3, 0, '2020-11-07 05:48:32', '2020-11-19 07:12:08', 0),
(57, '00000057', 2, '', 'PrincipalNew', 'LadtNee', 'SchoolNewRes', 1, 'School2@mailinator.com', '456-797-8788', '70b4269b412a8af42b1f7b0d26eceff2', '', 1, 13, 13, 3, 30, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 'idTi9IOZrb7K', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-07 06:02:04', '2020-11-07 06:06:09', 0),
(58, '00000058', 3, NULL, 'check5', 'teacher', '', 1, 'checkteacher1@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/ffe2a4544442462a039e31136c965d22.jpeg', 2, 1, 7, 2, 27, 1, '860360', '2020-11-07 05:48:32', 0, 1, 0, 0, NULL, NULL, NULL, 'bd1qdmHII2RZ', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 1, 0, '2020-11-07 05:48:32', '2020-11-07 06:25:18', 0),
(59, '00000059', 3, NULL, 'check4', 'teacher', '', 1, 'checkteacher2@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/ffe2a4544442462a039e31136c965d22.jpeg', 2, 1, 7, 2, 27, 1, '860360', '2020-11-07 05:48:32', 0, 1, 0, 0, NULL, NULL, NULL, 'bd1qdmHII2RZ', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 1, 0, '2020-11-07 05:48:32', '2020-11-07 12:03:21', 0),
(60, '00000060', 3, NULL, 'check3', 'teacher', '', 1, 'checkteacher3@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/ffe2a4544442462a039e31136c965d22.jpeg', 2, 1, 7, 2, 27, 1, '860360', '2020-11-07 05:48:32', 0, 1, 0, 0, NULL, NULL, NULL, 'bd1qdmHII2RZ', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-07 05:48:32', '2020-11-07 06:25:18', 0),
(61, '00000060', 3, '', 'check2', 'teacher', 'vghfgh', 1, 'checkteacher4@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', '', 2, 1, 7, 3, 27, 1, '860360', '2020-11-07 05:48:32', 0, 1, 0, 0, NULL, NULL, NULL, 'bd1qdmHII2RZ', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-07 05:48:32', '2020-11-12 07:43:03', 0),
(62, '00000060', 3, '', 'check1', 'teacher', 'sdghj kddddd1234', 1, 'checkteacher5@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/694d5401fecf74447026c7fa310882b8.png', 2, 1, 7, 2, 27, 1, '860360', '2020-11-07 05:48:32', 0, 1, 0, 0, NULL, NULL, NULL, 'bd1qdmHII2RZ', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-07 05:48:32', '2020-11-07 09:33:12', 0),
(63, '00000063', 4, NULL, 'new student', 'student', '', 2, 'newstudent@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/c46710e4aa309106b4a42a8751446087.png', 2, 1, 7, 2, 0, 1, '330521', '2020-11-07 10:00:06', 0, 1, 0, 0, NULL, NULL, NULL, 'wnpgeDAvFCUD', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-07 10:00:06', '0000-00-00 00:00:00', 0),
(64, '00000064', 4, '', 'newstudent1', 'student145', '', 1, 'newstudent1@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/459536936cb83d26c57bfa0ce45596e1.jpg', 2, 1, 7, 3, 0, 1, '670730', '2020-11-07 10:15:37', 0, 1, 0, 0, NULL, NULL, NULL, '0nyZmInofqqR', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 9, 0, '2020-11-07 10:15:09', '2020-11-10 09:10:24', 0),
(65, '00000065', 2, NULL, 'John', 'Thomas', 'johnny1ds', 1, 'johns@mailinator.com', '', 'e10adc3949ba59abbe56e057f20f883e', '', 2, 3, 14, 3, 0, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 's10TSxYZ0Qqi', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-08 05:54:55', '2020-11-08 06:07:52', 0),
(66, '00000066', 2, NULL, 'Mike', 'Hassi', '', 1, 'mike@mailinator.com', '7412587412', '70b4269b412a8af42b1f7b0d26eceff2', '', 2, 7, 15, 2, 0, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 'rBF7MJjC9jdq', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-08 11:48:50', '2020-11-08 14:29:34', 0),
(67, '00000067', 2, NULL, 'testtesttetest', 'test1', 'mike laes', 1, 'test11@mailinator.com', '4125789536', '70b4269b412a8af42b1f7b0d26eceff2', '', 2, 1, 16, 3, 0, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 't7IYfoKuRjfe', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 1, 0, '2020-11-08 13:28:20', '2020-11-08 14:23:09', 0),
(68, '00000068', 1, NULL, 'dist', 'last dist', '', 2, 'dist@mailinator.com', '7412589632', '70b4269b412a8af42b1f7b0d26eceff2', '', 2, 14, 0, 3, 1, 1, '256626', '2020-11-08 13:35:02', 0, 1, 0, 0, NULL, NULL, NULL, 'b42udJMuWSj2', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 1, 0, '2020-11-08 13:35:02', '2020-11-08 13:37:34', 0),
(69, '00000069', 1, '', 'Bharat', 'District', '', 2, 'bdis@yopmail.com', '514-564-5645', 'f925916e2754e5e03f75dd58a5733251', 'resources/images/profile/0b045c6572ac498e52a9fbc82a0ab9a4.jpg', 9, 15, 0, 3, 1, 1, '111361', '2020-11-09 05:23:40', 0, 1, 0, 0, NULL, NULL, NULL, '', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 3, 0, '2020-11-09 05:23:40', '2020-11-09 05:25:54', 0),
(70, '00000070', 2, NULL, 'Bharat', 'Teacher', '', 1, 'btea@yopmail.com', '897-896-5456', '7b8f5a7ac8a6906eadf20709992c00a0', 'resources/images/profile/ac35e9c80a5ecfbb5a88da31d8fde6fa.jpg', 9, 15, 17, 1, 0, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 'SYy3akJFRqo2', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-09 05:27:21', '0000-00-00 00:00:00', 0),
(71, '00000071', 2, '', 'Bharat', 'Teacher', 'Bharat Display Name', 1, 'bpri@yopmail.com', '897-896-5456', '7b8f5a7ac8a6906eadf20709992c00a0', 'resources/images/profile/9bf69c6b46e20e24fad65aafe44f3ec7.jpg', 9, 15, 18, 3, 69, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 1, 0, '2020-11-09 05:28:01', '2020-11-09 05:29:58', 0),
(72, '00000072', 3, '', 'Bharat', 'TeacherT', 'Rawal Display Name', 1, 'btea1@yopmail.com', '', '7b8f5a7ac8a6906eadf20709992c00a0', 'resources/images/profile/3cf7d957cb2e62fec030976bc601e898.jpg', 9, 15, 18, 3, 71, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 6, 0, '2020-11-09 05:34:04', '2020-11-09 05:36:04', 0),
(73, '00000073', 4, NULL, 'Bharat', 'Student', '', 1, 'bstd@yopmail.com', '', '7b8f5a7ac8a6906eadf20709992c00a0', 'resources/images/profile/395afc2eafb5ba5f7471fd8a5cf19b31.jpg', 9, 15, 18, 3, 0, 1, '', '0000-00-00 00:00:00', 1, 1, 0, 0, NULL, NULL, NULL, '', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 3, 0, '2020-11-09 06:01:20', '0000-00-00 00:00:00', 0),
(74, '00000074', 4, NULL, 'Student nnee', 'New', '', 1, 'student1@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', '', 1, 5, 3, 3, 0, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 25, 0, '2020-11-09 12:05:57', '0000-00-00 00:00:00', 0),
(75, '00000075', 4, NULL, 'Bharat', 'New', '', 1, 'bstd1@yopmail.com', '', '7b8f5a7ac8a6906eadf20709992c00a0', '', 9, 15, 18, 3, 0, 1, '', '0000-00-00 00:00:00', 1, 1, 0, 0, NULL, NULL, NULL, '', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-09 12:40:42', '0000-00-00 00:00:00', 0),
(76, '00000076', 2, '', 'Bharat', 'New', 'My Display', 1, 'bpri1@yopmail.com', '672-346-7823', '7b8f5a7ac8a6906eadf20709992c00a0', '', 9, 15, 19, 3, 69, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-09 12:50:07', '2020-11-09 12:51:55', 0),
(77, '00000077', 2, NULL, 'PRe primery', 'PRe primery', '', 1, 'pre@mailinator.com', '7412589632', '70b4269b412a8af42b1f7b0d26eceff2', '', 1, 3, 20, 1, 0, 2, '882658', '2020-11-09 13:18:35', 0, 1, 0, 0, NULL, NULL, NULL, 'u8aB7h0H7dgO', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-09 13:18:35', '0000-00-00 00:00:00', 0),
(78, '00000078', 2, 'Mr.', 'Juniour', 'test', 'junior king', 1, 'juniour@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', '', 2, 1, 21, 3, 1, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 2, 0, '2020-11-09 13:20:20', '2020-11-09 13:41:39', 0),
(79, '00000079', 3, '', 'TeacherII', 'teacher III', 'Teeeeeeeeeee', 1, 'teacher30@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', '', 2, 1, 21, 3, 78, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '699KLQ5g6o6B', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-09 13:44:33', '2020-11-09 13:54:39', 0),
(80, '00000080', 4, NULL, 'Bharat', 'Student', '', 1, 'bstd2@yopmail.com', '', '7b8f5a7ac8a6906eadf20709992c00a0', '', 9, 15, 19, 3, 0, 1, '', '0000-00-00 00:00:00', 1, 1, 0, 0, NULL, NULL, NULL, 'GpQsi4FWsfLL', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 07:45:46', '0000-00-00 00:00:00', 0),
(81, '00000081', 4, NULL, 'Bharat', 'School', '', 1, 'bstd12@yopmail.com', '', '7b8f5a7ac8a6906eadf20709992c00a0', '', 9, 15, 18, 3, 0, 1, '', '0000-00-00 00:00:00', 1, 1, 0, 0, NULL, NULL, NULL, '', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 09:04:17', '0000-00-00 00:00:00', 0),
(82, '00000082', 1, '', 'Bharat', 'Dis', '', 2, 'bdis123@yopmail.com', '632-886-2556', '7b8f5a7ac8a6906eadf20709992c00a0', '', 2, 16, 0, 3, 1, 1, '598479', '2020-11-10 09:30:02', 0, 1, 0, 0, NULL, NULL, NULL, '', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 9, 0, '2020-11-10 09:30:02', '2020-11-18 05:55:57', 1),
(83, '00000083', 2, '', 'Bharat', 'PRI', 'Display', 1, 'bpri123@yopmail.com', '123-456-7890', '7b8f5a7ac8a6906eadf20709992c00a0', '', 2, 16, 22, 3, 82, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 2, 0, '2020-11-10 09:39:13', '2020-11-10 10:04:44', 0),
(84, '00000084', 2, NULL, 'Bharat', 'Prima', '', 1, 'bpri1234@yopmail.com', '846-491-8484', '7b8f5a7ac8a6906eadf20709992c00a0', '', 2, 16, 23, 1, 0, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 'HFcPS737c3X1', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 09:43:25', '0000-00-00 00:00:00', 0),
(85, '00000085', 2, NULL, 'Bharat', 'Prima', '', 1, 'bpri12345@yopmail.com', '846-491-8484', '7b8f5a7ac8a6906eadf20709992c00a0', '', 2, 16, 24, 2, 82, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 'oqsdE8Sf92Y4', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 09:52:46', '2020-11-18 05:58:18', 0),
(86, '00000086', 2, NULL, 'Civici', 'H h h', '', 1, 'bhfy@yopmail.com', '838-383-8383', '7b8f5a7ac8a6906eadf20709992c00a0', '', 2, 16, 25, 1, 0, 2, '893363', '2020-11-10 09:55:42', 0, 1, 0, 0, NULL, NULL, NULL, 'pUd4EJAfxKXw', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 09:55:42', '0000-00-00 00:00:00', 0),
(87, '00000087', 2, NULL, 'Civici', 'H h h', '', 1, 'cjvifufuf@yopmail.com', '838-383-8383', '7b8f5a7ac8a6906eadf20709992c00a0', '', 2, 16, 26, 1, 0, 2, '704388', '2020-11-10 09:58:37', 0, 1, 0, 0, NULL, NULL, NULL, '16SAwKGe9ry8', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 09:58:37', '0000-00-00 00:00:00', 0),
(88, '00000088', 3, '', 'Bharat', 'Teach teach', 'Hiii', 1, 'btea123@yopmail.com', '', '7b8f5a7ac8a6906eadf20709992c00a0', 'resources/images/profile/c8bbad09bcd61af039ed709a772f9c68.jpg', 2, 16, 22, 3, 83, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '', '', '', 1, 0, '2020-11-10 10:06:33', '2020-11-17 05:20:09', 0),
(89, '00000089', 4, NULL, 'Vhvucu', 'Civic I', '', 1, 'guvucu@yopmvb.in', '', '7b8f5a7ac8a6906eadf20709992c00a0', '', 2, 16, 22, 3, 0, 2, '605202', '2020-11-10 10:12:59', 1, 1, 0, 0, NULL, NULL, NULL, 'EHzsn2YvAcV9', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 10:12:59', '0000-00-00 00:00:00', 0),
(90, '00000090', 3, NULL, 'Vuvuvuv', 'Vivuvu', '', 1, 'hufchcyc@yii.on', '', '7b8f5a7ac8a6906eadf20709992c00a0', '', 2, 16, 22, 1, 0, 2, '646819', '2020-11-10 10:13:58', 0, 1, 0, 0, NULL, NULL, NULL, '1RZ4z7pXVxoZ', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 10:13:58', '0000-00-00 00:00:00', 0),
(91, '00000091', 4, '', 'Bharat', 'STD', '', 1, 'bstd123@yopmail.com', '', '7b8f5a7ac8a6906eadf20709992c00a0', 'resources/images/profile/4da1447a4751cf32fc4c1253b4ccdedf.jpg', 2, 16, 22, 3, 0, 1, '', '0000-00-00 00:00:00', 1, 1, 0, 0, NULL, NULL, NULL, 'uHBMjznTtGxA', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 10:18:10', '2020-11-11 12:28:34', 0),
(92, '00000092', 2, 'Mr', 'Robin', 'Soleman', '', 0, 'robinsole@mailinator.com', '1234567890', '98bd27568c396306429a2afe26427c69', NULL, 2, 1, 27, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:02:52', '0000-00-00 00:00:00', 0),
(93, '00000093', 3, NULL, 'Sasha', 'Saleem', '', 0, 'sashasaleem@mailinator.com', '', 'cf26930db147277a162c4769d54ca2ed', NULL, 2, 1, 27, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:02:56', '0000-00-00 00:00:00', 0),
(94, '00000094', 3, NULL, 'Maria ', 'Sanchez', '', 0, 'mariasanchez@mailinator.com', '', 'e09216c71a711f37b04badbda3b330ec', NULL, 2, 1, 27, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:03:00', '0000-00-00 00:00:00', 0),
(95, '00000095', 3, NULL, 'Alex', 'Stuart', '', 0, 'alexstuart@mailinator.com', '', 'f421a057bcd117536cdae7d9d9f15431', NULL, 2, 1, 27, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:03:03', '0000-00-00 00:00:00', 0),
(96, '00000096', 3, NULL, 'Martha', 'Jolie', '', 0, 'marathajolir@mailinator.com', '', '6e2d2f78c1fcddf694f199a41b7cba6e', NULL, 2, 1, 27, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:03:06', '0000-00-00 00:00:00', 0),
(97, '00000097', 2, 'Dr.', 'Joe', 'Bing', '', 0, 'maxiprimary@mailinator.com', '2134567890', 'af5907eae7b51830778a8128013a0129', NULL, 2, 1, 28, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:03:11', '0000-00-00 00:00:00', 0),
(98, '00000098', 3, NULL, 'Monica', 'Stephen', '', 0, 'monicastephen@mailinator.com', '', 'eb95c89de789a46b1ec0e5cba3ffae8b', NULL, 2, 1, 28, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:03:15', '0000-00-00 00:00:00', 0),
(99, '00000099', 3, NULL, 'Ted', 'Toe', '', 0, 'tedtoe@mailinator.com', '', '1b9ba9dc43bf6a770ff8bcf649de8d29', NULL, 2, 1, 28, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:03:18', '0000-00-00 00:00:00', 0),
(100, '00000100', 2, 'Mr', 'John', 'One', '', 0, 'johnone@mailinator.com', '1234567890', '693c71be8cfe46a5ce9ca404f7c97c36', NULL, 2, 1, 29, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:21:24', '0000-00-00 00:00:00', 0),
(101, '00000101', 3, NULL, 'Jack', 'One', '', 0, 'jackone@mailinator.com', '', '799b19433544475fc1921ea6be2f342a', NULL, 2, 1, 29, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:21:28', '0000-00-00 00:00:00', 0),
(102, '00000102', 2, 'Mr', 'John', 'Two', '', 0, 'johntwo@mailinator.com', '1234567890', 'b48614d987099efb05cdb5a0ef786c73', NULL, 2, 1, 30, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:21:32', '0000-00-00 00:00:00', 0),
(103, '00000103', 3, NULL, 'Jack', 'Two', '', 0, 'jacktwo@mailinator.com', '', 'dc18c84a2b7ef7deec464b73c85511de', NULL, 2, 1, 30, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:21:35', '0000-00-00 00:00:00', 0),
(104, '00000104', 2, 'Mr', 'John', 'Three', '', 0, 'johnthree@mailinator.com', '1234567890', 'd2e380464e14a9ae272be823bb949110', NULL, 2, 1, 31, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:21:40', '0000-00-00 00:00:00', 0),
(105, '00000105', 3, NULL, 'Jack', 'Three', '', 0, 'jackthree@mailinator.com', '', '7d3b306d7c8b65974651abf68f96799b', NULL, 2, 1, 31, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:21:43', '0000-00-00 00:00:00', 0),
(106, '00000106', 2, 'Mr', 'John', 'Four', '', 0, 'johnfour@mailinator.com', '1234567890', '919c3f81eea292dc5c8eff52fd89fb1a', NULL, 2, 1, 32, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:21:48', '0000-00-00 00:00:00', 0),
(107, '00000107', 3, NULL, 'Jack', 'Four', '', 0, 'jackfour@mailinator.com', '', 'e97acf29c1be10f5e8a8a0ec0922e146', NULL, 2, 1, 32, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:21:51', '0000-00-00 00:00:00', 0),
(108, '00000108', 2, 'Dr.', 'Jane', 'One', '', 0, 'janeone@mailinator.com', '2134567890', '95351220a36c64e4e4766f96e3c6cf4c', NULL, 2, 1, 33, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:21:55', '0000-00-00 00:00:00', 0),
(109, '00000109', 2, 'Dr.', 'Jane', 'Two', '', 0, 'janetwo@mailinator.com', '213457890', 'c4dc21722ff47697fc101fcda75ba7a0', NULL, 2, 1, 34, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:22:00', '0000-00-00 00:00:00', 0),
(110, '00000110', 2, 'Ms.', 'Jane', 'Five', '', 0, 'Janefive@mailinator.com', '213457890', 'bc975231eac5369394abdf26ff5a6228', NULL, 2, 1, 35, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:23:31', '0000-00-00 00:00:00', 0),
(111, '00000111', 3, NULL, 'Jane', 'six', '', 0, 'janesix@mailinator.com', '', '0a9a3b065e360ee0ceab90127d9fec59', NULL, 2, 1, 35, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:23:35', '0000-00-00 00:00:00', 0),
(112, '00000112', 2, 'Mr', 'John', 'One', '', 0, 'john10@mailinator.com', '1234567890', '3251ed54a91ef9b410c5e28166dbb51b', NULL, 2, 1, 36, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:41:35', '0000-00-00 00:00:00', 0),
(113, '00000113', 3, NULL, 'Jack', 'One', '', 0, 'jack20@mailinator.com', '', '902280f45d144272b513d560d79b897d', NULL, 2, 1, 36, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:41:40', '0000-00-00 00:00:00', 0),
(114, '00000114', 2, 'Mr', 'John', 'Two', '', 0, 'john20@mailinator.com', '1234567890', '49eef1fee37c3a2d26e0e7fc008f935e', NULL, 2, 1, 37, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:41:46', '0000-00-00 00:00:00', 0),
(115, '00000115', 2, 'Mr', 'John', 'Three', '', 0, 'john30@mailinator.com', '1234567890', 'dd9eecd3f84c1e53d1ac3040493ec5a0', NULL, 2, 1, 38, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:41:51', '0000-00-00 00:00:00', 0),
(116, '00000116', 3, NULL, 'Jack', 'Three', '', 0, 'jack30@mailinator.com', '', '927654399e395a2932a6a40715e045f7', NULL, 2, 1, 38, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:41:56', '0000-00-00 00:00:00', 0),
(117, '00000117', 2, 'Mr', 'John', 'Four', '', 0, 'john40@mailiator.com', '1234567890', '56d0fdad317a53040462817b7f627f78', NULL, 2, 1, 39, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:42:01', '0000-00-00 00:00:00', 0),
(118, '00000118', 3, NULL, 'Jack', 'Four', '', 0, 'jack40@mailinator.com', '', '93b0b9d3397e893ac5b1445ffc258ec0', NULL, 2, 1, 39, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:42:05', '0000-00-00 00:00:00', 0),
(119, '00000119', 2, 'Dr.', 'Jane', 'One', '', 0, 'jane10@mailinator.com', '2134567890', 'b6eb290f34e4930fa532d0b93279e43c', NULL, 2, 1, 40, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:42:10', '0000-00-00 00:00:00', 0),
(120, '00000120', 2, 'Dr.', 'Jane', 'Two', '', 0, 'jane20@mailinator.com', '213457890', '0b410b88c46a76944670ce22dc14faaf', NULL, 2, 1, 41, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:42:16', '0000-00-00 00:00:00', 0),
(121, '00000121', 3, NULL, 'Ted', 'six', '', 0, 'janesiix@mailinator.com', '', '792e1b907e674f4bf31847b10046769f', NULL, 2, 1, 41, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:42:20', '0000-00-00 00:00:00', 0),
(122, '00000122', 2, 'Ms.', 'Jane', 'Five', '', 0, 'jane50@mailinator.com', '213457890', 'e76660dadb2a38b941508b589925e373', NULL, 2, 1, 42, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:42:24', '0000-00-00 00:00:00', 0),
(123, '00000123', 3, NULL, 'Jane', 'six', '', 0, 'janesiiix@mailinator.com', '', 'be30a3637bea752d62905d8044a68804', NULL, 2, 1, 42, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-10 12:42:27', '0000-00-00 00:00:00', 0),
(124, '00000124', 3, NULL, 'Trisha', 'shah', '', 1, 'trisha@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', '', 2, 1, 7, 2, 27, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 'FSdB2J4paXpX', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 8, 0, '2020-11-11 05:13:48', '0000-00-00 00:00:00', 0),
(125, '00000125', 2, 'Mr', 'zexa', 'jhaa', '', 0, 'zexa@mailinator.com', '999111789', '94d91f3e80a6c6f6dc0ebfc80323e6d8', NULL, 2, 1, 43, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-11 10:18:59', '0000-00-00 00:00:00', 0),
(126, '00000126', 3, NULL, 'laavi', 'khan', '', 0, 'lavvi89@mailinator.com', '', '89e5fd2e49433626e0d7d532ce15f272', NULL, 2, 1, 43, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-11 10:19:02', '0000-00-00 00:00:00', 0),
(127, '00000127', 2, 'Mr', 'zexa', 'jhaa', '', 0, 'teen@mailinator.com', '999111789', '7506b3d0e139d9cb0c94255f34b945d0', NULL, 2, 1, 44, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-11 10:45:25', '0000-00-00 00:00:00', 0),
(128, '00000128', 3, NULL, 'laavi', 'khan', '', 0, 'teacher100@mailinator.com', '', '44ee4e8217ccf9c01b6e721f137ede9b', NULL, 2, 1, 44, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-11 10:45:29', '0000-00-00 00:00:00', 0),
(129, '00000129', 4, NULL, 'student', 'last', '', 1, 'student11@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', '', 2, 16, 22, 3, 0, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 'XxaWYQfCqVCV', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-11 13:50:11', '0000-00-00 00:00:00', 0),
(130, '00000130', 2, 'Mr.', 'Robin', 'Mickey', '', 0, 'mickey@mailinator.com', '1234567890', 'ab2fa892b480e064e7c1943914e31209', NULL, 2, 1, 45, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-11 14:02:21', '0000-00-00 00:00:00', 0),
(131, '00000131', 3, NULL, 'Thor', 'Saleem', '', 0, 'thor@mailinator.com', '', '2d58230266834415bed2c20da0cec692', NULL, 2, 1, 45, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-11 14:09:44', '0000-00-00 00:00:00', 0),
(132, '00000132', 2, 'Mr.', 'Robin', 'Donald', 'donald', 0, 'donald@mailinator.com', '1234567890', '70b4269b412a8af42b1f7b0d26eceff2', NULL, 2, 1, 46, 3, 1, 1, NULL, NULL, 0, 1, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 1, 0, '2020-11-11 14:09:50', '2020-11-11 14:14:56', 0),
(133, '00000133', 3, NULL, 'Lokey', 'Sanchez', '', 0, 'lokey@mailinator.com', '', '88fd7965a1e74d19b3f1d8e4907ebfc1', NULL, 2, 1, 46, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-11 14:09:53', '0000-00-00 00:00:00', 0),
(134, '00000134', 2, 'Mr.', 'Robin', 'Duck', '', 0, 'duck@mailinator.com', '1234567890', 'b4da6e6e5d60b74dd55bd8a424bb258d', NULL, 2, 1, 47, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-11 14:09:59', '0000-00-00 00:00:00', 0),
(135, '00000135', 3, NULL, 'Adam', 'Stuart', '', 0, 'adam@mailinator.com', '', '3f04b30bf3274619bc4b3eaa45c18acf', NULL, 2, 1, 47, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-11 14:10:03', '0000-00-00 00:00:00', 0),
(136, '00000136', 2, 'Mr.', 'Robin', 'Goofy', '', 0, 'goofy@mailinator.com', '1234567890', '077e9e575840293ab0be2d3ca54175b2', NULL, 2, 1, 48, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-11 14:10:16', '0000-00-00 00:00:00', 0),
(137, '00000137', 3, NULL, 'Evan', 'Jolie', '', 0, 'evan@mailinator.com', '', 'dc70617879d115bab41203f3e2530439', NULL, 2, 1, 48, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-11 14:10:20', '0000-00-00 00:00:00', 0),
(138, '00000138', 2, 'Dr.', 'Joe', 'Daisy', '', 0, 'daisy@mailinator.com', '2134567890', 'f1e04e0a6d95c7dcfdfa36f91ece0354', NULL, 2, 1, 49, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-11 14:10:33', '0000-00-00 00:00:00', 0);
INSERT INTO `users` (`user_id`, `user_number`, `user_type`, `title`, `first_name`, `last_name`, `display_name`, `prospect_status`, `email`, `phone`, `password`, `profile_image`, `state_id`, `district_id`, `school_id`, `is_approve`, `approved_by`, `email_verified`, `verification_token`, `verification_token_date`, `is_public`, `status`, `is_deleted`, `deleted_by`, `deleted_date`, `reset_token`, `reset_token_date`, `mobile_auth_token`, `device_id`, `device_type`, `unread_notification`, `notification_flag`, `created`, `modified`, `modified_by`) VALUES
(139, '00000139', 3, NULL, 'Beaver', 'Stephen', '', 0, 'beaver@mailinator.com', '', '00d657e072fff5707923102251cd6911', NULL, 2, 1, 49, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-11 14:10:37', '0000-00-00 00:00:00', 0),
(140, '00000140', 2, 'Dr.', 'Joe', 'Pluto', '', 0, 'pluto@mailinator.com', '213457890', 'ce5405940f0776ab76dda89d1308ad46', NULL, 2, 1, 50, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-11 14:10:50', '0000-00-00 00:00:00', 0),
(141, '00000141', 3, NULL, 'Pretzel', 'Toe', '', 0, 'pretzel@mailinator.com', '', 'e44a7e5705c54d2e7b59ac5cc6f24643', NULL, 2, 1, 50, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-11 14:10:53', '0000-00-00 00:00:00', 0),
(142, '00000142', 2, 'Ms.', 'Black', 'Widow', '', 0, 'widow@mailinator.com', '1234567789', '3bbc8676f85998969fd4fda17ff9078c', NULL, 2, 1, 51, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-11 14:10:57', '0000-00-00 00:00:00', 0),
(143, '00000143', 1, NULL, 'Frida', 'peter', '', 2, 'fridas@mailinator.com', '7412587111', '98ccf5c26f057cc650e981c93e2bbca9', '', 2, 17, 0, 2, 1, 1, '976623', '2020-11-16 07:46:36', 0, 3, 0, 0, NULL, NULL, NULL, 'EqySkKkIMEQK', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 2, 0, '2020-11-16 07:46:36', '2020-11-19 11:33:00', 1),
(144, '00000144', 2, '', '', '', '', 0, 'masaba@mailinator.com', '', '9f38cf4001838b5fef48f26cb776ba26', NULL, 2, 1, 52, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-16 09:19:31', '0000-00-00 00:00:00', 0),
(145, '00000145', 3, NULL, '', '', '', 0, 'raxii@mailinator.com', '', '537c56a2d7a641aa41c5323807c1f88a', NULL, 2, 1, 52, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-16 09:19:35', '0000-00-00 00:00:00', 0),
(146, '00000146', 2, '', '', '', '', 0, 'trio@mailinator.com', '', 'f6e1d6e298e9dd74ab74ca92da7e196b', NULL, 2, 1, 53, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-16 09:19:40', '0000-00-00 00:00:00', 0),
(147, '00000147', 3, NULL, '', '', '', 0, 'indo@mailinator.com', '', '566dfc3602983dd82785a27904648140', NULL, 2, 1, 53, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-16 09:19:43', '0000-00-00 00:00:00', 0),
(148, '00000148', 2, '', '', '', '', 0, 'tereso@mailinator.com', '', '0af4100b4ea251d898c43534126994b7', NULL, 2, 1, 54, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-16 09:19:49', '0000-00-00 00:00:00', 0),
(149, '00000149', 3, NULL, '', '', '', 0, 'ujj@mailinator.com', '', 'dc51219d934d36a7f6c15761f3bf2757', NULL, 2, 1, 54, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-16 09:19:54', '0000-00-00 00:00:00', 0),
(150, '00000150', 2, '', '', '', '', 0, 'jaxi@mailinator.com', '', '1297f2ec74304971012c8325c2834120', NULL, 2, 1, 55, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-16 09:19:58', '0000-00-00 00:00:00', 0),
(151, '00000151', 3, NULL, '', '', '', 0, 'dex@mailinator.com', '', '3c93141caf5a426da5713eb87d3475af', NULL, 2, 1, 55, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-16 09:20:34', '0000-00-00 00:00:00', 0),
(152, '00000152', 4, NULL, 'parent', 'parents', '', 1, 'parent@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', '', 2, 16, 22, 3, 0, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 'UedWMhe9jFNX', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-16 12:24:54', '0000-00-00 00:00:00', 0),
(153, '00000153', 2, NULL, 'Dino', 'mie', 'test', 1, 'dino@mailinator.com', '8906786789', '70b4269b412a8af42b1f7b0d26eceff2', '', 2, 1, 56, 3, 0, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '6EquGINCtTqf', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 4, 0, '2020-11-17 10:10:50', '2020-11-17 10:41:28', 0),
(154, '00000154', 3, NULL, 'Myra', 'pilate', '', 1, 'myra@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', '', 2, 1, 56, 1, 0, 2, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 'ioqEWrpVQbQQ', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-17 10:47:21', '0000-00-00 00:00:00', 0),
(155, '00000155', 2, NULL, 'Harry', 'Ben', 'Harry ben', 1, 'harry@mailinator.com', '7412587412', '70b4269b412a8af42b1f7b0d26eceff2', '', 2, 1, 57, 3, 0, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 'SNNFZCQ9Fctr', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 3, 0, '2020-11-17 10:58:57', '2020-11-17 11:01:23', 0),
(156, '00000156', 3, NULL, 'zyan', 'Mile', '', 1, 'zyan@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', '', 2, 1, 57, 1, 0, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 'Vb9SJBq00bSc', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-17 11:02:20', '0000-00-00 00:00:00', 0),
(157, '00000157', 3, NULL, 'Michael', 'John', '', 1, 'michael@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/39c033dc5a7b8f6908bda8f12cd2eb01.png', 2, 1, 57, 1, 0, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '5m177zov3drN', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-17 11:06:20', '0000-00-00 00:00:00', 0),
(158, '00000158', 2, NULL, 'Bharat', 'Schooollll', '', 1, 'bpri33@yopmail.com', '946-484-8484', '7b8f5a7ac8a6906eadf20709992c00a0', '', 2, 16, 58, 2, 82, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 'yU10wZa2ctn1', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-18 05:57:15', '2020-11-18 05:58:18', 0),
(159, '00000159', 2, NULL, 'ocean', 'last', '', 1, 'ocean@mailinator.com', '123-456-7898', '70b4269b412a8af42b1f7b0d26eceff2', '', 2, 1, 63, 2, 1, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 'WZCkXJZ45wHs', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-18 12:50:46', '2020-11-18 13:33:25', 0),
(160, '00000160', 2, '', '', '', '', 0, 'svyas1234@mailinator.com', '', 'a48ba1797c1be17221dd6ca55a167664', NULL, 2, 1, 64, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-18 14:13:12', '0000-00-00 00:00:00', 0),
(161, '00000161', 3, NULL, '', '', '', 0, 'steach@mailinator.com', '', 'abe3d461f11ed3f48eba5335ddc149d5', NULL, 2, 1, 64, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-18 14:13:16', '0000-00-00 00:00:00', 0),
(162, '00000162', 2, 'mr.', 'sam', 'sanchez', '', 0, 'sam@mailinator.com', '', '76f1af43b0c3382c6f6f6c960d6d2d52', NULL, 2, 1, 65, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 06:27:28', '0000-00-00 00:00:00', 0),
(163, '00000163', 2, 'Dr.', 'Alex', 'popov', '', 0, 'alex@mailinator.com', '', '4c594275a59ec8483ac0349247bbf7d3', NULL, 2, 1, 66, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 06:27:32', '0000-00-00 00:00:00', 0),
(164, '00000164', 3, NULL, 'milller', '', '', 0, 'miller@mailinator.com', '', '383f487dc802e30b86f00affbee3923e', NULL, 2, 1, 66, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 06:27:36', '0000-00-00 00:00:00', 0),
(165, '00000165', 1, NULL, 'www', 'ww', '', 1, 'ww@mailinator.com', '123-456-7894', '', '', 3, 18, 0, 1, 0, 1, '450423', '2020-11-19 06:51:14', 0, 3, 0, 0, NULL, NULL, NULL, '6e3x7IKAhbQe', '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 06:51:14', '0000-00-00 00:00:00', 0),
(166, '00000166', 2, 'mr.', 'sam', 'sanchez', '', 0, 'sam1@mailinator.com', '', '23d0d638b39a36e7add939c32df3e30e', NULL, 2, 1, 67, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 11:36:51', '0000-00-00 00:00:00', 0),
(167, '00000167', 3, NULL, 'Amy', 'wong', '', 0, 'amy@mailinator.com', '', '806c2acc894f3d6bb8e997bdbdf00abb', NULL, 2, 1, 67, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 11:36:55', '0000-00-00 00:00:00', 0),
(168, '00000168', 3, NULL, 'samantha', '', '', 0, 'samantha@mailinator.com', '', '9505f42268b3bbbd0af57098397bdf69', NULL, 2, 1, 67, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 11:36:59', '0000-00-00 00:00:00', 0),
(169, '00000169', 2, 'mr.', 'sam', 'sanchez', '', 0, 'sam21@mailinator.com', '', '4830d740ef721ab88b7111e062ceb329', NULL, 2, 1, 68, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 11:38:49', '0000-00-00 00:00:00', 0),
(170, '00000170', 3, NULL, 'Amy', 'wong', '', 0, 'amy2@mailinator.com', '', '57332b12bef449ab539a837119a1210c', NULL, 2, 1, 68, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 11:38:53', '0000-00-00 00:00:00', 0),
(171, '00000171', 2, 'Mr', '', '', '', 0, 'nirmala21@mailinator.com', '', '87a90af9ac62659cb180314c03393bac', NULL, 2, 1, 69, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 11:41:54', '0000-00-00 00:00:00', 0),
(172, '00000172', 3, NULL, '', '', '', 0, 'sudhir21@milinator.com', '', 'ff316597016f673bb75af6294b043094', NULL, 2, 1, 69, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 11:41:57', '0000-00-00 00:00:00', 0),
(173, '00000173', 2, 'Mr', '', '', '', 0, 'manas332@mailinator.com', '', '22e05234499edc70eb147045da9d3a82', NULL, 2, 1, 70, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 11:42:01', '0000-00-00 00:00:00', 0),
(174, '00000174', 3, NULL, '', '', '', 0, 'sushi@mailinator.com', '', '27a8ee4c2f62242aa2c847b67976b0ce', NULL, 2, 1, 70, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 11:42:05', '0000-00-00 00:00:00', 0),
(175, '00000175', 2, 'Mr', '', '', '', 0, 'Arnya21@mailinator.com', '', 'f859884c400d956a014bf0cf2b375c5a', NULL, 2, 1, 71, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 11:42:11', '0000-00-00 00:00:00', 0),
(176, '00000176', 3, NULL, '', '', '', 0, 'alexa100@mailinator.com', '', '6c237b6f477c68733cc3157859fe1dbd', NULL, 2, 1, 71, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 11:42:15', '0000-00-00 00:00:00', 0),
(177, '00000177', 2, '', '', '', '', 0, 'masaba23@mailinator.com', '', 'b1f04ca0a780237ed4feb13680e774dc', NULL, 2, 1, 72, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 12:26:37', '0000-00-00 00:00:00', 0),
(178, '00000178', 2, '', '', '', '', 0, 'trio21@mailinator.com', '', '8e0a368ca75589072150d0b0e20f4a28', NULL, 2, 1, 73, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 12:26:41', '0000-00-00 00:00:00', 0),
(179, '00000179', 2, '', '', '', '', 0, 'masaba233@mailinator.com', '', '29b90d5aff3dd987ad007f9ad8036096', NULL, 2, 1, 74, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 12:38:36', '0000-00-00 00:00:00', 0),
(180, '00000180', 3, NULL, '', '', '', 0, '88@mailinator.com', '', '12f361e436ac33efebfcc9c22e9bd84b', NULL, 2, 1, 74, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 12:38:40', '0000-00-00 00:00:00', 0),
(181, '00000181', 2, '', '', '', '', 0, 'trio11@mailinator.com', '', '255604bccd3e86352bd8c17c0d66a29a', NULL, 2, 1, 75, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 12:38:44', '0000-00-00 00:00:00', 0),
(182, '00000182', 3, NULL, '', '', '', 0, '85@mailinator.com', '', '42b216a026c10a0b4afe0f29979e4efe', NULL, 2, 1, 75, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 12:38:47', '0000-00-00 00:00:00', 0),
(183, '00000183', 2, '', '', '', '', 0, 'masa11@mailinator.com', '', 'e9b9043c006b656e2328af8374a87b6f', NULL, 2, 1, 76, 1, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 2, 0, '2020-11-19 13:58:00', '0000-00-00 00:00:00', 0),
(184, '00000184', 2, '', '', '', '', 0, 'tri110@mailinator.com', '', '6b194b1aecc33066a21dfe7e80ca8b0f', NULL, 2, 1, 77, 1, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 2, 0, '2020-11-19 13:58:04', '0000-00-00 00:00:00', 0),
(185, '00000185', 3, NULL, '', '', '', 0, '51@mailinator.com', '', 'ae28488403eeb2c0af5cd79efe258bf6', NULL, 2, 1, 77, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, '30151a171086218bb66fb0aaa08d408463cc7dd868d1ddc24ac7b3c4ca2b38e4', 'ios', 0, 0, '2020-11-19 13:58:08', '0000-00-00 00:00:00', 0),
(186, '00000186', 2, NULL, 'merr', 'fddsf', '', 1, 'total@mailinator.com', '454-545-4545', '70b4269b412a8af42b1f7b0d26eceff2', '', 2, 1, 78, 1, 0, 2, '122157', '2020-11-20 13:55:45', 0, 1, 0, 0, NULL, NULL, NULL, '6kHDj9K79YeK', '', 'web', 0, 0, '2020-11-20 13:55:45', '0000-00-00 00:00:00', 0),
(187, '00000187', 3, NULL, 'teacher', 'teacher', '', 1, 'teacher20@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', '', 2, 1, 7, 1, 0, 2, '195847', '2020-11-21 07:18:07', 0, 1, 0, 0, NULL, NULL, NULL, '86eKDYUySKG4', '', 'web', 0, 0, '2020-11-21 07:18:07', '0000-00-00 00:00:00', 0),
(188, '00000188', 3, NULL, 'Rashi', 'Singh', '', 1, 'rashi@mailinator.con', '', 'f925916e2754e5e03f75dd58a5733251', '', 1, 5, 3, 1, 0, 2, '981850', '2020-11-21 09:27:59', 0, 1, 0, 0, NULL, NULL, NULL, 'tRM6fvvF1WSK', '', 'ios', 0, 0, '2020-11-21 09:27:59', '0000-00-00 00:00:00', 0),
(190, '00000190', 3, 'Mr.', 'TeacherNn', 'Tlast', 'Teachernn', 1, 'teacher55@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', 'resources/images/profile/0ef0ec0f9badfef7a5170082ac33a71f.jpg', 1, 5, 3, 3, 10, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 'DvRYV8gEFWTR', 'ew4kZrJE25w:APA91bHyqrP4kcEb_6oQcEBiH7sbKVYtDXhCG4-a_o8tzuvmHV_Bzl9V3ndTN7uvn_L_2APD6qFo60JJ1GZw1LyonUNPcpozEG_fXvcb5Xcmc6mJBDeGZF5LJkuSiw0gNSmqpv1pOqw7', 'android', 2, 0, '2020-11-21 10:31:06', '2020-11-21 11:03:18', 0),
(191, '00000191', 1, NULL, 'Bharat', 'District', '', 2, 'dis99@yopmail.com', '846-464-8448', 'f925916e2754e5e03f75dd58a5733251', '', 14, 19, 0, 3, 1, 1, '949422', '2020-11-21 12:13:24', 0, 1, 0, 0, NULL, NULL, NULL, '', '', '', 1, 0, '2020-11-21 12:13:24', '2020-11-21 12:14:48', 0),
(192, '00000192', 2, '', 'Bharat', 'Principal', 'Bharat display', 1, 'pri99@yopmail.com', '846-464-8494', 'f925916e2754e5e03f75dd58a5733251', '', 14, 19, 79, 3, 191, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '', '', '', 2, 0, '2020-11-21 12:17:13', '2020-11-21 12:29:35', 0),
(193, '00000193', 3, '', 'Bharat', 'Teacher', 'BharatTea', 1, 'tea99@yopmail.com', '', 'f925916e2754e5e03f75dd58a5733251', '', 14, 19, 79, 3, 192, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, '', '', '', 1, 0, '2020-11-21 12:47:56', '2020-11-21 12:50:56', 0),
(194, '00000194', 4, '', 'Bharat', 'Parent', '', 1, 'std99@yopmail.com', '', 'f925916e2754e5e03f75dd58a5733251', 'resources/images/profile/ec1bf0c0356fe674e12f040e6a939875.jpg', 14, 19, 79, 3, 0, 1, '', '0000-00-00 00:00:00', 1, 1, 0, 0, NULL, NULL, NULL, 'euitp9FKaTtO', 'dTiCaPsmt0E:APA91bEP3_CsS-puo1pIXrcp6vKNmamcVPlgssJU3iVsVabBsCwYLBuh9KbAlsqR4GxDWfOXEwaGM7Qu7Po4dSHMh6-IohpbFZ-KKufupJAXqFmfqkt92Vc1T2GXZT1c0Qpo1PhdKWCB', 'android', 3, 0, '2020-11-21 12:59:55', '2020-11-21 14:25:54', 0),
(196, '00000196', 3, '', 'jerra', 'sevy', 'Ms. jeera', 1, 'jeera@mailinator.com', '', '70b4269b412a8af42b1f7b0d26eceff2', '', 2, 1, 7, 3, 27, 1, '', '0000-00-00 00:00:00', 0, 1, 0, 0, NULL, NULL, NULL, 'kZjzGHkdxoUQ', '', 'web', 0, 0, '2020-11-22 07:06:05', '2020-11-22 07:11:49', 0),
(197, '00000197', 2, 'Mr', 'Devid', 'Soleman', '', 0, 'devid@mailinator.com', '901-234-6674', '690a68fa31aa179f350779343abca7d9', NULL, 2, 1, 80, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2020-11-22 08:53:51', '0000-00-00 00:00:00', 0),
(198, '00000198', 2, 'Mr', '', '', '', 0, 'chandel@mailinator.com', '123-212-3456', '2e7aba00b886c8e999ebb0000c6b5c03', NULL, 2, 1, 82, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2020-11-22 08:58:54', '0000-00-00 00:00:00', 0),
(199, '00000199', 3, NULL, '', '', '', 0, 'tomar@mailinator.com', '', '473dd69b68d5914f0cb9f3383609fca9', NULL, 2, 1, 82, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2020-11-22 09:03:54', '0000-00-00 00:00:00', 0),
(200, '00000200', 2, 'Mr', 'Jessy', 'jj', '', 0, 'jessy@mailinator.com', '798-775-0697', '80507395fd4e646214bab6d22b380c21', NULL, 2, 1, 84, 2, 1, 1, NULL, NULL, 0, 3, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2020-11-22 09:08:55', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_06-10-2020`
--

CREATE TABLE `users_06-10-2020` (
  `user_id` int(11) NOT NULL,
  `user_name` text DEFAULT NULL,
  `first_name` text DEFAULT NULL,
  `last_name` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `password` text DEFAULT NULL,
  `profile_image` text DEFAULT NULL,
  `gender` text DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `notification_flag` enum('yes','no') DEFAULT 'yes',
  `login_type` text DEFAULT NULL,
  `fb_id` text DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `created_date` datetime DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) DEFAULT 0,
  `dob` date DEFAULT NULL,
  `verify_code` int(11) DEFAULT 0,
  `status` tinyint(1) DEFAULT 2 COMMENT '1=Active, 2=Deactive',
  `admin_approve` tinyint(4) DEFAULT 1 COMMENT '1=Unblock, 2 = Block',
  `modified_datetime` datetime DEFAULT '0000-00-00 00:00:00',
  `veficationcode_send_time` datetime DEFAULT '0000-00-00 00:00:00',
  `forgot_password_code` int(11) DEFAULT 0,
  `forgot_password_datetime` datetime DEFAULT '0000-00-00 00:00:00',
  `facebook_login_count` tinyint(4) DEFAULT 0,
  `device_id` text DEFAULT NULL,
  `mobile_auth_token` varchar(150) DEFAULT NULL,
  `device_type` varchar(50) DEFAULT NULL,
  `unread_notification` int(11) DEFAULT 0,
  `is_deleted` tinyint(1) DEFAULT 2 COMMENT '1=Yes, 2 = No',
  `deleted_by` varchar(50) DEFAULT '0',
  `deleted_date` datetime DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_06-10-2020`
--

INSERT INTO `users_06-10-2020` (`user_id`, `user_name`, `first_name`, `last_name`, `email`, `password`, `profile_image`, `gender`, `state_id`, `country_id`, `notification_flag`, `login_type`, `fb_id`, `created_by`, `created_date`, `modified_by`, `dob`, `verify_code`, `status`, `admin_approve`, `modified_datetime`, `veficationcode_send_time`, `forgot_password_code`, `forgot_password_datetime`, `facebook_login_count`, `device_id`, `mobile_auth_token`, `device_type`, `unread_notification`, `is_deleted`, `deleted_by`, `deleted_date`) VALUES
(340, 'Rsoesman3', 'Richard', 'Soesman', 'richard.soesman@yahoo.com', 'VmljdG9yaWEwNA==', 'uploads/userthumbimage/1523471848-myImage.png', 'Male', 5, 0, 'yes', 'facebook', '10211060325523636', 0, '2018-04-11 11:37:28', 0, '1964-12-12', 0, 1, 1, '2019-02-11 12:08:24', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15499157041588145174', NULL, 10, 2, '0', '0000-00-00 00:00:00'),
(343, 'Admin_', 'Admin', 'User', 'thetruthdebate@gmail.com', 'QmVhdmVyczE3Jg==', 'uploads/userthumbimage/1065153540_1524470397.png', '', 5, 0, 'yes', 'normal', NULL, 0, '2018-04-11 11:58:56', 343, '1997-02-10', 0, 1, 1, '2019-03-07 14:18:18', '0000-00-00 00:00:00', 871726, '2018-06-14 10:48:58', 0, NULL, '1551997098669278742', NULL, 10, 2, '0', '0000-00-00 00:00:00'),
(361, 'Tjw0025', 'TJ', 'Wheat', 'thomasjwheat@gmail.com', '', 'uploads/userthumbimage/1524006563-myImage.png', 'Male', 43, 0, 'yes', 'facebook', '10107185831173760', 0, '2018-04-17 16:09:23', 0, '1983-08-05', 0, 1, 1, '2018-04-19 07:34:49', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15241484891996239241', NULL, 14, 2, '0', '0000-00-00 00:00:00'),
(365, 'Lynette', 'Lynette', 'Sosa', 'lynettesosa@yahoo.com', 'QXZyaWwyMDA3', 'uploads/userthumbimage/1524349308-profile_img.jpeg', 'Female', 5, 0, 'yes', 'normal', '', 0, '2018-04-21 15:21:48', 365, '1964-12-12', 0, 1, 1, '2018-04-21 15:23:12', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1524349392308036263', NULL, 48, 2, '0', '0000-00-00 00:00:00'),
(379, 'Saloul', 'Abdullah', 'saloul', 'saloul@hotmail.com', 'YXNkZmdoajE=', 'uploads/userthumbimage/1525977254-profile_img.jpeg', 'Male', 0, 51, 'no', 'normal', '', 0, '2018-05-10 11:34:15', 379, '1976-06-10', 0, 1, 1, '2018-05-10 11:35:32', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '152597733260535351', NULL, 5, 2, '0', '0000-00-00 00:00:00'),
(383, 'Johnmelodyme', 'John Melody', 'M Eskhol', 'johnmelodyme@icloud.com', '', 'uploads/userthumbimage/1526379106-myImage.png', 'Male', 0, 58, 'yes', 'facebook', '1972072262834846', 0, '2018-05-15 03:11:46', 0, '1996-05-10', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15263791061693035548', NULL, 52, 2, '0', '0000-00-00 00:00:00'),
(384, 'Jadozz', 'Iss', 'Jadozz', 'ismailajadama3@gmail.com', '', 'uploads/userthumbimage/1260136626_1526554235.jpeg', 'Male', 0, 7, 'yes', 'facebook', '259643837913440', 0, '2018-05-17 03:45:46', 384, '1996-10-03', 0, 1, 1, '2018-05-17 03:50:35', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1526553946662593666', NULL, 41, 2, '0', '0000-00-00 00:00:00'),
(386, 'Eleutherian', 'The', 'Eleutherian', 'ConservativeDialectic@gmail.com', 'TmVvbml4ODgqKg==', 'uploads/userthumbimage/1527008771-myImage.png', 'Male', 10, 0, 'yes', 'normal', '', 0, '2018-05-22 10:06:11', 386, '0000-00-00', 0, 1, 1, '2018-10-12 09:39:17', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15393623571525116878', NULL, 23, 2, '0', '0000-00-00 00:00:00'),
(387, 'Narendra', 'Me', 'Myself', 'nsr.curious@gmail.com', 'cXdlcnR5QDEyMw==', 'uploads/userthumbimage/1527058502-profile_img.jpg', 'Male', 5, 0, 'yes', 'normal', '', 0, '2018-05-22 23:55:02', 387, '1988-09-15', 0, 2, 1, '2018-06-22 00:27:46', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15296524662028275552', NULL, 3, 2, '0', '0000-00-00 00:00:00'),
(389, 'Lynettehill', 'lynette', 'sosa', 'lynetteysosa@gmail.com', 'UGFzc3dvcmQ=', 'uploads/userthumbimage/1527353129-profile_img.jpeg', 'Female', 5, 0, 'yes', 'normal', '', 0, '2018-05-26 09:45:29', 389, '1964-12-12', 0, 1, 1, '2018-06-20 10:06:52', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15295144121545821572', NULL, 175, 2, '0', '0000-00-00 00:00:00'),
(391, 'Prakash13', 'Prakash', 'Sharma', 'sharmaprakash.13@gmail.com', '', 'uploads/userthumbimage/1527667039-myImage.png', '', 0, 10, 'yes', 'facebook', '10155785671047695', 0, '2018-05-30 00:57:19', 0, '0000-00-00', 0, 1, 1, '2018-10-16 20:45:19', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1539747919258096002', NULL, 10, 2, '0', '0000-00-00 00:00:00'),
(392, 'Tokkyoisyo', 'tokkyo', 'isyo', 'tokkyo.isyo.2018.103@gmail.com', 'Z2F6bzIwMTg=', 'uploads/userthumbimage/1528181575-profile_img.jpeg', 'Female', 0, 47, 'yes', 'normal', '', 0, '2018-06-04 23:52:55', 392, '1979-12-31', 0, 1, 1, '2018-06-04 23:54:19', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1528181659395276210', NULL, 19, 2, '0', '0000-00-00 00:00:00'),
(393, 'Arshadsu', 'Arshad', 'SU', 'arshadsu005@gmail.com', '', 'uploads/userthumbimage/1528295933-profile_img.jpg', 'Male', 0, 41, 'no', 'facebook', '259131227994540', 0, '2018-06-06 07:38:53', 0, '0000-00-00', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15282959331218251655', NULL, 7, 2, '0', '0000-00-00 00:00:00'),
(394, 'Yahoo123', 'Sam', 'Userfour', 'ohireuser4@gmail.com', '', 'uploads/userthumbimage/1528367992-myImage.png', 'Female', 2, 0, 'yes', 'facebook', '231491927583070', 0, '2018-06-07 03:39:52', 0, '2011-08-09', 0, 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15283679921921284255', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(395, 'Aftabqureshi', 'Aftab', 'Qureshi', 'aftabqureshi.hack@gmail.com', '', 'uploads/userthumbimage/1528385271-profile_img.jpg', 'Male', 0, 41, 'yes', 'facebook', '599377653764927', 0, '2018-06-07 08:27:51', 0, '1999-11-15', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1528385271850838182', NULL, 42, 2, '0', '0000-00-00 00:00:00'),
(396, '_cassiel_', 'Cassiel', ':)', 'castorsart@gmail.com', 'UmVhcmVhMTI=', 'uploads/userthumbimage/1528439247-myImage.png', 'Questioning', 5, 0, 'no', 'normal', '', 0, '2018-06-07 23:27:27', 396, '2002-06-21', 0, 1, 1, '2018-06-07 23:27:56', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1528439276663969331', NULL, 12, 2, '0', '0000-00-00 00:00:00'),
(398, 'Poetic_outis', 'Christian', 'bighead', 'christianadam2002@gmail.com', 'cG9sbG9jazEyMw==', 'uploads/userthumbimage/1528776950-profile_img.jpg', 'Male', 36, 0, 'no', 'normal', '', 0, '2018-06-11 21:15:50', 398, '2002-09-28', 0, 1, 1, '2018-06-11 21:18:14', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1528777094125333735', NULL, 7, 2, '0', '0000-00-00 00:00:00'),
(401, 'Cyprustree', 'Ryan', 'Kretschmar', 'ryankrtschmr@gmail.com', 'V2luc3RvbjEw', 'uploads/userthumbimage/1529153617-profile_img.jpg', 'Male', 9, 0, 'no', 'normal', '', 0, '2018-06-16 05:53:37', 401, '1996-08-12', 0, 1, 1, '2018-06-16 05:54:53', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15291536931054516884', NULL, 7, 2, '0', '0000-00-00 00:00:00'),
(402, 'Mrwonderful', 'Eric', 'Rosner', 'rosner1@mac.com', '', 'uploads/userthumbimage/1529394004-myImage.png', '', 0, 10, 'yes', 'facebook', '10215164121911833', 0, '2018-06-19 00:40:04', 0, '0000-00-00', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1529394004781787758', NULL, 33, 2, '0', '0000-00-00 00:00:00'),
(403, 'Zhoumj888', 'Daniel', 'Zhou', 'zhoumj000@gmail.com', '', 'uploads/userthumbimage/1529496434-myImage.png', 'Male', 0, 2, 'yes', 'facebook', '224211235037856', 0, '2018-06-20 05:07:14', 0, '1987-11-03', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15294964341268726578', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(406, 'Yellhtetnaun', 'Yell Htet', 'Htet', 'yellhtetnaung@gmail.com', '', 'uploads/userthumbimage/1529712611-profile_img.jpg', 'Male', 0, 91, 'yes', 'facebook', '268581653715834', 0, '2018-06-22 17:10:11', 0, '2002-06-22', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1529712611897246632', NULL, 7, 2, '0', '0000-00-00 00:00:00'),
(407, 'Mlowe1981', 'Matt', 'Lowe', 'matt.lowe1981@gmail.com', '', 'uploads/userthumbimage/1530103578-profile_img.jpg', 'Male', 10, 0, 'yes', 'facebook', '10155435499307761', 0, '2018-06-27 05:46:18', 0, '1981-12-05', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15301035781494447942', NULL, 90, 2, '0', '0000-00-00 00:00:00'),
(408, 'Madeash', 'Asha Kanta', 'Sharma', 'made_ash@yahoo.co.in', 'SFNJTFI0NTY=', 'uploads/userthumbimage/1530257951-profile_img.jpg', 'Male', 0, 41, 'yes', 'normal', '', 0, '2018-06-29 00:39:11', 408, '1988-10-12', 0, 1, 1, '2018-06-29 00:40:14', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15302580141727410045', NULL, 29, 2, '0', '0000-00-00 00:00:00'),
(409, 'Sidratul', 'Sidratul', 'Muntaha', 'Sidratul12diamondgodda@gmail.com', 'NDVtb29uKz0lY2xvdGg=', 'uploads/userthumbimage/1530268326-profile_img.jpeg', 'Male', 0, 41, 'yes', 'normal', '', 0, '2018-06-29 03:32:06', 409, '2001-10-28', 0, 1, 1, '2018-06-29 07:51:04', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1530283864114455203', NULL, 17, 2, '0', '0000-00-00 00:00:00'),
(410, '0653187474', 'Abuu', 'Hamedy', 'athumanij99@gmail.com', '', 'uploads/userthumbimage/1530786312-profile_img.jpg', 'Male', 0, 90, 'yes', 'facebook', '1852596091714143', 0, '2018-07-05 03:25:13', 0, '0000-00-00', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15307863122000415768', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(412, 'Ranranran', 'Yan', 'Ran', '1260076542@qq.com', 'UkFOWUFOamlheW91Mw==', 'uploads/userthumbimage/1531195193-myImage.png', 'Female', 0, 6, 'yes', 'normal', '', 0, '2018-07-09 20:59:53', 412, '1996-08-14', 0, 1, 1, '2018-07-09 21:00:21', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1531195221724716072', NULL, 13, 2, '0', '0000-00-00 00:00:00'),
(413, 'Yourmanbran', 'Brandon', 'Simmons', 'brandoniscool715@gmail.com', 'RWxlbWVudDQ0', 'uploads/userthumbimage/1531202407-myImage.png', 'Male', 5, 0, 'no', 'normal', '', 0, '2018-07-09 23:00:07', 413, '2000-07-15', 0, 1, 1, '2018-07-09 23:04:43', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1531202683963625436', NULL, 7, 2, '0', '0000-00-00 00:00:00'),
(414, 'Itsmeluigi', 'Luigi', 'Re', 'rebollosluigi@gmail.com', '', 'uploads/userthumbimage/1731970036_1531441537.jpg', 'Male', 32, 0, 'yes', 'facebook', '10214504550626442', 0, '2018-07-12 17:16:06', 414, '1993-06-08', 0, 1, 1, '2018-07-12 17:25:37', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15314409661589017213', NULL, 12, 2, '0', '0000-00-00 00:00:00'),
(416, 'Rapwithchap', 'Otha', 'Bell', 'apostleotha.bell@gmail.com', 'Rml2ZWZvbGRtaW4=', 'uploads/userthumbimage/1531453161-profile_img.jpeg', 'Male', 43, 0, 'yes', 'normal', '', 0, '2018-07-12 20:39:21', 416, '1959-11-10', 0, 1, 1, '2018-07-12 20:43:47', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15314534271837513478', NULL, 18, 2, '0', '0000-00-00 00:00:00'),
(417, 'Missmaced', 'Raquel', 'Heeter', 'raquel.heeter@gmail.com', '', 'uploads/userthumbimage/1531455642-profile_img.jpg', 'Female', 35, 0, 'no', 'facebook', '1516120571825147', 0, '2018-07-12 21:20:42', 0, '1992-06-18', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1531455642285662406', NULL, 12, 2, '0', '0000-00-00 00:00:00'),
(418, 'Fuzzycurtain', 'Fuzzy', 'Curtain', 'fuzzycurtain97@gmail.com', 'Um9mbHJvZmwxPw==', 'uploads/userthumbimage/1531461382-profile_img.jpg', 'Male', 9, 0, 'no', 'normal', '', 0, '2018-07-12 22:56:22', 418, '1997-08-23', 0, 1, 1, '2018-07-12 22:57:24', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1531461444367437388', NULL, 9, 2, '0', '0000-00-00 00:00:00'),
(419, 'Wonder57', 'Preston', 'Young', 'preston99999@gmail.com', 'UGFnb2dvNzc=', 'uploads/userthumbimage/1531462211-profile_img.jpeg', 'Male', 28, 0, 'no', 'normal', '', 0, '2018-07-12 23:10:11', 419, '1999-10-20', 0, 1, 1, '2018-07-12 23:12:19', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1531462339515981321', NULL, 9, 2, '0', '0000-00-00 00:00:00'),
(420, 'Simari1240', 'Simari', 'Tiney', 'raginbeach55@gmail.com', '', 'uploads/userthumbimage/1531466654-profile_img.jpeg', 'Female', 42, 0, 'yes', 'facebook', '200067714025600', 0, '2018-07-13 00:24:15', 0, '2003-12-12', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15314666541832078563', NULL, 24, 2, '0', '0000-00-00 00:00:00'),
(421, 'Jason_seb', 'Jason', 'Sebera', 'jasonsebera@gmail.com', '', 'uploads/userthumbimage/1531483715-profile_img.jpg', 'Male', 0, 4, 'no', 'facebook', '237271976886307', 0, '2018-07-13 05:08:35', 0, '1998-12-06', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15314837151453604', NULL, 5, 2, '0', '0000-00-00 00:00:00'),
(422, 'Spacecaps', 'Nicholas', 'S', 'n.shokoff@gmail.com', 'QXByaWwyMDE2', 'uploads/userthumbimage/1531495877-profile_img.jpg', 'Male', 0, 4, 'no', 'normal', '', 0, '2018-07-13 08:31:17', 422, '1990-04-24', 0, 1, 1, '2018-07-13 08:33:08', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15314959881518989988', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(423, 'Smartdrevil', 'Sagar', 'Khanal', 'smartdrevil@gmail.com', '', 'uploads/userthumbimage/1531497669-profile_img.jpg', 'Male', 0, 62, 'no', 'facebook', '2160723334162632', 0, '2018-07-13 09:01:09', 0, '1996-05-20', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15314976691352633950', NULL, 8, 2, '0', '0000-00-00 00:00:00'),
(424, 'Waynewash', 'Dwayne', 'Washington', 'waynewash@gmail.com', 'Qmx1ZWJsYWNrODM=', 'uploads/userthumbimage/1531498840-profile_img.jpg', 'Male', 22, 0, 'no', 'normal', '', 0, '2018-07-13 09:20:40', 424, '1983-11-10', 0, 1, 1, '2018-07-13 09:21:46', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15314989061496775065', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(425, 'Bea543', 'Christopher', 'Gibson', 'cagibson543@gmail.com', '', 'uploads/userthumbimage/1531498862-profile_img.jpg', 'Male', 0, 2, 'yes', 'facebook', '2146098895623229', 0, '2018-07-13 09:21:02', 0, '2001-07-25', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15314988621789940977', NULL, 10, 2, '0', '0000-00-00 00:00:00'),
(426, 'Coastiepete', 'Peter', 'Mills', 'p.mills@y7mail.com', '', 'uploads/userthumbimage/1531503780-profile_img.jpg', 'Male', 0, 2, 'yes', 'facebook', '10214620427177272', 0, '2018-07-13 10:43:00', 0, '1971-06-28', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1531503780371477622', NULL, 28, 2, '0', '0000-00-00 00:00:00'),
(428, 'Serenajayde', 'Serena', 'Jayde', 'sj@serenajayde.com', 'Ym4xMDg5MzJE', 'uploads/userthumbimage/1531536361-profile_img.jpg', 'Female', 0, 2, 'no', 'normal', '', 0, '2018-07-13 19:46:01', 428, '1987-05-31', 0, 1, 1, '2018-07-13 19:46:43', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1531536403511127067', NULL, 10, 2, '0', '0000-00-00 00:00:00'),
(429, 'Cstuben', 'Christopher', 'Stubentauch', 'christopher.stubenrauch@gmail.com', 'enI2OU5rUjg=', 'uploads/userthumbimage/1531547340-myImage.png', 'Male', 0, 2, 'yes', 'normal', '', 0, '2018-07-13 22:49:00', 429, '1987-07-11', 0, 1, 1, '2018-07-13 22:50:55', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15315474551336907075', NULL, 14, 2, '0', '0000-00-00 00:00:00'),
(430, 'Julian', 'Julian', 'Atkins', 'julianatkins@msn.com', '', 'uploads/userthumbimage/1531558161-profile_img.jpeg', 'Male', 0, 10, 'yes', 'facebook', '10156214179676542', 0, '2018-07-14 01:49:21', 0, '1958-01-18', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15315581611287686887', NULL, 15, 2, '0', '0000-00-00 00:00:00'),
(431, 'Shaunhall', 'Shaun', 'Hall', 'skennyhall@gmail.com', '', 'uploads/userthumbimage/1531561993-profile_img.jpg', 'Male', 0, 10, 'yes', 'facebook', '10217490191952214', 0, '2018-07-14 02:53:13', 0, '1987-10-27', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15315619932083486997', NULL, 14, 2, '0', '0000-00-00 00:00:00'),
(434, 'Gcate08', 'Glenda', 'Cates', 'gmcates8485@hotmail.com', '', 'uploads/userthumbimage/1531586514-profile_img.jpg', 'Female', 34, 0, 'no', 'facebook', '10156541294088485', 0, '2018-07-14 09:41:54', 0, '1985-04-01', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15315865141770732373', NULL, 12, 2, '0', '0000-00-00 00:00:00'),
(435, 'Kuroiessek', 'Tanvir', 'Sk Hossain', 'hossaintanvir97@gmail.com', '', 'uploads/userthumbimage/1531595452-profile_img.jpg', 'Male', 0, 10, 'no', 'facebook', '1744325898969695', 0, '2018-07-14 12:10:52', 0, '1999-05-08', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15315954521760978540', NULL, 4, 2, '0', '0000-00-00 00:00:00'),
(436, 'Michxle', 'Michele', 'tarin', 'baileevillany@gmail.com', 'UXdlcnR5MTA5Mg==', 'uploads/userthumbimage/1531616396-profile_img.jpg', 'Female', 10, 0, 'yes', 'normal', '', 0, '2018-07-14 17:59:56', 436, '2001-08-29', 0, 1, 1, '2018-07-14 18:00:38', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1531616438183742107', NULL, 20, 2, '0', '0000-00-00 00:00:00'),
(437, 'Flyngmdgt', 'Brandon', 'Merry', 'brandonsmerry@gmail.com', '', 'uploads/userthumbimage/718495324_1531625477.jpeg', 'Male', 22, 0, 'no', 'facebook', '209010116608260', 0, '2018-07-14 20:28:14', 437, '1992-10-21', 0, 1, 1, '2018-08-08 12:41:20', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1533757280375145226', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(439, 'K23ndall23', 'Kendall', 'Fairley', 'k23ndall23@gmail.com', '', 'uploads/userthumbimage/1531629779-profile_img.jpg', 'Male', 38, 0, 'yes', 'facebook', '739340843092303', 0, '2018-07-14 21:42:59', 0, '1986-09-25', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15316297791892148069', NULL, 15, 2, '0', '0000-00-00 00:00:00'),
(441, 'Mahesh', 'Mahesh', 'Mareeswaran', 'maheshgautham21@gmail.com', '', 'uploads/userthumbimage/1531632954-profile_img.jpg', 'Male', 0, 41, 'yes', 'facebook', '1724399470992683', 0, '2018-07-14 22:35:54', 0, '0000-00-00', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15316329541820565244', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(442, 'Superiormind', 'Richard', 'Vittozzi', 'rav57rav@gmail.com', 'Qmx1ZVNreTU3', 'uploads/userthumbimage/1531652868-profile_img.jpeg', 'Male', 21, 0, 'yes', 'normal', '', 0, '2018-07-15 04:07:48', 442, '1961-08-02', 0, 1, 1, '2018-07-15 04:09:22', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15316529621227214272', NULL, 27, 2, '0', '0000-00-00 00:00:00'),
(444, 'Fdpanda222', 'Faith', 'D', 'fdpanda222@gmail.com', 'SmFzcGVyMSE=', 'uploads/userthumbimage/1531675131-profile_img.jpg', 'Female', 43, 0, 'no', 'normal', '', 0, '2018-07-15 10:18:51', 444, '2000-02-22', 0, 1, 1, '2018-07-15 10:19:51', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15316751911272049500', NULL, 10, 2, '0', '0000-00-00 00:00:00'),
(446, 'Superskippy', 'Angela', 'Wells', 'hawthorne.heights@hotmail.com', '', 'uploads/userthumbimage/1531680514-profile_img.jpg', 'Female', 0, 4, 'no', 'facebook', '10160559106265510', 0, '2018-07-15 11:48:34', 0, '1989-09-18', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15316805141607597258', NULL, 12, 2, '0', '0000-00-00 00:00:00'),
(449, 'Rhs007', 'Hamzah', 'Sarmad', 'hamzah12345@hotmail.co.uk', '', 'uploads/userthumbimage/1531696207-profile_img.jpg', 'Male', 0, 10, 'no', 'facebook', '10214611385472118', 0, '2018-07-15 16:10:07', 0, '0000-00-00', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1531696207751985346', NULL, 7, 2, '0', '0000-00-00 00:00:00'),
(451, 'Addie87', 'Ian', 'Somerville', 'ianrsomerville@yahoo.com', '', 'uploads/userthumbimage/1531713976-profile_img.jpg', 'Male', 20, 0, 'yes', 'facebook', '10156367932286698', 0, '2018-07-15 21:06:16', 0, '1987-03-18', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15317139762080265262', NULL, 23, 2, '0', '0000-00-00 00:00:00'),
(452, 'Joehiles', 'Joe', 'Hiles', 'joe.hiles@gmail.com', '', 'uploads/userthumbimage/1531715488-profile_img.jpg', 'Male', 35, 0, 'no', 'facebook', '10214890412906721', 0, '2018-07-15 21:31:28', 0, '1978-06-10', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1531715488176109267', NULL, 12, 2, '0', '0000-00-00 00:00:00'),
(453, 'Muffinbum', 'Becky', 'Parsons', 'muffinbum@myself.com', '', 'uploads/userthumbimage/1531726323-profile_img.jpg', 'Female', 0, 4, 'yes', 'facebook', '10160726835320066', 0, '2018-07-16 00:32:03', 453, '1975-02-19', 0, 1, 1, '2018-07-16 00:32:31', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1531726323877759031', NULL, 22, 2, '0', '0000-00-00 00:00:00'),
(454, 'Chasito', 'chase', 'w.', 'chasebruh69@gmail.com', 'UGF5bGVzczE=', 'uploads/userthumbimage/1531729651-profile_img.jpg', 'Male', 43, 0, 'no', 'normal', '', 0, '2018-07-16 01:27:31', 454, '2000-05-18', 0, 1, 1, '2018-07-16 01:34:32', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15317300721101542502', NULL, 3, 2, '0', '0000-00-00 00:00:00'),
(457, 'Rancis', 'Randar', 'Narme', 'randar2001@gmail.com', '', 'uploads/userthumbimage/1531771462-profile_img.jpeg', 'Male', 0, 31, 'no', 'facebook', '885839438257882', 0, '2018-07-16 13:04:22', 0, '2001-03-12', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15317714621814203696', NULL, 10, 2, '0', '0000-00-00 00:00:00'),
(460, 'Silent406', 'Tony', 'hi a be 11aaaa@q@. ,? Bill 7$zzz zzz%@ásd,xx', 'tonyhills406@gmail.com', 'bGFrZXJz', 'uploads/userthumbimage/1531791454-profile_img.jpg', 'Male', 26, 0, 'yes', 'normal', '', 0, '2018-07-16 18:37:34', 460, '0000-00-00', 0, 1, 1, '2018-07-16 18:51:41', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15317914541528761829', NULL, 24, 2, '0', '0000-00-00 00:00:00'),
(462, 'Silent', 'Anthony', 'Hill', 'anthonyhill406@gmail.com', '', 'uploads/userthumbimage/1531792759-profile_img.jpg', 'Male', 26, 0, 'yes', 'facebook', '2028155127197591', 0, '2018-07-16 18:59:19', 0, '0000-00-00', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1531792759836238443', NULL, 29, 2, '0', '0000-00-00 00:00:00'),
(463, 'Megasoma', 'Quincy', 'Hayes', 'qu.ce.al@gmail.com', 'Wm9vbG9vMDA=', 'uploads/userthumbimage/1531798252-profile_img.jpg', 'Transgendered Male', 16, 0, 'no', 'normal', '', 0, '2018-07-16 20:30:52', 463, '2001-05-31', 0, 1, 1, '2018-07-16 20:31:46', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1531798306854594095', NULL, 7, 2, '0', '0000-00-00 00:00:00'),
(465, 'Annabanana', 'Anna', 'Mikat', 'amika1999@gmail.com', '', 'uploads/userthumbimage/1531806995-profile_img.jpg', 'Female', 49, 0, 'no', 'facebook', '2272687792962740', 0, '2018-07-16 22:56:35', 0, '1999-09-27', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15318069951177457668', NULL, 9, 2, '0', '0000-00-00 00:00:00'),
(466, 'Jimmyybubs', 'Imogen', 'Krollig', 'imogenkrollig@gmail.com', '', 'uploads/userthumbimage/1531809127-profile_img.jpg', 'Female', 0, 2, 'yes', 'facebook', '491747461250008', 0, '2018-07-16 23:32:07', 0, '0000-00-00', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15318091271116749889', NULL, 11, 2, '0', '0000-00-00 00:00:00'),
(467, 'Tcrowe4288', 'Tonya', 'Crowe', 'tcrowe4288@gmail.com', '', 'uploads/userthumbimage/2015207102_1531813280.jpg', 'Female', 33, 0, 'yes', 'facebook', '1904517732902769', 0, '2018-07-17 00:40:28', 467, '1968-06-17', 0, 1, 1, '2018-07-17 00:41:20', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15318132281423619393', NULL, 19, 2, '0', '0000-00-00 00:00:00'),
(468, 'Digitalgraff', 'M', 'W', 'meg@digitalgraffiti.ca', 'MTc1NjkxNzU=', 'uploads/userthumbimage/1999877693_1531822309.jpg', 'Female', 0, 4, 'no', 'normal', '', 0, '2018-07-17 02:35:31', 468, '1983-04-22', 0, 1, 1, '2018-07-17 03:11:49', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '153182018050190671', NULL, 14, 2, '0', '0000-00-00 00:00:00'),
(469, 'M_iraq', '???', '?????', 'huhu199533@gmail.com', '', 'uploads/userthumbimage/1531820941-profile_img.jpeg', 'Male', 0, 43, 'yes', 'facebook', '274806973271085', 0, '2018-07-17 02:49:01', 0, '1995-03-03', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15318209411012484388', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(470, 'Austinkeelin', 'Austin', 'Keelin', 'austinkeelin90@gmail.com', '', 'uploads/userthumbimage/1531835464-profile_img.jpg', 'Male', 36, 0, 'yes', 'facebook', '10156443723078290', 0, '2018-07-17 06:51:04', 0, '1991-05-05', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1531835464826740702', NULL, 21, 2, '0', '0000-00-00 00:00:00'),
(471, 'Charles', 'Charles', 'Krista', 'charleskeith78@gmail.com', '', 'uploads/userthumbimage/1531837502-profile_img.jpg', 'Male', 17, 0, 'yes', 'facebook', '145214229708160', 0, '2018-07-17 07:25:02', 0, '0000-00-00', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1531837502191781405', NULL, 6, 2, '0', '0000-00-00 00:00:00'),
(473, 'John8apolo', 'Calvin', 'Decker', 'calvinkkdecker5@gmail.com', 'bml2bGFjMjAwMQ==', 'uploads/userthumbimage/1531846463-profile_img.jpg', 'Male', 0, 4, 'yes', 'normal', '', 0, '2018-07-17 09:54:23', 473, '2001-10-09', 0, 1, 1, '2018-07-17 09:55:32', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15318465321298854381', NULL, 7, 2, '0', '0000-00-00 00:00:00'),
(474, 'Aliceslayss', 'Alice', 'slayss', 'aliceayvazian09@gmail.com', '', 'uploads/userthumbimage/1531852521-profile_img.jpeg', 'Female', 0, 4, 'yes', 'facebook', '241343526595721', 0, '2018-07-17 11:35:21', 0, '2002-12-03', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1531852521204472604', NULL, 10, 2, '0', '0000-00-00 00:00:00'),
(475, 'Ukboy76', 'Stephen', 'Hick', 'stehick@live.co.uk', '', 'uploads/userthumbimage/1531886300-profile_img.jpg', 'Male', 0, 10, 'yes', 'facebook', '10155478303421921', 0, '2018-07-17 20:58:20', 0, '1976-01-12', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1531886300566691027', NULL, 23, 2, '0', '0000-00-00 00:00:00'),
(476, 'Mirandusa', 'Emily', 'Miranda', 'mirandusabrasil@gmail.com', 'MTIzbW9ua2V5', 'uploads/userthumbimage/1531890562-profile_img.jpg', '', 9, 0, 'no', 'normal', '', 0, '2018-07-17 22:09:22', 476, '0000-00-00', 0, 1, 1, '2018-07-17 22:10:03', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1531890603634136853', NULL, 9, 2, '0', '0000-00-00 00:00:00'),
(477, '2of5star', 'Mason', 'Scheels', 'markthrill2109@gmail.com', '', 'uploads/userthumbimage/1531898447-profile_img.jpg', 'Male', 9, 0, 'no', 'facebook', '196815230985781', 0, '2018-07-18 00:20:47', 0, '2001-09-20', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1531898447488860822', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(478, 'Gatsby', 'Michelle', 'Roshdy', 'michelleroshdy@yahoo.com', '', 'uploads/userthumbimage/1531910342-profile_img.jpg', 'Female', 5, 0, 'yes', 'facebook', '10217946152112049', 0, '2018-07-18 03:39:02', 0, '1992-10-15', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15319103421623318931', NULL, 20, 2, '0', '0000-00-00 00:00:00'),
(479, 'Blkboi', 'Kay', 'Jay', 'augmentrecords@gmail.com', 'YnJpbmdtZXRoZWhvcml6b24x', 'uploads/userthumbimage/1531920066-profile_img.jpg', 'Male', 0, 2, 'no', 'normal', '', 0, '2018-07-18 06:21:06', 479, '1993-09-15', 0, 1, 1, '2018-07-18 06:30:47', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1531920647276437865', NULL, 10, 2, '0', '0000-00-00 00:00:00'),
(480, 'Cajungirl', 'Susan', 'Barras Norton', 'susanfaulk20@gmail.com', '', 'uploads/userthumbimage/1531925945-profile_img.jpg', 'Female', 9, 0, 'yes', 'facebook', '577085169352773', 0, '2018-07-18 07:59:05', 0, '0000-00-00', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1531925945586033427', NULL, 29, 2, '0', '0000-00-00 00:00:00'),
(483, 'Rubydragon', 'kellen', 'flynn', 'kellen.flynn@gmail.com', 'MTg4c29uaWM=', 'uploads/userthumbimage/1531965055-profile_img.jpg', 'Male', 20, 0, 'no', 'normal', '', 0, '2018-07-18 18:50:55', 483, '1990-07-14', 0, 1, 1, '2018-07-18 18:53:40', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15319652201336076921', NULL, 5, 2, '0', '0000-00-00 00:00:00'),
(484, 'Xejaio', 'Jenna', 'Moor', 'jenex02@frontier.com', '', 'uploads/userthumbimage/1531968327-profile_img.jpg', 'Female', 14, 0, 'no', 'facebook', '140575483497243', 0, '2018-07-18 19:45:27', 0, '2002-09-13', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15319683271241217710', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(485, 'Stburri', 'Steven', 'Burris', 'stevenburrisdrumright@gmail.com', '', 'uploads/userthumbimage/1531968870-profile_img.jpg', 'Male', 36, 0, 'yes', 'facebook', '2072834283034978', 0, '2018-07-18 19:54:30', 0, '1974-03-18', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15319688701144721261', NULL, 18, 2, '0', '0000-00-00 00:00:00'),
(486, 'Cloudtheking', 'Cloud', 'King', 'cloudkingof94@gmail.com', 'U2hhZHljbG91ZA==', 'uploads/userthumbimage/1531970265-profile_img.jpg', 'Male', 36, 0, 'no', 'normal', '', 0, '2018-07-18 20:17:45', 486, '1991-05-20', 0, 1, 1, '2018-07-18 20:18:29', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15319703091171536826', NULL, 12, 2, '0', '0000-00-00 00:00:00'),
(487, 'Accrist', 'Alex', 'Crist', 'accrist@gmail.com', 'YzJXT09VWkwmUlJ0STklKg==', 'uploads/userthumbimage/1531974894-profile_img.jpg', 'Male', 3, 0, 'no', 'normal', '', 0, '2018-07-18 21:34:54', 487, '1991-08-31', 0, 1, 1, '2018-07-18 21:42:25', '0000-00-00 00:00:00', 969278, '2018-07-18 21:40:59', 0, NULL, '15319753451960290552', NULL, 12, 2, '0', '0000-00-00 00:00:00'),
(488, 'Mikestrike3', 'Michael', 'Ritchie', 'ma6214470@gmail.com', '', 'uploads/userthumbimage/1531984192-profile_img.jpg', 'Male', 0, 4, 'no', 'facebook', '2147862395492324', 0, '2018-07-19 00:09:52', 0, '1999-12-01', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15319841921422579378', NULL, 10, 2, '0', '0000-00-00 00:00:00'),
(489, 'Gr8dane', 'James', 'Ladefoged', 'gr8danejames81@gmail.com', 'QklHYW1lamFtZXM4MQ==', 'uploads/userthumbimage/1532009570-profile_img.jpg', 'Male', 35, 0, 'no', 'normal', '', 0, '2018-07-19 07:12:50', 489, '1981-10-13', 0, 1, 1, '2018-07-19 07:13:32', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1532009612824509473', NULL, 12, 2, '0', '0000-00-00 00:00:00'),
(490, 'Donnamarie', 'Donna Marie', 'Rasler', 'uneasyrider18@gmail.com', '', 'uploads/userthumbimage/1532021985-profile_img.jpg', 'Female', 14, 0, 'yes', 'facebook', '109776656615642', 0, '2018-07-19 10:39:45', 0, '1972-11-23', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1532021985616769541', NULL, 28, 2, '0', '0000-00-00 00:00:00'),
(492, 'Redhawx', 'Pavan', 'Kallam', 'Kallampavan365@gmail.com', 'UEB2QG4ja2FsbGFt', 'uploads/userthumbimage/1532049675-profile_img.jpg', 'Male', 0, 41, 'no', 'normal', '', 0, '2018-07-19 18:21:15', 492, '1993-05-15', 0, 1, 1, '2018-07-19 18:23:07', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15320497872104511545', NULL, 12, 2, '0', '0000-00-00 00:00:00'),
(495, 'Saberghaderi', 'Saber', 'Ghaderi', 'ghaderi.sabir@gmail.com', 'dGFlMTgyODg3NQ==', 'uploads/userthumbimage/1532113688-profile_img.jpeg', 'Male', 0, 93, 'yes', 'normal', '', 0, '2018-07-20 12:08:08', 495, '1991-06-01', 0, 1, 1, '2018-07-20 12:13:28', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15321140081169820192', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(496, 'Mrsthursday', 't', 'thom', 'ta_thom@yahoo.ca', 'cHJpbmNlc3NjYXQx', 'uploads/userthumbimage/1532114386-profile_img.jpeg', 'Female', 0, 4, 'yes', 'normal', '', 0, '2018-07-20 12:19:46', 496, '1957-10-31', 0, 1, 1, '2018-07-20 12:20:19', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1532114386691228510', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(499, 'Hugocanosa', 'Hugo', 'Canosa', 'hugo.canosa@hotmail.com', '', 'uploads/userthumbimage/1532130115-profile_img.jpg', 'Male', 0, 72, 'no', 'facebook', '1633908300068394', 0, '2018-07-20 16:41:55', 0, '2003-05-20', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15321301151769960374', NULL, 7, 2, '0', '0000-00-00 00:00:00'),
(500, 'Haggis606', 'Mark', 'Mcgugan', 'haggis_googie@hotmail.com', '', 'uploads/userthumbimage/1532171500-profile_img.jpg', 'Male', 0, 10, 'yes', 'facebook', '10216611686264853', 0, '2018-07-21 04:11:42', 0, '1976-07-20', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1532171500690450766', NULL, 15, 2, '0', '0000-00-00 00:00:00'),
(501, 'Ticemayn', 'Matthew', 'Ness', 'ticemayn1@hotmail.com', '', 'uploads/userthumbimage/1532176664-profile_img.jpg', 'Male', 0, 4, 'no', 'facebook', '1198044680334704', 0, '2018-07-21 05:37:44', 0, '2018-07-21', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15321766642112411218', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(502, 'Gmoore', 'Gary', 'Moore', 'gary@groundschool.org', 'YmlwbGFuZQ==', 'uploads/userthumbimage/1532190344-profile_img.jpg', 'Male', 43, 0, 'no', 'normal', '', 0, '2018-07-21 09:25:44', 502, '1960-08-12', 0, 1, 1, '2018-07-21 09:26:30', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1532190390968864035', NULL, 4, 2, '0', '0000-00-00 00:00:00'),
(503, 'Hk28902', 'hannah', 'kraljevic', 'hk28902@gmail.com', 'ZW1pdHRuZWxzb24=', 'uploads/userthumbimage/1532190407-profile_img.jpg', 'Female', 1, 0, 'no', 'normal', '', 0, '2018-07-21 09:26:47', 503, '1996-11-20', 0, 1, 1, '2018-07-21 09:27:21', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1532190441311899066', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(504, 'Pudnegg', 'Katrina', 'Hogan', 'katrinahogan87@gmail.com', 'SGFybGV5dGFzdGljMTE=', 'uploads/userthumbimage/1532193787-profile_img.jpg', 'Female', 0, 2, 'no', 'normal', '', 0, '2018-07-21 10:23:07', 504, '1987-04-17', 0, 1, 1, '2018-07-21 10:23:36', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15321938161440008060', NULL, 12, 2, '0', '0000-00-00 00:00:00'),
(505, 'Huesandblues', 'Nikki', 'O\'Connell', 'nikkioco@gmail.com', '', 'uploads/userthumbimage/1532196365-profile_img.jpg', 'Female', 40, 0, 'yes', 'facebook', '10215459449513673', 0, '2018-07-21 11:06:05', 0, '1985-05-05', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1532196365631456619', NULL, 29, 2, '0', '0000-00-00 00:00:00'),
(506, 'Vinyljedi', 'Vinyl', 'Jedi', 'kenny_higgs@yahoo.co.uk', '', 'uploads/userthumbimage/1532208165-profile_img.jpeg', 'Male', 0, 10, 'no', 'facebook', '10160576482465006', 0, '2018-07-21 14:22:45', 0, '1972-10-30', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1532208165700067932', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(508, 'Myshadow462', 'Adam', 'Wampler', 'adam.wampler@gmail.com', 'VnlFTyVWSmdMMyNAV09AbQ==', 'uploads/userthumbimage/1532235010-profile_img.jpg', 'Male', 46, 0, 'no', 'normal', '', 0, '2018-07-21 21:50:10', 508, '1985-03-25', 0, 1, 1, '2018-07-21 21:50:45', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1532235045467000509', NULL, 10, 2, '0', '0000-00-00 00:00:00'),
(509, 'Katticans', 'Kat', 'Hughes', 'katticans@gmail.com', '', 'uploads/userthumbimage/1532261604-profile_img.jpg', 'Female', 0, 2, 'no', 'facebook', '840263939504204', 0, '2018-07-22 05:13:24', 0, '1993-02-06', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15322616041382434975', NULL, 7, 2, '0', '0000-00-00 00:00:00'),
(510, 'Jayjay', 'Jay', 'Thomson', 'jaythomson100@gmail.com', 'RmVycmFyaTMxOTY=', 'uploads/userthumbimage/1532261745-profile_img.jpeg', 'Male', 0, 10, 'no', 'normal', '', 0, '2018-07-22 05:15:45', 510, '2000-09-22', 0, 1, 1, '2018-07-22 05:16:37', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1532261797522954966', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(511, 'Satch500', 'Simon', 'Archbold', 'just_me_and_my_guitar@hotmail.co.uk', '', 'uploads/userthumbimage/1532265438-profile_img.jpg', 'Male', 0, 10, 'yes', 'facebook', '10160702406525118', 0, '2018-07-22 06:17:19', 0, '1986-07-08', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1532265438399082314', NULL, 26, 2, '0', '0000-00-00 00:00:00'),
(512, 'Anotherone', 'Drew', 'EscareYou', 'drewgreat2013@gmail.com', '', 'uploads/userthumbimage/1532266211-profile_img.jpg', 'Male', 0, 10, 'yes', 'facebook', '1990344804309243', 0, '2018-07-22 06:30:11', 0, '0000-00-00', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15322662111362741782', NULL, 29, 2, '0', '0000-00-00 00:00:00'),
(514, 'Terrtam', 'Terrence', 'Tam', 'terrtam@gmail.com', 'aWhhdGVtaWxsaXBvbzEyMw==', 'uploads/userthumbimage/1532270339-profile_img.jpg', 'Male', 0, 4, 'no', 'normal', '', 0, '2018-07-22 07:38:59', 514, '2002-11-06', 0, 1, 1, '2018-07-22 07:40:13', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1532270413937342318', NULL, 13, 2, '0', '0000-00-00 00:00:00'),
(515, 'Joshua', 'Joshua', 'B', 'joshx115@gmail.com', '', 'uploads/userthumbimage/1532271021-profile_img.jpg', 'Male', 5, 0, 'no', 'facebook', '670280016668853', 0, '2018-07-22 07:50:21', 0, '1993-07-22', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1532271021477646875', NULL, 3, 2, '0', '0000-00-00 00:00:00'),
(518, 'Missangela', 'Angela', 'Everett', 'angelaeverett@sky.com', '', 'uploads/userthumbimage/1532314755-profile_img.jpg', 'Female', 0, 10, 'yes', 'facebook', '2142553249091601', 0, '2018-07-22 19:59:15', 0, '1979-12-14', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15323147551902242022', NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(520, 'Russianbot37', 'Sasha', 'Allen', 'e.s.a.oficiale@gmail.com', 'MjVhc2RmMjU=', 'uploads/userthumbimage/1532337138-profile_img.jpg', 'Male', 7, 0, 'yes', 'normal', '', 0, '2018-07-23 02:12:19', 520, '2004-11-09', 0, 1, 1, '2018-07-23 02:13:12', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1532337192137680238', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(521, 'Okkaymen', 'Lindsay', 'Kaymen', 'okkaymen@gmail.com', 'bC5rLjA0Mjk=', 'uploads/userthumbimage/1127272742_1532345653.jpg', 'Female', 13, 0, 'yes', 'normal', '', 0, '2018-07-23 04:31:56', 521, '1989-03-06', 0, 1, 1, '2018-07-23 04:34:13', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15323456201663991754', NULL, 27, 2, '0', '0000-00-00 00:00:00'),
(522, 'Jared_', 'Jared', 'Mercier', 'jaredm12234@gmail.com', 'TWVyY2llcg==', 'uploads/userthumbimage/1532359488-profile_img.jpg', 'Male', 42, 0, 'no', 'normal', '', 0, '2018-07-23 08:24:48', 522, '2002-02-18', 0, 1, 1, '2018-07-23 08:25:33', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1532359533194902862', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(524, 'Emmajedy', 'emma', 'jedy', 'omemma.jaadi34@gmail.com', 'aWxvdmVsb2xseXBvcHNzb211Y2g=', 'uploads/userthumbimage/1532380835-profile_img.jpg', 'Female', 47, 0, 'yes', 'normal', '', 0, '2018-07-23 14:20:35', 524, '2001-04-26', 0, 1, 1, '2018-07-23 14:21:21', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15323808812062159220', NULL, 29, 2, '0', '0000-00-00 00:00:00'),
(525, 'Sexy71', 'C', 'Forrester', 'forresterchriselda71@gmail.com', '', 'uploads/userthumbimage/1532391239-profile_img.jpg', 'Female', 43, 0, 'yes', 'facebook', '258219938309841', 0, '2018-07-23 17:13:59', 0, '0000-00-00', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15323912391831347190', NULL, 19, 2, '0', '0000-00-00 00:00:00'),
(526, 'Lanovelita', 'Jennifer', 'Ogosi Bergeron', 'beauteful_girl_05@hotmail.com', '', 'uploads/userthumbimage/1532391610-profile_img.jpg', 'Female', 0, 4, 'yes', 'facebook', '10155483465281288', 0, '2018-07-23 17:20:10', 0, '0000-00-00', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1532391610433316751', NULL, 18, 2, '0', '0000-00-00 00:00:00'),
(527, 'Lizbel', 'Liz', 'Belfast', 'elizabethannbelfast@gmail.com', '', 'uploads/userthumbimage/1532393481-profile_img.jpg', 'Female', 32, 0, 'no', 'facebook', '2003539413011697', 0, '2018-07-23 17:51:21', 0, '1997-01-28', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15323934811307911151', NULL, 9, 2, '0', '0000-00-00 00:00:00'),
(528, 'Lenze_p', 'Lenze', 'Pharris', 'lenze_west@yahoo.com', '', 'uploads/userthumbimage/1532396239-profile_img.jpg', 'Female', 17, 0, 'no', 'facebook', '10210779985009629', 0, '2018-07-23 18:37:19', 0, '1994-12-20', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15323962391870104639', NULL, 12, 2, '0', '0000-00-00 00:00:00'),
(529, 'Edel67', 'Adi', 'Edelhaus', 'adi.edelhaus@gmail.com', '', 'uploads/userthumbimage/1532408348-profile_img.jpg', 'Male', 5, 0, 'no', 'facebook', '922085714665139', 0, '2018-07-23 21:59:08', 0, '1999-12-10', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1532408348485120136', NULL, 7, 2, '0', '0000-00-00 00:00:00'),
(530, 'Casarah', 'kristen', 'h', 'krisslovesoranges@gmail.com', '', 'uploads/userthumbimage/1532413423-profile_img.jpeg', 'Female', 32, 0, 'yes', 'facebook', '2058566564360620', 0, '2018-07-23 23:23:43', 0, '2000-02-01', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15324134231354309777', NULL, 29, 2, '0', '0000-00-00 00:00:00'),
(531, 'Coopertroop', 'j', 'coops', 'coopz.on.tour@gmail.com', 'djE5OTAxNm0=', 'uploads/userthumbimage/1532434340-profile_img.jpg', 'Male', 0, 10, 'no', 'normal', '', 0, '2018-07-24 05:12:20', 531, '1995-01-08', 0, 1, 1, '2018-07-24 05:12:44', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15324343641925719158', NULL, 8, 2, '0', '0000-00-00 00:00:00'),
(532, 'Saylor', 'saylor', 'rose', 'doctor.harry.potter.who0@gmail.com', 'R2l6bW9kb2cx', 'uploads/userthumbimage/1532436749-profile_img.jpg', 'Female', 0, 2, 'no', 'normal', '', 0, '2018-07-24 05:52:29', 532, '2000-02-10', 0, 1, 1, '2018-07-24 05:53:07', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1532436787629757375', NULL, 4, 2, '0', '0000-00-00 00:00:00'),
(533, 'Kenodinks', 'Kendrick', 'Green', 'kenodinks@gmail.com', '', 'uploads/userthumbimage/1532449427-profile_img.jpg', 'Male', 43, 0, 'yes', 'facebook', '677548879276508', 0, '2018-07-24 09:23:47', 0, '1987-05-21', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1532449427173188992', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(534, 'Afolsom', 'Addison', 'Folsom', 'a.folsom2@gmail.com', 'YWdnaWVsYW5kMTE=', 'uploads/userthumbimage/1532449459-profile_img.jpg', 'Male', 43, 0, 'no', 'normal', '', 0, '2018-07-24 09:24:19', 534, '1988-11-16', 0, 1, 1, '2018-07-24 09:24:40', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15324494802011858022', NULL, 9, 2, '0', '0000-00-00 00:00:00'),
(535, 'Damarieh', 'Damarieh', 'Underwood', 'Damarieh32@gmail.Com', 'ZGFtYXJpZWg2', 'uploads/userthumbimage/1532466464-profile_img.jpg', 'Male', 5, 0, 'no', 'normal', '', 0, '2018-07-24 14:07:44', 535, '1996-07-24', 0, 1, 1, '2018-07-24 14:08:22', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15324665021645636681', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(536, 'Katiekros', 'Katie', 'Krosnick', 'katiekrosnick@gmail.com', 'dWlqdHJj', 'uploads/userthumbimage/1532483310-profile_img.jpg', 'Female', 35, 0, 'no', 'normal', '', 0, '2018-07-24 18:48:30', 536, '2002-01-24', 0, 1, 1, '2018-07-24 18:49:05', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1532483345954394754', NULL, 7, 2, '0', '0000-00-00 00:00:00'),
(537, 'Tinytwotoes', 'Michael', 'unknown', 'meq1970@gmail.com', '', 'uploads/userthumbimage/1532506474-profile_img.jpg', 'Male', 5, 0, 'yes', 'facebook', '1827448003967578', 0, '2018-07-25 01:14:34', 0, '1965-12-24', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1532506474938938399', NULL, 3, 2, '0', '0000-00-00 00:00:00'),
(538, 'Duncan', 'duncan', 'faris', 'leftback06@gmail.com', 'YmlzZXh1YWw=', 'uploads/userthumbimage/1532549279-profile_img.jpg', '', 0, 4, 'no', 'normal', '', 0, '2018-07-25 13:07:59', 538, '2003-09-29', 0, 1, 1, '2018-07-25 13:08:26', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1532549306853671083', NULL, 9, 2, '0', '0000-00-00 00:00:00'),
(539, 'Jennadau', 'Jenna', 'Dau', 'jrakright@gmail.com', '', 'uploads/userthumbimage/1532562181-profile_img.jpg', 'Female', 15, 0, 'yes', 'facebook', '657083947988769', 0, '2018-07-25 16:43:01', 0, '2002-10-03', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15325621812132031307', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(540, 'Zionsmood', 'Josh', 'Swacina', 'swacina40@gmail.com', '', 'uploads/userthumbimage/1532586533-profile_img.jpg', 'Male', 25, 0, 'no', 'facebook', '498968033891220', 0, '2018-07-25 23:28:53', 0, '1980-06-26', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15325865332064623008', NULL, 10, 2, '0', '0000-00-00 00:00:00'),
(541, 'Klawaz', 'kaila', 'wass', 'kwass688@gmail.com', 'Z3JhY2VsaWx5', 'uploads/userthumbimage/1532588359-profile_img.jpg', 'Female', 9, 0, 'yes', 'normal', '', 0, '2018-07-25 23:59:19', 541, '1985-11-03', 0, 1, 1, '2018-07-26 00:00:04', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15325884041030312758', NULL, 20, 2, '0', '0000-00-00 00:00:00'),
(542, 'Karanbin', 'Karan', 'Binani', 'karanbinani@hotmail.com', '', 'uploads/userthumbimage/1532607077-profile_img.jpg', 'Male', 0, 41, 'no', 'facebook', '10157739078769552', 0, '2018-07-26 05:11:17', 0, '0000-00-00', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1532607077975394292', NULL, 7, 2, '0', '0000-00-00 00:00:00'),
(543, 'Lmintoid', 'Lucy', 'M', 'lmintoid@gmail.com', 'MU1vdXNlMjU=', 'uploads/userthumbimage/1532629256-profile_img.jpg', 'Female', 0, 10, 'no', 'normal', '', 0, '2018-07-26 11:20:56', 543, '2003-07-25', 0, 1, 1, '2018-07-26 11:22:08', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15326293281210038261', NULL, 7, 2, '0', '0000-00-00 00:00:00'),
(544, 'Captlarry', 'Codey', 'Slape', 'codeymusician@gmail.com', '', 'uploads/userthumbimage/1532635265-profile_img.jpeg', 'Male', 6, 0, 'no', 'facebook', '10160719485505230', 0, '2018-07-26 13:01:05', 0, '1990-07-08', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15326352651959517028', NULL, 8, 2, '0', '0000-00-00 00:00:00'),
(545, 'Tanishavdl', 'Tanisha', 'Vandenlangenberg', 'tjvandenl@gmail.com', '', 'uploads/userthumbimage/1532653743-profile_img.jpg', 'Female', 49, 0, 'yes', 'facebook', '148476056045293', 0, '2018-07-26 18:09:03', 0, '2002-06-27', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15326537432075359777', NULL, 20, 2, '0', '0000-00-00 00:00:00'),
(546, 'Bronzereturn', 'Joel', 'Allred', 'sydcourt213@gmail.com', '', 'uploads/userthumbimage/1532661608-profile_img.jpg', 'Transgendered Male', 25, 0, 'no', 'facebook', '660305984329681', 0, '2018-07-26 20:20:08', 0, '0000-00-00', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15326616081141202387', NULL, 7, 2, '0', '0000-00-00 00:00:00'),
(547, 'Carrdanielle', 'Danielle', 'Carr', 'carrdanielle22@gmail.com', 'RGFuaWVsbGU=', 'uploads/userthumbimage/1532716280-profile_img.jpeg', 'Female', 22, 0, 'no', 'normal', '', 0, '2018-07-27 11:31:20', 547, '0000-00-00', 0, 1, 1, '2018-07-27 11:32:42', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15327163621449290141', NULL, 9, 2, '0', '0000-00-00 00:00:00'),
(548, 'Illsa92', 'Sahil', 'Bhardwaj', 'sahil.bhardwaj9512@gmail.com', '', 'uploads/userthumbimage/1532724390-profile_img.jpg', 'Male', 0, 10, 'yes', 'facebook', '10216941062263048', 0, '2018-07-27 13:46:30', 0, '1992-09-12', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15327243901668888693', NULL, 28, 2, '0', '0000-00-00 00:00:00'),
(549, 'Alice2468', 'Alice', 'Bishop', 'aliceivybishop@gmail.com', '', 'uploads/userthumbimage/1532757988-profile_img.jpg', 'Female', 0, 10, 'yes', 'facebook', '133227864262021', 0, '2018-07-27 23:06:28', 0, '2005-12-27', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1532757988731999522', NULL, 20, 2, '0', '0000-00-00 00:00:00'),
(550, 'Vaishnavikau', 'Vaishnavi', 'Kaushik', 'taru123vaikau@gmail.com', 'MjUxMDIwMDQ=', 'uploads/userthumbimage/1532796558-profile_img.jpeg', 'Female', 0, 41, 'yes', 'normal', '', 0, '2018-07-28 09:49:18', 550, '1997-10-28', 0, 1, 1, '2018-07-28 09:50:09', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15327966091754351264', NULL, 2, 2, '0', '0000-00-00 00:00:00');
INSERT INTO `users_06-10-2020` (`user_id`, `user_name`, `first_name`, `last_name`, `email`, `password`, `profile_image`, `gender`, `state_id`, `country_id`, `notification_flag`, `login_type`, `fb_id`, `created_by`, `created_date`, `modified_by`, `dob`, `verify_code`, `status`, `admin_approve`, `modified_datetime`, `veficationcode_send_time`, `forgot_password_code`, `forgot_password_datetime`, `facebook_login_count`, `device_id`, `mobile_auth_token`, `device_type`, `unread_notification`, `is_deleted`, `deleted_by`, `deleted_date`) VALUES
(551, 'Lpcomedian', 'Lloyd', 'Pickthall', 'lloyd.pickthall@gmail.com', '', 'uploads/userthumbimage/1532819825-profile_img.jpeg', 'Male', 0, 10, 'no', 'facebook', '10156555605094289', 0, '2018-07-28 16:17:05', 0, '1992-03-14', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1532819825806505112', NULL, 7, 2, '0', '0000-00-00 00:00:00'),
(552, 'Bunchy0311', 'Liam', 'Holland-Bunch', 'liamholland-bunch@sky.com', 'c2NhbXBlcg==', 'uploads/userthumbimage/1532821308-profile_img.jpg', 'Male', 0, 10, 'yes', 'normal', '', 0, '2018-07-28 16:41:48', 552, '1999-11-02', 0, 1, 1, '2018-07-28 16:43:14', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1532821394599359200', NULL, 28, 2, '0', '0000-00-00 00:00:00'),
(553, 'Azrael', 'Malik', 'Washington', 'icecoldmalik@gmail.com', '', 'uploads/userthumbimage/1532827086-profile_img.jpg', 'Male', 43, 0, 'yes', 'facebook', '10155367893671290', 0, '2018-07-28 18:18:06', 0, '1972-09-12', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1532827086816054619', NULL, 7, 2, '0', '0000-00-00 00:00:00'),
(554, 'Qw3rty', '.QW3RTY', '.', 'hmnjn04@gmail.com', 'ZGViYXRlbm90aGF0ZQ==', 'uploads/userthumbimage/1532833747-profile_img.jpg', 'Male', 10, 0, 'yes', 'normal', '', 0, '2018-07-28 20:09:07', 554, '2004-06-10', 0, 1, 1, '2018-07-28 20:09:37', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1532833777228205710', NULL, 3, 2, '0', '0000-00-00 00:00:00'),
(555, 'Djake_g8tes', 'Jake', 'Gates', 'tecnoprovlogs@gmail.com', 'Y29vbEpSRzIzMzc=', 'uploads/userthumbimage/1532852716-profile_img.jpg', 'Male', 0, 10, 'no', 'normal', '', 0, '2018-07-29 01:25:16', 555, '2006-07-29', 0, 1, 1, '2018-07-29 01:25:49', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15328527491078575084', NULL, 14, 2, '0', '0000-00-00 00:00:00'),
(556, 'Jamesblens', 'James', 'Blens', 'jamesgizmoleigh@hotmail.com', '', 'uploads/userthumbimage/1532871753-profile_img.jpg', 'Male', 0, 10, 'no', 'facebook', '10160820657910387', 0, '2018-07-29 06:42:33', 0, '0000-00-00', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1532871753616357695', NULL, 6, 2, '0', '0000-00-00 00:00:00'),
(557, 'Dogz4lyfe', 'Vic', 'M', 'torriemoreau@gmail.com', 'dGhhaWxhbmQxMQ==', 'uploads/userthumbimage/1532884803-profile_img.jpg', 'Female', 0, 4, 'no', 'normal', '', 0, '2018-07-29 10:20:03', 557, '1996-10-11', 0, 1, 1, '2018-07-29 10:20:46', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1532884846208555179', NULL, 12, 2, '0', '0000-00-00 00:00:00'),
(558, 'Jimmothyb', 'Jimmy', 'Barrett', 'jamesbarrett50cal.jb@gmail.com', '', 'uploads/userthumbimage/1532901726-profile_img.jpg', 'Male', 0, 10, 'no', 'facebook', '2090113021012904', 0, '2018-07-29 15:02:06', 0, '0000-00-00', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15329017261117756070', NULL, 7, 2, '0', '0000-00-00 00:00:00'),
(559, 'Finnster', 'Finian', 'Hagan', 'finnsterhagan99@gmail.com', '', 'uploads/userthumbimage/1532983905-profile_img.jpg', 'Male', 0, 10, 'yes', 'facebook', '663436140681243', 0, '2018-07-30 13:51:45', 0, '1999-03-10', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15329839052022980387', NULL, 15, 2, '0', '0000-00-00 00:00:00'),
(560, 'Chubz93', 'AB', 'Briggs', '2017abiebriggsmail@gmail.com', '', 'uploads/userthumbimage/1532984997-profile_img.jpeg', 'Male', 0, 2, 'no', 'facebook', '390774221447260', 0, '2018-07-30 14:09:57', 0, '1993-01-24', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15329849971198611935', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(561, 'Shellycasoos', 'Cherrelle', 'Casoose', 'cherrellecasoose777@gmail.com', '', 'uploads/userthumbimage/1532987666-profile_img.jpeg', 'Female', 3, 0, 'yes', 'facebook', '2207411665954415', 0, '2018-07-30 14:54:26', 0, '1988-10-26', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15329876661441188169', NULL, 28, 2, '0', '0000-00-00 00:00:00'),
(562, 'Firefighter1', 'Shawn', 'Hipp', 'shawnhipp@gmail.com', 'U3RhdGlvbjE4', 'uploads/userthumbimage/1533006022-profile_img.jpeg', 'Male', 33, 0, 'no', 'normal', '', 0, '2018-07-30 20:00:22', 562, '1992-05-13', 0, 1, 1, '2018-07-30 20:00:42', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15330060421009204235', NULL, 7, 2, '0', '0000-00-00 00:00:00'),
(563, 'Edward', 'edward', 'dickerson', 'edwarddickerson23@gmail.com', 'aG90d2hlZWxzMQ==', 'uploads/userthumbimage/1533013033-profile_img.jpg', 'Male', 43, 0, 'yes', 'normal', '', 0, '2018-07-30 21:57:13', 563, '2000-03-10', 0, 1, 1, '2018-07-30 21:58:23', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15330131031815638722', NULL, 21, 2, '0', '0000-00-00 00:00:00'),
(564, 'Krazeek8', 'Katie', 'Chase', 'chatiekase10@yahoo.com', '', 'uploads/userthumbimage/1533027598-profile_img.jpeg', 'Female', 27, 0, 'yes', 'facebook', '10213623465674271', 0, '2018-07-31 01:59:58', 0, '1983-02-10', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15330275972064684856', NULL, 19, 2, '0', '0000-00-00 00:00:00'),
(565, 'Ajironmonger', 'Asa', 'Ironmonger', 'aironmonger@outlook.com', '', 'uploads/userthumbimage/1533044292-profile_img.jpg', 'Male', 0, 10, 'yes', 'facebook', '2181965125381254', 0, '2018-07-31 06:38:12', 0, '1998-08-07', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1533044292893678681', NULL, 18, 2, '0', '0000-00-00 00:00:00'),
(566, 'Demarcus28', 'Hayden', 'Demarco', 'firestormer888@gmail.com', 'RGVtYXJjdXM=', 'uploads/userthumbimage/1533057769-profile_img.jpeg', 'Male', 0, 10, 'no', 'normal', '', 0, '2018-07-31 10:22:49', 566, '1999-07-31', 0, 1, 1, '2018-07-31 10:23:40', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1533057820162795629', NULL, 9, 2, '0', '0000-00-00 00:00:00'),
(567, 'Kkaitlynkken', 'Kkaitlyn', 'Kkenny', 'kaitlynannekennny01@gmail.com', '', 'uploads/userthumbimage/1533062630-profile_img.jpg', 'Female', 0, 10, 'yes', 'facebook', '151330802433254', 0, '2018-07-31 11:43:50', 0, '0000-00-00', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15330626301571534907', NULL, 11, 2, '0', '0000-00-00 00:00:00'),
(568, 'Bennettcub', 'Austin', 'Bennett', 'mancub43@gmail.com', '', 'uploads/userthumbimage/1533077719-profile_img.jpg', 'Male', 32, 0, 'yes', 'facebook', '292032324687072', 0, '2018-07-31 15:55:19', 0, '2002-02-20', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15330777191319870111', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(569, 'John2121', 'john', 'smith', 'backwards.clone@gmail.com', 'am9obmpvaG4=', 'uploads/userthumbimage/1533081147-profile_img.jpg', 'Male', 14, 0, 'yes', 'normal', '', 0, '2018-07-31 16:52:27', 569, '1984-02-04', 0, 1, 1, '2018-07-31 16:55:17', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1533081317170505958', NULL, 4, 2, '0', '0000-00-00 00:00:00'),
(571, 'Chikuwa', 'Chikuwa', 'Kochousui', 'chikuwakochousui@gmail.com', 'YmVzdHBhc3M=', 'uploads/userthumbimage/1533306246-profile_img.jpg', 'Male', 0, 47, 'no', 'normal', '', 0, '2018-08-03 07:24:06', 571, '2000-08-21', 0, 1, 1, '2018-08-03 07:24:27', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1533306267172228480', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(572, 'Adityaverma', 'Aditya', 'Verma', 'adityadva3@gmail.com', 'YWRpcml3', 'uploads/userthumbimage/1533377716-profile_img.jpg', '', 0, 41, 'yes', 'normal', '', 0, '2018-08-04 03:15:16', 572, '2001-07-13', 0, 1, 1, '2018-08-04 03:15:44', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1533377744167177530', NULL, 27, 2, '0', '0000-00-00 00:00:00'),
(574, 'Chayfay101', 'Charlie', 'Fahey', 'charlie.fahey@hotmail.co.uk', 'SW50ZXJuZXQxMDE=', 'uploads/userthumbimage/1533428483-profile_img.jpg', 'Male', 0, 10, 'yes', 'normal', '', 0, '2018-08-04 17:21:23', 574, '1995-10-24', 0, 1, 1, '2018-08-04 17:22:39', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15334285591827706473', NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(576, 'Garrykizzi', 'Garrison', 'Kizzier', 'garrison@kizzier.us', '', 'uploads/userthumbimage/1533684689-profile_img.jpg', 'Male', 23, 0, 'no', 'facebook', '2216566818572032', 0, '2018-08-07 16:31:29', 0, '2000-09-13', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1533684689753683635', NULL, 9, 2, '0', '0000-00-00 00:00:00'),
(579, 'Sliccvicc', 'Blessed', 'One', 'victoryugorji02@gmail.com', 'SmVzdXM5ODQwNDM=', 'uploads/userthumbimage/1533913091-profile_img.jpg', 'Female', 43, 0, 'yes', 'normal', '', 0, '2018-08-10 07:58:11', 579, '2002-10-15', 0, 1, 1, '2018-08-10 07:59:10', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15339131501978144260', NULL, 25, 2, '0', '0000-00-00 00:00:00'),
(580, 'Tklive2186', 'Tacka', 'Gotcher', 'tackagotcher93@gmail.com', '', 'uploads/userthumbimage/1534070064-profile_img.jpg', 'Female', 43, 0, 'yes', 'facebook', '289964221583295', 0, '2018-08-12 03:34:25', 0, '1987-01-28', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1534070064409051335', NULL, 25, 2, '0', '0000-00-00 00:00:00'),
(582, 'Satan666', 'Satan', 'Just Satan', 'deadpoetbeardoil@gmail.com', 'RWV2aWUyMDE3', 'uploads/userthumbimage/1534335108-profile_img.jpeg', 'Male', 0, 2, 'no', 'normal', '', 0, '2018-08-15 05:11:49', 582, '1966-06-06', 0, 1, 1, '2018-08-15 05:12:14', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15343351342066555361', NULL, 8, 2, '0', '0000-00-00 00:00:00'),
(583, 'Olegataman', '????', '????????', 'atamanyukoleg0412@gmail.com', '', 'uploads/userthumbimage/1534581414-profile_img.jpeg', 'Male', 0, 95, 'yes', 'facebook', '135889014007569', 0, '2018-08-18 01:36:54', 0, '0000-00-00', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15345814141948988267', NULL, 13, 2, '0', '0000-00-00 00:00:00'),
(584, 'Luminousmind', 'Josephine', 'Struffert', 'j0j0struffert@gmail.com', 'ZWxsaWVyb3Nl', 'uploads/userthumbimage/1534629714-profile_img.jpeg', 'Female', 5, 0, 'yes', 'normal', '', 0, '2018-08-18 15:01:54', 584, '2005-02-19', 0, 1, 1, '2018-08-18 15:02:19', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '153462973998239793', NULL, 1, 2, '0', '0000-00-00 00:00:00'),
(585, 'Jamesk', 'James', 'Marcel', 'm.jamestoo@gmail.com', 'YWJjOTl4eXo=', 'uploads/userthumbimage/1534691251-profile_img.jpeg', 'Male', 21, 0, 'yes', 'normal', '', 0, '2018-08-19 08:07:31', 585, '1968-06-26', 0, 1, 1, '2018-08-19 08:09:18', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '153469135890912550', NULL, 17, 2, '0', '0000-00-00 00:00:00'),
(586, 'Facetoface', 'Mehmet', 'Ali', 'mehmoli97@gmail.com', 'ODlxZWI2OTc=', 'uploads/userthumbimage/1534754200-profile_img.jpg', 'Male', 0, 6, 'yes', 'normal', '', 0, '2018-08-20 01:36:41', 586, '1997-10-20', 0, 1, 1, '2018-08-20 01:37:22', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15347542421066154695', NULL, 16, 2, '0', '0000-00-00 00:00:00'),
(587, '__matt__', 'Matt', 'Qin', 'luckyblockking@gmail.com', 'TWF0dGlzY29vbDEw', 'uploads/userthumbimage/1534827351-profile_img.jpg', 'Male', 0, 4, 'no', 'normal', '', 0, '2018-08-20 21:55:51', 587, '2003-04-04', 0, 1, 1, '2018-08-20 21:56:41', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15348274011037947782', NULL, 8, 2, '0', '0000-00-00 00:00:00'),
(588, 'Namit17', 'Namit', 'Agrawal', 'nagrawal@hitaishin.com', 'MTIzNDU2Nw==', 'uploads/userthumbimage/1534944681-myImage.png', 'Male', 0, 41, 'yes', 'normal', '', 0, '2018-08-22 06:31:21', 588, '1993-04-17', 0, 1, 1, '2018-08-22 06:32:32', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15349447521120490756', NULL, 1, 2, '0', '0000-00-00 00:00:00'),
(589, 'Denyrechared', 'Deny', 'Rechard', 'deny.rechared@gmail.com', 'MTIzNDU2Nw==', 'uploads/userthumbimage/1535005951-myImage.png', 'Male', 0, 10, 'yes', 'normal', '', 0, '2018-08-22 23:32:31', 589, '1991-05-20', 0, 1, 1, '2018-10-17 00:27:48', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15397612681081513691', NULL, 4, 2, '0', '0000-00-00 00:00:00'),
(590, 'Ronakthakrar', 'Ronak', 'Thakrar', 'ronakthakrar2@gmail.com', 'cm9uYWs5NDI2NDMyNTM5', 'uploads/userthumbimage/1535028484-profile_img.jpeg', 'Male', 0, 41, 'yes', 'normal', '', 0, '2018-08-23 05:48:04', 590, '1997-11-28', 0, 1, 1, '2018-08-23 05:48:47', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1535028527972593242', NULL, 14, 2, '0', '0000-00-00 00:00:00'),
(594, 'Jigjiga2', 'Mohamed', 'Mohamoud', 'primo436@msn.com', '', 'uploads/userthumbimage/1535132866-profile_img.jpg', 'Male', 0, 10, 'yes', 'facebook', '10213108572497969', 0, '2018-08-24 10:47:46', 0, '1976-12-23', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15351328661610409501', NULL, 4, 2, '0', '0000-00-00 00:00:00'),
(597, 'Sethallen', 'Seth', 'Allen', 'sethlallen0823@gmail.com', 'U2lkbmV5MDgyMw==', 'uploads/userthumbimage/1535247959-profile_img.jpeg', 'Male', 18, 0, 'yes', 'normal', '', 0, '2018-08-25 18:46:00', 597, '2003-08-23', 0, 1, 1, '2018-08-25 18:51:09', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1535248269371918188', NULL, 19, 2, '0', '0000-00-00 00:00:00'),
(602, 'Yessenei', 'Moh', 'Alam', 'mohibulalam@gmail.com', '', 'uploads/userthumbimage/1536013745-profile_img.jpg', 'Male', 0, 10, 'no', 'facebook', '10161791187145643', 0, '2018-09-03 15:29:06', 0, '1983-08-10', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1536013745614584178', NULL, 10, 2, '0', '0000-00-00 00:00:00'),
(604, 'Beadlema', 'Anthony', 'Beadle', 'beadlema@msn.com', '', 'uploads/userthumbimage/1536060369-myImage.png', 'Male', 44, 0, 'no', 'facebook', '10155787593031586', 0, '2018-09-04 04:26:09', 0, '1983-08-10', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15360603691214936892', NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(606, 'Sundar123', 'sundar', 'hitaishin', 'patidarsundar1992@gmail.com', 'cXdlcnR5MTIz', 'uploads/userthumbimage/1536157929-profile_img.jpg', 'Male', 32, 0, 'yes', 'normal', '', 0, '2018-09-05 07:32:09', 606, '1992-01-03', 0, 1, 1, '2018-09-05 07:33:54', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1536158034465448453', NULL, 6, 2, '0', '0000-00-00 00:00:00'),
(607, 'Tusharhitais', 'Tushar', 'Hitaishin', 'tusharmittalcs2014@gmail.com', 'MTIzNDU2', 'uploads/userthumbimage/1536157931-profile_img.jpg', 'Male', 0, 41, 'yes', 'normal', '', 0, '2018-09-05 07:32:12', 607, '1993-04-10', 0, 1, 1, '2018-09-05 07:33:48', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15361580281673715589', NULL, 17, 2, '0', '0000-00-00 00:00:00'),
(612, 'Jaymaika', 'Shane', 'Thompson', 'jaminpot@hotmail.com', '', 'uploads/userthumbimage/1537394646-profile_img.jpg', 'Male', 5, 0, 'no', 'facebook', '10204172129541366', 0, '2018-09-19 15:04:06', 0, '0000-00-00', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1537394646926455854', NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(613, 'Nolyn_wyatt', 'Nolyn', 'Wyatt', 'nolynofficial@gmail.com', 'NDAwMzA3MCE=', 'uploads/userthumbimage/1537637984-myImage.png', 'Male', 14, 0, 'no', 'normal', '', 0, '2018-09-22 10:39:44', 613, '2000-05-24', 0, 1, 1, '2018-09-22 10:48:14', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15376384941874798129', NULL, 5, 2, '0', '0000-00-00 00:00:00'),
(615, 'Johanlammers', 'Johan Gustav', 'Lammers', 'johangustavlammers@gmail.com', '', 'uploads/userthumbimage/1537988612-profile_img.jpg', 'Male', 0, 6, 'yes', 'facebook', '391236504745188', 0, '2018-09-26 12:03:32', 0, '2001-08-29', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1537988612648425907', NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(616, 'Vance33', 'Vance', 'Hulu', 'vancehulu33@gmail.com', '', 'uploads/userthumbimage/1538104746-profile_img.jpg', 'Male', 0, 42, 'yes', 'facebook', '284906062344558', 0, '2018-09-27 20:19:06', 0, '2001-07-16', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1538104746334913527', NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(619, 'Ias12000', 'Alex', 'Liu', 'ias12000@126.com', 'bGl1NjcxMjI1', 'uploads/userthumbimage/1538206413-myImage.png', 'Male', 0, 89, 'yes', 'normal', '', 0, '2018-09-29 00:33:33', 619, '1967-03-12', 0, 1, 1, '2018-09-29 00:34:45', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15382064851719064766', NULL, 20, 2, '0', '0000-00-00 00:00:00'),
(621, 'Andreaput23', 'Andrea', 'Jasmine', 'andreajasmine23@gmail.com', '', 'uploads/userthumbimage/1538521283-profile_img.jpg', 'Female', 0, 42, 'no', 'facebook', '1777374095707152', 0, '2018-10-02 16:01:23', 0, '2003-08-22', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1538521283232635811', NULL, 5, 2, '0', '0000-00-00 00:00:00'),
(623, 'Beth_boo_196', 'Bethany', 'Goldsworthy', 'betsiboo@outlook.com', '', 'uploads/userthumbimage/1538650604-profile_img.jpg', 'Female', 0, 10, 'yes', 'facebook', '2252540451636318', 0, '2018-10-04 03:56:44', 0, '2002-01-22', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15386506042123034338', NULL, 5, 2, '0', '0000-00-00 00:00:00'),
(625, 'Nikki6788', 'nikki', 'eckman', 'nme6788@gmail.com', 'dGh1am9wcmE=', 'uploads/userthumbimage/1538673826-profile_img.jpg', 'Female', 15, 0, 'yes', 'normal', '', 0, '2018-10-04 10:23:46', 625, '1988-06-07', 0, 1, 1, '2018-10-04 10:24:04', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15386738441028766472', NULL, 19, 2, '0', '0000-00-00 00:00:00'),
(626, 'Jamessteve', 'Aditya', 'Pradana', 'laurentiusaditya1@gmail.com', 'bGF1cmVudGl1czIx', 'uploads/userthumbimage/1538811339-profile_img.jpg', 'Male', 0, 42, 'yes', 'normal', '', 0, '2018-10-06 00:35:39', 626, '2001-05-22', 0, 1, 1, '2018-10-06 00:36:16', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15388113761123536608', NULL, 6, 2, '0', '0000-00-00 00:00:00'),
(627, 'Tdashmun', 'Thomas', 'Dashmun', 'tdashmun@yahoo.com', 'dGRhc2htdW4=', 'uploads/avatar/Adult_male.png', '', 16, 0, 'yes', 'normal', NULL, 0, '2018-10-08 11:09:09', 627, '0000-00-00', 0, 1, 1, '2018-10-08 11:10:04', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, NULL, NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(629, 'Hireman', 'Matt', 'Bowie', 'matthewbowie@Hotmail.com', 'QmlnWWVhcicxNw==', 'uploads/userthumbimage/1539087740-myImage.png', 'Male', 21, 0, 'no', 'normal', '', 0, '2018-10-09 05:22:20', 629, '1972-02-01', 0, 1, 1, '2018-10-09 05:24:42', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15390878821464659756', NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(630, 'Nehahegde', 'Neha', 'Hegde', 'geetadevarajhegde@gmail.com', '', 'uploads/userthumbimage/1539112714-profile_img.jpeg', 'Female', 0, 41, 'yes', 'facebook', '1892591517514310', 0, '2018-10-09 12:18:34', 0, '2002-01-13', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15391127141616997684', NULL, 7, 2, '0', '0000-00-00 00:00:00'),
(632, 'Tarunbhuria', 'Tarun', 'bhuria', 'tarunbhuria1205@gmail.com', '', 'uploads/userthumbimage/1539183038-profile_img.jpg', 'Male', 0, 41, 'yes', 'facebook', '359308858140202', 0, '2018-10-10 07:50:38', 0, '2002-01-20', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15391830381867898112', NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(633, 'Hassanaboali', 'Hassan', 'Abo Ali', 'hassanaboa384@gmail.com', '', 'uploads/userthumbimage/1539287954-profile_img.jpg', 'Male', 0, 10, 'yes', 'facebook', '332975704127068', 0, '2018-10-11 12:59:14', 0, '1991-03-01', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1539287954853291112', NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(635, 'Meryvergara', 'Mery', 'Vergara', 'meryvergaraaibar@gmail.com', '', 'uploads/userthumbimage/1539386517-profile_img.jpg', 'Female', 0, 13, 'yes', 'facebook', '2245161792380085', 0, '2018-10-12 16:21:57', 0, '0000-00-00', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '153938651742768853', NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(638, 'Ameena', 'Ameena', 'Khan', 'ameenakhan12@hotmail.com', '', 'uploads/userthumbimage/1539710406-profile_img.jpg', 'Female', 0, 68, 'no', 'facebook', '2211525325751057', 0, '2018-10-16 10:20:06', 0, '1999-09-20', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15397104061525652490', NULL, 8, 2, '0', '0000-00-00 00:00:00'),
(641, 'Aamyra2000', 'aamyra', 'Malhotra', 'ayushaswain2000@gmail.com', 'YWFtdUAyMDAw', 'uploads/userthumbimage/1539931628-profile_img.jpg', 'Female', 0, 41, 'yes', 'normal', '', 0, '2018-10-18 23:47:08', 641, '2000-09-17', 0, 1, 1, '2018-10-18 23:47:41', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15399316611613876254', NULL, 20, 2, '0', '0000-00-00 00:00:00'),
(644, 'Miag1023', 'mia', 'ga', 'miag1023@gmail.com', 'QW1hem9uMTM2', 'uploads/userthumbimage/1540473065-profile_img.jpg', 'Female', 10, 0, 'no', 'normal', '', 0, '2018-10-25 06:11:05', 644, '1990-11-06', 0, 1, 1, '2018-10-25 06:13:33', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1540473213488552314', NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(645, 'Paradise4u22', 'Trent', 'Ray', 'tlehman1000@gmail.com', 'R29sZGVuNzUk', 'uploads/userthumbimage/1541427683-profile_img.jpg', 'Male', 43, 0, 'yes', 'normal', '', 0, '2018-11-05 06:21:24', 645, '1975-10-19', 0, 1, 1, '2018-11-05 06:22:06', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15414277261432254881', NULL, 12, 2, '0', '0000-00-00 00:00:00'),
(646, 'Alessa1', 'al', 'luz', 'alessarluccetti@gmail.com', 'YXNkZmdo', 'uploads/userthumbimage/1541673360-profile_img.jpeg', '', 0, 9, 'yes', 'normal', '', 0, '2018-11-08 02:36:01', 646, '0000-00-00', 0, 1, 1, '2018-11-11 10:11:41', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1541959901812354722', NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(648, 'Lorenzo', 'Lorenzo', 'Prudenzano', 'prudenzanolorenzo@gmail.com', 'TG8xODAzMDM=', 'uploads/userthumbimage/1542022509-profile_img.jpg', 'Male', 0, 7, 'yes', 'normal', '', 0, '2018-11-12 03:35:11', 648, '2003-03-18', 0, 1, 1, '2018-11-12 03:36:40', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1542022600813260924', NULL, 10, 2, '0', '0000-00-00 00:00:00'),
(651, 'Bellara', 'Isabella', 'A.', 'kittens5i3oa@gmail.com', '', 'uploads/userthumbimage/1542425871-profile_img.jpg', 'Female', 22, 0, 'yes', 'facebook', '579987382429762', 0, '2018-11-16 19:37:51', 0, '2001-12-18', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1542425871569534430', NULL, 16, 2, '0', '0000-00-00 00:00:00'),
(653, 'Narendra8816', 'narendra', 'rathore', 'narendrarathore3@gmail.com', '', 'uploads/userthumbimage/1543217584-profile_img.jpg', 'Male', 0, 7, 'yes', 'facebook', '1778588692191216', 0, '2018-11-25 23:33:04', 0, '2018-11-25', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15432175841474849639', NULL, 5, 2, '0', '0000-00-00 00:00:00'),
(654, 'Aceabdu2', 'Mahad', 'Abdu', 'aceabdu2@gmail.com', '', 'uploads/userthumbimage/1543657014-profile_img.jpg', 'Male', 23, 0, 'yes', 'facebook', '1909639932489063', 0, '2018-12-01 01:36:55', 0, '1992-07-28', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1543657014286477991', NULL, 17, 2, '0', '0000-00-00 00:00:00'),
(657, 'Galattee', 'Galattée', 'Gattée', 'alexandra.courbot@orange.fr', 'Q2tlbnphMDE=', 'uploads/userthumbimage/1544137198-profile_img.jpeg', 'Female', 0, 5, 'no', 'normal', '', 0, '2018-12-06 14:59:58', 657, '2001-11-30', 0, 1, 1, '2018-12-06 15:00:43', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15441372431171375560', NULL, 5, 2, '0', '0000-00-00 00:00:00'),
(658, 'Musicalgamr', 'Bradley', 'Davis', 'bradleytd122801@gmail.com', 'bml0dGVyMzc4Mg==', 'uploads/userthumbimage/1544296878-myImage.png', 'Male', 9, 0, 'yes', 'normal', '', 0, '2018-12-08 11:21:19', 658, '2001-12-28', 0, 1, 1, '2018-12-08 11:23:35', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15442970151646847256', NULL, 10, 2, '0', '0000-00-00 00:00:00'),
(659, 'Therightside', 'Alex', 'cooper', 'rubixcubenoah@Gmail.com', 'eW5nbWFsaWU=', 'uploads/userthumbimage/1544333077-myImage.png', 'Male', 17, 0, 'yes', 'normal', '', 0, '2018-12-08 21:24:37', 659, '1995-09-20', 0, 1, 1, '2018-12-08 21:39:02', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1544333942376125311', NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(661, 'Dingdong', 'Mary', 'Cooper', '1605466935@qq.com', 'ZGRkNjU0MzIx', 'uploads/userthumbimage/1544888774-profile_img.jpg', 'Female', 0, 38, 'yes', 'normal', '', 0, '2018-12-15 07:46:15', 661, '1995-10-13', 0, 1, 1, '2018-12-15 07:46:44', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15448888042049381663', NULL, 7, 2, '0', '0000-00-00 00:00:00'),
(662, 'Womanhuman', 'dilya', 'Rysbekova', 'dilyarysbek@gmail.com', 'a24wODA0OTQ=', 'uploads/userthumbimage/1545321499-profile_img.jpeg', 'Female', 0, 49, 'yes', 'normal', '', 0, '2018-12-20 07:58:19', 662, '0000-00-00', 0, 1, 1, '2018-12-20 08:06:52', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1545322012959631998', NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(665, 'Conservative', 'Hugo', 'Brännlund', 'Benparaden@gmail.com', 'SHVnb2Jvc3MwNA==', 'uploads/userthumbimage/1545972463-myImage.png', 'Male', 0, 87, 'yes', 'normal', '', 0, '2018-12-27 20:47:44', 665, '2004-03-04', 0, 1, 1, '2018-12-27 20:49:04', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '154597254437212846', NULL, 5, 2, '0', '0000-00-00 00:00:00'),
(670, 'Fem_lgbt6', 'luita', 'soygenial', 'giseladenisew@yahoo.com.ar', '', 'uploads/userthumbimage/1546319141-profile_img.jpg', 'Questioning', 0, 13, 'yes', 'facebook', '10213493619799656', 0, '2018-12-31 21:05:42', 0, '2000-06-05', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1546319141369927727', NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(676, 'Vishwa', 'Vishwa', 'Vishwa', 'cocvishwa@gmail.com', '', 'uploads/userthumbimage/1546609561-profile_img.jpg', 'Male', 0, 41, 'no', 'facebook', '762514010787586', 0, '2019-01-04 05:46:01', 676, '2003-09-17', 0, 1, 1, '2019-01-04 05:51:50', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15466095611763301984', NULL, 10, 2, '0', '0000-00-00 00:00:00'),
(680, 'johnsmith', 'John', 'Smith', 'cpvdmwypys_1543823215@tfbnw.net', NULL, 'uploads/userthumbimage/1548810594.jpg', 'Male', 28, 0, 'yes', 'facebook', '10150055822825680', 0, '2019-01-29 17:11:02', 0, '1991-01-17', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, NULL, NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(681, 'benstoke', 'Ben', 'Stoke', 'qa.hitaishin@mailinator.com', 'MTIzNDU2', 'uploads/userthumbimage/1548844811.jpg', 'Male', 14, 0, 'yes', 'facebook', '', 0, '2019-01-30 02:41:55', 681, '2019-01-23', 0, 1, 1, '2020-10-03 05:54:07', '0000-00-00 00:00:00', 765298, '2019-02-12 22:33:02', 1, NULL, '16017296471272036589', NULL, 27, 2, '0', '0000-00-00 00:00:00'),
(683, 'Vikashkumar', 'vikash', 'patel', 'vsk069@gmail.com', 'ODgyNzE4MzY5Mg==', 'uploads/userthumbimage/1549696926-profile_img.jpeg', 'Male', 0, 41, 'yes', 'normal', '', 0, '2019-02-08 23:22:07', 683, '1996-02-17', 0, 1, 1, '2019-02-08 23:28:42', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1549697322992658730', NULL, 13, 2, '0', '0000-00-00 00:00:00'),
(684, 'Harrypatel', 'Harry', 'Patel', 'hp.khushi@gmail.com', 'MTIzNDU2', 'uploads/userthumbimage/1550036993-profile_img.jpg', 'Male', 0, 41, 'yes', 'normal', '', 0, '2019-02-12 21:49:53', 684, '1989-09-17', 0, 1, 1, '2020-10-04 22:40:21', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '16018764212104645152', NULL, 14, 2, '0', '0000-00-00 00:00:00'),
(688, 'Jonlin', 'Jonathan', 'Linblom', 'jonathanlindblom@rocketmail.com', '', 'uploads/userthumbimage/1550335169-myImage.png', 'Male', 0, 87, 'yes', 'facebook', '532271177267823', 0, '2019-02-16 08:39:31', 0, '2003-12-11', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '15503351691379635938', NULL, 2, 2, '0', '0000-00-00 00:00:00'),
(689, 'Batman2018', 'Ricky', 'Campeador', 'rc18cvhs@gmail.com', 'YWxwaGE1NDA=', 'uploads/userthumbimage/1550543750-profile_img.jpg', 'Male', 5, 0, 'yes', 'normal', '', 0, '2019-02-18 18:35:52', 689, '1985-04-20', 0, 1, 1, '2019-02-18 18:38:31', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15505439111435685509', NULL, 10, 2, '0', '0000-00-00 00:00:00'),
(694, 'Quansah4044', 'Alex', 'Quansah', 'quansah4044@gmail.com', 'NDA0NGFiY2Rl', 'uploads/userthumbimage/1550756054-profile_img.jpeg', 'Male', 0, 36, 'yes', 'normal', '', 0, '2019-02-21 05:34:15', 694, '1996-12-26', 0, 1, 1, '2019-02-21 05:37:21', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15507562411178907077', NULL, 3, 2, '0', '0000-00-00 00:00:00'),
(697, 'Abhrid', 'Ab', 'Hrid', 'abhrid24@gmail.com', '', 'uploads/userthumbimage/1551857920-myImage.png', 'Male', 0, 41, 'yes', 'facebook', '680018125733675', 0, '2019-03-05 23:38:41', 0, '1999-11-09', 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1, NULL, '1551857920718573372', NULL, 3, 2, '0', '0000-00-00 00:00:00'),
(698, 'Lmliberty', 'Zachary', 'Moss', 'zachmoss147@gmail.com', 'ZXZlcmxhc3Qz', 'uploads/userthumbimage/1551925241-profile_img.jpg', 'Male', 25, 0, 'yes', 'normal', '', 0, '2019-03-06 18:20:47', 698, '1996-01-04', 0, 1, 1, '2019-03-06 18:21:49', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15519253091731877446', NULL, 3, 2, '0', '0000-00-00 00:00:00'),
(699, 'Profejorge', 'jorge', 'hoyos', 'jorgeivanhoyos@gmail.com', 'MTIzNGFiY2Q=', 'uploads/userthumbimage/1552016716-profile_img.jpg', 'Male', 0, 25, 'yes', 'normal', '', 0, '2019-03-07 19:45:16', 699, '1973-01-21', 0, 1, 1, '2019-03-07 19:47:39', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '15520168591730619859', NULL, 8, 2, '0', '0000-00-00 00:00:00'),
(700, 'Jvnrawat', 'jee', 'Singh', 'jee@yopmail.com', 'MTIzNDU2', 'uploads/userthumbimage/1600253708-myImage.png', 'Male', 0, 41, 'yes', 'normal', '', 0, '2020-09-16 03:55:08', 700, '2020-09-16', 0, 1, 1, '2020-10-04 22:23:26', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '16018754061138161884', NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(701, 'Brawls', 'j', 'j@yopmail.com', 'j@yopmail.com', 'MTIzNDU2', 'uploads/userthumbimage/1600259788-myImage.png', 'Male', 0, 41, 'yes', 'normal', '', 0, '2020-09-16 05:36:28', 701, '2020-09-16', 0, 1, 1, '2020-09-16 05:37:36', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '16002598561493437527', NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(702, 'Jsrrawat', 'j', 'Singh', 'jsr@yopmail.com', 'MTIzNDU2', 'uploads/userthumbimage/1600261020-myImage.png', 'Male', 0, 41, 'yes', 'normal', '', 0, '2020-09-16 05:57:00', 702, '2020-09-16', 0, 1, 1, '2020-09-16 05:57:44', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '1600261064212001458', NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(703, 'Kjlhgjgj', 'k', 'k', 'hh@yopmail.com', 'MTIzNDU2', 'uploads/userthumbimage/1600320071-myImage.png', 'Male', 0, 41, 'yes', 'normal', '', 0, '2020-09-16 22:21:11', 703, '2019-09-17', 0, 1, 1, '2020-09-16 22:21:36', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '16003200962143035062', NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(704, 'Nnnnnnn', 'k', 'k', 'nn@yopmail.com', 'MTIzNDU2', 'uploads/userthumbimage/1600320952-myImage.png', 'Male', 0, 41, 'yes', 'normal', '', 0, '2020-09-16 22:35:52', 704, '2020-09-17', 0, 1, 1, '2020-09-16 22:36:26', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '16003209861223837115', NULL, 6, 2, '0', '0000-00-00 00:00:00'),
(705, 'Newuser', 'new', 'user', 'jj@yopmail.com', 'MTIzNDU2', 'uploads/userthumbimage/1601720397-myImage.png', 'Male', 0, 13, 'yes', 'normal', '', 0, '2020-10-03 03:19:57', 705, '2020-10-03', 0, 1, 1, '2020-10-03 03:28:37', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '16017209171684821288', NULL, 0, 2, '0', '0000-00-00 00:00:00'),
(706, 'Tmittal5', 'Tushar5', 'mittal', 'tmittal5@mailinator.com', 'MTIzNDU2', 'uploads/userthumbimage/1601882620-profile_img.jpg', '', 0, 41, 'yes', 'normal', '', 0, '2020-10-05 00:23:40', 706, '2020-10-05', 0, 1, 1, '2020-10-05 00:24:28', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, '16018826682103304048', NULL, 0, 2, '0', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `views`
--

CREATE TABLE `views` (
  `id` int(11) NOT NULL,
  `media_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `views_deleted`
--

CREATE TABLE `views_deleted` (
  `id` int(11) NOT NULL,
  `media_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `action_log`
--
ALTER TABLE `action_log`
  ADD PRIMARY KEY (`action_id`);

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `is_active` (`is_active`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`),
  ADD KEY `email` (`email`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `app_first_time_visitor`
--
ALTER TABLE `app_first_time_visitor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `broadcast_emails`
--
ALTER TABLE `broadcast_emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `broadcast_notification`
--
ALTER TABLE `broadcast_notification`
  ADD PRIMARY KEY (`notification_id`);

--
-- Indexes for table `broadcast_notifications_old`
--
ALTER TABLE `broadcast_notifications_old`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_active_users`
--
ALTER TABLE `chat_active_users`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `first_active_user` (`first_active_user`),
  ADD KEY `second_active_user` (`second_active_user`),
  ADD KEY `is_active` (`is_active`),
  ADD KEY `deleted` (`deleted`);

--
-- Indexes for table `chat_groups`
--
ALTER TABLE `chat_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `is_active` (`is_active`),
  ADD KEY `deleted` (`deleted`);

--
-- Indexes for table `chat_message`
--
ALTER TABLE `chat_message`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `is_active` (`is_active`),
  ADD KEY `deleted` (`deleted`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `parent_message_id` (`parent_message_id`);

--
-- Indexes for table `chat_message_recipients`
--
ALTER TABLE `chat_message_recipients`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `recipient_id` (`recipient_id`),
  ADD KEY `recipient_group_id` (`recipient_group_id`),
  ADD KEY `message_id` (`message_id`),
  ADD KEY `is_active` (`is_active`),
  ADD KEY `deleted` (`deleted`),
  ADD KEY `is_read` (`is_read`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `chat_room`
--
ALTER TABLE `chat_room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_user_group_assoc`
--
ALTER TABLE `chat_user_group_assoc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `is_active` (`is_active`),
  ADD KEY `deleted` (`deleted`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment_dislike`
--
ALTER TABLE `comment_dislike`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment_like`
--
ALTER TABLE `comment_like`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment_report`
--
ALTER TABLE `comment_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `continue_watch_media`
--
ALTER TABLE `continue_watch_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `csv_uploads`
--
ALTER TABLE `csv_uploads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dislikes`
--
ALTER TABLE `dislikes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `state_id` (`state_id`);

--
-- Indexes for table `email_notification`
--
ALTER TABLE `email_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `first_time_visitor`
--
ALTER TABLE `first_time_visitor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `follow_users`
--
ALTER TABLE `follow_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `friends`
--
ALTER TABLE `friends`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `friends_logs`
--
ALTER TABLE `friends_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Title` (`title`);

--
-- Indexes for table `media_block`
--
ALTER TABLE `media_block`
  ADD PRIMARY KEY (`block_id`);

--
-- Indexes for table `media_report`
--
ALTER TABLE `media_report`
  ADD PRIMARY KEY (`report_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sectionid` (`section_id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `school`
--
ALTER TABLE `school`
  ADD PRIMARY KEY (`id`),
  ADD KEY `district_id` (`district_id`),
  ADD KEY `state_id` (`state_id`);

--
-- Indexes for table `school_grade`
--
ALTER TABLE `school_grade`
  ADD PRIMARY KEY (`id`),
  ADD KEY `district_id` (`district_id`),
  ADD KEY `state_id` (`school_id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `district_id_2` (`district_id`);

--
-- Indexes for table `school_introductory_video`
--
ALTER TABLE `school_introductory_video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_information`
--
ALTER TABLE `site_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_grade`
--
ALTER TABLE `student_grade`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `teacher_grade_id` (`teacher_grade_id`),
  ADD KEY `is_approve` (`is_approve`);

--
-- Indexes for table `teacher_grade`
--
ALTER TABLE `teacher_grade`
  ADD PRIMARY KEY (`id`),
  ADD KEY `district_id` (`district_id`),
  ADD KEY `state_id` (`school_id`),
  ADD KEY `teacher_id` (`teacher_id`),
  ADD KEY `teacher_id_2` (`teacher_id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `district_id_2` (`district_id`),
  ADD KEY `grade` (`grade`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_type` (`user_type`),
  ADD KEY `district_id` (`district_id`),
  ADD KEY `status` (`status`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `state_id` (`state_id`),
  ADD KEY `state_id_2` (`state_id`),
  ADD KEY `is_approve` (`is_approve`),
  ADD KEY `state_id_3` (`state_id`),
  ADD KEY `user_type_2` (`user_type`),
  ADD KEY `is_deleted` (`is_deleted`);

--
-- Indexes for table `users_06-10-2020`
--
ALTER TABLE `users_06-10-2020`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `views`
--
ALTER TABLE `views`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `action_log`
--
ALTER TABLE `action_log`
  MODIFY `action_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `app_first_time_visitor`
--
ALTER TABLE `app_first_time_visitor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `broadcast_emails`
--
ALTER TABLE `broadcast_emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `broadcast_notification`
--
ALTER TABLE `broadcast_notification`
  MODIFY `notification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `broadcast_notifications_old`
--
ALTER TABLE `broadcast_notifications_old`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat_active_users`
--
ALTER TABLE `chat_active_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=631;

--
-- AUTO_INCREMENT for table `chat_groups`
--
ALTER TABLE `chat_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `chat_message`
--
ALTER TABLE `chat_message`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `chat_message_recipients`
--
ALTER TABLE `chat_message_recipients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `chat_room`
--
ALTER TABLE `chat_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat_user_group_assoc`
--
ALTER TABLE `chat_user_group_assoc`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=213;

--
-- AUTO_INCREMENT for table `comment_dislike`
--
ALTER TABLE `comment_dislike`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comment_like`
--
ALTER TABLE `comment_like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=185;

--
-- AUTO_INCREMENT for table `comment_report`
--
ALTER TABLE `comment_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `continue_watch_media`
--
ALTER TABLE `continue_watch_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `csv_uploads`
--
ALTER TABLE `csv_uploads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `dislikes`
--
ALTER TABLE `dislikes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `email_notification`
--
ALTER TABLE `email_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=264;

--
-- AUTO_INCREMENT for table `first_time_visitor`
--
ALTER TABLE `first_time_visitor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `follow_users`
--
ALTER TABLE `follow_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `friends`
--
ALTER TABLE `friends`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `friends_logs`
--
ALTER TABLE `friends_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `media_block`
--
ALTER TABLE `media_block`
  MODIFY `block_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media_report`
--
ALTER TABLE `media_report`
  MODIFY `report_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=524;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `role_permissions`
--
ALTER TABLE `role_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `school`
--
ALTER TABLE `school`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `school_grade`
--
ALTER TABLE `school_grade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=727;

--
-- AUTO_INCREMENT for table `school_introductory_video`
--
ALTER TABLE `school_introductory_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `site_information`
--
ALTER TABLE `site_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `student_grade`
--
ALTER TABLE `student_grade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;

--
-- AUTO_INCREMENT for table `teacher_grade`
--
ALTER TABLE `teacher_grade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=259;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;

--
-- AUTO_INCREMENT for table `users_06-10-2020`
--
ALTER TABLE `users_06-10-2020`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=707;

--
-- AUTO_INCREMENT for table `views`
--
ALTER TABLE `views`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

