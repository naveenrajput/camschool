$(window).scroll(function() {    
	var scroll = $(window).scrollTop();
	if (scroll >= 620) {
		$(".navbar").addClass("fixnav");
	} else {
		$(".navbar").removeClass("fixnav");
	}
});
	

$('#scrolltotop').on("click",function(){
      $(window).scrollTop(0);
});
		
$(".search .fa").click(function(){
	$("body").addClass("search-toggle");
	$(".overlay-search").fadeIn();
});		
		
$(function() {
  $(".search .fa").on("click", function(e) {
    $("body").addClass("search-toggle");
	$(".overlay-search").fadeIn();
    e.stopPropagation()
  });
  $(document).on("click", function(e) {
    if ($(e.target).is(".navbar-default, .navbar-default .form-control") === false) {
       $("body").removeClass("search-toggle");
	$(".overlay-search").fadeOut();
    }
  });
});
		

// custom radio js

  $(document).ready(function(){
    $("#custom-radio-buttons .radio-wrapper input[type='radio'] + label").click(function(){
      $("#custom-radio-buttons .radio-wrapper input[type='radio'] + label span.inner").removeClass("bounceIn");
      $(this).find("span.inner").addClass("bounceIn");
    });
  }); 
		
