function change_status(field,id,table)
{

    if(id) {

        $("#status_"+id).html('Wait...');

        $.ajax({

            type:'POST',

            data:{  

                id:id,

                table_name:table,

                field:field 

            },

            url: base_url+"admin/ajax/change_status/",

            success:function(data)

            {
                
                var response = JSON.parse(data);

                if(response.msg=="success") {

                     if(response.status == 'Inactive') {

                        $("#status_"+id).html(response.status);

                        $("#status_"+id).removeClass('bg-green');

                        $("#status_"+id).addClass('bg-red');

                    }

                    else {

                        $("#status_"+id).html(response.status);

                        $("#status_"+id).removeClass('bg-red');

                        $("#status_"+id).addClass('bg-green');

                        

                    }

                }
                else if(response.msg=="one_record")
                {
                    alert("Only one record is remaining.You cannot change status.");
                    $("#status_"+id).html('Active');
                }
                else {

                    alert("Check your internet connection and try again.");

                }

            }

        });

    }

}

function change_user_status(field,id,table)
{

    if(id) {

        $("#status_"+id).html('Wait...');

        $.ajax({

            type:'POST',

            data:{  

                id:id,

                table_name:table,

                field:field 

            },

            url: base_url+"admin/ajax/change_user_status/",

            success:function(data)

            {
                
                var response = JSON.parse(data);

                if(response.msg=="success") {

                     if(response.status == 'Inactive') {

                        $("#status_"+id).html(response.status);
                        $("#status_"+id).removeClass('bg-yellow');
                        $("#status_"+id).removeClass('bg-green');

                        $("#status_"+id).addClass('bg-red');

                    }
                    
                    else {

                        $("#status_"+id).html(response.status);

                        $("#status_"+id).removeClass('bg-red');

                        $("#status_"+id).addClass('bg-green');

                        

                    }

                }
            }

        });

    }

}
function change_profile_status(field,id,table)
{

    if(id) {
        var html_text=$("#pstatus_"+id).html();
        if(html_text!='Not Approved'){
            return false;
        }
        $("#pstatus_"+id).html('Wait...');

        $.ajax({

            type:'POST',

            data:{  

                id:id,

                table_name:table,

                field:field 

            },

            url: base_url+"admin/ajax/change_profile_status/",

            success:function(data)

            {

                
                var response = JSON.parse(data);

                if(response.msg=="success") {

                     if(response.status == 'Approved') {

                        $("#pstatus_"+id).html(response.status);

                        $("#pstatus_"+id).removeClass('bg-red');

                        $("#pstatus_"+id).addClass('bg-yellow');

                    }
                    else {

                        $("#pstatus_"+id).html(response.status);

                        $("#pstatus_"+id).removeClass('bg-red');

                        $("#pstatus_"+id).addClass('bg-green');

                        

                    }

                }
                

            }

        });

    }

}



function delete_record(id,table,field) {
    if(id && table && field) {
        var r = confirm('Are you sure you want to delete record ?');
        if (r == true) {
            $.ajax({
                type:'POST',
                url: "admin/Ajax/delete_records",
                data: {id:id,field:field,table:table},
                
                success:function(data)
                {
                    data = JSON.parse(data);
                    if(data.status==true) 
                    {
                        success_msg = data.msg ;
                        success = '<div class="alert alert-block alert-success fade in"><button data-dismiss="alert" class="close" type="button">×</button>'+success_msg+'</div>';
                        //$('#notification_msg').html(success).fadeIn(250).fadeOut(10000);
                        location.reload();
                    } else {
                        error_msg =  data.msg ;
                        error = '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close" type="button">×</button>'+error_msg+'</div>';
                        location.reload();
                        $('#notification_msg').html(error).fadeIn(250).fadeOut(10000);
                    }
                }       
            });
        } 
    }
}





function soft_delete_record(id,table,field) {
    if(id && table && field) {
        var r = confirm('Are you sure you want to delete record ?');
        if (r == true) {
            $.ajax({
                type:'POST',
                url: "admin/Ajax/soft_delete_record",
                data: {id:id,field:field,table:table},
                
                success:function(data)
                {
                    data = JSON.parse(data);
                    if(data.status==true) 
                    {
                        success_msg = data.msg ;
                        success = '<div class="alert alert-block alert-success fade in"><button data-dismiss="alert" class="close" type="button">×</button>'+success_msg+'</div>';
                        $('#notification_msg').html(success).fadeIn(250).fadeOut(10000);
                        location.reload();
                    } else {
                        error_msg =  data.msg ;
                        error = '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close" type="button">×</button>'+error_msg+'</div>';
                        $('#notification_msg').html(error).fadeIn(250).fadeOut(10000);
                    }
                }       
            });
        } 
    }
} 

function sort_delete_record(id,table,field) {
    if(id && table && field) {
        var r = confirm('Are you sure you want to delete record ?');
        if (r == true) {
            $.ajax({
                type:'POST',
                url: "admin/Ajax/sort_delete_record",
                data: {id:id,field:field,table:table},
                
                success:function(data)
                {
                    data = JSON.parse(data);
                    if(data.status==true) 
                    {
                        success_msg = data.msg ;
                        success = '<div class="alert alert-block alert-success fade in"><button data-dismiss="alert" class="close" type="button">×</button>'+success_msg+'</div>';
                        //$('#notification_msg').html(success).fadeIn(250).fadeOut(10000);
                        location.reload();
                    } else {
                        error_msg =  data.msg ;
                        error = '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close" type="button">×</button>'+error_msg+'</div>';
                        $('#notification_msg').html(error).fadeIn(250).fadeOut(10000);
                    }
                }       
            });
        } 
    }
} 


 window.addDashes = function addDashes(f) {
    if(f.value.length>1){
        var r = /(\D+)/g,
            npa = '',
            nxx = '',
            last4 = '';
        f.value = f.value.replace(r, '');
        npa = f.value.substr(0, 3);
        nxx = f.value.substr(3, 3);
        last4 = f.value.substr(6, 4);
        f.value = npa + '-' + nxx + '-' + last4;
    }
}
////////////////////////password validation///////////////////////////
   window.Parsley.addValidator('number', {
        requirementType: 'number',
        validateString: function(value, requirement) {
            var numbers = value.match(/[0-9]/g) || [];
            return numbers.length >= requirement;
        },
        messages: {
            en: 'Password must contain at least (%s) number'
        }
    });

    window.Parsley.addValidator('alphabet', {
        requirementType: 'alphabet',
        validateString: function(value, requirement) {
            var upercase = value.match(/[a-zA-Z]/g) || [];
            return upercase.length >= requirement;
        },
        messages: {
            en: 'Password must contain at least (%s) alphabet'
        }
    });
    window.Parsley.addValidator('special', {
      requirementType: 'number',
      validateString: function(value, requirement) {
        var specials = value.match(/[^a-zA-Z0-9]/g) || [];
        return specials.length >= requirement;
      },
      messages: {
        en: 'Password must contain at least (%s) special characters'
      }
    });
    window.Parsley.addValidator('uppercase', {
      requirementType: 'number',
      validateString: function(value, requirement) {
        var uppercases = value.match(/[A-Z]/g) || [];
        return uppercases.length >= requirement;
      },
      messages: {
        en: 'Password must contain at least (%s) uppercase letter'
      }
    });

    //has lowercase
    window.Parsley.addValidator('lowercase', {
      requirementType: 'number',
      validateString: function(value, requirement) {
        var lowecases = value.match(/[a-z]/g) || [];
        return lowecases.length >= requirement;
      },
      messages: {
        en: 'Password must contain at least (%s) lowercase letter'
      }
    });