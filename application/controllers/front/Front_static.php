<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Front_static extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('admin/Common_model'));
		$this->load->helper('common_helper');
		
	}

	public function about_us()
	{
		$data['title']= 'About Us' . " | ".SITE_TITLE;
	    $data['page_title'] = "About Us";
	    $data['page_detail']=$this->Common_model->getRecords('pages','*',array('page_id'=> '3'),'',true);
	    $this->load->view('front/include/header',$data);
	    $this->load->view('front/about_us');
	    $this->load->view('front/include/footer');
	}

	public function privacy_policy()
	{
		$data['title']= 'Privacy Policy' . " | ".SITE_TITLE;
	    $data['page_title'] = "Privacy Policy";
	    $data['page_detail']=$this->Common_model->getRecords('pages','*',array('page_id'=> '2'),'',true);
	    $this->load->view('front/include/header',$data);
	    $this->load->view('front/privacy-policy');
	    $this->load->view('front/include/footer');
	}

	public function terms_and_conditions()
	{
		$data['title']= 'Terms & Condition' . " | ".SITE_TITLE;
	    $data['page_title'] = "Terms & Condition";
	    $data['page_detail']=$this->Common_model->getRecords('pages','*',array('page_id'=> 1),'',true);
	    $this->load->view('front/include/header',$data);
	    $this->load->view('front/terms-and-conditions');
	    $this->load->view('front/include/footer');
	}
	public function cancellation_refund_policy()
	{
		$data['title']= 'Cancellation & Refund Policy' . " | ".SITE_TITLE;
	    $data['page_title'] = "Cancellation & Refund Policy";
	    $data['page_detail']=$this->Common_model->getRecords('pages','*',array('page_id'=> '4'),'',true);
	    $this->load->view('front/include/header',$data);
	    $this->load->view('front/cancellation_refund_policy');
	    $this->load->view('front/include/footer');
	}

}