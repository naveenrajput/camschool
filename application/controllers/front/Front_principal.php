<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Front_principal extends CI_Controller
{
    function __construct()  
    {
        parent::__construct();
        $this->load->model(array('Common_model','Auth_model','District_model','School_model')); 
        $this->load->helper(array('email','common_helper'));
       $this->load->library(array('Ajax_pagination','PHPExcel'));
      
    }

/*======================Edit Profile================ */
    public function profile() 
    {
        check_web_user_status(1);
        //echo '<pre>';print_r($_SESSION);exit;
        $data['title']= 'Profile' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Profile'." | ".SITE_TITLE;
        $data['meta_desc']= 'Profile'." | ".SITE_TITLE;
        $data['page_title'] = "Profile";
     
        
        $this->load->view('front/include/header',$data);
        $this->load->view('front/principal/profile');
        $this->load->view('front/include/footer');
    }


    /*-----------Principal Request--------------------*/

    public function principal_request(){
      
        check_web_user_status();
        $data['title']= 'Principal' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Principal'." | ".SITE_TITLE;
        $data['meta_desc']= 'Principal'." | ".SITE_TITLE;
        $data['page_title'] = "Principal";   

        $school_id = $this->session->userdata('school_id');
        $offset = 0;
        $per_page = APP_PAGE_LIMIT;
        $total_records  = $this->School_model->getTeacherListWeb($school_id,1,0,0);

        $b_url = base_url('teacher-list-request-data');
        $selector = '#ajax_data';
        $function = "filter_records";

        $config = configFrontAjaxPagination($total_records,$b_url,$selector,$per_page,$function,2);
        $this->ajax_pagination->initialize($config);

        $data['teacher_list'] = $this->School_model->getTeacherListWeb($school_id,1,$per_page,$offset);
        $data['offset'] = $offset;


        $this->load->view('front/include/header',$data);
        $this->load->view('front/principal/principal_request');
        $this->load->view('front/include/footer');
    }

    function teacher_list_request_data()
    {
        $page = $this->uri->segment(2);
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }
        $school_id = $this->session->userdata('school_id');
        $per_page = APP_PAGE_LIMIT;
        $total_records  = $this->School_model->getTeacherListWeb($school_id,1,0,0);
        $b_url = base_url('teacher-list-request-data');
        $selector = '#ajax_data';
        $function = "filter_records";

        //pagination configuration
        $config = configFrontAjaxPagination($total_records,$b_url,$selector,$per_page,$function);
        $this->ajax_pagination->initialize($config);
        //get the posts data
        $data['offset'] = $offset;
        $data['teacher_list'] = $this->School_model->getTeacherListWeb($school_id,1,$per_page,$offset);
        if(!empty($data['teacher_list'])) {
            $this->load->view('front/principal/teacher_request_list_data', $data);
        }
    }


    /*-----------Approved principal Request--------------------*/

    public function approved_teacher_request(){

        check_web_user_status();
        $data['title']= 'Principal' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Principal'." | ".SITE_TITLE;
        $data['meta_desc']= 'Principal'." | ".SITE_TITLE;
        $data['page_title'] = "Principal";

        $school_id = $this->session->userdata('school_id');
        $offset = 0;
        $per_page = APP_PAGE_LIMIT;
        $total_records  = $this->School_model->getTeacherListWeb($school_id,2,0,0);
        $b_url = base_url('teacher-approved-list-data');
        $selector = '#ajax_data';
        $function = "filter_records";

        $config = configFrontAjaxPagination($total_records,$b_url,$selector,$per_page,$function,2);
        $this->ajax_pagination->initialize($config);

        $data['teacher_list'] = $this->School_model->getTeacherListWeb($school_id,2,$per_page,$offset);
        $data['offset'] = $offset;
       unset($_SESSION['broadcast_email']);
        $this->load->view('front/include/header',$data);
        $this->load->view('front/principal/teacher_approved');
        $this->load->view('front/include/footer');
    }

    function approved_teacher_request_data()
    {
        $page = $this->uri->segment(2);
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }
        $school_id = $this->session->userdata('school_id');
        $per_page = APP_PAGE_LIMIT;
        $total_records  = $this->School_model->getTeacherListWeb($school_id,2,0,0);
        $b_url = base_url('teacher-approved-list-data');
        $selector = '#ajax_data';
        $function = "filter_records";

        //pagination configuration
        $config = configFrontAjaxPagination($total_records,$b_url,$selector,$per_page,$function);
        $this->ajax_pagination->initialize($config);
        //get the posts data
        $data['offset'] = $offset;
        $data['teacher_list'] = $this->School_model->getTeacherListWeb($school_id,2,$per_page,$offset);
        if(!empty($data['teacher_list'])) {
            $this->load->view('front/principal/teacher_list_data', $data);
        }
    }

    /*------------- Approved school send message -------------*/

    public function approved_teacher_sendmsg(){        
        check_web_user_status();
        if(!$this->input->post('teacher_data')){
            display_output('0','Select the recipients');
        }


        $this->session->unset_userdata('broadcast_email'); 
        $broadcast_email = $this->input->post('teacher_data');
        $get_broadcast_email = $this->School_model->get_broadcast_email($broadcast_email);
        
        $data['title']= 'Principal' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Principal'." | ".SITE_TITLE;
        $data['meta_desc']= 'Principal'." | ".SITE_TITLE;
        $data['page_title'] = "Principal";
        $this->session->set_userdata('broadcast_email', $get_broadcast_email);

        if($this->session->userdata('broadcast_email')){
                display_output('1','Teacher approved Email.',array('details'=>$get_broadcast_email));
        }else{
                display_output('0','Teacher approved Email.');
        }
    }

    public function teacher_broadcast_page(){

        // $this->session->unset_userdata('broadcast_email');
        check_web_user_status();
        $broadcast_email = $this->session->userdata('broadcast_email');
       
        $school_id = $this->session->userdata('school_id');
        
            $data['user_info'] = "";
            $email_list = [];
            $broad_email = "";
            if($broadcast_email && count($broadcast_email)>0){
                foreach($broadcast_email as $list){
                    array_push($email_list,$list['email'] ) ;
                }
                $broad_email = implode(',', $email_list);
                $data['user_info'] = $broadcast_email;
            }       
            $data['Tolist'] = $this->School_model->getTeacherListForMail($school_id,2);     
            
            $data['title']= 'Broadcast' . " | ".SITE_TITLE;
            $data['meta_keywords']= 'Broadcast'." | ".SITE_TITLE;
            $data['meta_desc']= 'Broadcast'." | ".SITE_TITLE;
            $data['page_title'] = "Broadcast";
            $data['broad_email'] = $broad_email;
             //$this->session->unset_userdata('broadcast_email'); 
           
            $this->load->view('front/include/header',$data); 
            $this->load->view('front/principal/teacher_send_broadcasr_msg');
            $this->load->view('front/include/footer');
            
    }

    public function teacher_broadcast_messages(){
        
        check_web_user_status();
        $data['title']= 'Principal' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Principal'." | ".SITE_TITLE;
        $data['meta_desc']= 'Principal'." | ".SITE_TITLE;
        $data['page_title'] = "Principal";
        unset($_SESSION['broadcast_email']);
        $this->load->view('front/include/header',$data); 
        $this->load->view('front/principal/broadcast_message');
        $this->load->view('front/include/footer');
    }
    public function teacher_videos(){
        // echo '<pre>';
        // print_r($this->session->all_userdata());die;
        check_web_user_status();
        $school_id = $this->session->userdata('school_id');
        $data['title']= 'Principal' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Principal'." | ".SITE_TITLE;
        $data['meta_desc']= 'Principal'." | ".SITE_TITLE;
        $data['page_title'] = "Principal";
        $data['school_id'] = $school_id;

        $data['teachers_list'] =  $this->School_model->getTeacherListForFilter($school_id);


        $this->load->view('front/include/header',$data); 
        $this->load->view('front/principal/teacher_videos');
        $this->load->view('front/include/footer');
    }

    public function school_videos(){

        check_web_user_status();
        $data['title']= 'District' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'District'." | ".SITE_TITLE;
        $data['meta_desc']= 'District'." | ".SITE_TITLE;
        $data['page_title'] = "District";
        $districtId = $this->session->userdata('district_id');
        $data['school_list'] =  $this->Auth_model->getSchoolList($districtId);
            
        $this->load->view('front/include/header',$data); 
        $this->load->view('front/district/school_videos');
        $this->load->view('front/include/footer');

    }

    public function video_fulldetail($media_slug){

        check_web_user_status();
        $data['title']= 'District' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'District'." | ".SITE_TITLE;
        $data['meta_desc']= 'District'." | ".SITE_TITLE;
        $data['page_title'] = "District";

        $res = $this->District_model->getDetailBySlug($media_slug);    
        $data['media_id'] = $res['id'];
        $data['grade_n'] = $res['grade'];

        $this->load->view('front/include/header',$data); 
        $this->load->view('front/district/video_fulldetail');
        $this->load->view('front/include/footer');

    }

    public function profile_update() {
      
        $postData = $this->input->post();
        //echo "<pre>";print_r($postData);die;
        if($this->input->post('type')=='web'){
            $user_id=$this->session->userdata('user_id');
            $user_type=$this->session->userdata('front_user_type');
        }else{
            $user_id        =   test_input($this->input->post('user_id'));
            $user_type        =   test_input($this->input->post('user_type'));
        }
        $school_name        =   test_input($this->input->post('school_name'));
        $first_name        =   test_input($this->input->post('first_name'));
        $last_name        =   test_input($this->input->post('last_name'));
        $display_name        =   test_input($this->input->post('display_name'));
        $phone        =   test_input($this->input->post('phone'));
        $address        =   test_input($this->input->post('address'));

        if(empty($user_type)) {
            display_output('0','Please enter user type.');
        }if(empty($first_name)) {
            display_output('0','Please enter first name.');
        }if(empty($school_name)) {
            display_output('0','Please enter school name.');
        }
        if(empty($address)) {
            display_output('0','Please enter school address.');
        }if(empty($phone)) {
            display_output('0','Please enter phone number.');
        }
        if($user_type==2 || $user_type==3){
            if(empty($display_name)) {
                display_output('0','Please enter display name.');
            }
        }
        $update_data = array();
        $update_school_data  = array();
        /*************profile pic upload ************/
        $imgfilepath="";
        $imgfilerror="";
        if($this->input->post('type')!='web'){
            if(isset($_FILES['profile_pic']) && !empty($_FILES['profile_pic']['name'])){
                if($_FILES['profile_pic']['error']==0) {
                    $image_path = USER_PROFILE_PATH;
                    $allowed_types = '*';
                    $file='profile_pic';
                    $height = '';
                    $width = '';
                    $imgresponce = commonImageUpload($image_path,$allowed_types,$file,$width,$height);

                    if($imgresponce['status']==0){
                        $upload_error = $imgresponce['msg'];   
                        $imgfilerror="1";
                    } else {
                        $imgfilepath=$imgresponce['image_path'];
                    }
                }
            }
        }
          ///if(isset($_POST['image'])){

         /*************logo pic upload ************/

        $imglogopath="";
        $imglogoerror="";
        if($this->input->post('type')!='web'){
            if(isset($_FILES['school_logo']) && !empty($_FILES['school_logo']['name'])){
                if($_FILES['school_logo']['error']==0) {
                    $image_path = SCHOOL_LOGO_PATH;
                    $allowed_types = '*';
                    $file='school_logo';
                    $height = '';
                    $width = '';
                    $imgresponce = commonImageUpload($image_path,$allowed_types,$file,$width,$height);

                    if($imgresponce['status']==0){
                        $upload_error = $imgresponce['msg'];   
                        $imglogoerror="1";
                    } else {
                        $imglogopath=$imgresponce['image_path'];
                    }
                }
            }
        }

        $update_data=array(
            'first_name'=>$first_name,
            'last_name'=>$last_name,
            'phone'=>$phone,
            'display_name'=>$display_name,
            'is_approve'=>3,
            'modified'=>date('Y-m-d H:i:s'),
        );
        if(!empty($imgfilepath)){
            $update_data['profile_image'] = $imgfilepath;
        }
        if(isset($_POST['image']) && !empty($_POST['image'])){

            $data = $_POST["image"];
            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $imageName = USER_PROFILE_PATH.time() . '.png';
            file_put_contents($imageName, $data);
            $update_data['profile_image']=$imageName;
        }

       
        $where= array("user_id"=>$user_id);
        if($this->Common_model->addEditRecords('users',$update_data,$where)){
            $school_id = $this->Common_model->getFieldValue('users','school_id',array("user_id"=>$user_id));
            $where_school= array("id"=>$school_id);
            $update_school_data=array(
                'name'=>$school_name,
                'address'=>$address,
            );
            if(!empty($imglogopath)){
                $update_school_data['logo'] = $imglogopath;
            }
            if(isset($_POST['croplogo']) && !empty($_POST['croplogo'])){

                $data = $_POST["croplogo"];
                $image_array_1 = explode(";", $data);
                $image_array_2 = explode(",", $image_array_1[1]);
                $data = base64_decode($image_array_2[1]);
                $imageName = USER_PROFILE_PATH.time() . '.png';
                file_put_contents($imageName, $data);
                $update_school_data['logo']=$imageName;
            }
            $this->Common_model->addEditRecords('school',$update_school_data,$where_school);
            $details=$this->Auth_model->getUserDetail($user_id,$user_type);
            $details['mobile_auth_token']=$this->Common_model->getFieldValue('users','mobile_auth_token',$where);
            display_output('1','Profile successfully updated',array('details'=>$details));
        }else{
            display_output('0','Check your internet connection and try again.'); 
        }

    }

    function config_ajax_pagination($total_records,$b_url,$selector,$per_page,$function) {
        $config['target']      = $selector;
        $config['base_url']    = $b_url;
        $config['total_rows']  = $total_records;
        $config['per_page']    = $per_page;
        $config['function']    = $function;
        $config["uri_segment"] = 2;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['cur_tag_open'] = "<li class='active'><a href='javascript:void(0);'>";
        $config['cur_tag_close'] = '</a></li>';
        //return $config;
        $this->ajax_pagination->initialize($config);
    }


    public function broadcast_details($id){
        check_web_user_status();

        $data['title']= 'Broadcast' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Broadcast'." | ".SITE_TITLE;
        $data['meta_desc']= 'Broadcast'." | ".SITE_TITLE;
        $data['page_title'] = "Broadcast";

        $data['broadcast'] = $this->Common_model->getRecords('broadcast_notification','*',array('notification_id'=>$id),'',true);
        $data['broadcast_email'] = $this->Common_model->getRecords('broadcast_emails','*',array('notification_id'=>$id),'',false);
        
        $this->load->view('front/include/header',$data); 
        $this->load->view('front/broadcast_details');
        $this->load->view('front/include/footer');
    }

    /*======================Schoo Upload video================ */
    public function school_upload_video() 
    {
        check_web_user_status();
        $data['title']= 'Upload Video' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Upload Video'." | ".SITE_TITLE;
        $data['meta_desc']= 'Upload Video'." | ".SITE_TITLE;
        $data['page_title'] = "Upload Video";
        $school_id=$this->session->userdata('school_id');
        $data['media'] =$this->Common_model->getRecords('school_introductory_video', 'thumb_image,large_image,id,media_name,title,description,duration', array('school_id'=>$school_id), '', true);
        // echo "<pre>";
        // print_r($data['media']);
        // die;
        
        $data['form_action'] = "api/school-upload-video";
        $this->load->view('front/include/header',$data);
        $this->load->view('front/principal/upload-video');
        $this->load->view('front/include/footer');
    }


} //End controller class
