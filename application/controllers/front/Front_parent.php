<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Front_parent extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model(array('Common_model','Auth_model','Teacher_model','Parent_student_model')); 
        $this->load->helper(array('email','common_helper'));
       $this->load->library(array('Ajax_pagination','PHPExcel'));
      
    }

/*======================Edit Profile================ */
    public function profile() 
    {
        check_web_user_status(1);
       // print_r( check_web_user_status(1));die;
        $data['title']= 'Profile' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Profile'." | ".SITE_TITLE;
        $data['meta_desc']= 'Profile'." | ".SITE_TITLE;
        $data['page_title'] = "Profile";


        $school_id = $this->session->userdata('school_id');
        $user_id = $this->session->userdata('user_id');
        /*$get_student_list = $this->Common_model->getRecords('student','id,student_name',array('parent_id'=>$user_id,'is_deleted'=>0),'student_name asc');
        foreach ($get_student_list as $key => $student_list) {
            $grade_list = $this->Parent_student_model->getGradeDetails($student_list['id']);
            $get_student_list[$key]['grade_list'] = $grade_list;

            $teacher_grade_ids = $this->Common_model->getRecords('student_grade','teacher_grade_id',array('student_id'=>$student_list['id']));
            if(!empty($teacher_grade_ids)){
               $get_teachers = $this->Parent_student_model->getTeacherDetails($teacher_grade_ids); 
            }
            

        }
        $data['get_student_list'] = $get_student_list;*/
        $this->load->view('front/include/header',$data);
        $this->load->view('front/parent/profile');
        $this->load->view('front/include/footer');
    }

    public function Add_Student() 
    {   
       check_web_user_status(1);
       
        $data['title']= 'Profile' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Profile'." | ".SITE_TITLE;
        $data['meta_desc']= 'Profile'." | ".SITE_TITLE;
        $data['page_title'] = "Profile";


        $school_id = $this->session->userdata('school_id');
        $user_id = $this->session->userdata('user_id');
        $get_student_list = $this->Common_model->getRecords('student','id,student_name',array('parent_id'=>$user_id,'is_deleted'=>0),'student_name asc');
        foreach ($get_student_list as $key => $student_list) {
            $grade_list = $this->Parent_student_model->getGradeDetails($student_list['id']);
            $get_student_list[$key]['grade_list'] = $grade_list;
            /*$teacher_grade_ids = $this->Common_model->getRecords('student_grade','teacher_grade_id',array('student_id'=>$student_list['id']));
            if(!empty($teacher_grade_ids)){
               $get_teachers = $this->Parent_student_model->getTeacherDetails($teacher_grade_ids); 
            }*/
        }
        $data['school_grades']= $this->Common_model->getRecords('school_grade','CAST(grade AS UNSIGNED) as grade,grade_display_name',array('school_id'=>$school_id),'grade asc');
        $data['get_student_list'] = $get_student_list;

        $records = $this->Parent_student_model->getStudentRecords($user_id);

        $data['is_adable'] = 1;
        if (!empty($records)) {
            foreach ($records as $row) {
                if (empty($row['name'])) {
                    $data['is_adable'] = 1;
                } else {
                    if (($row['is_approve']==1) || ($row['is_approve']==2)) {
                        $data['is_adable'] = 0;
                    }
                }
            }
        }

        $this->load->view('front/include/header',$data);
        $this->load->view('front/parent/add_student');
        $this->load->view('front/include/footer');
    }

    public function teacher_wise_video($teacher_id,$grade){
        
        check_web_user_status();
        $data['title']= 'Parent' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Parent'." | ".SITE_TITLE;
        $data['meta_desc']= 'Parent'." | ".SITE_TITLE;
        $data['page_title'] = "Parent";
        $user_id=$this->session->userdata('user_id');
        $user_type=$this->session->userdata('front_user_type');
        $school_id=$this->session->userdata('school_id');
        
        $teacher_grade_details =$this->Common_model->getRecords('teacher_grade', 'grade,grade_display_name', array('teacher_id'=>$teacher_id), '', false);
            $data['teacher_grade']=$teacher_grade_details;

        $teacher_detail =$this->Common_model->getRecords('users', 'user_id, first_name,last_name,display_name', array('user_id'=>$teacher_id), '', false);
        $data['teacher_detail']=$teacher_detail;
        if(!$this->Teacher_model->CheckTeacherApprovedStatus($grade,$teacher_id,$user_id)){
            //display_output('0','Your request either pending or not found.');
            $data['not_approved']=1;
            $data['msg']='Your request is either pending or not found.';
        }

        $teacher_id = $teacher_id;
        if($video_list = $this->Parent_student_model->getAllVideos($user_id,$user_type,$school_id,$teacher_id,$grade)){
            for($k=0; $k<count($video_list); $k++){
                $video_list[$k]['created'] = convertGMTToLocalTimezone($video_list[$k]['created'], false, false);
            }
            $data['teacher_video_list'] = $video_list; 
            // $data['recent_video'] = $video_list;
            // print_r($data['recent_video']);die;
        }
        $data['grade'] =$grade;
        // echo '<pre>';print_r($data); die;        
        $this->load->view('front/include/header',$data);
        $this->load->view('front/teacher/teacher_video_detail');
        $this->load->view('front/include/footer');

    }

    public function home()  {

        check_web_user_status();
        $data['title']= 'Profile' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Profile'." | ".SITE_TITLE;
        $data['meta_desc']= 'Profile'." | ".SITE_TITLE;
        $data['page_title'] = "Profile";
    
        $user_id=$this->session->userdata('user_id');
        $user_type=$this->session->userdata('front_user_type');
        $school_id=$this->session->userdata('school_id');
            if($user_id) {
              
                $data['school_video']=$this->Parent_student_model->getSchoolVideo($school_id);
                 $data['school'] = $this->db->get_where('school',array('id'=>$school_id))->row();
                if($teacher_grade_id=$this->Teacher_model->getApprovedTeachers($user_id)){
                    if(!empty($teacher_grade_id['teacher_grade_id'])){
                        $data['recently_video'] = $this->Teacher_model->getRecentVideo(1,0,"",$teacher_grade_id['teacher_grade_id'],$school_id);
                        $data['recently_video_count'] = $this->Teacher_model->getRecentVideoCount(0,0,"",$teacher_grade_id['teacher_grade_id'],$school_id);
                        
                    }else{
                        $data['recently_video']=array();
                    }
                   
                    
                }else{
                    $data['recently_video']=array();
                }

               
                $data['continue_video'] = $this->Teacher_model->getRecentVideo(1,1,$user_id);
                $data['continue_video_count'] = $this->Teacher_model->getRecentVideoCount(0,1,$user_id);
                //echo $this->db->last_query();die;
                    $data['badge'] = '0';
                    $data['notification_flag'] = '0';
                    if($res = $this->Common_model->getRecords('users', 'unread_notification,notification_flag', array('user_id'=>$user_id), '', true)) {
                        $data['badge'] = (string)$res['unread_notification'];
                        $data['notification_flag'] = (string)$res['notification_flag'];
                    }

                $this->load->view('front/include/header',$data);
                $this->load->view('front/parent/index');
                $this->load->view('front/include/footer');
               
            }
       
    } 


    public function principal_video($media_id){
        check_web_user_status();
        $data['title']= 'Parent' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Parent'." | ".SITE_TITLE;
        $data['meta_desc']= 'Parent'." | ".SITE_TITLE;
        $data['page_title'] = "Parent";
        $data['media_id'] = $media_id;

        $this->load->view('front/include/header',$data);
        $this->load->view('front/parent/principal-video');
        $this->load->view('front/include/footer');
    }
    public function story_time(){
        check_web_user_status();
        $data['title']= 'Story Time' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Story Time'." | ".SITE_TITLE;
        $data['meta_desc']= 'Story Time'." | ".SITE_TITLE;
        $data['page_title'] = "Story Time";
       
        $this->load->view('front/include/header',$data);
        $this->load->view('front/parent/story-time');
        $this->load->view('front/include/footer');
    }
    public function tutor(){
        check_web_user_status();
        $data['title']= 'Tutor' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Tutor'." | ".SITE_TITLE;
        $data['meta_desc']= 'Tutor'." | ".SITE_TITLE;
        $data['page_title'] = "Tutor";
       
        $this->load->view('front/include/header',$data);
        $this->load->view('front/parent/tutor');
        $this->load->view('front/include/footer');
    }

    public function recently_video(){
        check_web_user_status();
        $data['title']= 'Video' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Video'." | ".SITE_TITLE;
        $data['meta_desc']= 'Video'." | ".SITE_TITLE;
        $data['page_title'] = "Video";

        $user_id=$this->session->userdata('user_id');
        $user_type=$this->session->userdata('front_user_type');
        $school_id=$this->session->userdata('school_id');

        $teacher_grade_id=$this->Teacher_model->getApprovedTeachers($user_id);

        $data['recently_video'] = $this->Teacher_model->getRecentVideo(0,0,"",$teacher_grade_id['teacher_grade_id'],$school_id);

        $this->load->view('front/include/header',$data);
        $this->load->view('front/parent/recently');
        $this->load->view('front/include/footer');
    }

    public function continue_video(){
        check_web_user_status();
        $data['title']= 'Video' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Video'." | ".SITE_TITLE;
        $data['meta_desc']= 'Video'." | ".SITE_TITLE;
        $data['page_title'] = "Video";
        $user_id=$this->session->userdata('user_id');
        $user_type=$this->session->userdata('front_user_type');
        $school_id=$this->session->userdata('school_id');

        $teacher_grade_id=$this->Teacher_model->getApprovedTeachers($user_id);

        $data['continue_video'] = $this->Teacher_model->getRecentVideo(0,1,$user_id);

        $this->load->view('front/include/header',$data);
        $this->load->view('front/parent/continue_video');
        $this->load->view('front/include/footer');
    }

 



} //End controller class
