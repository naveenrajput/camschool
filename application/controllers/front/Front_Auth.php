<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Front_Auth extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model(array('Common_model','Auth_model')); 
        $this->load->helper(array('email','common_helper'));
        $this->load->library('PHPExcel');
      
    }

/*=======================================Signup======================================= */
    public function sign_up() 
    {  
        $user_id= $this->session->userdata('user_id');
        if(!empty($user_id))
        {
            $user_type= $this->session->userdata('front_user_type');
            userRedirection($user_type);
           
        }
        $data['title']= 'Sign Up' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Sign Up'." | ".SITE_TITLE;
        $data['meta_desc']= 'Sign Up'." | ".SITE_TITLE;
        $data['page_title'] = "Sign Up";
        $data['states']=  $this->Common_model->getRecords('states');
        $this->load->library('Recaptcha');
        $data['widget'] = $this->recaptcha->getWidget();
        $data['script'] = $this->recaptcha->getScriptTag();
        $this->load->view('front/include/header',$data);
        $this->load->view('front/signup');
       // $this->load->view('front/include/footer');
    }
    /*=======================================Login======================================= */
    public function login() 
    {
        $data['title']= 'Login' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Login'." | ".SITE_TITLE;
        $data['meta_desc']= 'Login'." | ".SITE_TITLE;

        $data['page_title'] = "Login";

        $user_id= $this->session->userdata('user_id');

        if(!empty($user_id))
        {
            $user_type= $this->session->userdata('front_user_type');
            userRedirection($user_type);
        }
        $this->load->view('front/include/header',$data);
        $this->load->view('front/login');
        //$this->load->view('front/include/footer');
    }
    /*=======================================verify email ======================================= */
    public function verify_email() 
    {

        $user_id= $this->session->userdata('user_id');
        if(!empty($user_id))
        {
            $user_type= $this->session->userdata('front_user_type');
            userRedirection($user_type);
        }
        
        $userId             =   test_input(base64_decode($this->uri->segment(2)));
        $verificationCode   =   test_input(base64_decode($this->uri->segment(3)));

        $where= array("user_id"=>$userId);
        $userData = $this->Common_model->getRecords('users','first_name,last_name,email,email_verified,verification_token,verification_token_date,user_type,mobile_auth_token,district_id,is_approve,school_id',$where,'user_id desc',true);
        if(!$userData) {
            $this->session->set_flashdata('error', 'We are not able to find you.');
            redirect('login');
        } 
        $userDate   = strtotime($userData['verification_token_date']); // current timestamp
        $timeSecond = strtotime(date("Y-m-d H:i:s"));
        $differenceInSeconds = $timeSecond - $userDate;
        $minutes = round($differenceInSeconds/60);
        
        if ($userData['email_verified']==1) {
            $msg="Your email has already been verified; please login.";
            if ($userData['user_type']==2 && $userData['is_approve']==1) {
                /*$msg="Your email has already been verified, but the district has not approved your profile yet! Contact your district administrator or email us at principals@chatatmeschools.com.";*/
                $msg="Your email has already been verified, but the district has not approved your profile yet. If 24 hours have passed since you registered, contact your district administrator or email us at principals@chatatmeschools.com.";
            }
            if ($userData['user_type']==3 && $userData['is_approve']==1) {
                /*$msg="Your email has already been verified, but your principal has not approved your profile yet. Contact your principal or email us at teachers@chatatmeschools.com.";*/
                $msg="Your email has already been verified, but your principal has not approved your profile yet. If 24 hours have passed since you registered, contact your principal or email us at teachers@chatatmeschools.com.";
            }
            $this->session->set_flashdata('error', $msg);
            redirect('login');
        }

        if($minutes>720) {
            $this->session->set_flashdata('error', 'Sorry! The time limit to verify your account has expired. Try again.');
            redirect('login');
        } else {
            
            if($userData['verification_token'] == $verificationCode) {
                $date = date('Y-m-d H:i:s');
                $update_data = array(
                    'email_verified'=>1,
                    'modified_datetime'=>$date,
                    'verification_token'=>'',
                    'verification_token_date'=>"0000-00-00 00:00:00"
                );
                $res = $this->Common_model->addEditRecords('users',$update_data,$where);
                if($res) {
                    if ($userData['user_type']==2) {
                        $district_info = $this->Common_model->getRecords('users','user_id,first_name,last_name,email',array('district_id' =>$userData['district_id'],'user_type'=>1),'', true);
                        $fromEmail = getAdminEmail();  
                        // $subject = 'New school registered-'.WEBSITE_NAME;
                        $subject = 'A principal registered for Chat At Me! Schools';
                        $chk_school = $this->Common_model->getRecords('school','id,name',array('id'=>$userData['school_id']),'',true,'','',1);
                        // $data['message']= ucfirst($chk_school['name'])." registered in your district. Please check the request.";
                        $data['message']="The principal at ".ucfirst($chk_school['name'])." has registered for Chat At Me! Schools. Please check and verify the request.";
                        $data['name']= ucfirst($district_info['first_name']).' '.$district_info['last_name'];
                        $toEmail = $district_info['email']; 
                        $body = $this->load->view('template/common', $data,TRUE);
                        $this->Common_model->sendEmail($toEmail,$subject,$body,$fromEmail);
                        // $msg = ucfirst($chk_school['name'])." registered in your district. Please check the request.";
                        $msg ="The principal at ".ucfirst($chk_school['name'])." has registered for Chat At Me! Schools. Please check and verify the request.";
                        addNotification($userId,$district_info['user_id'],'Request',$msg,$userId,0); 
                        sendNotification($district_info['user_id'],$msg);
                    }else if($userData['user_type']==3){
                        $school_info = $this->Common_model->getRecords('users','user_id,first_name,last_name,email',array('school_id' =>$userData['school_id'],'user_type'=>2),'', true);
                        $fromEmail = getAdminEmail();  
                        // $subject = 'New teacher registered-'.WEBSITE_NAME;
                        $subject = 'A new teacher registered for Chat At Me! Schools.';
                        $data['message']= ucfirst($userData['first_name'])." ".$userData['last_name']." registered in your school. Please check the request.";
                        $data['name']= ucfirst($school_info['first_name']).' '.$school_info['last_name'];
                        $toEmail = $school_info['email']; 
                        $body = $this->load->view('template/common', $data,TRUE);
                        $this->Common_model->sendEmail($toEmail,$subject,$body,$fromEmail);
                        $msg = ucfirst($userData['first_name'])." ".$userData['last_name']." registered in your school. Please check the request.";
                        addNotification($userId,$school_info['user_id'],'Request',$msg,$userId,0); 
                        sendNotification($school_info['user_id'],$msg);
                    }
                    $msg="Your email has been verified; please login.";
                    if ($userData['user_type']==2) {
                        /*$msg="Your email has been verified, but the district has not approved your profile yet! Contact your district administrator or email us at principals@chatatmeschools.com.";*/
                        $msg="Thank you for registering with ".WEBSITE_NAME.". The information has been sent to your district administrator for approval. If your login credentials are not active within 24 hours contact your district administrator or email us at principals@chatatmeschools.com.";
                    }
                    if ($userData['user_type']==3) {
                        /*$msg="Your email has been verified, but your principal has not approved your profile yet. Contact your principal or email us at teachers@chatatmeschools.com.";*/
                        $msg="Thank you for registering with ".WEBSITE_NAME.". The information has been sent to your school's principal for approval. If your login credentials are not active within 24 hours contact your principal or email us at teachers@chatatmeschools.com.";
                    }
                    $this->session->set_flashdata('success', $msg);
                    redirect('login');
                } else {
                    $this->session->set_flashdata('error', 'Check your internet connection and try again.');
                    redirect('login');
                }
            } else {
                $this->session->set_flashdata('error', 'You entered an invalid verification code. Please recheck the code and enter it again.');
                redirect('login');
            }
        }
        
    }
    public function email_verification() {
        try {
            if($this->input->post()) {

                $postData = $this->input->post();

                $userId             =   test_input($postData['user_id']);
                $email              =   test_input($postData['email']);
                $verificationCode   =   test_input($postData['verification_code']);
         
                if(empty($userId) || empty($email) || empty($verificationCode)){
                    echo json_encode(array("status" => '0', "msg" => "Fill in all the required fields - user id, email, verification code.", "details" => []));exit;
                }

                $where= array("user_id"=>$userId);
                $userData = $this->Common_model->getRecords('users','email,email_verified,verification_token,verification_token_date,user_type,mobile_auth_token',$where,'',true);
                if(!$userData) {
                    echo json_encode(array("status" => '0', "msg" => "Record not found", "details" => []));exit;
                } 

                $userDate   = strtotime($userData['verification_token_date']); // current timestamp
                $timeSecond = strtotime(date("Y-m-d H:i:s"));
                $differenceInSeconds = $timeSecond - $userDate;
                $minutes = round($differenceInSeconds/60);
                
                if ($userData['email_verified']==1) {
                    echo json_encode(array("status" => '0', "msg" => "Your email has already been verified; please login.", "details" => []));exit;
                }

                if($minutes>720) {
                    echo json_encode(array("status" => '0', "msg" => "Sorry! The time limit to verify your account has expired. Try again.", "details" => []));exit;
                } else {
                    if($userData['email'] != $email) {
                        echo json_encode(array("status" => '0', "msg" => "Please enter your registered email address.", "details" => []));exit;
                    }

                    if($userData['verification_token'] == $verificationCode) {
                        $date = date('Y-m-d H:i:s');
                        $mobileAuthToken = generateRandomString();
                        $update_data = array(
                            'email_verified'=>1,
                            'mobile_auth_token'=>$mobileAuthToken,
                            'modified_datetime'=>$date,
                            'verification_token'=>'',
                            'verification_token_date'=>"0000-00-00 00:00:00"
                        );
                        $res = $this->Common_model->addEditRecords('users',$update_data,$where);
                        if($res) {
                            // $userData = $this->Common_model->getRecords('users','*',array('user_id'=>$userId),'',true);
                            $userprofile = $this->Auth_model->getUserDetail($userId,$userData['user_type']);
                            $userprofile['mobile_auth_token']=$userData['mobile_auth_token'];
                            
                            echo json_encode(array("status" => '1', "msg" => "Your email has been verified; please login.", "details" => $userprofile));exit;
                        } else {
                            echo json_encode(array("status" => '0', "msg" => "Some error occured. Please try again later.", "details" => []));exit;
                        }
                    } else {
                        echo json_encode(array("status" => '0', "msg" => "Invalid verification code, please enter the code which is received on Email.", "details" => []));exit;
                    }
                }

            }
        } catch (Exception $e) {
            echo json_encode(array("status" => '0', "msg" => $e->getMessage(), "details" => []));
            exit;
        }
    }
     /*=============forgot password ================ */
    public function forgot_password()
    {   
        $user_id= $this->session->userdata('user_id');
        if(!empty($user_id))
        {
            $user_type= $this->session->userdata('front_user_type');
            userRedirection($user_type);
        }
        $data['title']= 'Forgot Password' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Forgot Password'." | ".SITE_TITLE;
        $data['meta_desc']= 'Forgot Password'." | ".SITE_TITLE;
        $data['page_title'] = "Forgot Password";
       
        $this->load->view('front/include/header',$data);
        $this->load->view('front/forgot_password');
        
    }
     /*=============reset password ================ */
    public function reset_password()
    {
        $user_id= $this->session->userdata('user_id');
        if(!empty($user_id))
        {
            $user_type= $this->session->userdata('front_user_type');
            userRedirection($user_type);
        }
        $data['title']= 'Reset Password' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Reset Password'." | ".SITE_TITLE;
        $data['meta_desc']= 'Reset Password'." | ".SITE_TITLE;
        $data['page_title'] = "Reset Password";
        if($this->input->get('token')) 
        {

            $data['token'] = $this->input->get('token');
            if($user_data = $this->Common_model->getRecords('users','user_id,user_type,reset_token_date,password',array('reset_token'=> $this->input->get('token')),'',true)) 
            { 
                if($this->input->post()) 
                {

                    $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[6]|max_length[30]');
                    $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[new_password]');

                    if ($this->form_validation->run() == FALSE) {   
                        $this->form_validation->set_error_delimiters('<div class="parsley-errors-list">', '</div>');
                        $this->load->view('front/include/header',$data);
                        $this->load->view('front/reset_password',$data);
                    } 
                    else {
                        $where = array('user_id' => $user_data['user_id']);
                        $update_data = array(
                            'password' => md5($this->input->post('new_password')), 
                            'reset_token' =>'',
                            'reset_token_date' =>''
                            );
                        if(!$this->Common_model->addEditRecords('users', $update_data, $where)) {
                            $this->session->set_flashdata('error', 'Check your internet connection and try again.');
                            redirect('reset_password');
                        } else {
                            $this->session->set_flashdata('success', 'Your password was successfully changed.');
                            redirect('login');
                        }
                    }
                } else {
                    $token_date = strtotime($user_data['reset_token_date']);
                    $current_date=strtotime(date("Y-m-d H:i:s"));
                    $diff=$current_date-$token_date;
                    if($diff > 3600) {
                        $this->session->set_flashdata('error', 'This reset password link has expired. Please have another one sent to your email.');
                        redirect('forgot-password');
                    } else {
                            $this->load->view('front/include/header',$data);
                            $this->load->view('front/reset_password',$data);
                    }
                }
            } else {
                $this->session->set_flashdata('error', 'This reset password code is invalid. Please check your email and re-enter the code.');
                redirect('forgot-password');
            }   
        } else {
            $this->session->set_flashdata('error', 'This reset password code is invalid. Please check your email and re-enter the code.');
            redirect('forgot-password');
        }
        
    }


    /*=========================Change password==================== */
    public function change_password()
    {   
        check_web_user_status(1);
        $data['title']= 'Change Password' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Change Password'." | ".SITE_TITLE;
        $data['meta_desc']= 'Change Password'." | ".SITE_TITLE;
        $data['page_title'] = "Change Password";
       
        $this->load->view('front/include/header',$data);
        $this->load->view('front/change_password');
        $this->load->view('front/include/footer');
        
    }
    public function support()
    {   
        check_web_user_status();
        $data['title']= 'Contact Us' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Contact Us'." | ".SITE_TITLE;
        $data['meta_desc']= 'Contact Us'." | ".SITE_TITLE;
        $data['page_title'] = "Contact Us";
        $data['form_action'] = "api/contact-us";
        $this->load->view('front/include/header',$data);
        $this->load->view('front/support');
        $this->load->view('front/include/footer');
        
    }
    /*=================================== notification =====================================*/

    public function notification()
    {   
        check_web_user_status();
        $data['title']= 'Notifications' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Notifications'." | ".SITE_TITLE;
        $data['meta_desc']= 'Notifications'." | ".SITE_TITLE;
        $data['page_title'] = "Notifications";
        $this->load->view('front/include/header',$data);
        $this->load->view('front/notification');
        $this->load->view('front/include/footer');
        
    }

    /*=======================================download======================================= */
    public function download_media($id)
    {
       
        if($id==2){
            $file_url = 'assets/front/csv/Sample File.xlsm';
        }else{
            $file_url = 'assets/front/csv/Sample File.xlsx';
        }

        header('Content-Type: application/octet-stream');

        header("Content-Transfer-Encoding: Binary");

        header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\"");

        readfile($file_url);

        exit();
    }
    /*=======================================Logout======================================= */

    public function logout()
    {
        
        $user_id = $this->session->userdata('user_id');
        $this->session->sess_destroy();
        redirect("login");
    }




} //End controller class
