<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Front_district extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model(array('Common_model','Auth_model','District_model')); 
        $this->load->helper(array('email','common_helper'));
       $this->load->library(array('Ajax_pagination','PHPExcel'));
        ini_set('memory_limit', '-1');
    }

/*======================Edit Profile================ */
    public function profile() 
    {
        check_web_user_status();
        //echo '<pre>';print_r($_SESSION);exit;
        $data['title']= 'Profile' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Profile'." | ".SITE_TITLE;
        $data['meta_desc']= 'Profile'." | ".SITE_TITLE;
        $data['page_title'] = "Profile";
        
        $this->load->view('front/include/header',$data);
        $this->load->view('front/district/profile');
        $this->load->view('front/include/footer');
    }
/*===================Upload CSV================= */
    public function upload_csv() 
    {
        check_web_user_status();
        $data['title']= 'Upload CSV' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Upload CSV'." | ".SITE_TITLE;
        $data['meta_desc']= 'Upload CSV'." | ".SITE_TITLE;
        $data['page_title'] = "Upload CSV";
        $user_id=$this->session->userdata('user_id');
        $data['csv_data'] =$this->Common_model->getRecords('csv_uploads', '*', array('user_id'=>$user_id), 'id desc', false);
                
        $this->load->view('front/include/header',$data);
        $this->load->view('front/district/csv-upload');
        $this->load->view('front/include/footer');
    }
    public function import_file() 
    {
        error_reporting(0);
        check_web_user_status();
        if(isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])){
            $fileName = time() .'_'. $_FILES['file']['name'];
            $grade_array=array('Pre-Kindergarten','Kindergarten','1st Grade','2nd Grade','3rd Grade','4th Grade','5th Grade','6th Grade','7th Grade','8th Grade','9th Grade','10th Grade','11th Grade','12th Grade');
            if($_FILES["file"]["size"] > 0)
            {
                $config['upload_path'] = DISTRICT_CSV_PATH;                           
                $config['file_name'] = $fileName;
                $config['allowed_types'] = '*';        //some files does not contain proper file type
                $path_parts = pathinfo($_FILES["file"]["name"]);
                $extension = $path_parts['extension'];
                if(!in_array($extension, array('xls','xlsx'))){
                    $this->session->set_flashdata('error', 'File type not permitted.');
                    redirect('district/upload-csv');
                }
                $this->load->library('upload');
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('file')){
                    $this->upload->display_errors();
                    $error = $this->upload->display_errors(); 
                    $this->session->set_flashdata('error', $error);
                    redirect('district/upload-csv');
                 }
                 else{
                    $media = $this->upload->data();
                    $inputFileName = DISTRICT_CSV_PATH . $media['file_name'];
                }
                try {
                    
                    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFileName);

              
                } catch (Exception $e) {
                    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
                }
               
                $sheet = $objPHPExcel->getSheet(0);
                // $highestRow = $sheet->getHighestRow();
                $highestRow = $sheet->getHighestDataRow(); 
                $highestColumn = $sheet->getHighestColumn();
                $maxCell = $sheet->getHighestRowAndColumn();
                $err_flag=0;

                $objPHPExcel->getActiveSheet()->SetCellValue('O' . 1,'Error Messages');

                for ($row = 1; $row <= $highestRow; $row++) { 
                    $error_msg='';
                    // Read a row of data into an array                 
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

                    
                    if($row==1){
                        if (!empty($rowData[0][0])) {
                            if (trim($rowData[0][0])!='School’s name*') {
                                $err_flag=1;
                                $error_msg.='Column name'.' '.$rowData[0][0].' '.'is invalid.';
                            }
                        }
                        if (!empty($rowData[0][1])){
                            if (trim($rowData[0][1])!='School’s Address') 
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][1].' '.'is invalid.';
                            }
                        }
                        if (!empty($rowData[0][2])){
                            if (trim($rowData[0][2])!='School logo URL')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][2].' '.'is invalid.';
                            }
                        }
                        if (!empty($rowData[0][3])){
                            if (trim($rowData[0][3])!='Grades taught - From*')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][3].' '.'is invalid.';
                            }
                        }
                        if (!empty($rowData[0][4])){
                            if (trim($rowData[0][4])!='Grades taught - To*')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][4].' '.'is invalid.';
                            }
                        }
                        if (!empty($rowData[0][5])){
                            if (trim($rowData[0][5])!='Principal’s Title')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][5].' '.'is invalid.';
                            }
                        }
                        if (!empty($rowData[0][6])){
                            if (trim($rowData[0][6])!='Principal’s First Name')
                            { 
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][6].' '.'is invalid.';  
                            }
                        }
                        if (!empty($rowData[0][7])){
                            if (trim($rowData[0][7])!='Principal’s Last Name')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][7].' '.'is invalid.';
                            }
                        }
                        if (!empty($rowData[0][8])){
                            if (trim($rowData[0][8])!='Principal’s email')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][8].' '.'is invalid.';
                            }
                        }if (!empty($rowData[0][9])){
                            if (trim($rowData[0][9])!='Principal’s Phone')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][9].' '.'is invalid.';
                            }
                        }if (!empty($rowData[0][10])){
                            if (trim($rowData[0][10])!='Teacher’s First Name')
                            { 
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][10].' '.'is invalid.';  
                            }
                        }if (!empty($rowData[0][11])){
                            if (trim($rowData[0][11])!='Teacher’s Last Name')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][11].' '.'is invalid.';
                            }
                        }if (!empty($rowData[0][12])){
                            if (trim($rowData[0][12])!='Teacher’s email')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][12].' '.'is invalid.';
                            }
                        }if (!empty($rowData[0][13])){
                            if (trim($rowData[0][13])!='Grades assigned to the teacher')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][13].' '.'is invalid.';
                            }
                        }
                    }

                    if($err_flag>0){
                        $objPHPExcel->getActiveSheet()->SetCellValue('O' . $row,$error_msg);
                    }

                    // new validation filter
                    if($row>1){ 
                        if(empty($rowData[0][0]) && empty($rowData[0][1]) && empty($rowData[0][2]) && empty($rowData[0][3]) && empty($rowData[0][4]) &&empty($rowData[0][5]) && empty($rowData[0][6]) && empty($rowData[0][7]) && empty($rowData[0][8]) && empty($rowData[0][9]) && empty($rowData[0][10]) && empty($rowData[0][11]) && empty($rowData[0][12]) && empty($rowData[0][13]) ){
                            continue;
                        }

                        $schoolName         = trim($rowData[0][0]);
                        $schoolAddress      = trim($rowData[0][1]);
                        $schoolLogo         = trim($rowData[0][2]);
                        $gradeFrom          = trim($rowData[0][3]);
                        $gradeTo            = trim($rowData[0][4]);
                        $pricipalTitle      = trim($rowData[0][5]);
                        $principalFirstName = trim($rowData[0][6]);
                        $principalLastName  = trim($rowData[0][7]);
                        $principalEmail     = trim($rowData[0][8]);
                        $principalPhone     = trim($rowData[0][9]);
                        $teacherFirstName   = trim($rowData[0][10]);
                        $teacherLastName    = trim($rowData[0][11]);
                        $teacherEmail       = trim($rowData[0][12]);
                        $gradesAssign       = trim($rowData[0][13]);
                        
                        if(empty($schoolName)){
                            $error_msg.= 'School name is required.';
                            $err_flag=1;
                        } else {
                            if (strlen(trim($schoolName)) > 100) {
                                $error_msg.='School’s name must be less than 100 characters.';
                                $err_flag=1;                            
                            }
                        }

                        if (!empty($schoolLogo)) {
                            $extension = pathinfo($schoolLogo, PATHINFO_EXTENSION);
                            if(!empty($extension)){
                                $extensionArr = array('jpg','jpeg','png','JPG','JPEG','PNG');
                                if (!in_array($extension, $extensionArr)) {
                                    $error_msg.='School’s logo url is incorrect.';
                                    $err_flag=1;                            
                                }
                            } else {
                                $error_msg.='School’s logo url is incorrect.';
                                $err_flag=1;                                                            
                            }
                        }

                        if((!empty($gradeFrom))){
                            if(!in_array($gradeFrom, $grade_array)){
                                $error_msg.='Invalid grade taught From.';
                                $err_flag=1;
                            }
                        }else{
                            $error_msg.='Invalid grade taught From.';
                            $err_flag==1;
                        }
                        if((!empty($gradeTo))){
                            if(!in_array($gradeTo, $grade_array)){
                                $error_msg.='Invalid grade taught To.';
                                $err_flag=1;
                            }
                        }else{
                            $error_msg.='Invalid grade taught To.';
                            $err_flag==1;
                        }

                        $school_taught_from=$this->Common_model->ranges($gradeFrom);
                        $school_taught_to=$this->Common_model->ranges($gradeTo);
                        if($school_taught_from > $school_taught_to){
                            $error_msg.='Taught From column must be less than taught To.';
                            $err_flag==1;
                        } 

                        if(!empty($principalEmail)){
                            if(!empty($principalPhone)){
                                $numbers_only = preg_replace("/[^\d]/", "", $principalPhone);
                                if(strlen($numbers_only)!=10){
                                    $error_msg.='Phone No. must equal 10 characters.';
                                    $err_flag=1; 
                                }
                            }

                            if(!empty($principalFirstName)){
                                if (strlen(trim($principalFirstName)) > 30) {
                                    $error_msg.='Principal’s first name must be less than 30 characters.';
                                    $err_flag=1;                            
                                }
                            }                        

                            if(!empty($principalLastName)){
                                if (strlen(trim($principalLastName)) > 30) {
                                    $error_msg.='Principal’s last name must be less than 30 characters.';
                                    $err_flag=1;                            
                                }
                            }

                            if(!empty($teacherFirstName)){
                                if (strlen($teacherFirstName) > 30) {
                                    $error_msg.='Teacher’s first name must be less than 30 characters.';
                                    $err_flag=1;                            
                                }
                            }

                            if(!empty($teacherLastName)){
                                if (strlen($teacherLastName) > 30) {
                                    $error_msg.='Teacher’s last name must be less than 30 characters.';
                                    $err_flag=1;                            
                                }
                            }

                            if((!empty($teacherEmail))){
                                if (!filter_var($teacherEmail, FILTER_VALIDATE_EMAIL)) 
                                {    
                                    $error_msg.='Teacher’s email invalid.';
                                    $err_flag=1;
                                }
                                if($check_teacher_email = $this->Common_model->getRecords('users','email',array('is_deleted'=>0,'email'=>$teacherEmail),'user_id desc',true)) 
                                {
                                    $error_msg.= 'Teacher’s email already exist.';
                                    $err_flag=1;
                                }
                            }                            
                            if((!empty($teacherEmail))){
                                if(!empty($gradesAssign)){
                                    $arrayRange = range($school_taught_from ,$school_taught_to);

                                    $r_trim=rtrim($gradesAssign,',');
                                    $l_trim=ltrim($r_trim,',');
                                    $gradesAssign=$l_trim;
                                    $teacher_grade= $this->teacher_grade_replace($gradesAssign);
                                    if(!empty($teacher_grade)){
                                        $explodeArr = explode(',', $teacher_grade);
                                        if (!empty($explodeArr)) {
                                            if($this->isInArray($explodeArr, $arrayRange) == false){
                                                $error_msg.='Invalid grades for teacher.';
                                                $err_flag=1;
                                            }
                                        }else{
                                            $error_msg.='Invalid grades for teacher.';
                                            $err_flag=1;
                                        }
                                    }else{
                                        $error_msg.='Invalid grades for teacher.';
                                        $err_flag=1;
                                    }
                                } else {
                                    $error_msg.='Teacher grades are empty.';
                                    $err_flag=1;
                                }    
                            }
                        }                          

                        if((!empty($principalEmail))){
                            if (!filter_var($principalEmail, FILTER_VALIDATE_EMAIL)) 
                            {    
                                $error_msg.='Principal’s email invalid.';
                                $err_flag=1;
                            }
                           /* if($check_school = $this->Common_model->getRecords('users','email',array('is_deleted'=>0,'email'=>$principalEmail),'user_id desc',true)) 
                            {
                                $error_msg.= 'Principal’s email already exist.';
                                $err_flag=1;
                            }*/
                            if($check_school = $this->Common_model->getRecords('users','email',array('is_deleted'=>0,'user_type !='=>2,'email'=>$principalEmail),'user_id desc',true)){
                                $error_msg.= 'Principal’s email already exist.';
                                $err_flag=1;
                            }
                        }
                        $objPHPExcel->getActiveSheet()->SetCellValue('O'. $row,$error_msg);
                    }
                    // new validation filter
                }
                
                if($err_flag==1){
                    if(file_exists($inputFileName)){
                        unlink($inputFileName);
                    }
                    $fileName = 'resources/district_csv/error/error_'.$media['file_name'];
                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                    $objWriter->save($fileName);
                    // download file
                    // header("Content-Type: application/vnd.ms-excel");
                    //redirect($fileName);
                    $link = '<a href="'.$fileName.'"> Click here to download</a>';
                    $this->session->set_flashdata('error', 'There are some errors in uploaded file. Please check error file.'.$link);
                    redirect('district/upload-csv');  
                }else{

                    $district_id = $this->session->userdata('district_id');
                    $state_id   = $this->Common_model->getFieldValue('district','state_id',array('id'=>$district_id));
                    $user_id = $this->session->userdata('user_id');
                    $created = date('Y-m-d H:i:s');

                    for ($row = 1; $row <= $highestRow; $row++) { 
                        $error_msg='';
                        // Read a row of data into an array                 
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

                        // new validation filter
                        if($row>1){ 
                            if(empty($rowData[0][0]) && empty($rowData[0][1]) && empty($rowData[0][2]) && empty($rowData[0][3]) && empty($rowData[0][4]) &&empty($rowData[0][5]) && empty($rowData[0][6]) && empty($rowData[0][7]) && empty($rowData[0][8]) && empty($rowData[0][9]) && empty($rowData[0][10]) && empty($rowData[0][11]) && empty($rowData[0][12]) && empty($rowData[0][13]) ){
                                continue;
                            }

                            $schoolName         = trim($rowData[0][0]);
                            $schoolAddress      = trim($rowData[0][1]);
                            $schoolLogo         = trim($rowData[0][2]);
                            $gradeFrom          = trim($rowData[0][3]);
                            $gradeTo            = trim($rowData[0][4]);
                            $pricipalTitle      = trim($rowData[0][5]);
                            $principalFirstName = trim($rowData[0][6]);
                            $principalLastName  = trim($rowData[0][7]);
                            $principalEmail     = trim($rowData[0][8]);
                            $principalPhone     = trim($rowData[0][9]);
                            $teacherFirstName   = trim($rowData[0][10]);
                            $teacherLastName    = trim($rowData[0][11]);
                            $teacherEmail       = trim($rowData[0][12]);
                            $gradesAssign       = trim($rowData[0][13]);

                            if(!empty($schoolLogo)){
                                $new_img_name="";
                                $schoolLogoCron=$schoolLogo;
                            }else{
                                $new_img_name="";
                                $schoolLogoCron="";
                            }
                            if(!$check_school = $this->Common_model->getRecords('school','id,name',array('district_id'=>$district_id,'name'=>$schoolName),'',true)) 
                            {
                                $school_taught_from=$this->Common_model->ranges($gradeFrom);
                                $school_taught_to=$this->Common_model->ranges($gradeTo);
                               
                                $schoolInsert=array(
                                    'state_id'=>$state_id,
                                    'district_id'=>$district_id,
                                    'name'=>$schoolName?ucfirst($schoolName):"",
                                    'address'=>$schoolAddress?$schoolAddress:"",
                                    'logo'=>$new_img_name?$new_img_name:"",
                                    'created'=>$created,
                                );
                                $school_id=$this->Common_model->addEditRecords('school',$schoolInsert);
                                // school's grade inserted
                                $this->school_grades_insert($school_taught_from,$school_taught_to,$school_id,$district_id);
                                //Principal inserted
                                if((!empty($principalEmail))){
                                    if(!$this->Common_model->getRecords('users','user_id',array('is_deleted'=>0,'user_type'=>2,'email'=>$principalEmail),'user_id desc',true)){
                                
                                        $ph_no="";
                                        if(!empty($principalPhone)){
                                            $numbers_only = preg_replace("/[^\d]/", "", $principalPhone);
                                            $ph_no= preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $numbers_only);
                                        }

                                        $principal_pw = generateRandomString();
                                        $principal_data=array(
                                            'title'=>$pricipalTitle?$pricipalTitle:"",
                                            'first_name'=>$principalFirstName?ucfirst($principalFirstName):"",
                                            'last_name'=>$principalLastName?ucfirst($principalLastName):"",
                                            'email'=>$principalEmail?$principalEmail:"",
                                            'phone'=>$ph_no,
                                            'school_id'=>$school_id,
                                            'state_id'=>$state_id,
                                            'district_id'=>$district_id,
                                            'password'=>md5($principal_pw),
                                            'is_approve'=>2,
                                            'user_type'=>2,
                                            'approved_by'=>$user_id,
                                            'email_verified'=>1,
                                            'created'=>$created
                                        );
                                        $pricipal_id=$this->Common_model->addEditRecords('users',$principal_data);
                                        $user_number = str_pad($pricipal_id, 8, '0', STR_PAD_LEFT);
                                        $update_user = $this->Common_model->addEditRecords('users',array('user_number'=>$user_number),array('user_id'=>$pricipal_id));

                                        $emailArray = array(
                                            'email' => $principalEmail,
                                            'otp'   => $principal_pw,
                                            'recipient_name'=> ucfirst($principalFirstName).' '.ucfirst($principalLastName),
                                            'status'   => 0,
                                            'user_type'   => 2,
                                            'school_id'   => $school_id,
                                            'school_logo'   => $schoolLogoCron,
                                            'created'   => $created,
                                        );
                                        
                                        $this->Common_model->addEditRecords('email_otp_cron',$emailArray);
                                    }

                                    // $school_detail = $this->Common_model->getRecords('school','id,state_id,district_id',array('district_id'=>$district_id,'name'=>$schoolName),'',true);
                                    if(!empty($teacherEmail)){
                                        if(!$check_teacher_email = $this->Common_model->getRecords('users','email',array('is_deleted'=>0,'email'=>$teacherEmail),'user_id desc',true)) {
                                            $check_school_grade="";
                                            if(!empty($gradesAssign)){
                                                $r_trim=rtrim($gradesAssign,',');
                                                $l_trim=ltrim($r_trim,',');
                                                $gradesAssign=$l_trim;
                                                $teacher_grade= $this->teacher_grade_replace($gradesAssign);
                                                
                                                $where_teacher_grade='school_id ='.$school_id.' and grade IN ('.$teacher_grade.')';
                                                $check_school_grade = $this->Common_model->getRecords('school_grade','*',$where_teacher_grade,'',false);
                                            }

                                            $teacher_grade_data=array();        
                                            $teacher_pw=generateRandomString();
                                            $teacher_data=array(
                                                'first_name'=>$teacherFirstName?ucfirst($teacherFirstName):"",
                                                'last_name'=>$teacherLastName?ucfirst($teacherLastName):"",
                                                'email'=>$teacherEmail?$teacherEmail:"",
                                                'state_id'=>$state_id,
                                                'district_id'=>$district_id,
                                                'school_id'=>$school_id,
                                                'password'=>md5($teacher_pw),
                                                'user_type'=>3,
                                                'is_approve'=>2,
                                                'approved_by'=>$user_id,
                                                'email_verified'=>1,
                                                'created'=>$created
                                                
                                            );
                                            if($teacher_id=$this->Common_model->addEditRecords('users',$teacher_data)){
                                                $user_number = str_pad($teacher_id, 8, '0', STR_PAD_LEFT);
                                                $update_user = $this->Common_model->addEditRecords('users',array('user_number'=>$user_number),array('user_id'=>$teacher_id));
                                                if(isset($check_school_grade) && !empty($check_school_grade)){
                                                    foreach ($check_school_grade as $key => $teacher_grade) {
                                                        $teacher_grade_data[]=array(
                                                            'teacher_id'=>$teacher_id,
                                                            'district_id'=>$teacher_grade['district_id'],
                                                            'school_id'=>$teacher_grade['school_id'],
                                                            'grade'=>$teacher_grade['grade'],
                                                            'grade_display_name'=>DISPLAY_GRADE_NAME[$teacher_grade['grade']],
                                                        );
                                                    }
                                                }
                                            }
                                            if (!empty($teacher_grade_data)) {
                                                $this->db->insert_batch('teacher_grade', $teacher_grade_data);
                                            }

                                            $teacherEmailArray = array(
                                                'email' => $teacherEmail,
                                                'otp'   => $teacher_pw,
                                                'recipient_name'=> ucfirst($teacherFirstName).' '.ucfirst($teacherLastName),
                                                'status'   => 0,
                                                'user_type'   => 3,                                        
                                                'created'   => $created,
                                            );
                                            
                                            $this->Common_model->addEditRecords('email_otp_cron',$teacherEmailArray);
                                        }
                                    }
                                }

                            }else{
                                //For teacher inseration , if same csv uploaded for other only teache so this function used.
                                $pricipal_id="";
                                $school_id=$check_school['id'];
                                if($check_principal = $this->Common_model->getRecords('users','user_id',array('school_id'=>$check_school['id'],'user_type'=>2,'is_deleted'=>0),'',true)) 
                                {
                                    $pricipal_id=$check_principal['user_id'];
                                }else{

                                    if((!empty($principalEmail))){
                                        if(!$this->Common_model->getRecords('users','user_id',array('is_deleted'=>0,'user_type'=>2,'email'=>$principalEmail),'user_id desc',true)){
                                            $ph_no="";
                                            if(!empty($principalPhone)){
                                                $numbers_only = preg_replace("/[^\d]/", "", $principalPhone);
                                                $ph_no= preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $numbers_only);
                                            }

                                            $principal_pw = generateRandomString();
                                            $principal_data=array(
                                                'title'=>$pricipalTitle?$pricipalTitle:"",
                                                'first_name'=>$principalFirstName?ucfirst($principalFirstName):"",
                                                'last_name'=>$principalLastName?ucfirst($principalLastName):"",
                                                'email'=>$principalEmail?$principalEmail:"",
                                                'phone'=>$ph_no,
                                                'school_id'=>$school_id,
                                                'state_id'=>$state_id,
                                                'district_id'=>$district_id,
                                                'password'=>md5($principal_pw),
                                                'is_approve'=>2,
                                                'user_type'=>2,
                                                'approved_by'=>$user_id,
                                                'email_verified'=>1,
                                                'created'=>$created
                                            );
                                            $pricipal_id=$this->Common_model->addEditRecords('users',$principal_data);
                                            $user_number = str_pad($pricipal_id, 8, '0', STR_PAD_LEFT);
                                            $update_user = $this->Common_model->addEditRecords('users',array('user_number'=>$user_number),array('user_id'=>$pricipal_id));

                                            $emailArray = array(
                                                'email' => $principalEmail,
                                                'otp'   => $principal_pw,
                                                'recipient_name'=> ucfirst($principalFirstName).' '.ucfirst($principalLastName),
                                                'status'   => 0,
                                                'user_type'   => 2,
                                                'school_id'   => $school_id,
                                                'school_logo'   => $schoolLogoCron,
                                                'created'   => $created,
                                            );
                                            
                                            $this->Common_model->addEditRecords('email_otp_cron',$emailArray);
                                        }
                                    }
                                }
                                if(!empty($pricipal_id)){
                                    if(!empty($teacherEmail)){
                                        if(!$check_teacher_email = $this->Common_model->getRecords('users','email',array('is_deleted'=>0,'email'=>$teacherEmail),'user_id desc',true)) {
                                            if(!empty($gradesAssign)){
                                                $r_trim=rtrim($gradesAssign,',');
                                                $l_trim=ltrim($r_trim,',');
                                                $gradesAssign=$l_trim;
                                                $teacher_grade= $this->teacher_grade_replace($gradesAssign);
                                                
                                                $where_teacher_grade='school_id ='.$school_id.' and grade IN ('.$teacher_grade.')';
                                                $check_school_grade = $this->Common_model->getRecords('school_grade','*',$where_teacher_grade,'',false);
                                            }

                                            $teacher_grade_data=array();        
                                            $teacher_pw=generateRandomString();
                                            $teacher_data=array(
                                                'first_name'=>$teacherFirstName?ucfirst($teacherFirstName):"",
                                                'last_name'=>$teacherLastName?ucfirst($teacherLastName):"",
                                                'email'=>$teacherEmail?$teacherEmail:"",
                                                'state_id'=>$state_id,
                                                'district_id'=>$district_id,
                                                'school_id'=>$school_id,
                                                'password'=>md5($teacher_pw),
                                                'user_type'=>3,
                                                'is_approve'=>2,
                                                'approved_by'=>$user_id,
                                                'email_verified'=>1,
                                                'created'=>$created
                                                
                                            );
                                            if($teacher_id=$this->Common_model->addEditRecords('users',$teacher_data)){
                                                $user_number = str_pad($teacher_id, 8, '0', STR_PAD_LEFT);
                                                $update_user = $this->Common_model->addEditRecords('users',array('user_number'=>$user_number),array('user_id'=>$teacher_id));
                                                if(isset($check_school_grade) && !empty($check_school_grade)){
                                                    foreach ($check_school_grade as $key => $teacher_grade) {
                                                        $teacher_grade_data[]=array(
                                                            'teacher_id'=>$teacher_id,
                                                            'district_id'=>$teacher_grade['district_id'],
                                                            'school_id'=>$teacher_grade['school_id'],
                                                            'grade'=>$teacher_grade['grade'],
                                                            'grade_display_name'=>DISPLAY_GRADE_NAME[$teacher_grade['grade']],
                                                        );
                                                    }
                                                }
                                            }
                                            if (!empty($teacher_grade_data)) {
                                                $this->db->insert_batch('teacher_grade', $teacher_grade_data);
                                            }

                                            $teacherEmailArray = array(
                                                'email' => $teacherEmail,
                                                'otp'   => $teacher_pw,
                                                'recipient_name'=> ucfirst($teacherFirstName).' '.ucfirst($teacherLastName),
                                                'status'   => 0,
                                                'user_type'   => 3,
                                                'created'   => $created,
                                            );
                                            
                                            $this->Common_model->addEditRecords('email_otp_cron',$teacherEmailArray);
                                        }
                                    }
                                }
                            }                            
                        }
                    }

                    $insert_array=array(
                        'district_id'=>$district_id,
                        'user_id'=>$user_id,
                        'state_id'=>$state_id,
                        'file_name'=>$inputFileName,
                        'status'=>1,
                        'created'=>date('Y-m-d H:i:s')
                    );  
                    if(!$id = $this->Common_model->addEditRecords('csv_uploads',$insert_array)) {
                       $this->session->set_flashdata('error', 'The input sheet has some errors, please fix and upload.');
                       redirect('district/upload-csv');
                    }else{
                        $this->session->set_flashdata('success', 'File successfully uploaded');
                        redirect('district/upload-csv');
                    }
                }
               

            }else{
                $this->session->set_flashdata('error', 'Upload a file');
                redirect('district/upload-csv');
            }
        }else{
            $this->session->set_flashdata('error', 'Upload a file');
            redirect('district/upload-csv');
        }
        
    }


    /*-----------District Request--------------------*/

    public function district_request(){
        
        check_web_user_status();
        $data['title']= 'District' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'District'." | ".SITE_TITLE;
        $data['meta_desc']= 'District'." | ".SITE_TITLE;
        $data['page_title'] = "District";    
        $this->load->view('front/include/header',$data);
        $this->load->view('front/district/district_request');
        $this->load->view('front/include/footer');
    }

    /*-----------Approved District Request--------------------*/

    public function approved_school_request(){
        check_web_user_status();
        $data['title']= 'District' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'District'." | ".SITE_TITLE;
        $data['meta_desc']= 'District'." | ".SITE_TITLE;
        $data['page_title'] = "District";

        $districtId = $this->session->userdata('district_id');
        $data['school_list'] = $this->District_model->getSchoolList($districtId,2);
        $this->load->view('front/include/header',$data);
        $this->load->view('front/district/school_approved');
        $this->load->view('front/include/footer');
    }

    /*------------- Approved school send message -------------*/

    public function approved_school_sendmsg(){        
        check_web_user_status();
        if(!$this->input->post('school_data')){
            display_output('0',"Select the recipients");
        }

        $this->session->unset_userdata('broadcast_email'); 
        $broadcast_email = $this->input->post('school_data');


        $data['title']= 'District' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'District'." | ".SITE_TITLE;
        $data['meta_desc']= 'District'." | ".SITE_TITLE;
        $data['page_title'] = "District";
        $this->session->set_userdata('broadcast_email', $broadcast_email);

        if($this->session->userdata('broadcast_email')){
            display_output('1','School approved Email.',array('details'=>$broadcast_email));
        }else{
            display_output('0',"Select the recipients");
        }
    }

    public function school_broadcast_page(){

        // $this->session->unset_userdata('broadcast_email');
        check_web_user_status();
        $broadcast_email = $this->session->userdata('broadcast_email');

        $districtId = $this->session->userdata('district_id');
        
            $data['user_info'] = "";
            $email_list = [];
            $broad_email = "";
            if($broadcast_email && count($broadcast_email)>0){
                foreach($broadcast_email as $list){
                    array_push($email_list,$list['email'] ) ;
                }
                $broad_email = implode(',', $email_list);
                $data['user_info'] = $broadcast_email;
            }       
            $data['Tolist'] = $this->District_model->getSchoolList($districtId,2,1);      
            
            $data['title']= 'Broadcast' . " | ".SITE_TITLE;
            $data['meta_keywords']= 'Broadcast'." | ".SITE_TITLE;
            $data['meta_desc']= 'Broadcast'." | ".SITE_TITLE;
            $data['page_title'] = "Broadcast";
            $data['broad_email'] = $broad_email;

          

            $this->load->view('front/include/header',$data); 
            $this->load->view('front/district/school_send_broadcasr_msg');
            $this->load->view('front/include/footer');
            
    }

    public function broadcast_messages(){

        check_web_user_status();
        $data['title']= 'District' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'District'." | ".SITE_TITLE;
        $data['meta_desc']= 'District'." | ".SITE_TITLE;
        $data['page_title'] = "District";
        unset($_SESSION['broadcast_email']);
        $this->load->view('front/include/header',$data); 
        $this->load->view('front/district/broadcast_message');
        $this->load->view('front/include/footer');
    }

    public function school_videos(){

        check_web_user_status();
        $data['title']= 'District' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'District'." | ".SITE_TITLE;
        $data['meta_desc']= 'District'." | ".SITE_TITLE;
        $data['page_title'] = "District";
        $districtId = $this->session->userdata('district_id');
        $data['school_list'] =  $this->Auth_model->getSchoolList($districtId);
        $data['grade']=SCHOOL_GRADE;
       // print_r(SCHOOL_GRADE);die;
        $this->load->view('front/include/header',$data); 
        $this->load->view('front/district/school_videos');
        $this->load->view('front/include/footer');

    }

    public function video_fulldetail($media_slug){

        check_web_user_status();
        $data['title']= 'Video Detail' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Video Detail'." | ".SITE_TITLE;
        $data['meta_desc']= 'Video Detail'." | ".SITE_TITLE;
        $data['page_title'] = "Video Detail";

        $res = $this->District_model->getDetailBySlug($media_slug);

        $data['media_id'] = $res['id'];
        $data['grade_n'] = $res['grade'];
        $this->load->view('front/include/header',$data); 
        $this->load->view('front/district/video_fulldetail');
        $this->load->view('front/include/footer');

    }


    /*public function ranges($grade){
        switch ($grade) {
            case 'Kindergarten':
            case 'K':
            case 'k':
                return 0;
            break;
            case '1st Grade':
            case '1st grade':
            case '1':
                return 1;
            break;
            case '2nd Grade':
            case '2nd grade':
            case '2':
                return 2;
            break;
            case '3rd Grade':
            case '3rd grade':
            case '3':
                return 3;
            break;
            case '4th Grade':
            case '4th grade':
            case '4':
                return 4;
            break;
            case '5th Grade':
            case '5th grade':
            case '5':
                return 5;
            break;
            case '6th Grade':
            case '6th grade':
            case '6':
                return 6;
            break;
            case '7th Grade':
            case '7th grade':
            case '7':
                return 7;
            break;
            case '8th Grade':
            case '8th grade':
            case '8':
                return 8;
            break;
            case '9th Grade':
            case '9th grade':
            case '9':
                return 9;
            break;
            case '10th Grade':
            case '10th grade':
            case '10':
                return 10;
            break;
            case '11th Grade':
            case '11th grade':
            case '11':
                return 11;
            break;
            case '12th Grade':
            case '12th grade':
            case '12':
                return 12;
            break;

        }
    }*/

    public function school_grades_insert($school_taught_from,$school_taught_to,$school_id,$district_id){
        $school_grades = range($school_taught_from,$school_taught_to);
        $school_grade_data=array();
        if(!empty($school_grades)){
            foreach ($school_grades as $key => $grades) {
                $school_grade_data[]=array(
                    'school_id'=>$school_id,
                    'district_id'=>$district_id,
                    'grade'=>$grades,
                    'grade_display_name'=>DISPLAY_GRADE_NAME[$grades],
                );
            }
        }
        $this->db->insert_batch('school_grade', $school_grade_data);
    }

    public function logo_upload_by_url($url){
        
       
        $new_img_name="";
        /*$extension = pathinfo($url, PATHINFO_EXTENSION);
        if(!empty($extension)){
            $extensionArr = array('jpg','jpeg','png','JPG','JPEG','PNG');
            if (in_array($extension, $extensionArr)) {
                $new_img_name = SCHOOL_LOGO_PATH.uniqid(time()).rand().'.png';
                $ch = curl_init($url);
                $destinationPath = $new_img_name;
                $fp = fopen($destinationPath, 'wb');
                curl_setopt($ch, CURLOPT_FILE, $fp);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_exec($ch);
                if(curl_exec($ch) === false)
                {
                    
                    return  'error: ' . curl_error($ch);
                }
                curl_close($ch);
                fclose($fp);
            }
        }*/
        return $new_img_name;
    }    

    public function teacher_grade_replace($grade_str){
        $teacher_grades="";
        $grade_pc=explode(',', $grade_str);
        foreach ($grade_pc as $key => $pc) {
            $teacher_grades.=$this->Common_model->ranges(trim($pc));
            $teacher_grades.=',';
        }
        return  rtrim($teacher_grades,',');
    }    






    function isInArray($needle, $haystack) 
    {
        foreach ($needle as $stack) {
            if (!in_array($stack, $haystack)) {
                return false;
            }
        }
        return true;
    }

    public function test()
    {
        $people = array(
            '0', 
            '1', 
            '2', 
            '3', 
            '4',
            '5',
            '6'
        );

        $exclude = array(
            '8',
            '9',
            '10'
        );

        if($this->isInArray($exclude, $people) == false){
            echo 'no exclusions';
        } else {
            echo 'people from excludes are in the array $people';
        }
    }









} //End controller class
