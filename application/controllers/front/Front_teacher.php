<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Front_teacher extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model(array('Common_model','Auth_model','Teacher_model','Parent_student_model')); 
        $this->load->helper(array('email','common_helper'));
       $this->load->library(array('Ajax_pagination','PHPExcel'));
      
    }

/*======================Edit Profile================ */
    public function profile() 
    {
        check_web_user_status(1);
        $data['title']= 'Profile' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Profile'." | ".SITE_TITLE;
        $data['meta_desc']= 'Profile'." | ".SITE_TITLE;
        $data['page_title'] = "Profile";
        $user_id=$this->session->userdata('user_id');
        $teacher_grade_details =$this->Common_model->getRecords('teacher_grade', 'grade,grade_display_name', array('teacher_id'=>$user_id), '', false);
            $data['teacher_grade']=$teacher_grade_details;

        // echo '<pre>';
        // print_r($data); die;
        
        $this->load->view('front/include/header',$data);
        $this->load->view('front/teacher/profile');
        $this->load->view('front/include/footer');
    } 
    

    public function profile_update() {
      
        $postData = $this->input->post();
        //echo "<pre>";print_r($postData);die;
        if($this->input->post('type')=='web'){
            $user_id=$this->session->userdata('user_id');
            $user_type=$this->session->userdata('front_user_type');
        }else{
            $user_id        =   test_input($this->input->post('user_id'));
            $user_type      =   test_input($this->input->post('user_type'));
        }
        
        $first_name       =   test_input($this->input->post('first_name'));
        $last_name        =   test_input($this->input->post('last_name'));
        $display_name     =   test_input($this->input->post('display_name'));
       

        if(empty($user_type)) {
            display_output('0','Please enter user type.');
        }if(empty($first_name)) {
            display_output('0','Please enter first name.');
        }if(empty($last_name)) {
            display_output('0','Please enter last name.');
        }
        if($user_type==2 || $user_type==3){
            if(empty($display_name)) {
                display_output('0','Please enter display name.');
            }
        }
        $update_data = array();
        $update_school_data  = array();
        /*************profile pic upload ************/
        $imgfilepath="";
        $imgfilerror="";
        if(isset($_FILES['profile_pic']) && !empty($_FILES['profile_pic']['name'])){
            if($_FILES['profile_pic']['error']==0) {
                $image_path = USER_PROFILE_PATH;
                $allowed_types = '*';
                $file='profile_pic';
                $height = '';
                $width = '';
                $imgresponce = commonImageUpload($image_path,$allowed_types,$file,$width,$height);

                if($imgresponce['status']==0){
                    $upload_error = $imgresponce['msg'];   
                    $imgfilerror="1";
                } else {
                    $imgfilepath=$imgresponce['image_path'];
                }
            }
        }

        $update_data=array(
            'first_name'=>$first_name,
            'last_name'=>$last_name,
            'display_name'=>$display_name,
            'modified'=>date('Y-m-d H:i:s'),
        );
        if(!empty($imgfilepath)){
            $update_data['profile_image'] = $imgfilepath;
        }
       
        $where= array("user_id"=>$user_id);
        if($this->Common_model->addEditRecords('users',$update_data,$where)){
            $school_id = $this->Common_model->getFieldValue('users','school_id',array("user_id"=>$user_id));
            $where_school= array("id"=>$school_id);
            $update_school_data=array(
                'name'=>$school_name,
                'address'=>$address,
            );
            if(!empty($imglogopath)){
                $update_school_data['logo'] = $imglogopath;
            }
            $this->Common_model->addEditRecords('school',$update_school_data,$where_school);
            $details=$this->Auth_model->getUserDetail($user_id,$user_type);
            $details['mobile_auth_token']=$this->Common_model->getFieldValue('users','mobile_auth_token',$where);
            display_output('1','Profile successfully updated',array('details'=>$details));
        }else{
            display_output('0','Check your internet connection and try again.'); 
        }
    }


     /*-----------Teacher  Request--------------------*/

    public function student_request(){
        check_web_user_status();
        $data['title']= 'Teacher' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Teacher'." | ".SITE_TITLE;
        $data['meta_desc']= 'Teacher'." | ".SITE_TITLE;
        $data['page_title'] = "Teacher";   
        $user_id =  $this->session->userdata('user_id');
        $school_id = $this->session->userdata('school_id');
        $offset = 0;
        $per_page = APP_PAGE_LIMIT;
        $total_records  = $this->Teacher_model->getStudentListWeb($user_id,$school_id,1,0,0);
        
        $b_url = base_url('student-list-request-data');
        $selector = '#ajax_data';
        $function = "filter_records";

        $config = configFrontAjaxPagination($total_records,$b_url,$selector,$per_page,$function,2);
        $this->ajax_pagination->initialize($config);

        $data['student_list'] = $this->Teacher_model->getStudentListWeb($user_id,$school_id,1,$per_page,$offset);

        $data['offset'] = $offset;
        $this->load->view('front/include/header',$data);
        $this->load->view('front/teacher/request');
        $this->load->view('front/include/footer');
    }

    function student_list_request_data()
    {
        $page = $this->uri->segment(2);
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }
        $school_id = $this->session->userdata('school_id');
        $user_id =  $this->session->userdata('user_id');
        $per_page = APP_PAGE_LIMIT;
        $total_records  = $this->Teacher_model->getStudentListWeb($user_id,$school_id,1,0,0);

        $b_url = base_url('student-list-request-data');
        $selector = '#ajax_data';
        $function = "filter_records";

        //pagination configuration
        $config = configFrontAjaxPagination($total_records,$b_url,$selector,$per_page,$function);
        $this->ajax_pagination->initialize($config);
        //get the posts data
        $data['offset'] = $offset;
        $data['student_list'] = $this->Teacher_model->getStudentListWeb($user_id,$school_id,1,$per_page,$offset);
        if(!empty($data['student_list'])) {
            $this->load->view('front/teacher/request_list_data', $data);
        }
    }


    public function grade_wise_video($grade){
        
        check_web_user_status();
        $data['title']= 'Teacher' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Teacher'." | ".SITE_TITLE;
        $data['meta_desc']= 'Teacher'." | ".SITE_TITLE;
        $data['page_title'] = "Teacher";
        $user_id=$this->session->userdata('user_id');
        $user_type=$this->session->userdata('front_user_type');
        $school_id=$this->session->userdata('school_id');
        
        $teacher_grade_details =$this->Common_model->getRecords('teacher_grade', 'grade,grade_display_name', array('teacher_id'=>$user_id), '', false);
            $data['teacher_grade']=$teacher_grade_details;

        $teacher_details =$this->Common_model->getRecords('users', 'first_name,last_name,display_name', array('user_id'=>$user_id), '', false);
        $data['teacher_detail']=$teacher_details;

        $teacher_id = $user_id;
        if($video_list = $this->Teacher_model->getAllVideos($user_id,$user_type,$school_id,$teacher_id,$grade)){
            for($k=0; $k<count($video_list); $k++){
                $video_list[$k]['created'] = convertGMTToLocalTimezone($video_list[$k]['created'], false, false);
            }
            $data['teacher_video_list'] = $video_list;
        }
        $data['grade'] =$grade;
        $data['grade_name'] = DISPLAY_GRADE_NAME[$grade];
        // echo '<pre>';
        // print_r($data); die;
        
        $this->load->view('front/include/header',$data);
        $this->load->view('front/teacher/teacher_video_detail');
        $this->load->view('front/include/footer');

    }

    /*======================teacher upload video================ */
    public function teacher_upload_video($grade,$id="",$cParentId="") 
    {  
        // phpinfo();die;
        check_web_user_status();
      
        $data['title']= 'Upload Video' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Upload Video'." | ".SITE_TITLE;
        $data['meta_desc']= 'Upload Video'." | ".SITE_TITLE;
        $data['page_title'] = "Upload Video";
        $data['form_action'] = "api/teacher-upload-video";
        $id=$this->uri->segment(3);
        $cParentId=$this->uri->segment(4);
        if(isset($id) && !empty($id)){
            $data['created_as']=2;
            $data['media_id']=$id;
            $data['media_slug'] = $this->db->get_where('media',array('id'=>$id))->row()->media_slug;
        }else{
            $data['created_as']=1;
            $data['media_id']="";
            $data['media_slug']="";
        }
        if(isset($cParentId) && !empty($cParentId)){
            $data['comment_parent_id']=$cParentId;
        }else{
            $data['comment_parent_id']="";
        }
        $data['grade']=$grade;
        $data['grade_n']=$grade;
        if(isset($_POST['data'])){
           $data['url']= $_POST['data'];
        }
         
      
        if(isset($data['media_slug']) && isset($id)){
          $data['back_url']='video-fulldetail/'.$data['media_slug'];
        }else{
          $urlid = $this->uri->segment(2);  
          $data['gradename'] = SCHOOL_GRADE[$urlid]['grade_display_name'];
          $data['back_url']='grade-wise-video/'.$urlid;
          // die;
        }
         
        $this->load->view('front/include/header',$data);
        $this->load->view('front/teacher/upload-video');
        $this->load->view('front/include/footer');
    }
    /*======================teacher upload video================ */
    public function teacher_edit_video($media_slug="") 
    {
        check_web_user_status();
        $data['title']= 'Edit Video' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Edit Video'." | ".SITE_TITLE;
        $data['meta_desc']= 'Edit Video'." | ".SITE_TITLE;
        $data['page_title'] = "Edit Video";
        $data['form_action'] = "api/teacher-update-video-content";
        $user_id=$this->session->userdata('user_id');
        if(!$data['media'] =$this->Common_model->getRecords('media', 'thumb_image,id,media_name,title,description,grade,large_image,media_slug', array('media_slug'=>$media_slug,'user_id'=>$user_id), '', true)){
            redirect('pages/page_not_found');
        }
        $data['backurl']=site_url('grade-wise-video/'.$data['media']['grade']);
        $this->load->view('front/include/header',$data);
        $this->load->view('front/teacher/edit_video');
        $this->load->view('front/include/footer');
    }

      public function home()  {

        check_web_user_status();
        $data['title']= 'Home' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Home'." | ".SITE_TITLE;
        $data['meta_desc']= 'Home'." | ".SITE_TITLE;
        $data['page_title'] = "Home";
    
        $user_id=$this->session->userdata('user_id');
        $user_type=$this->session->userdata('front_user_type');
        $school_id=$this->session->userdata('school_id');
            if($user_id) {
              
                $data['school_video']=$this->Parent_student_model->getSchoolVideo($school_id);
               
                    if(!empty($school_id)){
                        $data['recently_video'] = $this->Teacher_model->getVideosHome($user_id,"","","","");
                         $data['video_count'] = $this->Teacher_model->getAllVideos($user_id,"","","","");
                    }else{
                        $data['recently_video']=array();
                    }
                
                    $data['badge'] = '0';
                    $data['notification_flag'] = '0';
                    if($res = $this->Common_model->getRecords('users', 'unread_notification,notification_flag', array('user_id'=>$user_id), '', true)) {
                        $data['badge'] = (string)$res['unread_notification'];
                        $data['notification_flag'] = (string)$res['notification_flag'];
                    }
                $data['school'] = $this->db->get_where('school',array('id'=>$school_id))->row();
                //print_r($data);die;
                $this->load->view('front/include/header',$data);
                $this->load->view('front/teacher/index');
                $this->load->view('front/include/footer');
               
            }
       
    } 

     public function recently_video(){
        check_web_user_status();
        $data['title']= 'Video' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Video'." | ".SITE_TITLE;
        $data['meta_desc']= 'Video'." | ".SITE_TITLE;
        $data['page_title'] = "Video";

        $user_id=$this->session->userdata('user_id');
        $user_type=$this->session->userdata('front_user_type');
        $school_id=$this->session->userdata('school_id');

        $data['recently_video'] = $this->Teacher_model->getAllVideos($user_id,"","","","");
         
        $this->load->view('front/include/header',$data);
        $this->load->view('front/teacher/recently');
        $this->load->view('front/include/footer');
    }
   
    public function chatView(){

        check_web_user_status();
        $data['title']= 'Chat' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Chat'." | ".SITE_TITLE;
        $data['meta_desc']= 'Chat'." | ".SITE_TITLE;
        $data['page_title'] = "Chat";

        $this->load->view('front/include/header',$data);
        // $this->load->view('front/include/sidebar');    
        $this->load->view('front/teacher/chat_view');
        $this->load->view('front/include/footer');
    }



} //End controller class
