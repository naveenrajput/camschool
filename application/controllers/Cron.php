<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron extends CI_Controller {
	public function __construct()
	{
		parent:: __construct();
		$this->load->model(array('Common_model'));
		$this->load->helper('common_helper');
		$this->load->library(array('PHPExcel','S3'));
		ini_set('max_execution_time', 30000);
	}
	/********All the request notification , broadcast msg shoot Cron time per 2 min*******/
	public function send_pending_email()
    {
    	
        try{

        	if($email_data =$this->Common_model->getEmailNotificationList('email_notification', '*', array('status'=>0), '', false)){
	        	
	        	$fromEmail = getAdminEmail(); 
	        	foreach ($email_data as $key => $detail) {
	        		$this->Common_model->addEditRecords('email_notification',array('status'=>1),array('id'=>$detail['id']));
	        		$type=$detail['type'];
	        		$toEmail = $detail['email']; 
	        		$data['name']= $detail['recipient_name'];
  
	        		switch ($type) {
			            case 'Distrct_approved_school': //when district approve school, school receive email
					       
					        // $subject = "Your ".WEBSITE_NAME. " credentials are now active";
					        $subject = "Your Chat At Me! Schools Credentials are now active";
					        $data['message']="Your district has activated your ".WEBSITE_NAME." credentials. Please use the email address and password you used when you signed up to access your account. <a href=".base_url().'login'.">Click here</a> to login.";
					        // $data['sub_msg']="Please contact us if you have any questions.";
					        $templet = $this->load->view('template/principal_approved', $data,TRUE);
					        $find =array(
					          '{{name}}','{{verify_url}}','{{email}}'
					        );

					        $replace = array(
					          'name' => $data['name'],
					          'link'  => WEBSITE_NAME,
					          'email' =>$toEmail,
					        );
                       
                          	$body = str_replace($find, $replace, $templet);
					        $this->Common_model->sendEmail($toEmail,$subject,$body,$fromEmail);
					       
			                break; 
			            case 'School_approved_teacher': //when pricipal approve teacher, teacher receive email
					        // $subject = "Your ".WEBSITE_NAME." credentials are now active";
					        $subject = "Your Chat At Me! Schools Credentials are now active";
					        $data['message']="Your school has activated your ".WEBSITE_NAME." credentials. Please use the email address and password you used when you signed up to access your account. <a href=".base_url().'login'.">Click here</a> to login.";
					        // $data['sub_msg']="Please contact us if you have any questions.";
					        $body = $this->load->view('template/teacher_approved', $data,TRUE);

					        $this->Common_model->sendEmail($toEmail,$subject,$body,$fromEmail);
		            		
			                break; 
			            case 'Broadcast_notification': //broadcast message
			            	$user_id = $detail['user_id'];
					        $sender_id = $detail['sender_id'];
					        $fromEmail = $detail['sender_email']; 
					        $subject = $detail['subject'];
					        $data['message']= $detail['message'];
					        $data['sender_name']= $detail['sender_name']; 
					        $body = $this->load->view('template/broadcast_msg', $data,TRUE);
					        
                            if($detail['to_user_type']==2){
                              $broadcast_message="Your district admin sent you a message. Please check your email.";
                            }
                            else if($detail['to_user_type']==3){
                              $broadcast_message="Your principal sent you a message. Please check your email.";
                            }else{
                            	$broadcast_message="New broadcast message.";
                            }

                           //print_r($broadcast_message);die;
					        addNotification($sender_id,$user_id,'Broadcast Notification',$broadcast_message,$sender_id,0); 
					        sendNotification($detail['user_id'],$broadcast_message);
					        $this->Common_model->sendEmail($toEmail,$subject,$body,$fromEmail);
			                break;
		                case 'Teacher_approved_student': //broadcast message
					        $user_id = $detail['user_id'];
					        $sender_id = $detail['sender_id'];
					        $message = $detail['message'];
					        $subject = "Your teacher has accepted your request";
					        $data['message']=$message;
					        $data['sub_msg']="Please contact us if you have any questions.";
					        $body = $this->load->view('template/common', $data,TRUE);
                            addNotification($sender_id,$user_id,'Request Approved',$message,$sender_id,0); 
                            sendNotification($user_id,$message);
                            $this->Common_model->sendEmail($toEmail,$subject,$body,$fromEmail);
			                break;																			
		               	case 'Teacher_upload_video': //broadcast message
					        $user_id = $detail['user_id'];
					        $sender_id = $detail['sender_id'];
					        $message = $detail['message'];
					        $record_id = $detail['subject']?$detail['subject']:0;
					        $fromEmail = $detail['sender_email']; 
					        // $subject = 'Teacher uploaded video';
					        $subject = 'A new video was uploaded to the class page';
					        $data['message']=$message;
					        $data['sender_name']= $detail['sender_name']; 
					        $body = $this->load->view('template/common', $data,TRUE);
                            addNotification($sender_id,$user_id,'Teacher Upload Media',$message,$record_id,$record_id,'video'); 
                            sendNotification($user_id,$message,'video');
                            
                            $this->Common_model->sendEmail($toEmail,$subject,$body,$fromEmail);
			                break;
		            }
		           
	        	}
	            
	        }
   
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
    }
    /********After every 14 days media deleted Cron time per day******/
	public function teacher_video_delete()
    {
        try{
        	$today =date("Y-m-d");
        	$end_date=date("Y-m-d",strtotime('-14 days'));

        	$media_detail = $this->Common_model->getRecords('media','id,media_name,thumb_image,large_image',array("date(created) <"=>$end_date,'is_deleted'=>0),'',false);
        	if(!empty($media_detail)){
        		$s3 = new S3(AMAZON_ID, AMAZON_KEY); 
        		foreach($media_detail as $media_data){

        			if($media_data['thumb_image']!='') {
		                $thumb_image = getDeletePath($media_data['thumb_image']);
		                $object = $s3->getObjectInfo(AMAZON_BUCKET,$thumb_image);
		                if($object) {
		                    $s3->deleteObject(AMAZON_BUCKET, $thumb_image);
		                } 
		            }
		           	if($media_data['large_image']!='') {
		                $large_image = getDeletePath($media_data['large_image']);
		                $object = $s3->getObjectInfo(AMAZON_BUCKET,$large_image );
		                if($object) {
		                    $s3->deleteObject(AMAZON_BUCKET, $large_image);
		                } 
		            }
		            if($media_data['media_name']!='') {
		                $media_name = getDeletePath($media_data['media_name']);
		                $object = $s3->getObjectInfo(AMAZON_BUCKET,$media_name );
		                if($object) {
		                    $s3->deleteObject(AMAZON_BUCKET, $media_name);
		                }
		            }
		            $update_data = array(
		                'is_deleted' => 1,
		                'deleted_by' => 1,
		                'deleted_date' => date("Y-m-d H:i:s"),
		                'modfied' => date("Y-m-d H:i:s")
		            );
		            $media_id = $this->Common_model->addEditRecords('media',$update_data,array('id'=>$media_data['id']));
        		}
        		/*$writetxtlog  = date("Y-m-d H:i:s")." : ".$detail['file_name']." ". PHP_EOL;
                		file_put_contents('logs/teacher video deleted'.date("Y-m-d").'.txt', $writetxtlog, FILE_APPEND);*/
        		
        	}
            
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
    }
    /*********************CSV upload***********************/
    
	/********Send OneTime Password Email , email shoot Cron time per 5 min*******/
	public function send_otp_email()
    {
        try{

        	if($email_data = $this->Common_model->getRecords('email_otp_cron', '*', array('status'=>0), '', false)){
	        	$fromEmail = getAdminEmail(); 
	        	foreach ($email_data as $key => $detail) {
	        		$this->Common_model->addEditRecords('email_otp_cron',array('status'=>1),array('id'=>$detail['id']));
	        		$schoolId = $detail['school_id']; 
	        		$schoolLogo = $detail['school_logo']; 
	        		$userType = $detail['user_type']; 

	        		if ($userType==2) {
	        			if (!empty($schoolLogo)) {

                            $extension = pathinfo($schoolLogo, PATHINFO_EXTENSION);
                            if(!empty($extension)){
                                $extensionArr = array('jpg','jpeg','png','JPG','JPEG','PNG');
                                if (in_array($extension, $extensionArr)) {
                                    $newImgName = $this->logo_upload_by_url($schoolLogo);
                                    if (!empty($newImgName)) {
						        		$this->Common_model->addEditRecords('school',array('logo'=>$newImgName),array('id'=>$schoolId));
                                    }
                                }
                            }
	        			}
	        		}

	        		$toEmail = $detail['email']; 
	        		$otp = $detail['otp']; 
                    $recipient_name = $detail['recipient_name'];
                    if ($userType==2) {
                    	$data['name'] = ucfirst($recipient_name); 
                    	$templet = $this->load->view('template/principal_activate_account', $data,TRUE);
				        $find =array(
				          '{{name}}','{{link}}','{{email}}','{{password}}'
				        );

				        $replace = array(
				          'name' => $recipient_name,
				          'link'  => WEBSITE_NAME,
				          'password' =>$otp,
				          'email' =>$toEmail,
				        );
                   		$subject = 'Your registration for Chat At Me! Schools';
                      	$body = str_replace($find, $replace, $templet);
                      	// print_r($body);die;
                    }else{
                    	$subject = 'Registration Completed '.WEBSITE_EMAIL_NAME;

                    	$data['message'] = "You are registered with us. Your email address is ".$toEmail." and one time password is ".$otp." For login <a href=".base_url().'login'.">Click here</a>";
                    	$data['name'] = $recipient_name;
                    	$body = $this->load->view('template/common', $data,TRUE);
                	}
                    
                    if ($this->Common_model->sendEmail($toEmail,$subject,$body,$fromEmail)) {
		        		$this->Common_model->deleteRecords('email_otp_cron',array('id'=>$detail['id']));
                    }
	        	}
	            
	        }
   
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
    }    
   
    public function teacher_grade_replace($grade_str){
    	$teacher_grades="";
    	$grade_pc=explode(',', $grade_str);
    	foreach ($grade_pc as $key => $pc) {
    		$teacher_grades.=$this->Common_model->ranges(trim($pc));
    		$teacher_grades.=',';
    	}
    	return  rtrim($teacher_grades,',');
    }

    public function logo_upload_by_url($url){
        
        $new_img_name="";
        $extension = pathinfo($url, PATHINFO_EXTENSION);
        if(!empty($extension)){
            $extensionArr = array('jpg','jpeg','png','JPG','JPEG','PNG');
            if (in_array($extension, $extensionArr)) {
                $new_img_name = SCHOOL_LOGO_PATH.uniqid(time()).rand().'.png';
                $ch = curl_init($url);
                $destinationPath = $new_img_name;
                $fp = fopen($destinationPath, 'wb');
                curl_setopt($ch, CURLOPT_FILE, $fp);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_exec($ch);
                if(curl_exec($ch) === false)
                {
                    return  'error: ' . curl_error($ch);
                }
                curl_close($ch);
                fclose($fp);
            }
        }
        return $new_img_name;
    }    

    /*public function ranges($grade){
    	switch ($grade) {
			case 'Kindergarten':
			case 'K':
			case 'k':
				return 0;
			break;
			case '1st Grade':
			case '1st grade':
			case '1':
				return 1;
			break;
			case '2nd Grade':
			case '2nd grade':
			case '2':
				return 2;
			break;
			case '3rd Grade':
			case '3rd grade':
			case '3':
				return 3;
			break;
			case '4th Grade':
			case '4th grade':
			case '4':
				return 4;
			break;
			case '5th Grade':
			case '5th grade':
			case '5':
				return 5;
			break;
			case '6th Grade':
			case '6th grade':
			case '6':
				return 6;
			break;
			case '7th Grade':
			case '7th grade':
			case '7':
				return 7;
			break;
			case '8th Grade':
			case '8th grade':
			case '8':
				return 8;
			break;
			case '9th Grade':
			case '9th grade':
			case '9':
				return 9;
			break;
			case '10th Grade':
			case '10th grade':
			case '10':
				return 10;
			break;
			case '11th Grade':
			case '11th grade':
			case '11':
				return 11;
			break;
			case '12th Grade':
			case '12th grade':
			case '12':
				return 12;
			break;

		}
    }*/

    public function school_grades_insert($school_taught_from,$school_taught_to,$school_id,$district_id){
    	$school_grades = range($school_taught_from,$school_taught_to);
    	$school_grade_data=array();
		if(!empty($school_grades)){
			foreach ($school_grades as $key => $grades) {
				$school_grade_data[]=array(
					'school_id'=>$school_id,
					'district_id'=>$district_id,
					'grade'=>$grades,
					'grade_display_name'=>DISPLAY_GRADE_NAME[$grades],
				);
			}
		}
		$this->db->insert_batch('school_grade', $school_grade_data);
    }
   
}
