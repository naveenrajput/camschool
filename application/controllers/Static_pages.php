<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Static_pages extends CI_Controller {

	public function __construct() {
		parent::__construct();
		error_reporting(0);
		$this->data ='';
		//$this->load->model('admin/Common_model');
		$this->load->model(array('Common_model','Auth_model'));
		$this->load->helper(array('common_helper'));
	}
	
	// App Pages
	public function terms_and_conditions() {  
		$data['details'] = $this->Common_model->getRecords('pages','*',array('page_id'=>1),'',true); 
		$data['details']['title']="";
		$this->load->view('front/terms_and_conditions',$data); 
	}	
	public function about_us() {  
		$data['details'] = $this->Common_model->getRecords('pages','*',array('page_id'=>3),'',true); 
		$data['details']['title']="";
		$this->load->view('front/about_us',$data); 
	}
	
	public function privacy_policy() {  
		$data['details'] = $this->Common_model->getRecords('pages','*',array('page_id'=>2),'',true); 
		$data['details']['title']="";
		$this->load->view('front/privacy_policy',$data); 
	}
	public function chat_at_me_school_info_district() {  
		$data['details'] = $this->Common_model->getRecords('pages','*',array('page_id'=>4),'',true); 
		$data['details']['title']="";
		$this->load->view('front/chat_at_me_info',$data); 
	}public function chat_at_me_school_info_school() {  
		$data['details'] = $this->Common_model->getRecords('pages','*',array('page_id'=>5),'',true); 
		$data['details']['title']="";
		$this->load->view('front/chat_at_me_info',$data); 
	}public function chat_at_me_school_info_teacher() {  
		$data['details'] = $this->Common_model->getRecords('pages','*',array('page_id'=>6),'',true); 
		$data['details']['title']="";
		$this->load->view('front/chat_at_me_info',$data); 
	}public function chat_at_me_school_info_parent() {  
		$data['details']['title']="";
		$data['details'] = $this->Common_model->getRecords('pages','*',array('page_id'=>7),'',true); 
		$this->load->view('front/chat_at_me_info',$data); 
	}

	public function web_about_us() {  
		
		$data['title']= 'About Us' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'About Us'." | ".SITE_TITLE;
        $data['meta_desc']= 'About Us'." | ".SITE_TITLE;
        $data['page_title'] = "About Us";
        $data['details'] = $this->Common_model->getRecords('pages','*',array('page_id'=>3),'',true); 
        $data['is_sidebar']=1;
        $this->load->view('front/include/header',$data);
        $this->load->view('front/about_us');
        $this->load->view('front/include/footer');
		
	}
	public function web_terms_and_conditions() {  
		$data['title']= 'Terms of Service' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Terms of Service'." | ".SITE_TITLE;
        $data['meta_desc']= 'Terms of Service'." | ".SITE_TITLE;
        $data['page_title'] = "Terms of Service";
        $data['details'] = $this->Common_model->getRecords('pages','*',array('page_id'=>1),'',true); 
        $data['is_sidebar']=1;
        $this->load->view('front/include/header',$data);
        $this->load->view('front/terms_and_conditions');
        $this->load->view('front/include/footer');
		
	}
	public function web_privacy_policy() {  
		$data['title']= 'Privacy Policy' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Privacy Policy'." | ".SITE_TITLE;
        $data['meta_desc']= 'Privacy Policy'." | ".SITE_TITLE;
        $data['page_title'] = "Privacy Policy";
        $data['details'] = $this->Common_model->getRecords('pages','*',array('page_id'=>2),'',true);
        $data['is_sidebar']=1; 
        $this->load->view('front/include/header',$data);
        $this->load->view('front/privacy_policy');
        $this->load->view('front/include/footer');
		
	}
	public function web_chat_at_me_school_info() { 
		check_web_user_status(); 
		$data['title']= 'Information' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Information'." | ".SITE_TITLE;
        $data['meta_desc']= 'Information'." | ".SITE_TITLE;
        $data['page_title'] = "Information";
        if($user_type=$this->session->userdata('front_user_type')){
        	if($user_type==1){
        		$page_id=4;
        	}if($user_type==2){
        		$page_id=5;
        	}if($user_type==3){
        		$page_id=6;
        	}if($user_type==4){
        		$page_id=7;
        	}
        }else{
        	$page_id=4;
        }
        $data['details'] = $this->Common_model->getRecords('pages','*',array('page_id'=>$page_id),'',true);

        $data['title']= SITE_TITLE.' User Guide';
        $data['details']['title']= SITE_TITLE.' User Guide';
        $data['meta_keywords']= $data['details']['title'];
        $data['meta_desc']= $data['details']['title'];
        $data['page_title'] = "Information";
        $data['print'] = "yes";

        $data['is_sidebar']=1; 
        $this->load->view('front/include/header',$data);
        $this->load->view('front/chat_at_me_info');
        $this->load->view('front/include/footer');
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/login.php */
