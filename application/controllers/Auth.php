<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Auth extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model(array('Common_model','Auth_model')); 
        $this->load->helper(array('email','common_helper'));
        $this->load->library('session');
        //$session = $this->session->userdata();
        if(!apiAuthentication()){
            // if(!isset($session['user_email'])){
            //     echo json_encode(array('status' =>'0' ,'msg'=>"Error invalid api key"));
            //     header('HTTP/1.0 401 Unauthorized');
            //     redirect(base_url());
            //     die;
            // }
            echo json_encode(array('status' =>'0' ,'msg'=>"Invalid API key."));
            header('HTTP/1.0 401 Unauthorized');
            die;            
        }

    }

    /*====================================== States ================================= */
    public function state_list()
    {
        try{
            $states=  $this->Common_model->getRecords('states');
            echo json_encode(array("status" => '1', "msg" => 'State list', "details" => $states));exit;
        }catch(Exception $e){
            echo json_encode(array("status" => '0', "msg" => $e->getMessage(), "details" => []));
            exit;
        }
    }      


    /*====================================== District ================================= */
    public function district_list()
    {
        try{
            if($this->input->post()) {
                $postData = $this->input->post();
                $stateId = $postData['state_id'];
                if (empty($stateId)) {
                    echo json_encode(array('status' => '0', 'msg' => 'Please select state.','details'=>[])); exit;
                }


                if ($district =  $this->Auth_model->getDistrictList($stateId)) {
                    echo json_encode(array("status" => '1', "msg" => 'District list', "details" => $district));exit;
                } else {
                    echo json_encode(array("status" => '0', "msg" => 'Record not found', "details" => []));exit;
                }

            }
        }catch(Exception $e){
            echo json_encode(array("status" => '0', "msg" => $e->getMessage(), "details" => []));
            exit;
        }
    }


    /*====================================== School List ================================= */
    public function school_list()
    {
        try{
            if($this->input->post()) {
                $postData = $this->input->post();
                $districtId = $postData['district_id'];
                 
               
                if (empty($districtId)) {
                    echo json_encode(array('status' => '0', 'msg' => 'Please select district.','details'=>[])); exit;
                }
                if(isset($postData['call_type']) && !empty($postData['call_type'])){

                     if($school =$this->Common_model->getRecords('school','id,name',array('district_id'=>$districtId),'name asc',false)) {
                       echo json_encode(array("status" => '1', "msg" => 'School list', "details" => $school));exit;
                    }else{
                        echo json_encode(array("status" => '0', "msg" => 'Record not found', "details" => []));exit;
                    }
                }else{
                    if($school =  $this->Auth_model->getSchoolList($districtId)) {
                        echo json_encode(array("status" => '1', "msg" => 'School list', "details" => $school));exit;
                    } else {
                        echo json_encode(array("status" => '0', "msg" => 'Record not found', "details" => []));exit;
                    }
                }
                
            }
        }catch(Exception $e){
            echo json_encode(array("status" => '0', "msg" => $e->getMessage(), "details" => []));
            exit;
        }
    }      


    /*====================================== School Grade List ================================= */ 
    public function grade_list()
    {
        try{
            if($this->input->post()) {
                $postData = $this->input->post();
                $schoolId = $postData['school_id'];
                if (empty($schoolId)) {

                    $school=array();
                    $school=SCHOOL_GRADE;
                    echo json_encode(array("status" => '1', "msg" => 'Grade list', "details" => $school));exit;
                }else{
                    if ($school =  $this->Auth_model->getGradeList($schoolId)) {

                        //echo '<pre>';print_r($school);exit;
                        echo json_encode(array("status" => '1', "msg" => 'Grade list', "details" => $school));exit;
                    } else {
                        echo json_encode(array("status" => '0', "msg" => 'Record not found', "details" => []));exit;
                    }
                }

            }
        }catch(Exception $e){
            echo json_encode(array("status" => '0', "msg" => $e->getMessage(), "details" => []));
            exit;
        }
    }      


    /*=======================================Signup======================================= */

    public function signup()
    {
        try{
            if($this->input->post()) {

                $postData = $this->input->post();
                // Common
                $stateId            =   test_input($postData['state_id']);
                $firstName          =   test_input($postData['first_name']);
                $lastName           =   test_input($postData['last_name']);
                $email              =   test_input($postData['email']);
                $userType           =   test_input($postData['user_type']);
                if($postData['type']!='Mobile'){
                    $recaptcha = $postData['g-recaptcha-response'];
                    if (!empty($recaptcha)) {
                        $this->load->library('Recaptcha');
                        $response = $this->recaptcha->verifyResponse($recaptcha);
                        if (isset($response['success']) and $response['success'] === true) {
                           
                        }
                        else{
                             echo json_encode(array('status' => '0', 'msg' => ' Select reCaptcha.','details'=>[])); exit;
                        }
                    }
                }

                if($postData['type']=='Mobile'){
                    $deviceId           =   test_input($postData['device_id']);
                    
                }else{
                    $deviceId ="";$deviceType="";
                }
                $deviceType         =   strtolower(test_input($postData['device_type']));

                // Common except district
                if($userType==1){
                    $password ="";
                }else{

                    $password           =   test_input($postData['password']);
                }

                // District
                if($userType==1){
                    $districtName       =   test_input($postData['district_name']);
                    $askQuestion        =   test_input($postData['ask_question']);
                }
                // Principal
                if($userType!=1){
                    $districtId         =   test_input($postData['district_id']);
                }else{
                    $districtId="";
                }
                if($userType==1 || $userType==2){
                    $phone              =   test_input($postData['phone']);
                    if(empty($phone)){
                        echo json_encode(array('status' => '0', 'msg' => 'Enter your phone number.','details'=>[])); exit;
                    }
                }else{
                     $phone="";
                }
                if($userType==2){
                    $schoolName         =   test_input($postData['school_name']);
                }else{
                    $schoolName="";
                }

                // Teacher
                if($userType==3 || $userType==4){
                    $schoolId           =   test_input($postData['school_id']);
               }else{
                    $schoolId ="";
               }
                if($userType==3){
                    if($deviceType!='web'){
                        $grade              =   test_input($postData['grade']);
                    }else{
                        $grade              =   $postData['grade'];
                    }
                }

                // Student

                if($userType==4){
                    if(!isset($postData['is_public'])){
                        $isPublic           =   "";
                    }else{
                        $isPublic           =   test_input($postData['is_public']);
                    }
                }else{
                    $isPublic="";
                }


                if(empty($firstName) || empty($lastName) || empty($stateId) || empty($email)){
                    echo json_encode(array("status" => '0', "msg" => "Fill in all the required fields - user type, first name, last name, email, state", "details" => []));exit;
                }

                if ($userType!=1 && $userType!=2 && $userType!=3 && $userType!=4) {
                    echo json_encode(array('status' => '0', 'msg' => 'Select valid user type','details'=>[])); exit;
                }


                if ($userType!=1) {
                    if(empty($password)){
                        echo json_encode(array('status' => '0', 'msg' => 'Enter your password','details'=>[])); exit;
                    }

                    if(strlen($password) < 6){
                        echo json_encode(array('status' => '0', 'msg' => 'Your password must be a minimum of 6 characters with at least 1 upper case letter, 1 number, and 1 special character.','details'=>[])); exit;
                    }

                    if(empty($districtId)){
                        echo json_encode(array('status' => '0', 'msg' => 'Select your district','details'=>[])); exit;
                    }

                } else {
                    if($this->Common_model->getRecords('district','id',array('name'=>$districtName,'state_id'=>$stateId),'',true)) {
                        echo json_encode(array('status' => '0', 'msg' => 'This district already exists in the system. Enter another district or email districts@chatatmeschools.com if you need login assistance.','details'=>[])); exit;
                    }
                }


                $mobileAuthToken = generateRandomString();
                

                if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                    echo json_encode(array('status' => '0', 'msg' => 'Enter a valid email address.','details'=>[])); exit;
                }

                $where = array('email' => $email,'is_deleted' => 0);
               
                
                if($this->Common_model->getRecords('users','user_id',$where,'user_id DESC',true)) {
                    echo json_encode(array('status' => '0', 'msg' => 'This email already exists in our system. Please enter another email address.','details'=>[])); exit;
                }
               
              
                if(empty($deviceType)){
                    echo json_encode(array('status' => '0', 'msg' => 'Enter the device type.','details'=>[])); exit;
                } else if($deviceType !='android' && $deviceType !='ios' && $deviceType !='web'){
                    echo json_encode(array('status' => '0', 'msg' => 'Device type must be either Andriod, iOS, or Web.','details'=>[])); exit;
                }


                /****************** Image upload start ***********************/
                $picNew = '';
                if($deviceType!='web'){
                    if(isset($_FILES) && !empty($_FILES['profile_pic']['name'])) 
                    {
                        if($_FILES['profile_pic']['error']==0) {
                            $image_path = USER_PROFILE_PATH;
                            $allowed_types = 'jpg|jpeg|png|JPG|JPEG|PNG';
                            // $allowed_types = '*';
                            $file='profile_pic';
                            $height = '';
                            $width = '';
                            $responce = commonImageUpload($image_path,$allowed_types,$file,$width,$height);
                     
                            if($responce['status']==0){
                                // echo json_encode(array("status" => 0, "msg" => $responce['msg'], "details" => []));exit;
                                echo json_encode(array("status" => 0, "msg" => "Image must be in jpg, jpeg, or png.", "details" => []));exit;
                            } else {
                                $picNew = $responce['image_path'];
                            }
                        }                    
                    }
                }else{
                    if(isset($_POST['perantal_img']) && !empty($_POST['perantal_img'])){
                        $data = $_POST["perantal_img"];
                        $image_array_1 = explode(";", $data);
                        $image_array_2 = explode(",", $image_array_1[1]);
                        $data = base64_decode($image_array_2[1]);
                        $imageName = USER_PROFILE_PATH.time() . '.png';
                        file_put_contents($imageName, $data);
                        $picNew=$imageName;
                    }
                    else if(isset($_POST['techer_img']) && !empty($_POST['techer_img'])){
                        $data = $_POST["techer_img"];
                        $image_array_1 = explode(";", $data);
                        $image_array_2 = explode(",", $image_array_1[1]);
                        $data = base64_decode($image_array_2[1]);
                        $imageName = USER_PROFILE_PATH.time() . '.png';
                        file_put_contents($imageName, $data);
                        $picNew=$imageName;
                    } 
                    else if(isset($_POST['principal_img']) && !empty($_POST['principal_img'])){
                        $data = $_POST["principal_img"];
                        $image_array_1 = explode(";", $data);
                        $image_array_2 = explode(",", $image_array_1[1]);
                        $data = base64_decode($image_array_2[1]);
                        $imageName = USER_PROFILE_PATH.time() . '.png';
                        file_put_contents($imageName, $data);
                        $picNew=$imageName;
                    }
                }
               
                if(empty($picNew)){
                     $picNew ='';
                }
                /*********************************** Image upload end ****************************************/


                $date= date('Y-m-d H:i:s');
                $verificationToken = mt_rand(100000,999999);
                
                if (!empty($password)) {
                    $password = md5($password);
                } else {
                    $password = '';
                }

                $this->db->trans_begin();                
                $is_approve=1;
                $status=3;
                $email_verified=2;
                if ($userType==1) {
                    $email_verified=1;
                    if(!$districtId = $this->Common_model->addEditRecords('district',array('name'=>$districtName,'state_id'=>$stateId,'ask_question'=>$askQuestion,'created'=>$date))) {
                        $this->db->trans_rollback();
                        echo json_encode(array('status' => '0', 'msg' => 'Check your internet connection and try again.','details'=>[])); exit;
                    }
                }

                if ($userType==2) {
                    if(empty($schoolName)) {
                        echo json_encode(array('status' => '0', 'msg' => 'Enter your school name.','details'=>[])); exit;
                    }
                    $schoolId=$schoolName;
                    if($this->Common_model->getRecords('users','user_id',array('school_id'=>$schoolId,'is_deleted'=>0),'',true)) {
                        echo json_encode(array('status' => '0', 'msg' => 'This principal email already exists for this school. Please contact your district administrator or email us at principals@chatatmeschools.com.','details'=>[])); exit;
                    }
                    /*if($this->Common_model->getRecords('school','id',array('name'=>$schoolName,'district_id'=>$districtId),'',true)) {
                        echo json_encode(array('status' => '0', 'msg' => 'School name already exist.','details'=>[])); exit;
                    }

                    if(!$schoolId = $this->Common_model->addEditRecords('school',array('name'=>$schoolName,'district_id'=>$districtId,'state_id'=>$stateId,'created'=>$date))) {
                        $this->db->trans_rollback();
                        echo json_encode(array('status' => '0', 'msg' => 'Some error occured. Please try again !!','details'=>[])); exit;
                    }*/

                }

                $grades = array();
                if ($userType==3 ||  $userType==2) {
                    $status=1;
                    if(empty($schoolId)) {
                        echo json_encode(array('status' => '0', 'msg' => 'Select your school.','details'=>[])); exit;
                    }
                    
                }
                if ($userType==3) {
                    if(!empty($grade) && isset($grade)) {
                        if($deviceType=='web'){
                            $grades=$_POST['grade'];
                        }else{
                            $grades = explode(',', $grade);
                            if (empty($grades)) {
                                echo json_encode(array('status' => '0', 'msg' => 'Select your grade(s).','details'=>[])); exit;
                            }
                        }
                            
                    } else {
                        echo json_encode(array('status' => '0', 'msg' => 'Select your grade(s).','details'=>[])); exit;
                    }
                }

                if ($userType==4) {
                    if(empty($schoolId)) {
                        echo json_encode(array('status' => '0', 'msg' => 'Select your school.','details'=>[])); exit;
                    }
                    $is_approve=3;
                    $status=1;
                }
               
                $insertData = array(
                    'first_name'=>$firstName,
                    'last_name'=>$lastName,
                    'password'=>$password,
                    'user_type'=>$userType,
                    'phone'=>$phone,
                    'email'=>$email,
                    'mobile_auth_token'=>$mobileAuthToken,
                    'profile_image'=>$picNew,
                    'state_id'=>$stateId,
                    'is_public'=>$isPublic?$isPublic:0,
                    'district_id'=>$districtId?$districtId:0,
                    'school_id'=>$schoolId?$schoolId:0,
                    'status'=>$status, //pending
                    'is_approve'=>$is_approve, //Not approve
                    'email_verified'=>$email_verified, //Unverified
                    'verification_token'=>$verificationToken,
                    'verification_token_date'=>$date,
                    'prospect_status'=>1,
                    'device_id'=>$deviceId,
                    'device_type'=>$deviceType,
                    'created'=>$date,
                );
              
                if($userId = $this->Common_model->addEditRecords('users',$insertData)) {
                    $user_number = str_pad($userId, 8, '0', STR_PAD_LEFT);
                    $update_user = $this->Common_model->addEditRecords('users',array('user_number'=>$user_number),array('user_id'=>$userId));
                    if ($userType==3) {
                        if (!empty($grades)) { 
                            foreach ($grades as $row) {
                                $rowTrim = trim($row);
                                $people = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13");
                                if (in_array($rowTrim, $people)) {
                                    if($userType==3){
                                        if(!$this->Common_model->addEditRecords('teacher_grade',array('teacher_id'=>$userId,'school_id'=>$schoolId,'district_id'=>$districtId,'grade'=>$rowTrim,'grade_display_name'=>DISPLAY_GRADE_NAME[$rowTrim]))){
                                            $this->db->trans_rollback();
                                            echo json_encode(array('status' => '0', 'msg' => 'Check your internet connection and try again.','details'=>[])); exit;
                                        }
                                    }else{
                                        if(!$this->Common_model->addEditRecords('school_grade',array('school_id'=>$userId,'school_id'=>$schoolId,'district_id'=>$districtId,'grade'=>$rowTrim,'grade_display_name'=>DISPLAY_GRADE_NAME[$rowTrim]))){
                                            $this->db->trans_rollback();
                                            echo json_encode(array('status' => '0', 'msg' => 'Check your internet connection and try again.','details'=>[])); exit;
                                        }

                                    }
                                } else {
                                    $this->db->trans_rollback();
                                    echo json_encode(array('status' => '0', 'msg' => 'Select correct grade(s).','details'=>[])); exit;
                                }
                            }
                        }
                    }
                    if($userType!=1) {
                        $verificationArray = array(
                            'user_id'=>$userId,
                            'email'=>$email,
                            'verification_token'=>$verificationToken,
                            'name'=>ucfirst($firstName.' '.$lastName),
                        );
                        $this->verification_code($verificationArray);
                    }

                    // $where = array('user_id' => $userId);
                    if($user_data = $this->Auth_model->getUserDetail($userId,$userType)) {
                        $user_data['mobile_auth_token']=$mobileAuthToken;
                        $this->db->trans_commit();                        
                        $message = 'Thank you for registering with '. WEBSITE_NAME .'.';
                        if($userType!=1) {
                            $message.='Click the Verify button to complete the registration process. Check your SPAM folder and whitelist emails from info@chatatmeschools.com.';
                        }
                        /*if($userType==2){
                            $district_info = $this->Common_model->getRecords('users','user_id',array('district_id' =>$districtId,'user_type'=>1),'', true);
                            $msg = ucfirst($schoolName)." registered in your district. Please check the request.";
                            addNotification($userId,$district_info['user_id'],'Request',$msg,$userId,0); 
                            sendNotification($district_info['user_id'],$msg);
                        }
                        if($userType==3){
                            $school_info = $this->Common_model->getRecords('users','user_id',array('school_id' =>$schoolId,'user_type'=>2),'', true);
                            $msg = ucfirst($firstName)." ".$lastName." registered in your school. Please check the request.";
                            addNotification($userId,$school_info['user_id'],'Request',$msg,$userId,0); 
                            sendNotification($school_info['user_id'],$msg);
                        }*/
                        echo json_encode(array("status" => '1', "msg" => $message, "details" => $user_data));exit;
                    } else {
                        echo json_encode(array('status' => '0', 'msg' => 'Check your internet connection and try again.','details'=>[])); exit;
                    }
                } else {
                    $this->db->trans_rollback();
                    echo json_encode(array('status' => '0', 'msg' => 'Check your internet connection and try again.','details'=>[])); exit;
                }

            }
        }catch(Exception $e){
            echo json_encode(array("status" => '0', "msg" => $e->getMessage(), "details" => []));
            exit;
        }
    }

    public function verification_code($userArray)
    {
        //return true;
        $userId = $userArray['user_id'];
        $email = $userArray['email'];
        $verificationToken = $userArray['verification_token'];
        $firstName = $userArray['name'];

        $fromEmail = getAdminEmail(); 
        // $fromName = FROM_NAME; 
        $toEmail = $email; 

        $subject = "Verification mail from ".WEBSITE_EMAIL_NAME;
        $encoded_user_id = base64_encode($userId);
        $encoded_verify_code = base64_encode($verificationToken);
        $data['verification_url'] = base_url().'verify-email/'.$encoded_user_id.'/'.$encoded_verify_code;
        $data['name']= $firstName;
        $data['verify_code']= $verificationToken;
        $body = $this->load->view('template/user_verification', $data,TRUE);
        $this->Common_model->sendEmail($toEmail,$subject,$body,$fromEmail);
        
        return true;
    }


    /*========================= Verify Email =========================*/
    public function email_verification() {
        try {
            if($this->input->post()) {

                $postData = $this->input->post();

                $userId             =   test_input($postData['user_id']);
                $email              =   test_input($postData['email']);
                $verificationCode   =   test_input($postData['verification_code']);
         
                if(empty($userId) || empty($email) || empty($verificationCode)){
                    echo json_encode(array("status" => '0', "msg" => "Fill in all the required fields - user id, email, verification code", "details" => []));exit;
                }

                $where= array("user_id"=>$userId);
                $userData = $this->Common_model->getRecords('users','*',$where,'',true);
                if(!$userData) {
                    echo json_encode(array("status" => '0', "msg" => "Record not found", "details" => []));exit;
                } 

                $userDate   = strtotime($userData['verification_token_date']); // current timestamp
                $timeSecond = strtotime(date("Y-m-d H:i:s"));
                $differenceInSeconds = $timeSecond - $userDate;
                $minutes = round($differenceInSeconds/60);
                
                if ($userData['email_verified']==1) {
                    $msg="Your email has already been verified; please login.";
                    if ($userData['user_type']==2 && $userData['is_approve']==1) {
                        // $msg="Your email has already been verified, but the district has not approved your profile yet! Contact your district administrator or email us at principals@chatatmeschools.com.";
                        $msg="Your email has already been verified, but the district has not approved your profile yet. If 24 hours have passed since you registered, contact your district administrator or email us at principals@chatatmeschools.com.";
                    }
                    if ($userData['user_type']==3 && $userData['is_approve']==1) {
                        /*$msg="Your email has already been verified, but your principal has not approved your profile yet. Contact your principal or email us at teachers@chatatmeschools.com.";*/
                        $msg="Your email has already been verified, but your principal has not approved your profile yet. If 24 hours have passed since you registered, contact your principal or email us at teachers@chatatmeschools.com.";
                    }
                    echo json_encode(array("status" => '0', "msg" => $msg, "details" => []));exit;

                }

                if($minutes>720) {
                    echo json_encode(array("status" => '0', "msg" => "Verification link has expired. Try again.", "details" => []));exit;
                } else {
                    if($userData['email'] != $email) {
                        echo json_encode(array("status" => '0', "msg" => "Enter your registered email address.", "details" => []));exit;
                    }

                    if($userData['verification_token'] == $verificationCode) {
                        $date = date('Y-m-d H:i:s');
                        $mobileAuthToken = generateRandomString();
                        $update_data = array(
                            'email_verified'=>1,
                            'mobile_auth_token'=>$mobileAuthToken,
                            'modified_datetime'=>$date,
                            'verification_token'=>'',
                            'verification_token_date'=>"0000-00-00 00:00:00"
                        );
                        $res = $this->Common_model->addEditRecords('users',$update_data,$where);
                        if($res) {
                            // $userData = $this->Common_model->getRecords('users','*',array('user_id'=>$userId),'',true);
                            if ($userData['user_type']==2) {
                                $district_info = $this->Common_model->getRecords('users','user_id,first_name,last_name,email',array('district_id' =>$userData['district_id'],'user_type'=>1),'', true);
                                $fromEmail = getAdminEmail();  
                                // $subject = 'New school registered-'.WEBSITE_NAME;
                                $subject = 'A principal registered for Chat At Me! Schools';
                                $chk_school = $this->Common_model->getRecords('school','id,name',array('id'=>$userData['school_id']),'',true,'','',1);
                                // $data['message']= ucfirst($chk_school['name'])." registered in your district. Please check the request.";
                                $data['message']="The principal at ".ucfirst($chk_school['name'])." has registered for Chat At Me! Schools. Please check and verify the request.";

                                $data['name']= ucfirst($district_info['first_name']).' '.$district_info['last_name'];
                                $toEmail = $district_info['email']; 
                                $body = $this->load->view('template/common', $data,TRUE);
                                $this->Common_model->sendEmail($toEmail,$subject,$body,$fromEmail);
                                // $msg = ucfirst($chk_school['name'])." registered in your district. Please check the request.";
                                $msg ="The principal at ".ucfirst($chk_school['name'])." has registered for Chat At Me! Schools. Please check and verify the request.";

                                addNotification($userId,$district_info['user_id'],'Request',$msg,$userId,0); 
                                sendNotification($district_info['user_id'],$msg);
                            }else if($userData['user_type']==3){
                                $school_info = $this->Common_model->getRecords('users','user_id,first_name,last_name,email',array('school_id' =>$userData['school_id'],'user_type'=>2),'', true);
                                $fromEmail = getAdminEmail();  
                                // $subject = 'New teacher registered-'.WEBSITE_NAME;
                                $subject = 'A new teacher registered for Chat At Me! Schools.';
                                
                                $data['message']= ucfirst($userData['first_name'])." ".$userData['last_name']." registered in your school. Please check the request.";
                                $data['name']= ucfirst($school_info['first_name']).' '.$school_info['last_name'];
                                $toEmail = $school_info['email']; 
                                $body = $this->load->view('template/common', $data,TRUE);
                                $this->Common_model->sendEmail($toEmail,$subject,$body,$fromEmail);
                                $msg = ucfirst($userData['first_name'])." ".$userData['last_name']." registered in your school. Please check the request.";
                                addNotification($userId,$school_info['user_id'],'Request',$msg,$userId,0); 
                                sendNotification($school_info['user_id'],$msg);
                            }

                            $userprofile = $this->Auth_model->getUserDetail($userId,$userData['user_type']);
                            $userprofile['mobile_auth_token']=$mobileAuthToken;
                            $userprofile['is_student_add'] = '0';
                            $userprofile['is_school_add'] = '0';
                            $userprofile['is_redirect_change_password'] = '0';
                            $userprofile['is_profile_complete']='0';
                            if ($userData['is_approve']==2) {

                                if ($userData['user_type']==1) {
                                    $userprofile['is_profile_complete'] = '1';
                                    if($userData['status']!=1){
                                        $user_profile['is_redirect_change_password'] = '1';
                                    }
                                } 
                                if ($userData['user_type']==2) {
                                    if($userData['status']!=1){
                                        $userprofile['is_redirect_change_password'] = '1';
                                    }
                                    if(!empty($userData['display_name'])) {
                                       $userprofile['is_profile_complete'] = '1';
                                    }
                                    if($chk_school = $this->Common_model->getRecords('school','id',array('id'=>$userData['school_id'],'name!='=>"",'address!='=>""),'',true,'','',1)){
                                        $userprofile['is_school_add'] = '1';
                                    }
                                }
                                if($userData['user_type']==3) {
                                    if($userData['status']!=1){
                                        $userprofile['is_redirect_change_password'] = '1';
                                    }
                                    if (!empty($userData['display_name'])) {
                                       $userprofile['is_profile_complete'] = '1';
                                    }
                                } 
                            }
                            if ($userData['user_type']==4) {
                                $userprofile['is_profile_complete'] = '1';
                                if($chk_student = $this->Common_model->getRecords('student','parent_id',array('parent_id'=>$userData['user_id'],'is_deleted'=>0),'',true,'','',1)){
                                    $userprofile['is_student_add'] = '1';
                                }
                            }
                           
                            $msg="Email has been verified.";
                            /*if ($userData['user_type']==2) {
                                $msg="Your email has been verified, but the district has not approved your profile yet! Contact your district administrator or email us at principals@chatatmeschools.com.";
                            }
                            if ($userData['user_type']==3) {
                                $msg="Your email has been verified, but your principal has not approved your profile yet. Contact your principal or email us at teachers@chatatmeschools.com.";
                            }*/
                            if ($userData['user_type']==2) {
                                $msg="Thank you for registering with ".WEBSITE_NAME.". The information has been sent to your district administrator for approval. If your login credentials are not active within 24 hours contact your district administrator or email us at principals@chatatmeschools.com.";
                            }
                            if ($userData['user_type']==3) {
                                $msg="Thank you for registering with ".WEBSITE_NAME.". The information has been sent to your school's principal for approval. If your login credentials are not active within 24 hours contact your principal or email us at teachers@chatatmeschools.com.";
                            }

                            echo json_encode(array("status" => '1', "msg" => $msg, "details" => $userprofile));exit;
                        } else {
                            echo json_encode(array("status" => '0', "msg" => "Check your internet connection and try again.", "details" => []));exit;
                        }
                    } else {
                        echo json_encode(array("status" => '0', "msg" => "Invalid verification code, please check and re-enter the code"));exit;
                    }
                }

            }
        } catch (Exception $e) {
            echo json_encode(array("status" => '0', "msg" => $e->getMessage(), "details" => []));
            exit;
        }
    }


    /*========================= Resend Email Verification Code =========================*/
    public function resend_email_verification() {

        try {
            if($this->input->post()) {
                $postData = $this->input->post();
                $email  =   test_input($postData['email']);

                if(!$userData = $this->Common_model->getRecords('users','user_id,email,email_verified, first_name,last_name,',array('email'=>$email,'is_deleted'=>0),'user_id DESC',true)){
                    echo json_encode(array("status" => '0', "msg" => "Enter your registered email address.", "details" => []));exit;
                }

                if($userData['email_verified']==1) {
                    echo json_encode(array("status" => '0', "msg" => "Your email has been verified; please login.", "details" => []));exit;
                }

                $verify_token_date = date("Y-m-d H:i:s");
                $verificationToken = mt_rand(100000,999999);

                $update_data = array(
                    'verification_token'=>$verificationToken, 
                    'verification_token_date'=>$verify_token_date
                );
                
                $this->Common_model->addEditRecords('users',$update_data,array('user_id'=>$userData['user_id']));

                $verificationArray = array(
                    'user_id'=>$userData['user_id'],
                    'email'=>$email,
                    'verification_token'=>$verificationToken,
                    'name'=>ucfirst($userData['first_name'].' '.$userData['last_name']),
                );

                $this->verification_code($verificationArray);

                $userData = $this->Auth_model->getUserDetail($userData['user_id']);
                echo json_encode(array("status" => '1', "msg" => "Verification code has been sent to your email address, check your inbox and spam." ,"data"=> $userData));exit;
            }
        } catch (Exception $e) {
            echo json_encode(array("status" => '0', "msg" => $e->getMessage(), "details" => []));
            exit;
        }
    }    


    /*=============================login==================================*/
    public function login()
    {
        try {
            if($this->input->post()) { 
                $postData = $this->input->post();

                $email          =   test_input($postData['email']);
                $password       =   test_input($postData['password']);
                if(isset($postData['device_id'])){
                    $deviceId       =   test_input($postData['device_id']);
                }else{
                    $deviceId="";
                }
                 if(isset($postData['device_type'])){
                    $deviceType       =   test_input($postData['device_type']);
                    $deviceType     =   strtolower($deviceType);
                }else{
                    $deviceType="";
                }
                if($deviceType=='web'){
                    $rememberme=test_input($postData['rememberme']);
                    if(!empty($rememberme))
                    { 
                        $cookie = array(
                            'name'   => 'username',
                            'value'  => $email,
                            'expire' => '86500'
                        );
                        $this->input->set_cookie($cookie);
                        /*$cookie = array(
                            'name' => 'password',
                            'value'  => $password,
                            'expire' => '86500'
                        );
                        $this->input->set_cookie($cookie);*/
                    }
                    else { 
                        if($this->input->set_cookie('username'))
                        {
                            $cookie = array(
                                'name'   => 'username',
                                'value'  => '',
                                'expire' => time()-'86500'
                            );
                            $this->input->set_cookie('username');
                        }
                        /*$cookie = array(
                            'name' => 'password',
                            'value'  => "",
                            'expire' => '86500'
                        );
                        $this->input->set_cookie($cookie);*/
                    }
                }

                $mobile_auth_token= generateRandomString();
               

                $password=md5($password);

                if(empty($deviceType)) {
                    echo json_encode(array("status" => '0', "msg" => "Enter device type", "details" => []));exit;
                } else if($deviceType !='android' && $deviceType !='ios' && $deviceType !='web') {
                    echo json_encode(array("status" => '0', "msg" => "Device type must be either android or ios or web.", "details" => []));exit;
                }
                 
                if(empty($email)) {
                    echo json_encode(array("status" => '0', "msg" => "Enter your email", "details" => []));exit;
                } 
                if(empty($password)) {
                    echo json_encode(array("status" => '0', "msg" => "Enter your password", "details" => []));exit;
                }


                $where = array('email'=>$email,'password'=>$password,'is_deleted'=>0);
                if(!$userData = $this->Common_model->getRecords('users','*',$where,'user_id DESC',true,'','',1)) {
                    $err = array('status' => '0', 'msg' => 'Email or password does not match');
                    echo json_encode($err); exit;   
                }
            
                $user_profile=$this->Auth_model->getUserDetail($userData['user_id'],$userData['user_type']);
                $user_profile['mobile_auth_token']=$mobile_auth_token;
                /******************** Update token **********************/
                if ($deviceType!='web') {
                    $formData = array('device_id' => $deviceId,'device_type' => $deviceType,'mobile_auth_token'=>$mobile_auth_token);
                    $this->Common_model->addEditRecords('users',$formData,array('user_id'=>$userData['user_id']));
                }
                
                if ($userData['email_verified']==2) {
                    $verify_token_date = date("Y-m-d H:i:s");
                    $verificationToken = mt_rand(100000,999999);

                    $update_data = array(
                        'verification_token'=>$verificationToken, 
                        'verification_token_date'=>$verify_token_date
                    );

                    $this->Common_model->addEditRecords('users',$update_data,array('user_id'=>$userData['user_id']));

                    $verificationArray = array(
                        'user_id'=>$userData['user_id'],
                        'email'=>$userData['email'],
                        'verification_token'=>$verificationToken,
                        'name'=>ucfirst($userData['first_name'].' '.$userData['last_name']),
                    );
                    $this->verification_code($verificationArray);

                    echo json_encode(array("status" => '1', "msg" => "We've sent you a verification email to ".$email.". Please verify your email to activate your account.", 'details'=>$user_profile));exit;
                }
                if($userData['status']==2) {
                    echo json_encode(array("status" => '0', "msg" => "Your account is no longer active. Email accounts@chatatmeschools.com to have your account reinstated.", "details" => []));exit;
                }
                $user_profile['is_student_add'] = '0';
                $user_profile['is_school_add'] = '0';
                $user_profile['is_profile_complete'] = '0';
                $user_profile['is_redirect_change_password'] = '0';
               //is_approve =1 for status 2, 
                if ($userData['is_approve']==1) {
                    if($userData['user_type']==2) {
                        /*echo json_encode(array("status" => '2', "msg" => "Your email has been verified, but the district has not approved your profile yet! Contact your district administrator or email us at principals@chatatmeschools.com.", "details" => $user_profile));exit;*/
                        echo json_encode(array("status" => '2', "msg" => "Your email has been verified, but the district has not approved your profile yet. If 24 hours have passed since you registered, contact your district administrator or email us at principals@chatatmeschools.com.", "details" => $user_profile));exit;
                    }
                    if($userData['user_type']==3) {
                        // echo json_encode(array("status" => '2', "msg" => "Your principal has not approved your profile yet. Contact your principal or email us at teachers@chatatmeschools.com.", "details" => $user_profile));exit;
                        echo json_encode(array("status" => '2', "msg" => "Your email has been verified, but your principal has not approved your profile yet. If 24 hours have passed since you registered, contact your principal or email us at teachers@chatatmeschools.com.", "details" => $user_profile));exit;
                    }
                    echo json_encode(array("status" => '2', "msg" => "Your account is inactive contact the admin for more details.", "details" => $user_profile));exit;
                    
                } else if ($userData['is_approve']==2) {
                    if ($userData['user_type']==1) {
                        $user_profile['is_profile_complete'] = '1';
                        if($userData['status']!=1){
                            $user_profile['is_redirect_change_password'] = '1';
                        }
                    } 
                    if ($userData['user_type']==2) {
                        if($userData['status']!=1){
                            $user_profile['is_redirect_change_password'] = '1';
                        }
                        if(!empty($userData['display_name'])) {
                           $user_profile['is_profile_complete'] = '1';
                        }
                        if($chk_school = $this->Common_model->getRecords('school','id',array('id'=>$userData['school_id'],'name!='=>"",'address!='=>""),'',true,'','',1)){
                            $user_profile['is_school_add'] = '1';
                        }
                        
                    }
                    if($userData['user_type']==3) {
                        if($userData['status']!=1){
                            $user_profile['is_redirect_change_password'] = '1';
                        }
                        if (!empty($userData['display_name'])) {
                           $user_profile['is_profile_complete'] = '1';
                        }
                    } 
                    if ($userData['user_type']==4) {
                        $user_profile['is_profile_complete'] = '1';
                        if($chk_student = $this->Common_model->getRecords('student','parent_id',array('parent_id'=>$userData['user_id'],'is_deleted'=>0),'',true,'','',1)){
                            $user_profile['is_student_add'] = '1';
                        }
                    }

                    if ($deviceType=='web') {
                        $login_session=array(   
                            'user_id'=>$userData['user_id'],
                            'device_type'=>$deviceType,
                            'user_email'=>$userData['email'],
                            'front_user_type'=>$userData['user_type'],
                            'district_id'=>$userData['district_id'],
                            'school_id' => $userData['school_id'],
                        );

                        $this->session->set_userdata($login_session);
                    }

                    echo json_encode(array("status" => '1', "msg" => "The district has not approved your profile yet. Contact your district administrator or email us at principals@chatatmeschools.com.", "details" => $user_profile));exit;
                }
                if ($userData['status']==1) {

                    if ($userData['user_type']==1) {
                        $user_profile['is_profile_complete'] = '1';
                        if($userData['status']!=1){
                            $user_profile['is_redirect_change_password'] = '1';
                        }
                    } 
                    if ($userData['user_type']==2) {

                        if($userData['status']!=1){
                            $user_profile['is_redirect_change_password'] = '1';
                        }
                        if(!empty($userData['display_name'])) {
                           $user_profile['is_profile_complete'] = '1';
                        }

                        if($chk_school = $this->Common_model->getRecords('school','id',array('id'=>$userData['school_id'],'name!='=>"",'address!='=>""),'',true,'','',1)){
                            $user_profile['is_school_add'] = '1';
                        }
                        
                    }
                    if($userData['user_type']==3) {
                        if($userData['status']!=1){
                            $user_profile['is_redirect_change_password'] = '1';
                        }
                        if (!empty($userData['display_name'])) {
                           $user_profile['is_profile_complete'] = '1';
                        }
                    } 
                    if ($userData['user_type']==4) {
                        $user_profile['is_profile_complete'] = '1';
                        if($chk_student = $this->Common_model->getRecords('student','parent_id',array('parent_id'=>$userData['user_id'],'is_deleted'=>0),'',true,'','',1)){
                            $user_profile['is_student_add'] = '1';
                        }
                    }

                    if ($deviceType=='web') {
                        $login_session=array(   
                            'user_id'=>$userData['user_id'],
                            'device_type'=>$deviceType,
                            'user_email'=>$userData['email'],
                            'front_user_type'=>$userData['user_type'],
                            'district_id'=>$userData['district_id'],
                            'school_id' => $userData['school_id'],
                        );

                        $this->session->set_userdata($login_session);
                    }
                    $response =  array('status'=>'1','msg'=>'Login is Successfully','details'=>$user_profile);
                    echo json_encode($response); exit;

                }
                if ($userData['status']==3) {
                    echo json_encode(array("status" => '0', "msg" => "Your account is not approved", "details" => []));exit;
                }
                

                //action log
                // $log_msg = $userData['fullname'] ." login by email";
                // actionLog('users',$userData['user_id'],'Login',$log_msg,'User',$userData['user_id']);
                $response =  array('status'=>'1','msg'=>'Login is Successfully','details'=>$user_profile);
                echo json_encode($response); exit;

            }
        } catch (Exception $e) {
            echo json_encode(array("status" => '0', "msg" => $e->getMessage(), "details" => [])); exit;            
        }
    } 


    /*================================== change password ====================================*/
    public function change_password() {
        try {
            check_user_status();
            if ($this->input->post()) {
                // $this->check_user_status(); 
                // $this->check_user_status($user_id);                
                
                $postData = $this->input->post();
                if($this->input->post('type')=='web'){
                    $userId=$this->session->userdata('user_id');
                }else{
                    $userId             = test_input($postData['user_id']); 
                }
                $currentPassword    = test_input($postData['current_password']);
                $newPassword        = test_input($postData['new_password']);
                $isApprove          = test_input($postData['is_approve']);
                $is_redirect=0;
                if(empty($userId)) {
                    $err = array('data' =>array('status' => '0', 'msg' => 'Enter your user id'));
                    echo json_encode($err); exit;
                }  
                
                if(empty($newPassword)){
                    $err = array('data' =>array('status' => '0', 'msg' => 'Enter a new password'));
                    echo json_encode($err); exit;
                }  
        
                if(strlen($newPassword) < 6){
                    $err = array('data' =>array('status' => '0', 'msg' => 'Enter new password with a minimum of 6 characters'));
                    echo json_encode($err); exit;
                } 



                $user_data =$this->Common_model->getRecords('users', 'password,user_type,is_approve', array('user_id'=>$userId), '', true);
                if(!empty($user_data)) {
                    if(isset($currentPassword) && !empty($currentPassword)){
                        if(md5($currentPassword)!=$user_data['password']){
                            echo json_encode(array("status" => '0', "msg" => "Incorrect current password", "details" => []));exit;
                        }
                    }
                    $where = array('user_id'=>  $userId);
                    $date= date('Y-m-d H:i:s');
                    $update_data = array(
                        'password' => md5($newPassword),
                        'modified'=>$date,
                    ); 

                    if ($isApprove==2) {
                        //$update_data['is_approve'] = 3;
                        if($user_data['user_type']==1 || $user_data['user_type']==4){
                            $update_data['is_approve'] = 3;
                        }
                        
                        $update_data['status'] = 1;
                        if(!$this->Common_model->addEditRecords('users', $update_data, $where)) {
                            echo json_encode(array("status" => '0', "msg" => "Some error occured. Please try again.", "details" => []));exit;
                        } else {
                            $user_detail =$this->Common_model->getRecords('users', 'user_type,is_approve', array('user_id'=>$userId), '', true);
                            if($user_detail['user_type']==2 || $user_detail['user_type']==3){
                                if($user_detail['is_approve']==2){
                                    $is_redirect=1;
                                }
                            }
                            echo json_encode(array("status" => '1', "msg" => "Your password has successfully been changed", "details" => ['is_redirect'=>$is_redirect]));exit;
                        }
                    } else {

                        if(empty($currentPassword)){
                            echo json_encode(array("status" => '0', "msg" => "Enter your current password", "details" => []));exit;
                        } 

                        if($user_data['password'] == md5($currentPassword)) {

                            if(!$this->Common_model->addEditRecords('users', $update_data, $where)) {
                                echo json_encode(array("status" => '0', "msg" => "Some error occured. Please try again.", "details" => []));exit;
                            } else {

                                $user_detail =$this->Common_model->getRecords('users', 'user_type,is_approve', array('user_id'=>$userId), '', true);
                                if($user_detail['user_type']==2 || $user_detail['user_type']==3){
                                    if($user_detail['is_approve']==2){
                                        $is_redirect=1;
                                    }
                                }
                                // echo json_encode(array("status" => '1', "msg" => "Your password has successfully been changed", "details" => ['is_redirect'=>$is_redirect]));exit;
                                echo json_encode(array("status" => '1', "msg" => "Your password was successfully changed.", "details" => ['is_redirect'=>$is_redirect]));exit;
                            }
                        } else {
                            echo json_encode(array("status" => '0', "msg" => "Current password is incorrect.", "details" => []));exit;
                        }
                    }
                } else {
                    echo json_encode(array("status" => '0', "msg" => "Invalid user account.", "details" => []));exit;
                }
            }
        } catch (Exception $e) {
            echo json_encode(array("status" => '0', "msg" => $e->getMessage(), "details" => [])); exit;
        }
    }



    /*================================= Get user profile ======================================*/
    public function get_user_profile() 
    {
        check_user_status();

        if($this->input->post('type')=='web'){
            $user_id=$this->session->userdata('user_id');
            $user_type=$this->session->userdata('front_user_type');
        }else{
            $user_id            = test_input($this->input->post('user_id')); 
            $other_user_id      = test_input($this->input->post('other_user_id')); 
            $user_type          = test_input($this->input->post('user_type')); 
        }

        if(empty($user_id)) {
            display_output('0','Enter user id');
        }
        if(empty($user_type)) {
            display_output('0','Enter user type');
        }
        if(!empty($other_user_id)){
            
            $user_id=$other_user_id;
        }else{
            $user_id=$user_id;
        }
        if(!empty($other_user_id)){
            $details=$this->Auth_model->getUserDetail($other_user_id,$user_type);
        }else{
            $details=$this->Auth_model->getUserDetail($user_id,$user_type);

        }
        $details['is_school_add_video']=0;
        $details['is_story'] = 0;
        $details['unread_chat_count'] = 0;

        if($user_type==1){
            $where=array('is_approve'=>1,'is_deleted'=>0,'email_verified'=>1,'user_type'=>2,'district_id'=>$details['district_id']);
        }
        if($user_type==2){
            $where=array('is_approve'=>1,'is_deleted'=>0,'email_verified'=>1,'user_type'=>3,'school_id'=>$details['school_id']);
            if($check_video = $this->Common_model->getFieldValue('school_introductory_video','id',array("school_id"=>$details['school_id'],'is_deleted'=>0))){
                 $details['is_school_add_video']=1;
                 $details['media_id']=$check_video;
            }
        }
        if($user_type==1 || $user_type==2){
            $request_count=$this->Common_model->getNumRecords('users',array('user_id'),$where);
            $details['request_count']=$request_count;
        }
        if($user_type==3){
            $request_count=$this->Auth_model->getTeacherRequestCount($user_id);
           
            $details['request_count']=$request_count;
            $teacher_grade_details =$this->Common_model->getRecords('teacher_grade', 'grade,grade_display_name', array('teacher_id'=>$user_id), 'grade asc', false);
            $details['teacher_grade']=$teacher_grade_details;


            if($teacher_grade =$this->Common_model->getRecords('teacher_grade', 'grade', array('teacher_id'=>$user_id,'school_id'=>$details['school_id'],'grade<=' =>7), '', true)){
                $details['is_story'] = 1;
            } else {
                $details['is_story'] = 0;
            }
            $details['unread_chat_count']=$this->Common_model->getChatunreadCount($user_id);

            /*if (!empty($details['teacher_grade'])) {
                foreach ($details['teacher_grade'] as $row) {
                    if ($row['grade'] <= 6) {
                        $details['is_story'] = 1;
                    } else {
                        $details['is_story'] = 0;
                    }
                }
            }*/
        }
        if($user_type==4){
            $details['student']=$this->Common_model->getRecords('student','id,student_name',array('parent_id'=>$user_id),'',false);
            if($this->Common_model->getstudentgrades($user_id)){
                $details['is_story'] = 1;
            } else {
                $details['is_story'] = 0;
            }
            $details['unread_chat_count']=$this->Common_model->getChatunreadCount($user_id);
        }
        if($this->input->post('type')=='web'){
            $data['details']=$details;
            $data['states']=$this->Common_model->getRecords('states');
            if($user_type == 1){
                $data['form_action']='api/profile-update';
                $this->load->view('front/district/profile_data',$data);
            }elseif($user_type == 2){
                $data['form_action']='api/prinicpal-profile-update';
                $this->load->view('front/principal/profile_data',$data);
            }elseif($user_type == 3){
                $schoolId=$this->session->userdata('school_id');
                $data['school_grade'] =  $this->Auth_model->getGradeList($schoolId);
                $data['form_action']='api/profile-update';
                $this->load->view('front/teacher/profile_data',$data);
            }elseif($user_type == 4){
                $data['form_action']='api/profile-update';
                $this->load->view('front/parent/profile_data',$data);
            }
        }else{

            if($details){

                display_output('1','User detail',array('details'=>$details));
            }else{
                display_output('0','No detail found');  
            } 
        }
       
    }
    /*================================= Profile update ======================================*/

    public function profile_update() {
        
        check_user_status();
        $postData = $this->input->post();
      
        if($this->input->post('type')=='web'){
            $user_id=$this->session->userdata('user_id');
            $user_type=$this->session->userdata('front_user_type');
        }else{
            $user_id        =   test_input($this->input->post('user_id'));
            $user_type        =   test_input($this->input->post('user_type'));
        }
        $title        =   test_input($this->input->post('title'));
        $first_name        =   test_input($this->input->post('first_name'));
        $last_name        =   test_input($this->input->post('last_name'));
        $display_name        =   test_input($this->input->post('display_name'));
        $phone        =   test_input($this->input->post('phone'));

        if(empty($user_type)) {
            display_output('0','Enter your user type');
        }if(empty($first_name)) {
            display_output('0','Enter your first name');
        }if(empty($last_name)) {
            display_output('0','Enter your last name');
        }
        if($user_type==2 || $user_type==3){
            if(empty($display_name)) {
                display_output('0','Enter your display name');
            }
        }
        if($user_type==2 || $user_type==1){
            if(empty($phone)) {
                display_output('0','Enter your phone number');
            }
        }
        $this->db->trans_begin(); 
        if($user_type==3){
            if(!isset($postData['school_id']) || empty($postData['school_id'])){
                display_output('0','Please enter school id.');
            }else{
                $school_id   =  test_input($postData['school_id']);
            }
            if(!isset($postData['grade']) || ($postData['grade'] =="")){
                display_output('0','Enter your grade(s).');
            }else{
                $grade   =  $postData['grade'];
            }
         
            /*$grades = array();
            if ($this->input->post('type')==3 ||  $this->input->post('type')==2) {
                $status=1;
                if(empty($schoolId)) {
                    echo json_encode(array('status' => '0', 'msg' => 'Select your school','details'=>[])); exit;
                }
                
                if(!empty($grade) && isset($grade)) {
                    if($deviceType=='web'){
                        $grades=$_POST['grade'];
                    }else{
                        $grades = explode(',', $grade);
                        if (empty($grades)) {
                            echo json_encode(array('status' => '0', 'msg' => 'Select your grade(s)','details'=>[])); exit;
                        }
                    }
                    
                } else {
                    echo json_encode(array('status' => '0', 'msg' => 'Select your grade(s)','details'=>[])); exit;
                }
            }*/

            $update_grade=array();
            $old_teacher_grade_id=array();

            if($this->input->post('type')=='web'){
                $grade = implode(',', $grade);
            }
            $new_grade = explode(',', $grade);

            $school_where_in="school_id=$school_id and grade IN ($grade)";
            if(!$school_grade=$this->Common_model->getRecords('school_grade','id,grade,district_id',$school_where_in,'',false)){
                $this->db->trans_rollback();
                display_output('0','Some of the grade are not available in this school.');
            }else{
                if(count($school_grade)!=count($new_grade)){
                    $this->db->trans_rollback();
                    display_output('0','Some of the grade are not available in this school.');
                }
                $teacher_grade_district_id=$school_grade[0]['district_id'];
            }
            $old_grade=$this->Common_model->getRecords('teacher_grade','id,grade',array('teacher_id'=>$user_id,'school_id'=>$school_id),'',false);
            // echo $this->db->last_query();exit();
            if(!empty($old_grade)){
                foreach($old_grade as $list){
                    $update_grade[]=$list['grade'];
                    $old_teacher_grade_id[]=$list['id'];
                    if(!$group_info =$this->Common_model->getRecords('chat_groups', 'id as group_id,', array('created_by'=>$user_id,'grade'=>$list['grade'],'school_id'=>$school_id,'deleted'=>0,'is_teacher'=>1), '', true)){
                        $chat_group=array(
                            'name'=>$display_name."'s ".DISPLAY_GRADE_NAME[$list['grade']].' class',
                            'description'=>"",
                            'created_at'=>date('Y-m-d H:i:s'),
                            'is_teacher'=>1,
                            'school_id'=>$school_id,
                            'grade'=>$list['grade'],
                            'created_by'=>$user_id,
                            'is_active'=>1,
                        );
                        $chat_group_id=$this->Common_model->addEditRecords('chat_groups', $chat_group);
                        $chat_group_info=array(
                            'group_id'=>$chat_group_id,
                            'user_id'=>$user_id,
                            'created'=>date('Y-m-d H:i:s'),
                        );
                        $this->Common_model->addEditRecords('chat_user_group_assoc', $chat_group_info);
                    }   
                }
                $result_super=array_diff($update_grade,$new_grade);
                if(!empty($result_super))
                {
                    foreach ($result_super as  $key=>$list) {
                        $this->Auth_model->TeacherGradeUpdateRelavantRecordDelete($school_id,$user_id,$list,$old_teacher_grade_id[$key]);
                        $this->Auth_model->DeleteGradeGroup($school_id,$user_id,$list);
                    }
                }
            }
            if(!empty($new_grade)){
                foreach ($new_grade as $key => $grade_value) {
                    $arr=array('teacher_id'=>$user_id,'school_id'=>$school_id,'grade'=>$grade_value);
                    if(!$already_exist_grade=$this->Common_model->getRecords('teacher_grade','id,grade',$arr,'',true)){
                        $arr['grade_display_name']=DISPLAY_GRADE_NAME[$grade_value];
                        $arr['district_id']=$teacher_grade_district_id;
                        $inserted_id=$this->Common_model->addEditRecords('teacher_grade', $arr);
                        $chat_group=array(
                            'name'=>$display_name."'s ".DISPLAY_GRADE_NAME[$grade_value].' class',
                            'description'=>"",
                            'created_at'=>date('Y-m-d H:i:s'),
                            'is_teacher'=>1,
                            'school_id'=>$school_id,
                            'grade'=>$grade_value,
                            'created_by'=>$user_id,
                            'is_active'=>1,
                        );
                        $chat_group_id=$this->Common_model->addEditRecords('chat_groups', $chat_group);
                        $chat_group_info=array(
                            'group_id'=>$chat_group_id,
                            'user_id'=>$user_id,
                            'created'=>date('Y-m-d H:i:s'),
                        );
                        $this->Common_model->addEditRecords('chat_user_group_assoc', $chat_group_info);
                    }
                }
            }
        }

        $update_data=array(
            'title'=>$title,
            'first_name'=>$first_name,
            'last_name'=>$last_name,
            'phone'=>$phone,
            'display_name'=>$display_name,
            'is_approve'=>3,
            'modified'=>date('Y-m-d H:i:s')
            );
        /******************************* Image upload start *********************************/
        $imgfilepath="";
        $imgfilerror="";
        if($this->input->post('type')!='web'){
            if(isset($_FILES['profile_pic'])){
                if(!empty($_FILES['profile_pic']['name'])){
                    if($_FILES['profile_pic']['error']==0) {
                        $image_path = USER_PROFILE_PATH;
                        $allowed_types = '*';
                        $file='profile_pic';
                        $height = '';
                        $width = '';
                        $imgresponce = commonImageUpload($image_path,$allowed_types,$file,$width,$height);

                        if($imgresponce['status']==0){
                            $upload_error = $imgresponce['msg'];   
                            $imgfilerror="1";
                        } else {
                            $imgfilepath=$imgresponce['image_path'];
                        }
                    }
                    if($imgfilerror!=''){
                        $this->db->trans_rollback();
                        display_output('0',strip_tags($upload_error));
                    }
                    if(!empty($imgfilepath)){
                        $update_data['profile_image']=$imgfilepath;
                    }
                }
            }
        }
        if(isset($_POST['image']) && !empty($_POST['image'])){

            $data = $_POST["image"];
            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $imageName = USER_PROFILE_PATH.time() . '.png';
            file_put_contents($imageName, $data);
            $update_data['profile_image']=$imageName;
        }
         //print_r($update_data);die;
        /************** Image upload end ******************/
        $where= array("user_id"=>$user_id);
        $this->Common_model->addEditRecords('users',$update_data,$where);
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            display_output('0','Something went wrong. Please try again'); 
        } else {
            $this->db->trans_commit();
            $details=$this->Auth_model->getUserDetail($user_id,$user_type);
            $details['mobile_auth_token']=$this->Common_model->getFieldValue('users','mobile_auth_token',$where);
            display_output('1','Profile successfully updated',array('details'=>$details));
        }
    }


    /*========================================forgot_password====================================*/ 

    public function forgot_password() {
        $email              =   trim($this->input->post('email'));
        $postData = $this->input->post();
 
        if(empty($email)) {
            display_output('0','Enter your email');
        } 
       if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
        {    
            display_output('0','Enter a valid email address.');
        }
        // $user_data = $this->Common_model->getRecords('users','*',array("email"=>$email),'user_id desc',true);
       
        if(!$user_data= $this->Common_model->getRecords('users','user_id,first_name,last_name,status,email_verified,is_deleted,email,mobile_auth_token,user_type,password',array('email'=>$email),'user_id DESC',true)) {  
            display_output('0','Enter your registered email address.');
        }
        if($user_data) {
            if($user_data['is_deleted']==1) {
                display_output('0','Your account has been deleted contact the admin for more details.');
            } 
            if($user_data['email_verified']==2) {
                if($user_data['status']==2) {
                display_output('0','Your principal has not approved your profile yet. Contact your principal or email us at teachers@chatatmeschools.com.');
            }
                display_output('0',"We've sent you a verification email to ".$user_data['email'].". Please verify your email to activate your account.");
                
            }
            
            if($user_data['status']==2) {
                display_output('0','Your account has been inactive contact the admin for more details.');
            }
            
            if($user_data['user_type']==1 && $user_data['password']=="") {
                display_output('0','The district has not approved your profile yet. Contact your district administrator or email us at principals@chatatmeschools.com.');
            }  
            $token = mt_rand(100000,999999);
            
            $from_email = getAdminEmail(); 
            $from_name = FROM_NAME; 
            $to_email = $email; 
     
            $subject = "Link to reset your ".WEBSITE_EMAIL_NAME." Password";
            $data['reset_password_url'] = base_url().'reset-password?token='.$token;

            $data['name']= ucfirst($user_data['first_name']).' '.$user_data['last_name'];
            $data['type'] = 'admin';    
            $data['verify_code']= $token;
            $body = $this->load->view('template/forgot_password', $data,TRUE);
          
            if($this->Common_model->sendEmail($to_email,$subject,$body,$from_email)) 
            {
                $reset_token_date = date("Y-m-d H:i:s");
                $where = array('user_id'=> $user_data['user_id']); 
                $update_data = array(
                    'reset_token'=>$token, 
                    'reset_token_date'=>$reset_token_date
                    );
                $this->Common_model->addEditRecords('users', $update_data, $where);
                if(isset($postData['type']) && !empty($this->input->post('type'))){
                    $type=$this->input->post('type');
                    if($type=='web'){
                        display_output('1','A link to reset your password was sent to your email; check your inbox and spam folder.');
                    }else{
                        display_output('1','A link to reset your password was sent to your email; check your inbox and spam folder.');
                    }
                }else{
                    display_output('1','A link to reset your password was sent to your email; check your inbox and spam folder.');
                }
                
            } else {
                display_output('0','Check your internet connection and try again.');
            } 
        }
    }

    /*========================================Reset password====================================*/ 

    public function reset_password() {
        $new_password    =   test_input($this->input->post('password'));
        $email       =   test_input($this->input->post('email'));
        $otp  =   test_input($this->input->post('reset_code'));
        
        if(empty($email)){
            display_output('0','Please enter email.');
        }

        if(empty($otp)){
            display_output('0','Please enter reset code.');
        } 

        if(empty($new_password)){
            display_output('0','Please enter your password.');
        }

        if(strlen($new_password) < 6){
            display_output('0','Password must be at least 6 characters long.');
        }  
        
        if(!$user_data= $this->Common_model->getRecords('users','user_id,user_type,status,is_deleted,mobile_auth_token,reset_token,reset_token_date,password',array('email'=>$email),'user_id DESC',true)) {  
            display_output('0','Enter your registered email address.');
        }

        if($user_data){
            if($user_data['reset_token'] != trim($otp)) {
                display_output('0','Invalid reset password token.');
            }
            if($user_data['status']==3){
                display_output('0','The district has not approved your profile yet. Contact your district administrator or email us at principals@chatatmeschools.com.');
            }
            if($user_data['is_deleted']==0 && $user_data['status']==1){

                $token_date = strtotime($user_data['reset_token_date']);
                $current_date = new DateTime("now", new DateTimeZone('UTC'));
                $current_date = $current_date->format('Y-m-d H:i:s');
                $diff=strtotime($current_date)-$token_date;
                if($diff > 3600) {
                    display_output('0','This reset password link has expired. Please have another one sent to your email.');
                } else {
                    $where = array('user_id'=> $user_data['user_id']); 
                    $update_data = array(
                            'password'=>md5($new_password),
                            'reset_token'=>'', 
                            'reset_token_date'=>'',
                            'modified'=>date('Y-m-d H:i:s')
                        );

                    $this->Common_model->addEditRecords('users', $update_data, $where);
                    // display_output('1','Your password has successfully been changed');
                    display_output('1','Your password was successfully changed.');

                } 
            }else{
                display_output('0','Your account is no longer active. Email accounts@chatatmeschools.com to have your account reinstated.');
            }
        }
    }

    


    /*=============================logout_app===========================================*/
    
    public function logout() 
    {
        $user_id            =   test_input($this->input->post('user_id'));
        check_user_status();
        if(empty($user_id)) {
            display_output('0','Enter user id');
        }
        $update = array(
            'mobile_auth_token'=>'',
            'device_id'=>'',
            'device_type'=>''
        );
        $this->Common_model->addEditRecords('users',$update,array('user_id' => $user_id));
        display_output('1','Logout successfully.');
    }
   
    /*=================================== Common functionality =====================================*/
    /*=================================== Contact Us =====================================*/

    public function contact_us() {
        check_user_status();
        $name           = ucwords(test_input($this->input->post('name')));
        $from_email     = test_input($this->input->post('email'));
        $subject        = test_input($this->input->post('subject'));
        $message        = test_input($this->input->post('message'));

        if(empty($name)){
            display_output('0','Enter your name.');
        }

        if(empty($from_email)){
            display_output('0','Please enter email.');
        } 
         
        if (filter_var($from_email, FILTER_VALIDATE_EMAIL) === false) {
            display_output('0','Enter a valid email address.');
        }  

        if(empty($subject)){
            display_output('0','Enter your subject');
        }

        $data['message'] = $message; 
        $data['name'] = $name; 
        $body = $this->load->view('template/contact_us_template', $data,TRUE);
            
        $admin_email=getAdminEmail();
        
        //Send mail to admin
        if($this->Common_model->sendEmail($admin_email,$subject,$body,$from_email)) {
            //Send email to the user
            $reply_message = "The message just stated - Message sent successfully. Where is the message you state?";                      

            $data['message'] = $reply_message; 
            $data['name'] = $name; 
            $body = $this->load->view('template/common', $data,TRUE);
            $to_email = $from_email;
            
            // $from_name = FROM_NAME;  
              //Send mail to contact us user
            $this->Common_model->sendEmail($to_email,$subject,$body,$admin_email);
            
            $insert_data = array(
                'name' => $name,
                'email' => $from_email,
                'subject' => $subject,
                'message' => $message,
                'created' => date("Y-m-d H:i:s"),
            );
            $this->Common_model->addEditRecords('contact', $insert_data);
            $msg ='Message sent successfully';
            display_output('1',$msg);
        } else {
            display_output('0','Some error occured. Please try again');
        }
    }
    /*===================================  Broadcast Message =====================================*/

    public function broadcast_msg() {
        check_user_status();

        if($this->input->post('type') == 'web'){
            $user_info_ids = $this->input->post('user_info');
            $user_info_ids = implode(",",$user_info_ids);
            $where_in=  "user_id IN ($user_info_ids)";
            $participant=$this->Common_model->getRecords('users','email,concat(first_name," ",last_name) as name, user_id',$where_in,'',false);
            if($participant){
                $arr = [];
                // [{"name": "Methew Thomas", "email": "methew@mailinator.com","id":"19"}]
                foreach($participant as $list){
                    $email = $list['email'];
                    $name = $list['name'];
                    $id = $list['user_id'];
                    array_push($arr, array('name' =>$name, 'email'=>$email, 'id'=>$id ));
                }

                $user_info =  json_encode($arr);
            }
            
        }else{
            $user_info = test_input($this->input->post('user_info'));
        }


        $user_id        = test_input($this->input->post('user_id'));
        $subject        = test_input($this->input->post('subject'));
        $message        = test_input($this->input->post('message'));
        $user_type        = test_input($this->input->post('user_type'));
        
        $sender_user_details=$this->Common_model->getRecords('users','email,concat(first_name," ",last_name) as name',array('user_id'=>$user_id),'',true);
        if(empty($subject)){
            display_output('0','Enter your subject');
        }
        if(empty($message)){
            display_output('0','Enter your message');
        }if(empty($user_type)){
            display_output('0','Enter user type.');
        }
        if(empty($user_info)){
            display_output('0','Select the recipients');
        }
        $user_detail=json_decode($user_info);
        $insert_data = array(
            'notification_type'=>'email',
            'subject'=>$subject, 
            'message'=>$message, 
            'created_by'=>$user_id, 
            'created'=>date('Y-m-d H:i:s')
        );
        $this->db->trans_begin();
            $last_id=$this->Common_model->addEditRecords('broadcast_notification', $insert_data);
            $boradcast_email=array();
            $email_notification=array();
            if(!empty($user_detail)){
                foreach($user_detail as $key => $detail) {
                    $boradcast_email[]=array(
                        'notification_id'=>$last_id,
                        'user_id'=>$detail->id,
                        'email'=>$detail->email,
                        'created'=>date('Y-m-d H:i:s'),
                    );
                    $email_notification[]=array(
                        'user_type'=>$user_type,
                        'type'=>'Broadcast_notification',
                        'status'=>0,
                        'sender_id'=>$user_id,
                        'sender_email'=>$sender_user_details['email'],
                        'sender_name'=>$sender_user_details['name'],
                        'recipient_name'=>$detail->name,
                        'email'=>$detail->email,
                        'user_id'=>$detail->id,
                        'subject'=>$subject, 
                        'message'=>$message, 
                        'created'=>date('Y-m-d H:i:s'),
                    );
                }
                $this->db->insert_batch('broadcast_emails', $boradcast_email);
                $this->db->insert_batch('email_notification', $email_notification);
           
            }
        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            display_output('0','Something went wrong. Please try again'); 
        } else {
            $this->db->trans_commit();
            if($this->input->post('type') == 'web'){
                unset($_SESSION["broadcast_email"]);
            }
           
            display_output('1','Message broadcasted successfully.');
        }
       
    }
    /*===================================  Broadcast List =====================================*/

    public function broadcast_msg_list()
    {
        try{
            check_user_status();
            $user_id        =   test_input($this->input->post('user_id'));
            if($broadcast_details =  $this->Auth_model->getBroadcastMessageList($user_id)) {

                //$broadcast_details['email_participant']=array();
                foreach ($broadcast_details as $key => $broadcast) {
                    $participant=$this->Common_model->getRecords('broadcast_emails','email',
                        array('notification_id'=>$broadcast['notification_id']),'',false);

                    $broadcast_details[$key]['email_participant']=$participant;
                }
                if($this->input->post('type')=='web'){
                    $data['details'] = $broadcast_details;
                    $this->load->view('front/district/broadcast_msg_list',$data);
                }else{
                    display_output('1','Broadcast list.',array('details'=>$broadcast_details));
                }
            } else {
                if($this->input->post('type')=='web'){
                    $data['details'] = $broadcast_details;
                    $this->load->view('front/district/broadcast_msg_list',$data);

                }else{
                    $data['details'] = "";
                    display_output('0','No record found');
                }
                
            }
           
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
    }
    /*===================================  Broadcast Msg Delete =====================================*/

    public function broadcast_msg_delete()
    {
        try{
            check_user_status();
            $user_id        =   test_input($this->input->post('user_id'));
            $notification_id =   test_input($this->input->post('notification_id'));
            $update_data = array(
                'is_deleted'=>1,
                'modified'=>date('Y-m-d H:i:s'),
            );
            if($res = $this->Common_model->addEditRecords('broadcast_notification',$update_data,array('notification_id'=>$notification_id))){
                display_output('1','Broadcast message deleted');
            }else{
                display_output('0','No record found');
            }                                     
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
    }
    
  /*  public function Checkstatus(){

        $id = $this->input->post('user_id');
        if(isset($id)){
            $usrrstatus = $this->db->get_where('users',array('user_id'=>$id))->row()->status;
            if($usrrstatus != 1){
              
              echo json_encode(array('status' =>'0' ,'msg'=>"Your account is no longer active. Email accounts@chatatmeschools.com to have your account reinstated."));
              $user_id = $this->session->userdata('user_id');
              $this->session->sess_destroy();
              redirect("login");

            }
        }
    }*/
} //End controller class
