<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Parent_student extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model(array('Common_model','Parent_student_model','Teacher_model')); 
        $this->load->helper(array('email','common_helper'));
          error_reporting(0);
        //$session = $this->session->userdata();
        if(!apiAuthentication()){
            echo json_encode(array('status' =>'0' ,'msg'=>"Error invalid api key"));
            header('HTTP/1.0 401 Unauthorized');
            die;            
        }

    }


    /*===========================  teacher by grade======================= */
    public function teacher_by_grade()
    {
        // echo '<pre>';
        // print_r($this->input->post()); die;
        try{
            check_user_status();
            $user_id        =   test_input($this->input->post('user_id'));
            if($this->input->post()) {
                $postData = $this->input->post();
                if(!isset($postData['school_id']) || empty($postData['school_id'])){
                    display_output('0','Please enter school id.');
                }else{
                    $school_id   =  test_input($postData['school_id']);
                }
                if(!isset($postData['grade']) ||  ($postData['grade'] =="")){
                    display_output('0','Please enter grade.');
                }else{
                    $grade   =  test_input($postData['grade']);
                }
                $grade_arr=explode(',', $grade);
                $result=array();
                $result_grade['grade']=array();
                //$result_grade['grade'][]=array();
                // echo count($grade_arr);die;
                if(!empty($grade_arr)){
                    for($i=0;$i<count($grade_arr);$i++){
                        $result_grade['grade'][]=array('grade_id'=>$grade_arr[$i],'grade_name'=>DISPLAY_GRADE_NAME[$grade_arr[$i]]);
                        if($teacher_grade_data =$this->Parent_student_model->getTeacherByGrade($grade_arr[$i],$school_id)){
                            foreach ($teacher_grade_data as $key => $value) {
                                $result_grade['grade'][$i]['teacher'][]=$value;
                                $result=$result_grade;
                            }
                        }else{
                            $result_grade['grade'][$i]['teacher']=array();
                        }
                    }
                }
                if(!empty($result)){
                    display_output('1','Teacher grade list.',array('details'=>$result));
                }else {
                    display_output('0','No record found.');
                }
            }
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
    }
    /*===========================  Menu Grade teacher======================= */
    public function menu_teacher_by_grade()
    {
        try{
            check_user_status();
            $user_id        =   test_input($this->input->post('user_id'));
            if($this->input->post()) {
                $postData = $this->input->post();
                $result=array();
                $result['grade']=array();
                $result['is_story']=0;
                $result['unread_chat_count']=0;
                $result['first_name']="";
                $result['last_name']="";
                $result['profile_image']="";
                $result['logo']="";
                $parentdetail=$this->Common_model->getRecords('users','first_name,last_name,profile_image,school_id', array('user_id'=>$user_id), '', true);
                if(!empty($parentdetail)){
                    $res = $this->Common_model->getRecords('school','logo', array('id'=>$parentdetail['school_id']), '', true);
                    if(!empty($res['logo'])){
                        $result['logo']=$res['logo'];
                    }else {
                        $result['logo']='assets/front/images/phl_school.svg';
                    } 
                    $result['first_name']=$parentdetail['first_name'];
                    $result['last_name']=$parentdetail['last_name'];
                    if(!empty($parentdetail['profile_image'])){
                        $result['profile_image']=$parentdetail['profile_image'];
                    }
                }
                $result['unread_chat_count']=$this->Common_model->getChatunreadCount($user_id);


                if($teacher_grade_details=$this->Teacher_model->getUniqueGrade($user_id)){

                    if($this->Common_model->getstudentgrades($user_id)){
                        $result['is_story'] = 1;
                    } else {
                        $result['is_story'] = 0;
                    }

                    foreach ($teacher_grade_details as $key => $details) {
                        $teacher_info=$this->Teacher_model->getTeacherDetailsByGrade($details['teacher_grade_id']);
                        if($teacher_info){
                            // $result_grade['grade'][$key]['grade_name']=DISPLAY_GRADE_NAME[$details['grade']];
                            // $result_grade['grade'][$key]['teacher']=$teacher_info;
                            // $result=$result_grade;
                            $rt=array('grade_name'=>DISPLAY_GRADE_NAME[$details['grade']],
                                'teacher'=>$teacher_info
                                );
                            array_push($result['grade'],$rt );
                        }
                    }
                }
                if(!empty($result)){
                    display_output('1','Teacher grade list.',array('details'=>$result));
                }else {
                    display_output('0','No record found.');
                }
            }
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
    }
    
    /*===========================  Add student by parent ======================= */
    public function add_student() {
        try{
            check_user_status();
            if($this->input->post()) {

                $postData = $this->input->post();
                $user_id        =   test_input($postData['user_id']);
                
               
                if(!isset($postData['school_id']) || empty($postData['school_id'])){
                    display_output('0','Please enter school id.');
                }else{
                    $school_id = test_input($postData['school_id']);
                }
                if(!isset($postData['fullname']) || empty($postData['fullname'])){
                    display_output('0','Please enter fullname.');
                }else{
                    $fullname = test_input($postData['fullname']);
                }if(!isset($postData['grade_teacher']) || empty($postData['grade_teacher'])){
                    display_output('0','Please enter grade & teacher.');
                }else{
                    $grade_teacher = test_input($postData['grade_teacher']);
                }
             
                if(!$user_data = $this->Common_model->getRecords('users','user_type',array("user_id"=>$user_id,"school_id"=>$school_id,'is_deleted'=>0,'user_type'=>4),'',true)){
                     display_output('0','User details not found');
                }
                $this->db->trans_begin();

                if(!$exist_student=$this->Common_model->getRecords('student','id',array("parent_id"=>$user_id,"student_name"=>$fullname,'is_deleted'=>0),'',true)){
                    $insert_data=array(
                        'student_name'=>$fullname,
                        'parent_id'=>$user_id,
                        'status'=>1,
                        'is_deleted'=>0,
                        'created'=>date('Y-m-d H:i:s')
                    );
                    $student_id=$this->Common_model->addEditRecords('student',$insert_data);
                }else{
                    $student_id=$exist_student['id'];
                }
                $grade_arr=array();
                $teacher_grade_id_arr=array();
               
                if($student_id) {
                   $grade_details=json_decode($grade_teacher);
                   foreach($grade_details as $grade_teacher){
                        if(!$this->Parent_student_model->CheckstudentGrade($user_id,$fullname,$grade_teacher->teacher_grade_id)){
                            $grade_arr[]=array(
                                'student_id'=>$student_id,
                                'grade'=>$grade_teacher->grade_id,
                                'teacher_grade_id'=>$grade_teacher->teacher_grade_id,
                                'is_approve'=>1,
                                'created'=>date('Y-m-d H:i:s'),
                            );
                            $teacher_grade_id_arr[]=$grade_teacher->teacher_grade_id;
                        }
                   }
                    if(!empty($grade_arr)){
                        $this->db->insert_batch('student_grade', $grade_arr);
                    }
                }
                if($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    display_output('0','Check your internet connection and try again.'); 
                } else {
                    $this->db->trans_commit();
                    $student=$this->Common_model->getRecords('student','id,student_name',
                        array('parent_id'=>$user_id),'student_name asc',false);
                    if(!empty($grade_arr)){
                        $ids=implode(',', $teacher_grade_id_arr); 
                        $where_in="id IN($ids)";
                        if($teachers=$this->Common_model->getRecords('teacher_grade','teacher_id',
                            $where_in,'',false,'teacher_id')){
                            foreach ($teachers as $key => $value) {
                                $teacher_info=$this->Common_model->getRecords('users','first_name,last_name,email',
                                    array('user_id'=>$value['teacher_id']),'',true);
                                $fromEmail = getAdminEmail();  
                                $subject = 'New student registered-'.WEBSITE_EMAIL_NAME;
                                $data['message']= ucfirst($fullname)."requested to join your class. Please approve the request.";
                                $data['name']= ucfirst($teacher_info['first_name']).' '.$teacher_info['last_name'];
                                $toEmail = $teacher_info['email']; 
                                $body = $this->load->view('template/common', $data,TRUE);
                                $this->Common_model->sendEmail($toEmail,$subject,$body,$fromEmail);
                                $message = ucfirst($fullname)." requested to join your class. Please approve the request.";
                                addNotification($user_id,$value['teacher_id'],'Request',$message,$user_id,0); 
                                sendNotification($value['teacher_id'],$message);
                            }
                        }
                    }

                    if(empty($grade_arr)){
                        display_output('0',"The student has already been added to this teacher's class.",array('student_list'=>$student));
                    }
                    /*display_output('1','You are already following this teacher.',array('student_list'=>$student));*/
                    display_output('1','Student successfully added',array('student_list'=>$student));
                }

            }
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
       
    }

    public function add_student_web() {
        try{
            check_user_status();
            if($this->input->post()) {

                $postData = $this->input->post();
                $user_id   = $postData['user_id'];

                if(!isset($postData['school_id']) || empty($postData['school_id'])){
                    display_output('0','Please enter school id.');
                }else{
                    $school_id = $postData['school_id'];
                }
                // if(!isset($postData['fullname']) || empty($postData['fullname'])){
                //     display_output('0','Please enter fullname.');
                // }else{
                //     $fullname = $postData['fullname'];
                // }
                if(!isset($postData['grade_teacher']) || empty($postData['grade_teacher'])){
                    display_output('0','Please enter grade & teacher.');
                }else{
                    $grade_teacher = $postData['grade_teacher'];
                }

               
                if(!$user_data = $this->Common_model->getRecords('users','user_type,first_name,last_name',array("user_id"=>$user_id,"school_id"=>$school_id,'is_deleted'=>0,'user_type'=>4),'',true)){
                     display_output('0','You are not registered user for this school.');
                }
                $fullname = ucwords($user_data['first_name'].'  '.$user_data['last_name']);
                $this->db->trans_begin();
                if(!$exist_student=$this->Common_model->getRecords('student','id',array("parent_id"=>$user_id,"student_name"=>$fullname,'is_deleted'=>0),'',true)){
                    $insert_data=array(
                        'student_name'=>$fullname,
                        'parent_id'=>$user_id,
                        'status'=>1,
                        'is_deleted'=>0,
                        'created'=>date('Y-m-d H:i:s')
                    );
                    $student_id=$this->Common_model->addEditRecords('student',$insert_data);
                }else{
                    $student_id=$exist_student['id'];
                }
                
                $grade_arr=array();
                $teacher_grade_id_arr=array();
                if($student_id) {

                    for($i=0;$i<count($grade_teacher);$i++){

                        $grade_id = $this->Common_model->getFieldValue('teacher_grade','grade',array('id'=>$grade_teacher[$i]));
                        if(!$this->Parent_student_model->CheckstudentGrade($user_id,$fullname,$grade_teacher[$i])){
                            $grade_arr[]=array(
                                'student_id'=>$student_id,
                                'grade'=>$grade_id,
                                'teacher_grade_id'=>$grade_teacher[$i],
                                'is_approve'=>1,
                                'created'=>date('Y-m-d H:i:s'),
                            );
                            $teacher_grade_id_arr[]=$grade_teacher[$i];
                        }
                    }
                    if(!empty($grade_arr)){
                        $this->db->insert_batch('student_grade', $grade_arr);
                    }
                }
                if($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    display_output('0','Check your internet connection and try again.'); 
                } else {
                    $this->db->trans_commit();
                        $student=$this->Common_model->getRecords('student','id,student_name',
                            array('parent_id'=>$user_id),'student_name',false);
                    if(!empty($grade_arr)){
                        $ids=implode(',', $teacher_grade_id_arr); 
                        $where_in="id IN($ids)";
                        if($teachers=$this->Common_model->getRecords('teacher_grade','teacher_id',
                            $where_in,'',false,'teacher_id')){
                            foreach ($teachers as $key => $value) {
                                $teacher_info=$this->Common_model->getRecords('users','first_name,last_name,email',
                                    array('user_id'=>$value['teacher_id']),'',true);
                                $fromEmail = getAdminEmail();  
                                $subject = 'New student registered-'.WEBSITE_EMAIL_NAME;
                                $data['message']= ucfirst($fullname)." requested to join your class. Please approve the request.";
                                $data['name']= ucfirst($teacher_info['first_name']).' '.$teacher_info['last_name'];
                                $toEmail = $teacher_info['email']; 
                                $body = $this->load->view('template/common', $data,TRUE);
                                $this->Common_model->sendEmail($toEmail,$subject,$body,$fromEmail);
                                $message = ucfirst($fullname)." requested to join your class. Please approve the request.";
                                addNotification($user_id,$value['teacher_id'],'Request',$message,$user_id,0); 
                                sendNotification($value['teacher_id'],$message);
                            }
                        }
                    }
                    if(empty($grade_arr)){

                        display_output('0',"The student has already been added to this teacher's class.",array('student_list'=>$student));
                    }
                    display_output('1','Student successfully added',array('student_list'=>$student));
                }

            }
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
       
    }
    /*===========================  Get student detail by id ======================= */
    public function student_detail() {
        try{
            check_user_status();
            if($this->input->post()) {
                $postData = $this->input->post();
                $user_id        =   test_input($postData['user_id']);
                
                if(!isset($postData['student_id']) || empty($postData['student_id'])){
                    display_output('0','Please enter student id.');
                }else{
                    $student_id = test_input($postData['student_id']);
                }
                if(!$check_student=$this->Common_model->getRecords('student','id',
                        array('parent_id'=>$user_id,'id'=>$student_id),'',true)){
                    display_output('0', 'This student record doesn\'t match with parent id.');
                }
                $student_detail =$this->Parent_student_model->getStudentDetail($student_id);
                if(!empty($student_detail)){
                    foreach ($student_detail as $key => $value) {
                        $student_detail[$key]['status']= "";
                        if($student_detail[$key]['is_approve']==1){
                            $student_detail[$key]['status']= "Pending";
                        }else if($student_detail[$key]['is_approve']==2){
                            $student_detail[$key]['status']= "Accepted";
                        }else if($student_detail[$key]['is_approve']==3){
                            $student_detail[$key]['status']= "Rejected";
                        }
                    }
                        
                    display_output('1','Student detail',array('details'=>$student_detail));
                }else{
                    display_output('0','This teacher no longer assigned to this class. Please contact your school if the teacher was accidentally removed.'); 
                } 
            }
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
       
    }
     /*===========================  request canceled by student ======================= */
    public function request_canceled_by_student() {
        try{
            check_user_status();
            if($this->input->post()) {
                $postData = $this->input->post();
                $user_id        =   test_input($postData['user_id']);
                
                if(!isset($postData['student_id']) || empty($postData['student_id'])){
                    display_output('0','Please enter student id.');
                }else{
                    $student_id = test_input($postData['student_id']);
                }if(!isset($postData['teacher_grade_id']) || empty($postData['teacher_grade_id'])){
                    display_output('0','Please enter teacher grade id.');
                }else{
                    $teacher_grade_id = test_input($postData['teacher_grade_id']);
                }
                if(!$check_student=$this->Common_model->getRecords('student','id',
                        array('parent_id'=>$user_id,'id'=>$student_id),'',true)){
                    display_output('0', 'This student record doesn\'t match with parent id.');
                }

                if($this->Common_model->deleteRecords('student_grade', array('teacher_grade_id'=>$teacher_grade_id,'student_id'=>$student_id)))
                {

                    if(!$check_other_grade=$this->Common_model->getRecords('student_grade','id',array('student_id'=>$student_id),'',true)){
                        $this->Common_model->deleteRecords('student', array('id'=>$student_id));
                    }
                    if($teacher_info=$this->Common_model->getRecords('teacher_grade','teacher_id,grade,school_id',array('id'=>$teacher_grade_id),'',true)){
                        if($group_info =$this->Common_model->getRecords('chat_groups', 'id as group_id,', array('created_by'=>$teacher_info['teacher_id'],'grade'=>$teacher_info['grade'],'school_id'=>$teacher_info['school_id'],'deleted'=>0,'is_teacher'=>1), '', true)){
                            $this->Common_model->addEditRecords('chat_user_group_assoc', array('deleted'=>1),array('group_id'=>$group_info['group_id'],'user_id'=>$user_id));
                        }
                    }
                    display_output('1','Teacher successfully removed');
                }else{
                    display_output('0','Check your internet connection and try again.');
                }

               /* $update_data = array(
                    'is_deleted'=>1,
                    'deleted_date'=>date('Y-m-d H:i:s'),
                    'deleted_by'=>$user_id,
                    'modified'=>date('Y-m-d H:i:s'),
                );
                if($res = $this->Common_model->addEditRecords('student_grade',$update_data,array('teacher_grade_id'=>$teacher_grade_id,'student_id'=>$student_id))){
                    display_output('1','Teacher successfully removed');
                }else{
                    display_output('0','Something went wrong. Please try again..');
                }     */
            }
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
       
    }
    /*===========================  add continue watch detail ======================= */
    public function add_continue_watch_detail()  {
        try{
            check_user_status();
            $postData = $this->input->post();
           
            if($this->input->post()) {
                $postData = $this->input->post();
                if($this->input->post('type')=='web'){
                        $user_id=$this->session->userdata('user_id');
                        $user_type=$this->session->userdata('front_user_type');
                    }else{
                        $user_id        =   test_input($this->input->post('user_id'));
                        $user_type        =   test_input($this->input->post('user_type'));
                }

                if(!isset($postData['media_id']) || empty($postData['media_id'])){
                    display_output('0','Please enter media id.');
                }else{
                    $media_id   =  test_input($postData['media_id']);
                }
                if(!isset($postData['duration']) || empty($postData['duration'])){
                    display_output('0','Please enter duration.');
                }else{
                    $duration   =  test_input($postData['duration']);

                }
                if(!isset($postData['is_finish'])){
                    display_output('0','Please enter is finish.');
                }else{
                    $is_finish   =  test_input($postData['is_finish']);
                }
                
                if($is_finish==1){
                    if($this->Common_model->deleteRecords('continue_watch_media', array("media_id"=>$media_id,"user_id"=>$user_id)))
                    {
                        display_output('1','Video successfully removed');
                    }else{
                        display_output('0','Check your internet connection and try again.');
                    }
                }else{
                    $insert_data=array(
                        'user_id'=>$user_id,
                        'media_id'=>$media_id,
                        'duration'=>$duration,
                        'created'=>date('Y-m-d H:i:s'),
                    );
                    if($user_data = $this->Common_model->getRecords('continue_watch_media','id',array("media_id"=>$media_id,"user_id"=>$user_id),'',true)){
                         $insert_data['modified']=date('Y-m-d H:i:s');
                         if($this->Common_model->addEditRecords('continue_watch_media',$insert_data,array('id'=>$user_data['id']))){
                             display_output('1','Records successfully updated');
                         }else{
                            display_output('0','Check your internet connection and try again.'); 
                         }
                    }else{
                        $insert_data['created']=date('Y-m-d H:i:s');
                        if($this->Common_model->addEditRecords('continue_watch_media',$insert_data)){
                            display_output('1','Records successfully added');
                        }else{
                            display_output('0','Check your internet connection and try again.'); 
                        }
                    }
                }
                
                   
            }
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
            
    }     
    /*===========================  Home ======================= */
    public function home()  {
        try{
            check_user_status();
            if($this->input->post()) {
                $postData = $this->input->post();
                $user_id        =   test_input($postData['user_id']);
                if(!isset($postData['school_id']) || empty($postData['school_id'])){
                    display_output('0','Please enter school id.');
                }else{
                    $school_id   =  test_input($postData['school_id']);
                }
                /*if(!$data['school_video']= $this->Common_model->getRecords('school_introductory_video','*',array('is_deleted'=>0,'school_id'=>$school_id,'status'=>1),'',true)){
                    $data['school_video']=array();
                }*/
                $data['school_video']=$this->Parent_student_model->getSchoolVideo($school_id);
                if($teacher_grade_id=$this->Teacher_model->getApprovedTeachers($user_id)){
                    if(!empty($teacher_grade_id['teacher_grade_id'])){
                        $data['recently_video'] = $this->Teacher_model->getRecentVideo(1,0,"",$teacher_grade_id['teacher_grade_id'],$school_id);
                    }else{
                        $data['recently_video']=array();
                    }
                    /*echo $this->db->last_query();
                    echo '<pre>';print_r($teacher_grade_id);exit;*/
                    
                }else{
                    $data['recently_video']=array();
                }

               
                $data['continue_video'] = $this->Teacher_model->getRecentVideo(1,1,$user_id);
                if(!empty($data)){
                    $data['badge'] = '0';
                    $data['notification_flag'] = '0';
                    if($res = $this->Common_model->getRecords('users', 'unread_notification,notification_flag', array('user_id'=>$user_id), '', true)) {
                        $data['badge'] = (string)$res['unread_notification'];
                        $data['notification_flag'] = (string)$res['notification_flag'];
                    }
                    display_output('1','Video List',array('details'=>$data));
                }else{
                    display_output('0','No record found.');      
                }
            }
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
            
    }     
        
   /*===========================  Recently Uploaded ======================= */     
        
    public function all_recent_continue_video() {
        try{
            check_user_status();
            if($this->input->post()) {
                $postData = $this->input->post();
                $user_id = test_input($this->input->post('user_id'));
                if(!isset($postData['school_id']) || empty($postData['school_id'])){
                    display_output('0','Please enter school id.');
                }else{
                    $school_id   =  test_input($postData['school_id']);
                }
                if(!isset($postData['call_type']) || empty($postData['call_type'])){
                    display_output('0','Please enter call type.');
                }else{
                    $call_type   =  test_input($postData['call_type']);
                }
                if(!in_array($call_type, array('recent','continue'))){
                    display_output('0','Please enter  valid call type.');
                }
                if($call_type=='recent'){
                    if($teacher_grade_id=$this->Teacher_model->getApprovedTeachers($user_id)){
                        if(!empty($teacher_grade_id['teacher_grade_id'])){
                            $data['recently_video'] = $this->Teacher_model->getRecentVideo(2,0,"",$teacher_grade_id['teacher_grade_id'],$school_id);
                        }else{
                            $data['recently_video']=array();
                        }
                    }else{
                        $data['recently_video']=array();
                    }
                    if(!empty($data['recently_video'])){
                        display_output('1','Video List',array('details'=>$data));
                    }else{
                        display_output('0','No record found.');      
                    }
                                                                                                  
                }else{
                    $data['continue_video'] = $this->Teacher_model->getRecentVideo(2,1,$user_id);
                    if(!empty($data)){
                        display_output('1','Video List',array('details'=>$data));
                    }else{
                        display_output('0','No record found.');      
                    }
                }
               /* if(!empty($data)){
                    display_output('1','Video List',array('details'=>$data));
                }else{
                    display_output('0','No record found.');      
                }*/
            }
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }

    }
        

        
        
        


} //End controller class
