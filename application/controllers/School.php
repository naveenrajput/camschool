<?php defined('BASEPATH') OR exit('No direct script access allowed');
class School extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
        $this->load->library(array('S3'));
        $this->load->model(array('Common_model','School_model')); 
        $this->load->helper(array('email','common_helper'));
        
        //$session = $this->session->userdata();
        if(!apiAuthentication()){
            echo json_encode(array('status' =>'0' ,'msg'=>"Error invalid api key"));
            header('HTTP/1.0 401 Unauthorized');
            die;            
        }

    }



    /*===========================  Requested teacher list for school======================= */
    public function school_request_list()
    {
        try{
            check_user_status();
            $user_id        =   test_input($this->input->post('user_id'));
            if($this->input->post()) {
                $postData = $this->input->post();
                $school_id = test_input($postData['school_id']);
                if (empty($school_id)) {
                    display_output('0','Please enter school id.');
                }
                $this->Common_model->addEditRecords('notification', array('read' => '1'), array('to_user' => (string) $user_id));
                $this->Common_model->addEditRecords('users', array('unread_notification' => 0), array('user_id' => $user_id));
                if ($teacher =  $this->School_model->getTeacherList($school_id,1)) {
                    display_output('1','Teacher list.',array('details'=>$teacher));
                } else {
                    display_output('0','No request found.');
                }
            }
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
    }

    /*=======================teacher list approved by school=============================== */
    public function teacher_approved_by_school()
    {

        try{
            check_user_status();
            $user_id        =   test_input($this->input->post('user_id'));
            if($this->input->post()) {
                $postData = $this->input->post();
               
               // $school_id = test_input($postData['school_id']);
                if(!isset($postData['school_id']) || empty($postData['school_id'])){
                    display_output('0','Please enter school id.');
                }else{
                    $school_id = test_input($postData['school_id']);
                }
                if(!isset($postData['teacher_id']) || empty($postData['teacher_id'])){
                    display_output('0','Please enter teacher id.');
                }else{
                    $teacher_id = test_input($postData['teacher_id']);
                }
                
                if(!$user_data =$this->Common_model->getRecords('users', 'user_type', array('user_id'=>$user_id,'school_id'=>$school_id,'user_type'=>2), '', true)){
                    display_output('0','Unauthorized action.');
                }
                $teacher_arr=explode(',', $teacher_id);
                $email_notification=array();
                
                $approve_data=array(
                    'is_approve'=>2,
                    'status'=>1,
                    'approved_by'=>$user_id,
                    'modified'=>date('Y-m-d H:i:s')
                    );
                $invalid_teacher=array();
                $this->db->trans_begin();  
                if(!empty($teacher_arr)){
                    for($i=0;$i<count($teacher_arr);$i++){
                        //echo $i;
                        if($info =$this->Common_model->getRecords('users', 'user_id,email,first_name,last_name', array('user_id'=>$teacher_arr[$i],'school_id'=>$school_id,'is_deleted'=>0), '', true)){
                            $email_notification[]=array(
                                'user_type'=>3,
                                'type'=>'School_approved_teacher',
                                'status'=>0,
                                'sender_id'=>$user_id,
                                'recipient_name'=>$info['first_name'].' '.$info['last_name'],
                                'email'=>$info['email'],
                                'user_id'=>$info['user_id'],
                                'created'=>date('Y-m-d H:i:s'),
                            );
                            $this->Common_model->addEditRecords('users',$approve_data,array('user_id'=>$teacher_arr[$i]));
                        }else{
                            $this->db->trans_rollback();
                            $reject_data=user_details($teacher_arr[$i]);
                            $invalid_teacher[]=$reject_data['first_name']." ".$reject_data['last_name'];
                            display_output('0','Some invalid record found.', array('invalid_record'=>$invalid_teacher)); 
                        }
                    }
                } 
                $this->db->insert_batch('email_notification', $email_notification);
                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    display_output('0','Something went wrong. Please try again.'); 
                } else {
                    $this->db->trans_commit();
                    display_output('1','Request Approved');
                }
            }
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
    }  

    /*==================== Approved teacher list  by school========================== */
    public function approved_teacher_by_school()
    {
       try{
            check_user_status();
            $user_id        =   test_input($this->input->post('user_id'));
            if($this->input->post()) {
                $postData = $this->input->post();
                if(!isset($postData['school_id']) || empty($postData['school_id'])){
                    display_output('0','Please enter school id.');
                }else{
                    $school_id = test_input($postData['school_id']);
                }
                if ($teacher =  $this->School_model->getTeacherList($school_id,2)) {
                    display_output('1','Teacher list.',array('details'=>$teacher));
                } else {
                    display_output('0','No request found.');
                }

            }
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
    } 

    /*================================= School detail update ======================================*/

    public function school_detail_update() {
        try{
            check_user_status();
            if($this->input->post()) {
                $postData = $this->input->post();
                $user_id        =   test_input($postData['user_id']);
                if(!isset($postData['address'])){
                    display_output('0','Please enter address.');
                }else{
                    $address = test_input($postData['address']);
                }
                if(!isset($postData['school_id']) || empty($postData['school_id'])){
                    display_output('0','Please enter school id.');
                }else{
                    $school_id = test_input($postData['school_id']);
                }
                if(!$user_data = $this->Common_model->getRecords('users','user_type',array("user_id"=>$user_id,"school_id"=>$school_id),'',true)){
                     display_output('0','You are not authorize to update this school detail.');
                }

                $update_data=array(
                    'address'=>$address,
                    'modified'=>date('Y-m-d H:i:s')
                    );
                /******************************* Image upload start *********************************/
                $imgfilepath="";
                $imgfilerror="";
                if(isset($_FILES['logo']) || !empty($_FILES['logo']['name'])){
                    if($_FILES['logo']['error']==0) {
                        $image_path = SCHOOL_LOGO_PATH;
                        $allowed_types = '*';
                        $file='logo';
                        $height = '';
                        $width = '';
                        $imgresponce = commonImageUpload($image_path,$allowed_types,$file,$width,$height);

                        if($imgresponce['status']==0){
                            $upload_error = $imgresponce['msg'];   
                            $imgfilerror="1";
                        } else {
                            $imgfilepath=$imgresponce['image_path'];
                            if($school_logo = $this->Common_model->getFieldValue('school','logo',array("id"=>$school_id))){
                                if(file_exists($school_logo)){
                                    unlink($school_logo);
                                }
                            }
                            
                        }
                    }
                }
                if($imgfilerror!=''){
                    display_output('0',strip_tags($upload_error));
                }
                if(!empty($imgfilepath)){
                    $update_data['logo']=$imgfilepath;
                }

                /********** Image upload end ******************/
                $this->Common_model->addEditRecords('school',$update_data,array("id"=>$school_id));

                $school_data = $this->Common_model->getRecords('school','*',array("id"=>$school_id),'',true);
                display_output('1','School details updated successfully',array('details'=>$school_data));
                /*if($this->Common_model->addEditRecords('school',$update_data,array("id"=>$school_id))) 
                {
                    $school_data = $this->Common_model->getRecords('school','*',array("id"=>$school_id),'',true);
                    display_output('1','School details updated successfully',array('details'=>$school_data));
                } else {
                    display_output('1','Some error occured. Please try again.');
                }*/   

            }
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
       
    }
    /*======================== School introductory video =======================*/

    public function school_upload_video() {
        try{
            check_user_status();
            if($this->input->post()) {
                $postData = $this->input->post();
                $user_id = test_input($this->input->post('user_id'));
                $type = test_input($this->input->post('type'));
                if(!isset($postData['title']) || empty($postData['title'])){
                    display_output('0','Please enter title.');
                }else{
                    $title = test_input($postData['title']);
                }
                if(!isset($postData['description']) || empty($postData['description'])){
                    display_output('0','Please enter description.');
                }else{
                    $description = test_input($postData['description']);
                }
                if($type!='web'){
                  
                    if(!isset($postData['school_id']) || empty($postData['school_id'])){
                        display_output('0','Please enter school_id.');
                    }else{
                        $school_id = test_input($postData['school_id']);
                    }
                    if(!isset($postData['format']) || empty($postData['format'])){
                        display_output('0','Please enter format.');
                    }else{
                        $format = test_input($postData['school_id']);
                    }
                    if(!isset($postData['media_name']) || empty($postData['media_name'])){
                        display_output('0','Please enter media name.');
                    }else{
                        $media_name = test_input($postData['media_name']);
                    }
                    if(!isset($postData['s3thumbPath'])){
                        display_output('0','Please enter thumbh image path.');
                    }else{
                        $s3thumbPath = test_input($postData['s3thumbPath']);
                    }
                    if(!isset($postData['s3largeimagePath'])){
                        display_output('0','Please enter large image path.');
                    }else{
                        $s3largeimagePath = test_input($postData['s3largeimagePath']);
                    }
                    if(!isset($postData['duration']) || empty($postData['duration'])){
                        display_output('0','Please enter duration.');
                    }else{
                        $duration = test_input($postData['duration']);
                        $duration=convert_duration($duration);
                    }
                    if(!isset($postData['mobile_type']) || empty($postData['mobile_type'])){ //only for android
                        display_output('0','Please enter mobile type.');
                    }else{
                        $mobile_type = test_input($postData['mobile_type']);
                    }
                    if(!isset($postData['rotation'])){//only for android
                        display_output('0','Please enter rotation.');
                    }else{
                        $rotation = test_input($postData['rotation']);
                    }
                    $media_type=1;
                    if(!isset($mobile_type)) {
                        $mobile_type = 'ios';
                        $rotation = -1;
                    } else if($mobile_type=='android') {
                        if(!isset($rotation)) {
                            display_output('0','Please enter rotation.');
                        } 
                    }
                }else{
                    $school_id = $this->session->userdata('school_id');
                    $user_id = $this->session->userdata('user_id');
                }
                
                if(!$user_data = $this->Common_model->getRecords('users','user_type',array("user_id"=>$user_id,"school_id"=>$school_id,'user_type'=>2),'',true)){
                     display_output('0','You are not authorize to upload video for this school.');
                }
                if(!$school_name = $this->Common_model->getFieldValue('school','name',array("id"=>$school_id))){
                     display_output('0','School details not found.');
                }
                $check_video = $this->Common_model->getFieldValue('school_introductory_video','id',array("school_id"=>$school_id,'is_deleted'=>0));
                $insert_data = array(
                    'title' => $title,
                    'description' => $description,
                    'user_id' => $user_id,
                    'school_id' => $school_id,
                    'media_type' => 1,
                    'status' => 1,
                );
               
                if($type=='web'){

                    $format="video/mp4";
                    $s3 = new S3(AMAZON_ID, AMAZON_KEY);
                    if(!is_dir(MEDIA_THUMB_TEMP_PATH)){
                        mkdir(MEDIA_THUMB_TEMP_PATH);
                    } 
                    if(!empty($_FILES['file_upload'])){
                        if(!empty($_FILES['file_upload']['name'])){
                            $tmp_name = $_FILES['file_upload']['tmp_name'];
                            $ext = strtolower(substr(strrchr($_FILES['file_upload']['name'], '.'), 1)); 
                            $thumb_image="";
                            $thumb_imagePath="";
                            $large_image="";
                            $large_imagePath="";
                            if($ext=='avi' || $ext=='mov' || $ext=='mp4') {
                                $duration = get_duration($tmp_name);
                                $media_name = 'video'.'_'.time().'_v.mp4';
                                $images = createImage($tmp_name,MEDIA_THUMB_TEMP_PATH,"800x450",$duration);
                                $duration=gmdate("H:i:s", $duration);
                                $thumb_image = $images[1];
                                $large_image = $images[0];
                                $thumb_imagePath = MEDIA_THUMB_TEMP_PATH.$thumb_image;
                                $large_imagePath = MEDIA_THUMB_TEMP_PATH.$large_image;
                                //Convert and upload video on server
                                $newSource =MEDIA_PATH.$media_name;
                                $command = 'ffmpeg -i ' . $tmp_name . ' -r 21 -vcodec libx264 -b:v 1M -crf 32 -preset veryfast -profile:v baseline -level 3.0 -c:a copy -movflags +faststart -tune zerolatency '.$newSource ;
                                 $output = shell_exec($command);

                                $s3dir = 'school/';
                                $savemediapath = $s3dir.$media_name;
                                if($s3->putObjectFile($newSource, AMAZON_BUCKET, $savemediapath, S3::ACL_PUBLIC_READ)) 
                                {
                                    //upload media on the amazon s3
                                    $s3mediapath =AMAZON_PATH.$savemediapath;
                                    $media_name=$s3mediapath;
                                    $s3thumbPath='';
                                    $s3largeimagePath='';
                                    if($thumb_imagePath !='') {
                                        //store thumb image on the amazon s3 and delete temp file
                                        $save_path = $s3dir.$thumb_image;
                                        $s3->putObjectFile($thumb_imagePath, AMAZON_BUCKET, $save_path, S3::ACL_PUBLIC_READ);
                                        $s3thumbPath=AMAZON_PATH.$save_path;
                                        if(file_exists($thumb_imagePath)) {
                                            @unlink($thumb_imagePath);
                                        }
                                    }
                                    if($large_imagePath !='') {
                                        //store thumb image on the amazon s3 and delete temp file
                                        $save_path = $s3dir.$large_image;
                                        $s3->putObjectFile($large_imagePath, AMAZON_BUCKET, $save_path, S3::ACL_PUBLIC_READ);
                                        $s3largeimagePath=AMAZON_PATH.$save_path;
                                        if(file_exists($large_imagePath)) {
                                            @unlink($large_imagePath);
                                        }
                                    }
                                    if(file_exists($newSource)) {
                                        @unlink($newSource);
                                    }
                                }else{
                                    display_output('0','Some error occurred, try again.');
                                }
                            }else{
                                display_output('0','File must be avi or mov or mp4 .');
                            }
                            $insert_data['thumb_image']=$s3thumbPath;
                            $insert_data['large_image']=$s3largeimagePath;
                            $insert_data['media_name']=$media_name;
                            $insert_data['duration']=$duration;
                        }

                    }/*else{
                        display_output('0','Please upload file.');
                    }*/
                }else{
                    if(empty($s3thumbPath) || empty($s3largeimagePath))
                    {
                        $s3Images=getImageFromVideo($duration,1,$media_name,$mobile_type,$rotation,'school');
                        if(!empty($s3Images)){
                            $s3thumbPath=$s3Images['s3thumbPath'];
                            $s3largeimagePath=$s3Images['s3largeimagePath'];
                        }else{
                            $s3thumbPath="";$s3largeimagePath="";
                        }
                    }
                    $insert_data['thumb_image']=$s3thumbPath;
                    $insert_data['large_image']=$s3largeimagePath;
                    $insert_data['media_name']=$media_name;
                }
                
                $media_slug = create_slug_with_id($title,$school_id);
                $insert_data['media_slug']=$media_slug;
                
                if($check_video = $this->Common_model->getFieldValue('school_introductory_video','id',array("school_id"=>$school_id,'is_deleted'=>0))){
                    $insert_data['duration']=$duration;
                    $insert_data['modified']=date("Y-m-d H:i:s");
                    if($this->Common_model->addEditRecords('school_introductory_video',$insert_data,array('id'=>$check_video))) {
                        if(!isset($s3largeimagePath)){
                            $s3largeimagePath="";
                        }
                        display_output('1','Video successfully uploaded', array('media_id'=>(string)$check_video,'image_path'=>$s3largeimagePath,'insert_data'=>$insert_data));
                    } else {
                        display_output('0','Some error occured. Please try again.');
                    } 
                }else{
                     $insert_data = array(
                        'title' => $title,
                        'description' => $description,
                        'user_id' => $user_id,
                        'school_id' => $school_id,
                        'format' => $format,
                        'thumb_image' => $s3thumbPath,
                        'large_image' => $s3largeimagePath,
                        'media_name' => $media_name,
                        'media_type' => 1,
                        'status' => 1,
                        'duration' => $duration,
                        'media_slug' => $media_slug,
                    );
                    $insert_data['created']=date("Y-m-d H:i:s");
                    $insert_data['modified']=date("Y-m-d H:i:s");
                    if($media_id = $this->Common_model->addEditRecords('school_introductory_video',$insert_data)) {
                        display_output('1','Video successfully uploaded', array('media_id'=>(string)$media_id,'image_path'=>$s3largeimagePath,'added_data'=>$insert_data));
                    } else {
                        display_output('0','Some error occured. Please try again.');
                    } 
                }
                
            } 
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
    }


} //End controller class
