<?php defined('BASEPATH') or exit('No direct script access allowed');
class Common extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library(array('S3'));
        $this->load->model(array('Common_model', 'Teacher_model'));
        $this->load->helper(array('email', 'common_helper'));
        //$session = $this->session->userdata();
        if (!apiAuthentication()) {
            echo json_encode(array('status' => '0', 'msg' => "Error invalid api key"));
            header('HTTP/1.0 401 Unauthorized');
            die;
        }
    }

    /*============================= ConvertImageFromMEdia ==========================*/
    public function ConvertImageFromMedia()
    {
        try {
            check_user_status();
            if ($this->input->post()) {
                $postData = $this->input->post();
                $user_id = test_input($this->input->post('user_id'));
                if (!isset($postData['duration']) || empty($postData['duration'])) {
                    display_output('0', 'Enter duration.');
                } else {
                    $duration = test_input($postData['duration']);
                }
                if (!isset($postData['mobile_type']) || empty($postData['mobile_type'])) { //only for android
                    display_output('0', 'Enter your mobile type.');
                } else {
                    $mobile_type = test_input($postData['mobile_type']);
                }
                if (!isset($postData['rotation']) || empty($postData['rotation'])) { //only for android
                    display_output('0', 'Enter rotation.');
                } else {
                    $rotation = test_input($postData['rotation']);
                }
                if (!isset($postData['media_name']) || empty($postData['media_name'])) {
                    display_output('0', 'Enter media name');
                } else {
                    $media_name = test_input($postData['media_name']);
                }
                if (!isset($postData['call_for']) || empty($postData['call_for'])) {
                    display_output('0', 'Please enter call_for.');
                } else {
                    $call_for = test_input($postData['call_for']);
                }
                if (!in_array($call_for, array('school', 'teacher', 'student'))) {
                    display_output('0', 'Call for either school or teacher');
                }
                if ($call_for == 'school') {
                    $s3_dir = 'school';
                } else if ($call_for == 'student') {
                    if ($user_data = $this->Common_model->getRecords('users', 'user_number', array("user_id" => $user_id, 'user_type' => 4), '', true)) {
                        $s3_dir = $user_data['user_number'];
                    } else {
                        display_output('0', 'User detail not found');
                    }
                } else {
                    if ($user_data = $this->Common_model->getRecords('users', 'user_number', array("user_id" => $user_id, 'user_type' => 3), '', true)) {
                        $s3_dir = $user_data['user_number'];
                    } else {
                        display_output('0', 'User detail not found');
                    }
                }
                $s3Images = getImageFromVideo($duration, 1, $media_name, $mobile_type, $rotation, $s3_dir);
                if ($s3Images) {
                    display_output('1', 'Images', array('s3thumbPath' => $s3Images['s3thumbPath'], 's3largeimagePath' => $s3Images['s3largeimagePath']));
                } else {
                    display_output('0', 'Some error occured. Please try again.');
                }
            }
        } catch (Exception $e) {
            display_output('0', $e->getMessage());
        }
    }
    /*============================= video detail ==========================*/

    public function video_details()
    {
        try {
            check_user_status();
            if ($this->input->post()) {
                $postData = $this->input->post();
                $user_id = test_input($this->input->post('user_id'));
                if (!isset($postData['media_id']) || empty($postData['media_id'])) {
                    display_output('0', 'Please enter media id.');
                } else {
                    $media_id = test_input($postData['media_id']);
                }
                if (!isset($postData['call_type']) || empty($postData['call_type'])) {
                    display_output('0', 'Please enter call type.');
                } else {
                    $call_type = test_input($postData['call_type']);
                }
                if (!in_array($call_type, array('school', 'other'))) {
                    display_output('0', 'Please enter valid call type.');
                }
                if ($call_type == 'school') {
                    if (!$data['detail'] = $this->Teacher_model->getUploadShoolDetail($media_id)) {
                        display_output('0', 'No video found');
                    }
                    $data['current_time'] = $this->Common_model->getFieldValue('continue_watch_media', 'duration', array('user_id' => $user_id, 'media_id' => $media_id));
                    $data['detail']['current_duration'] = $data['current_time'];
                    $res_data = $data['detail'];
                    if (!empty($res_data)) {
                        if ($this->input->post('type') == 'web') {
                            $data['details'] = $res_data;
                            if ($call_type == 'school') {
                                $this->load->view('front/parent/principal-video-data', $data);
                            } else {

                                $res = $this->load->view('front/district/single_video_detail', $data, true);
                            }
                        } else {
                            display_output('1', 'Video details', array('video_details' => $res_data));
                        }
                    } else {
                        if ($this->input->post('type') == 'web') {
                            $data['details'] = "";
                            if ($call_type == 'school') {
                                $res = $this->load->view('front/parent/principal-video-data', $data);
                                exit;
                            } else {
                                $res = $this->load->view('front/district/single_video_detail', $data, true);
                            }
                        } else {
                            display_output('0', 'Some error occured. Please try again.');
                        }
                    }
                } else {
                    if (!$data['detail'] = $this->Teacher_model->getUploadDetail($media_id)) {
                        display_output('0', 'No video found');
                    }
                    $data['current_time'] = $this->Common_model->getFieldValue('continue_watch_media', 'duration', array('user_id' => $user_id, 'media_id' => $media_id));
                    $data['detail']['current_duration'] = $data['current_time'];
                    $data['detail']['parent_media_id'] = '';
                    $data['detail']['super_parent_media_id'] = '';
                    if ($data['detail']['created_as'] == 2) {
                        if ($media_comment = $this->Common_model->getRecords('comments', 'media_id', array('comment' => $media_id), '', true)) {
                            //echo $this->db->last_query();exit();
                            $data['detail']['parent_media_id'] = (string) $media_comment['media_id'];
                            $data['detail']['super_parent_media_id'] = $this->recursive_arr($media_id);
                        }
                    }
                    $today_date     = date('Y-m-d');
                    $yesterday_date = date('Y-m-d', strtotime(date('Y-m-d') . "-1 days"));
                    $requested_date = date('Y-m-d', strtotime($data['detail']['created']));

                    if ($requested_date == $today_date) {
                        $publish = 'Today';
                    } else if ($requested_date == $yesterday_date) {
                        $publish = 'Yesterday';
                    } else {
                        $publish = date('M d, Y', strtotime($requested_date));
                    }
                    $publish  = get_time_posted(date('Y-m-d H:i:s'), $requested_date);
                    $where = array('media_id' => $media_id, 'user_id' => $user_id);
                    $data['detail']['publish'] = $publish;
                    $data['detail']['like'] = (string) $this->Common_model->getNumRecords('likes', '', $where);

                    $data['detail']['total_comments'] = (string) $this->Common_model->getNumRecords('comments', '', array('media_id' => $media_id));
                    $res_data = $data['detail'];

                    if (!empty($res_data)) {
                        if ($this->input->post('type') == 'web') {
                            $data['details'] = $res_data;


                            $data['media_action'] = 'teacher-upload-video/' . $data['detail']['grade'] . '/' . $media_id;

                            if ($call_type == 'school') {

                                $res = $this->load->view('front/parent/principal-video-data', $data, true);
                                exit;
                            } else {
                                echo  $this->load->view('front/district/single_video_detail', $data, true);
                                exit;
                            }
                        } else {
                            display_output('1', 'Video details', array('video_details' => $res_data));
                        }
                    } else {
                        if ($this->input->post('type') == 'web') {
                            $data['details'] = "";

                            if ($call_type == 'school') {
                                $this->load->view('front/parent/principal-video-data', $data);
                            } else {
                                $res = $this->load->view('front/district/single_video_detail', $data, true);
                            }
                        } else {
                            display_output('0', 'Some error occured. Please try again.');
                        }
                    }
                }
            }
        } catch (Exception $e) {
            display_output('0', $e->getMessage());
        }
    }
    public function recursive_arr($id)
    {
        if ($media_comment = $this->Common_model->getRecords('comments', 'media_id', array('comment' => $id), '', true)) {

            $id = $media_comment['media_id'];
            return $this->recursive_arr($id);
        }
        return $id;
    }
    /*============================= all comments ==========================*/
    public function get_comments()
    {
        try {
            check_user_status();
            $postData = $this->input->post();
            $type = test_input($this->input->post('type'));
            if ($type == 'web') {
                $userId = $this->session->userdata('user_id');
            } else {
                $userId = test_input($this->input->post('user_id'));
            }
            /*if(!isset($postData['media_id']) || empty($postData['media_id'])){
                display_output('0','Please enter media id.');
            }else{
                $mediaId = test_input($postData['media_id']);
            }
            if(!isset($postData['parent_id']) || empty($postData['parent_id'])){
                display_output('0','Please enter parent id.');
            }else{
                $parentId = test_input($postData['parent_id']);
            }
            if(!isset($postData['limit']) || empty($postData['limit'])){
                display_output('0','Please enter limit.');
            }else{
                $limit = test_input($postData['limit']);
            }
            if(!isset($postData['page']) || empty($postData['page'])){
                display_output('0','Please enter page.');
            }else{
                $page = $postData['page'];
            } */

            //$userId = $this->session->userdata('user_id');
            $mediaId = $postData['media_id'];
            $parentId = $postData['parent_id'];
            $limit = $postData['limit'];
            $page = $postData['page'];

            $total_comments = (string) $this->Common_model->getNumRecords('comments', '', array('media_id' => $mediaId));

            $innerlimit = ($postData['innerlimit']) ? $postData['innerlimit'] : 0;
            $innerpage = ($postData['innerpage']) ? $postData['innerpage'] : 0;
            $requestType = @$postData['type'];
            $grade = (isset($postData['grade']) && !empty($postData['grade'])) ? $postData['grade'] : 0;
            $data = array(
                "media_id" => $mediaId,
                "parent_id" => array($parentId),
                "user_id" => $userId,
                "limit" => $limit,
                "request_type" => $postData['request_type'],
                "offset" => ($page * $limit)
            );
            $getTotalComments = $this->Common_model->getTotalComments(array(
                "media_id" => $mediaId, "request_type" => $postData['request_type'],
                "parent_id" => array($parentId)
            ));

            $getParentComments = $this->Common_model->getMediaComments($data);
            // echo $this->db->last_query();exit;
            $replyIds = $getChildComments = [];
            foreach ($getParentComments as $key => $value) {
                $replyIds[] = $value['id'];
                $getParentComments[$key]['name'] = $value['first_name'] . " " . $value['last_name'];
                $getParentComments[$key]['reply'] = [];
                $getParentComments[$key]['reply_count'] = 0;

                $commentDate = date('Y-m-d', strtotime($value['comment_publish']));

                unset($getParentComments[$key]['first_name']);
                unset($getParentComments[$key]['last_name']);
            }
            if (!empty($replyIds)) {
                $childData = array(
                    "media_id" => $mediaId,
                    "parent_id" => $replyIds,
                    "user_id" => $userId,
                    "limit" => 0,
                    "offset" => 0, //($innerpage*$innerlimit)
                );
                $getChildComments = $this->Common_model->getMediaComments($childData);
            }
            if (!empty($getChildComments)) {
                foreach ($getParentComments as $key => $value) {
                    $mediaIds[] = $value['media_id'];
                    foreach ($getChildComments as $ckey => $cvalue) {
                        if ($value['id'] == $cvalue['parent_id']) {
                            $getParentComments[$key]['load_more'] = 0;
                            if ($getParentComments[$key]['reply_count'] < $innerlimit) {
                                $cvalue['name'] = $cvalue['first_name'] . " " . $cvalue['last_name'];

                                unset($cvalue['first_name']);
                                unset($cvalue['last_name']);
                                $getParentComments[$key]['reply'][] = $cvalue;
                            } else {
                                $getParentComments[$key]['load_more'] = 1;
                            }
                            $getParentComments[$key]['next_page'] = 1;
                            $getParentComments[$key]['reply_count'] += 1;
                        }
                    }
                }
            }

            $data['comment_count'] = $getTotalComments['comment_count'];
            $data['comments'] = $getParentComments;
            $data['load_more'] = (($data['comment_count'] - ($page * $limit + $limit)) > 0) ? 1 : 0;
            $data['parent_id'] = $parentId;
            $data['limit'] = $limit;
            $data['next_page'] = 0;
            $data['innerlimit'] = $innerlimit;
            $data['innerpage'] = $innerpage;
            if ($data['load_more'] > 0) {
                $data['next_page'] = $page + 1;
            }
            $data['grade'] = $grade;

            $responseData = array("status" => "1", "details" => $data);
            if ($requestType != 'web') {
                display_output('1', 'Comment List', array('details' => $data));
                /*echo json_encode(array("data" => $responseData));
                exit;*/
            }
            // echo '<pre>';
            // print_r($data['comments']); 
            // die;
            $html = $this->load->view("comment", $data, true);
            $responseData["html"] = $html;
            $responseData["total_comments"] = $total_comments;
            echo json_encode($responseData);
            exit;
        } catch (Exception $e) {
            display_output('0', $e->getMessage());
        }
    }

    /*========= district & school & parent get teachers video list =============*/
    public function all_media()
    {
        try {
            check_user_status();
            if ($this->input->post()) {
                $postData = $this->input->post();
                $user_id = test_input($this->input->post('user_id'));
                if (!isset($postData['user_type']) || empty($postData['user_type'])) {
                    display_output('0', 'Please enter user type.');
                } else {
                    $user_type = test_input($postData['user_type']);
                }
                $school_id = $teacher_id = $grade = "";
                $school_id = test_input($this->input->post('school_id'));
                $teacher_id = test_input($this->input->post('teacher_id'));
                $grade = test_input($this->input->post('grade'));

                if ($user_type == 2) {
                    if (empty($postData['school_id'])) {
                        display_output('0', 'Please enter school id.');
                    }
                }
                if ($user_type == 4) {
                    if (empty($postData['teacher_id'])) {
                        display_output('0', 'Please enter teacher id.');
                    }
                    if (!isset($postData['grade']) ||  ($postData['grade'] == "")) {
                        display_output('0', 'Please enter grade.');
                    }

                    if (!$this->Teacher_model->CheckTeacherApprovedStatus($grade, $teacher_id, $user_id)) {
                        display_output('0', 'Your request either pending or not found.');
                    }
                }


                if ($video_list = $this->Teacher_model->getAllVideos($user_id, $user_type, $school_id, $teacher_id, $grade)) {

                    $total_count = $this->Teacher_model->getAllVideosCount($user_id, $user_type, $school_id, $teacher_id, $grade);
                    $tot_cnt = "";
                    if ($total_count && $total_count[0] && $total_count[0]['count']) {
                        $tot_cnt = $total_count[0]['count'];
                    } else {
                        $tot_cnt = 0;
                    }
                    
                    //echo $this->db->last_query();exit;

                    if ($this->input->post('type') == 'web') {
                        $data['media_list'] = $video_list;
                        $res = $this->load->view('front/district/media_list', $data, true);

                        display_output('1', 'Video List', array('video_list' => $video_list, 'html' => $res, 'total_media_count' => $tot_cnt));
                    } else {
                        display_output('1', 'Video List', array('video_list' => $video_list, 'total_media_count' => $tot_cnt));
                    }
                } else {

                    if ($this->input->post('type') == 'web') {
                        display_output('0', 'No record found');
                    } else {
                        display_output('0', 'No record found.');
                    }
                }
            }
        } catch (Exception $e) {
            display_output('0', $e->getMessage());
        }
    }
    /*=========== comment for all videos ============*/
    public function comment()
    {   
        try {
            check_user_status();
            if ($this->input->post()) {
                $postData = $this->input->post();
                $type = test_input($this->input->post('type'));
                if ($type == 'web') {
                    $user_id = $this->session->userdata('user_id');
                } else {
                    $user_id = test_input($this->input->post('user_id'));
                }
                if (!isset($postData['media_id']) || empty($postData['media_id'])) {
                    display_output('0', 'Please enter media id.');
                } else {
                    $id   =  test_input($postData['media_id']);
                }
                if (!isset($postData['comment']) || empty($postData['comment'])) {
                    display_output('0', 'Leave a reply');
                } else {
                    $comment   =  test_input($postData['comment']);
                }
                $date = date("Y-m-d H:i:s");
                $insert_data = array(
                    'user_id' => $user_id,
                    'media_id' => $id,
                    'comment_type' => 2,
                    'comment' => $comment,
                    'created' => $date
                );

                $username = getUserName($user_id);

                if ($comment_id = $this->Common_model->addEditRecords('comments', $insert_data)) {

                    /*============= Push Notification start  ================*/

                    if ($media_record = $this->Common_model->getRecords('media', 'title,user_id,media_type,media_slug,teacher_grade_id', array('id' => $id), '', true)) {
                        $media_title = $media_record['title'];
                        if ($user_id != $media_record['user_id']) {
                            $message = ucfirst($username) . " commented on your video titled '" . $media_title . "'.";
                            addNotification($user_id, $media_record['user_id'], 'Comment', $message, $id, $id,'comment');
                            sendNotification($media_record['user_id'], $message,'comment');
                        }

                        /*============Push Notification end ===================*/
                        $mediayusers = $this->Common_model->mediaNotifyUsers($media_record['teacher_grade_id']);
                        if(!empty($mediayusers)){
                            foreach ($mediayusers as $muser) {
                                if ($user_id != $muser['parent_id']) {
                                    $message = ucfirst($username) . " commented on video titled '" . $media_title . "'.";
                                    addNotification($user_id, $muser['parent_id'], 'Comment', $message, $id, $id,'comment');
                                    sendNotification($muser['parent_id'], $message,'comment');
                                }
                            }
                        }
                        // if ($user_record = $this->Common_model->getRecords('users', 'email,mobile_auth_token,device_id,device_type,profile_image', array('user_id' => $media_record['user_id']), '', true)) {
                        // }
                    }

                    $comment_parent = $this->Common_model->getNumRecords('comments', '', array('media_id' => $id));
                    $comment_poster = $this->Common_model->getRecords('users', "group_concat(first_name,' ',last_name) as fullname,profile_image", array('user_id' => $user_id), '', true);
                    $details = array();
                    $details['comment'] = $comment;
                    $details['comment_publish'] = $date;
                    $details['comment_type'] = 2;
                    $details['id'] = $comment_id;
                    $details['media_id'] = $id;
                    $details['user_id'] = $user_id;
                    $details['name'] = $comment_poster['fullname'];
                    $details['profile_image'] = $comment_poster['profile_image'];
                    $details['parent_id'] = 0;

                    $total_comments = $this->Common_model->getNumRecords('comments', '', array('media_id' => $id));
                    $this->Common_model->addEditRecords('media', array('comments' => $total_comments), array('id' => $id));
                    display_output('1', 'Comment posted successfully.', array('details' => $details));
                } else {
                    display_output('0', 'No record found.');
                }
            }
        } catch (Exception $e) {
            display_output('0', $e->getMessage());
        }
    }


    /*============ comment reply  ===========*/
    public function comment_reply()
    {
        try {
            check_user_status();
            if ($this->input->post()) {
                $postData = $this->input->post();
                $type = test_input($this->input->post('type'));
                if ($type == 'web') {
                    $user_id = $this->session->userdata('user_id');
                } else {
                    $user_id = test_input($this->input->post('user_id'));
                }
                if (!isset($postData['media_id']) || empty($postData['media_id'])) {
                    display_output('0', 'Please enter media id.');
                } else {
                    $media_id   =  test_input($postData['media_id']);
                }
                if (!isset($postData['comment_id']) || empty($postData['comment_id'])) {
                    display_output('0', 'Please enter comment id.');
                } else {
                    $comment_id   =  test_input($postData['comment_id']);
                }
                if (!isset($postData['reply']) || empty($postData['reply'])) {
                    display_output('0', 'Leave a reply');
                } else {
                    $reply   =  test_input($postData['reply']);
                }
                $username = getUserName($user_id);
                $email_user_id = array();
                $eid = $comment_id;
                $index = 0;
                $parent_id = 0;
                do {
                    $email_user_id[$index] = $this->Common_model->getRecords('comments', 'id,user_id,parent_id', array('media_id' => $media_id, 'id' => $eid), '', true);
                    $eid = $email_user_id[$index]['parent_id'];
                    $parent_id = $email_user_id[$index]['parent_id'];
                    $index++;
                } while ($parent_id > 0);
                $date = date("Y-m-d H:i:s");
                $insert_data = array(
                    'parent_id' => $comment_id,
                    'media_id' => $media_id,
                    'user_id' => $user_id,
                    'comment_type' => 2,
                    'comment' => $reply,
                    'created' => $date
                );
                if ($new_comment_id = $this->Common_model->addEditRecords('comments', $insert_data)) {
                    //Notification email to post owner
                    if ($media_record = $this->Common_model->getRecords('media', 'user_id,title,media_slug,media_type,teacher_grade_id', array('id' => $media_id), '', true)) {

                        $media_title = $media_record['title'];
                        if ($user_record = $this->Common_model->getRecords('users', 'group_concat(first_name," ",last_name) as fullname,email,mobile_auth_token,device_id,device_type,profile_image', array('user_id' => $media_record['user_id']), '', true)) {
                            $to_email = $user_record['email'];
                            $data['name'] = $user_record['fullname'];

                            /*=================Push Notification start ===================*/
                            $media_type = 'video';
                            if ($to_get_id = $this->Common_model->getRecords('comments', 'user_id,media_id,comment_type', array('id' => $comment_id), '', true)) {
                                $comment_type = 'comment';
                                if ($user_id != $media_record['user_id']) {
                                    //send to media owner
                                    /*$message = ucfirst($username) . " replied to " . getUserName($to_get_id['user_id']) . "'s " . $comment_type . " on your " . $media_type . " titled '" . $media_title . "'.";*/
                                    $message = ucfirst($username) . " replied on your video titled '" . $media_title . "'.";
                                    //addNotification($user_id,$media_record['user_id'],'Comment',$message,$new_comment_id,$to_get_id['media_id']); 
                                    //addNotification($user_id,$media_record['user_id'],'Comment',$message,$to_get_id['media_id'],$to_get_id['media_id']); 
                                    //send notification
                                    //sendNotification($media_record['user_id'],$message);
                                    //send to comment owner

                                }
                                if ($user_id != $to_get_id['user_id']) {
                                    //$message = ucfirst($username)." replied to your ".$comment_type." on ".$media_type." titled '". $media_title."'.";  
                                    // $message = ucfirst($username) . " replied to " . getUserName($to_get_id['user_id']) . "'s " . $comment_type . " on your " . $media_type . " titled '" . $media_title . "'.";
                                     // $message = ucfirst($username) . " replied to " . getUserName($to_get_id['user_id']) . "'s " . $comment_type . " on your " . $media_type . " titled '" . $media_title . "'.";
                                    $message = ucfirst($username) . " replied on your video titled '" . $media_title . "'.";
                                    //addNotification($user_id,$to_get_id['user_id'],'Comment',$message,$new_comment_id,$to_get_id['media_id']); 
                                    addNotification($user_id, $to_get_id['user_id'], 'Comment', $message, $to_get_id['media_id'], $to_get_id['media_id'],'comment');
                                    //send notification
                                    sendNotification($media_record['user_id'], $message,'comment');
                                }
                                $mediayusers = $this->Common_model->mediaNotifyUsers($media_record['teacher_grade_id']);
                                if(!empty($mediayusers)){
                                    foreach ($mediayusers as $muser) {
                                        if ($user_id != $muser['parent_id']) {
                                            $message = ucfirst($username) . " replied on your video titled '" . $media_title . "'.";
                                            addNotification($user_id, $muser['parent_id'], 'Comment', $message, $to_get_id['media_id'], $to_get_id['media_id'],'comment');
                                            //send notification
                                            sendNotification($muser['parent_id'], $message,'comment');
                                        }
                                    }
                                }
                            }
                            /*=============== Push Notification end ================*/
                        }


                        $comment_poster = $this->Common_model->getRecords('users', 'group_concat(first_name," ",last_name) as fullname,profile_image', array('user_id' => $user_id), '', true);
                        $details = array();
                        $details['comment'] = $reply;
                        $details['comment_publish'] = $date;
                        $details['comment_type'] = 2;
                        $details['id'] = $new_comment_id;

                        $details['media_id'] = $media_id;
                        $details['user_id'] = $user_id;
                        $details['name'] = $comment_poster['fullname'];
                        $details['profile_image'] = $comment_poster['profile_image'];
                        $details['parent_id'] = $comment_id;

                        $total_comments = $this->Common_model->getNumRecords('comments', '', array('media_id' => $media_id));
                        $this->Common_model->addEditRecords('media', array('comments' => $total_comments), array('id' => $media_id));
                        display_output('1', 'Reply posted successfully.', array('details' => $details));
                    }
                } else {
                    display_output('0', 'Some error occured. Please try again.');
                }
            }
        } catch (Exception $e) {
            display_output('0', $e->getMessage());
        }
    }
    /*============ comment like & unlike ===========*/
    public function comment_like_unlike()
    {
        try {
            check_user_status();
            if ($this->input->post()) {
                $postData = $this->input->post();
                $type = test_input($this->input->post('type'));
                if ($type == 'web') {
                    $user_id = $this->session->userdata('user_id');
                } else {
                    $user_id = test_input($this->input->post('user_id'));
                }

                if (!isset($postData['comment_id']) || empty($postData['comment_id'])) {
                    display_output('0', 'Please enter comment id.');
                } else {
                    $id   =  test_input($postData['comment_id']);
                }
                $where = array('comment_id' => $id, 'user_id' => $user_id);

                if ($this->Common_model->getRecords('comment_like', 'id', $where, '', true)) {
                    $this->Common_model->deleteRecords('comment_like', $where);
                    $comment_data = $this->Common_model->getRecords('comments', 'user_id,media_id', array('id' => $id), '', true);
                    removeNotification($user_id, 'Comment like', $id, $comment_data['media_id']);

                    $total_like = $this->Common_model->getNumRecords('comment_like', '', array('comment_id' => $id));
                    display_output('1', 'unliked', array('details' => (string) $total_like));
                } else {

                    $insert_data = array(
                        'user_id' => $user_id,
                        'comment_id' => $id,
                        'created' => date("Y-m-d H:i:s")
                    );
                    $this->Common_model->addEditRecords('comment_like', $insert_data);
                    //echo 'dis_like_ins'.$this->db->last_query();
                    if ($comment_data = $this->Common_model->getRecords('comments', 'user_id,media_id', array('id' => $id), '', true)) {
                        // echo 'dis_like_ins1'.$this->db->last_query();
                        if ($media_name = $this->Common_model->getRecords('media', 'title,user_id,media_type,media_slug', array('id' => $comment_data['media_id']), '', true)) {
                            $media_type = 'video';

                            if ($user_id) {
                                if ($user_id != $comment_data['user_id']) {
                                    $notification_message = getUserName($user_id) . " liked your comment on " . $media_type . " titled '" . $media_name['title'] . "'.";

                                    //addNotification($user_id,$comment_data['user_id'],'Comment like',$notification_message,$id,$comment_data['media_id']);
                                    addNotification($user_id, $comment_data['user_id'], 'Comment like', $notification_message, $comment_data['media_id'], $comment_data['media_id'],'comment');

                                    sendNotification($comment_data['user_id'], $notification_message,'comment');
                                }
                            }
                        }
                    }
                    /*================ Notification start  ===========*/
                    /*===Push Notification end ================*/
                    $total_like = $this->Common_model->getNumRecords('comment_like', '', array('comment_id' => $id));

                    display_output('1', 'liked', array('details' => (string) $total_like));
                }
            }
        } catch (Exception $e) {
            display_output('0', $e->getMessage());
        }
    }

    /*============ comment report ===========*/
    public function comment_report()
    {
        try {
            check_user_status();
            if ($this->input->post()) {
                $postData = $this->input->post();
                $type = test_input($this->input->post('type'));
                if ($type == 'web') {
                    $user_id = $this->session->userdata('user_id');
                } else {
                    $user_id = test_input($this->input->post('user_id'));
                }
                if (!isset($postData['comment_id']) || empty($postData['comment_id'])) {
                    display_output('0', 'Please enter comment id.');
                } else {
                    $id   =  test_input($postData['comment_id']);
                }
                if (!isset($postData['report_type']) || empty($postData['report_type'])) {
                    display_output('0', 'Please enter report type.');
                } else {
                    $report_type   =  test_input($postData['report_type']);
                }
                if (!isset($postData['reason']) || empty($postData['reason'])) {
                    display_output('0', 'Please enter reason.');
                } else {
                    $reason   =  test_input($postData['reason']);
                }
                if ($comment_poster = $this->Common_model->getRecords('comment_report', 'id', array('comment_id' => $id, 'user_id' => $user_id), '', true)) {
                    display_output('0', 'You already reported on this comment and you will receive a response shortly.');
                }
                $insert_data = array(
                    'user_id' => $user_id,
                    'comment_id' => $id,
                    'type' => $report_type,
                    'reason' => $reason,
                    'created' => date("Y-m-d H:i:s")
                );

                $comment_data = $this->Common_model->getRecords('comments', 'id,user_id,media_id', array('id' => $id), '', true);
                $user_data = $this->Common_model->getRecords('users', 'school_id,first_name,last_name,user_type,display_name', array('user_id' => $user_id), '', true);
                $media_title = $this->db->get_where('media', array('id' => $comment_data['media_id']))->row()->title;
                $name = "";
                $replytype = "";
                if ($user_data['user_type'] == 3) {
                    $replytype = "Teacher";
                    $name = ucwords($user_data['display_name']);
                }
                if ($user_data['user_type'] == 4) {
                    $replytype = "Parent";
                    $name = ucwords($user_data['first_name'] . ' ' . $user_data['last_name']);
                }

                $notification_message = 'Your comment is reported by ' . $name . ' on media "' . ucwords($media_title) . '"';

                $schoolmsg = $replytype . ' reported on a comment on media  "' . ucwords($media_title) . '"  ' . $name;

                $Report_to = $comment_data['user_id'];
                $school_id = $user_data['school_id'];
                $school_id = $this->db->get_where('users', array('school_id' => $school_id, 'user_type' => 2))->row()->user_id;

                addNotification($user_id, $Report_to, 'Report Comment', $notification_message, $comment_data['id'], $comment_data['media_id'],'comment');
                addNotification($user_id, $school_id, 'Report Comment', $schoolmsg, $comment_data['id'], $comment_data['media_id'],'comment');
                if ($this->Common_model->addEditRecords('comment_report', $insert_data)) {
                    display_output('1', 'Comment successfully reported');
                } else {
                    display_output('0', 'Some error occured. Please try again.');
                }
            }
        } catch (Exception $e) {
            display_output('0', $e->getMessage());
        }
    }

    /*============ media_like_unlike ===========*/
    public function media_like_unlike()
    {
        try {
            check_user_status();
            if ($this->input->post()) {
                $postData = $this->input->post();
                $type = test_input($this->input->post('type'));
                if ($type == 'web') {
                    $user_id = $this->session->userdata('user_id');
                } else {
                    $user_id = test_input($this->input->post('user_id'));
                }
                if (!isset($postData['media_id']) || empty($postData['media_id'])) {
                    display_output('0', 'Please enter media id.');
                } else {
                    $id   =  test_input($postData['media_id']);
                }
                $created_by = $user_id;
                $where = array('media_id' => $id, 'user_id' => $user_id);

                if ($this->Common_model->getRecords('likes', 'id', $where, '', true)) {
                    $updatepass = $this->Common_model->deleteRecords('likes', $where);
                    removeNotification($created_by, 'Like', $id, $id);

                    if ($media_user_id = $this->Common_model->getFieldValue('media', 'user_id', array('id' => $id))) {
                        removeNotificationCount($media_user_id, $created_by, 'Like', $id, $id);
                    }

                    $total_likes = $this->Common_model->getNumRecords('likes', '', array('media_id' => $id));

                    $this->Common_model->addEditRecords('media', array('likes' => $total_likes), array('id' => $id));
                    display_output('1', 'unliked', array('details' => (string) $total_likes));
                } else {

                    $insert_data = array(
                        'user_id' => $user_id,
                        'media_id' => $id,
                        'created' => date("Y-m-d H:i:s")
                    );
                    $this->Common_model->addEditRecords('likes', $insert_data);

                    /*========= Notification start  ==============*/
                    if ($media_name = $this->Common_model->getRecords('media', 'title,user_id,media_type', array('id' => $id), '', true)) {
                        $media_type = 'video';

                        if ($user_id) {
                            if ($user_id != $media_name['user_id']) {
                                $notification_message = getUserName($user_id) . " liked your " . $media_type . " titled '" . $media_name['title'] . "'.";
                                addNotification($user_id, $media_name['user_id'], 'Like', $notification_message, $id, $id,'video');
                                sendNotification($media_name['user_id'], $notification_message,'video');
                            }
                        }
                    }

                    /*=====================push Notification end ===================*/
                    $total_likes = $this->Common_model->getNumRecords('likes', '', array('media_id' => $id));

                    $this->Common_model->addEditRecords('media', array('likes' => $total_likes), array('id' => $id));
                    display_output('1', 'liked', array('details' => (string) $total_likes));
                }
            }
        } catch (Exception $e) {
            display_output('0', $e->getMessage());
        }
    }
    /*========= Teacher chat list =============*/
    public function teacher_chat_list()
    {
        try {
            check_user_status();
            if ($this->input->post()) {
                $postData = $this->input->post();
                $user_id = test_input($this->input->post('user_id'));
                if (!isset($postData['school_id']) || empty($postData['school_id'])) {
                    display_output('0', 'Please enter school id.');
                } else {
                    $school_id = test_input($postData['school_id']);
                }
                if (!$user_details = $this->Common_model->getRecords(
                    'users',
                    'user_id',
                    array('user_id' => $user_id, 'user_type' => 3, 'school_id' => $school_id),
                    '',
                    true
                )) {
                    display_output('0', 'User details not found');
                }
                $keyword = "";
                if (isset($postData['keyword'])) {
                    $keyword = test_input($postData['keyword']);
                }
                if ($user_list = $this->Teacher_model->getTeacherChatList($user_id, $school_id, $keyword)) {
                    display_output('1', 'User List', array('user_list' => $user_list));
                } else {
                    display_output('0', 'No record found.');
                }
            }
        } catch (Exception $e) {
            display_output('0', $e->getMessage());
        }
    }
    /*========= Parent chat list =============*/
    public function parent_chat_list()
    {
        try {
            check_user_status();
            if ($this->input->post()) {
                $postData = $this->input->post();
                $user_id = test_input($this->input->post('user_id'));
                if (!isset($postData['school_id']) || empty($postData['school_id'])) {
                    display_output('0', 'Please enter school id.');
                } else {
                    $school_id = test_input($postData['school_id']);
                }
                if (!$user_details = $this->Common_model->getRecords(
                    'users',
                    'user_id',
                    array('user_id' => $user_id, 'user_type' => 4, 'school_id' => $school_id),
                    '',
                    true
                )) {
                    display_output('0', 'User not found.');
                }
                $keyword = "";
                if (isset($postData['keyword'])) {
                    $keyword = test_input($postData['keyword']);
                }

                /* $this->Teacher_model->getParentChatList($user_id,$school_id,$keyword);
                echo $this->db->last_query();exit;*/
                $all_grades = $this->Teacher_model->getUniqueGradeCommaSeprated($user_id);

                if ($user_list = $this->Teacher_model->getParentChatList($user_id, $school_id, $all_grades['grade'], $keyword)) {
                    //echo $this->db->last_query();exit;
                    display_output('1', 'User List', array('user_list' => $user_list));
                } else {
                    display_output('0', 'No record found.');
                }
            }
        } catch (Exception $e) {
            display_output('0', $e->getMessage());
        }
    }
    /*==================Notification======================*/
    public function notification_setting()
    {
        try {
            check_user_status();
            if ($this->input->post()) {
                $postData = $this->input->post();
                $user_id = test_input($this->input->post('user_id'));
                if (!isset($postData['action'])) {
                    display_output('0', 'Please enter action.');
                } else {
                    $action = test_input($postData['action']);
                }
                if ($action != '0' && $action != '1') {
                    display_output('0', 'Action must be either 0 or 1.');
                }
                $data = array(
                    'notification_flag' => $action,
                    'modified' => date("Y-m-d H:i:s"),
                );
                if ($this->Common_model->addEditRecords('users', $data, array('user_id' => $user_id))) {
                    display_output('1', '');
                } else {
                    display_output('0', 'Some error occured. Please try again.');
                }
            }
        } catch (Exception $e) {
            display_output('0', $e->getMessage());
        }
    }
    public function notification_list()
    {
        try {
            check_user_status();
            if ($this->input->post()) {
                $postData = $this->input->post();
                if ($this->input->post('type') == 'web') {
                    $user_id = $this->session->userdata('user_id');
                    $user_type = $this->session->userdata('front_user_type');
                } else {
                    $user_id = test_input($this->input->post('user_id'));
                }
                $page =   test_input($this->input->post('page'));
                $limit  =  test_input($this->input->post('limit'));
                if (!$page) {
                    $page = 0;
                }
                if (!$limit) {
                    $limit = APP_PAGE_LIMIT;
                }

                $start = $limit * $page;
                $user_data = $this->Common_model->getRecords('users', 'user_id,created', array('user_id' => $user_id), '', true);
                $st_time = date("Y-m-d H:i:s");
                $new_time = date("Y-m-d H:i:s", strtotime("-24 hours"));

                $where = "to_user=$user_id AND is_deleted =0 AND created_by !=$user_id and created_datetime between '$new_time' and '$st_time'";
                if ($this->input->post('type') == 'web') {
                    $notifications = $this->Common_model->getRecords('notification', '*,(select profile_image from users where users.user_id= notification.created_by) as profile_image', $where, 'id DESC', false, '');
                } else {
                    $notifications = $this->Common_model->getRecords('notification', '*,(select profile_image from users where users.user_id= notification.created_by) as profile_image', $where, 'id DESC', false, '', $start, $limit);
                }
                //print_r($this->db->last_query($notifications));
                $this->Common_model->addEditRecords('notification', array('read' => '1'), array('to_user' => (string) $user_id));
                $this->Common_model->addEditRecords('users', array('unread_notification' => 0), array('user_id' => $user_id));
                if ($this->input->post('type') == 'web') {
                    $data['notifications'] = $notifications;
                    $this->load->view('front/notification_data', $data);
                } else {
                    if ($notifications) {
                        display_output('1', 'Notification List', array('details' => $notifications));
                    } else {
                        display_output('0', 'Notification not found.');
                    }
                }
            }
        } catch (Exception $e) {
            display_output('0', $e->getMessage());
        }
    }



    public function add_chat_user()
    {
        check_user_status();
        $chat_user_id = test_input($this->input->post('chat_user_id'));
        $user_id = test_input($this->input->post('user_id'));
        if ($user_id && $chat_user_id) {
            $whr=array('recipient_id' => $chat_user_id,'created_by' => $user_id,'recipient_group_id'=>0);
            if(!$this->Common_model->getRecords('chat_message_recipients', 'id',$whr, '', true)){
                $insert_data = array(
                    'message' => "0m02mx1z50",
                    'created_by' => $user_id,
                    'created' => date('Y-m-d H:i:s'),
                );
                $last_id = $this->Common_model->addEditRecords('chat_message', $insert_data);
                if ($last_id) {
                    $newInserData = array(
                        'recipient_id' => $chat_user_id,
                        'message_id' => $last_id,
                        'is_beMyDate' => 1,
                        'is_read' => 1,
                        'created_by' => $user_id
                    );
                    $Newlast_id = $this->Common_model->addEditRecords('chat_message_recipients', $newInserData);
                    if ($Newlast_id) {
                        $this->session->set_flashdata('privatechat_user_id',$chat_user_id);
                        echo json_encode(array('status' => 1, 'msg' => 'success'));
                        exit;
                    } else {
                        echo json_encode(array('status' => 1, 'msg' => 'fail'));
                        exit;
                    }
                } else { // 2
                    echo json_encode(array('status' => 1, 'msg' => 'fail'));
                    exit;
                }
            }else{
                $this->session->set_flashdata('privatechat_user_id',$chat_user_id);
                echo json_encode(array('status' => 1, 'msg' => 'success'));
                exit;
            }
        } else {
            echo json_encode(array('status' => 1, 'msg' => 'fail'));
            exit;
        }
    } // add_chat_user   

    public function delete_user_ingroup()
    {
        // echo 'hello 1'; die;
        check_user_status();
        $userId1 = $this->session->userdata('user_id');
        $user_Id = test_input($this->input->post('user_Id'));
        $group_Id = test_input($this->input->post('group_Id'));
        if ($user_Id && $group_Id) {
            $res = '';
            $update_data = array('deleted' => 1);
            $where = array('user_id' => $user_Id, 'group_id' => $group_Id);


            $res = $this->Common_model->addEditRecords('chat_user_group_assoc', $update_data, $where);

            // $ins_arr = array(
            //     'message' => '0m02mx1z50',
            //     'created_by' => $userId1,
            //     'created_at' => 'UTC_TIMESTAMP',
            //     'is_active' => 1,
            //     'deleted' => 0
            // );

            // $res1 = $this->Common_model->addEditRecords('chat_message',$ins_arr);

            // echo '<pre>'; 
            // print_r($res1);
            $timestamp = time() + date("Z");
            $date =  gmdate("Y-m-d H:i:s", $timestamp);

            $update_data = array(
                "updated" => $date,
            );

            $res2 = $this->Common_model->addEditRecords('chat_user_group_assoc', $update_data, array('group_id' => $group_Id));
            // echo $this->db->last_query();
            // echo '<pre>'; 
            // print_r($update_data); die;


            // $res3 =  $this->Common_model->deleteRecords('chat_message',array('id !=' =>$message_id, "message"=>'0m02mx1z50', "created_by"=>$userId1));


            if ($res) {
                echo json_encode(array('status' => 1, 'msg' => 'success', 'res2' => $res2, 'res' => $res));
                exit;
            } else {
                echo json_encode(array('status' => 0, 'msg' => 'fail'));
                exit;
            }
        }
    }
    public function delete_group_byadmin()
    {
        check_user_status();
        $user_Id = test_input($this->input->post('user_Id'));
        $group_Id = test_input($this->input->post('group_Id'));
        if ($user_Id && $group_Id) {
            $update_data = array('deleted' => 1);
            $where = array('created_by' => $user_Id, 'id' => $group_Id);
            $res = $this->Common_model->addEditRecords('chat_groups', $update_data, $where);

            if ($res) {
                echo json_encode(array('status' => 1, 'msg' => 'success'));
                exit;
            } else {
                echo json_encode(array('status' => 0, 'msg' => 'fail'));
                exit;
            }
        }
    }
} //End controller class
