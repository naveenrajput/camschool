<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service extends CI_Controller {
	public function __construct()
	{
		parent:: __construct();
		$this->load->model(array('admin/Common_model','admin/Service_model'));
		$this->load->helper('common_helper');
		$this->load->library('Ajax_pagination');
	}

	public function index()
	{
		$this->Common_model->check_login();
	}
	public function service_list()
	{ 
		$this->Common_model->check_login();
		
	    check_permission('45','view','yes');
    	if(check_permission('45','add')){$data['add_action']='admin/service_category/add';}
  		if(check_permission('45','edit')){$data['edit_action']='admin/service_category/edit';}
  		if(check_permission('45','delete')){$data['delete_action']='1';}
	  	
		
		$data['title']="Service Category | ".SITE_TITLE;
		$data['page_title']="Service Category";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Service Category',
			'link' => ""
		);

		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$like=array();
		$data['filter_title']='';
		if($this->input->get('title')){
			$data['filter_title']=$this->input->get('title');
		}
		
		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}
		
		$data['total_records']=$this->Service_model->get_service_list(0,0);
	 	
	 	$data['records_results']=$this->Service_model->get_service_list(ADMIN_LIMIT,$page);
		if($data['undeletable'] = $this->Common_model->group_By_Records('services','category_id')) {	
			$data['undeletable_ids'] = array();
			foreach($data['undeletable'] as $list) {
				$data['undeletable_ids'][] = $list['category_id'];
			}
		}
	 	
		$data['pagination']=$this->Common_model->paginate(site_url('admin/service_category/list'),$data['total_records']);
  		
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/service/service_list');
		$this->load->view('admin/include/footer');
	} 

	
	public function add_service() 
	{
		$this->Common_model->check_login();
	    check_permission('45','add','yes');
		$data['title']="Add Service Category| ".SITE_TITLE;
		$data['page_title']="Add Service Category";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Service Category List',
			'link' => site_url('admin/service_category/list')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Add Service category',
			'link' => ""
		);	
		
		if($this->input->post()) {
			
			$this->form_validation->set_rules('title', 'title', 'trim|required',array('required'=>'Please enter %s'));
			
			$this->form_validation->set_rules('status', 'status', 'trim|required',array('required'=>'Please select %s'));
		
			$this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

			if($this->form_validation->run()==TRUE) 
			{
				if($_FILES['image']['name'] =='') { 
					$this->session->set_flashdata('error', "Please upload image.");
				} else {
					if($_FILES['image']['error']==0) {
						$image_path = SERVICE_CATEGORY_PATH;
						$allowed_types = 'jpg|jpeg|png|JPG|PNG|JPEG';
						$file='image';
						$responce = BannerUpload($image_path,$allowed_types,$file);
				
						if($responce['status']==0){
							$data['upload_error'] = $responce['msg'];	
						} else {
							check_server_unique('service_category', 'title',$this->input->post('title') ,'','','Title');
							$insert_data = array(
								'title'=> $this->input->post('title'),
								'image'=> $responce['image_path'],
								'status'=> $this->input->post('status'),
								'created'=> date("Y-m-d H:i:s"),
								'created_by'=> $this->session->userdata('admin_id')
							);
					 		
							if(!$last_id = $this->Common_model->addEditRecords('service_category', $insert_data)) { 
			                	echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Some error occurred, Please try later .</div>')); exit;               
				            } else { 
				            	$admin_id = $this->session->userdata('admin_id');
								$admin_username = getAdminUsername($admin_id);
				            	$log_msg = $admin_username." has created a service category '".$this->input->post('title')."'";
					            actionLog('service',$last_id,'add',$log_msg,'Admin',$admin_id);    
				                echo json_encode(array('status'=>1,'message'=>'<div class="alert alert-success">Service category added successfully.</div>')); exit; 
				            } 
						}
					}else{
						$this->session->set_flashdata('error', "Invalid image, Please try again.");
					}
				}
			}else{
				echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Please enter all required field .</div>')); exit;
			}
		}
		
		$data['form_action']=site_url('admin/service_category/add');
		$data['back_action']=site_url('admin/service_category/list');
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/service/add_service');
		$this->load->view('admin/include/footer');
	}

	public function edit_service($id) 
	{
		
		$this->Common_model->check_login();
		check_permission('45','edit','yes');
		if(!$id) {
			redirect('pages/page_not_found');
		}
		$data['title']="Edit Service Category| ".SITE_TITLE;
		$data['page_title']="Edit Service Category";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Service Category List',
			'link' => site_url('admin/service_category/list')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Edit Service category',
			'link' => ""
		);	
		
		if(!$data['services']=$this->Common_model->getRecords('service_category','*',array('id'=>$id),'',true)) {
			redirect('pages/page_not_found');
		}

		if($this->input->post()) {
			
			$this->form_validation->set_rules('title', 'title', 'trim|required',array('required'=>'Please enter %s'));
			
			$this->form_validation->set_rules('status', 'status', 'trim|required',array('required'=>'Please select %s'));
			
		
			$this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

			if($this->form_validation->run()==TRUE) {
				check_server_unique('measurement_unit', 'title',$this->input->post('title'),'id',$id,'Title');

				$update_data = array(
					'title'=> $this->input->post('title'),
					'status'=> $this->input->post('status'),
					'modified_by'=> $this->session->userdata('admin_id'),
					'modified'=> date("Y-m-d H:i:s")
				);
				//echo "<pre>"; print_r($id); exit;	
		 		if(!$this->Common_model->addEditRecords('service_category', $update_data,array('id'=>$id))) {
					 echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Some error occurred, Please try later .</div>')); exit;
				} else {
					$admin_id = $this->session->userdata('admin_id');
					$admin_username = getAdminUsername($admin_id);
	            	$log_msg = $admin_username." has updated a service  '".$this->input->post('title')."'";
		            actionLog('service',$id,'update',$log_msg,'Admin',$admin_id);    
					echo json_encode(array('status'=>1,'message'=>'<div class="alert alert-success">Service category updated successfully.</div>')); exit; 
				}
			}else{
				echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Please enter all required field .</div>')); exit;
			}
		}
		
		$data['form_action']=site_url('admin/service_category/edit/'.$id);
		$data['back_action']=site_url('admin/service_category/list');

		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/service/edit_service');
		$this->load->view('admin/include/footer');

	}
	public function measurement_unit_list()
	{
		$this->Common_model->check_login();
		
	    check_permission('50','view','yes');
    	if(check_permission('50','add')){$data['add_action']='admin/measurement_unit/add';}
  		if(check_permission('50','edit')){$data['edit_action']='admin/measurement_unit/edit';}
  		if(check_permission('50','delete')){$data['delete_action']='1';}
	  	
		
		$data['title']="Measurement Unit List | ".SITE_TITLE;
		$data['page_title']="Measurement Unit List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Measurement Unit List',
			'link' => ""
		);

		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$like=array();
		$data['filter_title']='';
		if($this->input->get('title')){
			$data['filter_title']=$this->input->get('title');
		}
		
		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}
		
		$data['total_records']=$this->Service_model->get_measurement_unit_list(0,0);
	 	
	 	$data['records_results']=$this->Service_model->get_measurement_unit_list(ADMIN_LIMIT,$page);
		if($data['undeletable'] = $this->Common_model->group_By_Records('services_item','unit','','is_deleted')) {	
			$data['undeletable_ids'] = array();
			foreach($data['undeletable'] as $list) {
				$data['undeletable_ids'][] = $list['unit'];
			}
		}
	 	
		$data['pagination']=$this->Common_model->paginate(site_url('admin/measurement_unit/list'),$data['total_records']);
  		
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/service/measurement_unit_list');
		$this->load->view('admin/include/footer');
	} 
	
	public function add_measurement_unit() 
	{
		$this->Common_model->check_login();
	    check_permission('50','add','yes');
		$data['title']="Add Measurement Unit | ".SITE_TITLE;
		$data['page_title']="Add Measurement Unit";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Measurement Unit List',
			'link' => site_url('admin/measurement_unit/list')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Add Measurement Unit',
			'link' => ""
		);	
		
		if($this->input->post()) {
			
			$this->form_validation->set_rules('title', 'title', 'trim|required',array('required'=>'Please enter %s'));
			
			$this->form_validation->set_rules('status', 'status', 'trim|required',array('required'=>'Please select %s'));
		
			$this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

			if($this->form_validation->run()==TRUE) 
			{

				$insert_data = array(
					'title'=> $this->input->post('title'),
					'status'=> $this->input->post('status'),
					'created'=> date("Y-m-d H:i:s"),
				);
		 		check_server_unique('measurement_unit', 'title',$this->input->post('title') ,'','','Measurement unit');
				if(!$last_id = $this->Common_model->addEditRecords('measurement_unit', $insert_data)) { 
                	echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Some error occurred, Please try later .</div>')); exit;               
	            } else { 
	            	$admin_id = $this->session->userdata('admin_id');
					$admin_username = getAdminUsername($admin_id);
	            	$log_msg = $admin_username." has created a measurement unit '".$this->input->post('title')."'";
		            actionLog('service',$last_id,'add',$log_msg,'Admin',$admin_id);    
	                echo json_encode(array('status'=>1,'message'=>'<div class="alert alert-success">Measurement unit added successfully.</div>')); exit; 
	            } 
			}else{
				echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Please enter all required field .</div>')); exit;
			}
		}
		
		$data['form_action']=site_url('admin/measurement_unit/add');
		$data['back_action']=site_url('admin/measurement_unit/list');
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/service/add_measurement_unit');
		$this->load->view('admin/include/footer');
	}
	public function edit_measurement_unit($id) 
	{
		
		$this->Common_model->check_login();
		check_permission('50','edit','yes');
		if(!$id) {
			redirect('pages/page_not_found');
		}
		$data['title']="Edit Measurement Unit | ".SITE_TITLE;
		$data['page_title']="Edit Measurement Unit";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Measurement Unit List',
			'link' => site_url('admin/measurement_unit/list')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Edit Measurement Unit',
			'link' => ""
		);	
		
		if(!$data['units']=$this->Common_model->getRecords('measurement_unit','*',array('id'=>$id),'',true)) {
			redirect('pages/page_not_found');
		}

		if($this->input->post()) {
			
			$this->form_validation->set_rules('title', 'title', 'trim|required',array('required'=>'Please enter %s'));
			
			$this->form_validation->set_rules('status', 'status', 'trim|required',array('required'=>'Please select %s'));
			
		
			$this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

			if($this->form_validation->run()==TRUE) {
				check_server_unique('measurement_unit', 'title',$this->input->post('title'),'id',$id,'Measurement unit');
				$update_data = array(
					'title'=> $this->input->post('title'),
					'status'=> $this->input->post('status'),
					'modified'=> date("Y-m-d H:i:s")
				);
				//echo "<pre>"; print_r($id); exit;	
		 		if(!$this->Common_model->addEditRecords('measurement_unit', $update_data,array('id'=>$id))) {
					 echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Some error occurred, Please try later .</div>')); exit;
				} else {
					$admin_id = $this->session->userdata('admin_id');
					$admin_username = getAdminUsername($admin_id);
	            	$log_msg = $admin_username." has updated a measurement unit '".$this->input->post('title')."'";
		            actionLog('service',$id,'update',$log_msg,'Admin',$admin_id);    
					echo json_encode(array('status'=>1,'message'=>'<div class="alert alert-success">Measurement unit updated successfully.</div>')); exit; 
				}
			}else{
				echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Please enter all required field .</div>')); exit;
			}
		}
		
		$data['form_action']=site_url('admin/measurement_unit/edit/'.$id);
		$data['back_action']=site_url('admin/measurement_unit/list');

		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/service/edit_measurement_unit');
		$this->load->view('admin/include/footer');

	}



	public function user_services_list() {
		$this->Common_model->check_login();
		check_permission('54','view','yes');
		$data['title']="Service List | ".SITE_TITLE;
		$data['page_title']="Service List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Service List',
			'link' => ""
		);
		$page = $this->uri->segment(4) ? $this->uri->segment(4) : 0; 
		

		if(check_permission('54','edit')){$data['edit_action']=site_url('admin/services/edit');}
		if(check_permission('54','delete')){$data['delete_action']=site_url('admin/services/delete');}
 
		$data['is_indi'] = '';

		$data['filter_username']='';
		if($this->input->get('username')){
			$data['filter_username']=$this->input->get('username');
		}

		$data['filter_services_title']='';
		if($this->input->get('services_title')){
			$data['filter_services_title']=$this->input->get('services_title');
		}

		$data['filter_category_title']='';
		if($this->input->get('category_title')){
			$data['filter_category_title']=$this->input->get('category_title');
		}
		$data['filter_user_id']='';
		if($this->input->get('user_id')){
			$data['filter_user_id']=$this->input->get('user_id');
		} 

		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}
		$data['back_url'] = '';
		if(empty($data['filter_user_id'])){
			$data['reset_action']=site_url('admin/services/list');
			$urll = site_url('admin/services/list');
		}else{
			$data['reset_action']=site_url('admin/user/services_list?user_id='.$data['filter_user_id']);
			$data['back_url']=site_url('admin/user/list');
			$urll = site_url('admin/user/services_list');


		}

		
		$data['total_records']=$this->Service_model->getUserService(0,0);
	 	$data['records_results']=$this->Service_model->getUserService($page,ADMIN_LIMIT);
	 	// echo "<pre>";print_r($this->db->last_query());die;
		$data['pagination']=$this->Common_model->paginate($urll,$data['total_records']);


		

		$data['undeletable_ids'] = array(); 
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/user_service_list');	
		$this->load->view('admin/include/footer');
	}


	public function view_user_service($id) 
	{
		
		$this->Common_model->check_login();
		check_permission('54','view','yes');
		if(!$id) {
			redirect('pages/page_not_found');
		}
		$data['title']="View Service| ".SITE_TITLE;
		$data['page_title']="View Service";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Service List',
			'link' => site_url('admin/services/list')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'View Service',
			'link' => ""
		);	
		
		if(!$data['services']=$this->Service_model->getUserServicedetails($id)) {
			redirect('pages/page_not_found');
		}
		// echo "<pre>";print_r($this->db->last_query());die;

		$data['services_item']=$this->Service_model->getCategoryItem($id);

		 // echo "<pre>";print_r($data['services']);
		 // echo "<pre>";print_r($data['services_item']);die;
		
		$data['form_action']=site_url('admin/services/edit/'.$id);
		$data['back_action']=site_url('admin/services/list');
		// $data['back_action'] = base_url().'admin/services/list';

		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/view_user_service');
		$this->load->view('admin/include/footer'); 
	}


	public function service_offer_list($id) {
		$this->Common_model->check_login();
		check_permission('54','view','yes');
		$data['title']="Service Offer List | ".SITE_TITLE;
		$data['page_title']="Service Offer List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Service Offer List',
			'link' => ""
		);
		 $page = $this->uri->segment(5) ? $this->uri->segment(5) : 0;
		 
		$data['type'] = 1;
		$data['reset_action']=site_url('admin/services/offer/'.$id);
 
 
		$data['filter_offer_name']='';
		if($this->input->get('offer_name')){
			$data['filter_offer_name'] = trim($this->input->get('offer_name')); 
		}
		$data['filter_date_rang']='';
		if($this->input->get('date_rang')){
		 	$date_rang=trim($this->input->get('date_rang'));
            $split_date=explode(" - ",$date_rang);
            $start_date= date('Y-m-d',strtotime($split_date[0]));
            $end_date=date('Y-m-d',strtotime($split_date[1]));

            $data['filter_date_rang'] = $this->input->get('date_rang');
		 
		}
 

		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
			$where['status']=$this->input->get('status');
		}
 	

		$where['service_id'] = $id;
		$where['is_deleted'] = 0;

	 
		 
	 	$data['total_records']=$this->Service_model->getServiceOffer($id);
	
	 	$data['records_results']=$this->Service_model->getServiceOffer($id,$page,ADMIN_LIMIT);
 	
 		$data['back_action'] = base_url().'admin/services/list';
	 	
		$data['pagination']=$this->Common_model->paginate(site_url('admin/services/offer/'.$id),$data['total_records'],'seg5');

		$data['undeletable_ids'] = array(); 
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/service_offer_list');	
		$this->load->view('admin/include/footer');
	}
/*
	function getOfferDetails(){
		
	}
  */

	/***************** Email Templates *******************/
}
