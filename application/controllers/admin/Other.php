<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Other extends CI_Controller {

	public function __construct() {
		parent::__construct(); 
		$this->load->model('admin/Common_model');
		$this->load->model('admin/Other_model');
		
		$this->load->helper('common_helper');
		$this->load->library('PHPExcel');
		$this->load->library("excel");
		ini_set('max_execution_time', 0);
	}

	public function index()	{
		$this->Common_model->check_login(); 
		
	}

	/* Action Log list */	
	public function action_log_list() {
		$this->Common_model->check_login();
		check_permission('69','view','yes');
		$data['title']="Action Log List | ".SITE_TITLE;
		$data['page_title']="Action Log List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Action Log List',
			'link' => ""
		);
		$page = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
		
		$like=array();
		$data['filter_admin_name']='';
		if($this->input->get('admin_name')){
			$data['filter_admin_name']=$this->input->get('admin_name');
		}
		$data['filter_username']='';
		if($this->input->get('username')){
			$data['filter_username']=$this->input->get('username');
		}
		$data['filter_action_date']='';
		if($this->input->get('action_date')){
			$data['filter_action_date']=$this->input->get('action_date');
		}
		$data['filter_keyword']='';
		if($this->input->get('keyword')){
			$data['filter_keyword']=$this->input->get('keyword');
		}
		
		$data['total_records']=$this->Other_model->get_log_list(0,0);
	 	$data['records_results']=$this->Other_model->get_log_list(ADMIN_LIMIT,$page);
		$data['pagination']=$this->Common_model->paginate(site_url('admin/action_log/list'),$data['total_records']);

		
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/action_log_list');	
		$this->load->view('admin/include/footer');
	} 	
	/* Action Log list */	




	//add offers

} 