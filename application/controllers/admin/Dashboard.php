<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	
        $this->load->model(array('Common_model','admin/Dashboard_model','admin/User_model'));
       // $this->load->model(array('admin/Common_model','admin/F_model'));
		$this->load->helper('Common_helper'); 
	}

	public function index()
	{
		
		$this->Common_model->check_login();
		$type = $this->session->userdata('user_type');
		if($type=='Super Admin'){
			$type='Admin';
		}
		$role_id = $this->session->userdata('role_id');
		$data['title']="Dashboard| ".SITE_TITLE;
		$data['page_title']="Dashboard";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Home',
			'link' => 'admin/dashboard'
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		
		// echo "<pre>";print_r($data['total_order_payment']);die;
		switch ($type){
			case 'Admin':
			$data['totalDistricts'] = $this->Common_model->getNumRecords('users','user_id',array('user_type'=>1,'prospect_status'=>2,'is_deleted'=>0));
            $user_type=1;
			$data['totalDistrictsapprove'] =$this->Common_model->getNumRecords('users','user_id',array('user_type'=>1,'is_deleted'=>0,'is_approve'=>1));
			$data['totalDistrictsapproveurl'] = base_url('admin/prospect/list');

			$data['districtUrl'] = base_url('admin/user/list');
			$data['totalPrincipals'] = $this->Common_model->getNumRecords('users','user_id',array('user_type'=>2,'is_deleted'=>0));
			$data['principalUrl'] = base_url('admin/principal/list');
			$data['totalTeachers'] = $this->Common_model->getNumRecords('users','user_id',array('user_type'=>3,'is_deleted'=>0));
			$data['teacherUrl'] = base_url('admin/teacher/list');
			$data['totalParents'] = $this->Common_model->getNumRecords('users','user_id',array('user_type'=>4,'is_deleted'=>0));
			$data['parentUrl'] = base_url('admin/parent/list');
			$data['totalPendingSupports'] = $this->Common_model->getNumRecords('contact','id',array('parent_id'=>0,'status'=>1,'is_deleted'=>2));
			
			$data['supportUrl'] = base_url('admin/contact_us/list');
			$this->load->view('admin/include/header',$data);
			$this->load->view('admin/include/sidebar');
			if($this->session->userdata('user_type')!='Super Admin'){
	            if($this->Common_model->getRecords('role_permissions','view',array('role_id'=>$role_id,'section_id'=>1,'view'=>1),'',true)){
					$this->load->view('admin/dashboard');
				}else{
					$this->load->view('admin/prohibit_dashboard');
				}
			}else{
				$this->load->view('admin/dashboard');
			}
			
			$this->load->view('admin/include/footer');
                        # code...
            break;
    	}
	}
	
}

