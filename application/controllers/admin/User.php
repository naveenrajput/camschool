<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
    public function __construct()
    {
        parent:: __construct();
        $this->load->model(array('Common_model','admin/User_model'));
        $this->load->helper('common_helper');
        $this->load->library(array('PHPExcel','Ajax_pagination'));
        error_reporting(0);
    }

    public function index()
    {
        $this->Common_model->check_login();
    }


    public function rolekey_exists($key,$field) {
        $param=explode('-', $field);
        $table=$param[0];
        $col_name=$param[1];
        $type=$param[2];
        if($type=='add') {
            $where_cond = array($col_name=>$key,'is_deleted'=>0);
            if($this->Common_model->getRecords($table, $col_name, $where_cond, '', true)) {
                $this->form_validation->set_message(__FUNCTION__, 'This %s is already exist.');
                return false;
            } else {
                return true;
            }
        } else {
            $id_col=$param[3];
            $id_col_val=$param[4];
            $where_cond = array("$id_col!=" =>$id_col_val,$col_name=>$key,'is_deleted'=>0);
            if($this->Common_model->getRecords($table, $col_name, $where_cond, '', true)) {
                $this->form_validation->set_message(__FUNCTION__, 'This %s is already exist.');
                return false;
            } else {
                return true;
            }
        }
    }

   public function upload_user($id){
   
        $data['title']= 'Upload CSV' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Upload CSV'." | ".SITE_TITLE;
        $data['meta_desc']= 'Upload CSV'." | ".SITE_TITLE;
        $data['page_title'] = "Upload CSV";
        
        $data['title']="Upload List | ".SITE_TITLE;
        $data['page_title']="Upload List";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'icon'=>'<i class="fa fa-dashboard"></i>',
            'class'=>'',
            'title' => 'Dashboard',
            'link' => site_url('admin/dashboard')
        );

        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'active',
            'title' => 'upload List',
            'link' => ""
        );
        $data['csv_data'] =$this->Common_model->getRecords('csv_uploads', '*', array('user_id'=>$id), 'id desc', false);
        $data['user_id']=$id;
        $data['back_action']=site_url('admin/user/list');    
        $this->load->view('admin/include/header',$data);
        $this->load->view('admin/include/sidebar');
        $this->load->view('admin/user/csv-upload');
        $this->load->view('admin/include/footer');

       
   }
    public function import_file() 
    {   
        // ini_set('display_errors', 1); 
        // ini_set('display_startup_errors', 1); 
        error_reporting(0);
        $user_id = $this->input->post('user_id');
        $district_res =$this->Common_model->getRecords('users', 'district_id', array('user_id'=>$user_id,'user_type'=>1), 'user_id desc', true);
        if(empty($district_res)){
            $this->session->set_flashdata('error', 'District not found');
            redirect('admin/user/upload/'.$user_id);
        }
        $district_id=$district_res['district_id'];

        if(isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])){

            $fileName = time() .'_'. $_FILES['file']['name'];
            $grade_array=array('Pre-Kindergarten','Kindergarten','1st Grade','2nd Grade','3rd Grade','4th Grade','5th Grade','6th Grade','7th Grade','8th Grade','9th Grade','10th Grade','11th Grade','12th Grade');
            // $grade_array=array('Kindergarten','1st grade','2nd Grade','3rd Grade','4th Grade','5th grade','6th Grade','7th Grade','8th Grade','9th Grade','10th Grade','11th Grade','12th Grade');
            if($_FILES["file"]["size"] > 0)
            { 
                $config['upload_path'] = DISTRICT_CSV_PATH;                           
                $config['file_name'] = $fileName;
                $config['allowed_types'] = '*';        //some files does not contain proper file type
                $path_parts = pathinfo($_FILES["file"]["name"]);
                $extension = $path_parts['extension'];
                if(!in_array($extension, array('xls','xlsx'))){
                    $this->session->set_flashdata('error', 'File type not permitted.');
                    redirect('admin/user/upload/'.$user_id);
                }
                $this->load->library('upload');
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('file')){
                    $this->upload->display_errors();
                    $error = $this->upload->display_errors(); 
                    $this->session->set_flashdata('error', $error);
                    redirect('admin/user/upload/'.$user_id);
                 }
                 else{
                    $media = $this->upload->data();
                    $inputFileName = DISTRICT_CSV_PATH . $media['file_name'];
                }
                try {
                    
                    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFileName);

              
                } catch (Exception $e) {
                    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
                }
               
                $sheet = $objPHPExcel->getSheet(0);
                // $highestRow = $sheet->getHighestRow();
                $highestRow = $sheet->getHighestDataRow(); 
                $highestColumn = $sheet->getHighestColumn();
                $maxCell = $sheet->getHighestRowAndColumn();
                $err_flag=0;

                $objPHPExcel->getActiveSheet()->SetCellValue('P' . 1,'Error Messages');

                for ($row = 1; $row <= $highestRow; $row++) { 
                    $error_msg='';
                    // Read a row of data into an array                 
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

                    
                    if($row==1){
                        if (!empty($rowData[0][0])) {
                            if (trim($rowData[0][0])!='School’s name*') {
                                $err_flag=1;
                                $error_msg.='Column name'.' '.$rowData[0][0].' '.'is invalid.';
                            }
                        }
                        if (!empty($rowData[0][1])){
                            if (trim($rowData[0][1])!='School’s Address') 
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][1].' '.'is invalid.';
                            }
                        }
                        if (!empty($rowData[0][2])){
                            if (trim($rowData[0][2])!='City')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][2].' '.'is invalid.';
                            }
                        }
                        if (!empty($rowData[0][3])){
                            if (trim($rowData[0][3])!='State')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][3].' '.'is invalid.';
                            }
                        }
                        if (!empty($rowData[0][4])){
                            if (trim($rowData[0][4])!='Zip Code')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][4].' '.'is invalid.';
                            }
                        }
                        if (!empty($rowData[0][5])){
                            if (trim($rowData[0][5])!='Grades taught - From*')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][5].' '.'is invalid.';
                            }
                        }
                        if (!empty($rowData[0][6])){
                            if (trim($rowData[0][6])!='Grades taught - To*')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][6].' '.'is invalid.';
                            }
                        }
                       /* if (!empty($rowData[0][7])){
                            if (trim($rowData[0][7])!='Principal’s Title')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][7].' '.'is invalid.';
                            }
                        }*/
                        if (!empty($rowData[0][7])){
                            if (trim($rowData[0][7])!='Principal’s First Name')
                            { 
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][7].' '.'is invalid.';  
                            }
                        }
                        if (!empty($rowData[0][8])){
                            if (trim($rowData[0][8])!='Principal’s Last Name')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][8].' '.'is invalid.';
                            }
                        }
                        if (!empty($rowData[0][9])){
                            if (trim($rowData[0][9])!='Principal’s email')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][9].' '.'is invalid.';
                            }
                        }if (!empty($rowData[0][10])){
                            if (trim($rowData[0][10])!='Principal’s Phone')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][10].' '.'is invalid.';
                            }
                        }if (!empty($rowData[0][11])){
                            if (trim($rowData[0][11])!='Teacher’s First Name')
                            { 
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][11].' '.'is invalid.';  
                            }
                        }if (!empty($rowData[0][12])){
                            if (trim($rowData[0][12])!='Teacher’s Last Name')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][12].' '.'is invalid.';
                            }
                        }if (!empty($rowData[0][13])){
                            if (trim($rowData[0][13])!='Teacher’s email')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][13].' '.'is invalid.';
                            }
                        }if (!empty($rowData[0][14])){
                            if (trim($rowData[0][14])!='Grades assigned to the teacher')
                            {   
                                $err_flag=1; 
                                $error_msg.='Column name'.' '.$rowData[0][14].' '.'is invalid.';
                            }
                        }
                    }
                    
                    if($err_flag>0){
                        $objPHPExcel->getActiveSheet()->SetCellValue('P' . $row,$error_msg);
                    }

                    // new validation filter
                    if($row>1){ 
                        if(empty($rowData[0][0]) && empty($rowData[0][1]) && empty($rowData[0][2]) && empty($rowData[0][3]) && empty($rowData[0][4]) &&empty($rowData[0][5]) && empty($rowData[0][6]) && empty($rowData[0][7]) && empty($rowData[0][8]) && empty($rowData[0][9]) && empty($rowData[0][10]) && empty($rowData[0][11]) && empty($rowData[0][12]) && empty($rowData[0][13]) && empty($rowData[0][14])){
                            continue;
                        }

                        $schoolName         = trim($rowData[0][0]);
                        $schoolAddress      = trim($rowData[0][1]);
                        $city               = trim($rowData[0][2]);
                        $state              = trim($rowData[0][3]);
                        $zipcode            = trim($rowData[0][4]);
                        $schoolAddress      = $schoolAddress.' '.$state.', '.$city.', '.$zipcode;
                        $gradeFrom          = trim($rowData[0][5]);
                        $gradeTo            = trim($rowData[0][6]);
                       /* $pricipalTitle      = trim($rowData[0][7]);*/
                        $principalFirstName = trim($rowData[0][7]);
                        $principalLastName  = trim($rowData[0][8]);
                        $principalEmail     = trim($rowData[0][9]);
                        $principalPhone     = trim($rowData[0][10]);
                        $teacherFirstName   = trim($rowData[0][11]);
                        $teacherLastName    = trim($rowData[0][12]);
                        $teacherEmail       = trim($rowData[0][13]);
                        $gradesAssign       = trim($rowData[0][14]);
                        
                        if(empty($schoolName)){
                            $error_msg.= 'School name is required.';
                            $err_flag=1;
                        } else {
                            if (strlen(trim($schoolName)) > 70) {
                                $error_msg.='School’s name must be less than 70 characters.';
                                $err_flag=1;                            
                            }
                        }
                        if((!empty($schoolAddress))){
                           if (strlen(trim($schoolAddress)) > 100) {
                                $error_msg.='School’s address must be less than 100 characters.';
                                $err_flag=1;                            
                            }
                        }
                        $gradeFrom=str_replace('grade','Grade',$gradeFrom);
                        $gradeTo=str_replace('grade','Grade',$gradeTo);
                        if((!empty($gradeFrom))){
                            if(!in_array($gradeFrom, $grade_array)){
                                $error_msg.='Invalid grade taught From.';
                                $err_flag=1;
                            }
                        }else{
                            $error_msg.='Invalid grade taught From.';
                            $err_flag==1;
                        }
                        if((!empty($gradeTo))){
                            if(!in_array($gradeTo, $grade_array)){
                                $error_msg.='Invalid grade taught To.';
                                $err_flag=1;
                            }
                        }else{
                            $error_msg.='Invalid grade taught To.';
                            $err_flag==1;
                        }

                        $school_taught_from=$this->Common_model->ranges($gradeFrom);
                        $school_taught_to=$this->Common_model->ranges($gradeTo);
                        if($school_taught_from > $school_taught_to){
                            $error_msg.='Taught From column must be less than taught To.';
                            $err_flag==1;
                        } 

                        if(!empty($principalEmail)){
                            if(!empty($principalPhone)){
                                $numbers_only = preg_replace("/[^\d]/", "", $principalPhone);
                                if(strlen($numbers_only)!=10){
                                    $error_msg.='Phone No. must equal 10 characters.';
                                    $err_flag=1; 
                                }
                            }

                            if(!empty($principalFirstName)){
                                if (strlen(trim($principalFirstName)) > 30) {
                                    $error_msg.='Principal’s first name must be less than 30 characters.';
                                    $err_flag=1;                            
                                }
                            }                        

                            if(!empty($principalLastName)){
                                if (strlen(trim($principalLastName)) > 30) {
                                    $error_msg.='Principal’s last name must be less than 30 characters.';
                                    $err_flag=1;                            
                                }
                            }

                            if(!empty($teacherFirstName)){
                                if (strlen($teacherFirstName) > 30) {
                                    $error_msg.='Teacher’s first name must be less than 30 characters.';
                                    $err_flag=1;                            
                                }
                            }

                            if(!empty($teacherLastName)){
                                if (strlen($teacherLastName) > 30) {
                                    $error_msg.='Teacher’s last name must be less than 30 characters.';
                                    $err_flag=1;                            
                                }
                            }

                            if((!empty($teacherEmail))){
                                if (!filter_var($teacherEmail, FILTER_VALIDATE_EMAIL)) 
                                {    
                                    $error_msg.='Teacher’s email invalid.';
                                    $err_flag=1;
                                }
                                if($check_teacher_email = $this->Common_model->getRecords('users','email',array('is_deleted'=>0,'email'=>$teacherEmail),'user_id desc',true)) 
                                {
                                    $error_msg.= 'Teacher’s email already exist.';
                                    $err_flag=1;
                                }
                            }                            
                            if((!empty($teacherEmail))){
                                if(!empty($gradesAssign)){
                                    $arrayRange = range($school_taught_from ,$school_taught_to);

                                    $r_trim=rtrim($gradesAssign,',');
                                    $l_trim=ltrim($r_trim,',');
                                    $gradesAssign=$l_trim;
                                    $teacher_grade= $this->teacher_grade_replace($gradesAssign);
                                    if(!empty($teacher_grade)){
                                        $explodeArr = explode(',', $teacher_grade);
                                        if (!empty($explodeArr)) {
                                            if($this->isInArray($explodeArr, $arrayRange) == false){
                                                $error_msg.='Invalid grades for teacher.';
                                                $err_flag=1;
                                            }
                                        }else{
                                            $error_msg.='Invalid grades for teacher.';
                                            $err_flag=1;
                                        }
                                    }else{
                                        $error_msg.='Invalid grades for teacher.';
                                        $err_flag=1;
                                    }
                                } else {
                                    $error_msg.='Teacher grades are empty.';
                                    $err_flag=1;
                                }    
                            }
                        }                          

                        if((!empty($principalEmail))){
                            if (!filter_var($principalEmail, FILTER_VALIDATE_EMAIL)) 
                            {    
                                $error_msg.='Principal’s email invalid.';
                                $err_flag=1;
                            }
                           /* if($check_school = $this->Common_model->getRecords('users','email',array('is_deleted'=>0,'email'=>$principalEmail),'user_id desc',true)) 
                            {
                                $error_msg.= 'Principal’s email already exist.';
                                $err_flag=1;
                            }*/
                            if($check_school = $this->Common_model->getRecords('users','email',array('is_deleted'=>0,'user_type !='=>2,'email'=>$principalEmail),'user_id desc',true)){
                                $error_msg.= 'Principal’s email already exist.';
                                $err_flag=1;
                            }
                        }
                        // echo $err_flag.'=='.$error_msg;
                        $objPHPExcel->getActiveSheet()->SetCellValue('P'. $row,$error_msg);
                    }
                    // new validation filter
                }
                // echo $err_flag;
                // print_r($error_msg);die;
                if($err_flag==1){
                    if(file_exists($inputFileName)){
                        unlink($inputFileName);
                    }
                    $fileName = 'resources/district_csv/error/error_'.$media['file_name'];
                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                    $objWriter->save($fileName);
                    // download file
                    // header("Content-Type: application/vnd.ms-excel");
                    //redirect($fileName);
                    $link = '<a href="'.$fileName.'"> Click here to download</a>';
                    $this->session->set_flashdata('error', 'The input sheet has some errors, please fix and upload.'.$link);
                    redirect('admin/user/upload/'.$user_id);
                }else{

                    
                    $state_id   = $this->Common_model->getFieldValue('district','state_id',array('id'=>$district_id));
                    
                    $created = date('Y-m-d H:i:s');

                    for ($row = 1; $row <= $highestRow; $row++) { 
                        $error_msg='';
                        // Read a row of data into an array                 
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

                        // new validation filter
                        if($row>1){ 
                            if(empty($rowData[0][0]) && empty($rowData[0][1]) && empty($rowData[0][2]) && empty($rowData[0][3]) && empty($rowData[0][4]) &&empty($rowData[0][5]) && empty($rowData[0][6]) && empty($rowData[0][7]) && empty($rowData[0][8]) && empty($rowData[0][9]) && empty($rowData[0][10]) && empty($rowData[0][11]) && empty($rowData[0][12]) && empty($rowData[0][13]) && empty($rowData[0][14]) ){
                                continue;
                            }

                            $schoolName         = trim($rowData[0][0]);
                            $schoolAddress      = trim($rowData[0][1]);
                            $city               = trim($rowData[0][2]);
                            $state              = trim($rowData[0][3]);
                            $zipcode            = trim($rowData[0][4]);
                           
                            if(!empty($state) && !empty($city) && !empty($zipcode)){
                                $schoolAddress      =$schoolAddress.' '.$state.', '.$city.', '.$zipcode;
                            }else if(!empty($state) && !empty($zipcode)){
                                $schoolAddress      =$schoolAddress.' '.$state.', '.$zipcode;
                            }else if(!empty($city) && !empty($zipcode)){
                                $schoolAddress      =$schoolAddress.' '.$city.', '.$zipcode;
                            }else if(!empty($state) && !empty($city)){
                                $schoolAddress      =$schoolAddress.' '.$state.', '.$city;
                            }

                            $gradeFrom          = trim($rowData[0][5]);
                            $gradeTo            = trim($rowData[0][6]);
                           /* $pricipalTitle      = trim($rowData[0][7]);*/
                            $principalFirstName = trim($rowData[0][7]);
                            $principalLastName  = trim($rowData[0][8]);
                            $principalEmail     = trim($rowData[0][9]);
                            $principalPhone     = trim($rowData[0][10]);
                            $teacherFirstName   = trim($rowData[0][11]);
                            $teacherLastName    = trim($rowData[0][12]);
                            $teacherEmail       = trim($rowData[0][13]);
                            $gradesAssign       = trim($rowData[0][14]);

                            $gradeFrom=str_replace('grade','Grade',$gradeFrom);
                            $gradeTo=str_replace('grade','Grade',$gradeTo);
                            // print_r($rowData);
                            $new_img_name="";
                            $schoolLogoCron="";
                            if(!$check_school = $this->Common_model->getRecords('school','id,name',array('district_id'=>$district_id,'name'=>$schoolName),'',true)) 
                            {
                                $school_taught_from=$this->Common_model->ranges($gradeFrom);
                                $school_taught_to=$this->Common_model->ranges($gradeTo);
                               
                                $schoolInsert=array(
                                    'state_id'=>$state_id,
                                    'district_id'=>$district_id,
                                    'name'=>$schoolName?ucfirst($schoolName):"",
                                    'address'=>$schoolAddress?$schoolAddress:"",
                                    'logo'=>$new_img_name?$new_img_name:"",
                                    'created'=>$created,
                                );
                                $school_id=$this->Common_model->addEditRecords('school',$schoolInsert);
                                // school's grade inserted
                                $this->school_grades_insert($school_taught_from,$school_taught_to,$school_id,$district_id);
                                //Principal inserted
                                if((!empty($principalEmail))){
                                    if(!$this->Common_model->getRecords('users','user_id',array('is_deleted'=>0,'user_type'=>2,'email'=>$principalEmail),'user_id desc',true)){
                                
                                        $ph_no="";
                                        if(!empty($principalPhone)){
                                            $numbers_only = preg_replace("/[^\d]/", "", $principalPhone);
                                            $ph_no= preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $numbers_only);
                                        }

                                        $principal_pw = generateRandomString();
                                        $principal_data=array(
                                            /*'title'=>$pricipalTitle?$pricipalTitle:"",*/
                                            'first_name'=>$principalFirstName?ucfirst($principalFirstName):"",
                                            'last_name'=>$principalLastName?ucfirst($principalLastName):"",
                                            'email'=>$principalEmail?$principalEmail:"",
                                            'phone'=>$ph_no,
                                            'school_id'=>$school_id,
                                            'state_id'=>$state_id,
                                            'district_id'=>$district_id,
                                            'password'=>md5($principal_pw),
                                            'is_approve'=>2,
                                            'user_type'=>2,
                                            'approved_by'=>$user_id,
                                            'email_verified'=>1,
                                            'created'=>$created
                                        );
                                        $pricipal_id=$this->Common_model->addEditRecords('users',$principal_data);
                                        $user_number = str_pad($pricipal_id, 8, '0', STR_PAD_LEFT);
                                        $update_user = $this->Common_model->addEditRecords('users',array('user_number'=>$user_number),array('user_id'=>$pricipal_id));

                                        $emailArray = array(
                                            'email' => $principalEmail,
                                            'otp'   => $principal_pw,
                                            'recipient_name'=> ucfirst($principalFirstName).' '.ucfirst($principalLastName),
                                            'status'   => 0,
                                            'user_type'   => 2,
                                            'school_id'   => $school_id,
                                            'school_logo'   => $schoolLogoCron,
                                            'created'   => $created,
                                        );
                                        
                                        $this->Common_model->addEditRecords('email_otp_cron',$emailArray);
                                    }

                                    // $school_detail = $this->Common_model->getRecords('school','id,state_id,district_id',array('district_id'=>$district_id,'name'=>$schoolName),'',true);
                                    if(!empty($teacherEmail)){
                                        if(!$check_teacher_email = $this->Common_model->getRecords('users','email',array('is_deleted'=>0,'email'=>$teacherEmail),'user_id desc',true)) {
                                            $check_school_grade="";
                                            if(!empty($gradesAssign)){
                                                $r_trim=rtrim($gradesAssign,',');
                                                $l_trim=ltrim($r_trim,',');
                                                $gradesAssign=$l_trim;
                                                $teacher_grade= $this->teacher_grade_replace($gradesAssign);
                                                
                                                $where_teacher_grade='school_id ='.$school_id.' and grade IN ('.$teacher_grade.')';
                                                $check_school_grade = $this->Common_model->getRecords('school_grade','*',$where_teacher_grade,'',false);
                                            }

                                            $teacher_grade_data=array();        
                                            $teacher_pw=generateRandomString();
                                            $teacher_data=array(
                                                'first_name'=>$teacherFirstName?ucfirst($teacherFirstName):"",
                                                'last_name'=>$teacherLastName?ucfirst($teacherLastName):"",
                                                'email'=>$teacherEmail?$teacherEmail:"",
                                                'state_id'=>$state_id,
                                                'district_id'=>$district_id,
                                                'school_id'=>$school_id,
                                                'password'=>md5($teacher_pw),
                                                'user_type'=>3,
                                                'is_approve'=>2,
                                                'approved_by'=>$user_id,
                                                'email_verified'=>1,
                                                'created'=>$created
                                                
                                            );
                                            if($teacher_id=$this->Common_model->addEditRecords('users',$teacher_data)){
                                                $user_number = str_pad($teacher_id, 8, '0', STR_PAD_LEFT);
                                                $update_user = $this->Common_model->addEditRecords('users',array('user_number'=>$user_number),array('user_id'=>$teacher_id));
                                                if(isset($check_school_grade) && !empty($check_school_grade)){
                                                    foreach ($check_school_grade as $key => $teacher_grade) {
                                                        $teacher_grade_data[]=array(
                                                            'teacher_id'=>$teacher_id,
                                                            'district_id'=>$teacher_grade['district_id'],
                                                            'school_id'=>$teacher_grade['school_id'],
                                                            'grade'=>$teacher_grade['grade'],
                                                            'grade_display_name'=>DISPLAY_GRADE_NAME[$teacher_grade['grade']],
                                                        );
                                                    }
                                                }
                                            }
                                            if (!empty($teacher_grade_data)) {
                                                $this->db->insert_batch('teacher_grade', $teacher_grade_data);
                                            }

                                            $teacherEmailArray = array(
                                                'email' => $teacherEmail,
                                                'otp'   => $teacher_pw,
                                                'recipient_name'=> ucfirst($teacherFirstName).' '.ucfirst($teacherLastName),
                                                'status'   => 0,
                                                'user_type'   => 3,                                        
                                                'created'   => $created,
                                            );
                                            
                                            $this->Common_model->addEditRecords('email_otp_cron',$teacherEmailArray);
                                        }
                                    }
                                }

                            }else{
                                
                                //For teacher inseration , if same csv uploaded for other only teache so this function used.
                                $pricipal_id="";
                                $school_id=$check_school['id'];
                                if($check_principal = $this->Common_model->getRecords('users','user_id',array('school_id'=>$check_school['id'],'user_type'=>2,'is_deleted'=>0),'',true)) 
                                {
                                    $pricipal_id=$check_principal['user_id'];
                                }else{

                                    if((!empty($principalEmail))){
                                        if(!$this->Common_model->getRecords('users','user_id',array('is_deleted'=>0,'user_type'=>2,'email'=>$principalEmail),'user_id desc',true)){
                                            $ph_no="";
                                            if(!empty($principalPhone)){
                                                $numbers_only = preg_replace("/[^\d]/", "", $principalPhone);
                                                $ph_no= preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $numbers_only);
                                            }

                                            $principal_pw = generateRandomString();
                                            $principal_data=array(
                                                /*'title'=>$pricipalTitle?$pricipalTitle:"",*/
                                                'first_name'=>$principalFirstName?ucfirst($principalFirstName):"",
                                                'last_name'=>$principalLastName?ucfirst($principalLastName):"",
                                                'email'=>$principalEmail?$principalEmail:"",
                                                'phone'=>$ph_no,
                                                'school_id'=>$school_id,
                                                'state_id'=>$state_id,
                                                'district_id'=>$district_id,
                                                'password'=>md5($principal_pw),
                                                'is_approve'=>2,
                                                'user_type'=>2,
                                                'approved_by'=>$user_id,
                                                'email_verified'=>1,
                                                'created'=>$created
                                            );
                                            //print_r( $principal_data);die;
                                            $pricipal_id=$this->Common_model->addEditRecords('users',$principal_data);
                                            $user_number = str_pad($pricipal_id, 8, '0', STR_PAD_LEFT);
                                            $update_user = $this->Common_model->addEditRecords('users',array('user_number'=>$user_number),array('user_id'=>$pricipal_id));

                                            $emailArray = array(
                                                'email' => $principalEmail,
                                                'otp'   => $principal_pw,
                                                'recipient_name'=> ucfirst($principalFirstName).' '.ucfirst($principalLastName),
                                                'status'   => 0,
                                                'user_type'   => 2,
                                                'school_id'   => $school_id,
                                                'school_logo'   => $schoolLogoCron,
                                                'created'   => $created,
                                            );
                                            
                                            $this->Common_model->addEditRecords('email_otp_cron',$emailArray);
                                        }
                                    }
                                }
                                
                                if(!empty($pricipal_id)){
                                    if(!empty($teacherEmail)){
                                        if(!$check_teacher_email = $this->Common_model->getRecords('users','email',array('is_deleted'=>0,'email'=>$teacherEmail),'user_id desc',true)) {

                                            if(!empty($gradesAssign)){
                                                $r_trim=rtrim($gradesAssign,',');
                                                $l_trim=ltrim($r_trim,',');
                                                $gradesAssign=$l_trim;
                                                $teacher_grade= $this->teacher_grade_replace($gradesAssign);
                                                
                                                $where_teacher_grade='school_id ='.$school_id.' and grade IN ('.$teacher_grade.')';
                                                $check_school_grade = $this->Common_model->getRecords('school_grade','*',$where_teacher_grade,'',false);
                                            }

                                            $teacher_grade_data=array();        
                                            $teacher_pw=generateRandomString();
                                            $teacher_data=array(
                                                'first_name'=>$teacherFirstName?ucfirst($teacherFirstName):"",
                                                'last_name'=>$teacherLastName?ucfirst($teacherLastName):"",
                                                'email'=>$teacherEmail?$teacherEmail:"",
                                                'state_id'=>$state_id,
                                                'district_id'=>$district_id,
                                                'school_id'=>$school_id,
                                                'password'=>md5($teacher_pw),
                                                'user_type'=>3,
                                                'is_approve'=>2,
                                                'approved_by'=>$user_id,
                                                'email_verified'=>1,
                                                'created'=>$created
                                                
                                            );
                                            
                                            if($teacher_id=$this->Common_model->addEditRecords('users',$teacher_data)){
                                                $user_number = str_pad($teacher_id, 8, '0', STR_PAD_LEFT);
                                                $update_user = $this->Common_model->addEditRecords('users',array('user_number'=>$user_number),array('user_id'=>$teacher_id));
                                                if(isset($check_school_grade) && !empty($check_school_grade)){
                                                    foreach ($check_school_grade as $key => $teacher_grade) {
                                                        $teacher_grade_data[]=array(
                                                            'teacher_id'=>$teacher_id,
                                                            'district_id'=>$teacher_grade['district_id'],
                                                            'school_id'=>$teacher_grade['school_id'],
                                                            'grade'=>$teacher_grade['grade'],
                                                            'grade_display_name'=>DISPLAY_GRADE_NAME[$teacher_grade['grade']],
                                                        );
                                                    }
                                                }
                                            }
                                            if (!empty($teacher_grade_data)) {
                                                $this->db->insert_batch('teacher_grade', $teacher_grade_data);
                                            }

                                            $teacherEmailArray = array(
                                                'email' => $teacherEmail,
                                                'otp'   => $teacher_pw,
                                                'recipient_name'=> ucfirst($teacherFirstName).' '.ucfirst($teacherLastName),
                                                'status'   => 0,
                                                'user_type'   => 3,
                                                'created'   => $created,
                                            );
                                            
                                            $this->Common_model->addEditRecords('email_otp_cron',$teacherEmailArray);
                                        }
                                    }
                                }
                            }                            
                        }
                    }

                    $insert_array=array(
                        'district_id'=>$district_id,
                        'user_id'=>$user_id,
                        'state_id'=>$state_id,
                        'file_name'=>$inputFileName,
                        'status'=>1,
                        'created'=>date('Y-m-d H:i:s')
                    );  
                    if(!$id = $this->Common_model->addEditRecords('csv_uploads',$insert_array)) {
                       $this->session->set_flashdata('error', 'The input sheet has some errors, please fix and upload.');
                       redirect('admin/user/upload/'.$user_id);
                    }else{
                        
                        $this->session->set_flashdata('success', 'File uploaded successfully.');
                        redirect('admin/user/list');
                    }
                }

            }else{
                $this->session->set_flashdata('error', 'Upload a file');
                redirect('admin/user/upload/'.$user_id);
            }
        }else{
            $this->session->set_flashdata('error', 'Upload a file');
            redirect('admin/user/upload/'.$user_id);
        }
        
    }


    public function users_list()
    {
        $this->Common_model->check_login();
        
        check_permission('13','view','yes');
        if(check_permission('13','add')){$data['add_action']='admin/user/add';}
        if(check_permission('13','edit')){$data['edit_action']='admin/user/edit';}
        if(check_permission('13','delete')){$data['delete_action']='1';}
        $data['upload_action']='admin/user/upload';
        
        $data['title']="District List | ".SITE_TITLE;
        $data['page_title']="District List";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'icon'=>'<i class="fa fa-dashboard"></i>',
            'class'=>'',
            'title' => 'Dashboard',
            'link' => site_url('admin/dashboard')
        );

        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'active',
            'title' => 'District List',
            'link' => ""
        );

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $getArray = array('first_name','last_name','email','phone','is_approve','status','state','district_name');

        foreach ($getArray as $row) {
            if ($this->input->get($row)) {
                $data['filter_'.$row]=$this->input->get($row);
            } else {
                $data['filter_'.$row]='';
            }
        }

        $user_type=1;
        $data['total_records']=$this->User_model->get_user_list(0,0,$user_type,1);
        
        $records_results = $this->User_model->get_user_list(ADMIN_LIMIT,$page,$user_type,1);

        $brdCstdata = $this->session->userdata('brdCstdata');

        $data['records_results'] = array();
        if (!empty($records_results)) {
            $index = 0;
            foreach ($records_results as $row) {
                $data['records_results'][$index] = $row;

                if (isset($brdCstdata) && !empty($brdCstdata)) {
                    if (in_array($row['user_id'], $brdCstdata)) {
                        $data['records_results'][$index]['is_added'] = 1;
                    } else {
                        $data['records_results'][$index]['is_added'] = 0;
                    }
                } else {
                    $data['records_results'][$index]['is_added'] = 0;
                }

                $index++;
            }
        }
        // $data['records_results']=$this->User_model->get_user_list(ADMIN_LIMIT,$page,$user_type,1);
        
        $data['pagination']=$this->Common_model->paginate(site_url('admin/user/list'),$data['total_records']);
        $data['states']=  $this->Common_model->getRecords('states');

        $data['form_action']=site_url('admin/user/list/');

        $this->load->view('admin/include/header',$data);
        $this->load->view('admin/include/sidebar');
        $this->load->view('admin/user/user_list');
        $this->load->view('admin/include/footer');
    } 

    public function district_export() { 

        $this->Common_model->check_login();
        check_permission('70','view','yes');

        $fileName = EXCEL_PATH.'District-'.time().'.xlsx';   

        $recordList = $this->User_model->get_user_list('','',1,1,'all');        

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        // set Header

        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Sr. No.');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'First Name'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Last Name'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Email'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Phone'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'State'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'District'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Status'); 


        // set Row
        $rowCount = 2;
        if (!empty($recordList)) {
            foreach ($recordList as $key=> $row) {  

                $status = 'Pending';
                if ($row['status']==1) {
                    $status = 'Active';
                } else if ($row['status']==2) {
                    $status = 'Inactive';
                } else if ($row['status']==3) {
                    $status = 'Pending';
                }

                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $key+1);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, !empty($row['first_name'])?ucfirst($row['first_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, !empty($row['last_name'])?ucfirst($row['last_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, !empty($row['email'])?$row['email']:'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, !empty($row['phone'])?$row['phone']:'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, !empty($row['state_name'])?ucfirst($row['state_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, !empty($row['district_name'])?ucfirst($row['district_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, !empty($status)?$status:'N/A');

                $rowCount++;
            }

        }

    
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($fileName);
        // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect($fileName);  
    }

    /****************** Updatation of checkbox value ******************/
    public function check_broadcast(){
        if($this->input->post()) 
        {    
            $admin_id   =   $this->session->userdata('admin_id');
            $district_id        =   $this->input->post('district_id'); 
            $type = $this->input->post('type'); 
            $url = $this->input->post('url');
            $brdCstdata = $this->session->userdata('brdCstdata');

            if ($type=='add') {

                if($brdCstdata == NULL){
                    $array_one = array();
                    array_push($array_one, $district_id);
                    $this->session->set_userdata('brdCstdata', $array_one); 
                    $this->session->set_userdata('url', $url);
                }  else {
                   $session_two=  $this->session->userdata('brdCstdata');
                   if (!in_array($district_id, $session_two)) {
                       array_push($session_two, $district_id);               
                       $this->session->set_userdata('brdCstdata', $session_two);
                       $this->session->set_userdata('url', $url);
                   }

                }

            } else if($type=='delete'){

                if (($key = array_search($district_id, $brdCstdata)) !== false) {
                    unset($brdCstdata[$key]);
                }               
                $brdCstdata = array_values($brdCstdata);
                $this->session->set_userdata('brdCstdata', $brdCstdata);
            }

            $data = array('status' =>1,'msg' =>'success');
            echo json_encode($data);
        }
    }
    /****************** Updatation of checkbox value ******************/


    /****************** Broadcast Message ******************/
    public function broadcast_message(){

        $this->Common_model->check_login();
        check_permission('13','edit','yes');
        $data['title']="Send Message | ".SITE_TITLE;
        $data['page_title']="Send Message";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'icon'=>'<i class="fa fa-dashboard"></i>',
            'class'=>'',
            'title' => 'Dashboard',
            'link' => site_url('admin/dashboard')
        );
        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'',
            'title' => 'District List',
            'link' => site_url('admin/user/list')
        );
        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'active',
            'title' => 'Send Message',
            'link' => ""
        );  

        $brdCstdata = $this->session->userdata('brdCstdata');
        $admin_id = $this->session->userdata('admin_id');
        $user_name = $this->session->userdata('user_name');
        $admin_email = getAdminEmail();

        if (isset($brdCstdata) && !empty($brdCstdata)) {

            if($this->input->post()) {

                $this->form_validation->set_rules('subject', 'subject', 'trim|required|max_length[100]',array('required'=>'Please enter %s'));
                $this->form_validation->set_rules('message', 'message', 'trim|required',array('required'=>'Please enter %s'));

                $this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

                if($this->form_validation->run()==TRUE) 
                {

                    $user_info = array();
                    $userInfoIds = implode(",",$brdCstdata);
                    $whereIn=  "user_id IN ($userInfoIds)";
                    $participant=$this->Common_model->getRecords('users','email,concat(first_name," ",last_name) as name, user_id',$whereIn,'',false);
                    if($participant){
                        $arr = [];
                        foreach($participant as $list){
                            $email = $list['email'];
                            $name = $list['name'];
                            $id = $list['user_id'];
                            array_push($arr, array('name' =>$name, 'email'=>$email, 'id'=>$id ));
                        }
                        $user_info =  $arr;
                    }
                    $id = $_SESSION['brdCstdata'];
                    $type = $_POST['type'];
                    // $username = array();
                    foreach ($id as $key) {

                        $data = $this->db->get_where('users',array('user_id'=>$key))->row();
                        if(isset($data->display_name)){
                            $name = $data->display_name;
                        }else{
                            $name = $data->first_name.' '.$data->last_name;
                        }
                       $type1 = $data->user_type;
                       // array_push($username, $name); 
                    }
                   
                    $subject    = $this->input->post('subject');
                    $message    = $this->input->post('message');
                   
                    $insert_data = array(
                        'notification_type'=>'email',
                        'subject'=>$subject, 
                        // 'user_name'=>implode(',', $username),  
                        'user_type'=>$type, 
                        'message'=>$message, 
                        'created_by'=>$admin_id, 
                        'sent_by'=>1, 
                        'created'=>date('Y-m-d H:i:s')
                    );
                   
                    $this->db->trans_begin();

                    $last_id=$this->Common_model->addEditRecords('broadcast_notification', $insert_data);

                    $boradcast_email=array();
                    $email_notification=array();
                    if(!empty($user_info)){
                        foreach($user_info as $key => $detail) {
                            $boradcast_email[]=array(
                                'notification_id'=>$last_id,
                                'user_id'=>$detail['id'],
                                'email'=>$detail['email'],
                                'created'=>date('Y-m-d H:i:s'),
                            );
                            $email_notification[]=array(
                                'user_type'=>0,
                                'type'=>'Broadcast_notification',
                                'status'=>0,
                                'sender_id'=>$admin_id,
                                'created_by'=>$admin_id,
                                'sender_email'=>$admin_email,
                                'sender_name'=>'The '.WEBSITE_EMAIL_NAME.' team',
                                'recipient_name'=>$detail['name'],
                                'email'=>$detail['email'],
                                'user_id'=>$detail['id'],
                                'subject'=>$subject, 
                                'message'=>$message, 
                                'created'=>date('Y-m-d H:i:s'),
                            );
                        }
                        $this->db->insert_batch('broadcast_emails', $boradcast_email);
                        $this->db->insert_batch('email_notification', $email_notification);

                        $this->session->unset_userdata('brdCstdata');
                    }

                    if($this->db->trans_status() === FALSE){
                        $this->db->trans_rollback();
                        $this->session->set_flashdata('error', 'Something went wrong. Please try again.');
                    } else {
                        $this->db->trans_commit();
                      
                        $this->session->set_flashdata('success', 'Message broadcasted successfully.');
                        redirect($_SESSION['url'],'refresh');
                    }
                } else {
                    $this->session->set_flashdata('error', 'Please fill all required fields.');
                }
            }
            $user = $this->input->get('type');
            if($user=='district'){
              $user ='user';  
            }
            $data['form_action']=site_url('admin/district/broadcast_message/');
            $data['back_action']=site_url('admin/'.$user.'/list');
            
            $this->load->view('admin/include/header',$data);
            $this->load->view('admin/include/sidebar');
            $this->load->view('admin/user/broadcast_message');
            $this->load->view('admin/include/footer');
        } else {
            $this->session->set_flashdata('error', 'Please select district for send broadcast message.');
            redirect('admin/user/list','refresh');
        }       
    }
    /****************** Updation of checkbox value ******************/

    public function broadcast_list()
    {

        // echo get_time_posted(date('Y-m-d H:i:s'),'2020-12-12 15:26:52');
        // die;
        $this->Common_model->check_login();
        
        check_permission('71','view','yes');
        if(check_permission('71','add')){$data['add_action']='admin/broadcast/add';}
        if(check_permission('71','edit')){$data['edit_action']='admin/broadcast/edit';}
        if(check_permission('71','delete')){$data['delete_action']='1';}
        
        
        $data['title']="Broadcast List | ".SITE_TITLE;
        $data['page_title']="Broadcast List";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'icon'=>'<i class="fa fa-dashboard"></i>',
            'class'=>'',
            'title' => 'Dashboard',
            'link' => site_url('admin/dashboard')
        );

        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'active',
            'title' => 'Broadcast List',
            'link' => ""
        );

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $getArray = array('subject','user_type','user_name');

        foreach ($getArray as $row) {
            if ($this->input->get($row)) {
                $data['filter_'.$row]=$this->input->get($row);
                $data['user_'.$row]=$this->input->get($row);
            } else {
                $data['filter_'.$row]='';
                
            }
        }
  
        $data['total_records']=$this->User_model->broadcastList(0,0);
        $data['records_results']=$this->User_model->broadcastList(ADMIN_LIMIT,$page);
        
        $data['pagination']=$this->Common_model->paginate(site_url('admin/broadcast/list'),$data['total_records']);

        $data['form_action']=site_url('admin/broadcast/list/');
        
        $this->load->view('admin/include/header',$data);
        $this->load->view('admin/include/sidebar');
        $this->load->view('admin/user/broadcast_list');
        $this->load->view('admin/include/footer');
    } 


    // Broadcast User List
    public function broadcast_user_list()
    {   

        $this->Common_model->check_login();
        
        check_permission('71','view','yes');
        if(check_permission('71','add')){$data['add_action']='admin/student/add';}
        if(check_permission('71','edit')){$data['edit_action']='admin/student/edit';}
        if(check_permission('71','delete')){$data['delete_action']='1';}
        
        
        $data['title']="Broadcast User List | ".SITE_TITLE;
        $data['page_title']="Broadcast User List";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'icon'=>'<i class="fa fa-dashboard"></i>',
            'class'=>'',
            'title' => 'Dashboard',
            'link' => site_url('admin/dashboard')
        );

        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'active',
            'title' => 'Broadcast User List',
            'link' => ""
        );

        $parentID = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;

        $getArray = array('email');

        foreach ($getArray as $row) {
            if ($this->input->get($row)) {
                $data['filter_'.$row]=$this->input->get($row);
            } else {
                $data['filter_'.$row]='';
            }
        }
        $data['message'] = $this->Common_model->getFieldValue('broadcast_notification','message',array('notification_id'=>$parentID));
        $data['subject'] = $this->Common_model->getFieldValue('broadcast_notification','subject',array('notification_id'=>$parentID));
        $data['user_name'] = $this->Common_model->getFieldValue('broadcast_notification','user_name',array('notification_id'=>$parentID));
        $data['sent_by'] = $this->Common_model->getFieldValue('broadcast_notification','sent_by',array('notification_id'=>$parentID));
        $data['user_type'] = $this->Common_model->getFieldValue('broadcast_notification','user_type',array('notification_id'=>$parentID));

        $data['date'] = $this->Common_model->getFieldValue('broadcast_notification','created',array('notification_id'=>$parentID));

        $data['total_records']=$this->User_model->getBroadcastUserList(0,0,$parentID);
        $data['records_results']=$this->User_model->getBroadcastUserList(ADMIN_LIMIT,$page,$parentID);
        
        $data['pagination']=$this->Common_model->paginate(site_url('admin/broadcast_user/list/'.$parentID),$data['total_records'],'seg5');

        $data['form_action']=site_url('admin/broadcast_user/list/'.$parentID);
        $data['back_action']=site_url('admin/broadcast/list');

        $this->load->view('admin/include/header',$data);
        $this->load->view('admin/include/sidebar');
        $this->load->view('admin/user/broadcast_user_list');
        $this->load->view('admin/include/footer');
    } 


    public function prospect_list()
    {
        $this->Common_model->check_login();
        
        check_permission('70','view','yes');
        if(check_permission('70','add')){$data['add_action']='admin/user/add';}
        if(check_permission('70','edit')){$data['edit_action']='admin/user/edit';}
        if(check_permission('70','delete')){$data['delete_action']='1';}
        
        
        $data['title']="Prospect List | ".SITE_TITLE;
        $data['page_title']="Prospect List";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'icon'=>'<i class="fa fa-dashboard"></i>',
            'class'=>'',
            'title' => 'Dashboard',
            'link' => site_url('admin/dashboard')
        );

        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'active',
            'title' => 'Prospect List',
            'link' => ""
        );

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $like=array();
        $data['filter_first_name']='';
        if($this->input->get('first_name')){
            $data['filter_first_name']=$this->input->get('first_name');
        }
        $data['filter_last_name']='';
        if($this->input->get('last_name')){
            $data['filter_last_name']=$this->input->get('last_name');
        }
        $data['filter_email']='';
        if($this->input->get('email')){
            $data['filter_email']=$this->input->get('email');
        }
        $data['filter_phone']='';
        if($this->input->get('phone')){
            $data['filter_phone']=$this->input->get('phone');
        }
        $data['filter_is_approve']='';
        if($this->input->get('is_approve')){
            $data['filter_is_approve']=$this->input->get('is_approve');
        }
        $data['filter_state']='';
        if($this->input->get('state')){
            $data['filter_state']=$this->input->get('state');
        }
        $data['filter_district_name']='';
        if($this->input->get('district_name')){
            $data['filter_district_name']=$this->input->get('district_name');
        }
        $data['filter_prospect_status']='';
        if($this->input->get('prospect_status')){
            $data['filter_prospect_status']=$this->input->get('prospect_status');
        }

        $user_type=1;
        $data['view_action']=site_url('admin/user/prospect_details/');
        $data['total_records']=$this->User_model->get_user_list(0,0,$user_type,2);
        
        $data['records_results']=$this->User_model->get_user_list(ADMIN_LIMIT,$page,$user_type,2);
        
        $data['pagination']=$this->Common_model->paginate(site_url('admin/prospect/list'),$data['total_records']);
        $data['states']=  $this->Common_model->getRecords('states');
        $this->load->view('admin/include/header',$data);
        $this->load->view('admin/include/sidebar');
        $this->load->view('admin/user/prospect_list');
        $this->load->view('admin/include/footer');
    } 


    public function prospect_export() { 

        $this->Common_model->check_login();
        check_permission('70','view','yes');

        $fileName = EXCEL_PATH.'Prospect-'.time().'.xlsx';   

        $recordList = $this->User_model->get_user_list('','',1,2,'all');        

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        // set Header

        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Sr. No.');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'First Name'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Last Name'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Email'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Phone'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'State'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'District'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Prospect Status'); 

        // set Row
        $rowCount = 2;
        if (!empty($recordList)) {
            foreach ($recordList as $key=> $row) {  

                $prospect_status = 'Pending';
                if ($row['prospect_status']==1) {
                    $prospect_status = 'Pending';
                } else if ($row['prospect_status']==2) {
                    $prospect_status = 'Sold';
                } else if ($row['prospect_status']==3) {
                    $prospect_status = 'Lost';
                } else if ($row['prospect_status']==4) {
                    $prospect_status = 'Requested Info';
                } else if ($row['prospect_status']==5) {
                    $prospect_status = 'Follow Up';
                }

                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $key+1);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, !empty($row['first_name'])?ucfirst($row['first_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, !empty($row['last_name'])?ucfirst($row['last_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, !empty($row['email'])?$row['email']:'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, !empty($row['phone'])?$row['phone']:'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, !empty($row['state_name'])?ucfirst($row['state_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, !empty($row['district_name'])?ucfirst($row['district_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, !empty($prospect_status)?$prospect_status:'N/A');

                $rowCount++;
            }
        }
    
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($fileName);
        // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect($fileName);  
    }


    public function edit_user($id) 
    {
        $this->Common_model->check_login();
        check_permission('13','edit','yes');
        $data['title']="Edit District | ".SITE_TITLE;
        $data['page_title']="Edit District";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'icon'=>'<i class="fa fa-dashboard"></i>',
            'class'=>'',
            'title' => 'Dashboard',
            'link' => site_url('admin/dashboard')
        );
        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'',
            'title' => 'District List',
            'link' => site_url('admin/user/list')
        );
        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'active',
            'title' => 'Edit District',
            'link' => ""
        );  
        // if(!$data['details'] = $this->Common_model->getRecords('users','*',array('user_id'=>$id),'',true)){
        if(!$data['details'] = $this->User_model->get_user_detail($id)){
            redirect('pages/page_not_found');
        }

        // echo "<pre>";print_r($data['details']);die;
        if($this->input->post()) {

            $this->form_validation->set_rules('first_name', 'first name', 'trim|required|max_length[30]',array('required'=>'Please enter %s'));
            $this->form_validation->set_rules('last_name', 'last name', 'trim|required|max_length[30]',array('required'=>'Please enter %s'));
            $this->form_validation->set_rules('email', 'email', 'trim|required',array('required'=>'Please enter %s'));
            // $this->form_validation->set_rules('email', 'email', 'callback_rolekey_exists[users-email-edit-user_id-'.$id.']', 'trim|required|max_length[100]',array('required'=>'Please enter %s'));
            // $this->form_validation->set_rules('phone', 'phone', 'callback_rolekey_exists[users-phone-edit-user_id-'.$id.']', 'trim|required',array('required'=>'Please enter %s'));

            $this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

            if($this->form_validation->run()==TRUE) 
            {
                $email = $this->input->post('email');
                $where = array('email' => trim($email),'is_deleted' => 0,'user_id !=' => $id);
                $this->Common_model->getRecords('users','user_id',$where,'user_id DESC',true);
                // echo $this->db->last_query();die;
                if($this->Common_model->getRecords('users','user_id',$where,'user_id DESC',true)) {
                    echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">This email already exists in our system. Please enter another email address.</div>')); 
                    exit;     
                }
                $firstName = $this->input->post('first_name');
                $lastName = $this->input->post('last_name');
                $email = $this->input->post('email');
                $phone = $this->input->post('phone');

                $updateData['first_name'] = $firstName;
                $updateData['last_name'] = $lastName;
                $updateData['email'] = $email;
                $updateData['phone'] = $phone;
                $updateData['modified'] = date("Y-m-d H:i:s");
                $updateData['modified_by'] = $this->session->userdata('admin_id');

                if ($data['details']['email']!=$email) {
                    $newPassword = generateRandomString();
    
                    $updateData['password'] = md5($newPassword);
                    $updateData['is_approve'] = 2;
                    $updateData['status'] = 3;
                    $record = array('otp'=>$newPassword,'name'=>ucwords($firstName.' '.$lastName),'email'=>$email);
                    $this->sendOtpEmail($record);
                }

                if(!$last_id = $this->Common_model->addEditRecords('users', $updateData,array('user_id'=>$id))) { 
                    echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Some error occurred, Please try later .</div>')); exit;     
                } else { 
                    echo json_encode(array('status'=>1,'message'=>'<div class="alert alert-success">User updated successfully.</div>')); exit; 
                } 
            } else {
                echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Please fill all required fields.</div>')); exit; 
            }
        }

    

        $data['form_action']=site_url('admin/user/edit/'.$id);
        $data['back_action']=site_url('admin/user/list');

        $this->load->view('admin/include/header',$data);
        $this->load->view('admin/include/sidebar');
        $this->load->view('admin/user/edit_user');
        $this->load->view('admin/include/footer');
    }

    /********Send OneTime Password Email*******/
    public function sendOtpEmail($detail)
    {
        try{
            $fromEmail = getAdminEmail(); 
            $toEmail = $detail['email']; 
            $otp = $detail['otp']; 
            $recipient_name = $detail['name'];
            $subject = 'Email Change '.WEBSITE_EMAIL_NAME;

            // $data['message'] = "You'r email changed by admin. Your email address is ".$toEmail." and one time password is ".$otp." For login <a href=".base_url().'login'.">Click here</a>";
            $data['message'] = "You are registered with us. Your email address is ".$toEmail." and one time password is ".$otp.". For login <a href=".base_url().'login'.">Click here</a>.";
            
            $data['name'] = $recipient_name;
            $body = $this->load->view('template/common', $data,TRUE);
            $this->Common_model->sendEmail($toEmail,$subject,$body,$fromEmail);
   
            return true;
        }catch(Exception $e){
            return false;
        }
    }       

    public function user_profile($id) 
    {
        $this->Common_model->check_login();
        check_permission('13','view','yes');
        $data['title']="User Detail | ".SITE_TITLE;
        $data['page_title']="User Detail";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'icon'=>'<i class="fa fa-dashboard"></i>',
            'class'=>'',
            'title' => 'Dashboard',
            'link' => site_url('admin/dashboard')
        );
        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'',
            'title' => 'User List',
            'link' => site_url('admin/user/list')
        );
        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'active',
            'title' => 'Detail',
            'link' => ""
        );  
        if(!$data['details'] = $this->Common_model->getRecords('users','*',array('user_id'=>$id),'',true)){
            redirect('pages/page_not_found');
        }

        $user_address = $this->Common_model->getRecords('user_address','*',array('user_id'=>$id,'is_deleted'=>0),'status DESC',false); 
        $address  = array();
        if(!empty($user_address)){
            foreach ($user_address as $key => $list) {
                    $address[$key]= $list;
                    $country = $this->Common_model->getRecords('countries','name',array('id'=>$list['country']),'name asc',true);

                    $address[$key]['country_name']= $country['name'];
                    $state = $this->Common_model->getRecords('states','name',array('country_id'=>$list['country'], 'id'=>$list['state']), '',true);
                    $address[$key]['state_name']= $state['name'];
                    $city = $this->Common_model->getRecords('cities','name',array('state_id'=>$list['state'], 'id'=>$list['city']), '',true); 
                    $address[$key]['city_name']= $city['name']; 

            }
        }
        
        $data['address'] = $address;
        // echo "<pre>";print_r($address);die;

        $data['get_report_post'] = $this->User_model->getReportPost($data['details']['user_id'],true);
        $data['get_like_post'] = $this->User_model->getPostLike($data['details']['user_id'],true);
        $data['get_shared_post'] = $this->User_model->getPostshared($data['details']['user_id'],true);
        $data['get_friends_count'] = $this->Chat_model->getFriendList($data['details']['user_id'],true);
        // echo $this->db->last_query();die;

        if(check_permission('13','edit')){$data['edit_action']=site_url('admin/user/edit/'.$id);}
        
        $data['back_action']=site_url('admin/user/list');
        
        $this->load->view('admin/include/header',$data);
        $this->load->view('admin/include/sidebar');
        $this->load->view('admin/user/user_detail');
        $this->load->view('admin/include/footer');
    }

    public function prospect_details($id) 
    {
        $this->Common_model->check_login();
        check_permission('70','view','yes');
        $data['title']="Prospect Detail | ".SITE_TITLE;
        $data['page_title']="Prospect Detail";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'icon'=>'<i class="fa fa-dashboard"></i>',
            'class'=>'',
            'title' => 'Dashboard',
            'link' => site_url('admin/dashboard')
        );
        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'',
            'title' => 'Prospect List',
            'link' => site_url('admin/user/list')
        );
        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'active',
            'title' => 'Detail',
            'link' => ""
        );  
        $this->User_model->get_user_detail($id);
        if(!$data['details'] = $this->User_model->get_user_detail($id)){
            redirect('pages/page_not_found');
        }
        if($this->input->post()) 
        {   //echo '<pre>';print_r($_POST);exit;
            $update_data = array(
                'prospect_status'=> $this->input->post('prospect_status'),
                'modified'=> date('Y-m-d H:i:s'),
            );
            if(!$last_id = $this->Common_model->addEditRecords('users', $update_data,array('user_id'=>$id))) { 
                echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Some error occurred, Please try later .</div>')); exit;     
            } else { 
                echo json_encode(array('status'=>1,'message'=>'<div class="alert alert-success">Status updated successfully.</div>')); exit; 
            } 
        }
        if(check_permission('70','edit')){$data['edit_action']=1;}
        $data['form_action']=site_url('admin/user/prospect_details/'.$id);
        $data['back_action']=site_url('admin/prospect/list');
        $this->load->view('admin/include/header',$data);
        $this->load->view('admin/include/sidebar');
        $this->load->view('admin/user/prospect_detail');
        $this->load->view('admin/include/footer');
    }


    // Principal List
    public function principal_list()
    {
        $this->Common_model->check_login();
        
        check_permission('13','view','yes');
        if(check_permission('13','add')){$data['add_action']='admin/principal/add';}
        if(check_permission('13','edit')){$data['edit_action']='admin/principal/edit';}
        if(check_permission('13','delete')){$data['delete_action']='1';}
        
        $data['title']="Principal List | ".SITE_TITLE;
        $data['page_title']="Principal List";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'icon'=>'<i class="fa fa-dashboard"></i>',
            'class'=>'',
            'title' => 'Dashboard',
            'link' => site_url('admin/dashboard')
        );

        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'active',
            'title' => 'Principal List',
            'link' => ""
        );

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $getArray = array('display_name','first_name','last_name','email','phone','status','state','district_name','school_name');

        foreach ($getArray as $row) {
            if ($this->input->get($row)) {
                $data['filter_'.$row]=$this->input->get($row);
            } else {
                $data['filter_'.$row]='';
            }
        }


        $user_type=2;
        // $data['total_records']=$this->User_model->getPrincipalList(0,0,$user_type);
        
        $data['states']=  $this->Common_model->getRecords('states');

        $data['form_action']=site_url('admin/principal/list/');
        

        $user_type=2;
        $data['total_records']=$this->User_model->getPrincipalList(0,0,$user_type);
        
        $brdCstdata = $this->session->userdata('brdCstdata');
        $data['states']=  $this->Common_model->getRecords('states');

        $data['form_action']=site_url('admin/principal/list/');
        $records_results = $this->User_model->getPrincipalList(ADMIN_LIMIT,$page,$user_type);
        $data['pagination']=$this->Common_model->paginate(site_url('admin/principal/list'),$data['total_records']);
       
        if (!empty($records_results)) {
            $index = 0;
            foreach ($records_results as $row) {
                $data['records_results'][$index] = $row;

                if (isset($brdCstdata) && !empty($brdCstdata)) {
                    if (in_array($row['user_id'], $brdCstdata)) {
                        $data['records_results'][$index]['is_added'] = 1;
                    } else {
                        $data['records_results'][$index]['is_added'] = 0;
                    }
                } else {
                    $data['records_results'][$index]['is_added'] = 0;
                }

                $index++;
            }
        }
       
        $this->load->view('admin/include/header',$data);
        $this->load->view('admin/include/sidebar');
        $this->load->view('admin/user/principal_list');
        $this->load->view('admin/include/footer');
    } 


    public function principal_export() { 

        $this->Common_model->check_login();
        check_permission('70','view','yes');

        $fileName = EXCEL_PATH.'Principal-'.time().'.xlsx';   

        $user_type=2;
        $recordList = $this->User_model->getPrincipalList('','',$user_type,'all');      

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        // set Header

        $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Sr. No.');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Display Name'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'First Name'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Last Name'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Email'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Phone'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'State'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'District'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'School'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Status'); 


        // set Row
        $rowCount = 2;
        if (!empty($recordList)) {
            foreach ($recordList as $key=> $row) {  

                $status = 'Pending';
                if ($row['status']==1) {
                    $status = 'Active';
                } else if ($row['status']==2) {
                    $status = 'Inactive';
                } else if ($row['status']==3) {
                    $status = 'Pending';
                }

                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $key+1);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, !empty($row['display_name'])?ucfirst($row['display_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, !empty($row['first_name'])?ucfirst($row['first_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, !empty($row['last_name'])?ucfirst($row['last_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, !empty($row['email'])?$row['email']:'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, !empty($row['phone'])?$row['phone']:'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, !empty($row['state_name'])?ucfirst($row['state_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, !empty($row['district_name'])?ucfirst($row['district_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, !empty($row['school_name'])?ucfirst($row['school_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, !empty($status)?$status:'N/A');

                $rowCount++;
            }

        }

    
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($fileName);
        // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect($fileName);  
    }   


    public function edit_principal($id) 
    {
        $this->Common_model->check_login();
        check_permission('13','edit','yes');
        $data['title']="Edit Principal | ".SITE_TITLE;
        $data['page_title']="Edit Principal";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'icon'=>'<i class="fa fa-dashboard"></i>',
            'class'=>'',
            'title' => 'Dashboard',
            'link' => site_url('admin/dashboard')
        );
        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'',
            'title' => 'Principal List',
            'link' => site_url('admin/principal/list')
        );
        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'active',
            'title' => 'Edit Principal',
            'link' => ""
        );  
        // if(!$data['details'] = $this->Common_model->getRecords('users','*',array('user_id'=>$id),'',true)){
        if(!$data['details'] = $this->User_model->getPrincipalDetail($id)){
            redirect('pages/page_not_found');
        }

        // echo "<pre>";print_r($data['details']);die;
        if($this->input->post()) {

            $this->form_validation->set_rules('display_name', 'display name', 'trim|max_length[70]',array('required'=>'Please enter %s'));
            $this->form_validation->set_rules('first_name', 'first name', 'trim|required|max_length[30]',array('required'=>'Please enter %s'));
            $this->form_validation->set_rules('last_name', 'last name', 'trim|required|max_length[30]',array('required'=>'Please enter %s'));
            $this->form_validation->set_rules('email', 'email', 'trim|required',array('required'=>'Please enter %s'));
            // $this->form_validation->set_rules('email', 'email', 'callback_rolekey_exists[users-email-edit-user_id-'.$id.']', 'trim|required|max_length[100]',array('required'=>'Please enter %s'));
            // $this->form_validation->set_rules('phone', 'phone', 'callback_rolekey_exists[users-phone-edit-user_id-'.$id.']', 'trim|required',array('required'=>'Please enter %s'));

            $this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

            if($this->form_validation->run()==TRUE) 
            {
                $email = $this->input->post('email');
                $where = array('email' => trim($email),'is_deleted' => 0,'user_id !=' => $id);
                $this->Common_model->getRecords('users','user_id',$where,'user_id DESC',true);
                // echo $this->db->last_query();die;
                if($this->Common_model->getRecords('users','user_id',$where,'user_id DESC',true)) {
                    echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">This email already exists in our system. Please enter another email address.</div>')); 
                    exit;     
                }


                $displayName = $this->input->post('display_name');
                $firstName = $this->input->post('first_name');
                $lastName = $this->input->post('last_name');
                $email = $this->input->post('email');
                $phone = $this->input->post('phone');

                $updateData['display_name'] = $displayName;
                $updateData['first_name'] = $firstName;
                $updateData['last_name'] = $lastName;
                $updateData['email'] = $email;
                $updateData['phone'] = $phone;
                $updateData['modified'] = date("Y-m-d H:i:s");
                $updateData['modified_by'] = $this->session->userdata('admin_id');

                if ($data['details']['email']!=$email) {
                    $newPassword = generateRandomString();
    
                    $updateData['password'] = md5($newPassword);
                    $updateData['is_approve'] = 2;
                    $updateData['status'] = 3;
                    $record = array('otp'=>$newPassword,'name'=>ucwords($firstName.' '.$lastName),'email'=>$email);
                    $this->sendOtpEmail($record);
                }
                
                if(!$last_id = $this->Common_model->addEditRecords('users', $updateData,array('user_id'=>$id))) { 
                    echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Some error occurred, Please try later .</div>')); exit;     
                } else { 
                    echo json_encode(array('status'=>1,'message'=>'<div class="alert alert-success">Principal updated successfully.</div>')); exit; 
                } 
            } else {
                echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Please fill all required fields.</div>')); exit; 
            }
        }

    

        $data['form_action']=site_url('admin/principal/edit/'.$id);
        $data['back_action']=site_url('admin/principal/list');

        $this->load->view('admin/include/header',$data);
        $this->load->view('admin/include/sidebar');
        $this->load->view('admin/user/edit_principal');
        $this->load->view('admin/include/footer');
    }   

    // Teacher List
    public function teacher_list()
    {
        $this->Common_model->check_login();
        
        check_permission('13','view','yes');
        if(check_permission('13','add')){$data['add_action']='admin/teacher/add';}
        if(check_permission('13','edit')){$data['edit_action']='admin/teacher/edit';}
        if(check_permission('13','delete')){$data['delete_action']='1';}
        
        
        $data['title']="Teacher List | ".SITE_TITLE;
        $data['page_title']="Teacher List";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'icon'=>'<i class="fa fa-dashboard"></i>',
            'class'=>'',
            'title' => 'Dashboard',
            'link' => site_url('admin/dashboard')
        );

        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'active',
            'title' => 'Teacher List',
            'link' => ""
        );

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $getArray = array('first_name','last_name','email','status','state','district_name','school_name');

        foreach ($getArray as $row) {
            if ($this->input->get($row)) {
                $data['filter_'.$row]=$this->input->get($row);
            } else {
                $data['filter_'.$row]='';
            }
        }


        $user_type=3;
        $data['total_records']=$this->User_model->getTeacherList(0,0,$user_type);
        
        
        $data['states']=  $this->Common_model->getRecords('states');

        $data['form_action']=site_url('admin/teacher/list/');
        $records_results = $this->User_model->getTeacherList(ADMIN_LIMIT,$page,$user_type);
        $data['pagination']=$this->Common_model->paginate(site_url('admin/teacher/list'),$data['total_records']);

        if (!empty($records_results)) {
            $index = 0;
            foreach ($records_results as $row) {
                $data['records_results'][$index] = $row;

                if (isset($brdCstdata) && !empty($brdCstdata)) {
                    if (in_array($row['user_id'], $brdCstdata)) {
                        $data['records_results'][$index]['is_added'] = 1;
                    } else {
                        $data['records_results'][$index]['is_added'] = 0;
                    }
                } else {
                    $data['records_results'][$index]['is_added'] = 0;
                }

                $index++;
            }
        }

        $this->load->view('admin/include/header',$data);
        $this->load->view('admin/include/sidebar');
        $this->load->view('admin/user/teacher_list');
        $this->load->view('admin/include/footer');
    } 

    public function teacher_export() { 

        $this->Common_model->check_login();
        check_permission('70','view','yes');

        $fileName = EXCEL_PATH.'Teacher-'.time().'.xlsx';   

        $user_type=3;
        $recordList = $this->User_model->getTeacherList('','',$user_type,'all');        

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        // set Header

        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Sr. No.');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'First Name'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Last Name'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Email'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'State'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'District'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'School'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Status'); 


        // set Row
        $rowCount = 3;
        if (!empty($recordList)) {
            foreach ($recordList as $key=> $row) {  

                $status = 'Pending';
                if ($row['status']==1) {
                    $status = 'Active';
                } else if ($row['status']==2) {
                    $status = 'Inactive';
                } else if ($row['status']==3) {
                    $status = 'Pending';
                }

                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $key+1);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, !empty($row['first_name'])?ucfirst($row['first_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, !empty($row['last_name'])?ucfirst($row['last_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, !empty($row['email'])?$row['email']:'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, !empty($row['state_name'])?ucfirst($row['state_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, !empty($row['district_name'])?ucfirst($row['district_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, !empty($row['school_name'])?ucfirst($row['school_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, !empty($status)?$status:'N/A');

                $rowCount++;
            }

        }

    
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($fileName);
        // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect($fileName);  
    }   

    
    public function edit_teacher($id) 
    {
        $this->Common_model->check_login();
        check_permission('13','edit','yes');
        $data['title']="Edit Teacher | ".SITE_TITLE;
        $data['page_title']="Edit Teacher";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'icon'=>'<i class="fa fa-dashboard"></i>',
            'class'=>'',
            'title' => 'Dashboard',
            'link' => site_url('admin/dashboard')
        );
        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'',
            'title' => 'Teacher List',
            'link' => site_url('admin/teacher/list')
        );
        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'active',
            'title' => 'Edit Teacher',
            'link' => ""
        );  
        // if(!$data['details'] = $this->Common_model->getRecords('users','*',array('user_id'=>$id),'',true)){
        if(!$data['details'] = $this->User_model->getTeacherDetail($id)){
            redirect('pages/page_not_found');
        }

        // echo "<pre>";print_r($data['details']);die;
        if($this->input->post()) {

            $this->form_validation->set_rules('first_name', 'first name', 'trim|required|max_length[30]',array('required'=>'Please enter %s'));
            $this->form_validation->set_rules('last_name', 'last name', 'trim|required|max_length[30]',array('required'=>'Please enter %s'));
            $this->form_validation->set_rules('email', 'email', 'callback_rolekey_exists[users-email-edit-user_id-'.$id.']', 'trim|required|max_length[100]',array('required'=>'Please enter %s'));

            $this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

            if($this->form_validation->run()==TRUE) 
            {
                $updateData = array(
                    'first_name'=> $this->input->post('first_name'),
                    'last_name'=> $this->input->post('last_name'),
                    'email'=> $this->input->post('email'),
                    'phone'=> $this->input->post('phone'),
                    'modified'=> date("Y-m-d H:i:s"),
                    'modified_by'=> $this->session->userdata('admin_id')
                );
                
                if(!$last_id = $this->Common_model->addEditRecords('users', $updateData,array('user_id'=>$id))) { 
                    echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Some error occurred, Please try later .</div>')); exit;     
                } else { 
                    echo json_encode(array('status'=>1,'message'=>'<div class="alert alert-success">Teacher updated successfully.</div>')); exit; 
                } 
            } else {
                echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Please fill all required fields.</div>')); exit; 
            }
        }

    

        $data['form_action']=site_url('admin/teacher/edit/'.$id);
        $data['back_action']=site_url('admin/teacher/list');

        $this->load->view('admin/include/header',$data);
        $this->load->view('admin/include/sidebar');
        $this->load->view('admin/user/edit_teacher');
        $this->load->view('admin/include/footer');
    }   

    // Parent List
    public function parent_list()
    {
        $this->Common_model->check_login();
        
        check_permission('13','view','yes');
        if(check_permission('13','add')){$data['add_action']='admin/parent/add';}
        if(check_permission('13','edit')){$data['edit_action']='admin/parent/edit';}
        if(check_permission('13','delete')){$data['delete_action']='1';}
        
        
        $data['title']="Parent List | ".SITE_TITLE;
        $data['page_title']="Parent List";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'icon'=>'<i class="fa fa-dashboard"></i>',
            'class'=>'',
            'title' => 'Dashboard',
            'link' => site_url('admin/dashboard')
        );

        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'active',
            'title' => 'Parent List',
            'link' => ""
        );

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $getArray = array('first_name','last_name','email','status','state','district_name','school_name');

        foreach ($getArray as $row) {
            if ($this->input->get($row)) {
                $data['filter_'.$row]=$this->input->get($row);
            } else {
                $data['filter_'.$row]='';
            }
        }

        
        $user_type=4;
        $data['total_records']=$this->User_model->getParentList(0,0,$user_type);
        
        
        $data['states']=  $this->Common_model->getRecords('states');

        $data['form_action']=site_url('admin/parent/list/');
        $records_results = $this->User_model->getParentList(ADMIN_LIMIT,$page,$user_type);
        $data['pagination']=$this->Common_model->paginate(site_url('admin/parent/list'),$data['total_records']);

        $data['records_results'] = array();
        if (!empty($records_results)) {
            $index = 0;
            foreach ($records_results as $row) {
                $data['records_results'][$index] = $row;

                if (isset($brdCstdata) && !empty($brdCstdata)) {
                    if (in_array($row['user_id'], $brdCstdata)) {
                        $data['records_results'][$index]['is_added'] = 1;
                    } else {
                        $data['records_results'][$index]['is_added'] = 0;
                    }
                } else {
                    $data['records_results'][$index]['is_added'] = 0;
                }

                $index++;
            }
        }
        $this->load->view('admin/include/header',$data);
        $this->load->view('admin/include/sidebar');
        $this->load->view('admin/user/parent_list');
        $this->load->view('admin/include/footer');
    } 


    public function parent_export() { 

        $this->Common_model->check_login();
        check_permission('70','view','yes');

        $fileName = EXCEL_PATH.'Parent-'.time().'.xlsx';   

        $user_type=4;
        $recordList = $this->User_model->getParentList('','',$user_type,'all');     

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        // set Header

        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Sr. No.');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'First Name'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Last Name'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Email'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'State'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'District'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'School'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Status'); 


        // set Row
        $rowCount = 2;
        if (!empty($recordList)) {
            foreach ($recordList as $key=> $row) {  

                $status = 'Pending';
                if ($row['status']==1) {
                    $status = 'Active';
                } else if ($row['status']==2) {
                    $status = 'Inactive';
                } else if ($row['status']==3) {
                    $status = 'Pending';
                }

                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $key+1);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, !empty($row['first_name'])?ucfirst($row['first_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, !empty($row['last_name'])?ucfirst($row['last_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, !empty($row['email'])?$row['email']:'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, !empty($row['state_name'])?ucfirst($row['state_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, !empty($row['district_name'])?ucfirst($row['district_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, !empty($row['school_name'])?ucfirst($row['school_name']):'N/A');
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, !empty($status)?$status:'N/A');

                $rowCount++;
            }

        }

    
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($fileName);
        // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect($fileName);  
    }   

    
    public function edit_parent($id) 
    {
        $this->Common_model->check_login();
        check_permission('13','edit','yes');
        $data['title']="Edit Parent | ".SITE_TITLE;
        $data['page_title']="Edit Parent";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'icon'=>'<i class="fa fa-dashboard"></i>',
            'class'=>'',
            'title' => 'Dashboard',
            'link' => site_url('admin/dashboard')
        );
        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'',
            'title' => 'Parent List',
            'link' => site_url('admin/parent/list')
        );
        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'active',
            'title' => 'Edit Parent',
            'link' => ""
        );  
        // if(!$data['details'] = $this->Common_model->getRecords('users','*',array('user_id'=>$id),'',true)){
        if(!$data['details'] = $this->User_model->getParentDetail($id)){
            redirect('pages/page_not_found');
        }

        // echo "<pre>";print_r($data['details']);die;
        if($this->input->post()) {

            $this->form_validation->set_rules('first_name', 'first name', 'trim|required|max_length[30]',array('required'=>'Please enter %s'));
            $this->form_validation->set_rules('last_name', 'last name', 'trim|required|max_length[30]',array('required'=>'Please enter %s'));
            $this->form_validation->set_rules('email', 'email', 'callback_rolekey_exists[users-email-edit-user_id-'.$id.']', 'trim|required|max_length[100]',array('required'=>'Please enter %s'));

            $this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

            if($this->form_validation->run()==TRUE) 
            {
                $updateData = array(
                    'first_name'=> $this->input->post('first_name'),
                    'last_name'=> $this->input->post('last_name'),
                    'email'=> $this->input->post('email'),
                    'phone'=> $this->input->post('phone'),
                    'modified'=> date("Y-m-d H:i:s"),
                    'modified_by'=> $this->session->userdata('admin_id')
                );
                
                if(!$last_id = $this->Common_model->addEditRecords('users', $updateData,array('user_id'=>$id))) { 
                    echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Some error occurred, Please try later .</div>')); exit;     
                } else { 
                    echo json_encode(array('status'=>1,'message'=>'<div class="alert alert-success">Parent updated successfully.</div>')); exit; 
                } 
            } else {
                echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Please fill all required fields.</div>')); exit; 
            }
        }

        $data['form_action']=site_url('admin/parent/edit/'.$id);
        $data['back_action']=site_url('admin/parent/list');

        $this->load->view('admin/include/header',$data);
        $this->load->view('admin/include/sidebar');
        $this->load->view('admin/user/edit_parent');
        $this->load->view('admin/include/footer');
    }   


    // Student List
    public function student_list()
    {
        $this->Common_model->check_login();
        
        check_permission('13','view','yes');
        if(check_permission('13','add')){$data['add_action']='admin/student/add';}
        if(check_permission('13','edit')){$data['edit_action']='admin/student/edit';}
        if(check_permission('13','delete')){$data['delete_action']='1';}
        
        
        $data['title']="Student List | ".SITE_TITLE;
        $data['page_title']="Student List";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'icon'=>'<i class="fa fa-dashboard"></i>',
            'class'=>'',
            'title' => 'Dashboard',
            'link' => site_url('admin/dashboard')
        );

        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'active',
            'title' => 'Student List',
            'link' => ""
        );

        $parentID = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;

        $getArray = array('student_name','first_name','last_name','email','status','state','district_name','school_name');

        foreach ($getArray as $row) {
            if ($this->input->get($row)) {
                $data['filter_'.$row]=$this->input->get($row);
            } else {
                $data['filter_'.$row]='';
            }
        }

        $data['total_records']=$this->User_model->getStudentList(0,0,$parentID);
        
        $data['records_results']=$this->User_model->getStudentList(ADMIN_LIMIT,$page,$parentID);
        
        $data['pagination']=$this->Common_model->paginate(site_url('admin/student/list/'.$parentID),$data['total_records'],'seg5');
        $data['states']=  $this->Common_model->getRecords('states');

        $data['form_action']=site_url('admin/student/list/'.$parentID);

        $this->load->view('admin/include/header',$data);
        $this->load->view('admin/include/sidebar');
        $this->load->view('admin/user/student_list');
        $this->load->view('admin/include/footer');
    } 

    public function teacher_grade_replace($grade_str){
        $teacher_grades="";
        $grade_pc=explode(',', $grade_str);
        foreach ($grade_pc as $key => $pc) {
            $teacher_grades.=$this->Common_model->ranges(trim($pc));
            $teacher_grades.=',';
        }
        return  rtrim($teacher_grades,',');
    }   
    function isInArray($needle, $haystack) 
    {
        foreach ($needle as $stack) {
            if (!in_array($stack, $haystack)) {
                return false;
            }
        }
        return true;
    }
       public function school_grades_insert($school_taught_from,$school_taught_to,$school_id,$district_id){
        $school_grades = range($school_taught_from,$school_taught_to);
        $school_grade_data=array();
        if(!empty($school_grades)){
            foreach ($school_grades as $key => $grades) {
                $school_grade_data[]=array(
                    'school_id'=>$school_id,
                    'district_id'=>$district_id,
                    'grade'=>$grades,
                    'grade_display_name'=>DISPLAY_GRADE_NAME[$grades],
                );
            }
        }
        $this->db->insert_batch('school_grade', $school_grade_data);
    }
    /***************** Email Templates *******************/

    public function DeleteAllUser(){

        $post_ids  = $this->input->post('post_id');
        $user_type = $this->input->post('user_type');
        if(empty($post_ids) || empty($user_type)){
            echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Please select users.</div>')); 
            exit;
        }
        $this->session->unset_userdata('brdCstdata');
        foreach($post_ids as $id){ 

            $update_data =array('is_deleted'=>1,'status'=>2,'deleted_date'=>date('Y-m-d H:i:s'));
            $where= array('user_id'=>$id,'user_type'=>$user_type);
            $this->Common_model->addEditRecords('users',$update_data,$where);

            if($user_type==1){
                $records =$this->Common_model->getRecords('users', 'district_id', array('user_id'=>$id), 'user_id desc', true);
                if(!empty($records)){
                    $update_data =array('is_deleted'=>1,'status'=>2,'deleted_date'=>date('Y-m-d H:i:s'));
                    $where= array('district_id'=>$records['district_id']);
                    $this->Common_model->addEditRecords('users',$update_data,$where);

                    $this->Common_model->addEditRecords('district',array('is_deleted'=>1),array('id'=>$records['district_id']));
                    $this->Common_model->addEditRecords('school',array('is_deleted'=>1),array('district_id'=>$records['district_id']));
                }
            }

            if($user_type==2){
                $records =$this->Common_model->getRecords('users', 'school_id', array('user_id'=>$id), 'user_id desc', true);
                if(!empty($records)){
                    $update_data =array('is_deleted'=>1,'status'=>2,'deleted_date'=>date('Y-m-d H:i:s'));
                    $where= array('school_id'=>$records['school_id']);
                    $this->Common_model->addEditRecords('users',$update_data,$where);
                    $this->Common_model->addEditRecords('school',array('is_deleted'=>1),array('id'=>$records['school_id']));

                    $this->Common_model->addEditRecords('chat_groups',array('deleted'=>1),array('school_id'=>$records['school_id']));
                }
            }

            if($user_type==3){
                
                $this->Common_model->addEditRecords('chat_groups',array('deleted'=>1),array('created_by'=>$id));
                $this->Common_model->addEditRecords('chat_message_recipients',array('deleted'=>1,'is_read'=>1),array('created_by'=>$id));
                $this->Common_model->addEditRecords('chat_message_recipients',array('deleted'=>1,'is_read'=>1),array('recipient_id'=>$id));
                $this->Common_model->addEditRecords('chat_user_group_assoc',array('deleted'=>1),array('user_id'=>$id));
            }

            if($user_type==4){

                $this->Common_model->addEditRecords('chat_groups',array('deleted'=>1),array('created_by'=>$id));
                $this->Common_model->addEditRecords('chat_message_recipients',array('deleted'=>1,'is_read'=>1),array('created_by'=>$id));
                $this->Common_model->addEditRecords('chat_message_recipients',array('deleted'=>1,'is_read'=>1),array('recipient_id'=>$id));
                $this->Common_model->addEditRecords('chat_user_group_assoc',array('deleted'=>1),array('user_id'=>$id));
            }
        }

        $msg="User deleted successfully.";
        if($user_type==1){
            $msg=count($post_ids)." district's deleted successfully.";
        }else if($user_type==2){
            $msg=count($post_ids)." principal's deleted successfully.";
        }else if($user_type==3){
            $msg=count($post_ids)." teacher's deleted successfully.";
        }else if($user_type==4){
            $msg=count($post_ids)." parent's deleted successfully.";
        }
            
        echo json_encode(array('status'=>1,'message'=>'<div class="alert alert-success">'.$msg.'.</div>')); exit;
    }
}
