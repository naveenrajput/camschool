<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller {
	public function __construct()
	{
		parent:: __construct();
		$this->load->model('Common_model');
		$this->load->helper('common_helper');
		
	}
	public function list()
	{   
		// echo "string";exit();
		$this->Common_model->check_login();
		$data['title']="Report List | ".SITE_TITLE;
		$data['page_title']="Report List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Report List',
			'link' => ""
		);
		$data['filter_school_name']='';
		if($this->input->get('school_name')){
			$data['filter_school_name']=$this->input->get('school_name');
		}
		$data['filter_media_title']='';
		if($this->input->get('media_title')){
			$data['filter_media_title']=$this->input->get('media_title');
		}
		$data['filter_report_by']='';
		if($this->input->get('report_by')){
			$data['filter_report_by']=$this->input->get('report_by');
		}
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
      
        $data['total_records']=$this->Common_model->get_report_list(0,0,1);
        
        $records_results = $this->Common_model->get_report_list(ADMIN_LIMIT,$page,1);
        $brdCstdata = $this->session->userdata('brdCstdata');

	 	$data['records_results'] = array();
	 	if (!empty($records_results)) {
	 		$index = 0;
	 		foreach ($records_results as $row) {
	 			$data['records_results'][$index] = $row;

	 			if (isset($brdCstdata) && !empty($brdCstdata)) {
		           	if (in_array($row['user_id'], $brdCstdata)) {
			 			$data['records_results'][$index]['is_added'] = 1;
		           	} else {
			 			$data['records_results'][$index]['is_added'] = 0;
		           	}
	 			} else {
		 			$data['records_results'][$index]['is_added'] = 0;
	 			}

	 			$index++;
	 		}
	 	}
	 	 //$data['records_results']=$this->Common_model->get_report_list(ADMIN_LIMIT,$page,1);
	 
        $data['pagination']=$this->Common_model->paginate(site_url('admin/report'),$data['total_records']);
		$data['form_action']=site_url('admin/user/list/');
      
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/report');
		$this->load->view('admin/include/footer');
	}
    public function detail(){
    	 $this->Common_model->check_login();
		$data['title']="Report View | ".SITE_TITLE;
		$data['page_title']="Report View";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Report View',
			'link' => ""
		);
		$data['back_action']=site_url('admin/report');
    	$report_id = $this->uri->segment(4);
    	$data['records_results'] = $this->Common_model->get_single_report($report_id);
    	// echo "<pre>";
    	// print_r($data['records_results']);
    	// die;
    	$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/report_detail');
		$this->load->view('admin/include/footer');
    }

}
