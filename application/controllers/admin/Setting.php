<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends CI_Controller {

	public function __construct() {
		parent::__construct(); 
		$this->load->model('admin/Common_model');
		$this->load->helper('common_helper');
	}

	public function edit_setting($id)	{
		
		$this->Common_model->check_login();
		check_permission('47','edit','yes');

		$data['title']="System Settings | ".SITE_TITLE;
		$data['page_title']="System Settings";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Edit System Settings',
			'link' => ""
		);		
		if(!$data['details'] = $this->Common_model->getRecords('settings','*',array('id'=>$id),'',true)){
			redirect('pages/page_not_found');
		}
		if($this->input->post()) {
			$this->form_validation->set_rules('commission', 'commission', 'trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('before_cancellation', 'before cancellation', 'trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('after_cancellation', 'after cancellation', 'trim|required',array('required'=>'Please enter %s'));
			
			if($this->form_validation->run()==TRUE) 
			{
				$insert_data = array(
					'commission'=> $this->input->post('commission'),
					'before_cancellation'=> $this->input->post('before_cancellation'),
					'after_cancellation'=> $this->input->post('after_cancellation'),
					'modified'=> date("Y-m-d H:i:s"),
					'modified_by'=> $this->session->userdata('admin_id')
				);
		 		
				if(!$last_id = $this->Common_model->addEditRecords('settings', $insert_data,array('id'=>$id))) { 
					$this->session->set_flashdata('error', 'Some error occured! Please try again.');
					redirect("admin/settings/edit/".$id);	
	            } else { 
	            	$admin_id = $this->session->userdata('admin_id');
					$admin_username = getAdminUsername($admin_id);
	            	$log_msg = $admin_username." has updated setting information";
		            actionLog('settings',$last_id,'update',$log_msg,'Admin',$admin_id);    
	                $this->session->set_flashdata('success', 'System settings updated successfully.');
	                redirect("admin/settings/edit/".$id);
	            } 
			} else {
               $this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');
			}
		}
		$data['form_action']=site_url('admin/settings/edit/'.$id);
		$data['back_action']=site_url('admin/dashboard');
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/edit_setting');
		$this->load->view('admin/include/footer');
		
	}


} // class end