<?php defined('BASEPATH') OR exit('No direct script access allowed');
class District extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model(array('Common_model','District_model')); 
        $this->load->helper(array('email','common_helper'));
        
        //$session = $this->session->userdata();
        if(!apiAuthentication()){
            echo json_encode(array('status' =>'0' ,'msg'=>"Error invalid api key"));
            header('HTTP/1.0 401 Unauthorized');
            die;            
        }

    }



    /*===========================  Requested school list for disctrict======================= */
    public function district_request_list()
    {
        try{
            check_user_status();
            $user_id        =   test_input($this->input->post('user_id'));
            if($this->input->post()) {
                $postData = $this->input->post();
                $districtId = test_input($postData['district_id']);
                if (empty($districtId)) {
                    display_output('0','Please enter district id.');
                }
                $this->Common_model->addEditRecords('notification', array('read' => '1'), array('to_user' => (string) $user_id));
                $this->Common_model->addEditRecords('users', array('unread_notification' => 0), array('user_id' => $user_id));

                if ($school =  $this->District_model->getSchoolList($districtId,1)) {
                    display_output('1','School request list.',array('details'=>$school));
                } else {
                    display_output('0','No request found.');
                }
            }
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
    }

    /*=================== School list approved by disctrict========================== */
    public function school_approved_by_district()
    {
        try{
            check_user_status();
            $user_id        =   test_input($this->input->post('user_id'));
            if($this->input->post()) {
                $postData = $this->input->post();
                $districtId = test_input($postData['district_id']);
                if(!isset($postData['school_id'])){
                    display_output('0','Please enter school id.');
                }else{
                    $school_id = test_input($postData['school_id']);
                }
                if (empty($districtId)) {
                    display_output('0','Please enter district id.');
                }
                if (empty($school_id)) {
                    display_output('0','Please enter school id.');
                }
                
                if(!$user_data =$this->Common_model->getRecords('users', 'user_type', array('user_id'=>$user_id,'district_id'=>$districtId,'user_type'=>1), '', true)){
                    display_output('0','Unauthorized action');
                }
                
                $school_arr=explode(',', $school_id);
                $email_notification=array();
                
                $approve_data=array(
                    'is_approve'=>2,
                    'status'=>1,
                    'approved_by'=>$user_id,
                    'modified'=>date('Y-m-d H:i:s')
                    );
                $this->db->trans_begin();  
                if(!empty($school_arr)){
                    for($i=0;$i<count($school_arr);$i++){
                        //echo $i;
                        $school_data =$this->Common_model->getRecords('users', 'user_id,email,first_name,last_name', array('school_id'=>$school_arr[$i]), '', true);
                            $email_notification[]=array(
                                'user_type'=>2,
                                'type'=>'Distrct_approved_school',
                                'status'=>0,
                                'sender_id'=>$user_id,
                                'recipient_name'=>$school_data['first_name'].' '.$school_data['last_name'],
                                'email'=>$school_data['email'],
                                'user_id'=>$school_data['user_id'],
                                'created'=>date('Y-m-d H:i:s'),
                            );
                        $this->Common_model->addEditRecords('users',$approve_data,array('user_type'=>2,'school_id'=>$school_arr[$i]));
                    }
                } 
                $this->db->insert_batch('email_notification', $email_notification);
                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    display_output('0','Check your internet connection and try again.'); 
                } else {
                    $this->db->trans_commit();
                    display_output('1','Request Approved');
                }
            }
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
    }  

    /*================== Approved School list  by disctrict=========================== */
    public function approved_school_by_district()
    {
       try{
            check_user_status();
            $user_id        =   test_input($this->input->post('user_id'));
            if($this->input->post()) {
                $postData = $this->input->post();
                $districtId = test_input($postData['district_id']);
                if (empty($districtId)) {
                    display_output('0','Please enter district id.');
                }

                if ($school =  $this->District_model->getSchoolList($districtId,2)) {
                    display_output('1','School request list.',array('details'=>$school));
                } else {
                    display_output('0','No request found.');
                }

            }
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
    }  


} //End controller class
