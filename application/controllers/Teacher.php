<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Teacher extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
        $this->load->library(array('S3'));
        $this->load->model(array('Common_model','Teacher_model')); 
        $this->load->helper(array('email','common_helper'));
        //$session = $this->session->userdata();
        if(!apiAuthentication()){
            echo json_encode(array('status' =>'0' ,'msg'=>"Error invalid api key"));
            header('HTTP/1.0 401 Unauthorized');
            die;            
        }
    }

    /*======================  Requested teacher list for school====================== */
    public function teacher_request_list()
    {
        try{
            check_user_status();
            $user_id        =   test_input($this->input->post('user_id'));
            if($this->input->post()) {
                $postData = $this->input->post();
                if(!isset($postData['school_id']) || empty($postData['school_id'])){
                    display_output('0','Please enter school id.');
                }else{
                    $school_id   =  test_input($postData['school_id']);
                }
                if(!$check_teacher=$this->Common_model->getRecords('teacher_grade','id',
                        array('teacher_id'=>$user_id,'school_id'=>$school_id),'',true)){
                    display_output('0', 'This teacher record doesn\'t match with The school id.');
                }
                if ($teacher_request =  $this->Teacher_model->getStudentList($user_id,$school_id,1)) {

                    display_output('1','Teacher request list.',array('details'=>$teacher_request));
                } else {
                    display_output('0','No request found.');
                }
            }
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
    }

    /*=================== Student approved by teacher========================== */
    public function student_approved_by_teacher()
    {
        try{
            check_user_status();
            $user_id        =   test_input($this->input->post('user_id'));
            if($this->input->post()) {
                $postData = $this->input->post();
                if(!isset($postData['request_id'])){
                    display_output('0','Please enter request id.');
                }else{
                    $request_id = test_input($postData['request_id']);
                }
                if(!isset($postData['school_id'])){
                    display_output('0','Please enter school id.');
                }else{
                    $school_id = test_input($postData['school_id']);
                }
                if(!$user_data =$this->Common_model->getRecords('users', 'user_type,display_name', array('user_id'=>$user_id,'school_id'=>$school_id,'user_type'=>3), '', true)){
                    display_output('0','You are not authorized to request approved.');
                }
                $request_arr=explode(',', $request_id);
                $email_notification=array();
                
                $approve_data=array(
                    'is_approve'=>2,
                    'approved_by'=>$user_id,
                    'modified'=>date('Y-m-d H:i:s')
                    );
                $invalid_student=array();

                $this->db->trans_begin();  
                if(!empty($request_arr)){
                    for($i=0;$i<count($request_arr);$i++){
                        $this->Common_model->addEditRecords('student_grade',$approve_data,array('id'=>$request_arr[$i]));
                        if($student_info=$this->Teacher_model->getParentByRequest($request_arr[$i])){
                            $parent_info =$this->Common_model->getRecords('users', 'email,first_name,last_name', array('user_id'=>$student_info['parent_id']), '', true);
                            $group_info =$this->Common_model->getRecords('chat_groups', 'id as group_id,', array('created_by'=>$user_id,'grade'=>$student_info['grade'],'school_id'=>$school_id,'deleted'=>0,'is_teacher'=>1), '', true);
                            if(!empty($group_info)){
                                    $chat_group_info=array(
                                    'group_id'=>$group_info['group_id'],
                                    'user_id'=>$student_info['parent_id'],
                                    'created'=>date('Y-m-d H:i:s'),
                                );
                                if(!$check_group_member =$this->Common_model->getRecords('chat_user_group_assoc', 'id', array('deleted'=>0,'is_active'=>1,'group_id'=>$group_info['group_id'],'user_id'=>$student_info['parent_id']), '', true)){

                                    $this->Common_model->addEditRecords('chat_user_group_assoc', $chat_group_info);
                                }
                            }
                            $email_notification[]=array(
                                'user_type'=>4,
                                'type'=>'Teacher_approved_student',
                                'status'=>0,
                                'sender_id'=>$user_id,
                                'recipient_name'=>ucfirst($parent_info['first_name']).' '.$parent_info['last_name'],
                                'email'=>$parent_info['email'],
                                // 'message'=>"Your child ".$student_info['student_name']."'s request has been approved to ".$user_data['display_name']."'s ".DISPLAY_GRADE_NAME[$student_info['grade']]." class.",
                                'message'=>$student_info['student_name']."'s request has been approved to be added to ".$user_data['display_name']."'s ".DISPLAY_GRADE_NAME[$student_info['grade']]." class.",
                                'user_id'=>$student_info['parent_id'],
                                'created'=>date('Y-m-d H:i:s'),
                            );
                        }
                    }
                }

                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    display_output('0','Some error occurred, try again.'); 
                } else {
                    $this->db->trans_commit();
                    if(!empty($email_notification)){
                        
                        $this->db->insert_batch('email_notification', $email_notification);
                    }
                    display_output('1','Request Approved');
                }
            }
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
    }  

    /*======================== Approved student list  by teacher========================== */
    public function approved_student_by_teacher()
    {
       try{
            check_user_status();
            $user_id        =   test_input($this->input->post('user_id'));
            if($this->input->post()) {
                $postData = $this->input->post();
                if(!isset($postData['school_id']) || empty($postData['school_id'])){
                    display_output('0','Please enter school id.');
                }else{
                    $school_id   =  test_input($postData['school_id']);
                }

                if(!$check_teacher=$this->Common_model->getRecords('teacher_grade','id',
                        array('teacher_id'=>$user_id,'school_id'=>$school_id),'',true)){
                   display_output('0', 'This teacher record doesn\'t match with school id.');
                }
                if ($teacher_request =  $this->Teacher_model->getStudentList($user_id,$school_id,2)) {

                    display_output('1','Teacher request list.',array('details'=>$teacher_request));
                } else {
                    display_output('0','No request found.');
                }

            }
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
    } 

    /*======================== Teacher upload video =======================*/

    public function teacher_upload_video() {
       
        try{
            // print_r($_FILES);
            // $tmp_name = $_FILES['file_upload']['tmp_name'];
                            
            // echo $duration = get_duration($tmp_name);
            // die;
            // print_r($_FILES);
            // print_r($_POST);die;
            // display_output('0',$_FILES);
            check_user_status();
            if($this->input->post()) {
                $postData = $this->input->post();
                $user_id = test_input($this->input->post('user_id'));
                $id = test_input($this->input->post('parent_id'));
                $type = test_input($this->input->post('type'));
                $commentParentID = (isset($postData['comment_parent_id']) && !empty($postData['comment_parent_id']))?$postData['comment_parent_id']:0;
                
                if(!isset($postData['grade']) ||  ($postData['grade'] =="")){
                    display_output('0','Please enter grade.');
                }else{
                    $grade = test_input($postData['grade']);
                }
                if(!isset($postData['created_as']) || empty($postData['created_as'])){//only for android
                    display_output('0','Please enter created as.');
                }else{
                    $created_as = test_input($postData['created_as']);
                }
                if(empty($id) && $created_as==2) {
                    display_output('0','Please enter parent id.');
                }
                if($created_as==1){
                    if(!isset($postData['title']) || empty($postData['title'])){
                        display_output('0','Please enter title.');
                    }else{
                        $title = test_input($postData['title']);
                    }
                    if(!isset($postData['description']) || empty($postData['description'])){
                        display_output('0','Please enter description.');
                    }else{
                        $description = test_input($postData['description']);
                    }
                }else{
                    $title="";
                    $description="";
                }
                
                if($type!='web'){
                    if(!isset($postData['school_id']) || empty($postData['school_id'])){
                        display_output('0','Please enter school_id.');
                    }else{
                        $school_id = test_input($postData['school_id']);
                    }
                    if(!isset($postData['format']) || empty($postData['format'])){
                        display_output('0','Please enter format.');
                    }else{
                        $format = test_input($postData['school_id']);
                    }
                    if(!isset($postData['media_name']) || empty($postData['media_name'])){
                        display_output('0','Please enter media name.');
                    }else{
                        $media_name = test_input($postData['media_name']);
                    }
                    if(!isset($postData['duration']) || empty($postData['duration'])){
                        display_output('0','Please enter duration.');
                    }else{
                        $duration = test_input($postData['duration']);
                        $duration=convert_duration($duration);
                    }
                    if(!isset($postData['mobile_type']) || empty($postData['mobile_type'])){ //only for android
                        display_output('0','Please enter mobile type.');
                    }else{
                        $mobile_type = test_input($postData['mobile_type']);
                    }
                    if(!isset($postData['rotation'])){//only for android
                        display_output('0','Please enter rotation.');
                    }else{
                        $rotation = test_input($postData['rotation']);
                    }
                    if(!isset($postData['s3thumbPath'])){
                        display_output('0','Please enter thumb image path.');
                    }else{
                        $s3thumbPath = test_input($postData['s3thumbPath']);
                    }
                    if(!isset($postData['s3largeimagePath'])){
                        display_output('0','Please enter large image path.');
                    }else{
                        $s3largeimagePath = test_input($postData['s3largeimagePath']);
                    }
                    $media_type=1;
                    if(!isset($mobile_type)) {
                        $mobile_type = 'ios';
                        $rotation = -1;
                    } else if($mobile_type=='android') {
                        if(!isset($rotation)) {
                            display_output('0','Please enter rotation.');
                        } 
                    }
                }else{
                    $user_id=$this->session->userdata('user_id');
                    $school_id=$this->session->userdata('school_id');
                }
                
                if($created_as==2){
                    if(!$media_check = $this->Common_model->getRecords('media','id',array("user_id"=>$user_id,"school_id"=>$school_id,'id'=>$id),'',true)){
                        display_output('0','Parent media not found.');
                    }
                }
                if(!$user_data = $this->Common_model->getRecords('users','user_type,user_number,display_name,email',array("user_id"=>$user_id,"school_id"=>$school_id,'user_type'=>3),'',true)){
                     display_output('0','You are not authorize to upload  video for this school.');
                }
                if($teacher_grade_check = $this->Common_model->getRecords('teacher_grade','GROUP_CONCAT(grade) as grade',array("teacher_id"=>$user_id,"school_id"=>$school_id),'',true)){
                    $teacher_grade_arr=explode(',', $teacher_grade_check['grade']);
                    if(!in_array($grade,  $teacher_grade_arr)){
                        display_output('0','Please select valid grade.');
                    }
                }else{
                    display_output('0','Please select valid grade at your profile first.');
                }
                if($type=='web'){
                    $format="video/mp4";
                    $s3 = new S3(AMAZON_ID, AMAZON_KEY);
                    if(!is_dir(MEDIA_THUMB_TEMP_PATH)){
                        mkdir(MEDIA_THUMB_TEMP_PATH);
                    } 
                    if(!empty($_FILES['file_upload'])){
                        if(!empty($_FILES['file_upload']['name'])){
                            $tmp_name = $_FILES['file_upload']['tmp_name'];
                            $ext = strtolower(substr(strrchr($_FILES['file_upload']['name'], '.'), 1)); 
                            $thumb_image="";
                            $thumb_imagePath="";
                            $large_image="";
                            $large_imagePath="";
                            if($ext=='avi' || $ext=='mov' || $ext=='mp4') {
                                $duration = get_duration($tmp_name);
                                $media_name = 'video'.'_'.time().'_v.mp4';
                                
                                $images = createImage($tmp_name,MEDIA_THUMB_TEMP_PATH,"800x450",$duration);
                                $duration=gmdate("H:i:s", $duration);
                                $thumb_image = $images[1];
                                $large_image = $images[0];
                                $thumb_imagePath = MEDIA_THUMB_TEMP_PATH.$thumb_image;
                                $large_imagePath = MEDIA_THUMB_TEMP_PATH.$large_image;
                                //Convert and upload video on server
                                $newSource =MEDIA_PATH.$media_name;
                                $command = 'ffmpeg -i ' . $tmp_name . ' -r 21 -vcodec libx264 -b:v 1M -crf 32 -preset veryfast -profile:v baseline -level 3.0 -c:a copy -movflags +faststart -tune zerolatency '.$newSource ;
                                $output = shell_exec($command);
                                $s3dir = $user_data['user_number'].'/';
                                $savemediapath = $s3dir.$media_name;
                                if($s3->putObjectFile($newSource, AMAZON_BUCKET, $savemediapath, S3::ACL_PUBLIC_READ)) 
                                {
                                    //upload media on the amazon s3
                                    $s3mediapath =AMAZON_PATH.$savemediapath;
                                    $media_name=$s3mediapath;
                                    $s3thumbPath='';
                                    $s3largeimagePath='';
                                    if($thumb_imagePath !='') {
                                        //store thumb image on the amazon s3 and delete temp file
                                        $save_path = $s3dir.$thumb_image;
                                        $s3->putObjectFile($thumb_imagePath, AMAZON_BUCKET, $save_path, S3::ACL_PUBLIC_READ);
                                        $s3thumbPath=AMAZON_PATH.$save_path;
                                        if(file_exists($thumb_imagePath)) {
                                            @unlink($thumb_imagePath);
                                        }
                                    }
                                    if($large_imagePath !='') {
                                        //store thumb image on the amazon s3 and delete temp file
                                        $save_path = $s3dir.$large_image;
                                        $s3->putObjectFile($large_imagePath, AMAZON_BUCKET, $save_path, S3::ACL_PUBLIC_READ);
                                        $s3largeimagePath=AMAZON_PATH.$save_path;
                                        if(file_exists($large_imagePath)) {
                                            @unlink($large_imagePath);
                                        }
                                    }
                                    if(file_exists($newSource)) {
                                        @unlink($newSource);
                                    }
                                }else{
                                    display_output('0','Some issue occured in file upload may be your file corrupted.');
                                }
                            }else{
                                display_output('0','File must be avi or mov or mp4 .');
                            }
                        }
                        

                    }else{
                        display_output('0','Please upload file.');
                    }
                }else{
                    if(empty($s3thumbPath) || empty($s3largeimagePath))
                    {
                        $s3Images=getImageFromVideo($duration,1,$media_name,$mobile_type,$rotation,'school');
                        if(!empty($s3Images)){
                            $s3thumbPath=$s3Images['s3thumbPath'];
                            $s3largeimagePath=$s3Images['s3largeimagePath'];
                        }else{
                            $s3thumbPath="";$s3largeimagePath="";
                        }
                    }
                }
                
                $district_id = $this->Common_model->getFieldValue('school','district_id',array("id"=>$school_id));
                $teacher_grade_id= $this->Common_model->getFieldValue('teacher_grade','id',array("school_id"=>$school_id,"teacher_id"=>$user_id,"grade"=>$grade));
                $insert_data = array(
                    'title' => $title,
                    'description' => $description,
                    'user_id' => $user_id,
                    'grade' => $grade,
                    'teacher_grade_id' => $teacher_grade_id,
                    'grade_display_name' => DISPLAY_GRADE_NAME[$grade],
                    'school_id' => $school_id,
                    'district_id' => $district_id,
                    'format' => $format,
                    'thumb_image' => $s3thumbPath,
                    'large_image' => $s3largeimagePath,
                    'media_name' => $media_name,
                    'media_type' => 1,
                    'status' => 1,
                    'created_as' => $created_as,
                    'duration' => $duration,
                    'created' => date("Y-m-d H:i:s")
                );
                
                $this->db->trans_begin();
                if($media_id = $this->Common_model->addEditRecords('media',$insert_data)) {
                    $media_slug = create_slug_with_id($title,$media_id);
                    $this->Common_model->addEditRecords('media',array('media_slug'=>$media_slug),array('id'=>$media_id));
                    if($id || $created_as==2) {
                        $comment_data = array(
                            'user_id' => $user_id,
                            'parent_id' => $commentParentID,
                            'media_id' => $id,
                            'comment_type' => 1, 
                            'comment' => $media_id,
                            'created' => date("Y-m-d H:i:s")
                        );
                        if($this->Common_model->addEditRecords('comments',$comment_data)){
                            $total_comments = $this->Common_model->getNumRecords('comments', '', array('media_id'=>$id));
                            $this->Common_model->addEditRecords('media',array('comments'=>$total_comments),array('id'=>$id));
                        }
                    }
                }
                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    display_output('0','Some error occurred, try again.'); 
                } else {
                    $this->db->trans_commit();
                    $email_notification=array();
                    if($created_as==1){
                        if($parent_info=$this->Teacher_model->getParentByTeacherGradeId($teacher_grade_id)){
                            foreach ($parent_info as $key => $value) {
                               $email_notification[]=array(
                                    'user_type'=>4,
                                    'type'=>'Teacher_upload_video',
                                    'status'=>0,
                                    'sender_id'=>$user_id,
                                    'email'=>$value['email'],
                                    'sender_email'=>$user_data['email'],
                                    'sender_name'=>ucfirst($user_data['display_name']),
                                    'recipient_name'=>$value['first_name'].' '.$value['last_name'],
                                    'subject'=>$media_id,
                                    'message'=>ucfirst($user_data['display_name'])." uploaded a new video to the ".DISPLAY_GRADE_NAME[$grade].' class page.',
                                    // 'message'=>ucfirst($user_data['display_name'])." uploaded new video for ".DISPLAY_GRADE_NAME[$grade],
                                    'user_id'=>$value['parent_id'],
                                    'created'=>date('Y-m-d H:i:s'),
                                );
                            }
                        }
                        if(!empty($email_notification)){
                            $this->db->insert_batch('email_notification', $email_notification);
                        }
                        display_output('1','Video successfully uploaded', array('media_id'=>(string)$media_id,'image_path'=>$s3largeimagePath));
                    }else{
                        display_output('1','Comment posted successfully.');
                    }
                }
                
            } 
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
    }

    /*======================== Teacher update video content ======================*/

    public function teacher_update_video_content($id="") {
        try{
            check_user_status();
            if($this->input->post()) {
                $postData = $this->input->post();
                $type = test_input($this->input->post('type'));
                if($type=='web'){
                    $user_id = $this->session->userdata('user_id');
                    $school_id = $this->session->userdata('school_id');
                    if(!isset($postData['media_slug']) || empty($postData['media_slug'])){
                        display_output('0','Please enter media slug.');
                    }else{
                        $media_slug = test_input($postData['media_slug']);
                    }
                }else{
                    $user_id = test_input($this->input->post('user_id'));

                    if(!isset($postData['media_id']) || empty($postData['media_id'])){
                        display_output('0','Please enter media id.');
                    }else{
                        $media_id = test_input($postData['media_id']);
                    }
                    if(!isset($postData['school_id']) || empty($postData['school_id'])){
                        display_output('0','Please enter school_id.');
                    }else{
                        $school_id = test_input($postData['school_id']);
                    }
                }
                
                if(!isset($postData['title']) || empty($postData['title'])){
                    display_output('0','Please enter title.');
                }else{
                    $title = test_input($postData['title']);
                }
                if(!isset($postData['description']) || empty($postData['description'])){
                    display_output('0','Please enter description.');
                }else{
                    $description = test_input($postData['description']);
                }
                
                if(!isset($postData['grade']) ||  ($postData['grade'] =="")){
                    display_output('0','Please enter grade.');
                }else{
                    $grade = test_input($postData['grade']);
                }
                if($type!='web'){
                    if(!$media_data = $this->Common_model->getRecords('media','id',array("user_id"=>$user_id,"school_id"=>$school_id,'is_deleted'=>0,'id'=>$media_id),'',true)){
                         display_output('0','Media not found.');
                    }
                }else{
                    if(!$media_data = $this->Common_model->getRecords('media','id',array("user_id"=>$user_id,"school_id"=>$school_id,'is_deleted'=>0,'media_slug'=>$media_slug),'',true)){
                         display_output('0','Media not found.');
                    }
                    $media_id=$media_data['id'];
                }
                if($teacher_grade_check = $this->Common_model->getRecords('teacher_grade','GROUP_CONCAT(grade) as grade',array("teacher_id"=>$user_id,"school_id"=>$school_id),'',true)){
                    $teacher_grade_arr=explode(',', $teacher_grade_check['grade']);
                    if(!in_array($grade,  $teacher_grade_arr)){
                        display_output('0','Please select valid grade.');
                    }
                }else{
                    display_output('0','Please select grade at your profile first.');
                }
                
                $media_slug = create_slug_with_id($title,$media_id);
                $insert_data = array(
                    'title' => $title,
                    'description' => $description,
                    'user_id' => $user_id,
                    'grade' => $grade,
                    'grade_display_name' => DISPLAY_GRADE_NAME[$grade],
                    //'media_slug' => $media_slug,
                    'created' => date("Y-m-d H:i:s")
                );
                if($media_id = $this->Common_model->addEditRecords('media',$insert_data,array('id'=>$media_id))) {
                    display_output('1','Content updated successfully');
                } else {
                    display_output('0','Some error occured. Please try again.');
                } 
            } 
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
    }

    /*======================== Teacher delete video  ======================*/

    public function teacher_delete_video() {
        try{
            error_reporting(0);
            check_user_status();
            if($this->input->post()) {
                $postData = $this->input->post();
                $type = test_input($this->input->post('type'));
                if($type=='web'){
                    $user_id =$this->session->userdata('user_id');
                }else{
                    $user_id = test_input($this->input->post('user_id'));
                }
                if(!isset($postData['media_id']) || empty($postData['media_id'])){
                    display_output('0','Please enter media id.');
                }else{
                    $media_id = test_input($postData['media_id']);
                }
                if(!$media_data = $this->Common_model->getRecords('media','id,media_name,thumb_image,large_image',array("user_id"=>$user_id,'is_deleted'=>0,'id'=>$media_id),'',true)){
                     display_output('0','Media not found.');
                }else
                {
                    /*$s3 = new S3(AMAZON_ID, AMAZON_KEY); 
                    if($media_data['thumb_image']!='') {
                        $thumb_image = getDeletePath($media_data['thumb_image']);
                        $object = $s3->getObjectInfo(AMAZON_BUCKET,$thumb_image);
                        if($object) {
                            $s3->deleteObject(AMAZON_BUCKET, $thumb_image);
                        } 
                    }
                    if($media_data['large_image']!='') {
                        $large_image = getDeletePath($media_data['large_image']);
                        $object = $s3->getObjectInfo(AMAZON_BUCKET,$large_image );
                        if($object) {
                            $s3->deleteObject(AMAZON_BUCKET, $large_image);
                        } 
                    }
                    if($media_data['media_name']!='') {
                        $media_name = getDeletePath($media_data['media_name']);
                        $object = $s3->getObjectInfo(AMAZON_BUCKET,$media_name );
                        if($object) {
                            $s3->deleteObject(AMAZON_BUCKET, $media_name);
                        }
                    }*/
                }
                $update_data = array(
                    'is_deleted' => 1,
                    'deleted_by' => $user_id,
                    'deleted_date' => date("Y-m-d H:i:s"),
                    'modfied' => date("Y-m-d H:i:s")
                );
                if($media_id = $this->Common_model->addEditRecords('media',$update_data,array('id'=>$media_id))) {
                    display_output('1','Video deleted successfully');
                } else {
                    display_output('0','Some error occured. Please try again.');
                } 
            } 
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
    }

    /*============================= My upload ==========================*/

    public function teacher_my_upload() {
        try{
            $this->load->model('Parent_student_model');             
            check_user_status();
            if($this->input->post()) {
                $postData = $this->input->post();
                $user_id = test_input($this->input->post('user_id'));
                $grade = test_input($this->input->post('grade'));
                if($video_list = $this->Teacher_model->getAllVideos($user_id,"","","",$grade)){
                    $schoolId = $video_list[0]['school_id'];
                    $school_video=$this->Parent_student_model->getSchoolVideo($schoolId);
                    display_output('1','Video List',array('video_list'=>$video_list,'school_video'=>$school_video));
                }else{
                    display_output('0','No record found.');
                }
            }
        }catch(Exception $e){
            display_output('0', $e->getMessage());
        }
    }



    


} //End controller class
