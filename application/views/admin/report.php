<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12 message_box">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-block alert-success fade in">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
				<?php } ?>
				<div class="box">
                   
		

					 <div class="box-header with-border">  
                		<form method="get" action="<?php echo base_url('admin/report');//$form_action; ?>" > 
							<div class="box-body row"> 
							<div class="form-group col-md-3">
					              <input class="column_filter form-control" id="report_by" name='report_by' type="text" placeholder="Report by" value="<?php echo $filter_report_by;?>">
	                        	</div> 
				              	<div class="form-group col-md-3">
	                                <input class="column_filter form-control" id="school" name='school_name' type="text" placeholder="School Name" value="<?php echo $filter_school_name;?>">
				              	</div>
				              	<div class="form-group col-md-3">
					              <input class="column_filter form-control" id="midia_title" name='media_title' type="text" placeholder="Media title" value="<?php echo $filter_media_title;?>">
	                        	</div>
	                        	
				              	
				              	                        	
				             <div class="form-group col-md-3">
				               	<input class="btn btn-primary" type="submit" value="Filter">
				               	<a class="btn btn-default" href="admin/report">Reset</a>
				 
				             </div>
				             
				          	</div>
			          	</form> 
                        <div id="notification_msg"> </div>
                	</div> 
                    
					<div class="box-body">

						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									
									<th>Report By</th>
									<th>School Name</th>
									<th>Media Title</th>
									<th>Comment By</th>
									<th>Comment Date</th>
									<th>Action</th>
									<?php if(isset($edit_action) && !empty($edit_action)){ ?>
										<th>Profile Status</th>
									<?php } ?>
									 <?php if(isset($edit_action) || isset($delete_action)){ ?>
                                    	<th>Actions</th>
                                    <?php } ?>
								</tr>
							</thead>
							<tbody>
	
                                <?php foreach($records_results as $value) { ?>
                                	<tr>
                                		
	                             	    <td><?php 
                                            if(!empty($value['display_name'])){
                                            	echo ucfirst(wordwrap($value['display_name'] ,30, "<br />\n",true));
                                                //echo ucwords($value['display_name']);
                                            }else{
                                            	echo ucfirst(wordwrap($value['first_name'].' '.$value['last_name'], 30, "<br />\n",true));
                                                //echo ucwords($value['first_name'].' '.$value['last_name']);
                                            }
                                            ?> 

                                        </td>
										<td><?php echo ucfirst(wordwrap($value['name'], 30, "<br />\n",true));//echo $value['name'];?></td>
										<td><?php echo ucfirst($value['title']);?></td>
										
										<td><?php echo ucfirst($this->db->get_where('users',array('user_id'=>$value['cid']))->row()->first_name); ?></td>
										<td>
											<?php echo convertGMTToLocalTimezone($value['created']) ?></td>
										<td>
                                            <a id="" href="<?php echo site_url('admin/report/detail/'.$value['reportId']);?>" class="btn btn-xs btn-primary" title="" data-toggle="tooltip" data-original-title="view Report">
														<i class="fa fa-eye"></i>
													</a>
													
											<a href="javascript:void(0)" class="btn btn-xs btn-danger" <?php if($value['is_delete']==1){ ?> disabled="disabled";data-original-title="Deleted" <?php }else { ?> onclick="soft_delete_record(<?php echo $value['comment_id'];?>,'comment_report','id')" data-toggle="tooltip" data-original-title="Delete"<?php  }?>>
														<i class="fa fa-trash-o"></i>
													</a></td>
									</tr>
						        <?php } ?>		
							</tbody>
                              <tfoot>						
								<tr>
									<?php if(!empty($pagination)) { ?>
										<td >Total Records - <?php echo $total_records;?></td>
										<td colspan="7" align="center">
											<div><?php echo $pagination; ?></div>
										</td>
									<?php }else{ ?>	
										<td align="center">Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
										<td colspan="7" align="center"></td>
									<?php } ?>			
								</tr>
							</tfoot>	
						</table>
							
					</div>			
				</div>			
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
	
	$("#idForm").submit(function(e) {

    e.preventDefault(); // avoid to execute the actual submit of the form.

    var form = $(this);
    var url = form.attr('action');
    
    $.ajax({
           type: "POST",
           url: url,
           data: form.serialize(), // serializes the form's elements.
           success: function(data)
           {
               alert(data); // show response from the php script.
           }
         });

    
});
</script>