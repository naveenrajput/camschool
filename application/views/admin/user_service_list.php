
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-block alert-success fade in">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
				<?php } ?>
				<div class="box">
					<div class="box-header with-border">  
                  		<!-- <h3 class="box-title">Filter Here</h3>   -->
              			<div class="box-body row"> 
                			<form method="get" action="<?php echo $reset_action; ?>"> 
						
				              	<div class="form-group col-md-3">
	                                <input class="column_filter form-control" id="services_title" name='services_title' type="text" placeholder="Services Title" value="<?php echo $filter_services_title;?>">
				              	</div>   
				              	<?php if(empty($filter_user_id)){?> 
				              	<div class="form-group col-md-3">
	                                <input class="column_filter form-control" id="username" name='username' type="text" placeholder="User Name" value="<?php echo $filter_username;?>">
				              	</div>   
				              	<?php }?>
				              	<div class="form-group col-md-3">
	                                <input class="column_filter form-control" id="category_title" name='category_title' type="text" placeholder="Category Name" value="<?php echo $filter_category_title;?>">
				              	</div>   
                                <input class="column_filter form-control" id="user_id" name='user_id' type="hidden" value="<?php echo $filter_user_id;?>">  
					              	<div class="form-group col-md-3">
					              		<select name="status" id="status" class="column_filter form-control">
					              			<option value="">Status</option>
					              			<option value="Active" <?php if(!empty($filter_status)&& $filter_status=='Active'){ echo 'selected'; }?>>Active</option>
					              			<option value="Inactive" <?php if(!empty($filter_status)&& $filter_status=='Inactive'){ echo 'selected'; }?>>Inactive</option>
					              		</select> 
		                        	</div>  
				             	<div class="form-group col-md-3">
				               		<input class="btn btn-primary" type="submit" value="Filter">
				               		<a class="btn btn-default" href="<?php echo $reset_action; ?>">Reset</a>
			               		  	<?php if(!empty($back_url)){?>
		           	              	<a class="btn btn-success" href="<?php echo $back_url;?>">Back</a>
					             	<?php }?>
				             	</div>
			          		</form> 
            			</div>
                	</div> 

					<div class="box-body">

						<table <?php  if(!empty($records_results))
								{	?>  <?php } ?> class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Service Title</th>
									<?php if(empty($filter_user_id)){?>
										<th>User Name</th>
									<?php }?>
									<th>Category Name</th>
									<th>Description</th> 
									<th>Created</th>
									<?php if(isset($edit_action) && !empty($edit_action)){ ?>
									<th>Status</th> 
									<th>Actions</th>
									<?php } ?>
								</tr>
							</thead>
							<tbody>
	

								<?php 
								if(!empty($records_results))
								{	
									$i = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
									$table="services";
									$field = "id";

									foreach ($records_results as $row) { $i++; 
										if(isset($row['status'])) {
	                                        if($row['status']=="Active") {
	                                            $status = "Active";
	                                            $class = "pointer badge bg-green";
	                                        } else {
	                                            $status = "Inactive";
	                                            $class = "pointer badge bg-red";
	                                        }
                                    	} ?>
                                     
                                    	                                   	
										<tr id="tr_<?php echo $row[$field]; ?>">
											<td><?php echo $i; ?></td>
											<td><?php if(!empty($row['service_title'])) echo ucfirst($row['service_title']);?></td>
											<?php if(empty($filter_user_id)){?> 
												<td><?php if(!empty($row['fullname'])) echo ucfirst($row['fullname']);?></td> 
							              	<?php }?>
											<td>
												<?php if(!empty($row['title'])) echo $row['title']; ?>
											</td>
											<td>
												<?php if(!empty($row['description']))  if(strlen($row['description']) > 25) {echo substr($row['description'],0,25).'...';}else{ echo $row['description'];} ?>
											</td> 
											<td><?php if(!empty($row['created'])) echo convertGMTToLocalTimezone($row['created'],true); ?></td>

												<?php if(isset($edit_action) && !empty($edit_action)){ ?>
												<td>
													<p id="status_<?php echo $row[$field]; ?>" onclick="change_status('<?php echo $field; ?>','<?php echo $row[$field]; ?>','<?php echo $table; ?>')" class="<?php echo $class; ?>" title="" data-toggle="tooltip" data-original-title="Change Status"><?php echo $status; ?></p>
												</td>
											<?php } ?> 
											<td class="td-actions">
											<div class="btn-group">
								                  <button type="button" class="btn btn-sm btn-info">Action</button>
								                  <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								                    <span class="caret"></span>
								                    <span class="sr-only">Toggle Dropdown</span>
								                  </button>
											 <ul class="dropdown-menu" role="menu">
											 	<li><a href="<?php echo base_url().'admin/services/view/'.$row[$field]?>">Details</a></li> 
											 	<li><a href="<?php echo base_url().'admin/services/offer/'.$row[$field]?>">Offers</a></li> 
											 	<?php if(isset($delete_action) && !empty($delete_action)) { ?>
													
														<li><a  href="javascript:void(0)"  onclick="return soft_delete_record(<?php if(!empty($row[$field])) echo $row[$field]; ?>,'<?php echo $table; ?>','<?php echo $field; ?>');">Delete</a></li> 

												 
											<?php } ?>
											 	</ul>
										 	 </div>	  
											</td>
										</tr>

									<?php }
								} else {
									echo "<tr><td colspan='10' align='center'> No Record Found</td></tr>";
								} ?>
							</tbody>
							<tfoot>						
								<tr>
									<?php if(!empty($pagination)) { ?>
										<td colspan="2" >Total Records - <?php echo $total_records;?></td>
										<td colspan="10" align="center">
											<div><?php echo $pagination; ?></div>
										</td>
									<?php }else{ ?>	
										<td colspan="2">Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
										<td colspan="10" align="center"></td>
									<?php } ?>			
								</tr>
							</tfoot>
						</table>
					</div>			
				</div>			
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->


