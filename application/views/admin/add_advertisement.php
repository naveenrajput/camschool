<div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $page_title;?> </h1>
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php }?>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-primary"> 
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">  
                    <div class="col-lg-12">
                        <!-- flash messages-->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('error') ?>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success')) { ?>
                            <div class="alert alert-block alert-success fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('success') ?>
                            </div>
                        <?php } ?>
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <?php if(isset($from_action) && !empty($from_action)){ ?>
                                    <form class="" method="POST" id="add_advertisement_form" action="<?php echo $from_action; ?>" enctype="multipart/form-data" role="form"  data-parsley-validate>
                                <?php } ?>
                                <div class="box-body">
                                    <div class="form-group col-lg-12">
                                        <label class="">Image *</label>
                                        <!-- <div class="col-sm-4"> -->
                                        <div data-provides="fileupload" class="fileupload fileupload-new">
                                            <input type="hidden">
                                            <div style="min-width: 150px;min-height: 120px;" class="fileupload-new thumbnail"> 
                                            <!-- <img alt="No Image" src=""> --> 
                                            </div>
                                            <div style="min-width: 150px;min-height: 120px; line-height: 5px;" class="fileupload-preview fileupload-exists thumbnail"></div>
                                            <div> 
                                                <span class="btn btn-file">
                                                    <span class="fileupload-new btn btn-default">Select image</span> 
                                                    <span class="fileupload-exists">Change</span>
                                                    <input type="file" name="image" class="default" accept="image/*" onchange="validate_image_upload(this);" data-parsley-required data-parsley-required-message="Please upload image." data-parsley-errors-container='#image_error'>
                                                </span> 
                                                <a data-dismiss="fileupload" class="btn fileupload-exists error v-align-middle">Remove</a> 
                                            </div>
                                            <div id="image_error" class="error error1"></div>
                                            <?php echo isset($upload_error)?$upload_error:'';?> 
                                        </div>
                                    </div>
                                     <div class="form-group col-lg-12">
                                        <label for="title">Title *</label>
                                        <input type="text" class="form-control" name="title" id="title" value="<?php echo set_value('title'); ?>" placeholder="Title" maxlength="150" data-parsley-required data-parsley-required-message="Please enter title.">
                                        <?php echo form_error('title');?> 
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <label for="title">Advertisement URL *</label>
                                        <input type="text" placeholder="Advertisement URL" name="advertisement_url"  id="advertisement_url" class="form-control " value="<?php echo set_value(' advertisement_url'); ?>" type="url"  data-parsley-type="url" data-parsley-required data-parsley-required-message="Please enter advertisement URL." data-parsley-type-message="Please enter valid advertisement URL.">
                                        <?php echo form_error('advertisement_url');?> 
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label for="title">Expiry Date *</label>
                                        <div class="input-group">
                                              <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                              </div>
                                                <input type="text" placeholder="Expiry Date" name="expiry_date"  id="expiry_date" autocomplete="off" class="form-control " value="" data-date-format="mm/dd/yyyy"   oninput="this.value = this.value.replace(/[^]/g, '').replace(/(\..*)\./g, '$1');"  data-parsley-errors-container="#expiry_date_error" data-parsley-required data-parsley-required-message="Please select expiry date." onchange="error_blank()">
                                                <?php echo form_error('expiry_date');?> 
                                        </div>
                                        <div id="expiry_date_error"></div>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label>Status *</label>
                                        <?php $options_status = array('Active'  => 'Active','Inactive'  => 'Inactive');
                                        echo form_dropdown('status', $options_status, set_value('status'),'class="form-control"');?>
                                        <?php echo form_error('status');?> 
                                    </div>
                                </div>
                                <div class="box-footer text-center">
                                    <?php if(isset($from_action) && !empty($from_action)){ ?>
                                        <button type="submit" id="add" class="btn btn-primary" onclick="return form_submit('add_advertisement_form');">Add</button>
                                    <?php } ?>
                                    <a href="<?php echo $back_action;?>" class="btn btn-default">Back</a> 
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box --> 
            </div><!-- col-12--> 
        </div><!-- row--> 
    </section>
</div>
<!-- row--> 
<!-- Include Bootstrap Datepicker -->

<script type="text/javascript">
function validate_image_upload(input) {
    $(input).parent().parent().parent().find('.error1').html('');
    if (!input.files[0].name.toLowerCase().match(/\.(jpg|jpeg|png)$/)) {
        $(input).val("");
        $(input).parent().parent().parent().find('.error1').html("Only JPG|JPEG|PNG file types allowed.");
        return false;
    }
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(input.files[0]);

        reader.onload = function (e) {
            var image = new Image();
            image.src = e.target.result; 
            image.onload = function () {
                //Determine the Height and Width.
                var height = this.height;
                var width = this.width;
                // alert(width);
                  if(width<700 ||  width>1150 || height<50 || height>100)
                {
                    $(input).val("");
                    $(input).parent().parent().parent().find('.error1').html("Image width  in between 700 to 1150 & height in between 50 to 100.");
                    $(input).parent().parent().parent().find('.fileupload-preview img').attr('src','');
                    return false;
                }
            }
        }
    }
}


$(function () {
    $('#expiry_date').datepicker({
          "startDate":"date('m/d/y')" 
    });
});

function error_blank()
{
    $("#expiry_date_error li").text('');
}

function form_submit(id)
{
    $("#"+id).parsley().validate();
    if($("#"+id).parsley().isValid()){
       //$("#add").attr('disabled',true);
       $("#loader").show(); 
       return true;
    }else{
        return false;
    }
}

</script>

