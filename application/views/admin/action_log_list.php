<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-block alert-success fade in">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
				<?php } ?>
				<div class="box">
					<div class="box-header with-border">  
                  		<!-- <h3 class="box-title">Filter Here</h3>   -->
                		<form method="get" action="<?php echo base_url().'admin/action_log/list';?>"> 
							<div class="box-body row"> 
				              	<div class="form-group col-md-3">
                                    <div class="input-group">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control" id="action_date" name="action_date" value="<?php echo $filter_action_date;?>" Placeholder="Action Log Date Range"  oninput="this.value = this.value.replace(/[^]/g, '').replace(/(\..*)\./g, '$1');" autocomplete="off">
		                        	</div> 
	                        	</div> 
				              	<div class="form-group col-md-3">
	                                <input class="column_filter form-control" id="admin_name" name='admin_name' type="text" placeholder="Activity By Admin (username)" value="<?php echo $filter_admin_name;?>">
				              	</div>
				              	<div class="form-group col-md-3">
	                                <input class="column_filter form-control" id="username" name='username' type="text" placeholder="Activity By User (login ID)" value="<?php echo $filter_username;?>">
				              	</div>
				              	<div class="form-group col-md-3">
	                                <input class="column_filter form-control" id="keyword" name='keyword' type="text" placeholder="Keyword" value="<?php echo $filter_keyword;?>">
				              	</div>


				             	<div class="form-group col-md-3">
				               		<input class="btn btn-primary" type="submit" value="Filter">
				               		<a class="btn btn-default" href="<?php echo base_url().'admin/action_log/list';?>">Reset</a>
				             	</div>
				          	</div>
			          	</form> 
		          		<?php if(isset($add_action) && !empty($add_action)){ ?>
                  			<a href="<?php echo $add_action;?>" title="" data-toggle="tooltip" data-original-title="Add Category" class="btn btn-primary pull-right"><i class="fa fa-plus"></i></a>
                		<?php } ?>  
                	</div> 

					<div class="box-body">
						<table <?php  if(!empty($records_results))
								{ ?>  <?php } ?> class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Activity Detail</th>
									<th>Activity By</th>
									<th>IP Address</th>
									<th>Action Date</th>
								</tr>
							</thead>
							<tbody>
	

								<?php 
								if(!empty($records_results))
								{	
									$i = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
									$table="action_log";
									$field = "action_id";

									foreach ($records_results as $row) { $i++; ?>
										<tr id="tr_<?php echo $row[$field]; ?>">
											<td><?php echo $i; ?></td>
											<td><?php if(!empty($row['description'])) echo $row['description'];?></td>
											<td><?php if(!empty($row['perform_by'])) echo $row['perform_by'];?></td>
											<td><?php if(!empty($row['ip_address'])) echo $row['ip_address'];?></td>
	                                        <!-- <td><?php// if(!empty($row['created'])) echo date('d/m/Y',strtotime($row['created'])); ?></td> -->
	                                        <td><?php if(!empty($row['created'])) echo convertGMTToLocalTimezone($row['created'],TRUE) ?></td>
										</tr>

									<?php }
								} else {
									echo "<tr><td colspan='6' align='center'> No Record Found</td></tr>";
								} ?>
							</tbody>
							<tfoot>						
								<tr>
									<?php if(!empty($pagination)) { ?>
										<td colspan="2" >Total Records - <?php echo $total_records;?></td>
										<td colspan="6" align="center">
											<div><?php echo $pagination; ?></div>
										</td>
									<?php }else{ ?>	
										<td colspan="2">Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
										<td colspan="6" align="center"></td>
									<?php } ?>			
								</tr>
							</tfoot>
						</table>
					</div>			
				</div>			
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
$(function () {
        $('#action_date').daterangepicker({
            timePicker:false,
            format: 'MM/DD/YYYY'
        });
    });     	
</script>