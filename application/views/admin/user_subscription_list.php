<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-block alert-success fade in">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
				<?php } ?>
				<div class="box"> 

                	<div class="box-header with-border">  
                  		<!-- <h3 class="box-title">Filter Here</h3>   -->
              			<div class="box-body row"> 
                			<form method="get" action="<?php echo $reset_action;;?>"> 
                        	 	<input type="hidden" name="user_id" value="<?php echo $this->input->get('user_id');?>">
                        	 	<?php if(!$this->input->get('user_id')){?>
									<div class="form-group col-md-3"> 
										<input class="column_filter form-control" oninput="this.value = this.value.replace(/[^A-Za-z0-9-'()&amp; ]/g,'');" name="username" data-column="1" id="col1_filter" type="text" placeholder="User Name" value="<?php echo $filter_username?>">
									</div>  
								<?php }?>
								<div class="form-group col-md-3">
									<input class="column_filter form-control" oninput="this.value = this.value.replace(/[^A-Za-z0-9-'()&amp; ]/g,'');" name="title" data-column="1" id="col1_filter" type="text" placeholder="Title" value="<?php echo $filter_title?>">
								</div>  
				             	<div class="form-group col-md-3">
				               		<input class="btn btn-primary" type="submit" value="Filter">
				               		<a class="btn btn-default" href="<?php echo $reset_action;;?>">Reset</a>
				             	</div>
				          
			          	</form> 
		          		<?php if(isset($add_action) && !empty($add_action)){ ?>
		          			<div class="form-group col-md-offset-0 col-md-3">
                  				<a href="<?php echo $add_action;?>" title="" data-toggle="tooltip" data-original-title="Add Subscription" class="btn btn-primary pull-right"><i class="fa fa-plus"></i></a>
              				</div>
                		<?php } ?>  
            			</div>
                	</div> 

					<div class="box-body">

						<table <?php  if(!empty($records_results))
								{	?>  <?php } ?> class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th> 
									<?php if(!$this->input->get('user_id')){?>
									<th>User Name</th> 
									<?php }?>
									<th>Transaction Id</th> 
									<th>Plan Title</th> 
									<th>Start Date</th> 
									<th>End Date</th>
									<th>Duration</th> 
									<th>Amount</th> 
									<th>Plan Orders</th>
									<th>Used Order</th> 
									<th>Remaining Order</th>  
								</tr>
							</thead>
							<tbody> 

								<?php 
								if(!empty($records_results))
								{	
									$i = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
									$table="subscriptions";
									$field = "id";

									foreach ($records_results as $row) { $i++; 
										?>
										<tr id="tr_<?php echo $row[$field]; ?>">
											<td><?php echo $i; ?></td>
											<?php if(!$this->input->get('user_id')){?>
											<td><?php echo $row['fullname']?></td>
											<?php }?>
											<td><?php echo $row['transaction_id']?></td>
											<td><?php echo $row['title']?></td>
											<td><?php if(!empty($row['start_date'])) echo convertGMTToLocalTimezone($row['start_date'].' '.$row['start_time']); ?></td>				
											<td><?php if(!empty($row['end_date'])) echo convertGMTToLocalTimezone($row['end_date'].' '.$row['end_time']); ?></td> 
											
											<td><?php echo $row['time_value'].' '.$row['time_type']?></td> 
											<td><?php echo $row['amount']?></td>
											<td><?php echo $row['plan_orders']?></td>
											<td><?php echo $row['total_used_order']?></td>
											<td><?php echo $row['total_remaining_order']?></td>
										
										</tr>

									<?php }
								} else {
									echo "<tr><td colspan='8' align='center'> No Record Found</td></tr>";
								} ?>
							</tbody>
							<tfoot>						
								<tr>
									<?php if(!empty($pagination)) { ?>
										<td colspan="2" >Total Records - <?php echo $total_records;?></td>
										<td colspan="9" align="center">
											<div><?php echo $pagination; ?></div>
										</td>
									<?php }else{ ?>	
										<td colspan="2">Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
										<td colspan="9" align="center"></td>
									<?php } ?>			
								</tr>
							</tfoot>
						</table>
					</div>			
				</div>			
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->
