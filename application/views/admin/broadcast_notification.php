<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-block alert-success fade in">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
				<?php } ?>
				<div class="box">
					<div class="box-header with-border">
                  		
                		<form method="get" action="<?php echo base_url().'admin/broadcast_notification/list';?>"  data-parsley-validate>

							<div class="box-body col-md-12">
			              		<div class="form-group col-md-3">
			                 		<input class="column_filter form-control" name="title" data-column="1" id="col1_filter" data-parsley-type="title" data-parsley-type-message="Please enter valid title." type="title" placeholder="Title" value="<?php echo $filter_title;?>">
			              		</div>  
		                    	<div class="form-group col-md-3">
			                 		<input class="column_filter form-control" name="content" data-column="1" id="col1_filter" data-parsley-type="content" data-parsley-type-message="Please enter valid content." type="content" placeholder="Content" value="<?php echo $filter_content;?>">
			              		</div>
			              		<div class="form-group col-md-3">
					               	<input class="btn btn-primary" type="submit" value="Filter">
					               	<a class="btn btn-default" href="<?php echo base_url().'admin/broadcast_notification/list';?>">Reset</a>
			              		</div>
                  				<?php if(isset($add_action) && !empty($add_action)){ ?>
                  					<a href="<?php echo $add_action;?>" title="" data-toggle="tooltip" data-original-title="Add Notification" class="btn btn-default pull-right"><i class="fa fa-plus"></i></a>
                				<?php } ?>
                			</div>
			          </form>
                	</div>

					<div class="box-body">
						<table <?php  if(!empty($records_results)){	?>  <?php } ?> class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>Sr.No.</th>									
									<th>Title</th>
									<th>Content</th> 
									<?php  if(isset($delete_action) && !empty($delete_action)){ ?>
										<th>Actions</th>
									<?php }  ?>	 
									
								</tr>
							</thead>
							<tbody>
	

								<?php 
								if(!empty($records_results))
								{	
									$i = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
									$table="broadcast_notification";
									$field = "notification_id";
									foreach ($records_results as $row) { $i++; 
										if(isset($row['status'])) {
	                                        if($row['status']=="Active") {
	                                            $status = "Active";
	                                            $class = "pointer badge bg-green";
	                                        } else {
	                                            $status = "Inactive";
	                                            $class = "pointer badge bg-red";
	                                        }
                                    	}?>
										<tr id="tr_<?php echo $row[$field]; ?>">
											<td><?php echo $i; ?></td>
											
											
											<td><?php if(!empty($row['title'])) echo $row['title']; ?></td>
											<td><?php if(!empty($row['content'])) echo $row['content']; ?></td>
											
											
											<?php  if(isset($delete_action) && !empty($delete_action)){ ?>
											<td> 	
												<a  href="javascript:void(0)" class="btn btn-xs btn-danger"  
												onclick="soft_delete_record('<?php echo $row['notification_id'];?>','broadcast_notification','notification_id')" data-toggle="tooltip" data-original-title="Delete">
													<i class="fa fa-trash-o"></i>
												</a>&nbsp;&nbsp;
											</td>
											<?php }  ?>	 
										
										</tr> 

									<?php }
								} else {
									echo "<tr><td colspan='7' align='center'> No Record Found</td></tr>";
								} ?>
							</tbody>
							<tfoot>						
								<tr>
									<?php if(!empty($pagination)) { ?>
										<td colspan="2">Total Records - <?php echo $total_records;?></td>
										<td colspan="4">
											<div><?php echo $pagination; ?></div>
										</td>
									<?php }else{ ?>	
										<td colspan="2">Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
										<td colspan="4"></td>
									<?php } ?>			
								</tr>
							</tfoot>	
						</table>
					</div>			
				</div>			
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
function delete_report_page_list(field,id,table)
{ 
    if(id) {
         var a = confirm("Are you sure to delete this record?");
		if(a) {
	         $.ajax({
            type:'POST',
            data:{ 
                id:id,
                table_name:table,
                field:field 
            },
            url: base_url+"admin/ajax/delete_record/",
            success:function(data)
            {
             location.reload(); 
            }
        });
		}else{
		  return false;
		}

       
    }
}
</script>