<!-- Content Wrapper. Contains page content  -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
    <h1> <?php echo $page_title;?> </h1>
    <ol class="breadcrumb">
      <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
      <li class="<?php echo $breadcrumb['class'];?>"> 
        <?php if(!empty($breadcrumb['link'])) { ?>
        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
        <?php } else {
          echo $breadcrumb['icon'].$breadcrumb['title'];
        } ?>
      </li>
      <?php }?>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
     <div class="col-xs-12"> 
       <!-- flash messages-->
       <?php if ($this->session->flashdata('error')) { ?>
       <div class="alert alert-block alert-danger fade in">
        <button data-dismiss="alert" class="close" type="button">×</button>
        <?php echo $this->session->flashdata('error') ?>
      </div>
      <?php } ?>
      <?php if ($this->session->flashdata('success')) { ?>
      <div class="alert alert-block alert-success fade in">
        <button data-dismiss="alert" class="close" type="button">×</button>
        <?php echo $this->session->flashdata('success') ?>
      </div>
      <?php } ?>
      <div class="box">
        <div class="box-header with-border">
          <!-- <h3 class="box-title">Filter Here</h3>  -->
          <form method="get" action="<?php echo base_url().'admin/pages/list';?>"  data-parsley-validate>
            <div class="box-body row">
              <div class="form-group col-md-3">
               <input class="column_filter form-control" oninput="this.value = this.value.replace(/[^A-Za-z0-9-'()& ]/g,'');"  name="title" data-column="1" id="col1_filter" type="text" placeholder="Title" value="<?php echo $filter_title;?>">
             </div>
             <div class="form-group col-md-3">
              <input class="btn btn-primary" type="submit" value="Filter">
              <a class="btn btn-default" href="<?php echo base_url().'admin/pages/list';?>">Reset</a>

            </div>
          </div>
          <?php if(isset($add_action) && !empty($add_action)){ ?>
          <a href="<?php echo $add_action;?>" title="" data-toggle="tooltip" data-original-title="Add User" class="btn btn-default pull-right"><i class="fa fa-plus"></i></a>
          <?php } ?>
        </form>
      </div>
      <div class="box-body">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>No.</th>
              <th>Title</th>
              <?php if(isset($edit_action) && !empty($edit_action)){ ?>
              <!-- <th>Status</th>  -->
              <th>Actions</th>
              <?php } ?>

            </tr>
          </thead>
          <tbody>


            <?php 
            if(!empty($records_results))
            {   
              $i = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
              $table="pages";
              $field = "page_id";
              foreach ($records_results as $row) { $i++; 
                if(isset($row['status'])) {
                  if($row['status']=="Active") {
                    $status = "Active";
                    $class = "pointer badge bg-green";
                  } else {
                    $status = "Inactive";
                    $class = "pointer badge bg-red";
                  }
                }?>
                <tr id="tr_<?php echo $row[$field]; ?>">
                  <td><?php echo $i; ?></td>

                  <td><?php if(!empty($row['title'])) echo $row['title']; ?></td>


                  <?php if(isset($edit_action) && !empty($edit_action)){ ?>
                  <td class="td-actions">
                    <a id="edit_product" href="<?php echo $edit_action.'/'.$row['page_id']; ?>" class="btn btn-xs btn-primary edit_product" title="" data-toggle="tooltip" data-original-title="Edit">

                      <i class="fa fa-pencil"></i>

                    </a>

                  </td>
                  <?php } ?>
                </tr>

                  <?php }
              } else {
                echo "<tr><td colspan='7' align='center'> No Record Found</td></tr>";
              } ?>
            </tbody>

            <tfoot>                     
              <tr>
                <?php if(!empty($pagination)) { ?>
                <td colspan="2"  >Total Records - <?php echo $total_records;?></td>
                <td colspan="7" align="center">
                  <div><?php echo $pagination; ?></div>
                </td>
                <?php }else{ ?> 
                <td colspan="2" >Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
                <td colspan="7" align="center"></td>
                <?php } ?>          
              </tr>
            </tfoot> 
          </table>   
        </div>          
      </div>          
    </div>
</div>
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

