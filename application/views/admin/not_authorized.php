<div class="content-wrapper">
   <section class="content-header">
   </section>
    <section class="content center text-center">
        <h4 style="text-align: center;">You do not have permission to view this section.</h4>
        <a href="<?php if(isset($_SERVER['HTTP_REFERER'])) echo $_SERVER['HTTP_REFERER']?>" class="btn btn-primary">Back</a>
    </section>
</div>
<style>
.center {
  margin-top: 20%;
  width: 60%;
}
</style>