<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
     <section class="content-header">
        <h1> <?php echo $page_title;?> </h1>
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php }?>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row"> 
            <!-- left column -->
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header">
                        <!-- flash messages-->
                         <?php if ($this->session->flashdata('error')) { ?>
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('error') ?>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success')) { ?>
                            <div class="alert alert-block alert-success fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('success') ?>
                            </div>
                        <?php } ?>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <?php if(isset($from_action) && !empty($from_action)){ ?>
                    <form method="POST" id="update_advertisement_form" action="<?php echo $from_action; ?>" role="form"  data-parsley-validate>
                    <?php } ?>
                        <div class="box-body">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="title">Title *</label>
                                    <input type="text" class="form-control" name="title" id="title" value="<?php echo !set_value('title') ? $advertisement['title'] : set_value('title'); ?>" placeholder="Title" maxlength="150" data-parsley-required data-parsley-required-message="Please enter title.">
                                    <?php echo form_error('title');?>
                                </div>
                                <div class="form-group">
                                    <label for="title">Advertisement URL *</label>
                                    <input type="text" placeholder="Advertisement URL" name="advertisement_url"  id="advertisement_url" class="form-control " value="<?php echo !set_value('advertisement_url') ? $advertisement['advertisement_url'] : set_value('advertisement_url'); ?>"   data-parsley-required data-parsley-required-message="Please enter advertisement URL." data-parsley-type-message="Please enter valid advertisement URL.">
                                    <?php echo form_error('advertisement_url');?> 
                                </div>
                                 <div class="form-group">
                                    <label for="title">Expiry Date *</label>
                                    <div class="input-group">
                                          <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                          </div>
                                            <input type="text" placeholder="Expiry Date" autocomplete="off" name="expiry_date"  id="expiry_date" class="form-control " value="<?php echo !set_value('expiry_date') ? date('m/d/Y',strtotime($advertisement['expiry_date'])) : set_value('expiry_date'); ?>" data-date-format="mm/dd/yyyy"   oninput="this.value = this.value.replace(/[^]/g, '').replace(/(\..*)\./g, '$1');"  data-parsley-errors-container="#expiry_date_error" data-parsley-required data-parsley-required-message="Please select expiry date." onchange="error_blank()">
                                            <?php echo form_error('expiry_date');?> 
                                    </div>
                                </div>
                                <div id="expiry_date_error"></div>
                               
                            </div><!-- /.box-body -->
                       
                            <div class="box-footer text-right">
                                <?php if(isset($from_action) && !empty($from_action)){ ?>
                                <button type="submit" id="update" class="btn btn-primary" onclick="return form_submit('update_advertisement_form');">Update</button>
                                <?php } ?>
                                <a href="<?php echo $back_action;?>" class="btn btn-default">Back</a>
                            </div> 
                        </div><!-- /.box-body -->
                    </form>
                </div><!-- /.box -->
            </div><!-- col-12-->

            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-body">
                        <form class="form-horizontal">
                                <input type="hidden" id="record_id" name="record_id" value="<?php echo $advertisement['advertisement_id']; ?>" />
                                <input type="hidden" id="table" name="table" value="advertisement" />
                                <input type="hidden" id="upload_path" name="upload_path" value="<?php echo ADVERTISEMENT_PATH; ?>" />
                                <input type="hidden" id="select" name="select" value="image" />
                                <input type="hidden" id="where" name="where" value="advertisement_id" />
                                <input type="hidden" id="height" name="height" value="100" />
                                <input type="hidden" id="width" name="width" value="700" />
                                <div class="control-group col-md-6">
                                    <label class="control-label">Image *</label>
                                    <div class="controls">
                                        <div data-provides="fileupload" class="fileupload fileupload-new">
                                            <div  class="fileupload-new thumbnail">
                                                <img alt="No Image"src="<?php echo !empty($advertisement['image']) ? base_url().$advertisement['image']:'';?>" >
                                            </div>
                                            <div style="min-width: 180px; min-height: 100px; line-height: 5px;" class="fileupload-preview fileupload-exists thumbnail"></div>
                                            
                                            <div>
                                            <?php if(isset($from_action) && !empty($from_action)){ ?>
                                                <span class="btn btn-file"><span class="fileupload-new btn btn-default">Change image</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="file" name="image" id="image" class="default" accept="image/*" onchange="validate_images(this);"></span>
                                                <a data-dismiss="fileupload" class="btn fileupload-exists">Remove</a>
                                            <?php } ?>
                                            </div>
                                            <div id="fimage_error" class="error"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group"></div>
                                    <div class="form-group"></div>
                                    <div class="form-group"></div>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <button type="button" id="img_upload_button" class="btn btn-info pull-right common_update_image">Save</button>
                                        </div>
                                    </div>
                                    <div class="form-group" >
                                        <div class="col-sm-8">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" id="progressBar_image" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12" id="status_image"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-red text-center" id="error"></div>
                                <div class="col-sm-12 text-green text-center" id="success"></div>
                        </form>
                    </div><!-- panel body-->
                </div>
            </div><!-- col-12-->
        </div><!-- row-->
  </section>
</div><!-- row-->
<script type="text/javascript">
function validate_images(input) {
    $(input).parent().parent().parent().find('.error').html("");
    if (!input.files[0].name.toLowerCase().match(/\.(jpg|jpeg|png)$/)) {
        $(input).val("");
        $(input).parent().parent().parent().find('.error').html("Only jpg|jpeg|png image types allowed.");
        return false;
    }
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(input.files[0]);

        reader.onload = function (e) {
            var image = new Image();
            image.src = e.target.result; 
            image.onload = function () {
                //Determine the Height and Width.
                var height = this.height;
                var width = this.width;
              
                if(width<700 ||  width>1150 || height<50 || height>100)
                {
                    $(input).val("");
                    $(input).parent().parent().parent().find('.error').html("Image width  in between 700 to 1150 & height in between 50 to 100.");
                    $(input).parent().parent().parent().find('.fileupload-preview img').attr('src','');
                    return false;
                }
            }
        }
    }
}

$(function () {
    $('#expiry_date').datepicker({
          "startDate":"date('m/d/y')" 
    });
});
function error_blank()
{
    $("#expiry_date_error li").text('');
}
function form_submit(id)
{
    $("#"+id).parsley().validate();
    if($("#"+id).parsley().isValid()){
     //  $("#update").attr('disabled',true);
       $("#loader").show(); 
       return true;
    }else{
        return false;
    }
}
</script>