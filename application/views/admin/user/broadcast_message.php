<!-- Content Wrapper. Contains page content  -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $page_title;?> </h1>
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php }?>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-primary"> 
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- flash messages-->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('error') ?>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success')) { ?>
                            <div class="alert alert-block alert-success fade in">
                            <button data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo $this->session->flashdata('success') ?>
                        </div>
                        <?php } ?>
                 
                        <div class="panel">
                            <div class="">
                                <?php if(isset($form_action) && !empty($form_action)){ ?>
                                    <form method="POST" id="send_broadcast" action="<?php echo $form_action; ?>" role="form"  onsubmit="return form_submit('send_broadcast');" data-parsley-validate>

                                        <div class="">
                                            <div class="form-group col-md-6">
                                                <label for="">Subject *</label>
                                                <input type="hidden" name="url" value="<?php echo $_SESSION['url'];?>">
                                                <input type="text" class="form-control" maxlength="100" oninput="this.value = this.value.replace(/[^A-Za-z0-9-'()& ]/g,'');" name="subject" id="subject" data-parsley-maxlength="100" value="<?php if(set_value('subject')) echo set_value('subject');?>" placeholder="Subject" data-parsley-required data-parsley-required-message="Please enter subject.">
                                                <?php echo form_error('subject'); ?>
                                            </div>

                                            <div class="form-group col-md-12">
                                                <label for="message">Message *</label>
                                                <textarea class="form-control parsley-error ckeditor_required" id="" name="message"  placeholder="Message" rows="5" data-parsley-required data-parsley-required-message="Please enter message." data-parsley-errors-container="#msg_error"><?php if(set_value('message')) echo set_value('message');?> </textarea>
                                                <p class="error" id="msg_error"></p>
                                                <?php echo form_error('message');?> 
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12 text-right">
                                                        <input type="hidden" name="type" value="<?php echo $_GET['type'];?>">
                                                        <?php if(isset($form_action) && !empty($form_action)){ ?>
                                                            <button type="submit" id="reply" class="btn btn-primary" >Reply</button>
                                                        <?php } ?>
                                                        <a href="<?php echo $back_action;?>" class="btn btn-default">Back</a> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                <?php }else{ ?>
                                <a href="<?php echo $back_action;?>" class="btn btn-default">Back</a> 
                                <?php } ?>
                            </div><!-- panel body--> 
                        </div><!-- end panel --> 
                    </div><!-- col-6--> 
                </div><!-- row--> 
            </div><!-- /.box-body --> 
        </div><!-- /.box --> 
    </section><!-- /.content --> 
</div><!-- /.content-wrapper -->

<!-- <script src="<?php // echo base_url(); ?>assets/admin/js/ckeditor/ckeditor.js"></script>  -->
<script>
    /*$(function () {
        // Replace the <textarea id="ckeditor"> with a CKEditor
        CKEDITOR.replace('ckeditor');
        $("#reply").click(function(){
            $('.ckeditor_required').attr('required', '');
            for (var i in CKEDITOR.instances){
                CKEDITOR.instances[i].updateElement();
            }
        });
    });*/

    function form_submit(id)
    {
        $("#"+id).parsley().validate();
        if($("#"+id).parsley().isValid()){ 
           //$("#reply").attr('disabled',true);
           $("#loader").show(); 
            $("#"+id).submit();
           return true;
        }else{
            return false;
        }
    }

</script>