<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12 message_box">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-block alert-success fade in">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
				<?php } ?>
				<div class="box">
                   	<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="checkboxuser"><a href="<?php echo base_url().'admin/user/list';?>">District List</a></li>
							<li class="active checkboxuser"><a href="javascript:void(0);">Principal List</a></li>
							<li class="checkboxuser"><a href="<?php echo base_url().'admin/teacher/list';?>">Teacher List</a></li>
							<li class="checkboxuser"><a href="<?php echo base_url().'admin/parent/list';?>">Parent List</a></li>
						</ul>
					</div>

					<div class="box-header with-border">  
                		<form method="get" action="<?php echo $form_action; ?>"> 
						<div class="box-body row"> 
							 <div class="form-group col-md-3">
								<input class="column_filter form-control" id="first_name" name='first_name' type="text" placeholder="First Name" value="<?php echo $filter_first_name; ?>">
							</div>
							<div class="form-group col-md-3">
								<input class="column_filter form-control" id="last_name" name='last_name' type="text" placeholder="Last Name" value="<?php echo $filter_last_name; ?>">
							</div>
			              	<div class="form-group col-md-3">
                                <input class="column_filter form-control" id="email" name='email' type="text" placeholder="Email" value="<?php echo $filter_email; ?>">
			              	</div>
			              	<div class="form-group col-md-3">
                                <input class="column_filter form-control" id="mobile" name='phone' type="text" placeholder="Phone" value="<?php echo $filter_phone; ?>">
			              	</div> 
			              	<div class="form-group col-md-3">
                                <input class="column_filter form-control" id="school_name" name='school_name' type="text" placeholder="School" value="<?php echo $filter_school_name;?>">
			              	</div>
			              	<div class="form-group col-md-3">
                                <input class="column_filter form-control" id="district_name" name='district_name' type="text" placeholder="District" value="<?php echo $filter_district_name;?>">
			              	</div>
			              	<div class="form-group col-md-3">
				              	<select name="state" id="state" class="column_filter form-control">
				              		<option value="">State</option>
				              		<?php if(!empty($states)){
                                        foreach($states as $state){ ?>
                                        <option value="<?php echo $state['state_id'];?>" <?php if(!empty($filter_state) && $filter_state==$state['state_id']){ echo 'selected'; }?>><?php echo $state['state_name'];?></option>
                                    <?php } }?>
		                           
				              	</select> 
                        	</div> 
			              	<div class="form-group col-md-3">
				              	<select name="status" id="status" class="column_filter form-control">
				              		<option value="">Status</option>
				              		<option value="1" <?php if(!empty($filter_status)&& $filter_status=='1'){ echo 'selected'; }?>>Active</option>
				              		<option value="2" <?php if(!empty($filter_status)&& $filter_status=='2'){ echo 'selected'; }?>>Inactive</option>
				              		<option value="3" <?php if(!empty($filter_status)&& $filter_status=='3'){ echo 'selected'; }?>>Pending</option>
				              		
				              	</select> 
                        	</div> 
			             <div class="form-group col-md-3">
			               	<input class="btn btn-primary" type="submit" value="Filter">
			               	<a class="btn btn-default" href="<?php echo $form_action; ?>">Reset</a>
			 
			             </div>
			             
			          	</div>
			          </form> 
	                    <?php  if(!empty($records_results)){?>
	              			

                  			<?php if (isset($edit_action) && !empty($edit_action)) { ?>
	                  			<a style="float: right;" href="<?php echo base_url().'admin/district/broadcast_message?type=principal'; ?>" title="" data-toggle="tooltip" data-original-title="Broadcast Message" class="btn btn-primary" id='broadcast'>Broadcast Message</a>	
	                  		<?php } ?>
	                  		<?php if (isset($delete_action) && !empty($delete_action)) { ?>
	                  			  <a href="javascript:void(0)" class="btn  btn-warning deleteall"  data-toggle="tooltip" data-original-title="Delete" style="float: right;margin-right: 5px;" id="delete">Delete</a>
                  			<?php } ?>
	            		<?php } ?>
                	</div> 

					<div class="box-body">

						<table class="table table-bordered table-hover" id="recordsTable">
							<thead>
								<tr>
									<th><input type="checkbox" id="check_all" value=""></th>
									<th>Display Name</th>
									<th>First Name</th>
									<th>Last Name</th>
									<th>Email</th>
									<th>Phone</th>
									<th>State</th>
									<th>District</th>
									<th>School</th>
									<?php if(isset($edit_action) && !empty($edit_action)){ ?>
									<th>Profile Status</th>
									<?php } ?>
									<?php if(isset($edit_action) && !empty($edit_action)){ ?>
                                    	<th>Actions</th>
                                    <?php } ?>                                    
								</tr>
							</thead>
							<tbody>
	

								<?php 
								if(!empty($records_results))
								{	
									// echo "<pre>";print_r($records_results);die;
									$i = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
									$table="users";
									$field = "user_id";

									foreach ($records_results as $row) { $i++;
                                    $class=''; 
									$status= 'Not Approved';
									if(isset($row['status'])) {
	                                        if($row['status']=="1") {
	                                            $status = "Active";
	                                            $class = "pointer badge bg-green";
	                                        }elseif($row['status']=="3") {
	                                            $status = "Pending";
	                                            $class = "badge bg-yellow";
	                                        } else {
	                                            $status = "Inactive";
	                                            $class = "pointer badge bg-red";
	                                        }
                                    	}

                                        if($row['is_added'] == 1) {
                                            $checked = 'checked';
                                        } else {
                                            $checked = '';
                                        } ?>
										<tr id="tr_<?php echo $row[$field]; ?>">
											<td><input class="select_all" <?php echo $checked; ?> type="checkbox" onchange="change_status(this);" id="<?php echo $row[$field]; ?>"></td> 

											<td><?php echo !empty($row['display_name'])?ucfirst($row['display_name']):'N/A';?></td>
											<td><?php if(!empty($row['first_name'])) echo ucfirst($row['first_name']);?></td>
											<td><?php if(!empty($row['last_name'])) echo ucfirst($row['last_name']);?></td>
											<td><?php if(!empty($row['email'])) echo ucfirst($row['email']);?></td>
											<td><?php if(!empty($row['phone'])) echo ucfirst($row['phone']);?></td>
											<td><?php if(!empty($row['state_name'])) echo ucfirst($row['state_name']);?></td>
											<td><?php if(!empty($row['district_name'])) echo ucfirst($row['district_name']);?></td>
											<td><?php if(!empty($row['school_name'])) echo ucfirst($row['school_name']);?></td>
											<?php if(isset($edit_action) && !empty($edit_action)){ ?>
												<td>
													<?php if ($row['email_verified']==2) { ?>
	                                                	<p id="" class="badge bg-yellow" title="" data-toggle="tooltip" data-original-title="Pending">Pending</p>
													<?php } else { ?>
														<?php if ($row['status']==3) { ?>
		                                                	<p id="status_<?php echo $row[$field]; ?>" class="<?php echo $class; ?>" title="" data-toggle="tooltip" data-original-title="<?php echo $status; ?>"><?php echo $status; ?></p>
														<?php } else { ?>
		                                                	<p id="status_<?php echo $row[$field]; ?>" onclick="change_user_status('<?php echo $field; ?>','<?php echo $row[$field]; ?>','<?php echo $table; ?>')" class="<?php echo $class; ?>" title="" data-toggle="tooltip" data-original-title="Change Status"><?php echo $status; ?></p>
														<?php } ?>
													<?php } ?>													
												</td>
											<?php } ?>
											
						                  	<?php if(isset($edit_action) && !empty($edit_action)){ ?>
											<td class="td-actions">
												<div class="btn-group">
														<a id="" href="<?php echo $edit_action.'/'.$row[$field]; ?>" class="btn btn-xs btn-primary" title="" data-toggle="tooltip" data-original-title="Edit">
															<i class="fa fa-pencil"></i>
														</a>
								                </div>
											</td>
											<?php } ?>
										</tr>

									<?php }
								} else {
									echo "<tr><td colspan='9' align='center'> No Record Found</td></tr>";
								} ?>
							</tbody>
						</table>
							<tfoot>						
								<tr>
									<?php if(!empty($pagination)) { ?>
										<td >Total Records - <?php echo $total_records;?></td>
										<td colspan="7" align="center">
											<div><?php echo $pagination; ?></div>
										</td>
									<?php }else{ ?>	
										<td align="center">Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
										<td colspan="7" align="center"></td>
									<?php } ?>			
								</tr>
							</tfoot>	
					</div>			
				</div>			
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php unset($_SESSION['brdCstdata']);?>
<script src="assets/front/js/jquerymask.min.js"></script>

<script type="text/javascript">
 	$('#mobile').mask('000-000-0000');
	function change_status(value)
	{
	    // $("#loader").show();
	    var districtId = value.id;
        var url = 'admin/'+'<?php echo $this->uri->segment(2).'/'.$this->uri->segment(3); ?>';
	    if($(value).prop('checked') == true){
	        //do something
	        // alert(id);
	        $.ajax({
	            type: 'POST',
	            data: {"district_id":districtId,'type':'add','url':url},
	            url: 'admin/district/check_broadcast',
	            success: function(data) {
	                var jsonA = JSON.parse(data);
					return false;                    
	            }
	        });
	    }
	    else{
	        $.ajax({
	            type: 'POST',
	            data: {"district_id":districtId,'type':'delete','url':url},
	            url: 'admin/district/check_broadcast',
	            success: function(data) {
				    
	            }
	        });
	    }
	}	

$('#check_all').click(function () {

	var checked = $(this).prop('checked');

	if (!checked) {
		$(".select_all").each(function() {
			$(this).trigger('click');
		});    	
	} else {
		$(".select_all").each(function() {
			if (!$(this).is(":checked") && checked) {
				$(this).trigger('click');
			}    	
		});    	
	}


});	
$('.checkboxuser').on('click',function(){
	  $.ajax({
	            type: 'POST',
	            data: {"district_id":districtId,'type':'delete'},
	           data: {"district_id":districtId,'type':'delete','url':url},
	            success: function(data) {
				   $(".select_all").attr("checked", false);
	            }
	        });
})

//Delet all
$('.deleteall').on("click", function(event){
     var post_arr = [];

    $('#recordsTable input[type=checkbox]').each(function() {

      if ($(this).is(":checked")) {
        var id = this.id;
       	if(id!='check_all'){
    		post_arr.push(id);
    	}
      }
    });

    if(post_arr.length > 0){

        var isDelete = confirm("Are you sure you want to delete principal's record ?");
        if (isDelete == true) {
            $("#delete").off("click").attr('disabled',true);
            $("#broadcast").attr('href','javascript:void(0)').attr('disabled',true);
            $.ajax({
              url: '<?php echo base_url("admin/deleteall");?>',
              type: 'POST',
              data: { post_id: post_arr,user_type:2},
		        success: function(resp){
		        	 data = JSON.parse(resp);
		          	 $("#loader").hide();  
			         $(".message_box").show(); 
			         $("html, body").animate({ scrollTop: 0 }, "slow");
			         $(".message_box").prepend(data.message);
		            
		             if(data.status==0){
			            setTimeout(function() {
			                $(".message_box").prepend('');
			                $(".message_box").hide();
			            }, 5000); 
		            }
		            else{
		            
		                setTimeout(function() {
		                 $(".message_box").prepend('');
		                 $(".message_box").hide();
		                  location.reload();
		                }, 2000); 
		            }
		        },
		        error: function (jqXHR, textStatus, errorThrown) {
		        	$("#delete").attr('disabled',false);
                    $("#broadcast").attr('href',"<?php echo base_url().'admin/district/broadcast_message?type=principal'; ?>").attr('disabled',false);
                  if (jqXHR.status == 500) {
                      alert('Internal error: ' + jqXHR.responseText);
                  } else {
                      alert('Unexpected error.');
                  }
                }
            });
        } 
    } else{
    	alert("Please select principal's for delete.");
    }
});
</script>
</script>