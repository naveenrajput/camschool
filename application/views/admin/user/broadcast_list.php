<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12 message_box">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-block alert-success fade in">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
				<?php } ?>
				<div class="box">

					<div class="box-header with-border">  
                		<form method="get" action="<?php echo $form_action; ?>"> 
						<div class="box-body row"> 
							<div class="form-group col-md-3">
								<input class="column_filter form-control" id="subject" name='subject' type="text" placeholder="Subject" value="<?php echo $filter_subject;?>">
							</div>
							<div class="form-group col-md-3">
								<input class="column_filter form-control" id="user_name" name='user_name' type="text" placeholder="User name" value="<?php echo $filter_user_name;?>">
							</div>
							
                            <div class="form-group col-md-3">
								<select class="column_filter form-control" id="user_type" name='user_type' type="text" placeholder="User type" value="">
								<option value="">Select type</option>	
								<option  <?php if($filter_user_type=='Principal'){echo 'Principal';}?> value="Principal">Principal</option>	
								<option <?php if($filter_user_type=='District'){echo 'selected';}?> value="District">District</option>	
								<option  <?php if($filter_user_type=='Teacher'){echo 'selected';}?> value="Teacher">Teacher</option>	
								<option  <?php if($filter_user_type=='Parent'){echo 'selected';}?> value="Parent">Parent</option>	
								</select>
							</div>

							<div class="form-group col-md-3">
								<input class="btn btn-primary" type="submit" value="Filter">
								<a class="btn btn-default" href="<?php echo $form_action; ?>">Reset</a>
							</div>
			             
			          	</div>
			          </form> 

                	</div>

					<div class="box-body">

						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Subject</th>
									<th>User Name</th>
									<th>User Type</th>
								
									<th>Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php 
								if(!empty($records_results))
								{	
									// echo "<pre>";print_r($records_results);die;
									$i = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
									$table="broadcast_notification";
									$field = "notification_id";
                                   
									foreach ($records_results as $row) { $i++; 
										   $message = strip_tags($row['message']);
										   if(strlen($message) > 70){
										      $string_desc = substr(ucfirst($message),0,70).'...';
										   }else{ 
										      $string_desc = $message;
										  	} 
										?>
										<tr id="tr_<?php echo $row[$field]; ?>"><tr id="tr_<?php echo $row[$field]; ?>">
											<td><?php echo $i; ?></td> 

											<td><?php if(!empty($row['subject'])) echo ucfirst($row['subject']);?></td>
											<!-- <td><?php //echo $string_desc ;?></td> -->
											<td> <?php echo $this->db->get_where('admin',array('admin_id'=>$row['sent_by']))->row()->username; ?></td>
											<td><?php echo ucfirst($row['user_type']); ?></td>

											<td><?php echo ucfirst(convertGMTToLocalTimezone($row['created']));?></td>
											<td class="td-actions">
												<div class="btn-group">
													<a id="" href="<?php echo base_url('admin/broadcast_user/list').'/'.$row[$field]; ?>" class="btn btn-xs btn-primary" title="" data-toggle="tooltip" data-original-title="Broadcast User List">
														<i class="fa fa-eye"></i>
													</a>
								                </div>
											</td>
										</tr>

									<?php }
								} else {
									echo "<tr><td colspan='4' align='center'> No Record Found</td></tr>";
								} ?>
							</tbody>
						</table>
							<tfoot>						
								<tr>
									<?php if(!empty($pagination)) { ?>
										<td >Total Records - <?php echo $total_records;?></td>
										<td colspan="7" align="center">
											<div><?php echo $pagination; ?></div>
										</td>
									<?php }else{ ?>	
										<td align="center">Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
										<td colspan="7" align="center"></td>
									<?php } ?>			
								</tr>
							</tfoot>	
					</div>			
				</div>			
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

