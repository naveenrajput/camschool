<div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $page_title;?> </h1>
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php } ?>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-primary"> 
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">  
                    <div class="col-lg-12">
                        <!-- flash messages-->
                        <div class="message_box" style="display:none;"></div>
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('error') ?>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success')) { ?>
                            <div class="alert alert-block alert-success fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('success') ?>
                            </div>
                        <?php } ?>
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <?php if(isset($form_action) && !empty($form_action)){ ?>
                                    <form id="request_type_form" class="" method="POST" action="" enctype="multipart/form-data" role="form"  data-parsley-validate>
                                <?php } ?>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label for="display_name">Display Name </label>
                                            <input type="text" class="form-control" maxlength="60" oninput="this.value = this.value.replace(/[^A-Za-z0-9-'()& ]/g,'');" name="display_name" id="display_name" data-parsley-maxlength="60" value="<?php echo (isset($details['display_name']) && !empty($details['display_name']))?$details['display_name']:''; ?>" placeholder="Display Name" data-parsley-required-message="Please enter display name.">
                                            <?php echo form_error('display_name'); ?>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="title">First Name *</label>
                                            <input type="text" class="form-control" maxlength="30" oninput="this.value = this.value.replace(/[^A-Za-z0-9-'()& ]/g,'');" name="first_name" id="first_name" data-parsley-maxlength="30" value="<?php echo (isset($details['first_name']) && !empty($details['first_name']))?$details['first_name']:''; ?>" placeholder="First Name" data-parsley-required data-parsley-required-message="Please enter first name.">
                                            <?php echo form_error('first_name'); ?>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="title">Last Name *</label>
                                            <input type="text" class="form-control" maxlength="30" oninput="this.value = this.value.replace(/[^A-Za-z0-9-'()& ]/g,'');" name="last_name" id="last_name" data-parsley-maxlength="30" value="<?php echo (isset($details['last_name']) && !empty($details['last_name']))?$details['last_name']:''; ?>" placeholder="Last Name" data-parsley-required data-parsley-required-message="Please enter last name.">
                                            <?php echo form_error('last_name'); ?>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label for="title">Email *</label><!-- oninput="email_check()" -->
                                            <input type="email" class="form-control" maxlength="100" id="email" name="email" data-parsley-required placeholder="Email"  value="<?php echo (isset($details['email']) && !empty($details['email']))?$details['email']:''; ?>" data-parsley-maxlength="100" data-parsley-required data-parsley-required-message="Please enter email.">
                                            <p class="error" id="access_email_error" style="display:none;">Email already used.</p>
                                            <?php echo form_error('email'); ?>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="phone">Mobile Number *</label><!-- oninput="mobile_check()" -->
                                            <input type="text" maxlength="14" class="form-control" id="mobile" name="phone"  placeholder="Mobile Number" value="<?php echo isset($details['phone'])?$details['phone']:''; ?>" data-parsley-required-message="Please enter mobile number." data-parsley-minlength="7" data-parsley-minlength-message="Mobile number must be at least 7 digits long." data-parsley-maxlength-message="Mobile number must be at most 14 digits long." data-parsley-maxlength="14" data-parsley-type-message="Please enter valid mobile number">
                                            <p class="error" id="access_mobile_error" style="display:none;">Mobile number already used.</p>
                                            <?php echo form_error('phone'); ?>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="school_name">School</label>
                                            <input type="text" disabled=""  class="form-control" placeholder="School" value="<?php echo isset($details['school_name'])?$details['school_name']:''; ?>" >
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label for="state_name">State</label>
                                            <input type="text" disabled=""  class="form-control" placeholder="State" value="<?php echo isset($details['state_name'])?$details['state_name']:''; ?>" >
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="district_name">District </label>
                                            <input type="text" disabled=""  class="form-control" placeholder="District" value="<?php echo isset($details['district_name'])?$details['district_name']:''; ?>" >
                                        </div>
                                    </div>


                                </div>
                                <div class="box-footer text-center">
                                    <?php if(isset($form_action) && !empty($form_action)){ ?>
                                        <a href="javascript:void(0)" class="btn btn-primary" id="submit_form" onclick="form_submit('request_type_form');">Update</a>
                                    <?php } ?>
                                    <a href="<?php echo $back_action;?>" class="btn btn-default">Back</a> 
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box --> 
            </div><!-- col-12--> 
        </div><!-- row--> 
    </section>
</div>
<!-- row--> 
<!-- Include Bootstrap Datepicker -->


<script src="assets/front/js/jquerymask.min.js"></script>


<script>
$('#mobile').mask('000-000-0000');
 
var is_mobile_valid=1;
function mobile_check(){
    error_msg_mobile="";
    $("#access_mobile_error").hide();
    var matched_value = $("#mobile").val().trim();
    var matched_column = 'phone';
    var table="users";
    var id = <?php echo $details['user_id'] ?>;
    var matched_id="user_id";
    if(matched_value!='') {
        $.ajax({
            type:'POST',
            url: "<?php echo base_url(); ?>admin/ajax/check_unique/",
            data: {matched_value:matched_value,matched_column:matched_column,table:table,id:id,matched_id:matched_id},
            
            success:function(data)
            {
                if(data==1) {
                    is_mobile_valid=0;
                    error_msg_mobile = "Mobile number already used." ;
                    $("#access_mobile_error").show();
                } else {
                    $("#access_mobile_error").hide();
                    is_mobile_valid=1;
                }
                
            }
        });
    } else {
        is_mobile_valid=1;
        error_msg_mobile = "" ;
        $("#access_mobile_error").hide();
    }
}

var is_email_valid=1;
function email_check(){
    error_msg_email="";
    $("#access_email_error").hide();
    var matched_value = $("#email").val().trim();
    var matched_column = 'email';
    var table="users";
    var id = <?php echo $details['user_id'] ?>;
    var matched_id="user_id";
    if(matched_value!='') {
        $.ajax({
            type:'POST',
            url: "<?php echo base_url(); ?>admin/ajax/check_unique/",
            data: {matched_value:matched_value,matched_column:matched_column,table:table,id:id,matched_id:matched_id},
            
            success:function(data)
            {
                if(data==1) {
                    is_email_valid=0;
                    error_msg_email = "Email already used." ;
                    $("#access_email_error").show();
                } else {
                    $("#access_email_error").hide();
                    is_email_valid=1;
                }
                
            }
        });
    } else {
        is_email_valid=1;
        error_msg_email = "" ;
        $("#access_email_error").hide();
    }
}




function form_submit(id)
{
    if (is_mobile_valid && is_email_valid) {
       submitDetailsForm(id);
    }
}
</script>


