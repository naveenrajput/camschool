<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12 message_box">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-block alert-success fade in">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
				<?php } ?>
				<div class="box">
                   	<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class=""><a href="<?php echo base_url().'admin/user/list';?>">District List</a></li>
							<li class=""><a href="<?php echo base_url().'admin/principal/list';?>">Principal List</a></li>
							<li class=""><a href="<?php echo base_url().'admin/teacher/list';?>">Teacher List</a></li>
							<li class="active"><a href="javascript:void(0);">Parent List</a></li>
						</ul>
					</div>

					<div class="box-header with-border">  
                		<form method="get" action="<?php echo $form_action; ?>"> 
						<div class="box-body row"> 
							<!-- <div class="form-group col-md-3">
								<input class="column_filter form-control" id="student_name" name='student_name' type="text" placeholder="Student Name" value="<?php /*echo $filter_student_name;*/ ?>">
							</div>
							<div class="form-group col-md-3">
								<input class="column_filter form-control" id="first_name" name='first_name' type="text" placeholder="First Name" value="<?php /*echo $filter_first_name;*/ ?>">
							</div>
							<div class="form-group col-md-3">
								<input class="column_filter form-control" id="last_name" name='last_name' type="text" placeholder="Last Name" value="<?php /*echo $filter_last_name;*/ ?>">
							</div>
			              	<div class="form-group col-md-3">
                                <input class="column_filter form-control" id="email" name='email' type="text" placeholder="Email" value="<?php /*echo $filter_email;*/ ?>">
			              	</div> -->
			              	<div class="form-group col-md-3">
                                <input class="column_filter form-control" id="school_name" name='school_name' type="text" placeholder="School" value="<?php echo $filter_school_name;?>">
			              	</div>
			              	<div class="form-group col-md-3">
                                <input class="column_filter form-control" id="district_name" name='district_name' type="text" placeholder="District" value="<?php echo $filter_district_name;?>">
			              	</div>
			              	<div class="form-group col-md-3">
				              	<select name="state" id="state" class="column_filter form-control">
				              		<option value="">State</option>
				              		<?php if(!empty($states)){
                                        foreach($states as $state){ ?>
                                        <option value="<?php echo $state['state_id'];?>" <?php if(!empty($filter_state) && $filter_state==$state['state_id']){ echo 'selected'; }?>><?php echo $state['state_name'];?></option>
                                    <?php } }?>
		                           
				              	</select> 
                        	</div> 
			              	<div class="form-group col-md-3">
				              	<select name="status" id="status" class="column_filter form-control">
				              		<option value="">Status</option>
				              		<option value="1" <?php if(!empty($filter_status)&& $filter_status=='1'){ echo 'selected'; }?>>Active</option>
				              		<option value="2" <?php if(!empty($filter_status)&& $filter_status=='2'){ echo 'selected'; }?>>Inactive</option>
				              	</select> 
                        	</div> 
							<div class="form-group col-md-3">
								<input class="btn btn-primary" type="submit" value="Filter">
								<a class="btn btn-default" href="<?php echo $form_action; ?>">Reset</a>
							</div>
			             
			          	</div>
			          </form> 
	                    <?php /*  if(!empty($records_results)){?>
	              			<a href="<?php echo base_url().'admin/student/export?student_name='.$filter_student_name.first_name='.$filter_first_name.'&last_name='.$filter_last_name.'&email='.$filter_email.'&school_name='.$filter_school_name.'&district_name='.$filter_district_name.'&state='.$filter_state.'&status='.$filter_status;?>" title="" data-toggle="tooltip" data-original-title="Export Excel" class="btn btn-primary"><div id="" class="" value="Export ">Export to Excel</div></a>
	            		<?php } */ ?>
                	</div>

					<div class="box-body">

						<table class="table table-bordered table-hover" id="recordsTable">
							<thead>
								<tr>
									<th>#</th>
									<th>Student Name</th>
									<th>First Name</th>
									<th>Last Name</th>
									<th>Email</th>
									<th>State</th>
									<th>District</th>
									<th>School</th>
									<?php if(isset($edit_action) && !empty($edit_action)){ ?>
									<th>Profile Status</th>
									<?php } ?>
								</tr>
							</thead>
							<tbody>
	

								<?php 
								if(!empty($records_results))
								{	
									// echo "<pre>";print_r($records_results);die;
									$i = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
									$table="student";
									$field = "id";

									foreach ($records_results as $row) { $i++; 
										$class=''; 
										$profile_status= 'Not Approved';
										if(isset($row['email_verified'])) {
	                                        if($row['email_verified']=="1") {
	                                            $status = "Active";
	                                            $class = "pointer badge bg-green";
	                                        }elseif($row['email_verified']=="2") {
	                                            $status = "Pending";
	                                            $class = "badge bg-yellow";
	                                        } else {
	                                            $status = "Inactive";
	                                            $class = "pointer badge bg-red";
	                                        }
                                    	} ?>
										<tr id="tr_<?php echo $row[$field]; ?>">
											<td><?php echo $i; ?></td> 

											<td><?php if(!empty($row['student_name'])) echo ucfirst($row['student_name']);?></td>
											<td><?php if(!empty($row['first_name'])) echo ucfirst($row['first_name']);?></td>
											<td><?php if(!empty($row['last_name'])) echo ucfirst($row['last_name']);?></td>
											<td><?php if(!empty($row['email'])) echo ucfirst($row['email']);?></td>
											<td><?php if(!empty($row['state_name'])) echo ucfirst($row['state_name']);?></td>
											<td><?php if(!empty($row['district_name'])) echo ucfirst($row['district_name']);?></td>
											<td><?php if(!empty($row['school_name'])) echo ucfirst($row['school_name']);?></td>
											
											<?php if(isset($edit_action) && !empty($edit_action)){ ?>
												<td>
													<!-- <?php if ($row['status']==3) { ?>
	                                                	<p id="status_<?php echo $row[$field]; ?>" class="<?php echo $class; ?>" title="" data-toggle="tooltip" data-original-title="<?php echo $status; ?>"><?php echo $status; ?></p>
													<?php } else { ?>
	                                                	<p id="status_<?php echo $row[$field]; ?>" onclick="change_user_status('<?php echo $field; ?>','<?php echo $row[$field]; ?>','<?php echo $table; ?>')" class="<?php echo $class; ?>" title="" data-toggle="tooltip" data-original-title="Change Status"><?php echo $status; ?></p>
													<?php } ?> -->
													<?php if ($row['email_verified']==1) { ?>
	                                                	<p id="status_<?php echo $row[$field]; ?>" class="<?php if(isset($class)){echo $class; }?>" title="" data-toggle="tooltip" data-original-title="<?php echo $status; ?>"><?php  if(isset($status)){ echo $status;} ?></p>
													<?php } else { ?>
	                                                	<p id="status_<?php echo $row[$field]; ?>"  class="<?php  if(isset($class)){ echo $class; } ?>" title="" data-toggle="tooltip" data-original-title="Change Status"><?php  if(isset($status)){ echo $status;} ?>/p>
													<?php } ?>
												</td>
											<?php } ?>
										</tr>

									<?php }
								} else {
									echo "<tr><td colspan='9' align='center'> No Record Found</td></tr>";
								} ?>
							</tbody>
						</table>
							<tfoot>						
								<tr>
									<?php if(!empty($pagination)) { ?>
										<td >Total Records - <?php echo $total_records;?></td>
										<td colspan="7" align="center">
											<div><?php echo $pagination; ?></div>
										</td>
									<?php }else{ ?>	
										<td align="center">Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
										<td colspan="7" align="center"></td>
									<?php } ?>			
								</tr>
							</tfoot>	
					</div>			
				</div>			
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

