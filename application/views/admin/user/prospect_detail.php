<div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $page_title;?> </h1>
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php }?>
        </ol>
    </section>

     
    <section class="content">
        <div class="message_box" style="display:none;"></div>
        <?php if ($this->session->flashdata('error')) { ?>
            <div class="alert alert-block alert-danger fade in">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?php echo $this->session->flashdata('error') ?>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="alert alert-block alert-success fade in">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?php echo $this->session->flashdata('success') ?>
            </div>
        <?php } ?>
         <form id="prospect_form" class="" method="POST" action="" enctype="multipart/form-data" role="form"  data-parsley-validate>
            <div class="invoice">
               <div class="row">
                  <div class="col-xs-12">

                    <h2 class="page-header">
                        <i class="fa fa-globe"></i> <?php if(!empty($details['first_name']))echo Ucfirst($details['first_name'])." ".$details['last_name']; ?>
                        <small class="pull-right">
                            Reg Date: <?php if(!empty($details['created']))echo date('m/d/Y',strtotime($details['created'])); ?>
                        </small>
                    </h2>
                  </div>
               </div>
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                        <label>State</label>
                        <p><?php echo !empty($details['state_name'])?$details['state_name']:'NA'; ?></p>
                        <br>
                        <label>Phone Number </label>
                        <p><?php echo !empty($details['phone'])?$details['phone']:'NA'; ?></p>
                    </div>
                     <div class="col-sm-4 invoice-col">
                        <label>District </label>
                        <p><?php  if(!empty($details['district_name'])){echo $details['district_name'];}else{echo  'NA';} ?></p>
                    </div>
                    <div class="col-sm-4 invoice-col"> 
                        <label>Email </label>
                        <p><?php echo !empty($details['email'])?$details['email']:'NA'; ?></p>
                    </div>   
                    <div class="col-sm-4 invoice-col"> 
                        <label>Status</label>
                        <select name="prospect_status" id="prospect_status" class="column_filter form-control" data-parsley-required data-parsley-required-message="Please select status.">
                            <option value="1" <?php echo ($details['prospect_status']==1)?'selected':''; ?>>Pending</option>
                            <option value="2" <?php echo ($details['prospect_status']==2)?'selected':''; ?>>Sold</option>
                            <option value="3" <?php echo ($details['prospect_status']==3)?'selected':''; ?>>Lost</option>
                            <option value="4" <?php echo ($details['prospect_status']==4)?'selected':''; ?>>Requested Info</option>
                            <option value="5" <?php echo ($details['prospect_status']==5)?'selected':''; ?>>Follow Up</option>
                        </select> 
                    </div>
                </div>   
                <br>
                <!-- info row -->
                <div class="row invoice-info">
                    <div class="col-sm-12 invoice-col">
                        <label>Question</label>
                        <p><?php echo !empty($details['ask_question'])?$details['ask_question']:'NA'; ?></p>
                    </div>
                </div>   
                <br>
                  
                <div class="row no-print">
                  <div class="col-xs-12 text-center">
                    <?php if(isset($edit_action) && !empty($edit_action)){ 
                         if($details['prospect_status']!=2){ ?>
                            <a href="javascript:void(0)" id="submit_form" class="btn btn-primary" onclick=" form_submit('prospect_form');">Update</a>
                    <?php }  } ?>
                    <?php if(isset($back_action) && !empty($back_action)){ ?>
                        <a  href="<?php echo $back_action; ?>" class="btn btn-default ">
                         Back
                        </a>
                    <?php } ?>
                  </div>
                </div>

            </div>
        </form>
    </section>
</div>
<script>
function form_submit(id)
{
    submitDetailsForm(id);
}
</script>
