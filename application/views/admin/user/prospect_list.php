<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12 message_box">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-block alert-success fade in">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
				<?php } ?>
				<div class="box">
					<div class="box-header with-border">  
                  		<!--<h3 class="box-title">Filter Here</h3>-->  
                		<form method="get" action="<?php echo base_url().'admin/prospect/list';?>"> 
						<div class="box-body row"> 
							 <div class="form-group col-md-3">
								<input class="column_filter form-control" id="first_name" name='first_name' type="text" placeholder="First Name" value="<?php echo $filter_first_name; ?>">
							</div>
							<div class="form-group col-md-3">
								<input class="column_filter form-control" id="last_name" name='last_name' type="text" placeholder="Last Name" value="<?php echo $filter_last_name; ?>">
							</div>
			              	<div class="form-group col-md-3">
                                <input class="column_filter form-control" id="email" name='email' type="text" placeholder="Email" value="<?php echo $filter_email; ?>">
			              	</div>
			              	<div class="form-group col-md-3">
                                <input class="column_filter form-control" id="phone" name='phone' type="text" placeholder="Phone" value="<?php echo $filter_phone; ?>">
			              	</div> 
			              	<div class="form-group col-md-3">
                                <input class="column_filter form-control" id="district_name" name='district_name' type="text" placeholder="District" value="<?php echo $filter_district_name;?>">
			              	</div>
			              	<div class="form-group col-md-3">
				              	<select name="state" id="state" class="column_filter form-control">
				              		<option value="">State</option>
				              		<?php if(!empty($states)){
                                        foreach($states as $state){ ?>
                                        <option value="<?php echo $state['state_id'];?>" <?php if(!empty($filter_state) && $filter_state==$state['state_id']){ echo 'selected'; }?>><?php echo $state['state_name'];?></option>
                                    <?php } }?>
		                           
				              	</select> 
                        	</div> 
			              	<div class="form-group col-md-3">
				              	<select name="prospect_status" id="prospect_status" class="column_filter form-control">
				              		<option value="">Prospect Status</option>
				              		<option value="1"  <?php if(!empty($filter_is_approve)&& $filter_is_approve=='1'){ echo 'selected'; }?>>Pending</option>
		                            <option value="2" <?php if(!empty($filter_is_approve)&& $filter_is_approve=='2'){ echo 'selected'; }?>>Sold</option>
		                            <option value="3" <?php if(!empty($filter_is_approve)&& $filter_is_approve=='3'){ echo 'selected'; }?>>Lost</option>
		                            <option value="4" <?php if(!empty($filter_is_approve)&& $filter_is_approve=='4'){ echo 'selected'; }?>>Requested Info</option>
		                            <option value="5" <?php if(!empty($filter_is_approve)&& $filter_is_approve=='5'){ echo 'selected'; }?>>Follow Up</option>
				              	</select> 
                        	</div> 
			             <div class="form-group col-md-3">
			               	<input class="btn btn-primary" type="submit" value="Filter">
			               	<a class="btn btn-default" href="<?php echo base_url().'admin/prospect/list';?>">Reset</a>
			 
			             </div>
			             
			          	</div>
			          </form> 
                        <?php  if(!empty($records_results)){?>
                  			<a href="<?php echo base_url().'admin/prospect/export?first_name='.$filter_first_name.'&last_name='.$filter_last_name.'&email='.$filter_email.'&phone='.$filter_phone.'&district_name='.$filter_district_name.'&state='.$filter_state.'&prospect_status='.$filter_prospect_status;?>" title="" data-toggle="tooltip" data-original-title="Export Excel" class="btn btn-primary"><div id="" class="" value="Export ">Export to Excel</div></a>
                		<?php } ?>
			            <?php /* if(isset($add_action) && !empty($add_action)){ ?>
                  			<!-- <a href="<?php echo $add_action;?>" title="" data-toggle="tooltip" data-original-title="Add User" class="btn btn-default pull-right"><i class="fa fa-plus"></i></a> -->
                		<?php } */ ?>
                	</div> 

					<div class="box-body">

						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<!-- <th>Profile Pic</th> -->
									<th>First Name</th>
									<th>Last Name</th>
									<th>Email</th>
									<th>Phone</th>
									<th>State</th>
									<th>District</th>
									<th>Prospect status</th>
									<?php /*if(isset($edit_action) && !empty($edit_action)){ ?>
									<th>Status</th>
									<?php }*/ ?>
                                    <th>Actions</th>
                                    
								</tr>
							</thead>
							<tbody>
	

								<?php 
								if(!empty($records_results))
								{	
									// echo "<pre>";print_r($records_results);die;
									$i = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
									$table="users";
									$field = "user_id";

									foreach ($records_results as $row) { $i++; 
										 ?>
										<tr id="tr_<?php echo $row[$field]; ?>">
											<td><?php echo $i; ?></td> 
											<?php /* <td>
												<?php if(!empty($row['profile_image']) && file_exists($row['profile_image'])){
													$image = base_url().$row['profile_image'];
												}else{
													$image = base_url().'assets/front/images/person.jpg';
												}
												?> 
												<a href="<?php echo $image?>"><img src="<?php echo $image?>" width="25px" height="25px"></a> 
											</td> */ ?>
											<td><?php if(!empty($row['first_name'])) echo ucfirst($row['first_name']);?></td>
											<td><?php if(!empty($row['last_name'])) echo ucfirst($row['last_name']);?></td>
											<td><?php if(!empty($row['email'])) echo ucfirst($row['email']);?></td>
											<td><?php if(!empty($row['phone'])) echo ucfirst($row['phone']);?></td>
											<td><?php if(!empty($row['state_name'])) echo ucfirst($row['state_name']);?></td>
											<td><?php if(!empty($row['district_name'])) echo ucfirst($row['district_name']);?></td>
											<td><?php 
											$profile_status= 'Pending';
											$class = "badge bg-yellow";
												if(!empty($row['prospect_status'])) {
													if($row['prospect_status']==1){
														$profile_status= 'Pending';
														$class = "badge bg-yellow";
													}if($row['prospect_status']==2){
														$profile_status='Sold';
														$class = "badge bg-green";
													}if($row['prospect_status']==3){
														$profile_status= 'Lost';
														$class = "badge bg-red";
													}
													if($row['prospect_status']==4){
														$profile_status= 'Requested Info';
														$class = "badge bg-blue";
													}
													if($row['prospect_status']==5){
														$profile_status= 'Follow Up';
														$class = "badge bg-pink";
													}

												} //echo '<pre>';print_r($row);exit;?>
												<p id="status_<?php echo $row[$field]; ?>"  title="" data-toggle="tooltip" data-original-title="Prospect status" class="<?php echo $class; ?>" ><?php echo $profile_status; ?></p>
											</td>
											<td class="td-actions">
												<div class="btn-group">
													<a id="" href="<?php echo $view_action.'/'.$row[$field]; ?>" class="btn btn-xs btn-primary" title="" data-toggle="tooltip" data-original-title="Details"><i class="fa fa-eye"></i></a>
								                </div>
											</td>
										</tr>

									<?php }
								} else {
									echo "<tr><td colspan='7' align='center'> No Record Found</td></tr>";
								} ?>
							</tbody>
						</table>
							<tfoot>						
								<tr>
									<?php if(!empty($pagination)) { ?>
										<td >Total Records - <?php echo $total_records;?></td>
										<td colspan="7" align="center">
											<div><?php echo $pagination; ?></div>
										</td>
									<?php }else{ ?>	
										<td align="center">Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
										<td colspan="7" align="center"></td>
									<?php } ?>			
								</tr>
							</tfoot>	
					</div>			
				</div>			
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

