<div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $page_title;?> </h1>
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php }?>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-primary"> 
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">  
                    <div class="col-lg-12">
                        <!-- flash messages-->
                        <div class="message_box" style="display:none;"></div>
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('error') ?>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success')) { ?>
                            <div class="alert alert-block alert-success fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('success') ?>
                            </div>
                        <?php } ?>
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <?php if(isset($form_action) && !empty($form_action)){ ?>
                                    <form id="request_type_form" class="" method="POST" action="" enctype="multipart/form-data" role="form"  data-parsley-validate>
                                <?php } ?>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="title">Full Name *</label>
                                            <input type="text" class="form-control" name="fullname" id="fullname" value="<?php echo set_value('fullname'); ?>" placeholder="Full Name" maxlength="100" data-parsley-required data-parsley-required-message="Please enter full name.">
                                            <?php echo form_error('fullname'); ?>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="email">Login ID *</label>
                                            <div class="input-group">
                                                <input type="text"  class="form-control" id="staging_id" name="staging_id" placeholder="Login ID"   oninput="this.value = this.value.replace(/[^A-Za-z0-9.']/g,'');" value="" maxlength="200" data-parsley-required data-parsley-required-message="Please enter Login ID.">
                                                <span class="input-group-addon">@cele.com</span>
                                            </div>                                            
                                            <p class="error" id="un_error_title" style="display:none;">Login ID already used.</p>
                                            <?php echo form_error('staging_id'); ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="mobile">Mobile Number *</label>
                                            <input type="text" class="form-control" id="mobile"  name="mobile" oninput="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'');mobile_check()" placeholder="Mobile Number" value="" 
                                            data-parsley-required data-parsley-required-message="Please enter telephone number." 
                                            data-parsley-type="number" 
                                            maxlength="25" 
                                            data-parsley-type-message="Please enter valid telephone number" data-parsley-minlength-message="Please enter valid number">
                                            <p class="error" id="access_mobile_error" style="display:none;">Mobile number already used.</p>
                                            <?php echo form_error('mobile'); ?>
                                        </div>
                                        <div class="form-group col-md-6">
                                           <label for="email">Password *</label>
                                            <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="" data-parsley-required data-parsley-required-message="Please enter password." minlength="6" maxlength="15" autocomplete="off">
                                            <?php echo form_error('password'); ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="email">Confirm Password *</label>
                                            <input type="password" class="form-control" id="confpassword" name="confpassword" placeholder="Confirm Password" value="" data-parsley-required data-parsley-required-message="Please enter confirm password." minlength="6" maxlength="15" data-parsley-equalto="#password" data-parsley-equalto-message="Confirm password must be same as password." autocomplete="off">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="email">User Type *</label>
                                            <select name="user_type" id="user_type" class="column_filter form-control" data-parsley-required data-parsley-required-message="Please select user type.">
                                                <option value="">User Type</option>
                                                <option value="1">Celebrant</option>
                                                <option value="2">Service Provider</option>
                                                <option value="3">Celebrant + Service Provider</option>
                                            </select> 
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="title">Company Name *</label>
                                            <input type="text" class="form-control" name="company_name" id="company_name" placeholder="Company Name" maxlength="150" data-parsley-required data-parsley-required-message="Please enter company name.">
                                            <?php echo form_error('company_name'); ?>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="country">Country *</label>
                                            <select class="form-control" id="country" name="country" data-parsley-required data-parsley-required-message="Please select country.">
                                                <option value=''>Select Country</option>
                                                <?php if(isset($countries)) {
                                                    foreach($countries as $country) { ?>
                                                        <option value=<?php echo $country['id'];?> ><?php echo $country['name'];?></option>";
                                                    <?php }
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="state">State</label>
                                            <select class="form-control" id="state" name="state" data-parsley-required data-parsley-required-message="Please select state.">
                                                <option value='' id='first'>Select State</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="description">Address *</label>
                                             <textarea name="address" placeholder="Address" class="form-control" rows="3" autocomplete="off" data-parsley-required data-parsley-required-message="Please enter address." maxlength="200"><?php echo set_value('address'); ?></textarea>
                                         </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="description">Business Number </label>
                                            <input type="text" class="form-control" name="business_no" id="business_no" value="" placeholder="Business Number" maxlength="100">
                                            <?php echo form_error('business_no'); ?>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="description">Year of Establishment *</label>
                                            <input type="text" class="form-control" name="establishment_year" id="establishment_year" value="" placeholder="Year of Establishment" maxlength="4" oninput="this.value = this.value.replace(/[^0-9]/g,'');" data-parsley-required data-parsley-required-message="Please enter year of establishment." >
                                            <?php echo form_error('establishment_year'); ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="description">SSN </label>
                                            <input type="text" class="form-control" name="identification_number" id="identification_number" value="" placeholder="SSN" maxlength="9">
                                            <?php echo form_error('identification_number'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer text-center">
                                    <?php if(isset($form_action) && !empty($form_action)){ ?>
                                        <button type="submit" id="submit_form" class="btn btn-primary" onclick="return form_submit('request_type_form');">Add</button>
                                    <?php } ?>
                                    <a href="<?php echo $back_action;?>" class="btn btn-default">Back</a> 
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box --> 
            </div><!-- col-12--> 
        </div><!-- row--> 
    </section>
</div>
<!-- row--> 
<!-- Include Bootstrap Datepicker -->

<script>
$('#country').change(function(e) {
    var id = $("#country").val();
    if(id!='') {
        $('#state option').slice(1).remove();
        $('#city option').slice(1).remove();
        $.ajax({
            type:'POST',
            url: "<?php echo base_url(); ?>admin/ajax/get_states/",
            data: {id:id},
            
            success:function(data)
            {
                if(data) {
                    var states="";
                    data = JSON.parse(data);
                    for(var i=0;i<data.length;i++) {
                        states += "<option value="+data[i].id+">"+data[i].name+"</option>";
                    }
                    $("#state").find("#first").after(states);
                } else {
                    error_msg = "Some Error occured. Please try again !!" ;
                    error = '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close" type="button">×</button>'+error_msg+'</div>';
                    $('#notification_msg').html(error).fadeIn(250).fadeOut(10000);
                }
            }
        });
    }
});

var is_staging_id_valid=1;
$('#staging_id').keyup(function(e) {
    error_msg="";
    $("#un_error_title").hide();
    var check_value = $("#staging_id").val().trim();
    var matched_value = check_value+'@cele.com';
    var matched_column = 'staging_id';
    var table="users";
    if(matched_value!='') {
        $.ajax({
            type:'POST',
            url: "<?php echo base_url(); ?>admin/ajax/check_unique/",
            data: {matched_value:matched_value,matched_column:matched_column,table:table},
            
            success:function(data)
            {
                if(data==1) {
                    is_staging_id_valid=0;
                    error_msg = "Login ID already used." ;
                    $("#un_error_title").show();
                } else {
                    $("#un_error_title").hide();
                    is_staging_id_valid=1;
                }
                
            }
        });
    } else {
        is_staging_id_valid=1;
        error_msg = "" ;
        $("#un_error_title").hide();
    }
});


var is_mobile_valid=1;
function mobile_check(){
    error_msg_mobile="";
    $("#access_mobile_error").hide();
    var matched_value = $("#mobile").val().trim();
    var matched_column = 'mobile';
    var table="users";
    if(matched_value!='') {
        $.ajax({
            type:'POST',
            url: "<?php echo base_url(); ?>admin/ajax/check_unique/",
            data: {matched_value:matched_value,matched_column:matched_column,table:table},
            
            success:function(data)
            {
                if(data==1) {
                    is_mobile_valid=0;
                    error_msg_mobile = "Mobile number already used." ;
                    $("#access_mobile_error").show();
                } else {
                    $("#access_mobile_error").hide();
                    is_mobile_valid=1;
                }
                
            }
        });
    } else {
        is_mobile_valid=1;
        error_msg_mobile = "" ;
        $("#access_mobile_error").hide();
    }
}

function form_submit(id)
{
    if(is_staging_id_valid==1 && is_mobile_valid==1) {
       submitDetailsForm(id);
        return false;
    } else {
        return false;
    }
    //return false;
}
</script>


