<div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $page_title;?> </h1>
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php }?>
        </ol>
    </section>

    <!-- Main content -->
    <!-- <div class="pad margin no-print">
       
        <div class="callout callout-info" style="margin-bottom: 0!important;">
           <div class="row">
             <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="ion ion-information-circled"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text text-black">Confirmed Request</span>
                        <span class="info-box-number text-black">Yes</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-olive"><i class="ion ion-information-circled"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text text-black">Completed Request</span>
                        <span class="info-box-number text-black">Yes</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="ion ion-information-circled"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text text-black">Canceled Request</span>
                        <span class="info-box-number text-black">Yes</span>
                    </div>
                </div>
            </div>
        </div>
       </div>
       
    </div> -->
    <section class="content">
        <div class="invoice">
           <!-- title row -->
           <div class="row">
              <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-globe"></i> <?php if(!empty($details['fullname']))echo Ucfirst($details['fullname']); ?>
                    <?php if($details['user_type']==1){
                        echo "- Celebrant";
                    }elseif ($details['user_type']==2) {
                        echo "- Service Provider";
                    }elseif ($details['user_type']==3) {
                        echo "- Celebrant + Service Provider";
                    }
                    ?>
                    <small class="pull-right">
                        Reg Date: <?php if(!empty($details['created']))echo date('m/d/Y',strtotime($details['created'])); ?>
                    </small>
                </h2>
              </div>
              <!-- /.col -->
           </div>
           <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    <label>Profile Pic</label><br>
                    <?php if(!empty($details['profile_picture']) && file_exists($details['profile_picture'])){
                        $image = base_url().$details['profile_picture'];
                    }else{
                        $image = base_url().'resources/default_image.png';
                    }
                    ?> 
                    <a href="<?php echo $image?>"><img src="<?php echo $image?>" width="100px" height="100px"></a> 
                 
                </div>
                
            </div>
            <BR>
           <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    <label>Login ID </label>
                    <p><?php echo !empty($details['staging_id'])?$details['staging_id']:'NA'; ?></p>
                    <br>
                    <label>Mobile Number </label>
                    <p><?php echo !empty($details['mobile'])?$details['mobile']:'NA'; ?></p>
                    <br>
                    <label>Email </label>
                    <p><?php echo !empty($details['email'])?$details['email']:'NA'; ?></p>
                    <br>
                    <label>Status </label>
                    <p><?php if(!empty($details['status']))echo $details['status']; ?></p>
                    
                </div>
                 <div class="col-sm-4 invoice-col">
                    <label>Dob </label>
                    <p><?php  if(!empty($details['dob']) && $details['dob']!='0000-00-00'){echo $details['dob'];}else{echo  'NA';} ?></p>
                    <br>
                    <label>Gender </label>
                    <p><?php echo !empty($details['gender'])?$details['gender']:'NA'; ?></p>
                    <br> 
                    <label>Hobbies </label>
                    <p><?php echo !empty($details['hobbies'])?$details['hobbies']:'NA'; ?></p>
                    <br>
                    <label>Interests </label>
                    <p><?php echo !empty($details['interests'])?$details['interests']:'NA'; ?></p>
                  
                </div>
                <div class="col-sm-4 invoice-col"> 
                    <label>Education </label>
                    <p><?php echo  !empty($details['education']) ?$details['education']:'NA'; ?></p>
                    <br>   
                    <label>Hometown </label>
                    <p><?php echo !empty($details['hometown'])?$details['hometown']:'NA'; ?></p>
                    <br>   
                    <label>Company Name </label>
                    <p><?php echo !empty($details['company_name'])?$details['company_name']:'NA'; ?></p>
                    <br>
                    <label>Business Number </label>
                    <p><?php echo !empty($details['business_no'])?$details['business_no']:'NA'; ?></p> 
                </div>   
                <div class="col-sm-4 invoice-col"> 
                    <label>Paypal ID </label>
                    <p><?php echo  !empty($details['paypal_id']) ?$details['paypal_id']:'NA'; ?></p><br>
                </div>
            </div>   
            <br>
              <!-- info row -->
            <?php if(!empty($address))
            foreach ($address as $key => $add_list) { ?>
              <div class="row invoice-info">
                  <div class="col-sm-3 invoice-col">
                      <b><label>Address <?php echo ++$key?></label></b><hr>
                  </div>
              </div>
                <div class="row invoice-info">
                     <div class="col-sm-3 invoice-col">
                        <label>Country </label>
                        <p><?php echo !empty($add_list['country_name'])?$add_list['country_name']:'NA'; ?></p>
                        <br> 
                    </div>
                    <div class="col-sm-3 invoice-col">
                        <label>State Name </label>
                        <p><?php echo !empty($add_list['state_name'])?$add_list['state_name']:'NA'; ?></p>
                        <br> 
                    </div>
                    <div class="col-sm-3 invoice-col"> 
                        <label>City Name </label>
                        <p><?php echo  !empty($add_list['city_name']) ?$add_list['city_name']:'NA'; ?></p>
                        <br>    
                    </div>   
                    <div class="col-sm-3 invoice-col"> 
                        <label>Address </label>
                        <p><?php echo  !empty($add_list['address']) ?$add_list['address']:'NA'; ?></p>
                        <br>    
                    </div>   
                </div>   
              <?php }
            ?>
            <BR>
            
            <!-- info row -->
              
            <BR> 
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    <label>Report Abused </label>
                    <p><?php if(!empty($get_report_post)){ echo '<a href="'.base_url().'admin/user/post_report/'.$details['user_id'].'">'.$get_report_post.'</a>';}else{ echo '0';}?></p>
                    <br>
                </div> 
                <div class="col-sm-4 invoice-col">
                    <label>Liked Post </label>
                    <p><?php if(!empty($get_like_post)){ echo '<a href="'.base_url().'admin/user/post_like/'.$details['user_id'].'">'.$get_like_post.'</a>';}else{ echo '0';}?></p>
                    <br>
                </div> 
                <div class="col-sm-4 invoice-col">
                    <label>Shared Post </label>
                    <p><?php if(!empty($get_shared_post)){ echo '<a href="'.base_url().'admin/user/post_shared/'.$details['user_id'].'">'.$get_shared_post.'</a>';}else{ echo '0';}?></p>
                    <br>
                </div> 
            </div>
            <BR> 
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    <label>Friends</label>
                    <p><?php if(!empty($get_friends_count)){ echo '<a href="'.base_url().'admin/user/friend/'.$details['user_id'].'">'.$get_friends_count.'</a>';}else{ echo '0';}?></p>
                    <br>
                </div>  
            </div>
            <div class="row no-print">
              <div class="col-xs-12 text-center">
                
                <?php if(isset($edit_action) && !empty($edit_action)){ ?>
                    <a  href="<?php echo $edit_action; ?>" class="btn btn-primary">
                     EDIT
                    </a>
                <?php } ?>
                <?php if(isset($back_action) && !empty($back_action)){ ?>
                    <a  href="<?php echo $back_action; ?>" class="btn btn-default ">
                     Back
                    </a>
                <?php } ?>
              </div>
           </div>
        </div>
    </section>
</div>

