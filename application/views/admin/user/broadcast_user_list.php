<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section>
 <section class="content">
        <div class="box box-primary"> 
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">  
                    <div class="col-lg-12">
                        <!-- flash messages-->
                        <div class="message_box" style="display:none;"></div>
                                                                        <div class="panel panel-primary">
                            <div class="panel-body">
                                                                <div class="box-body">
                                    <div class="row ">
                                        <div class="form-group col-md-6">
                                            <label for="title">Subject :- </label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <div>
                                           <?php echo ucfirst($subject); ?>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="row ">
                                        <div class="form-group col-md-6">
                                            <label for="title">Massage :- </label>
                                           
                                        </div>
                                        <div class="form-group col-md-6">
                                            <div><?php echo ucfirst($message); ?> </div>
                                           
                                        </div>
                                    </div>  
                                
                                    <div class="row ">
                                        <div class="form-group col-md-6">
                                            <label for="title">User Name :- </label>
                                           
                                        </div>
                                        <div class="form-group col-md-6">
                                            <div><div><?php echo $this->db->get_where('admin',array('admin_id'=>$sent_by))->row()->username; ?></div> </div>
                                           
                                        </div>
                                    </div>
                                     <div class="row ">
                                        <div class="form-group col-md-6">
                                            <label for="title">User type :- </label>
                                           
                                        </div>

                                         <div class="form-group col-md-6">
                                            <div>
                                            	<?php echo ucfirst($user_type); ?>
											</div>
                                        </div>
                                    
                                       
                                    </div>
                                    <div class="row ">
                                        <div class="form-group col-md-6">
                                            <label for="title">Date :- </label>
                                           
                                        </div>
                                        <div class="form-group col-md-6">
                                            <div><?php echo ucfirst(convertGMTToLocalTimezone($date)); ?> </div>
                                        </div>
                                    </div>
                                  
                                </div>

                                <div class="form-group">
                                    
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Email</th>
								</tr>
							</thead>
							<tbody>
	

								<?php 
								if(!empty($records_results))
								{	
									// echo "<pre>";print_r($records_results);die;
									$i = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
									$table="broadcast_emails";
									$field = "id";

									foreach ($records_results as $row) { $i++;  ?>
										<tr id="tr_<?php echo $row[$field]; ?>">
											<td><?php echo $i; ?></td> 
											<td><?php if(!empty($row['email'])) echo ucfirst($row['email']);?></td>
										</tr>

									<?php }
								} else {
									echo "<tr><td colspan='2' align='center'> No Record Found</td></tr>";
								} ?>
							</tbody>
						</table>
							<tfoot>						
								<tr>
									<?php if(!empty($pagination)) { ?>
										<td >Total Records - <?php echo $total_records;?></td>
										<td colspan="7" align="center">
											<div><?php echo $pagination; ?></div>
										</td>
									<?php }else{ ?>	
										<td align="center">Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
										<td colspan="7" align="center"></td>
									<?php } ?>			
								</tr>
							</tfoot>
						</table>
                                </div>
                              
                               
                            </div>
                        </div>
                    </div>
                </div><!-- /.box --> 
            </div><!-- col-12--> 
        </div><!-- row--> 
    </section>
	<!-- Main content -->
	
</div><!-- /.content-wrapper -->

