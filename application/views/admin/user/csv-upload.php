<div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $page_title;?> </h1>
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php } ?>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-primary"> 
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">  
                    <div class="col-lg-12">
                        <!-- flash messages-->
                        <div class="message_box" style="display:none;"></div>
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('error') ?>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success')) { ?>
                            <div class="alert alert-block alert-success fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('success') ?>
                            </div>
                        <?php } ?>
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <form method="post" action="admin/user/import-file" id="import_file_upload" enctype="multipart/form-data">
                                
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <!-- <a href="assets/front/csv/Sample File.xlsm" class="btn btn-primary"><span class="hide-xs">Sample Macro File</span></a> -->

                                                <a href="download-media/1" class="btn btn-primary"><span class="hide-xs">Sample File</span> <!-- <i class="fa fa-apple"></i> --></a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                               <label>Upload Your File </label>
                                                <div class="files">
                                                    <input type="hidden" name="user_id" value="<?php if(isset($user_id)){ echo $user_id;}?>">
                                                 <input type="file" class="form-control" name="file" onchange="file_form_submit();" accept=".xlsx, .xls">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer text-left">
                                        <div class="form-group col-md-4">
                                        <input type="submit" class="btn btn-primary" name="submit" value="Submit">
                                        <a href="<?php echo $back_action;?>" class="btn btn-default">Back</a>
                                        </div>
                                    </div>
                                </form>

                                <div class="upload-list bg-light">
                                    <h2 class="company-name">Uploaded Files</h2>
                                </div>
                                <div class="">
                                    <table class="table table-responsive-sm table-responsive-md table-hover child-list">
                                        <tr>
                                            <th>File Name</th>
                                           <!--  <th>Status</th> -->
                                            <th>Date</th>
                                        </tr>
                                        <?php if(!empty($csv_data)){
                                        $i=0;
                                        foreach ($csv_data as $key => $details) { $i++;?>
                                            <tr>
                                                <td><a href="<?php if(!empty($details['file_name']))
                                                {echo $details['file_name'];}else{echo 'javascript:void(0);';} ?>"><?php if(!empty($details['file_name'])){
                                                        $file=explode('/',$details['file_name']);
                                                        echo str_replace('_', ' ', $file[2]);
                                                    } ?></a></td>
                                             <!--    <td><?php //if($details['status']==0){echo 'Pending';}elseif($details['status']==1){echo 'Completed';}else{echo 'Error';}?></td> -->
                                                <td><?php if($details['created']){echo convertGMTToLocalTimezone($details['created']);}?></td>
                                            </tr>
                                        <?php } }else{ ?>
                                        <tr>
                                            <td colspan='4' align='center'>No records available.</td>
                                        </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box --> 
            </div><!-- col-12--> 
        </div><!-- row--> 
    </section>
</div>
<!-- row--> 
 <script type="text/javascript">
function file_form_submit(){
    // $("#loader").show();
    $("#import_file_upload").submit();
}

function downloadFile(filePath) {
    var link = document.createElement('a');
    link.href = filePath;
    link.download = filePath.substr(filePath.lastIndexOf('/') + 1);
    link.click();
}
</script>