<div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
<?php if(empty($records_results)){
    redirect(base_url()."admin/report");
}?>
    <section class="content-header">
        <h1> <?php echo $page_title;?> </h1>
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php } ?>
        </ol>
    </section>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-primary"> 
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">  
                    <div class="col-lg-12">
                        <!-- flash messages-->
                        <div class="message_box" style="display:none;"></div>
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('error') ?>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success')) { ?>
                            <div class="alert alert-block alert-success fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('success') ?>
                            </div>
                        <?php } ?>
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <?php if(isset($form_action) && !empty($form_action)){ ?>
                                    
                                <?php } ?>
                                <div class="box-body">
                                    <div class="row ">
                                        <div class="form-group col-md-6">
                                            <label for="title">Report By :- </label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <div>
                                            <?php 
                                            if(!empty($records_results['display_name'])){
                                                echo ucwords($records_results['display_name']);
                                            }else{
                                                echo ucwords($records_results['first_name'].' '.$records_results['last_name']);
                                            }
                                            ?> 
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="row ">
                                        <div class="form-group col-md-6">
                                            <label for="title">School Name :- </label>
                                           
                                        </div>
                                        <div class="form-group col-md-6">
                                            <div><?php echo ucwords($records_results['name']);?> </div>
                                           
                                        </div>
                                    </div>  
                                
                                    <div class="row ">
                                        <div class="form-group col-md-6">
                                            <label for="title">Media Title :- </label>
                                           
                                        </div>
                                        <div class="form-group col-md-6">
                                            <div><?php echo ucwords($records_results['title']);?> </div>
                                           
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="form-group col-md-6">
                                            <label for="title">Comment By :- </label>
                                           
                                        </div>
                                        <div class="form-group col-md-6">
                                            <div><?php echo ucfirst($this->db->get_where('users',array('user_id'=> $records_results['cid']))->row()->first_name); ?> </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="form-group col-md-6">
                                            <label for="title">Comment :- </label>
                                           
                                        </div>
                                        <div class="form-group col-md-6">
                                            <div><?php echo nl2br($records_results['comment']);?> </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="form-group col-md-6">
                                            <label for="title">Reported Date :- </label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <div><?php echo convertGMTToLocalTimezone($records_results['created']) ?> </div>
                                           
                                        </div>
                                    </div>
                                     <div class="row ">
                                        <div class="form-group col-md-6">
                                            <label for="title">Reason :- </label>
                                           
                                        </div>
                                        <div class="form-group col-md-6">
                                            <div><?php echo nl2br($records_results['reason']);?> </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <a href="<?php echo $back_action;?>" class="btn btn-primary">Back</a>
                                       
                                       <a href="javascript:void(0)" class="btn btn-danger" onclick="soft_delete_record(<?php echo $records_results['comment_id'];?>,'comment_report','id')" data-toggle="tooltip" <?php if($records_results['is_delete']==1){ ?> disabled="disabled";data-original-title="Deleted" <?php }else { ?> data-original-title="Delete"<?php  }?>>Delete
                                        </a>
                                    </div>
                                </div>
                              
                               
                            </div>
                        </div>
                    </div>
                </div><!-- /.box --> 
            </div><!-- col-12--> 
        </div><!-- row--> 
    </section>
</div>
                                
<!-- row--> 
<!-- Include Bootstrap Datepicker -->
