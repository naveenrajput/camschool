<div class="content-wrapper">
    <section class="content-header">
        <h1> Dashboard </h1>
        <ol class="breadcrumb">

            <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">

            <?php if(!empty($districtUrl)){ ?>
            <div class="col-md-3 col-sm-4 col-xs-12">
                <a href="<?php echo $districtUrl?>">
                    <div class="info-box">
                        <span class="info-box-icon  bg-yellow"><i class="fa fa-users"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Total Districts</span>
                            <span class="info-box-number"><?php if (!empty($totalDistricts)){echo $totalDistricts;}else{ echo 0;} ?>
                            </span>
                        </div>
                    </div>
                </a>
            </div>
            <?php } ?>    

            <?php if(!empty($districtUrl)){ ?>
            <div class="col-md-3 col-sm-4 col-xs-12">
                <a href="<?php echo $totalDistrictsapproveurl?>">
                    <div class="info-box">
                        <span class="info-box-icon  bg-red"><i class="fa fa-users"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Total Prospect</span>
                            <span class="info-box-number"><?php if (!empty($totalDistrictsapprove)){echo $totalDistrictsapprove;}else{ echo 0;} ?>
                            </span>
                        </div>
                    </div>
                </a>
            </div>
            <?php } ?> 

            <?php if(!empty($principalUrl)) {?>
            <div class="col-md-3 col-sm-4 col-xs-12">
                 <a href="<?php echo $principalUrl?>">
                    <div class="info-box">
                        <span class="info-box-icon  bg-green"><i class="fa fa-users"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Total Principals</span>
                            <span class="info-box-number"><?php if (!empty($totalPrincipals)){echo $totalPrincipals;}else{ echo 0;} ?></span>
                        </div>
                    </div>
                </a>
            </div>
            <?php } ?>  

            <?php if(!empty($teacherUrl)) {?>
            <div class="col-md-3 col-sm-4 col-xs-12">
                  <a href="<?php echo $teacherUrl?>">
                    <div class="info-box">
                        <span class="info-box-icon  bg-red"><i class="fa fa-users"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Total Teachers</span>
                            <span class="info-box-number"><?php if (!empty($totalTeachers)){echo $totalTeachers;}else{ echo 0;} ?></span>
                        </div>
                    </div>
                </a>
            </div>
            <?php } ?> 

            <?php if(!empty($parentUrl)) {?>
            <div class="col-md-3 col-sm-4 col-xs-12">
                 <a href="<?php echo $parentUrl?>">
                    <div class="info-box">
                        <span class="info-box-icon  bg-blue"><i class="fa fa-users"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Total Parents </span>
                            <span class="info-box-number"><?php if (!empty($totalParents)){echo $totalParents;}else{ echo 0;} ?></span>
                        </div>
                    </div>
                </a>
            </div>
            <?php }?>

            <?php if(!empty($supportUrl)){ ?>
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <a href="<?php echo $supportUrl?>">
                        <div class="info-box">
                            <span class="info-box-icon  bg-blue"><i class="fa fa-envelope"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Pending Support <br>Queries</span>
                                <span class="info-box-number"><?php if (!empty($totalPendingSupports)){echo $totalPendingSupports;}else{ echo 0;} ?></span>
                            </div>
                        </div>
                    </a>
                </div>
            <?php } ?> 




        </div>
       
    </section>
</div>