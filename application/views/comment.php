<style type="text/css">
    .clr{color:#e3548a;}
    .nocursor{
        cursor: auto!important;
    }
</style>

<?php
// echo "<pre>";
// print_r($comments);
$userId = $this->session->userdata("user_id");
$user_type = $this->session->userdata("front_user_type");
$type = ($innerlimit > 0) ? 'main' : 'inner';

?> <ul class="media-list remove-pd ">  <?php
foreach($comments as $key => $value){ ?>

    <li class="media <?php echo ($innerlimit > 0) ? '' : 'sub-coment';?>">
        
        <a href="javascript:void(0)" class="pull-left">
            <img src="<?= $value['profile_image']?$value['profile_image']:"assets/front/images/person.jpg";?>" width="40px" height="40px" alt="" class="img-circle">
        </a>

        <!-- <div class="blog-media-body test_three"> -->
        <div class="media-body test_three">
            
                <small class="text-muted pull-right"><?php echo get_time_posted(date('Y-m-d H:i:s'),$value['comment_publish']);?></small>
                <small class="text-muted ">
                    <?php if($this->session->userdata('front_user_type')==1 || $this->session->userdata('front_user_type')==2){
                        echo ucwords($value['name']);
                    }
                    else{
                        // echo ucfirst($value['display_name']);
                        echo ucfirst(($value['display_name'])?$value['display_name']:$value['name']);
                        
                    }?>
                </small>
               
            <?php if($value['comment_type'] == 2){ ?> 
                <p class="m-0"><?php echo wordwrap(ucfirst($value['comment']),150," ",TRUE);?></p>    
            <?php } ?>            
            <div class="row test_three_2">
            <!-- <ul class="list-inline test_three_2"> -->
                <?php 
                if($value['comment_type'] == 1){
                    $duration = date("i:s", strtotime($value['duration']));
                    $mediaType = $value['media_type'];
                    $mediaSlug = $value['media_slug'];
                    $views = $value['views'];
                    // $stance = $value['stance'];
                    $thumbImage = $value['thumb_image'];
                    $mediaImage = ($mediaType == 1) ? $thumbImage: "assets/images/slider_images/img7.jpg";
                    // $isFav = ($value['is_fav'] == 1)? "":"-o";
                    $mediaTitle = $value['title'];
                    $mediaDescription = $value['description'];
                    $mediaIcon = ($mediaType == 1)? "link_icon": "audio_icon";
                    // $subOutletName = ($value['sub_outlet_name'])? $value['sub_outlet_name']: $value['other_sub_outlet'];
                    if(!empty($mediaType)) { ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xxs-12 mrgT-20 mrgB-10">
                            <div class="box_design audio_box">
                                <a href="video-fulldetail/<?= $mediaSlug; ?>">
                                    <div class="video_cover_box">
                                        <div class="hover_overlay"></div>
                                            <span class='<?= $mediaIcon;?>'></span>
                                            <div class="video_img"> 
                                                <img alt="" src="<?= $mediaImage; ?>" class="img-responsive">
                                            <!-- <span class="time_srip"><?= $duration; ?></span>
                                            <span class='favourite_icon'><i class='fa fa-star' aria-hidden='true'></i></span> -->
                                            <h6 class="utitle"><?php echo $mediaTitle; ?></h6>
                                            <div class="hover_discription"><p><?php echo $mediaDescription; ?></p></div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php  ?>    
                <?php 
                    }
                } ?>

                <div class="col-md-12 col-xs-12 test_nine pl-0">

                    <?php if($value['comment_like']){ $fa_class='fa fa-thumbs-up';}else{$fa_class='fa fa-thumbs-o-up'; }?>

                <div class="col-md-12 col-xs-12 test_nine">
                <?php //echo "<pre>";print_r($value);?>
                    <?php if($value['like_count']){ $fa_class='fa fa-thumbs-up';}else{$fa_class='fa fa-thumbs-o-up'; }?>

                    <?php if($user_type==3 || $user_type==4){ ?>
                     
                        <span id="like_<?= $value['id'];?>" <?php  if($value['like_count']!=0){ ?> class="cursor btn <?php echo ($value['comment_like']==1)?'clr':'';?>" <?php }else{?>
                        class="cursor btn btn_design_light btn-small like_btn_design"<?php } ?> onclick="like_comment(<?= $value['id'];?>);">
                        <i class="<?php echo $fa_class;?>" aria-hidden="true"></i> <?= $value['like_count'];?> </span>


                    <?php }else{  ?>
                        <span id="like_<?= $value['id'];?>" <?php if($user_type!=1 || $user_type!=2){ ?> class="nocursor btn btn-small like_btn_design"<?php } else{ ?> class="nocursor btn btn_design_light btn-small like_btn_design"<?php }?>>
                            <i class="<?php echo $fa_class;?>" aria-hidden="true"></i> <?= $value['like_count'];?> </span>
                    <?php } ?>


                    <?php if($user_type==3 || $user_type==4){ ?>
                        <a  href="javascript:void(0);" class="cursor btn btn_design btn-small like_btn_design" onclick="$('#reply_<?= $value['id'];?>').slideToggle(500);"><i class="" aria-hidden="true"></i> Reply <?= $value['reply_count']?$value['reply_count']:"";?></a>
                    <?php } else{ ?>
                        <?php if($user_type!=1 || $user_type!=2 ){ }else{?>
                        <a  href="javascript:void(0);" class="cursor btn btn_design btn-small like_btn_design" ><i class="" aria-hidden="true" ></i> Reply <?= $value['reply_count']?$value['reply_count']:"";?></a>
                    <?php }?>
                    <?php } ?>
                    <?php if($value['reply_count'] > 0 && $value['parent_id'] > 0){
                        if(count($value['reply']) == 0){
                            echo '<a href="javascript:void(0);" class="cursor btn btn_design btn-small like_btn_design" onclick="loadMoreComments('.$value['parent_id'].', 10, 0, 2, 0,\'main\');">Show Replies</a>';
                        }
                    } ?>
                    <?php if($user_type==3 || $user_type==4){ ?>
                        <?php if($value['report_count']){ ?>
                                <span id="report_<?= $value['id'];?>" class="nocursor btn btn-small like_btn_design <?php echo ($value['report_by']==1)?'clr':'';?>">
                                <i class="fa fa-flag" aria-hidden="true"></i> </span>
                            <?php }else{ ?>
                                
                             <?php if($_SESSION['user_id']!=$value['user_id']){ ?>
                             <span id="report_<?= $value['id'];?>" class="btn btn_design_light btn-small like_btn_design" onclick="report_comment_modal(<?= $value['id'];?>)" >
                            <i class="fa fa-flag-o" aria-hidden="true"></i> </span>
            
                            <?php }?>
                        <?php } ?>
                    <?php } else{ ?>
                        <?php if($user_type!=1 || $user_type!=2 ){ }else{?>
                            <span id="report_<?= $value['id'];?>" class="nocursor btn btn_design_light btn-small like_btn_design" >
                            <i class="fa fa-flag" aria-hidden="true"></i></span>
                    <?php }} ?>
                    <?php if(!empty($userId)){ ?>
                    <div class="detail_media comment_area test_four" id="reply_<?= $value['id'];?>" style="display: none;">
                        <div class="blog-media-body">
                            <form method="post" name="comment_reply_<?= $value['id'];?>">
                                <div class="comment_textarea">
                                    <textarea class="form-control comment" placeholder="Write a comment" name="reply_comment" id="reply_text_<?= $value['id'];?>" maxlength="250"></textarea>
                                    
                                    <p class="error" id="reply_error_<?= $value['id'];?>"></p>
                                </div>
                                <?php if($user_type !=4){?>
                                <a class="btn btn-outline-primary upload-video-comment" type="button" href="teacher-upload-video/<?php echo (isset($grade) && !empty($grade))?$grade:'0'; ?>/<?php echo isset($value['media_id'])?$value['media_id']:''; ?>/<?php echo isset($value['id'])?$value['id']:''; ?> " onclick="myfunction()">Click here to post video </a>
                                 <?php }?>
                                <button class="btn btn-primary pull-right loader_btn" type="button" id="btn_<?= $value['id'];?>" onclick="comment_reply(<?= $value['id'];?>,<?= $value['media_id'];?>)">Post</button>
                                
                            </form>
                        </div>
                    </div>
                    <?php }   
                    
                    // now changes here only
                    foreach($value['reply'] as $ikey => $ivalue){ ?>
                     <?php //echo "<pre>"; print_r($ivalue);?>
                        <!-- <div class="detail_media  test_five" style="<?php // if($ikey == 0){ echo 'margin-top:20px;'; }?>"> -->
                        <li class="media sub-coment test_five" style="<?php if($ikey == 0){ echo 'margin-top:20px;'; }?>">
                            <a href="javascript:void(0)" class="pull-left">
                                <img src="<?= $ivalue['profile_image']?$ivalue['profile_image']:"assets/front/images/person.jpg";?>" width="40px" height="40px" alt="" class="img-circle">
                            </a>
                            <!-- <div class="blog-media-body"> -->
                            <div class="media-body">
                                <small class="text-muted pull-right">
                                <?php echo get_time_posted(date('Y-m-d H:i:s'),$ivalue['comment_publish']);?></small>
                               
                                 <small class="text-muted ">
                                <?php if($this->session->userdata('front_user_type')==1||$this->session->userdata('front_user_type')==2){
                                    echo ucwords($ivalue['name']);
                                }
                                else{
                                    echo ucfirst(($ivalue['display_name'])?$ivalue['display_name']:$ivalue['name']);
                                }?>
                                </small>
                                <?php if($ivalue['comment_type'] == 2){ ?>
                                <p class="m-0"><?= wordwrap(ucfirst($ivalue['comment']),145," ",TRUE);?></p>
                                <?php } ?>
                                <div class="row">
                                    <?php 
                                    if($ivalue['comment_type'] == 1){
                                        $duration = date("i:s", strtotime($ivalue['duration']));
                                        $mediaType = $ivalue['media_type'];
                                        $mediaSlug = $ivalue['media_slug'];
                                        $views = $ivalue['views'];
                                        // $stance = $ivalue['stance'];
                                        $thumbImage = $ivalue['thumb_image'];
                                        $mediaImage = ($mediaType == 1) ? $thumbImage: "assets/images/slider_images/img7.jpg";
                                        // $isFav = ($ivalue['is_fav'] == 1)? "":"-o";
                                        $mediaTitle = $ivalue['title'];
                                        $mediaDescription = $ivalue['description'];
                                        $mediaIcon = ($mediaType == 1)? "link_icon": "audio_icon";
                                        // $subOutletName = ($ivalue['sub_outlet_name'])? $ivalue['sub_outlet_name']: $ivalue['other_sub_outlet'];
                                        if(!empty($mediaType)) { ?>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xxs-12 mrgT-20 mrgB-10">
                                                <div class="box_design audio_box">
                                                    <a href="video-fulldetail/<?= $mediaSlug; ?>">
                                                        <div class="video_cover_box">
                                                            <div class="hover_overlay"></div>
                                                                <span class='<?= $mediaIcon;?>'></span>
                                                                <div class="video_img"> 
                                                                    <img alt="" src="<?= $mediaImage; ?>" class="img-responsive">
                                                                <!-- <span class="time_srip"><?= $duration; ?></span>
                                                                <span class='favourite_icon'><i class='fa fa-star' aria-hidden='true'></i></span> -->
                                                                <h6 class="utitle"><?= $mediaTitle; ?></h6>
                                                                <div class="hover_discription"><p><?= $mediaDescription; ?></p></div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <!-- <div class="video_discription">
                                                        <ul>
                                                            <li class="play_detail"></li>
                                                            <li class="veiw_detail">
                                                                <span class="display_count"></span>  <br><span></span>
                                                            </li>
                                                        </ul>
                                                    </div> -->
                                                </div>
                                            </div>
                                        <?php  ?>    
                                    <?php 
                                        }
                                    } ?>

                                    <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 test_eight pb-3 pl-0">
                                        <?php if($ivalue['comment_like']){ $fa_class='fa fa-thumbs-up';}else{$fa_class='fa fa-thumbs-o-up'; }?>

                                    <div class="col-xs-12 test_eight">
                                       
                                        <?php if($ivalue['like_count']){ $fa_class='fa fa-thumbs-up';}else{$fa_class='fa fa-thumbs-o-up'; }?>

                                        <?php if($user_type==3 || $user_type==4){ ?>

                                         <span id="like_<?= $ivalue['id'];?>" <?php  if($ivalue['like_count']!=0){ ?> class="cursor btn <?php echo ($ivalue['comment_like']==1)?'clr':'';?>" <?php }else{?> class="cursor btn btn_design_light btn-small like_btn_design"<?php } ?> onclick="like_comment(<?= $ivalue['id'];?>);"><i class="<?php echo $fa_class;?>" aria-hidden="true"></i> <?= $ivalue['like_count'];?> </span>

                                        <?php } else {  ?>
                                            <span id="like_<?= $ivalue['id'];?>" class="nocursor btn btn_design_light btn-small 123132 like_btn_design"><i class="<?php echo $fa_class;?>" aria-hidden="true"></i> <?= $ivalue['like_count'];?></span>
                                        <?php } ?>

                                        <?php if($user_type==3 || $user_type==4){ ?>
                                            <a href="javascript:void(0);"  class="cursor btn btn_design btn-small like_btn_design" onclick="$('#reply_<?= $ivalue['id'];?>').slideToggle(500);"><i class="" aria-hidden="true"></i> Reply <?= $ivalue['reply_count']?$ivalue['reply_count']:"";?></a>
                                        <?php }else { ?>
                                            <?php if($user_type!=1 || $user_type!=2){ }else{?>

                                            <a href="javascript:void(0);"  class="cursor btn btn_design btn-small like_btn_design"><i class="" aria-hidden="true"></i> Reply <?= $ivalue['reply_count']?$ivalue['reply_count']:"";?></a>

                                        <?php } }?>
                                        <?php if($ivalue['reply_count'] > 0 && $ivalue['parent_id'] > 0){ ?>
                                        <a href="javascript:void(0);" class="cursor btn btn_design btn-small like_btn_design" onclick="loadMoreComments(<?= $ivalue['parent_id'];?>, 10, 0, 2, 0, '', 'main');"> Show Replies </a>
                                        <?php } ?>


                                        <?php if($user_type==3 || $user_type==4){ ?>
                                            <?php if($ivalue['report_count']){ ?>
                                                    <span id="report_<?= $ivalue['id'];?>" class="nocursor btn btn-small like_btn_design <?php echo ($ivalue['report_by']==1)?'clr':'';?>">
                                                    <i class="fa fa-flag" aria-hidden="true"></i> </span>
                                                <?php }else{ ?>
                                                    
                                                 <?php if($_SESSION['user_id']!=$ivalue['user_id']){ ?>
                                                 <span id="report_<?= $ivalue['id'];?>" class="btn btn_design_light btn-small like_btn_design" onclick="report_comment_modal(<?= $ivalue['id'];?>)" >
                                                <i class="fa fa-flag-o" aria-hidden="true"></i> </span>
                                
                                                <?php }?>
                                            <?php } ?>
                                        <?php } else{ ?>
                                            <?php if($user_type!=1 || $user_type!=2 ){ }else{?>
                                                <span id="report_<?= $ivalue['id'];?>" class="nocursor btn btn_design_light btn-small like_btn_design" >
                                                <i class="fa fa-flag" aria-hidden="true"></i></span>
                                        <?php } } ?>

                                        <?php if(!empty($userId)){ ?>
                                        <div class="detail_media comment_area" id="reply_<?= $ivalue['id'];?>" style="display: none;">
                                            <div class="blog-media-body test_six">
                                                <form method="post" name="comment_reply_<?= $ivalue['id'];?>">
                                                    <div class="comment_textarea">
                                                        <textarea class="form-control comment" placeholder="Write a comment" name="reply_comment" id="reply_text_<?= $ivalue['id'];?>" maxlength="5000"></textarea>
                                                        <p class="error" id="reply_error_<?= $ivalue['id'];?>"></p>
                                                    </div>
                                                     <!-- <a href="upload_media/<?php /*if(isset($ivalue['media_slug'])) echo $ivalue['media_slug'];*/ ?>/<?php // echo $ivalue['outlet_id'];?>/<?php //echo $ivalue['sub_outlet_id'];?>">Click here</a> to Post Video -->
                                                      <?php if($user_type !=4){?>
                                                      <a class="btn btn-outline-primary upload-video-comment" type="button" href="teacher-upload-video/<?php echo (isset($grade) && !empty($grade))?$grade:'0'; ?>/<?php echo $ivalue['media_id'];?>/<?php echo $ivalue['id']; ?> ">Click here to post video </a>
                                                   <?php }?>
                                                    <button class="btn btn-primary pull-right loader_btn" type="button" id="btn_<?php $ivalue['id'];?>" onclick="comment_reply(<?php echo $ivalue['id'];?>,<?php echo $ivalue['media_id'];?>)">Post</button>
                                                </form>
                                            </div>
                                        </div>
                                        <?php } ?>                    
                                    </div>
                                </div>
                            </div>
                        </li> <!-- test five div to li -->
                    <?php }
                    // echo "load_more=";
                    // print_r($value['load_more']);
                    // echo "repli=";
                    // print_r($value['reply']);
                    if(!empty($value['load_more']) && !empty($value['reply'])){ ?>
                    <div id="loadMore<?= $ivalue['parent_id'];?>">
                        <div class="detail_media load_more" id="load-more-<?= $ivalue['parent_id'];?>">
                            <a class="btn btn-primary" href="javascript:void(0)" onclick="loadMoreComments(<?= $ivalue['parent_id'];?>, 2, <?= $value['next_page'];?>, 0, 0, '', 'inner', 1);" class="btn btn-primary load-more-btn"> Load More</a>
                        </div>
                    </div>
                    <?php } ?>
                    
                    <?php 
                    // echo $value['parent_id'].'=='.$innerlimit.'=='.$load_more;
                    // if($key == 0 && !empty($value['parent_id']) && !empty($innerlimit) && empty($load_more)){
                    if(!empty($value['reply']) && !empty($value['parent_id']) && !empty($innerlimit) && empty($load_more)){
                      
                        echo '<span style="cursor:pointer;position: absolute;bottom: -50px; right: 0;" style="display:none" onclick="loadMoreComments('.$value['parent_id'].', 10, 0, 2, 0, \'previous\',\'main\');"><i class="fa fa-reply" aria-hidden="true"></i> Previous</span>';
                    }?>
                </div>
            </div> <!-- test_three_2 chnages div to li  -->
        </div> <!-- test_three chnages div to li  -->

    </li> <!-- test_one chnages div to li -->

<?php } ?>
</ul> <?php
if(!empty($load_more) && !empty($comments)){ ?>
    <div class="detail_media pt-4 load_more" id="load-more-<?= $parent_id;?>">
        <a class="btn btn-primary" href="javascript:void(0)" onclick="loadMoreComments(<?= $parent_id;?>, <?= $limit;?>, <?= $next_page;?>, <?= $innerlimit;?>, <?= $innerpage;?>, '', '<?= $type;?>', 1);" class="load-more-btn"> Load More</a></div>
<?php } ?>