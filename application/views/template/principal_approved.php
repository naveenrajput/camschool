<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <title></title>
</head>
<body>
    <table width="85%" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
            <tr>
                <td style="background:linear-gradient(90deg, #BD708C, #9A7093);height:40px; padding-left:15px; color:#ffffff;">
                    <strong>
                        <span class="il" style="color:#ffffff; font-family:Verdana"><font size="4"><?php echo WEBSITE_EMAIL_NAME; ?></font></span>
                    </strong>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="background:#f6f6f6;padding-left:20px;padding-top:20px;line-height:20px;">
                    <p style="font-family:Verdana">
                        <strong><font size="2"><?php if(!empty($name)) echo ucwords($name); ?>,</strong></font>
                    </p>
                </td>
            </tr>
            <tr>
                <td style="line-height:20px;"></td>
            </tr>
            <tr>
                <td colspan="2" style="background:#f6f6f6;padding:20px; line-height:20px;">
                   <p style="font-family:Verdana"><font  size="2"> Your district has activated your Chat At Me! Schools credentials. This program will enhance communication between your teachers and the school community. Please use the email and password you used when you created your account to access Chat At Me! Schools. <a href="<?php echo base_url().'login';?>">Click here</a> to login. You can also access the program through a mobile phone or iPad - visit the App Store or Google Play Store to download the app.
                    </font></p>

                   <p style="font-family:Verdana"><font  size="2">
                      After logging in, please review the user guide to get the most out of Chat At Me! Schools. The CAM! Schools User Guide can be found on the bottom of a website page or under Settings in the apps.   </font></p>

                    <p style="font-family:Verdana"><font  size="2">
                      Please email your teachers to register for Chat At Me! Schools via the website, <a href="https://chatatmeschools.com">https://chatatmeschools.com</a> or through one of the apps. For security purposes, when each teacher registers, you will receive a notification in the program to accept each teacher. Use the Contact Us form if you have any questions and we hope your teachers and parents will find Chat At Me! Schools very beneficial during these trying times.  </font></p>
                       
                       
                </td>
            </tr>
            <tr>
                <td style="line-height:20px;"></td>
            </tr>
            <?php if(isset($sub_msg) && !empty($sub_msg)){ ?>
            <tr>
                <td colspan="2" style="background:#f6f6f6;padding-left:20px; line-height:20px;">
                    <p style="font-family:Verdana"><font  size="2"><?php if(!empty($sub_msg)) echo $sub_msg; ?></font></p>
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td style="line-height:20px;"></td>
            </tr>
            <tr>
                <td colspan="2" style="background:#f6f6f6;padding:0px 0 10px 20px;line-height:20px;" >
                    <p style="font-family:Verdana">
                        <font size="2">
                            Best Regards,<br>
                            The <?php echo WEBSITE_EMAIL_NAME.' team';?> 
                        </font>
                    </p>
                </td>
            </tr>
            <tr>
                <td style="background:#c4c4c4;height:30px; padding-left:20px;">
                    <p style="font-family:Verdana">
                        <font size="2"><?php if(isset($note) && !empty($note)) echo $note; ?></font>
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
    </body>
</html>