<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <title></title>
</head>
<body>
    <table width="85%" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
            <tr>
                <td style="background:linear-gradient(90deg, #BD708C, #9A7093);height:40px; padding-left:15px; color:#ffffff;">
                    <strong>
                        <span class="il" style="color:#ffffff; font-family:Verdana"><font size="4"><?php echo WEBSITE_EMAIL_NAME; ?></font></span>
                    </strong>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="background:#f6f6f6;padding-left:20px;padding-top:20px;line-height:20px;">
                    <p style="font-family:Verdana">
                        <strong><font size="2"><?php if(!empty($name)) echo ucwords($name); ?>,</strong></font>
                    </p>
                </td>
            </tr>
            <tr>
                <td style="line-height:20px;"></td>
            </tr>
            <tr>
                <td colspan="2" style="background:#f6f6f6;padding:20px; line-height:20px;">
                   <p style="font-family:Verdana"><font  size="2"> Your principal has activated your Chat at Me! Schools credentials. This program was designed to help enhance communication between you and your classroom parents/caregivers. Please use the email and password you used when you created your account to access Chat At Me! Schools. <a href="<?php echo base_url().'login';?>">Click here</a> to login. You can also access the program through a mobile phone or iPad - visit the App Store or Google Play Store to download the app.
                    </font></p>

                   <p style="font-family:Verdana"><font  size="2">
                      After logging in, please review the user guide to get the most out of Chat At Me! Schools. The CAM! Schools User Guide can be found on the bottom of a website page or under Settings in the apps. </font></p>

                       
                </td>
            </tr>
            <tr>
                <td style="line-height:20px;"></td>
            </tr>
            <?php if(isset($sub_msg) && !empty($sub_msg)){ ?>
            <tr>
                <td colspan="2" style="background:#f6f6f6;padding-left:20px; line-height:20px;">
                    <p style="font-family:Verdana"><font  size="2"><?php if(!empty($sub_msg)) echo $sub_msg; ?></font></p>
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td style="line-height:20px;"></td>
            </tr>
            <tr>
                <td colspan="2" style="background:#f6f6f6;padding:0px 0 10px 20px;line-height:20px;" >
                    <p style="font-family:Verdana">
                        <font size="2">
                            Best Regards,<br>
                            The <?php echo WEBSITE_EMAIL_NAME.' team';?> 
                        </font>
                    </p>
                </td>
            </tr>
            <tr>
                <td style="background:#c4c4c4;height:30px; padding-left:20px;">
                    <p style="font-family:Verdana">
                        <font size="2"><?php if(isset($note) && !empty($note)) echo $note; ?></font>
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
    </body>
</html>