<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>User Verification </title>
</head>
<body>
	<table width="85%" cellspacing="0" cellpadding="0" border="0" align="center">
		<tbody>
			<tr>
				<td style="background:linear-gradient(90deg, #BD708C, #9A7093);height:40px; padding-left:20px; color:#ffffff;">
					<strong>
						<span class="il" style="color:#ffffff;font-family:Verdana"><font size="4"><?php echo WEBSITE_EMAIL_NAME; ?></font></span>
					</strong>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="background:#f6f6f6;padding-left:20px;padding-top:5px;padding-bottom:5px;line-height:20px;">
					<p style="font-family:Verdana">
						<strong><font size="2"><?php if(!empty($name)) echo ucwords($name); ?>,</font></strong>
					</p>
				</td>
			</tr>
			
			<tr>
				<td colspan="2" style="background:#f6f6f6;padding-left:20px; padding-bottom:10px;line-height:20px;">
					<p style="font-family:Verdana">
						<font size="2">Click on the verify below to complete your registration for 'Web' application.</font>
					</p>
					<p style="font-family:Verdana">
						<a style="font-size: 13px;background: linear-gradient(90deg, #BD708C, #9A7093);color: #fff;text-decoration: none;padding: 5px;margin: 2px 0 0 0;display: table;" href="<?php echo $verification_url; ?>" target="_blank">Verify</a>
					</p>
					<p style="font-family:Verdana;text-align:left;">Or,</p>

					<p style="font-family:Verdana">
						<font size="2">Use the verification code for 'Mobile' application.</font>
					</p>
					<p style="font-family:Verdana">
						<font size="2">Verification Code: <b><?php if(!empty($verify_code)) echo $verify_code; ?></b></font>
					</p>
				</td>
			</tr>
			
			<tr>
				<td style="background:#f6f6f6;padding:20px;padding-top:0px;line-height:20px;" colspan="2" height="10" >
					<p style="font-family:Verdana">
                        <font  size="2">Please contact us if you need any assistance. </font>
                    </p>
                    <p style="font-family:Verdana">
                        <font size="2">
                            Best Regards,<br>
                            The <?php echo WEBSITE_EMAIL_NAME.' team';?> 
                        </font>
                    </p>
				
				</td>
			</tr>
			<tr>
				<td style="background:#c4c4c4;height:30px; padding-left:20px;">
					<b>Note :</b> 
					<font size="2">The above verification is only valid for next 12 hours.</font>
				</td>
			</tr>
		</tbody>
	</table>
	</body>
</html>