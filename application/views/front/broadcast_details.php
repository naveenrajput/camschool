
<section class="content">
    <div class="container">
        <div class="row">
            <?php include APPPATH.'views/front/include/sidebar.php'; ?>
            <div class="col-md-12">
                <div class="main-body">
                    <div class="content-header justify-content-between align-self-center">
                        <h2 class="company-name"><?php if(isset($broadcast['subject'])) echo wordwrap(ucfirst($broadcast['subject']),40,"<br>\n",TRUE) ?></h2>    
                        <span><?php if(isset($broadcast['created'])) echo convertGMTToLocalTimezone($broadcast['created'],$showTime=true, $only_time=false,"", $fulltime =TRUE); ?></span>    
                    </div>
                    <div class="content-body">
                        <span><?php if(isset($broadcast['message']))  echo wordwrap(ucfirst($broadcast['message']),40,"<br>\n",TRUE)?></span>
                      

                        <hr>
                        <h6>Recipient</h6>
                        <ul class="teachers-list">
                            <?php
                                if(isset($broadcast_email) && $broadcast_email){
                                    foreach ($broadcast_email as $key => $mail_list) { ?>
                                     <li><span class="badge badge-light"><?php echo ucfirst($mail_list['email']); ?></span></li>
                            <?php  }
                                }
                            ?>
                            
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){
  $('[data-toggle="popover"]').popover();   
});
</script>