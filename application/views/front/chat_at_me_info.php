<section class="content">
    <div class="container">
        <div class="row">            
            <?php if(isset($is_sidebar)) { include APPPATH.'views/front/include/sidebar.php';}  ?>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="main-body">
                    <div class="content-header">
                        <h2 class="company-name"> <?php if(!empty($details['title'])) { echo $details['title'];}?> </h2>
                        <?php if(isset($print)) { ?>
                        <button type="button" class="btn notfication-btn" onclick="PrintElem()"><i class="fa fa-print"></i></button>
                        <?php } ?>
                    </div>
                    <div class="content-body tutor-filter" id="contprint">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <?php if(!empty($details['content'])) { echo $details['content'];}else{ echo 'Not available';}?>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<!-- <div class="spacer20"></div>
<div class="spacer20"></div>
 -->
  <?php if(isset($print)) { ?>
<script src="assets/front/js/jquery.min.js"></script> 
<script type="text/javascript">
    function PrintElem() {
        var contents = document.getElementById("contprint").innerHTML;
        var frame1 = document.createElement('iframe');
        frame1.name = "frame15435";
        frame1.style.position = "absolute";
        frame1.style.top = "-1000000px";
        document.body.appendChild(frame1);
        var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
        frameDoc.document.open();
        frameDoc.document.write('<html><head><title>' + document.title  + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(contents);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame15435"].focus();
            window.frames["frame15435"].print();
            document.body.removeChild(frame1);
        }, 500);
        return false;
    }
    function PrintElem12()
    {
        var divContents = document.getElementById("contprint").innerHTML; 
        var mywindow = window.open('', 'PRINT', 'height=600,width=1024');

        mywindow.document.write('<html><head><title>' + document.title  + '</title>');
        mywindow.document.write('</head><body >');
        // mywindow.document.write('<h1>' + document.title  + '</h1>');
        mywindow.document.write(divContents);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10*/

        // mywindow.print();
        if (!mywindow.document.execCommand('print', false, null)) {
            // document.execCommand returns false if it failed -http://stackoverflow.com/a/21336448/937891
            mywindow.print();
        }
        setTimeout( function () { 
            mywindow.close();
        }, 200);
        return true;
    }
 </script>
 <?php } ?>