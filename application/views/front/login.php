<!-- <style type="text/css">
    #pass-status {
    position: absolute;
    top: 10px;
    right: 30px;
}
</style> -->
    <section class="content signup-page login-page">
        <div class="col-sm-6 col-md-4 right-form">
            <p class="alert_message" id="msg" style="display:none;"></p>
           
            <?php if ($this->session->flashdata('error')) { ?>
                <div class="alert alert-block alert-danger rmvflash">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <?php echo $this->session->flashdata('error') ?>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('success')) { ?>
                <div class="alert alert-block alert-success rmvflash">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?php echo $this->session->flashdata('success') ?>
            </div>
            <?php } ?>
            <form class="login-form" method="POST" id="login_form" action="" role="form" data-parsley-validate>

                <div class="row">
                    <div class="spacer20"></div>
                    <div class="col-md-12 col-sm-12 form-group">
                        <!-- <label>Email / Username</label> -->
                        <input class="form-control" id="email" placeholder="Email Address" type="text" data-parsley-required data-parsley-required-message="Enter your email address" autocomplete="off"  data-parsley-type="email" data-parsley-type-message="Enter valid email address" value="<?php if($this->input->cookie('username', TRUE)) { echo $this->input->cookie('username');}?>">
                    </div>
                    <div class="col-md-12 col-sm-12 form-group">
                        <!-- <label>Password</label> -->
                        <input class="form-control" id="login_password" placeholder="Password" type="password" data-parsley-required data-parsley-required-message="Enter your password" value="" autocomplete="off" />
                         <i id="pass-status" class="fa fa-eye-slash pass-eye" aria-hidden="true" onClick="viewPassword()"></i>
                    </div>
                    <div class="checkbox res-align col-md-12">
                        <label>
                            <input type='checkbox' name='rememberme' id='rememberme' <?php if(isset($_COOKIE["username"])) { ?> checked <?php } ?>> Remember Me
                        </label>
                         <a href="forgot-password" class="link-btn">Forgot Password?</a>
                    </div>
                    <div class="col-sm-12 text-center">
                        <!-- <a href="javascript:void(0);" class="btn btn-primary loader_btn" onclick="login_submit();">Sign In</a> -->
                        <button onclick="login_submit()" class="btn btn-primary loader_btn" type="submit">Sign In</button>

                    </div>
                    <h6 class="text-center col-sm-12">Don’t have an account yet? <a href="signup" class="link-btn">Sign up.</a></h6>
                </div>
            </form>
        </div>
    </section>

<script>
function login_submit() {
    $('#login_form').parsley().validate();
     event.preventDefault();

        var email=$("#email").val();
       
        var login_password=$("#login_password").val();
        var rememberme = $("#rememberme").is(":checked");
        if(rememberme) {
            rememberme=1;
        }else{
            rememberme=0;
        }
        $(".rmvflash").remove();
        // $('#login_form').parsley().validate();
        if ($('#login_form').parsley().isValid()) {
            $('.loader_btn').html('<span class="   fa fa-spinner fa-spin fa-lg mr-2" role="status" aria-hidden="true"></span>Loading...').attr('disabled', true);
            $.ajax({
                type: "POST",
                async: true,
                url: "<?php echo base_url(); ?>api/login",
                data: {email:email,password:login_password,rememberme:rememberme,device_type:'web'},
                headers: { 'apikey': '<?php echo APP_KEY ?>' },
                success: function(response) { 

                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    var res = JSON.parse(response);
                   
                    if(res.status==1) {

                        var user_type=res['details']['user_type'];
                        var is_approve=res['details']['is_approve'];
                        var email_verified=res['details']['email_verified'];
                        var user_status=res['details']['status'];
                        var is_redirect_change_password=res['details']['is_redirect_change_password'];
                        var is_profile_complete=res['details']['is_profile_complete'];
                        var is_school_add=res['details']['is_school_add'];
                        var is_student_add=res['details']['is_student_add'];
                       
                        if(email_verified==2){
                            $('#msg').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+res.msg+'</div>');
                            $('#msg').css('display','block');
                             $('.loader_btn').html('Sign In').attr('disabled', false);

                            setTimeout(function() {
                                $('#msg').fadeOut('slow');
                            }, 3000); 
                            return false;
                        }
                        
                        if(user_type ==1){
                            if(user_status==1 ){
                                window.location ="<?php echo base_url();?>school-videos";
                            }else{
                                window.location= "<?php echo base_url();?>change-password";
                            }
                        }
                       else if(user_type ==2){
                            if(user_status!=1){
                                window.location= "<?php echo base_url();?>change-password";
                            }else if(is_profile_complete ==0 || is_school_add ==0){
                                window.location ="<?php echo base_url();?>principal/profile";
                            }else{
                                window.location ="<?php echo base_url();?>teacher-video";
                            }
                        }
                        else if(user_type ==3){
                            if(user_status!=1){
                                window.location.href ="<?php echo base_url();?>change-password";
                            }else if(is_profile_complete ==0){
                                window.location ="<?php echo base_url();?>teacher/profile";
                            }else{
                                window.location=  "<?php echo base_url();?>teacher-index";
                            }
                        }
                        else if(user_type ==4){
                            if(is_student_add==1){
                                window.location= "<?php echo base_url();?>parent/index";
                            }else{
                                window.location ="<?php echo base_url();?>parent/add-student";
                            }
                        }else{
                           alert();
                        }
                        $('.loader_btn').html('Sign In').attr('disabled', false); 
                    } else {
                        
                        $('.loader_btn').html('Sign In').attr('disabled', false);
                        $('#msg').html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+res.msg+'</div>');
                        $('#msg').css('display','block');
                        setTimeout(function() {
                            $('#msg').fadeOut('slow');
                        }, 3000); 
                    }
                },
                error: function(xhr, status, error) {
                 
                }
            });
        } else {
            /* $('.loader_btn').html('Sign In').attr('disabled', false);
                        $('#msg').html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Enter your email address and password</div>');
                        $('#msg').css('display','block');
                        setTimeout(function() {
                            $('#msg').fadeOut('slow');
                        }, 3000); */
            return false;
        }
        
    }
    /*$('#login_password,#email').keydown(function (e){
        if(e.keyCode == 13){
            //$("#login_submit").click();
            login_submit();
            return false; 
        }
    });*/
</script>
<script>
function viewPassword()
{
  var passwordInput = document.getElementById('login_password');
  var passStatus = document.getElementById('pass-status');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye-slash';
  }
}
</script>