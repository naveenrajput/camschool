<section class="content">
    <div class="container">
        <div class="row">
            <?php if(isset($is_sidebar)) { include APPPATH.'views/front/include/sidebar.php';}  ?>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="main-body">
                    <?php if(!empty($details['title'])) { ?>
                    <div class="content-header">
                        <h2 class="company-name"> <?php if(!empty($details['title'])) { echo $details['title'];}?> </h2>
                    </div>
                    <?php } ?>
                    <div class="content-body tutor-filter">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <?php if(!empty($details['content'])) { echo $details['content'];}else{ echo 'Not available';}?>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- <div class="spacer20"></div>
<div class="spacer20"></div>
 -->