<?php  $user_id =  $this->session->userdata('user_id'); ?>

<style type="text/css">
    .select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #e4e4e4;
    border: 1px solid #aaa;
    border-radius: 4px;
    display: inline-block;
    /* margin-left: 5px; */
    /* margin-top: 5px; */
    padding: 0;
    margin: 5px;
}
.select2-container--default .select2-selection--multiple .select2-selection__choice__display {
    cursor: default;
    padding-left: 2px;
    padding-right: 5px;
    word-break: break-word;
}
</style>


<section class="content">
    <div class="container">
        <div class="row">            
             <?php include APPPATH.'views/front/include/sidebar.php';  ?>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="main-body">
                 <p class="alert_message" id="msg" style="display:none;"></p>
                    <div class="content-header">
                        <h2 class="company-name">Student Profile</h2>
                    </div>
                    <div class="content-body">
                     
                        <div class="row">
                        <div class="col-md-12 col-sm-12 saprator mt-4 mb-4"></div>
                       
                        <div class="col-md-12">
                         <div id="success_div"></div>

                         <div class="alert alert-success alert_message" id="delete_div" style="display:none;"><button data-dismiss="alert" class="close" type="button">×</button>The teacher was successfully removed.</div>
                                <form id="student_form" action="" role="form" data-parsley-validate enctype="multipart/form-data">
                                    <!-- <lable class="">Add a Student</lable> -->
                                    <div class="row mt-3">
                                        <!-- <div class="col-md-3 col-sm-6 form-group">
                                            <input type="text" class="form-control" name="name" id="name" placeholder="Full Name" data-parsley-required  data-parsley-required-message="Enter child’s full name" maxlength="30">
                                        </div> -->
                                        <div class="col-md-3 col-sm-6 form-group">
                                            <select class="form-control select2" id="grade" name= "grade[]" data-parsley-required data-parsley-errors-container="#errorGrade" data-parsley-required-message="Select a grade" multiple="multiple">
                                                <option  value="">Select Grade</option>
                                                <?php 
                                                /*if(!empty(SCHOOL_GRADE)){
                                                    foreach(SCHOOL_GRADE as $school_grade){ ?>
                                                    <option  value="<?php echo $school_grade['grade'];?>"><?php echo ucfirst($school_grade['grade_display_name']);?></option>
                                                <?php } }*/ ?>

                                                <?php 
                                                if(!empty($school_grades)){
                                                    foreach($school_grades as $school_grade){ ?>
                                                    <option  value="<?php echo $school_grade['grade'];?>"><?php echo ucfirst($school_grade['grade_display_name']);?></option>
                                                <?php } } ?>
                                                                    

                                            </select>
                                            <span id="errorGrade"></span>
                                        </div>
                                        <div class="col-md-3 col-sm-6 form-group">
                                            <select class="form-control select3" id="teacher" name= "teacher[]" data-parsley-required data-parsley-errors-container="#errorTeacher" data-parsley-required-message="Select a teacher" multiple="multiple">
                                                <option  value="" >Select Teacher</option>
                                            </select>
                                            <span id="errorTeacher"></span>
                                        </div>
                                        <div class="col-md-3 col-sm-6 form-group" >
                                            <?php if ($is_adable == 1) { ?>
                                                <button style="margin-left: 25px;" type="button" class="btn btn-primary add-row" id="stu_add_btn" data-toggle="modal" data-target="#confirmation-student" >Add</button>
                                            <?php } else { ?>
                                                <button style="margin-left: 25px;" disabled="" type="button" class="btn btn-primary">Add</button>
                                                <!-- <a style="margin-left: 25px;" class="btn btn-primary" disabled="true" >Add</button> -->
                                            <?php } ?>
                                        </div>
                                    </div>
                                </form>
                                <table class="table table-bordered table-hover child-list table-responsive-sm table-responsive-md">
                                    <thead>
                                        <tr>
                                            <!-- <th width="18%"> 
                                               <span>Name</span>
                                            </th> -->
                                           <th width="82%"> 
                                                <span>Teacher</span>
                                            </th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                         // echo "<pre>";print_r($get_student_list);exit;
                                        foreach ($get_student_list as $key => $student_list) { ?>
                                            <tr>
                                                <!-- <td>
                                                    <h6> 
                                                        <?php // echo wordwrap(ucfirst($student_list['student_name']),15,"<br>\n",TRUE); ?>
                                                    </h6>
                                                </td> -->
                                             <?php /* <td>
                                                <ul class="grade-ul">
                                                <?php
                                                foreach($student_list['grade_list'] as $grade_list
                                                ){
                                               
                                                ?>
                                                <li><?php echo DISPLAY_GRADE_NAME[$grade_list['grade']]; ?></li>
                                                <?php } ?>
                                                    </ul>
                                                </td> */?>
                                                <td>
                                                    <?php
                                                    if(empty($student_list['grade_list'])){
                                                        echo "Teacher not available";
                                                    }else{
                                                        foreach($student_list['grade_list'] as $grade_list){
                                                            $teacher_grade_id = $this->Common_model->getRecords('student_grade','teacher_grade_id,is_approve',array('student_id' => $student_list['id'],'grade'=>$grade_list['grade'])); ?>
                                                            <span><?php echo DISPLAY_GRADE_NAME[$grade_list['grade']]; ?></span>
                                                            <ul class="grade-ul">
                                                                <?php foreach ($teacher_grade_id as $key => $teacher_grade_ids) { ?>
                                                                    <li>
                                                                        <?php $teacher_name = $this->Parent_student_model->getTeacherName($teacher_grade_ids['teacher_grade_id']);
                                                                        echo ($teacher_name['name'])?$teacher_name['name']:'Teacher not available';
                                                                        if($teacher_name){ ?>
                                                                            -
                                                                            <?php if($teacher_grade_ids['is_approve']==1){
                                                                                    echo "Pending";
                                                                                }else if($teacher_grade_ids['is_approve']==2){
                                                                                    echo "Accepted";
                                                                                }else if($teacher_grade_ids['is_approve']==3){
                                                                                    echo "Rejected";
                                                                                }
                                                                            ?>
                                                                            <a href="javascript:void(0)" onclick="deleteStudent(<?php echo $user_id ?>,<?php echo $student_list['id'] ?>,<?php echo $teacher_grade_ids['teacher_grade_id'] ?>)"><i class="fa fa-times"></i></a> 
                                                                        <?php } ?>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        <?php } ?>
                                                    <?php  } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- <div class="spacer20"></div>
<div class="spacer20"></div>
 -->
 <style type="text/css">
    .grade-ul li{
     list-style: disc;
    }
 </style>
<script>
     $(document).ready(function() {
            $('.select2').select2({
                 placeholder: 'Select Grade',
                 multiple:true,
            });
            $('.select3').select2({
                 placeholder: 'Select Teacher',
                 multiple:true,
            });
        });
    function renderImage(seq) {
        $("#img_err").html('');
        //$("#loader-wrapper").show();
        var file = event.target.files[0];
        var fileReader = new FileReader();
        if(!file.type.match('image')) {
            $("#loader-wrapper").hide();
            $("#fileUpload_"+seq+"").val('');
            $("#img_err").html("File must be image");
            return false;
        }
        if (file.type.match('image')) {
            var FileExt = file.name.substr(file.name.lastIndexOf('.') + 1);
            if ((FileExt.toUpperCase() != "JPG" && FileExt.toUpperCase() != "JPEG" && FileExt.toUpperCase() != "PNG")) {
                    $("#loader-wrapper").hide();
                    $("#fileUpload_"+seq+"").val('');
                    $("#img_err").html("File must be jpg or jpeg or png");
                    return false;
            }
            fileReader.onload = function() {
              $("#img_src_"+seq+"").attr('src', fileReader.result);
              $("#loader-wrapper").hide();
            };
            fileReader.readAsDataURL(file);
        } 
    }
</script>

<script>
$("#grade").change(function(){
    var grade = $("#grade").val();
    var grade = grade.toString();
    $("#teacher").empty();
    $("#teacher").attr("disabled", true );
    var school_id = "<?php echo  $this->session->userdata('school_id'); ?>";
    var user_id = "<?php echo  $this->session->userdata('user_id'); ?>";

     $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>api/teacher-by-grade",
            data: {school_id:school_id,user_id:user_id,grade:grade,type:'web'},
            headers: { 'apikey': '<?php echo APP_KEY ?>' },
            success: function(response) { 
                var res = JSON.parse(response);
                if(response.status==4){
                    location.reload();
                } 

                $("#teacher").attr("disabled", false );
                if(res.details){
                    var all_data = res.details.grade;
                    for(var i=0;i<all_data.length;i++){
                        var teacher = all_data[i]['teacher'];
                        $('#teacher').append(`<optgroup label="${all_data[i]['grade_name']}">`);  
                        for(var j=0;j<teacher.length;j++){
                            if(teacher[j]['display_name']!=""){
                                $('#teacher').append(`<option value="${teacher[j]['teacher_grade_id']}"> ${teacher[j]['display_name']}  </option>`);
                            }else{
                                $('#teacher').append(`<option value="${teacher[j]['teacher_grade_id']}"> 
                                ${teacher[j]['first_name'] +" "+teacher[j]['last_name']}  </option>`);
                            }
                             
                        }
                    }
                }
               
            },error: function(){
                
            }
        });

}); 

function deleteStudent(user_id,student_id,teacher_grade_id) {
    swal({
        title: "Confirmation",
        text: "You are attempting to delete a teacher. Would you like to continue?",
        //icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: true,
       // buttonsColor: "#DD6B55",
        dangerMode: true,
       
       buttons: {
            dance : {
              text : "No",
              className : 'btn btn-default add-row'
            },
            confirm: {
              text : 'Yes',
              className : 'btn btn-primary add-row'
            }
          }
        })
    .then((willDelete) => {
        if(willDelete==true){
          $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>api/request-canceled-by-student",
                data: {type:'web',user_id:user_id,student_id:student_id,teacher_grade_id:teacher_grade_id},
                headers: { 'apikey': '<?php echo APP_KEY ?>' },
                success: function(response) {     
                   $("#loader-wrapper").hide();
                      var res = JSON.parse(response);
                      
                      $('#delete_div').attr('style', 'display:block;');
                     
                      $("#msg").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+res.msg+'</div>');
                      $(".content-body").animate({ scrollTop: 0 }, "fast");

                        setTimeout(function() { 
                          $("#msg").html('');
                          
                          $("#msg").hide();
                          location.reload();
                        }, 1000);
                },error: function(){
                     $("#loader-wrapper").hide();
                    $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Some error occured in loading the data.</div>');
                    $('#msg').css('display','block');
                        setTimeout(function() {
                        $('#msg').fadeOut('slow');
                    }, 3000); 
                }
            });
        }
    });
}

/*function deleteStudent(user_id,student_id,teacher_grade_id){
        if(student_id != "" && teacher_grade_id && user_id){
            $("#loader-wrapper").show();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>api/request-canceled-by-student",
                data: {type:'web',user_id:user_id,student_id:student_id,teacher_grade_id:teacher_grade_id},
                headers: { 'apikey': '<?php echo APP_KEY ?>' },
                success: function(response) {     
                   $("#loader-wrapper").hide();
                      var res = JSON.parse(response);
                      
                      $('#delete_div').attr('style', 'display:block;');
                     
                      $("#msg").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+res.msg+'</div>');
                      $(".content-body").animate({ scrollTop: 0 }, "fast");

                        setTimeout(function() { 
                          $("#msg").html('');
                          
                          $("#msg").hide();
                          location.reload();
                        }, 1000);
                },error: function(){
                     $("#loader-wrapper").hide();
                    $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Some error occured in loading the data.</div>');
                    $('#msg').css('display','block');
                        setTimeout(function() {
                        $('#msg').fadeOut('slow');
                    }, 3000); 
                }
            });
        }
    }*/
</script>
