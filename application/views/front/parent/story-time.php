

<style type="text/css">
.story-line img
{
height: 150px;
display: flex;
justify-content: center;
align-content: center;
align-items: center;
margin: auto; 
}
</style>
<section class="content">
    <div class="container">
        <div class="row">            
            <?php include APPPATH.'views/front/include/sidebar.php'; ?>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="main-body">
                    <div class="content-header">
                        <h2 class="company-name">Story Time</h2>
                    </div>
                    <div class="content-body tutor-filter">
                        <div class="row">
                           
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <span class="f-14">
                                    Being read to is fundamental to a child’s development. Unfortunately, many students lost classroom story time due to virtual or hybrid learning. The <a href="https://sagaftra.foundation/childrens-literacy/storyline-online/" target="_blank">SAG-AFTRA non-profit foundation</a> created an incredible resource for children (and parents & caregivers) - Storyline Online®. 
                                </span>
                                <div class="spacer-20"></div>
                                <br>
                                <span class="f-14">
                                    This children’s literacy website has numerous videos of books being read by actors and TV personalities. 
                                    The Storyline Online® books also have additional resources created by credentialed educators to help English-language learners. <?php echo WEBSITE_NAME;?> is proud to financially support Storyline Online® and hope children as well as adults will spend many hours enjoying the wonderful storytelling. 
                                </span>
                            </div>
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <!-- <h5>Wednesday’s Lesson</h5> -->
                                <div class="story-line mt-5">    
                                        <a href="https://www.storylineonline.net/library/" target="_blank"><img src="assets/front/images/img_d.png" class="img-fluid" alt="storytime"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- <div class="spacer20"></div>
<div class="spacer20"></div>
 -->