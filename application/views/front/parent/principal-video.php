<?php
$user_id=$this->session->userdata('user_id');
$user_type=$this->session->userdata('front_user_type');
$school_id=$this->session->userdata('school_id');

?>
    <section class="content">
        <div class="container">
        <div class="row">
            
           <?php include APPPATH.'views/front/include/sidebar.php';  ?>
            
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="main-body">
                    <div  id="video_content">
                    </div> 
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
    </div>
</section>

<script>
    $( document ).ready(function() {
        $("#loader-wrapper").show();
        var user_id = "<?php echo $user_id; ?>";
        var user_type = "<?php echo $user_type; ?>";
        var school_id = "<?php echo $school_id; ?>";
        var media_id = "<?php echo $media_id; ?>";
         $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>api/video-details",
            data: {type:'web',user_id:user_id,call_type:'school',school_id:school_id,media_id:media_id},
            headers: { 'apikey': '<?php echo APP_KEY ?>' },
            success: function(response) { 
                $("#loader-wrapper").hide();
                if(response.status==4){
                    location.reload();
                } 
                $("#video_content").html(response);
            },error: function(){
                $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Some error occured in loading the data.</div>');
                $('#msg').css('display','block');
                    setTimeout(function() {
                    $('#msg').fadeOut('slow');
                }, 3000); 
            }
        });
    });
</script>


