<?php include 'header.php';?>

<section class="content">
    <div class="container">
        <div class="row">
        <?php include 'sidebar.php';?>  
            <div class="col-md-12">
                <div class="main-body">
                    <div class="content-header">
                        <h2 class="company-name">Notifications</h2>
                    </div>
                    <div class="content-body">
                        <div class="media">
                          <img class="d-flex mr-3" width="40" height="40" src="assets/images/user-profile.png" alt="image">
                          <div class="media-body">
                            <h6 class="mt-0">A new video has been uploaded under the “First Grade”.</h6>
                            <span class="f-14">Jun 27, 2018</span>
                          </div>
                        </div>
                        <hr>
                        <div class="media">
                          <img class="d-flex mr-3" width="40" height="40" src="assets/images/user-profile.png" alt="image">
                          <div class="media-body">
                            <h6 class="mt-0">You have started following Ms. Maria.</h6>
                            <span class="f-14">23 hours ago</span>
                          </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>

    <?php include 'footer.php';?>