<?php //echo "<pre>";print_r($details); ?>
<div class="content-header">
    <h2 class="company-name"><?php echo $details['first_name']." ".$details['last_name']; ?></h2>
</div>
<div class="content-body video-detail">
    <div class="profile-block">
        <div class="row">
            <div class="video-box col-sm-12">
             <!-- <video width="100%" height="100%" id="my_video" poster="assets/front/images/transparent.png" controls> -->
                <video width="100%" height="400" id="my_video" poster="<?= $details['large_image']; ?>" controls>
                <source src="<?= $details['media_name']; ?>"> </video>
                <span><?php echo $details['duration']; ?>
                    <i class="fa fa-play"></i>
                </span>
            </div>
            <div class="video-content col-sm-12">
                <h3>Principal's Video</h3>
                <p><?php echo $details['description'] ?></p>
            </div>
        </div>

    </div>
    <!-- //profile block -->
</div>

<style type="text/css">
    /*video {
        width: 100%;
        height: 400px;
        background:transparent url('<?= $details['large_image']; ?>') no-repeat 0 0;
        -webkit-background-size:cover;
        -moz-background-size:cover;
        -o-background-size:cover;
        background-size:cover;
    }*/
</style>
<script>
$(document).ready(function(){
var vid = document.getElementById("my_video");
  $("#my_video").on("timeupdate",function(event){
        vid.onpause = function() {
            
        };
    });
});
</script>

     
