
<section class="content">
    <div class="container">
        <div class="row">
            <?php include APPPATH.'views/front/include/sidebar.php'; ?>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="main-body">
                    <div class="content-header d-flex flex-wrap justify-content-between">
                        <h2 class="company-name mt-2 mb-2"> Continue Watching Video</h2>
                    </div>

                    <div class="content-body">
                        <div class="timeline">
                            <div class="row">
                            <?php
                                if(isset($continue_video) && $continue_video){
                                    foreach ($continue_video as $key => $continues_video) {?>
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                         <a href="video-fulldetail/<?php echo $continues_video['media_slug']; ?>">
                                            <div class="card timeline">
                                                <div class="video-block">
                                                    <img class="card-img-top" src="<?php echo $continues_video['thumb_image']; ?>">
                                                    <span><?php echo $continues_video['duration']; ?>
                                                        <i class="fa fa-play"></i>
                                                    </span>
                                                </div>
                                                <div class="card-block">
                                                    <div class="card-text">
                                                        <h3><?php echo mb_strimwidth($continues_video['title']?$continues_video['title']:"", 0, 20, "..."); ?></h3>
                                                    </div>
                                                    <figure class="profile profile-inline">
                                                        <img src="<?php echo $continues_video['profile_image']; ?>" class="profile-avatar" alt="">
                                                    </figure>
                                                    <h3 class="card-title w-70"><?php echo $continues_video['first_name']." ".$continues_video['last_name']; ?>
                                                        <span class="f-12">- <?php echo $continues_video['grade_display_name']; ?> </span>
                                                        <br>
                                                        <span class="f-11"><?php echo date("M d, Y", strtotime($continues_video['created'])) ; ?></span>
                                                    </h3>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                            <?php }
                                }
                            ?>
                            

                            <!-- video block -->
                            
                        </div>
                            

                            <!-- video block -->
                        </div>
                        </div>

                       <!--  <div class="col-md-12 text-center">
                            <a href="#!" class="btn btn-primary">See All</a>
                        </div> -->

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- <div class="spacer20"></div>
<div class="spacer20"></div>
 -->
