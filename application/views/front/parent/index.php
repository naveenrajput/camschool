<?php //echo "<pre>";print_r($continue_video_count);die; ?>
<section class="content">
    <div class="container">
        <div class="row">
        <?php include APPPATH.'views/front/include/sidebar.php';  ?>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="main-body">
                <?php if(empty($school_video[0]['name'])){?>
                    <div class="content-header">
                            <h2 class="company-name"><?php if(isset($school)){  echo wordwrap(ucfirst($school->name),40,"<br>\n",TRUE); } ?></h2>
                             <h4 class="company-address"><?php if(isset($school)){ echo wordwrap(ucfirst($school->address),40,"<br>\n",TRUE);  } ?></h4>
                    </div>
                <?php }else{ ?>
                <div class="content-header d-flex flex-wrap justify-content-between">
                    <h2 class="company-name"><?php if(isset($school_video[0]['name'])){ echo ucfirst($school_video[0]['name'])?wordwrap(ucfirst($school_video[0]['name']),40,"<br>\n",TRUE):""; } ?></h2>
                    <h4 class="company-address"><?php if(isset($school_video[0]['address'])){  echo ucfirst($school_video[0]['address'])?wordwrap(ucfirst($school_video[0]['address']),40,"<br>\n",TRUE):""; } ?></h4>
                </div>
                <?php }?>

                <div class="content-body">
                <?php if(isset($school_video[0])) { ?>
                    <div class="profile-block">
                        <div class="row">
                                <div class="video-box col-lg-5 col-md-6 col-sm-6 col-xs-12">
                                    <a href="<?php echo base_url()."principal-video/".$school_video[0]['id'] ?>">
                                        <img class="img-video" src="<?php echo $school_video[0]['thumb_image']?$school_video[0]['thumb_image']:"assets/front/images/video_thumb.jpg"; ?>" />
                                        <span><?php echo $school_video[0]['duration'] ?>
                                            <i class="fa fa-play"></i>
                                        </span>
                                    </a>
                                </div>
                                <div class="video-content col-lg-7 col-md-6 col-sm-6 col-xs-12">
                                    <h3><?php echo ucfirst($school_video[0]['display_name'])?wordwrap(ucfirst($school_video[0]['display_name']),30,"<br>\n",TRUE):wordwrap(ucfirst($school_video[0]['first_name']." ".$school_video[0]['last_name']),40,"<br>\n",TRUE); ?></h3>
                                    <h6 class="mt-2">Principal's Message</h6>
                                   
                                    <p><b><?php echo ucfirst($school_video[0]['title'])?ucfirst($school_video[0]['title']):""; ?></b>:  
                                       <?php echo ucfirst($school_video[0]['description'])?ucfirst($school_video[0]['description']):"" ?>
                                    </p>
                                </div>
                        </div>
                    </div>
                      <?php } ?>
                    <!-- //profile block -->

                    <?php
                        if(isset($continue_video) && count($continue_video)){?>
                    <div class="timeline mb-3">
                        <div class="playlist-section">
                            <div class="playlist-header">
                                <h3>Continue Watching</h3>
                                <?php
                                    if(count($continue_video_count) > 3){ ?>

                                    <a href="<?php echo base_url(); ?>parent/continue-video" class="see-more">View All</a>
                                <?php }
                                ?>
                            </div>
                        </div>

                        <div class="row row-ff">
                            <?php
                                if(isset($continue_video) && $continue_video){
                                    foreach ($continue_video as $key => $continues_video) {?>
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                         <a href="video-fulldetail/<?php echo $continues_video['media_slug']; ?>">
                                            <div class="card timeline">
                                                <div class="video-block">
                                                    <img class="card-img-top" src="<?php echo $continues_video['thumb_image']?$continues_video['thumb_image']:"assets/front/images/video_thumb.jpg"; ?>">
                                                    <span><?php echo $continues_video['duration']; ?>
                                                        <i class="fa fa-play"></i>
                                                    </span>
                                                </div>
                                                <div class="card-block">
                                                    <div class="card-text">
                                                        <h3><?php echo mb_strimwidth(ucfirst($continues_video['title'])?ucfirst($continues_video['title']):"", 0, 28, "..."); ?></h3>
                                                    </div>
                                                    <figure class="profile profile-inline">
                                                        <img src="<?php echo $continues_video['profile_image']?$continues_video['profile_image']:"assets/front/images/person.jpg"; ?>" class="profile-avatar" alt="">
                                                    </figure>
                                                    <h3 class="card-title w-70">
                                                        <!-- <?php /* echo $continues_video['first_name']." ".$continues_video['last_name']; */ ?> -->
                                                        <?php echo (isset($continues_video['display_name']) && $continues_video['display_name'])?$continues_video['display_name']:$continues_video['first_name']; ?>
                                                        <span class="f-12">- <?php echo $continues_video['grade_display_name']; ?> </span>
                                                        <br>
                                                      
                                                        <span class="f-11"><?php echo date("M d, Y", strtotime($continues_video['created'])) ; ?></span>
                                                    </h3>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                            <?php }
                                }
                            ?>
                            
                            <!-- video block -->
                        </div>
                    </div>

                    <?php }
                    ?>
                    <!-- //timeline section -->
                    <?php
                        if(isset($recently_video) && count($recently_video)){?>
                    <div class="timeline mb-3">
                        <div class="playlist-section">
                            <div class="playlist-header">
                                <h3>Recently Uploaded</h3>
                                <?php
                                    if(count($recently_video_count) > 3){
                                ?>
                                    <a href="<?php  echo base_url(); ?>parent/recently-video" class="see-more">View All</a>
                                <?php }
                                ?>
                            </div>
                        </div>

                        <div class="row row-ff">
                            <?php
                                if(isset($recently_video) && $recently_video){
                                    foreach ($recently_video as $key => $recent_video) {?>
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                        <a href="video-fulldetail/<?php echo $recent_video['media_slug']; ?>">
                                            <div class="card timeline">
                                                <div class="video-block">
                                                    <img class="card-img-top" src="<?php echo $recent_video['thumb_image']?$recent_video['thumb_image']:"assets/front/images/video_thumb.jpg"; ?>">
                                                    <span><?php echo $recent_video['duration']; ?>
                                                        <i class="fa fa-play"></i>
                                                    </span>
                                                </div>
                                                <div class="card-block">
                                                    <div class="card-text">
                                                        <h3><?php echo mb_strimwidth(ucfirst($recent_video['title'])?ucfirst($recent_video['title']):"", 0, 20, "...");?></h3>
                                                    </div>
                                                    <figure class="profile profile-inline">
                                                        <img src="<?php echo $recent_video['profile_image']?$recent_video['profile_image']:"assets/front/images/person.jpg"; ?>" class="profile-avatar" alt="">
                                                    </figure>
                                                    <h3 class="card-title w-70">
                                                        <!-- <?php /* echo $recent_video['first_name']." ".$recent_video['last_name']; */ ?> -->
                                                        <?php echo (isset($recent_video['display_name']) && $recent_video['display_name'])?$recent_video['display_name']:$recent_video['first_name']; ?>
                                                        <span class="f-12">- <?php echo $recent_video['grade_display_name']; ?> </span>
                                                        <br>
                                                        <span class="f-11"><?php echo date("M d, Y", strtotime($recent_video['created'])) ; ?></span>
                                                    </h3>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                            <?php }
                                }
                            ?>
                            
                            <div class="spacer20"></div>
                            <div class="spacer20"></div>
                            <!-- video block -->
                        </div>

                    </div>
                <?php } ?>
                    <!-- //playlist section -->

                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    </div>

</section>

