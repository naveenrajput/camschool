<?php if(!empty($notifications)){
      foreach($notifications as $details){  ?>
      <div class="media">
        <img class="d-flex mr-3" width="40" height="40" src="<?php if(!empty($details['profile_image'])){ echo $details['profile_image'];}else{ echo 'assets/front/images/person.jpg';}?>" alt="image">
        <div class="media-body">
          <h6 class="mt-0"><?php if(!empty($details['message'])){ echo wordwrap(ucfirst($details['message']),150,"<br>\n",TRUE);;}else{ echo 'N/A';}?></h6>
          <span class="f-14"><?php echo convertGMTToLocalTimezone($details['created_datetime']);?></span>
        </div>
      </div>
      <hr>
<?php } }?>

