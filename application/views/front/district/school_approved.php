    <section class="content">
        <div class="container">
            <div class="row">
            <?php include APPPATH.'views/front/include/sidebar.php'; ?>               
                <div class="col-md-12">
                    <div class="main-body">
                        <p class="alert_message" id="msg" style="display:none;"></p>
                        <div class="content-header">
                            <h2 class="company-name">Schools/Principals</h2>
                            <a href="javascript:void(0)" onclick="sendSchoolmsg()" class="btn btn-primary "><span class="hide-xs">Send New Message</span> <span class="hide-lg"><i style="font-size:18px" class="fa fa-pencil"></i></span></a>
                        </div>
                        <div class="content-body" id="ajax_data">
                            <?php $this->load->view('front/district/school_list_data.php');?>
                            <!-- <div class="col-12 text-center">
                                <a href="javascript:void(0)" onclick="sendSchoolmsg()" class="btn btn-primary ">Send new Message</a>
                            </div> -->
                        </div>                            
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
   

    function getschoolval(ee){
        // return false
        if($(ee).prop("checked") == true){
            $('input[class="isChecked"]').each(function(){                
                $(this).prop("checked", "checked");
            })            
        }else{    
            $('input[class="isChecked"]').each(function(){                
                $(this).prop("checked", false);
            }) 
        }
    }

    function sendSchoolmsg(){
       
        var URL = "<?php echo base_url().'school-broadcast-page';?>"
        var school_data = [];
        var district_id = `<?php echo $this->session->userdata('district_id'); ?>`;
        var user_id = `<?php echo $this->session->userdata('user_id'); ?>`;
        
        $('input[class="isChecked"]').each(function(){
            var school_ids = $(this).val();
            var school_email = $(this).attr('data-email');
            var school_name  = $(this).attr('data-name')
            if($(this).prop("checked") == true){
                school_data.push({
                    name: school_name,
                    email: school_email,
                    id: school_ids

                });
            }
        });
       
        if(school_data && school_data.length > 0 && district_id && user_id){

            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>approved-school-sendmsg",
                data: {type:'web',school_data: school_data, district_id ,user_id},
                headers: { 'apikey': '<?php echo APP_KEY ?>' },
                success: function(response) {
                    response = JSON.parse(response);
                
                    if(response.status==4){
                        location.reload();
                    } 
                    if(response && response.status == 1 && response.msg && response.details){    
                        window.location.href = URL
                    }
                },error: function(){
                    $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close"  type="button">×</button>Some error occured in loading the data.</div>');
                    $('#msg').css('display','block');
                    setTimeout(function() {
                         $('#msg').fadeOut('slow');
                    }, 3000);
                }
            });
        }else{ 
            $("html, body").animate({ scrollTop: 0 }, "fast");  
            $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close"  type="button">×</button>Select the recipients</div>');
            $('#msg').css('display','block');
            setTimeout(function() {
                 $('#msg').fadeOut('slow');
            }, 5000);
           
        }

    }

  

   

</script>