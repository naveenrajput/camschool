<section class="content">
    <div class="container">
        <div class="row">
        <?php include APPPATH.'views/front/include/sidebar.php'; ?>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="main-body">
                <div class="message_box" id="msg" style="display:none;"></div>
                    <div class="content-header">
                        <h2 class="company-name mt-2 mb-2">School Videos</h2>
                        <div class="d-flex flex-wrap justify-content-between align-content-center">
                        	
                            <button type="" class="openbtn" onclick="openNav()">
                                <img src="assets/front/images/filter.svg" width="30" alt="filter-icon">
                            </button>  
                            <div id="mySidepanel" class="sidepanel">
                                <div class="filter-header p-2">
                                    <h6>Filter By</h6>
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

                                </div>
                                <div class="mb-3">
                                    <select class="form-control" id="schoolList" onchange="getGradeList(this)">
                                        <option value="">Select School</option>
                                        <?php
                                            if($school_list){
                                                foreach($school_list as $list){?>
                                                <option value="<?= $list['id']; ?>"><?= $list['name']; ?></option>
                                        <?php } } ?>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <select class="form-control" id="gradelist" onchange="getTeacherList(this)">
                                        <option value="">Select Grade</option>
                                        <?php  foreach($grade as $list){  ?>
                                                <option value="<?= $list['grade']; ?>"><?= $list['grade_display_name']; ?></option>
                                        <?php  } ?>
                                    </select>
                                </div>
                                <div class="mb-3">                           
                                    <select class="form-control" id="teacherList">
                                        <option value="">Select Teacher</option>
                                    </select>
                                </div>

                                <div class="text-center">
                                    <a onclick="getallvideos();" class="btn btn-primary">Search</a>
                                 <div class="">
                                    <a href="javascript:void(0)" onclick="reset()">Reset</a>
                                        <!--  <button onclick="reset()"  class="btn btn-primary ">Reset</button> -->
                                    </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="content-body">
                    <div class="timeline">
                        <div class="row row-fff" id="videoList">

                        </div>
                    </div>

                    <div class="col-md-12 text-center" >
                        <a href="javascript:void(0)"  id="moreVideos" onclick="getMoreVideos()" class="btn btn-primary loader_btn">See More</a>
                    </div>
                    <input type="hidden" id="page" value="0">
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>
<div class="spacer20"></div>
<!-- <div class="spacer20"></div> -->

<script>
function reset() {
    $('#schoolList').prop('selectedIndex',0);
    $('#gradelist').prop('selectedIndex',0);
    $('#teacherList').prop('selectedIndex',0);
    $('#page').val(0);  
    getallvideos();
    $('#teacherList').empty().append(`<option value="">Select Teacher</option>`);
}
function getGradeList(id){
    var schooId = $(id).val().trim();
    
    $('#gradelist').empty().append(`<option value="">Select Grade</option>`);
    $('#teacherList').empty().append(`<option value="">Select Teacher</option>`);
    $('#gradelist').attr('disabled',true);
    $('#teacherList').attr('disabled',true);
    if(schooId && schooId.length>0){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>api/grade-list",
            data: {type:'web',school_id: schooId},
            headers: { 'apikey': '<?php echo APP_KEY ?>' },
            success: function(response) {
                response = JSON.parse(response);
                $('#gradelist').removeAttr('disabled');
                $('#teacherList').removeAttr('disabled');
                if(response.status==4){
                    location.reload();
                } 
                if(response.status == 1 && response.msg == 'Grade list' && response.details.length>0){
                    $('#gradelist').empty().append(`<option value="">Select Grade</option>`);
                    // $('#teacherList').empty().append(`<option value="">Select Teacher</option>`);

                   var res = response.details.map((val) => {
                        return `<option value="${val.grade}">${val.grade_display_name}</option>`;
                    });    
                    $('#gradelist').append(res);
                }else{
                    
                    $('#gradelist').empty().append(`<option value="">Select Grade</option>`);
                }
            },error: function(){
                $('#gradelist').removeAttr('disabled');
                $('#teacherList').removeAttr('disabled');
                $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close"  type="button">×</button>Some error occured in loading the data.</div>');
                $('#msg').css('display','block');
                setTimeout(function() {
                     $('#msg').fadeOut('slow');
                }, 3000);
            }
        });
    }else{
        $('#gradelist').removeAttr('disabled');
        $('#teacherList').removeAttr('disabled');
    }
}

function getTeacherList(id){
    $('#teacherList').empty().append(`<option value="">Select Teacher</option>`);
    var gradeID = $(id).val().trim();
    var school_id = $('#schoolList').val();
    var user_id = `<?php echo $this->session->userdata('user_id'); ?>`;
    $('#teacherList').attr('disabled',true);
    if(gradeID && gradeID.length>0 && school_id && user_id){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>api/teacher-by-grade",
            data: {type:'web',school_id: school_id, grade:gradeID,user_id  },
            headers: { 'apikey': '<?php echo APP_KEY ?>' },
            success: function(response) {
                response = JSON.parse(response);
                $('#teacherList').removeAttr('disabled');
                if(response.status==4){
                    location.reload();
                } 
                $('#teacherList').empty().append(`<option value="">Select Teacher</option>`);
                if(response.status == 1 && response.details && response.details.grade && response.details.grade[0] && response.details.grade[0].teacher ){
                   var res = response.details.grade[0].teacher.map((val) => {
                    let Dpname ='';
                        // if((val.display_name).length >0){
                        //     Dpname = val.display_name;
                        // }else{
                            Dpname = (val.first_name+' '+val.last_name).replace(/(^\w|\s\w)/g, m => m.toUpperCase());;
                        // }

                        return `<option value="${val.user_id}">${Dpname}</option>`;
                    });    
                    $('#teacherList').append(res);
                }else{
                    
                    $('#teacherList').empty().append(`<option value="">Select Teacher</option>`);
                }
            },error: function(){
                $('#teacherList').removeAttr('disabled');
                $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close"  type="button">×</button>Some error occured in loading the data.</div>');
                $('#msg').css('display','block');
                setTimeout(function() {
                     $('#msg').fadeOut('slow');
                }, 3000);
            }
        });
    }else{
        $('#teacherList').removeAttr('disabled');
    }
} 

function getMoreVideos(){
    var pageno = parseInt($('#page').val());
    pageno = pageno+1
    $('#page').val(pageno);    
    getallvideos();
}
getallvideos();

function getallvideos(){
    //$("#loader-wrapper").show();
    
    var user_type = `<?php echo $this->session->userdata('front_user_type'); ?>`;
    var user_id = `<?php echo $this->session->userdata('user_id'); ?>`;
    
    var page = $('#page').val();
    var limit = 6;
    var data = {type:'web',user_id,user_type,school_id, teacher_id,grade, page,limit  };
    
    var teacher_id = $('#teacherList').val().trim() ? $('#teacherList').val().trim() : "";
    
    var school_id = $('#schoolList').val().trim() ? $('#schoolList').val().trim() : "";
   
    var grade = $('#gradelist').val().trim() ? $('#gradelist').val().trim() : "";
     

    var data = {type:'web',user_id,user_type,school_id, teacher_id,grade, page,limit  };
    if(user_type && user_id){
        $('.loader_btn').html('<span class="   fa fa-spinner fa-spin fa-lg mr-2" role="status" aria-hidden="true"></span>Loading...').attr('disabled', true);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>api/all-media",
            data: data,
            dataType: 'JSON',
            headers: { 'apikey': '<?php echo APP_KEY ?>' },
            success: function(response) { 
                closeNav();
                $('.loader_btn').html('See More').attr('disabled', false);
                // return false
                if(response.status==4){
                    location.reload();
                } 
                if(response && response.status == 1 && response.msg == "Video List" ){
                    if(page !=0){
                        $('#videoList').append(response.html)
                        var numMedia = $('.media_vid').length;                               
                        if(numMedia == parseInt( response.total_media_count)){
                            $('#moreVideos').hide();
                        }else{
                            $('#moreVideos').show();
                        }
                    }else{
                        $('#videoList').empty();
                        $('#videoList').html(response.html);
                        var numMedia = $('.media_vid').length;
                        // console.log(numMedia)
                        // console.log(response.total_media_count)
                        if(numMedia == parseInt( response.total_media_count)){
                            $('#moreVideos').hide();
                        }else{
                            $('#moreVideos').show();
                        }
                    }
                   
                }else{
                    if(response && response.status == 0 && response.msg == "No record found" ){
                        $('#moreVideos').hide();
                         $('#videoList').empty();
                        //$("#msg").html('<div class="alert alert-warning"><button data-dismiss="alert" class="close" type="button">×</button>No more videos</div>');
                        // $('#videoList').empty()
                        $('#msg').css('display','block');
                            setTimeout(function() {
                            $('#msg').fadeOut('slow');
                        }, 3000);
                    }
                }
                
            },error: function(){
                closeNav();
              
                $('.loader_btn').html('See More').attr('disabled', false);
                $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Some error occured in loading the data.</div>');
                $('#msg').css('display','block');
                    setTimeout(function() {
                    $('#msg').fadeOut('slow');
                }, 3000); 
            }
        });
    }
     
};


</script>

