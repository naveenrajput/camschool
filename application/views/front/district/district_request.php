    <section class="content">
        <div class="container">
            <div class="row">
            <?php include APPPATH.'views/front/include/sidebar.php'; ?>
                <div class="col-md-12">
                    <div class="main-body">
                    <p class="alert_message" id="msg" style="display:none;"></p>
                        <div class="content-header">
                            <h2 class="company-name">Requests</h2>
                        </div>
                        <div class="content-body" id="content_table" style="display:none;">
                            <table class="table table-responsive-sm table-responsive-md table-hover" id="schoollist">
                                <tr>
                                    <th><input type="checkbox" onchange="getschoolval(this)"/></th>
                                    <th>Principals</th>
                                    <th>School Name</th>                                    
                                </tr>
                            </table>
                            <div class="col-12 text-center">
                                <a href="javascript:void(0)" onclick="updateSchool()" class="btn btn-primary loader_btn">Request Approved</a> 
                                <!-- // district-administration-compose -->
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
    $( document ).ready(function() {
      
        var district_id = `<?php echo $this->session->userdata('district_id'); ?>`;
       
        if(district_id && district_id.length > 0){
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>api/district-request-list",
                data: {type:'web',district_id },
                headers: { 'apikey': '<?php echo APP_KEY ?>' },
                success: function(response) { 
                    response = JSON.parse(response);
                    try {
                        var data =$.parseJSON(response)
                        if(data.status==4){
                           
                              location.reload(); 
                        }

                    }catch(e) {
                       
                       if(response.status == 1 && response.msg){
                            if(response && response.details.length >0 ){
                                $('#content_table').css('display','block');
                                var res = response.details.map(val => { 
                                    var dspname="--";
                                    if(val.first_name && val.last_name){
                                        dspname=val.first_name+' '+val.last_name;
                                    }
                                return `<tr>
                                    <td><input class="isChecked" type="checkbox" value="${val.id}"/></td>
                                        <td>${dspname}</td>
                                        <td>${val.school_name}</td>    
                                    </tr>`;
                                 });    
                                 $("#schoollist").append(res);
                            }else{
                                $('#content_table').empty();
                            }
                            
                        }else{
                            $('#content_table').empty();
                        }
                    }
                },error: function(){
                   
                    $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Some error occurred, try again.</div>');
                    $('#msg').css('display','block');
                        setTimeout(function() {
                        $('#msg').fadeOut('slow');
                    }, 3000); 
                }
            });
        }
         
    });

    function getschoolval(ee){
        if($(ee).prop("checked") == true){
            $('input[class="isChecked"]').each(function(){                
                $(this).prop("checked", "checked");
            })            
        }else{    
            $('input[class="isChecked"]').each(function(){                
                $(this).prop("checked", false);
            }) 
        }
    }

    function updateSchool(){
        var school_data = [];
        var district_id = `<?php echo $this->session->userdata('district_id'); ?>`;
        var user_id = `<?php echo $this->session->userdata('user_id'); ?>`;
        $('input[class="isChecked"]').each(function(){
            var school_ids = $(this).val();
            if($(this).prop("checked") == true){
                school_data.push(school_ids);
            }
        })
        let schoolID = school_data.toString();
        if(schoolID && schoolID.length > 0 && user_id && district_id){
            $('.loader_btn').html('<span class="   fa fa-spinner fa-spin fa-lg mr-2" role="status" aria-hidden="true"></span>Loading...').attr('disabled', true);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>api/district-approved-school",
                data: {type:'web',school_id: schoolID, district_id ,user_id},
                headers: { 'apikey': '<?php echo APP_KEY ?>' },
                success: function(response) {
                    response = JSON.parse(response);
                    $('.loader_btn').html('Request Approved').attr('disabled', false);
                    if(response.status==4){
                        location.reload();
                    } 
                    if(response.status == 1 && response.msg){
                        $("#msg").html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+response.msg+'</div>');
                        $('#msg').css('display','block');
                        setTimeout(function() {
                        $('#msg').fadeOut('slow');
                        }, 3000);

                        setTimeout(function() {
                            location.reload();
                        }, 3000);
                        // location.reload();                        
                    }
                },error: function(){
                   
                    $('.loader_btn').html('Request Approved').attr('disabled', false);
                    $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close"  type="button">×</button>Some error occurred, try again.</div>');
                    $('#msg').css('display','block');
                    setTimeout(function() {
                         $('#msg').fadeOut('slow');
                    }, 3000);
                }
            });


        }else{
            
           
        }

    }


</script>