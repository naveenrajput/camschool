
<?php
    // echo '<pre>';
    //print_r($this->session->all_userdata()); die;
?>
    <section class="content">
        <div class="container">
            <div class="row">
            <?php include APPPATH.'views/front/include/sidebar.php'; ?>       

            <div class="col-md-12">
                <div class="main-body">
                <p class="alert_message" id="msg" style="display:none;"></p>
                <div class="content-header">
                    <h2 class="company-name">Broadcast Message</h2>
                </div>
                
                <form id="broadcast_form" action="" role="form" data-parsley-validate enctype="multipart/form-data">

                    <div class="content-body">
                        <!-- <div class="form-group">
                            <label for="to">To</label>
                            <input class="form-control" name="emailTo" id="emailTo" type="text" value="<?php //if(isset($broad_email) && !empty($broad_email)){ echo $broad_email; } ?>" />                       

                        </div> -->

                        <div class="form-group">
                          
                            <label for="to">To</label>
                                <select class="form-control" id="emailTo" name="emailTo" multiple="multiple" data-parsley-required data-parsley-required-message="Select the recipients" data-parsley-errors-container="#recipent_error">
                                  
                                    <?php
                                          
                                        if($Tolist && count($Tolist)>0){
                                            foreach($Tolist as $list){ 
                                                if(!empty($list['display_name'])){
                                                    if(!empty($list['first_name']) && !empty($list['last_name'])){
                                                        
                                                        $user_name=$list['first_name']." ".$list['last_name']; 
                                                    }
                                                    else{
                                                        $user_name=$list['display_name'];
                                                    }
                                                    
                                                }
                                                else{
                                                    
                                                    if(empty($list['first_name']) || empty($list['last_name'])){
                                                        if(!empty($list['email'])){
                                                            $user_name=$list['email'];
                                                        }
                                                    }
                                                    else{
                                                        $user_name=$list['first_name']." ".$list['last_name'];    
                                                    }
                                                }?>

                                     <!--        <option data-email="<?php //echo $list['email']?>" value="<?php //echo $list['teacher_id']?>"><?php //echo $list['first_name']?ucfirst($list['first_name']).' '.$list['last_name']:$list['email'];?></option>  --> 
                                     <option data-email="<?php echo $list['email']?>" value="<?php echo $list['user_id']?>"><?php echo $user_name;?></option>

                                    <?php }  } ?>
                                </select>
                                 <div id="recipent_error"></div>
                            
                        </div>
                        <div class="form-group">
                            <label for="to">Subject</label>
                            <input class="form-control" name="subject" id="subject" type="text" placeholder="" data-parsley-required data-parsley-required-message="Enter your subject" maxlength="60" />
                        </div>
                          <div id="the-count">
                                    <span id="current1"style="color: rgb(102, 102, 102);">0</span>
                                    <span id="maximum1"style="color: rgb(102, 102, 102);">/ 60</span>

                                </div>
                        <div class="form-group">
                            <label for="to">Message</label>
                            <textarea name="message" id="message" class="form-control" rows="6" maxlength="250"  data-parsley-required data-parsley-required-message="Enter your message"></textarea>
                        </div>
  <div id="the-count">

                                    <span id="current"style="color: rgb(102, 102, 102);">0</span>
                                    <span id="maximum"style="color: rgb(102, 102, 102);">/ 250</span>

                                </div>
                        <div class="form-group text-center">
                            <button onclick="sendEmail()" class="btn btn-primary loader_btn">Send</button>
                        </div>

                    </div>

                </form>

                <div class="clearfix"></div>
            </div>
        </div>

    </div>
</div>

</section>

<script>
    function sendEmail(){
      
        var subject   = $('#subject').val().trim();
        var message   = $('#message').val().trim();
        var user_type = `<?php echo $this->session->userdata('front_user_type'); ?>`;
        var user_info =  $('#emailTo').val();
        // var user_info = `<?php //echo json_encode($user_info); ?>`;
        var user_id   = `<?php echo $this->session->userdata('user_id'); ?>`;
        var URL = "<?php echo base_url().'broadcast-messages';?>"
        $("#broadcast_form").parsley().validate();
        if($("#broadcast_form").parsley().isValid()){
            if(subject.length > 0 && message.length > 0 && user_type && user_info && user_id ){
                 
                $('.loader_btn').html('<span class="   fa fa-spinner fa-spin fa-lg mr-2" role="status" aria-hidden="true"></span>Loading...').attr('disabled', true);
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>api/broadcast-msg",
                    data: {type:'web', subject, message, user_info, user_type, user_id},
                    headers: { 'apikey': '<?php echo APP_KEY ?>' },
                    success: function(response) {
                        response = JSON.parse(response);
                        $('.loader_btn').html('Send').attr('disabled', false);
                        if(response.status==4){
                            location.reload();
                        } 
                        if(response && response.status == 1 && response.msg){ 
                            /*$("#msg").html('<div class="alert alert-success"><button data-dismiss="alert" class="close"  type="button">×</button>Broadcast message sent successfully.</div>');
                            $('#msg').css('display','block');
                            setTimeout(function() {
                                $('#msg').fadeOut('slow');
                            }, 3000);*/

                            window.location.href = URL
                        }
                    },error: function(){
                       
                        $('.loader_btn').html('Send').attr('disabled', false);
                        $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close"  type="button">×</button>Some error occurred, try again.</div>');
                        $('#msg').css('display','block');
                        setTimeout(function() {
                             $('#msg').fadeOut('slow');
                        }, 3000);
                    }
                });

            }else{
                $("#msg").html('<div class="alert alert-warning"><button data-dismiss="alert" class="close"  type="button">×</button> Complete all of the fields.</div>');
                $('#msg').css('display','block');
                setTimeout(function() {
                    $('#msg').fadeOut('slow');
                }, 3000);
            }
        }
    }
    
    var usersINFO = `<?php echo json_encode($user_info); ?>`;
    usersINFO = JSON.parse(usersINFO);
    var selectedvalues = [];

    if(usersINFO && usersINFO.length>0){
        usersINFO.map(x =>{
            selectedvalues.push(x.id)    
        });

        $('#emailTo').val(selectedvalues).trigger('change');
    }
    

  

    $('#emailTo').select2({
         placeholder: '',
         multiple:true,
    });



</script>
<script>
window.onload = (event) => {
    var characterCount = $('#message').val().length; 
    var characterCount1 = $('#subject').val().length;
        current = $('#current');
        maximum = $('#maximum');
        theCount = $('#the-count'); 
        current1 = $('#current1');
        maximum1 = $('#maximum1');
        theCount1 = $('#the-count1');
     current.text(characterCount);
      current1.text(characterCount1);
   
};

$('#subject').keyup(function() {
   var characterCount1 = $(this).val().length;
        current1 = $('#current1');
        maximum1 = $('#maximum1');
        theCount1 = $('#the-count1');
      current1.text(characterCount1);
});
$('textarea').keyup(function() {
    
    var characterCount = $(this).val().length,
        current = $('#current'),
        maximum = $('#maximum'),
        theCount = $('#the-count');
      
    current.text(characterCount);    
    /*This isn't entirely necessary, just playin around*/
    if (characterCount < 500) {
      current.css('color', '#666');
    }
    if (characterCount > 500 && characterCount < 1500) {
      current.css('color', '#6d5555');
    }
    if (characterCount > 1500 && characterCount < 2000) {
      current.css('color', '#793535');
    }
    if (characterCount > 2000 && characterCount < 2500) {
      current.css('color', '#841c1c');
    }
    if (characterCount > 2500 && characterCount < 2800) {
      current.css('color', '#8f0001');
    }
    
    if (characterCount >= 3000) {
      maximum.css('color', '#8f0001');
      current.css('color', '#8f0001');
      theCount.css('font-weight','bold');
    } else {
      maximum.css('color','#666');
      theCount.css('font-weight','normal');
    }
        
  });


  

</script>