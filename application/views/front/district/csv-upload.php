
    <section class="content">
        <div class="container">
            <div class="row">
               <?php include APPPATH.'views/front/include/sidebar.php';  ?>
                <div class="col-md-12">
                    <div class="main-body">
                        <div class="message_box" id="msg" style="display:none;"></div>
                        <?php if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger">
                              <button data-dismiss="alert" class="close" type="button">×</button>
                              <?php echo $this->session->flashdata('error') ?>
                          </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success">
                              <button data-dismiss="alert" class="close" type="button">×</button>
                              <?php echo $this->session->flashdata('success') ?>
                          </div>
                        <?php } ?>
                   
                        <div class="content-header">
                            <h2 class="company-name flex-grow-1">Upload School File</h2>
                            <!-- <a href="assets/front/csv/Sample File.xlsx" class="btn btn-primary">Download Sample file</a> -->
                            <a href="assets/front/csv/Sample File.xlsm" class="btn btn-primary"><span class="hide-xs">Sample Macro File</span></a> &nbsp;
                            <a href="download-media/1"  class="btn btn-primary"><span class="hide-xs">Sample File</span> <!-- <i class="fa fa-apple"></i> --></a>

                        </div>
                        <div class="content-body">
                            <div class="container">
                                <div class="row">
                                  <div class="col-sm-12">
                                      <form method="post" action="district/import-file" id="file_upload" enctype="multipart/form-data">
                                          <div class="form-group">
                                              <label>Upload Your File </label>
                                                <div class="files">
                                                  <input type="file" class="form-control" name="file" onchange="file_form_submit();" accept=".xlsx, .xls">
                                                </div>
                                          </div>
                                      </form>
                                  </div>
                                </div>
                            </div>

                            <div class="upload-list bg-light">
                                <h2 class="company-name">Uploaded Files</h2>
                            </div>
                            <div class="">
                                <table class="table table-responsive-sm table-responsive-md table-hover child-list">
                                    <tr>
                                        
                                        <th>File Name</th>
                                       <!--  <th>Status</th> -->
                                        <th>Date</th>
                                    </tr>
                                    <?php if(!empty($csv_data)){
                                        $i=0;
                                        foreach ($csv_data as $key => $details) { $i++;?>
                                            <tr>
                                                
                                                <td><a href="<?php if(!empty($details['file_name']))
                                                {echo $details['file_name'];}else{echo 'javascript:void(0);';} ?>"><?php if(!empty($details['file_name'])){
                                                        $file=explode('/',$details['file_name']);
                                                        echo str_replace('_', ' ', $file[2]);
                                                    } ?></a></td>
                                             <!--    <td><?php //if($details['status']==0){echo 'Pending';}elseif($details['status']==1){echo 'Completed';}else{echo 'Error';}?></td> -->
                                                <td><?php if($details['created']){echo convertGMTToLocalTimezone($details['created']);}?></td>
                                            </tr>
                                    <?php } }else{ ?>
                                        <tr>
                                            <td colspan='4' align='center'>No records available.</td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <style type="text/css">
        .files input {
            outline: 2px dashed #bc718c;
            outline-offset: -10px;
            -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
            transition: outline-offset .15s ease-in-out, background-color .15s linear;
            padding: 120px 0px 85px 35%;
            text-align: center !important;
            margin: 0;
            width: 100% !important;
            opacity:0;
        }
        .files input:focus{     outline: 2px dashed #92b0b3;  outline-offset: -10px;
            -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
            transition: outline-offset .15s ease-in-out, background-color .15s linear; border:1px solid #92b0b3;
         }
        .files{ 
            position:relative;
            outline: 2px dashed #bc718c;
            outline-offset: -10px;
            -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
            transition: outline-offset .15s ease-in-out, background-color .15s linear;
            text-align: center !important;
        }
        .files:after {  pointer-events: none;
            position: absolute;
            top: 60px;
            left: 0;
            width: 50px;
            right: 0;
            height: 56px;
            content: "";
            background-image: url(assets/front/images/109612.png);
            display: block;
            margin: 0 auto;
            background-size: 100%;
            background-repeat: no-repeat;
        }
        .color input{ background-color:#f1f1f1;}
        .files:before {
            position: absolute;
            bottom: 30px;
            left: 0;  pointer-events: none;
            width: 100%;
            right: 0;
            height: 40px;
            content: "select or drag it here. ";
            display: block;
            margin: 0 auto;
            color: #e3548a;
            font-weight: 600;
            text-transform: capitalize;
            text-align: center;
        }
    </style>
    <script type="text/javascript">
        function file_form_submit(){
            $("#loader-wrapper").show();
            $("#file_upload").submit();
        }
        
        function downloadFile(filePath) {
            var link = document.createElement('a');
            link.href = filePath;
            link.download = filePath.substr(filePath.lastIndexOf('/') + 1);
            link.click();
        }
    </script>