<style type="text/css">
    .clr{color:#e3548a;}
    .nocursor{
        cursor: auto!important;
    }
</style>
<?php
    if($details && count($details) >0 ){       
        $date = convertGMTToLocalTimezone($details['created'],$showTime=false, $only_time=false,"", $fulltime =FALSE);
        ?>


            <div class="main-body">
                    <p class="alert_message" id="msg" style="display:none;"></p>
                    <?php /*if($this->session->userdata('front_user_type')==1){ ?>
                        <div class="content-header d-flex flex-wrap justify-content-between">
                            <div>
                                <figure class="profile profile-inline">
                                    <img src="<?php echo $details['profile_image']?$details['profile_image']:"assets/front/images/person.jpg"; ?>" class="profile-avatar" alt="">
                                </figure>
                                <h3 class="card-title">

                                <?php echo  ucfirst($details['display_name'])?ucfirst($details['display_name']):ucfirst($details['first_name'].' '.$details['last_name']); ?><br>
                                    <span class="f-12"> <?php echo $details['grade_display_name']; ?> </span>
                                </h3>
                            </div>
                            <small><?php echo $date; ?></small>
                        </div>
                    <?php }else{ ?>
                        <!-- <div class="content-header">
                            <h2 class="company-name"><?php //echo  ucfirst($details['display_name'])?ucfirst($details['display_name']):ucfirst($details['first_name'].' '.$details['last_name']); ?></h2>
                        </div> -->
                    <?php }*/ ?>
                   
                    <div class="content-body video-detail">
                        <div class="profile-block" id="profile_block">
                            <div class="row">
                                <div class="video-box col-sm-12">

                                    <video width="100%" height="400" id="my_video" poster="<?= $details['large_image']; ?>" controls>
                                        <source src="<?= $details['media_name']; ?>">
                                    </video>
                                    <input type="hidden" name="user_id" id="user_id" value="<?php echo $this->session->userdata('user_id'); ?>">
                                    <input type="hidden" name="media_id" id="media_id" value="<?php echo $details['id']; ?>">
                                    <input type="hidden" name="user_type" id="user_type" value="<?php echo $this->session->userdata('front_user_type'); ?>">
       

                                    <span><?php echo $details['duration']; ?>

                                        <i class="fa fa-play"></i>
                                    </span>
                                </div>
                                <div class="video-content col-sm-12 d-flex justify-content-between">
                                    <div>
                                        <?php if($this->session->userdata("front_user_type")==1){ ?>
                                        <span class="f-12 text-black-50"><?php echo ucwords($details['school_name']);?></span>
                                        <?php } ?>
                                        <h5><?php echo wordwrap(ucfirst($details['title']),160,"<br>\n",TRUE); ?></h5>
                                        <p class="f-14"><?php echo wordwrap(ucfirst($details['description']),160," ",TRUE); ?></p>
                                    </div>
                                    <div>
                                        <?php if($details['likes']){ $fa_class='fa fa-thumbs-up';}else{$fa_class='fa fa-thumbs-o-up'; }?>
                                        <?php if($this->session->userdata("front_user_type")==3 || $this->session->userdata("front_user_type")==4){ ?>

                                            <span class="<?php echo ($details['like']==1)?'clr':'';?> cursor btn btn_design_light btn-small 123132  pl-0_bkup" id="media_like_<?php echo $details['id'];?>"  onclick="media_like(<?php echo $details['id']?$details['id']:"";?>);"><i class="<?php echo $fa_class;?>" aria-hidden="true"></i> <?php echo $details['likes'];?></span>

                                            <?php /* <span <?php if($this->session->userdata('user_id')==$details['user_id']){ ?> class="<?php echo ($details['like']==1)?'clr':'';?> cursor btn btn_design_light btn-small 123132  pl-0"<?php }else{?> class="<?php echo ($details['like']==1)?'clr':'';?> cursor btn btn_design_light btn-small 123132 like_btn_design pl-0"<?php  }?> id="media_like_<?php echo $details['id'];?>"  onclick="media_like(<?php echo $details['id']?$details['id']:"";?>);"><i class="<?php echo $fa_class;?>" aria-hidden="true"></i> <?php echo $details['likes'];?></span> */?>


                                        <?php } else{   ?>
                                            <span id="media_like_<?php echo $details['id'];?>" class="<?php echo ($details['like']==1)?'clr':'';?> btn btn_design_light btn-small 123132 like_btn_design nocursor"><i class="<?php echo $fa_class;?>" aria-hidden="true"></i> <?php echo $details['likes'];?></span>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php 
                            if($this->session->userdata("front_user_type")==1 || $this->session->userdata("front_user_type")==2){
                                echo ucwords($details['first_name'].' '.$details['last_name']).' - '.$details['grade_display_name'];
                            }
                            if($this->session->userdata("front_user_type")==3){
                                echo $details['grade_display_name'];
                            }
                            if($this->session->userdata("front_user_type")==4){
                                 echo ucwords($details['display_name'].' - '.$details['grade_display_name']);
                            }
                            // echo "<pre>";print_r($details);
                            ?>
                            <span class="pull-right"><?php echo $date;?></span>
                           
                        </div>
                       

                <!-- strat here 02 all_post_comment-->
                <?php if($details['created_as']==1){ ?>
                <div class="playlist-section" id="">
                    <div class="playlist-header">
                        <h3 id="total_comments_dsiaply"><?php  echo ($details['total_comments'])?$details['total_comments']:''; ?> Comments </h3>
                    </div>

                    <div class="">
                        <div class="col-md-12 col-sm-12">
                            <div class="comment-wrapper">
                                <div class="panel panel-info">
                                    <div class="panel-body">
                                        <?php $user_type=$this->session->userdata('front_user_type');
                                        if($user_type==3 || $user_type==4){
                                        ?>
                                        <form method="post" name="post_comment_form" id="post_comment_form" action="" data-parsley-validate>
                                            <textarea class="form-control comment" placeholder="Write a comment" rows="3" style="margin-bottom: 10px" data-parsley-required data-parsley-required-message="Write a comment" name="comment" maxlength="250"></textarea>
                                            <?php if($user_type==3||$user_type==4){ ?>
                                                <?php if($user_type!=4){?>
                                                <a class="btn btn-outline-primary upload-video-comment" href="<?php echo $media_action?$media_action:"";?>">Click here to post video</a>
                                            <?php }?>
                                            <?php } ?>
                                            <button type="submit" id="comment_post_btn" class="btn btn-primary pull-right loader_btn" onclick="add_comment();">Post</button>
                                        </form>
                                        <?php } ?>
                                        <div class="clearfix"></div>
                                        <div id="all_post_comment">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <!--end here  02 -->
            </div>
                <div class="clearfix"></div>
            </div> 

    <?php }/*else{ ?>
            <h2>No Data found</h2>
    <?php }*/ ?>
    <div class="modal" id="report_comment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenteredLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenteredLabel">Report Comment</h5>

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form id="comment_report_form" action="" role="form" data-parsley-validate enctype="multipart/form-data">
          <div class="modal-body">
                <input type="hidden" id="report_comment_modal_id" value="">
               <div class="col-md-12 form-group">
                    <!-- <input type="radio" id="abusive" name="report_type" value="Abusive/Offensive Content" data-parsley-required data-parsley-required-message="Please enter type." data-parsley-errors-container='#report_comment_err'> -->
                    <label for="Abusive/Offensive Content">Reason for reporting this comment</label><br>
                   <!--  <input type="radio" id="adult" name="report_type" value="Reason for reporting this comment" data-parsley-required data-parsley-required-message="Please enter type."  data-parsley-errors-container='#report_comment_err'> -->
                    <!-- <label for="Adult/violent content">Adult/violent content</label><br>
                    <input type="radio" id="other" name="report_type" value="other" data-parsley-required data-parsley-required-message="Please enter type." data-parsley-errors-container='#report_comment_err'>
                    <label for="other">Other</label> -->
                    <div id="report_comment_err"></div>
                </div>
                <div class="col-md-12 form-group">
                    <textarea class="form-control" id="reason"  placeholder="Reason" name="reason" rows="5" data-parsley-required data-parsley-required-message="Please enter the reason you are reporting this comment."></textarea>
                 </div>
                </div>
                <div class="modal-footer">
                    <div if="report_msg"></div>
                    <button type="button" class="btn btn-primary" onclick="return report_comment()">Submit</button>
                </div>
            </form>
        </div>
      </div>
    </div>
<script>
    function report_comment_modal(id){
        $("#report_comment").modal('show');
        $("#report_comment_modal_id").val(id);

    }
    function report_comment() { 
        $("#comment_report_form").parsley().validate();
        var form = $('#comment_report_form')[0];
        if($("#comment_report_form").parsley().isValid()){
            var id= $("#report_comment_modal_id").val();
            var formData = new FormData(form);
            formData.append('type','web');
            // var report_type=$('input[name="report_type"]:checked').val();
            var report_type='other';
            var reason=$("#reason").val();
            $.ajax({
                type:'POST',
               // data: formData,
                data:{ 
                    comment_id:id,
                    type:'web',
                    report_type:report_type,
                    reason:reason,
                },
                url: "api/comment-report",
                headers: { 'apikey': '<?php echo APP_KEY ?>' },
                success:function(data)
                {    
                    $("#report_comment").modal('hide');
                    var response = JSON.parse(data);
                    if(response.status == '1') {
                        $("#report_"+id).removeClass("btn_design_light");
                        $("#report_"+id).addClass("btn_design");
                        $("#report_"+id).html('<i class="fa fa-flag" aria-hidden="true"></i>');
                        get_comments(0, 10, 0, 2, 0, '', 'main');
                    }else {
                        $("#report_msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+response.msg+'</div>');
                        setTimeout(function() {
                         $("#report_msg").html('');
                          $("#report_msg").hide();
                        }, 1000); 
                    }
                },error:function(err){
                
                }
            });
        }
    }
    function media_like(id) {
        $.ajax({
            type:'POST',
            data:{ 
                media_id:id,
                type:'web',
            },
            url: "api/media-like-unlike",
            headers: { 'apikey': '<?php echo APP_KEY ?>' },
            success:function(data)
            {
                var response = JSON.parse(data);
                if(response.status == '1') {
                    if(response.msg == 'liked') {
                        $("#media_like_"+id).removeClass("btn_design_light");
                        $("#media_like_"+id).addClass("btn_design");
                        $("#media_like_"+id).addClass("clr");
                        $("#media_like_"+id).html('<i class="fa fa-thumbs-up" aria-hidden="true";></i> '+response.details);
                        
                    } else {
                        $("#media_like_"+id).removeClass("btn_design");
                        $("#media_like_"+id).addClass("btn_design_light");
                        $("#media_like_"+id).removeClass("clr");
                        if(response.details!='0'){
                            $("#media_like_"+id).html('<i class="fa fa-thumbs-up" aria-hidden="true"></i> '+response.details);
                        }else{
                            $("#media_like_"+id).html('<i class="fa fa-thumbs-o-up" aria-hidden="true"></i> '+response.details);    
                        }
                        
                    }
                    
                }else {
                    $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+response.msg+'</div>');
                    setTimeout(function() {
                     $("#msg").html('');
                      $("#msg").hide();
                    }, 5000); 
                }
            }
        });
    }
    function like_comment(id) {
        $.ajax({
            type:'POST',
            data:{ 
                comment_id:id,
                type:'web',
            },
            url: "api/comment-like-unlike",
            headers: { 'apikey': '<?php echo APP_KEY ?>' },
            success:function(data)
            {
                var response = JSON.parse(data);
                if(response.status == '1') {
                    if(response.msg == 'liked') {
                        $("#like_"+id).removeClass("btn_design_light");
                        $("#like_"+id).addClass("btn_design");
                        $("#like_"+id).html('<i class="fa fa-thumbs-up" aria-hidden="true"></i>'+' '+response.details);
                         $("#like_"+id).addClass("clr");
                         //$('#report_'+id).hide();
                    } else {
                        $("#like_"+id).removeClass("btn_design");
                        $("#like_"+id).addClass("btn_design_light");
                        $("#like_"+id).removeClass("clr");
                        $('#report_'+id).show();
                         if(response.details!='0'){
                           $("#like_"+id).html('<i class="fa fa-thumbs-up" aria-hidden="true"></i>'+' '+response.details);
                        }else{
                            $("#like_"+id).html('<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>'+' '+response.details);  
                        }
                    }
                    
                }else {
                    $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+response.msg+'</div>');
                    setTimeout(function() {
                     $("#msg").html('');
                      $("#msg").hide();
                    }, 5000); 
                }
            }
        });
    }
    //function for like comments
    function comment_reply(comment_id,media_id) {  
        event.preventDefault();     
        var reply = $("#reply_text_"+comment_id).val();
        if(reply=="") {
            $("#reply_error_"+comment_id).html("Leave a reply!");
            return false;
        }
        if(reply.length>5000) {
            $("#reply_error_"+comment_id).html("Your reply must be at most 5000 characters long.");
            return false;
        }
        $("#btn_"+comment_id).html('<span class="fa fa-spinner fa-spin fa-lg mr-2" role="status" aria-hidden="true"></span>Loading...').attr('disabled', true);
        $.ajax({
            type:'POST',
            data:{
                type:'web',
                media_id:media_id,
                comment_id:comment_id,
                reply:reply,
            },
            url: "api/comment-reply",
            headers: { 'apikey': '<?php echo APP_KEY ?>' },
            success:function(data){
                $('.comment').val();
                $("#btn_"+comment_id).html('Post').attr('disabled', false);
                var response = JSON.parse(data);
                if(response.status == '1') {
                    // location.reload();
                    get_comments(0, 10, 0, 2, 0, '', 'main');
                }else {
                     $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+response.msg+'</div>');
                    setTimeout(function() {
                     $("#msg").html('');
                      $("#msg").hide();
                    }, 5000); 
                }
                // get_comments();
            },error:function(err){
              $("#btn_"+comment_id).html('Post').attr('disabled', false);
            }
        });
    }
    function add_comment() {     
        event.preventDefault();
        $("#post_comment_form").parsley().validate();
        var form = $("#post_comment_form")[0];
        if($("#post_comment_form").parsley().isValid()){
            var formData = new FormData(form);
            var id='<?php echo $details['id']?$details['id']:"";?>';
            formData.append('type','web');
            formData.append('media_id',id);
            $('#comment_post_btn').html('<span class="   fa fa-spinner fa-spin fa-lg mr-2" role="status" aria-hidden="true"></span>Loading...').attr('disabled', true);
            $.ajax({
                type:'POST',
                data:formData,
                url: "api/add-comment",
                headers: { 'apikey': '<?php echo APP_KEY ?>' },
                dataType: 'json',
                // async: false,
                cache: false,
                contentType: false,
                processData: false,
                success:function(response){
                    $('.comment').val('');
                    $('#comment_post_btn').html('Post').attr('disabled', false);
                    if(response.status == '1') {
                        // location.reload();
                        get_comments(0, 10, 0, 2, 0, '', 'main');
                    }else {
                         $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+response.msg+'</div>');
                        setTimeout(function() {
                         $("#msg").html('');
                          $("#msg").hide();
                        }, 5000); 
                    }
                },error:function(err){
                  $('#comment_post_btn').html('Post').attr('disabled', false);
                }
            });
        }
    }
</script>
<script>
$(document).ready(function(){
    var vid = document.getElementById("my_video");
    var old_duration = "<?php echo $current_time; ?>";
    var hms = old_duration;  
    var a = hms.split(':'); 
    var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 
    if(seconds>0){
        vid.currentTime=seconds;
    }
    $("#my_video").on("timeupdate",function(event){
        vid.onpause = function() {
            var user_id = $("#user_id").val();
            var media_id = $("#media_id").val();
            var user_type = $("#user_type").val();
            
            total_duration = vid.duration;
            current_duration = this.currentTime;
            
            if(current_duration < total_duration){
                var is_finish = 0;
            }else{
                var is_finish = 1;
            }
            if(user_type == 4){
                var convert_time =  new Date(current_duration* 1000).toISOString().substr(11, 8);
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>api/add-continue-watch-video",
                    data: {type:"web",media_id:media_id,user_id:user_id,duration:convert_time,is_finish:is_finish},
                    headers: { 'apikey': '<?php echo APP_KEY ?>' },
                    dataType: 'json',
                    // async: false,
                   
                    success:function(response){
                      
                    }
                });
            }
        };
    });
});
</script>