<?php //print_r($_SESSION);exit;?>
<form id="profile_form" action="" role="form" data-parsley-validate enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-12 col-sm-12 form-group">
            <label>Upload a profile picture or use the CAM! avatar.</label><br>
            <div class="upload-btn-wrapper">
                <input type="file" name="profile_pic" id="fileUpload_1"  accept="image/x-png,image/gif,image/jpeg"> 
                <span class="upload"><i class="fa fa-camera"></i></span> 
                <img src="<?php echo $details['profile_image']?$details['profile_image']:'assets/front/images/person.jpg';?>" id="img_src_1" class="img-fluid" height="100" >
            </div>
               <div id="img_err" style="color:red;"></div>
        </div>
        <!--  <div class="col-md-2 col-sm-6 form-group">
            <label>Title </label>
            <input class="form-control" type="text" value="<?php /*echo $details['title']?$details['title']:'';*/ ?>" name="title" id="title" placeholder="Title" maxlength="10" >
        </div> -->
        <div class="col-md-6 col-sm-6 form-group">
            <label>First Name <span class="mandatory"></span></label>
            <input class="form-control" type="text" value="<?php echo $details['first_name']?$details['first_name']:'';?>" name="first_name" id="first_name" placeholder="First Name" maxlength="30" data-parsley-required data-parsley-required-message="Enter your first name" autocomplete="off">
        </div>

        <div class="col-md-6 col-sm-6 form-group">
            <label>Last Name <span class="mandatory"></span></label>
            <input class="form-control" type="text" value="<?php echo $details['last_name']?$details['last_name']:'';?>" name="last_name" id="last_name" placeholder="Last Name" maxlength="30" data-parsley-required data-parsley-required-message="Enter your last name" autocomplete="off">
        </div>

        <div class="col-md-6 col-sm-6 form-group">
            <label>Email<span class="mandatory"></span></label>
            <input type=hidden name="image" id="imageinput"value="">
            <input class="form-control" type="text" disabled value="<?php echo $details['email']?$details['email']:'';?>">
        </div>

        <div class="col-md-6 col-sm-6 form-group">
            <label>Phone Number</label>
            <?php /* <input class="form-control" type="text" value="<?php echo $details['phone']?$details['phone']:'';?>" id="phone" name="phone" oninput="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'');" onKeyup='addDashes(this)' placeholder="Phone Number" value="" maxlength="12" data-parsley-required data-parsley-required-message="Enter your phone number." data-parsley-minlength="10" data-parsley-minlength-message="Phone number must be at least 10 digits long." autocomplete="off"> */ ?>
            <input class="form-control" type="text" value="<?php echo $details['phone']?$details['phone']:'';?>" id="phone" name="phone" placeholder="Phone Number" value="" maxlength="12" data-parsley-required data-parsley-required-message="Enter your phone number" data-parsley-minlength="10" data-parsley-minlength-message="Phone number must be at least 10 digits long" autocomplete="off">
        </div>

        <div class="col-md-6 col-sm-6 form-group">
            <label>State<span class="mandatory"></span></label>
            <input class="form-control" type="text" disabled value="<?php echo $details['state_name']?$details['state_name']:'';?>">
            
        </div>
        <div class="col-md-6 col-sm-6 form-group">
            <label>District<span class="mandatory"></span></label>
            <input class="form-control" type="text" disabled value="<?php echo $details['district_name']?$details['district_name']:'';?>">
        </div>
        <div class="col-sm-12 text-center mt-3">
            <button type="button" id="submit_form" class="btn btn-primary" onclick="return form_submit('profile_form');">Update</button>
        </div>
    </div>
</form>

<div id="uploadimageModal" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Crop Image</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div id="image_demo" style=" margin-top:10px"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary crop_image">Crop & Upload Image</button>
       <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </div>
    </div>
  </div>
</div>
<script src="assets/front/js/jquerymask.min.js"></script>
<script>

    $('#phone').mask('000-000-0000');

   var imgerror=0;
    function renderImage(seq) {
        $("#img_err").html('');
        //$("#loader-wrapper").show();
        var file = event.target.files[0];
        var fileReader = new FileReader();
        if(!file.type.match('image')) {
            $("#loader-wrapper").hide();
            $("#fileUpload_"+seq+"").val('');
            imgerror=1;
            $("#img_err").html("Image must be a jpg, jpeg or png.");
            return false;
        }else{
            imgerror=0;
        }
        if (file.type.match('image')) {
            var FileExt = file.name.substr(file.name.lastIndexOf('.') + 1);
            if ((FileExt.toUpperCase() != "JPG" && FileExt.toUpperCase() != "JPEG" && FileExt.toUpperCase() != "PNG")) {
                    $("#loader-wrapper").hide();
                    $("#fileUpload_"+seq+"").val('');
                    imgerror=1;
                    $("#img_err").html("File must be jpg or jpeg or png");
                    return false;
            }else{
                imgerror=0;
            }
            fileReader.onload = function() {
              $("#img_src_"+seq+"").attr('src', fileReader.result);
              $("#loader-wrapper").hide();
            };
            fileReader.readAsDataURL(file);
        } 
    }

function form_submit(id)
{   if(imgerror==0){
     submitDetailsForm(id,'<?php echo $form_action;?>');
    }
}   


</script>



<script>  
$(document).ready(function(){

 $image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:200,
      height:200,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  });

  $('#fileUpload_1').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });

  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
       
       $("#img_src_1").attr("src", response); 
       $("#imageinput").val(response);
       
       $('#uploadimageModal').modal('hide')
    })
  });

});  
</script>
