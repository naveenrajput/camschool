<?php
    // echo '<pre>';
    // print_r($media_list); 

    if($media_list && count($media_list)>0){

        foreach($media_list as $list){
            $date = convertGMTToLocalTimezone($list['created'],$showTime=false, $only_time=false,"", $fulltime =FALSE); ?>

            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 media_vid">
                <a href="video-fulldetail/<?php echo $list['media_slug']; ?>">
                    <div class="card timeline height-fixed">
                        <div class="video-block">
                            <img class="card-img-top" src="<?php echo $list['thumb_image']; ?>">
                            <span><?php echo  ucfirst($list['duration']); ?>
                                <i class="fa fa-play"></i>
                            </span>
                        </div>
                        <div class="card-block">
                            <div class="card-text">
                                <h3 class="m-0"><?php echo ucfirst($list['title']); ?></h3>
                                <span class="f-12 text-black-50 "><?php echo strlen($list['school_name']>30)?substr(ucfirst($list['school_name']),0,32).'...':ucfirst($list['school_name']);?></span>
                            </div>
                            <figure class="profile profile-inline">
                                <img src="<?php echo $list['profile_image']?$list['profile_image']:"assets/front/images/person.jpg"; ?>" class="profile-avatar" alt="">
                            </figure>
                            <h3 class="card-title w-70">

                            <span class="f-12">  <?php if($this->session->userdata("front_user_type")==2 || $this->session->userdata("front_user_type")==1){
                                // echo  ucwords($list['first_name'].' '.$list['last_name']);
                                if(strlen($list['first_name'].' '.$list['last_name'])>22){
                                    echo substr(ucwords($list['first_name'].' '.$list['last_name']),0,20).'...';
                                }else{
                                    echo ucwords($list['first_name'].' '.$list['last_name']);
                                }
                                
                            }else{
                                // echo  ucfirst($list['display_name']);
                                echo strlen($list['display_name'])>22?substr(ucfirst($list['display_name']),0,20).'...':ucfirst($list['display_name']);
                            }//echo  ucfirst($list['display_name']); ?>  </span>
                          
                                <span class="f-12"><?php echo ucfirst($list['grade_display_name']); ?> </span>
                                <span class="f-11"><?php echo $date; ?></span>
                            </h3>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </a>
            </div>

<?php } } ?>