<section class="content">
    <div class="container">
        <div class="row">
            <?php include APPPATH.'views/front/include/sidebar.php';  ?>
            <div class="col-md-12">
                <div class="main-body">
                    <p class="alert_message" id="msg" style="display:none;"></p>
                    <div class="content-header">
                        <h2 class="company-name">My Profile</h2>
                    </div>
                    
                    <div class="content-body" id="profile_content">
                        
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/croppie.css" />
<script src="<?php echo base_url();?>assets/js/croppie.js"></script>
<script>
    $( document ).ready(function() {
        //$("#loader-wrapper").show();
         $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>api/get-user-profile",
            data: {type:'web'},
            headers: { 'apikey': '<?php echo APP_KEY ?>' },
            success: function(response) { 
               // $("#loader-wrapper").hide();
              try {
                    var data =$.parseJSON(response)
                    if(data.status==4){
                         location.reload(); 
                    }
                } catch(e) {
                   
                     $("#profile_content").html(response);
                }
            },error: function(){
                $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Some error occured in loading the data.</div>');
                $('#msg').css('display','block');
                    setTimeout(function() {
                    $('#msg').fadeOut('slow');
                }, 3000); 
            }
        });
    });
</script>