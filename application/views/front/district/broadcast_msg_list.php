<?php
    if($details && count($details) >0){ 
        // echo '<pre>';
        // print_r($this->session->userdata()); 
        $front_user_type = $this->session->userdata('front_user_type');

        switch ($front_user_type) {
            case 1:
              $name = "Principal/school";
              break;
            case 2:
              $name = "Teacher";
              break;
            case 3:
              $name = "Student";
              break;   
            default:
               $name = "";
          }
        ?>
            
            <table class="table table-responsive-sm table-responsive-md table-hover"  id="messagelist">
                <thead>
                    <tr>
                    <th width="25%">Subject</th>
                   <!--  <th width="40%">Message</th> -->
                    <th width="25%">Date/Time</th>
                    <th width="10%">Action</th>
                    </tr>
                </thead>
         

            <?php
                foreach($details as $list){ 
                    $email = [];
                    $emails = '';
                    $date = convertGMTToLocalTimezone($list['created'],$showTime=true, $only_time=false,"", $fulltime =TRUE);

                   // $date = str_replace(" "," . ",$date);

                    foreach($list['email_participant'] as $emailList){        
                            array_push($email, $emailList['email']);
                    }
                    $emails = implode(" ",$email);
                    
                    ?>
                    <tbody>
                    <tr>
                        <td>
                          <?php if($list['subject']){ 
                            echo ucfirst(wordwrap($list['subject'],35,"<br>\n",TRUE));
                          }?>
                              
                        </td>
                        <!-- <td><?php //if($list['message']){ echo $list['message']; }?></td> -->
                        <td><?php echo $date; ?></td>
                        <td>
                            <a href="<?php echo base_url()."broadcast_details/".$list['notification_id']?>"><i class="fa fa-eye"></i></a> 
                            <a href="javascript:void(0)" onclick="deleteBroadMsg(this, <?php echo $list['notification_id']; ?>)"><i class="fa fa-trash"></i></a> 
                        </td>
                    </tr>
                    </tbody>
            <?php } ?>
                
           </table>

    <?php }/*else{ ?>
        <div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>No Data Found</div>
    <?php }*/
?>
                       
                    