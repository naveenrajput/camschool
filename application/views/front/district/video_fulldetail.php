
<section class="content">
    <div class="container">
        <div class="row">
            <?php include APPPATH.'views/front/include/sidebar.php'; ?>
            <div class="col-md-12 col-sm-12 col-xs-12" id="videoDetail">
                
            </div>

        </div>
    </div>
</section>

<script>
var media_id = `<?php echo $media_id; ?>`;
// var grade = "<?php // if(isset($grade['grade_id'])){echo $grade['grade_id'];}else{ echo $grade;}?>";
var grade = "<?php echo isset($grade_n)?$grade_n:0; ?>";
getvideoDetails();

function getvideoDetails(){
    // $('#videoDetail').empty();
    // $("#loader-wrapper").show();
        
    var user_id = `<?php echo $this->session->userdata('user_id'); ?>`;
    var data = {type:'web', user_id, media_id, call_type:"other"};
   

    // profile_controller/get_comments
    if(media_id && user_id){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>api/video-details",
            data: data,
            //dataType:'JSON',
            headers: { 'apikey': '<?php echo APP_KEY ?>' },
            success: function(response) { 
                $("#loader-wrapper").hide();  
                $('#videoDetail').html(response);
                get_comments(0, 10, 0, 2, 0, '', 'main');
                  
            },error: function(){
             
                $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Some error occured in loading the data.</div>');
                $('#msg').css('display','block');
                    setTimeout(function() {
                    $('#msg').fadeOut('slow');
                }, 3000); 
                get_comments(0, 10, 0, 2, 0, '', 'main');
            }
        });
        
    }else{
        get_comments(0, 10, 0, 2, 0, '', 'main');
    }
     
};

// get_comments(0, 10, 0, 2, 0, '', 'main');
function get_comments(parentId, limit, page, innerlimit, innerpage, requestType, type, loadmore = "") {
    // $("#loader-wrapper").show();
    // event.preventDefault();
    $.ajax({ 
        type: "POST",
        url: "<?php echo base_url(); ?>common/get_comments",
        data: {
            'type': 'web',
            'media_id': media_id,
            'parent_id': parentId,
            'type': 'web',
            'limit': limit,
            'innerlimit': innerlimit,
            'page': page,
            'innerpage': innerpage,
            'request_type': requestType,
            'grade': grade,
        },
        headers: { 'apikey': '<?php echo APP_KEY ?>' },
        dataType:'JSON', 
        success: function(response) {  
            $("#loader-wrapper").hide();
            if(response.status == '1'){
                let container = (type == 'inner')? "#loadMore"+parentId: "#all_post_comment";
                if(loadmore == 1){
                    $("#load-more-"+parentId).remove();
                    $(container).append(response.html);
                }else{
                    $(container).html(response.html);
                }
                $("#total_comments_dsiaply").html(response.total_comments+' Comments');
                
                $(".ajax_loader").hide();
            }else{
                $(".denger_msg").html('Some error occured. Please try again !!');
                $(".ajax_loader").hide();
            }
        }
    });
}

function loadMoreComments(parentId, limit, page, innerlimit, innerpage, requestType, type, loadmore){
    get_comments(parentId, limit, page, innerlimit, innerpage, requestType, type, loadmore);
}
</script>