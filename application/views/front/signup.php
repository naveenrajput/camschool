 
    <section class="content signup-page">
        <div class="container">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 right-form">
                <div class="row">
                    <div class="col-md-12">
                        <label>Select User Type</label>
                    </div>
                    <!-- Nav pills -->
                    <div class="pl-3">
                        <?php 
                        $activecls="";
                        if(isset($_GET['user']) && !empty($_GET['user'])){
                            $activecls=$_GET['user'];
                        }
                        ?>
                        <div style="" class="switch-field">
                            <ul class="nav nav-pills">
                                <!-- <li class="nav-item">
                                    <a class="nav-link active" data-toggle="pill" href="#parent">Parental</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="pill" href="#teacher">Teacher</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="pill" href="#principal">Principal</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="pill" href="#district">District</a>
                                </li> -->
                                <li class="nav-item" onclick="location.href='<?php echo site_url('signup');?>'">
                                    <a class="nav-link <?php echo ($activecls=='')?'active':'';?>" data-toggle="pill" href="<?php echo site_url('signup');?>">Parental</a>
                                </li>
                                <li class="nav-item" onclick="location.href='<?php echo site_url('signup?user=teacher');?>'">
                                    <a class="nav-link <?php echo ($activecls=='teacher')?'active':'';?>" data-toggle="pill" href="<?php echo site_url('signup?user=teacher');?>">Teacher</a>
                                </li>
                                <li class="nav-item" onclick="location.href='<?php echo site_url('signup?user=principal');?>'">
                                    <a class="nav-link <?php echo ($activecls=='principal')?'active':'';?>" data-toggle="pill" href="<?php echo site_url('signup?user=principal');?>">Principal</a>
                                </li>
                                <li class="nav-item" onclick="location.href='<?php echo site_url('signup?user=district');?>'">
                                    <a class="nav-link <?php echo ($activecls=='district')?'active':'';?>" data-toggle="pill" href="<?php echo site_url('signup?user=district');?>">District</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="spacer20"></div>
                <!-- Tab panes -->
                <p class="alert_message" id="msg" style="display:none;"></p>
                <div class="tab-content">
                    <div class="tab-pane container p-0 <?php echo ($activecls=='')?'active':'fade';?>" id="parent">
                        <form id="student_form" class="signup_form" action="" role="form" data-parsley-validate enctype="multipart/form-data">
                            <div class="row">
                                <input type="hidden" name="user_type" value="4">
                                <div class="col-md-6 col-sm-6 form-group">
                                    <select class="form-control" id="s_state_id" name="state_id" data-parsley-required data-parsley-required-message="Select your state" onchange="get_district('s_state_id','s_district_id')">
                                        <option hidden value="">Select State</option>
                                       <?php if(!empty($states)){
                                            foreach($states as $state){ ?>
                                            <option value="<?php echo $state['state_id'];?>" <?php if(isset($details['state_id']) && !empty($details['state_id'])){if($details['state_id']==$state['state_id']){echo 'selected';}} ?>><?php echo $state['state_name'];?></option>
                                        <?php } }?>
                                    </select>
                                </div>        
                                <div class="col-md-6 col-sm-6 form-group">
                                    <select class="form-control" id="s_district_id" name="district_id" data-parsley-required data-parsley-required-message="Select your district" onchange="get_school('s_district_id','s_school_id')">
                                        <option value="" id="s_district_id_first">Select District</option>
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-6 form-group">
                                    <select class="form-control" id="s_school_id" name="school_id" data-parsley-required data-parsley-required-message="Select your school" onchange="get_grade('s_school_id','s_grade_id')">
                                        <option value="" id="s_school_id_first">Select School</option>
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-6 form-group">
                                    <input class="form-control" placeholder="First Name" type="text" id="first_name1" name="first_name" type="text"
                                     maxlength="30"data-parsley-maxlength-message="First name must be at least 30 characters long" data-parsley-required data-parsley-required-message="Enter your first name" oninput="this.value = this.value.replace(/[^A-Za-z-'()& ]/g,'');" autocomplete="off"  />
                                </div>
                                <div class="col-md-6 col-sm-6 form-group">
                                    <input class="form-control" placeholder="Last Name" type="text" id="last_name4" name="last_name" type="text"  maxlength="30"data-parsley-maxlength-message="Last name must be at least 30 characters long" data-parsley-required data-parsley-required-message="Enter your last name" oninput="this.value = this.value.replace(/[^A-Za-z-'()& ]/g,'');" autocomplete="off" />
                                </div>

                                <div class="col-md-6 col-sm-6 form-group">
                                    <input class="form-control" placeholder="Email Address" type="text" name="email" id="email1" data-parsley-required data-parsley-required-message="Enter your email address" data-parsley-type="email" data-parsley-type-message="Enter a valid email address" autocomplete="off" />
                                </div>

                                <div class="col-md-6">
                                    <div class="col-md-12 col-sm-12 p-0 form-group">
                                        <input class="form-control" type="password" id="s_password" name="password" placeholder="Password"  data-parsley-minlength="6" maxlength="30"  data-parsley-required data-parsley-required-message="Create a  password" autocomplete="off" data-parsley-number="1" data-parsley-alphabet="1"  data-parsley-special="1"  data-parsley-uppercase="1" data-parsley-lowercase="1" data-parsley-minlength-message="Password must be at least 6 characters long">
                                        <i id="signup-pass-status-s" class="fa fa-eye-slash" aria-hidden="true" onClick="viewPasswordStudent()"></i>
                                    </div>
                                    <div class="col-md-12 col-sm-12 p-0 form-group">
                                        <input class="form-control" type="password" placeholder="Confirm Password" data-parsley-equalto="#s_password" id="s_password_r" required data-parsley-required data-parsley-required-message="Re-enter your new password" data-parsley-equalto-message="New password and the confirm password doesn't match" autocomplete="off">
                                        <i id="signup-pass-status-sr" class="fa fa-eye-slash" aria-hidden="true" onClick="viewPasswordStudentRepet()"></i>
                                         <input type="hidden" name="perantal_img" id="imageinput" value="">
                                    </div>
                                </div>        
                                
                                <div class="col-md-6 col-sm-6 form-group" id="ocean-file">
                                    <label>Upload a profile picture or use the CAM! avatar</label><br>
                                    <div class="upload-btn-wrapper">
                                        <input type="file" name="profile_pic" id="fileUpload_3" onchange="renderImage(3)"accept="image/x-png,image/gif,image/jpeg">

                                        <span class="upload">
                                            <img src="assets/front/images/cam-logo-signup.png" class="teacher-img teacher-phl" width="40" height="40" alt="" id="img_src_3">

                                        </span>
                                        <p id="img_err_3"></p>
                                    </div>
                                </div>
                                <?php if($activecls==''){?>
                                <div class="checkbox col-md-6 col-sm-6 form-group">
                                  <?php echo $widget.$script;?>
                                   <span id="reerror" style="color:red;"></span> 

                                </div>
                                <?php } ?>

                                <div class="checkbox col-md-6 col-sm-6 ">
                                    <label>
                                        <input type="checkbox" id="is_public_check" checked onchange="publicCheck();"> Allow your profile to be public to the teacher and other parents in your child's class to create groups and for chats.
                                        <input type="hidden" id="public_check_valid" value="1" name="is_public">
                                    </label>

                                    <label>
                                        <input type="checkbox" name="terms" data-parsley-required data-parsley-required-message="Agree to the Terms of Service and Privacy Policy"> By continuing you agree to our
                                         <a href="terms-of-conditions">  Terms of Service</a> and <a href="privacy-policy">Privacy Policy. </a>
                                    </label>
                                </div>
                                <div class="col-sm-12 text-center">
                                   
                                   <!--  <input type="button" onclick="return submitSignupForm('student_form','student','reerror');" class="btn btn-primary"value="Submit" id="buttonLock"> -->
                                    <a href="javascript:void(0);" onclick="return submitSignupForm('student_form','student','reerror');" class="btn btn-primary " id="student_btn">Register</a> 
                                </div>
                                <!-- <h6 class="text-center col-sm-12 f-14 mt-4">Already have an account? <a href="parent/login.php" class="link-btn"> <u>Login.</u></a></h6> -->
                            </div>
                        </form>
                    </div>

                    <div class="tab-pane container p-0  <?php echo ($activecls=='teacher')?'active':'fade';?>" id="teacher">
                        <form id="teacher_form" class="signup_form" action="" role="form" data-parsley-validate enctype="multipart/form-data">
                            <div class="row">
                                <input type="hidden" name="user_type" value="3">
                                <div class="col-md-6 col-sm-6 form-group">
                                    <select class="form-control" id="t_state_id" name="state_id" data-parsley-required data-parsley-required-message="Select your state" onchange="get_district('t_state_id','t_district_id')">
                                        <option hidden value="">Select State</option>
                                       <?php if(!empty($states)){
                                            foreach($states as $state){ ?>
                                            <option value="<?php echo $state['state_id'];?>" <?php if(isset($details['state_id']) && !empty($details['state_id'])){if($details['state_id']==$state['state_id']){echo 'selected';}} ?>><?php echo $state['state_name'];?></option>
                                        <?php } }?>
                                    </select>
                                </div>                               
                                <div class="col-md-6 col-sm-6 form-group">
                                    <select class="form-control" id="t_district_id" name="district_id" data-parsley-required data-parsley-required-message="Select your district" onchange="get_school('t_district_id','t_school_id')">
                                        <option value="" id="t_district_id_first">Select District</option>
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-6 form-group">
                                    <select class="form-control" id="t_school_id" name="school_id" data-parsley-required data-parsley-required-message="Select your school" onchange="get_grade('t_school_id','t_grade_id')">
                                        <option value="" id="t_school_id_first">Select School</option>
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-6 form-group custom-multi-select">
                                    <select class="form-control select2" id="t_grade_id" name="grade[]" data-parsley-required data-parsley-required-message="Select your grade" data-parsley-errors-container='#t_grade_error' multiple="multiple">
                                        <option value="" id="t_grade_id_first">Select Grade</option>
                                    </select>
                                    <div class="error" id="t_grade_error"></div>
                                </div>
                                
                                <div class="col-md-6 col-sm-6 form-group">
                                    <input class="form-control" placeholder="First Name" type="text" id="first_name2" name="first_name" type="text" maxlength="30" data-parsley-required data-parsley-required-message="Enter your first name" oninput="this.value = this.value.replace(/[^A-Za-z-'()& ]/g,'');" autocomplete="off" />
                                </div>
                                <div class="col-md-6 col-sm-6 form-group">
                                    <input class="form-control" placeholder="Last Name" type="text" id="last_name1" name="last_name" type="text" maxlength="30" data-parsley-required data-parsley-required-message="Enter your last name" oninput="this.value = this.value.replace(/[^A-Za-z-'()& ]/g,'');" autocomplete="off" />
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-12 col-sm-12 p-0 form-group">
                                        <input class="form-control email" placeholder="Email Address" type="text" name="email" id="email2" data-parsley-required data-parsley-required-message="Enter your email address" data-parsley-type="email" data-parsley-type-message="Enter a valid email address" autocomplete="off" />
                                    </div>
                                    <div class="col-md-12 col-sm-12 p-0 form-group">
                                        <!-- <label>Password</label> -->
                                       <input class="form-control" type="password" id="t_password" name="password" placeholder="Password"  data-parsley-minlength="6" maxlength="30"  data-parsley-required data-parsley-required-message="Create a  password" autocomplete="off" data-parsley-number="1" data-parsley-alphabet="1"  data-parsley-special="1"  data-parsley-uppercase="1" data-parsley-lowercase="1" data-parsley-minlength-message="Password must be at least 6 characters long">
                                        <i id="signup-pass-status-t" class="fa fa-eye-slash" aria-hidden="true" onClick="viewPasswordTeacher()"></i>
                                    </div>
                                    <div class="col-md-12 col-sm-12 p-0 form-group">
                                        <input class="form-control" type="password" placeholder="Confirm Password" data-parsley-equalto="#t_password" id="t_password_r" required data-parsley-required data-parsley-required-message="Re-enter your new password" data-parsley-equalto-message="New password and the confirm password doesn't match" autocomplete="off">
                                        <i id="signup-pass-status-tr" class="fa fa-eye-slash" aria-hidden="true" onClick="viewPasswordTeacherRepet()"></i>
                                    </div>
                                </div>        
                                <div class="col-md-6 col-sm-6 form-group" id="ocean-file">
                                    <label>Upload a profile picture or use the CAM! avatar</label><br>
                                    <div class="upload-btn-wrapper">
                                        <input type="file" name="profile_pic" id="fileUpload_1" onchange="renderImage(1)" accept="image/x-png,image/gif,image/jpeg">
                                        <input type="hidden" name="techer_img"id="techer_img">
                                        <span class="upload">
                                            <img src="assets/front/images/cam-logo-signup.png" class="teacher-img" width="40" height="40" alt="" id="img_src_1">
                                        </span>
                                        <p id="img_err_1"></p>
                                    </div>
                                </div>
                                <?php if($activecls=='teacher'){?>
                                <div class="checkbox col-md-6 col-sm-6 form-group">
                                   <!--  <img src="assets/front/images/captcha.png" class="img-fluid" width="250" /> -->
                                   <?php echo $widget.$script;?>
                                    <span id="reerror" style="color:red;"></span> 
                                </div>
                                <?php } ?>
                                <div class="checkbox col-md-6 col-sm-6 ">
                                    <label>
                                        <input type="checkbox" name="terms" data-parsley-required data-parsley-required-message="Agree to the Terms of Service and Privacy Policy"> By continuing you agree to our
                                        <a href="terms-of-conditions">  Terms of Service</a> and <a href="privacy-policy">Privacy Policy. </a>
                                    </label>
                                </div>
                                <div class="col-sm-12 text-center">
                                    <a href="javascript:void(0);" onclick="return submitSignupForm('teacher_form','teacher','reerror');" class="btn btn-primary " id="teacher_btn">Register</a> 


                                </div>
                                 
                                <!-- <h6 class="text-center col-sm-12 f-14 mt-4">Already have an account? <a href="teacher/login.php" class="link-btn"> <u>Login.</u></a></h6> -->
                            </div>
                        </form>
                    </div>

                    <div class="tab-pane container p-0  <?php echo ($activecls=='principal')?'active':'fade';?>" id="principal">
                        
                        <form id="principal_form" class="signup_form" action="" role="form" data-parsley-validate enctype="multipart/form-data">
                            <div class="row">
                                <input type="hidden" name="user_type" value="2">
                                <div class="col-md-6 col-sm-6 form-group">
                                    <select class="form-control" id="state_id" name="state_id" data-parsley-required data-parsley-required-message="Select your state" onchange="get_district('state_id','district_id')">
                                        <option hidden value="">Select State</option>
                                       <?php if(!empty($states)){
                                            foreach($states as $state){ ?>
                                            <option value="<?php echo $state['state_id'];?>" <?php if(isset($details['state_id']) && !empty($details['state_id'])){if($details['state_id']==$state['state_id']){echo 'selected';}} ?>><?php echo $state['state_name'];?></option>
                                        <?php } }?>
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-6 form-group">
                                    <select class="form-control" id="district_id" name="district_id" data-parsley-required data-parsley-required-message="Select your district" onchange="get_school('district_id','p_school_id','signup')">
                                        <option value="" id="district_id_first">Select District</option>
                                    </select>
                                </div>
                               <!--  <div class="col-md-6 col-sm-6 form-group">
                                    <input class="form-control" name="school_name" placeholder="School Name" type="text" type="text" maxlength="100" data-parsley-required data-parsley-required-message="Enter your school name."/>
                                </div> -->
                                 <div class="col-md-6 col-sm-6 form-group">
                                    <select class="form-control" id="p_school_id" name="school_name" data-parsley-required data-parsley-required-message="Select your school"  maxlength="50">
                                        <option value="" id="p_school_id_first">Select School</option>
                                    </select>
                                </div>
                                <!-- <div class="col-md-6 col-sm-6 form-group">
                                    <select class="form-control select2" id="grade" name= "grade[]" data-parsley-required data-parsley-required-message="Select your grade(s)" multiple="multiple" data-parsley-errors-container='#sch_grade_err'>
                                        <option  value="" >Select Grade</option>
                                        <?php /*if(!empty(SCHOOL_GRADE)){
                                            foreach(SCHOOL_GRADE as $school_grade){ ?>
                                             <option value="<?php echo $school_grade['grade'];?>"><?php echo $school_grade['grade_display_name'];?></option>
                                        <?php } }*/ ?>
                                    </select>
                                    <div id="sch_grade_err"></div>
                                </div> -->
                                <div class="col-md-6 col-sm-6 form-group">
                                    <input class="form-control" placeholder="First Name" type="text" id="first_name3" name="first_name" type="text"  maxlength="30"data-parsley-maxlength-message="First name must be at least 30 characters long" data-parsley-required data-parsley-required-message="Enter your first name" oninput="this.value = this.value.replace(/[^A-Za-z-'()& ]/g,'');" autocomplete="off" />
                                </div>
                                <div class="col-md-6 col-sm-6 form-group">
                                    <input class="form-control" placeholder="Last Name" type="text" id="last_name2" name="last_name" type="text"  maxlength="30"data-parsley-maxlength-message="Last name must be at least 30 characters long" data-parsley-required data-parsley-required-message="Enter your last name" oninput="this.value = this.value.replace(/[^A-Za-z-'()& ]/g,'');" autocomplete="off" />
                                </div>
                                <div class="col-md-6 col-sm-6 form-group">
                                   <input class="form-control phone" type="text"  id="phone1" name="phone" placeholder="Phone Number" value=""  maxlength="12"   data-parsley-minlength="10" data-parsley-minlength-message="Phone number must be at least 10 digits long." autocomplete="off" data-parsley-required data-parsley-required-message="Enter your phone number">
                                </div>
                                <div class="col-md-6 col-sm-6 form-group">
                                    <input class="form-control email" placeholder="Email Address" type="text" name="email" id="email3" data-parsley-required data-parsley-required-message="Enter your email address" data-parsley-type="email" data-parsley-type-message="Enter a valid email address" autocomplete="off" />
                                </div>                                
                                <div class="col-md-6">
                                    <div class="col-md-12 col-sm-12 p-0 form-group">
                                        <input class="form-control" type="password" id="p_password" name="password" placeholder="Password"  data-parsley-minlength="6" maxlength="30"  data-parsley-required data-parsley-required-message=" Create a  password" autocomplete="off" data-parsley-number="1" data-parsley-alphabet="1"  data-parsley-special="1"  data-parsley-uppercase="1" data-parsley-lowercase="1" data-parsley-minlength-message="Password must be at least 6 characters long">
                                        <i id="signup-pass-status-p" class="fa fa-eye-slash" aria-hidden="true" onClick="viewPasswordPrincipal()"></i>
                                    </div>  
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-12 col-sm-12 p-0 form-group">
                                        <input class="form-control" type="password" placeholder="Confirm Password" data-parsley-equalto="#p_password" id="p_password_r" required data-parsley-required data-parsley-required-message="Re-enter your new password" data-parsley-equalto-message="New password and the confirm password doesn't match" autocomplete="off">
                                        <i id="signup-pass-status-pr" class="fa fa-eye-slash" aria-hidden="true" onClick="viewPasswordPrincipalRepet()"></i>
                                    </div>
                                    <?php if($activecls=='principal'){?>
                                    <div class="checkbox col-md-12 col-sm-12 form-group p-0">
                                        <!-- <img src="assets/front/images/captcha.png" class="img-fluid" width="250" /> -->
                                        <?php echo $widget.$script;?>
                                        <span id="reerror" style="color:red;"></span>  
                                    </div>
                                    <?php } ?>
                                </div>        

                                <div class="col-md-6 col-sm-6 form-group" id="ocean-file">
                                    <label>Upload a profile picture or use the CAM! avatar</label><br>
                                    <div class="upload-btn-wrapper">
                                        <input type="file" name="profile_pic" id="fileUpload_2" onchange="renderImage(2)"accept="image/x-png,image/gif,image/jpeg" >
                                        <input type="hidden" name="principal_img" id="principal_img">
                                        <span class="upload">
                                            <img src="assets/front/images/cam-logo-signup.png" class="teacher-img" width="40" height="40" alt="" id="img_src_2">
                                        </span>
                                        <p id="img_err_2"></p>
                                    </div>
                                </div>
                               
                                <div class="checkbox col-md-12 col-sm-12 form-group">
                                    <label>
                                    <input type="checkbox" name="terms" data-parsley-required data-parsley-required-message="Agree to the Terms of Service and Privacy Policy"> By continuing you agree to our
                                        <a href="terms-of-conditions">  Terms of Service</a> and <a href="privacy-policy">Privacy Policy. </a>
                                </div>
                                <div class="col-sm-12 text-center">
                                   <a href="javascript:void(0);" onclick="return submitSignupForm('principal_form','principal','reerror');" class="btn btn-primary" id="principal_btn">Register</a> 
                                    <!-- <a href="#" data-toggle="modal" data-target="#myModal-confirmation-principal" class="btn btn-primary">Register</a> -->
                                </div>
                               <!--  <h6 class="text-center col-sm-12 f-14 mt-4">Already have an account? <a href="principal/login.php" class="link-btn"> <u>Login.</u></a></h6> -->
                            </div>
                        </form>
                    </div>

                    <div class="tab-pane container p-0  <?php echo ($activecls=='district')?'active':'fade';?>" id="district">
                        <form id="district_form" class="signup_form" action="" role="form" data-parsley-validate enctype="multipart/form-data">
                            <div class="row">
                                <input type="hidden" name="user_type" value="1">
                                <div class="col-md-6 col-sm-6 form-group">
                                    <select class="form-control" id="exampleSelect" name="state_id" data-parsley-required data-parsley-required-message="Select your state" >
                                        <option hidden value="">Select State</option>
                                       <?php if(!empty($states)){
                                            foreach($states as $state){ ?>
                                            <option value="<?php echo $state['state_id'];?>" <?php if(isset($details['state_id']) && !empty($details['state_id'])){if($details['state_id']==$state['state_id']){echo 'selected';}} ?>><?php echo $state['state_name'];?></option>
                                        <?php } }?>
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-6 form-group">
                                    <input class="form-control" placeholder="District Name" id="district_name" name="district_name" type="text" maxlength="50" data-parsley-required data-parsley-required-message="Enter your district" autocomplete="off" />
                                </div>
                                <div class="col-md-6 col-sm-6 form-group">
                                    <input class="form-control" placeholder="First Name" type="text" id="first_name4" name="first_name" type="text" maxlength="30" data-parsley-required data-parsley-required-message="Enter your first name" oninput="this.value = this.value.replace(/[^A-Za-z-'()& ]/g,'');" autocomplete="off" />
                                </div>
                                <div class="col-md-6 col-sm-6 form-group">
                                    <input class="form-control" placeholder="Last Name" type="text" id="last_name3" name="last_name" type="text" maxlength="30" data-parsley-required data-parsley-required-message="Enter your last name" oninput="this.value = this.value.replace(/[^A-Za-z-'()& ]/g,'');" autocomplete="off" />
                                </div>
                                <div class="col-md-6 col-sm-6 form-group">
                                    <input class="form-control" placeholder="Email Address" type="text" name="email" id="email4" data-parsley-required data-parsley-required-message="Enter your email address" data-parsley-type="email" data-parsley-type-message="Enter a valid email address" autocomplete="off" />
                                     <p class="error" id="access_email_error" style="display:none;">Email already used.</p>
                                </div>
                                <div class="col-md-6 col-sm-6 form-group">
                                    <input class="form-control phone" type="text"  id="phone2" name="phone" placeholder="Phone Number" value="" 
                                         maxlength="10" 
                                       data-parsley-minlength="10" data-parsley-minlength-message="Phone number must be at least 10 digits long." autocomplete="off" data-parsley-required data-parsley-required-message="Enter your phone number">
                                       <!--    <input class="form-control" type="text"  id="phone" name="phone" oninput="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'');" onKeyup='addDashes(this)' placeholder="Phone Number" value="" 
                                         maxlength="12" 
                                       data-parsley-minlength="10" data-parsley-minlength-message="Phone number must be at least 10 digits long." autocomplete="off" data-parsley-required data-parsley-required-message="Enter your phone number"> -->
                                </div>
                                <div class="col-md-12 col-sm-12 form-group">
                                    <textarea class="form-control" rows="3" cols="10" placeholder="Ask your questions here..." name="ask_question" id="ask_question" maxlength="250" data-parsley-required data-parsley-required-message="What information can we provide you?"></textarea>
                                </div>  
                                <?php if($activecls=='district'){?>   
                                <div class="checkbox col-md-6 col-sm-6 form-group">
                                     <?php echo $widget.$script;?>
                                     
                                      
                                   <!--  <img src="assets/front/images/captcha.png" id="html_element" class="img-fluid" width="250" /> -->
                                    <span id="reerror" style="color:red;"></span> 
                                </div>
                                <?php } ?>
                                <div class="checkbox col-md-6 col-sm-6 ">
                                    
                                    <label>
                                        <?php echo WEBSITE_NAME;?> <a href="terms-of-conditions">Terms of Service</a> and <a href="privacy-policy">Privacy Policy. </a>
                                    </label>
                                </div>
                                <div class="col-sm-12 text-center">
                                    <a href="javascript:void(0);" onclick="return submitSignupForm('district_form','district','recaptcha_district','reerror');" class="btn btn-primary" id="district_btn" >Submit</a>
                                </div>
                                <!-- <h6 class="text-center col-sm-12 f-14 mt-4">Already have an account? <a href="login" class="link-btn"> <u>Login.</u></a></h6> -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Modal confirmation-parent -->
    <div class="modal fade" id="myModal-confirmation-parent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title" id="myModalLabel"><?php echo WEBSITE_NAME;?></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <p>Thank you for registering with <?php echo WEBSITE_NAME;?>. Click on the link sent to your email address to complete the registration process. Check your SPAM folder and whitelist emails from <a href="mailto:info@chatatmeschools.com">info@chatatmeschools.com</a>.</p>
                    <!-- <div class="col-sm-12 text-center"> <a href="index.php" class="btn btn-primary">Login</a></div> -->
                  

                </div>
            </div>
        </div>
    </div>
    <!-- Modal confirmation-teacher -->
    <div class="modal fade" id="myModal-confirmation-teacher" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title" id="myModalLabel"><?php echo WEBSITE_NAME;?></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <p>Thank you for registering with <?php echo WEBSITE_NAME;?>. Please click on the link sent to your registered email address to complete the registration process. After verification the information will be sent to your school’s principal for approval. If your account is not active within 24 hours, contact your principal or email us at <a href="teachers@chatatmeschools.com">teachers@chatatmeschools.com.</a></p>
                    <!-- <div class="col-sm-12 text-center"> <a href="index.php" class="btn btn-primary">Login</a></div> -->
                </div>
            </div>
        </div>
    </div>
    <!-- Modal confirmation-principal -->
    <div class="modal fade" id="myModal-confirmation-principal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title" id="myModalLabel"><?php echo WEBSITE_NAME;?></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                  <!--   <p>principals@chatatmeschools.com</a>. -->
                    Thank you for registering with <?php echo WEBSITE_NAME;?>. Please click on the link sent to your registered email address to complete the registration process. After verification the information will be sent to your district administrator for approval. If your account is not active within 24 hours, contact your principal or email us at <a href="mailto:principals@chatatmeschools.com">principals@chatatmeschools.com.</a></p>
                    <!-- <div class="col-sm-12 text-center"> <a href="index.php" class="btn btn-primary">Login</a></div> -->
                </div>
            </div>
        </div>
    </div>
    <!-- Modal confirmation-district -->
    <div class="modal fade" id="myModal-confirmation-district" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title" id="myModalLabel"><?php echo WEBSITE_NAME;?></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <p>Thank you for your interest in <?php echo WEBSITE_NAME;?>. We received your information and will contact you within 24 hours.</p>
                </div>
            </div>
        </div>
    </div>

    <div id="uploadimageModal" class="modal" rolee="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Crop Image</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div id="image_demo" style=" margin-top:10px"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary crop_image">Crop & Upload Image</button>
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </div>
    </div>
  </div>
</div>
<div id="uploadimageModal3" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Crop Image</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div id="image_demo1" style=" margin-top:10px"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary crop_image2">Crop & Upload Image</button>
       <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </div>
    </div>
  </div>
</div>
<div id="uploadimageModal2" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Crop Image</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div id="image_demo2" style=" margin-top:10px"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary crop_image3">Crop & Upload Image</button>
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </div>
    </div>
  </div>
</div>

<script src="assets/front/js/jquerymask.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/croppie.css" />
<script src="<?php echo base_url();?>assets/js/croppie.js"></script>
<script>  
$(document).ready(function(){
//$('#myModal-confirmation-principal').modal('show');
 $image_crop1 = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:200,
      height:200,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  }); 
 $image_crop = $('#image_demo1').croppie({
    enableExif: true,
    viewport: {
      width:200,
      height:200,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  });  
 $image_crop2 = $('#image_demo2').croppie({
    enableExif: true,
    viewport: {
      width:200,
      height:200,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  }); 


  $('#fileUpload_3').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop1.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });

  $('.crop_image').click(function(event){
    $image_crop1.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
       
       $("#img_src_3").attr("src", response); 
       $("#imageinput").val(response);
       
       $('#uploadimageModal').modal('hide')
    })
  });

});  



  $('#fileUpload_1').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal3').modal('show');
  });

  $('.crop_image2').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
       
       $("#img_src_1").attr("src", response); 
       $("#techer_img").val(response);
       
       $('#uploadimageModal3').modal('hide')
    })
  });

  $('#fileUpload_2').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop2.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal2').modal('show');
  });

  $('.crop_image3').click(function(event){
    $image_crop2.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
       
       $("#img_src_2").attr("src", response); 
       $("#principal_img").val(response);
       
       $('#uploadimageModal2').modal('hide')
    })
  });

</script>
<script>

    $('.phone').mask('000-000-0000');
</script>
<script type="text/javascript">
    $('#s_district_id').attr('disabled', 'disabled'); 
    $('#s_school_id').attr('disabled', 'disabled'); 

    $('#t_district_id').attr('disabled', 'disabled'); 
    $('#t_school_id').attr('disabled', 'disabled');

    $('#district_id').attr('disabled', 'disabled'); 
    $('#p_school_id').attr('disabled', 'disabled');

    $(document).ready(function() {
        $('.select2').select2({
             placeholder: 'Select Grade',
             multiple:true,
        });
    });
    function get_district(id,replace_id){  
        var state_id = $("#"+id).val();
        if(state_id!='') {
            $('#'+replace_id+' option').slice(1).remove();
            $("#"+replace_id).attr('disabled', 'disabled'); 
            $.ajax({
                type:'POST',
                url: "api/district-list",
                data: {state_id:state_id},
                headers: { 'apikey': '<?php echo APP_KEY ?>' },
                success:function(data)
                {
                    if(data) {
                        var district="";
                        data = JSON.parse(data);
                        var details_length=data.details.length;
                        for(var i=0;i<details_length;i++) {
                            district += "<option value="+data['details'][i]['id']+">"+data['details'][i]['name']+"</option>";
                        }
                        $("#"+replace_id).find("#"+replace_id+"_first").after(district);
                        $("#"+replace_id).removeAttr('disabled'); 
                    } else {

                        $("#"+replace_id).find("#"+replace_id+"_first").after('<option value="">No Record</option>');
                    }
                }
            });
        }
    }
    function enableBtn(){
         var response = grecaptcha.getResponse();
        if(response.length == 0) {
            $("#reerror").html('Select reCaptcha');
            return false;
        }else{
            $("#reerror").html('');
        }
    }
    function get_school(id,replace_id,call_type=""){
        var district_id = $("#"+id).val();
        if(district_id!='') {
            $('#'+replace_id+' option').slice(1).remove();
            $("#"+replace_id).attr('disabled', 'disabled'); 
            $.ajax({
                type:'POST',
                url: "api/school-list",
                data: {district_id:district_id,call_type:call_type},
                headers: { 'apikey': '<?php echo APP_KEY ?>' },
                success:function(data)
                {
                    if(data) {
                        var school="";
                        data = JSON.parse(data);
                        var details_length=data.details.length;
                        for(var i=0;i<details_length;i++) {
                            school += "<option value="+data['details'][i]['id']+">"+data['details'][i]['name']+"</option>";
                        }
                        $("#"+replace_id).removeAttr('disabled');
                        $("#"+replace_id).find("#"+replace_id+"_first").after(school);
                    } else {
                        $("#"+replace_id).find("#"+replace_id+"_first").after('<option value="">No Record</option>');
                    }
                }
            });
        }
    }

    function get_grade(id,replace_id){
        var school_id = $("#"+id).val();
        if(school_id!='') {
            $('#'+replace_id+' option').slice(1).remove();
            $("#"+replace_id).attr('disabled', 'disabled'); 
            $.ajax({
                type:'POST',
                url: "api/grade-list",
                data: {school_id:school_id},
                headers: { 'apikey': '<?php echo APP_KEY ?>' },
                success:function(data)
                {
                    if(data) {
                        var school="";
                        data = JSON.parse(data);
                        var details_length=data.details.length;
                        for(var i=0;i<details_length;i++) {
                            school += "<option value="+data['details'][i]['grade']+">"+data['details'][i]['grade_display_name']+"</option>";
                        }
                        $("#"+replace_id).removeAttr('disabled');
                        $("#"+replace_id).find("#"+replace_id+"_first").after(school);
                    } else {
                        $("#"+replace_id).find("#"+replace_id+"_first").after('<option value="">No Record</option>');
                    }
                }
            });
        }
    }
  

   
    function submitSignupForm(id,form_type,rid) { 
      
        if(form_type=='district'){
            sign_up_btn_text='Submit';
        }else{
             sign_up_btn_text='Register';
        }
        var form_action= "api/signup";
        $("#"+id).parsley().validate();
        var form = $('#'+id)[0];
         var response = grecaptcha.getResponse();
        if(response.length == 0) {
            $("#reerror").html('Select reCaptcha');
            return false;
        }else{
            $("#reerror").html('');
        }
        
        if($("#"+id).parsley().isValid()){
          var formData = new FormData(form);
          var email = $(".email").val();

          $("#"+form_type+'_btn').attr('disabled',true);
          formData.append('type','web');
          formData.append('device_type','web');
          $('#'+form_type+'_btn').html('<span class="   fa fa-spinner fa-spin fa-lg mr-2" role="status" aria-hidden="true"></span>Loading...').attr('disabled', true);
          $.ajax({
            url: form_action,
            type: 'POST',
            data: formData,
           // data: {type:'web'},
            headers: { 'apikey': '<?php echo APP_KEY ?>' },
            dataType: 'json',
            // async: false,
            cache: false,
            contentType: false,
            processData: false,
        
            success:function(resp){
             // $('body').addClass('loaded');
              //$("#loader-wrapper").hide(); 
              $('#'+form_type+'_btn').html(sign_up_btn_text).attr('disabled', false);
              $("html, body").animate({ scrollTop: 0 }, "slow");
              $("#msg").show(); 
              $("#"+form_type+'_btn').attr('disabled',false);
                if(resp.status==0){
                    $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                     grecaptcha.reset();
                    setTimeout(function() {
                     $("#msg").html('');
                      $("#msg").hide();
                    }, 3000); 
                  }else{
                    $(".select2").val('#').trigger('change');
                     //$("#msg").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                    $("#"+id).parsley().reset();
                    $("#"+id)[0].reset();
                    $("#img_src_1").attr("src", 'assets/front/images/cam-logo-signup.png'); 
                    $("#img_src_2").attr("src", 'assets/front/images/cam-logo-signup.png'); 
                    $("#img_src_3").attr("src", 'assets/front/images/cam-logo-signup.png');
                    $("#imageinput").val('');
                    $("#techer_img").val('');
                    $("#principal_img").val(''); 
                    setTimeout(function() {
                        /*$("#msg").html('');
                        $("#msg").hide();*/
                        if(form_type=='district'){
                            $('#myModal-confirmation-district').modal('show');
                        }
                        if(form_type=='principal'){
                            $('#myModal-confirmation-principal').modal('show');
                            $('#email').attr("href", email).html(email);
                        }
                        if(form_type=='teacher'){
                            $('#myModal-confirmation-teacher').modal('show');
                             $('#email').attr("href", email).html(email);
                        }
                        if(form_type=='student'){
                            $('#myModal-confirmation-parent').modal('show');
                        }
                    }, 2000); 
                    grecaptcha.reset();
                }
            },
            error:function(err){
               // $("#"+form_type+'_btn').attr('disabled',false);
              $('#'+form_type+'_btn').html(sign_up_btn_text).attr('disabled', false);
            
              //$("#loader-wrapper").hide();
            }
          });
        }
    }
    
    function renderImage(seq) {
        $("#img_err_"+seq+"").html('');
        //$("#loader-wrapper").show();
        var file = event.target.files[0];
        var fileReader = new FileReader();
        if(!file.type.match('image')) {
            $("#loader-wrapper").hide();
            $("#fileUpload_"+seq+"").val('');
            $("#img_err_"+seq+"").html("File must be image");
            return false;
        }
        if (file.type.match('image')) {
            var FileExt = file.name.substr(file.name.lastIndexOf('.') + 1);
            if ((FileExt.toUpperCase() != "JPG" && FileExt.toUpperCase() != "JPEG" && FileExt.toUpperCase() != "PNG")) {
                    $("#loader-wrapper").hide();
                    $("#fileUpload_"+seq+"").val('');
                    $("#img_err_"+seq+"").html("File must be jpg or jpeg or png");
                    return false;
            }
            fileReader.onload = function() {
              $("#img_src_"+seq+"").attr('src', fileReader.result);
              $("#loader-wrapper").hide();
            };
            fileReader.readAsDataURL(file);
        } 
    }
</script>
<script>
function viewPasswordStudent()
{
  var passwordInput = document.getElementById('s_password');
  var passStatus = document.getElementById('signup-pass-status-s');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye-slash';
  }
}

function viewPasswordStudentRepet()
{
  var passwordInput = document.getElementById('s_password_r');
  var passStatus = document.getElementById('signup-pass-status-sr');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye-slash';
  }
}


function viewPasswordTeacher()
{
  var passwordInput = document.getElementById('t_password');
  var passStatus = document.getElementById('signup-pass-status-t');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye-slash';
  }
}
function viewPasswordTeacherRepet()
{
  var passwordInput = document.getElementById('t_password_r');
  var passStatus = document.getElementById('signup-pass-status-tr');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye-slash';
  }
}

function viewPasswordPrincipal()
{
  var passwordInput = document.getElementById('p_password');
  var passStatus = document.getElementById('signup-pass-status-p');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye-slash';
  }
}

function viewPasswordPrincipalRepet()
{
  var passwordInput = document.getElementById('p_password_r');
  var passStatus = document.getElementById('signup-pass-status-pr');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye-slash';
  }
}


function publicCheck() {
    if($("#is_public_check").is(":checked")){
        $('#public_check_valid').val('1');
    } else if($("#is_public_check").is(":not(:checked)")){
        $('#public_check_valid').val('0');
    }

}

</script>
