<section class="content">
    <div class="container">
        <div class="row">
            <?php include APPPATH.'views/front/include/sidebar.php';  ?>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="main-body">
                    <div class="content-header">
                        <h2 class="company-name"><?php echo $page_title?$page_title:"";?></h2>
                    </div>
                    <div class="content-body">
                        <p class="alert_message" id="msg" style="display:none;"></p>
                        <form id="changepw_form" action="" role="form" data-parsley-validate enctype="multipart/form-data" >
                            <div class="row">
                                <!-- <div class="col-md-12 col-sm-12 saprator mt-1 mb-2"></div> -->
                                <div class="col-md-4 col-sm-6 form-group">
                                    <label>Current Password</label>
                                    <input class="form-control" type="password"  name="current_password" id="current_password" placeholder="Current Password" data-parsley-required data-parsley-required-message="Enter your current password" autocomplete="off">
                                    <i id="pass-status-current-password" class="fa fa-eye-slash pass-eye" aria-hidden="true" onClick="viewOldPassword()"></i>
                                </div>
                                <div class="col-md-4 col-sm-6 form-group">
                                    <label>New Password</label>
                                    <input class="form-control" type="password" id="new_password" name="new_password" placeholder="New Password"  data-parsley-minlength="6" maxlength="30"  data-parsley-required data-parsley-required-message="Enter a new password" autocomplete="off" data-parsley-number="1" data-parsley-alphabet="1"  data-parsley-special="1"  data-parsley-uppercase="1" data-parsley-lowercase="1" data-parsley-minlength-message="Password must be at least 6 characters long." data-parsley-notequalto="#current_password" data-parsley-notequalto-message="Current and new password can't be the same.">
                                    <i id="pass-status-new-password" class="fa fa-eye-slash pass-eye" aria-hidden="true" onClick="viewNewPassword()"></i>
                                </div>
                                <div class="col-md-4 col-sm-6 form-group">
                                    <label>Re-enter new Password</label>
                                    <input class="form-control" type="password" placeholder="Re-enter new Password" id="new-repet" data-parsley-equalto="#new_password" required data-parsley-required data-parsley-required-message="Re-enter your new password" data-parsley-equalto-message="New password and the confirm password doesn't match." autocomplete="off">
                                    <i id="pass-status-new-repet" class="fa fa-eye-slash pass-eye" aria-hidden="true" onClick="viewRepetPassword()"></i>
                                </div>
                                <div class="col-sm-12 text-center mt-3">
                                    <button type="button" id="changebtn_form" class="btn btn-primary loader_btn" onclick="return changepw_form_submit('changepw_form');" >Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
  window.Parsley.addValidator("notequalto", {
    requirementType: "string",
    validateString: function(value, element) {
        return value !== $(element).val();
    }
});
function changepw_form_submit(id){
    $("#"+id).parsley().validate();
    var form = $('#'+id)[0];
    if($("#"+id).parsley().isValid()){
        var formData = new FormData(form);
        formData.append('type','web');
        formData.append('is_approve',2);
        //$("#loader-wrapper").show();
        $('.loader_btn').html('<span class="   fa fa-spinner fa-spin fa-lg mr-2" role="status" aria-hidden="true"></span>Loading...').attr('disabled', true);
        $("#changebtn_form").attr('disabled',true);
        $.ajax({
            url: 'api/change-password',
            type: 'POST',
            data: formData,
            dataType: 'json',
            // async: false,
            cache: false,
            contentType: false,
            processData: false,
            headers: { 'apikey': '<?php echo APP_KEY ?>' },
            success:function(resp){
             // $("#loader-wrapper").hide();  
              $('.loader_btn').html('Update').attr('disabled', false);
              $("html, body").animate({ scrollTop: 0 }, "fast");
              $("#msg").show(); 
              $("#changebtn_form").attr('disabled',false);
                if(resp.status==0){
                    $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                    setTimeout(function() {
                     $("#msg").html('');
                      $("#msg").hide();
                    }, 5000); 
                  }else{

                    $("#msg").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                    $("#"+id).parsley().reset();
                    $("#"+id)[0].reset();
                    setTimeout(function() { 
                        $("#msg").html('');
                        $("#msg").hide();
                    }, 2000);
                    if(resp['details']['is_redirect']==1){
                        var user_type='<?php echo $this->session->userdata('front_user_type');?>';
                        if(user_type==2){
                            window.location.href='principal/profile';
                        }if(user_type==3){
                            window.location.href='teacher/profile';
                        }
                        
                    } 
                }
            },
            error:function(err){
                 $('.loader_btn').html('Update').attr('disabled', false);
              /*$("#changebtn_form").attr('disabled',false);
              $("#loader-wrapper").hide();*/
            }
        });
    }
}
</script>
<script>
function viewOldPassword()
{
  var passwordInput = document.getElementById('current_password');
  var passStatus = document.getElementById('pass-status-current-password');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye-slash';
  }
}
function viewNewPassword()
{
  var passwordInput = document.getElementById('new_password');
  var passStatus = document.getElementById('pass-status-new-password');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye-slash';
  }
}
function viewRepetPassword()
{
  var passwordInput = document.getElementById('new-repet');
  var passStatus = document.getElementById('pass-status-new-repet');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye-slash';
  }
}
</script>