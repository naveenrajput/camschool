<section class="content">
    <div class="container">
        <div class="row">
            <?php include APPPATH.'views/front/include/sidebar.php'; ?>
            <div class="col-md-12">
                <div class="main-body">
                    <div class="content-header">
                        <h2 class="company-name">Upload Video<?php if(isset($gradename)){ echo ' - '.$gradename;}?></h2>
                          <a href="<?php echo $back_url;?>" class="btn btn-primary">Back</a>
                    </div>

                    <div class="content-body">
                        <p class="alert_message" id="msg" style="display:none;"></p>
                        <form id="video_form" action="" method="post" role="form" data-parsley-validate enctype="multipart/form-data" >
                            <input type="hidden" id="created_as" name="created_as" value="<?php echo $created_as;?>">
                            <input type="hidden" id="parent_id" name="parent_id"  value="<?php echo $media_id;?>">
                            <input type="hidden" id="comment_parent_id" name="comment_parent_id"  value="<?php echo $comment_parent_id;?>">
                            <input type="hidden" id="grade" name="grade" value="<?php echo $grade_n;?>">
                            <!-- <div class="form-group">
                                <label>Upload Your File </label>
                                <div class="files">
                                  <input type="file" class="form-control" name="file_upload" id="fileUpload_1" onchange="renderImage(1);">
                                  <span id="vid_err" class="error"></span>
                                </div>
                            </div> -->
                            

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Upload Your File </label>
                                        <div class="files">
                                            <input type="file" class="form-control" name="file_upload" id="fileUpload_1" onchange="renderImage(1);" data-parsley-required data-parsley-required-message="Select video" accept="video/mp4,video/x-m4v,video/*" data-parsley-errors-container="#vid_errmsg">
                                        </div>
                                        <span id="vid_errmsg" class="error text-center"></span>
                                        <span id="vid_err" class="error text-center"></span>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <label>Uploaded File </label>
                                     <div class="phl-video"> 
                                         <img src="assets/front/images/video_placeholder.jpg" class="img-fluid" alt="placeholder-image" id="img_src_1" >
                                     </div> 

                                     <video controls width="500px" autoplay="off" id="vid" style="display:none;"></video>  
                                   
                                </div>
                            </div>


                            <?php if($created_as==1){?>
                            <div class="form-group">
                                <label>Title</label>
                                <input class="form-control" id="title" name="title" type="text" placeholder="" maxlength="60" data-parsley-required data-parsley-required-message="Enter title"  />
                                <!-- <small class="text-right">(Up to  60 characters)</small> -->
                                <div id="input-count">
                                    <span id="current">0</span>
                                    <span id="maximum">/ 60</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" rows="5" id="description" name="description" type="text" placeholder="" maxlength="250" data-parsley-required data-parsley-required-message="Enter description" ></textarea>
                                <!-- <small class="text-right">(Up to 3000 characters)</small> -->
                                <div id="the-count">
                                    <span id="current-a">0</span>
                                    <span id="maximum-a">/ 250</span>
                                </div>
                            </div>
                            <?php } ?>

                            <div class="progress" style="display: none;margin-top: 30px;">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 0%;background-color: #BC718C;"></div>
                            </div></br>

                            <div class="form-group text-center">
                                <!-- <button id="submit_form" type="submit" class="btn btn-primary loader_btn" onclick="return form_submit('video_form');">Publish</button> -->
                                <button id="submit_form" type="button" class="btn btn-primary loader_btn" onclick="form_submit('video_form');">Publish</button>
                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>

$('textarea').keyup(function() {
    
    var characterCount = $(this).val().length,
        current = $('#current-a'),
        maximum = $('#maximum-a'),
        theCount = $('#the-count');
      
    current.text(characterCount);    
    /*This isn't entirely necessary, just playin around*/
    if (characterCount < 500) {
      current.css('color', '#666');
    }
    if (characterCount > 500 && characterCount < 1500) {
      current.css('color', '#6d5555');
    }
    if (characterCount > 1500 && characterCount < 2000) {
      current.css('color', '#793535');
    }
    if (characterCount > 2000 && characterCount < 2500) {
      current.css('color', '#841c1c');
    }
    if (characterCount > 2500 && characterCount < 2800) {
      current.css('color', '#8f0001');
    }
    
    if (characterCount >= 3000) {
      maximum.css('color', '#8f0001');
      current.css('color', '#8f0001');
      theCount.css('font-weight','bold');
    } else {
      maximum.css('color','#666');
      theCount.css('font-weight','normal');
    }
        
  });


  $('input').keyup(function() {
    
    var characterCount = $(this).val().length,
        current = $('#current'),
        maximum = $('#maximum'),
        theCount = $('#input-count');
      
    current.text(characterCount);    
    /*This isn't entirely necessary, just playin around*/
   
    
        
  });

</script>

<style type="text/css">
    .files input {
        outline: 2px dashed #bc718c;
        outline-offset: -10px;
        -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
        transition: outline-offset .15s ease-in-out, background-color .15s linear;
        padding: 120px 0px 85px 35%;
        text-align: center !important;
        margin: 0;
        width: 100% !important;
        opacity:0;
    }
    .files input:focus{     outline: 2px dashed #92b0b3;  outline-offset: -10px;
        -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
        transition: outline-offset .15s ease-in-out, background-color .15s linear; border:1px solid #92b0b3;
        }
    .files{ 
        position:relative;
        outline: 2px dashed #bc718c;
        outline-offset: -10px;
        -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
        transition: outline-offset .15s ease-in-out, background-color .15s linear;
        text-align: center !important;
    }
    .files:after {  pointer-events: none;
        position: absolute;
        top: 60px;
        left: 0;
        width: 50px;
        right: 0;
        height: 56px;
        content: "";
        background-image: url(assets/front/images/109612.png);
        display: block;
        margin: 0 auto;
        background-size: 100%;
        background-repeat: no-repeat;
    }
    .color input{ background-color:#f1f1f1;}
    .files:before {
        position: absolute;
        bottom: 30px;
        left: 0;  pointer-events: none;
        width: 100%;
        right: 0;
        height: 40px;
        content: "select or drag it here. ";
        display: block;
        margin: 0 auto;
        color: #e3548a;
        font-weight: 600;
        text-transform: capitalize;
        text-align: center;
    }
</style>
<script>
var filemaxzie=40;
var imageCount=0;
var objectUrl; 
function videoerrobox(seq,msg){
  $("#loader-wrapper").hide();
  $("#fileUpload_"+seq+"").val('');
  $("#fileUpload_"+seq+"").attr('disabled',false);
  $("#vid_err").text(msg);
  $("#img_src_"+seq+"").attr('src', 'assets/front/images/video_placeholder.jpg');
}
function renderImage(seq) {
    $("#vid_err").text('');
    $("#img_src_"+seq+"").css('display','block');
    $("#img_src_"+seq+"").attr('src', 'assets/front/images/video_placeholder.jpg');
    var file = event.target.files[0];
    if(!file){
      return;
    }

    $("#fileUpload_"+seq+"").attr('disabled',true);
    $("#loader-wrapper").show();
    var fileReader = new FileReader();
    if(!file.type.match('video')) {
        videoerrobox(seq,"File must be video.\n\n");
        return false;
    }
    var filesize=(file.size/1000000).toFixed(2);
    if(filesize>filemaxzie){
     
      videoerrobox(seq,"Media size must be less then "+filemaxzie+"MB.\n\n");
      return false;
    }
    objectUrl = URL.createObjectURL(file);  
    
    fileReader.onload = function() {
      var blob = new Blob([fileReader.result], {type: file.type});
      var myVid=document.getElementById("fileUpload_1");
       
      var FileExt = file.name.substr(file.name.lastIndexOf('.') + 1);
       if ((FileExt.toLowerCase() != "avi" && FileExt.toLowerCase() != "mov" && FileExt.toLowerCase() != "mp4")) {
        videoerrobox(seq,"Media must be avi or mov or mp4.\n\n");
        return false;
      }
      
      var url = URL.createObjectURL(blob);
      var video = document.createElement('video');
      if(FileExt.toLowerCase() == "mp4"){
        var timeupdate = function() {
          if (snapImage()) {
            video.removeEventListener('timeupdate', timeupdate);
            video.pause();
          }
        };
        video.addEventListener('loadeddata', function() {
          if (snapImage()) {
            video.removeEventListener('timeupdate', timeupdate);
          }
        });
        var snapImage = function() {
          var canvas = document.createElement('canvas');
          canvas.width = video.videoWidth;
          canvas.height = video.videoHeight;
          canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
          var image = canvas.toDataURL();
          // var success = image.length > 100000;
          var success = image.length > 60000;
          if (success) { 
            $("#img_src_"+seq+"").attr('src', '');
            $("#img_src_"+seq+"").attr('src', image);
            $("#loader-wrapper").hide();
            $("#fileUpload_"+seq+"").attr('disabled',false);
            URL.revokeObjectURL(url);
            imageCount++;
          }
          return success;
        };
        video.addEventListener('timeupdate', timeupdate);
        video.preload = 'metadata';
        video.src = url;
        video.muted = true;
        video.autostart=false;
        video.playsInline = true;
        video.play();
      }else{
        $("#fileUpload_"+seq+"").attr('disabled',false);
        $("#loader-wrapper").hide();
        $("#img_src_"+seq+"").attr('src', 'assets/front/images/nopreveew_video.png');
      }
    };
    fileReader.readAsArrayBuffer(file);
}

function form_submit(id)
{
  $("#vid_err").text('');
  $("#"+id).parsley().validate();
  var myVid=document.getElementById("fileUpload_1").files[0];
  if(myVid){
    var filesize=(myVid.size/1000000).toFixed(2);
    if(filesize>filemaxzie){
      return false;
    }
    submitDetailsFormVideo(id,'<?php echo $form_action;?>');
  }
}
function submitDetailsFormVideo(id,seturl='') {

    if(seturl)
    {
        var form_action=seturl;
    }else
    {
        var form_action= "<?php if(!empty($form_action))echo $form_action;?>";
    }
    var last_element = form_action.split("/").pop(-1);
    //alert(last_element);
    $("#"+id).parsley().validate();
    var form = $('#'+id)[0];
    if($("#"+id).parsley().isValid()){
      var formData = new FormData(form);
      formData.append('type','web');
      //$("#loader-wrapper").show();
      $('.loader_btn').html('<span class="fa fa-spinner fa-spin fa-lg mr-2" role="status" aria-hidden="true"></span>Uploading...').attr('disabled', true);
      $('.progress').show();
      $.ajax({
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
              if (evt.lengthComputable) {
                  var percentComplete = Math.round(((evt.loaded / evt.total) * 100));
                  if(percentComplete<=95){
                    $(".progress-bar").width(percentComplete + '%');
                    $(".progress-bar").html(percentComplete+'%');
                  }else{
                    $(".progress-bar").width('95%');
                    $(".progress-bar").html('95%');
                    $('.loader_btn').html('<span class="fa fa-spinner fa-spin fa-lg mr-2" role="status" aria-hidden="true"></span>Processing...');
                    // setTimeout(function() { 
                    //   $(".progress-bar").width('97%');
                    //   $(".progress-bar").html('97%');
                    // },2000);
                  }
              }
          }, false);
          return xhr;
        },
        url: form_action,
        type: 'POST',
        data: formData,
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        headers: { 'apikey': '<?php echo APP_KEY ?>' },
        beforeSend: function(){
          $(".progress-bar").width('0%');
        },
        success:function(resp){
          $(".progress-bar").width('100%');
          $(".progress-bar").html('100%');
          $('.loader_btn').html('Publish').attr('disabled', false);
          $(".content-body").animate({ scrollTop: 0 }, "fast");
          $("#msg").show(); 
          $("#submit_form").attr('disabled',false);
          if(resp.status==4){
            location.reload();
          }
          if(resp.status==0){
            $('.progress').hide();
            $(".progress-bar").width('0%');
            $(".progress-bar").html('0%');
            $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
            setTimeout(function() {
             $("#msg").html('');
              $("#msg").hide();
            }, 5000); 
          }else{

            $("#msg").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
            setTimeout(function() { 
                $("#msg").html('');
                $("#msg").hide();
                <?php if(isset($media_slug) && !empty($media_slug)){?>
                  window.location.href="video-fulldetail/<?php echo $media_slug;?>";
                <?php }else{ ?>
                  window.location.href="grade-wise-video/"+<?php echo $grade_n;?>;
                <?php } ?>
            }, 2000); 
          }
        },
        error:function(err){
          $('.progress').hide();
          $(".progress-bar").width('0%');
          $(".progress-bar").html('0%');
          $('.loader_btn').html('Publish').attr('disabled', false);
        }
      });
    }
}



</script>
