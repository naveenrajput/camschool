
<section class="content">
    <div class="container">
        <div class="row">
            <?php include APPPATH.'views/front/include/sidebar.php'; ?>
            <div class="col-md-12">
                <div class="main-body">
                    <div class="content-header">
                        <h2 class="company-name">Upload Video</h2>
                    </div>
                    <div class="content-body">
                        <p class="alert_message" id="msg" ></p>
                        <form id="video_form" action="" role="form" data-parsley-validate enctype="multipart/form-data" >
                            
                            <input type="hidden" id="grade" name="grade" value="<?php echo $media['grade'];?>">
                            <input type="hidden" id="media_slug" name="media_slug" value="<?php echo $media['media_slug']?$media['media_slug']:'';?>">
                            <div class="form-group">
                                <!-- <label>Upload Your File </label>
                                <input type="file" class="form-control" name="file_upload" id="fileUpload_1" onchange="renderImage(1);"> -->
                                <img src="<?php echo $media['thumb_image']?$media['thumb_image']:"assets/front/images/person.jpg";?>">
                                <!-- <span id="vid_err" class="error"></span> -->
                            </div>
                            <div class="form-group">
                                <label>Title</label>
                                <input class="form-control" id="title" name="title" type="text" placeholder="" maxlength="60" data-parsley-required data-parsley-required-message="Enter your video's title"  value="<?php if(!empty($media['title'])){echo $media['title'];}?>"/>
                                 <div id="the-count">
                                    <span id="current1">0</span>
                                    <span id="maximum1">/ 150</span>

                                </div>
                            </div>
                           
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" rows="5" id="description" name="description" type="text" placeholder="" maxlength="250" data-parsley-required data-parsley-required-message="Enter your video's description " ><?php if(!empty($media['description'])){echo $media['description'];}?></textarea>
                                 <div id="the-count">

                                    <span id="current">0</span>
                                    <span id="maximum">/ 250</span>

                                </div>
                            </div>

                            <div class="form-group text-center">
                                <button id="submit_form" type="button" class="btn btn-primary loader_btn" onclick="return form_submit('video_form');">Update</button>
                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<style type="text/css">
    .files input {
        outline: 2px dashed #bc718c;
        outline-offset: -10px;
        -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
        transition: outline-offset .15s ease-in-out, background-color .15s linear;
        padding: 120px 0px 85px 35%;
        text-align: center !important;
        margin: 0;
        width: 100% !important;
    }
    .files input:focus{     outline: 2px dashed #92b0b3;  outline-offset: -10px;
        -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
        transition: outline-offset .15s ease-in-out, background-color .15s linear; border:1px solid #92b0b3;
        }
    .files{ position:relative}
    .files:after {  pointer-events: none;
        position: absolute;
        top: 60px;
        left: 0;
        width: 50px;
        right: 0;
        height: 56px;
        content: "";
        background-image: url(assets/front/images/109612.png);
        display: block;
        margin: 0 auto;
        background-size: 100%;
        background-repeat: no-repeat;
    }
    .color input{ background-color:#f1f1f1;}
    .files:before {
        position: absolute;
        bottom: 10px;
        left: 0;  pointer-events: none;
        width: 100%;
        right: 0;
        height: 40px;
        content: " or drag it here. ";
        display: block;
        margin: 0 auto;
        color: #e3548a;
        font-weight: 600;
        text-transform: capitalize;
        text-align: center;
    }
</style>
<script>

    function form_submit(id)
    {
        submitTeachereditDetailsForm(id,'<?php echo $form_action;?>');
    }
function submitTeachereditDetailsForm(id,seturl='') {
    if(seturl)
    {
        var form_action=seturl;
    }else
    {
        var form_action= "<?php if(!empty($form_action))echo $form_action;?>";
    }
    var last_element = form_action.split("/").pop(-1);
    // alert(last_element);
    $("#"+id).parsley().validate();
    var form = $('#'+id)[0];
    if($("#"+id).parsley().isValid()){
      var formData = new FormData(form);
      formData.append('type','web');
      //$("#loader-wrapper").show();
      $('.loader_btn').html('<span class="   fa fa-spinner fa-spin fa-lg mr-2" role="status" aria-hidden="true"></span>Loading...').attr('disabled', true);
      var btn_text = $("#submit_form").attr('attr-id');
      $.ajax({
        url: form_action,
        type: 'POST',
        data: formData,
        dataType: 'json',
        // async: false,
        cache: false,
        contentType: false,
        processData: false,
        headers: { 'apikey': '<?php echo APP_KEY ?>' },
        success:function(resp){
         
          $(".content-body").animate({ scrollTop: 0 }, "fast");
          $("#msg").show(); 
          $('.loader_btn').html(btn_text).attr('disabled', false);
          if(resp.status==4){
            location.reload();
          }
        if(resp.status==0){
            $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
            setTimeout(function() {
             $("#msg").html('');
              $("#msg").hide();
            }, 5000); 
          }else{
           
            $("#msg").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
            $("#"+id).parsley().reset();
            if(last_element!='profile-update' && last_element!='prinicpal-profile-update' && last_element!='teacher-update-video-content'){
                $("#"+id)[0].reset();
            }
            setTimeout(function() { 
                $("#msg").html('');
                $("#msg").hide();
                window.location.href="<?php echo $backurl;?>";
            }, 1000); 
        }
    },
    error:function(err){
      $('.loader_btn').html(btn_text).attr('disabled', false);
      $("#loader-wrapper").hide();
    }
  });
}
} 

    window.onload = (event) => {
    var characterCount = $('#description').val().length; 
    var characterCount1 = $('#title').val().length;
        current = $('#current');
        maximum = $('#maximum');
        theCount = $('#the-count'); 
        current1 = $('#current1');
        maximum1 = $('#maximum1');
        theCount1 = $('#the-count1');
     current.text(characterCount);
      current1.text(characterCount1);
   
};
    $('#title').keyup(function() {
   var characterCount1 = $(this).val().length;
        current1 = $('#current1');
        maximum1 = $('#maximum1');
        theCount1 = $('#the-count1');
      current1.text(characterCount1);
});
$('textarea').keyup(function() {
    
    var characterCount = $(this).val().length,
        current = $('#current'),
        maximum = $('#maximum'),
        theCount = $('#the-count');
      
    current.text(characterCount);    
    /*This isn't entirely necessary, just playin around*/
    if (characterCount < 500) {
      current.css('color', '#666');
    }
    if (characterCount > 500 && characterCount < 1500) {
      current.css('color', '#6d5555');
    }
    if (characterCount > 1500 && characterCount < 2000) {
      current.css('color', '#793535');
    }
    if (characterCount > 2000 && characterCount < 2500) {
      current.css('color', '#841c1c');
    }
    if (characterCount > 2500 && characterCount < 2800) {
      current.css('color', '#8f0001');
    }
    
    if (characterCount >= 3000) {
      maximum.css('color', '#8f0001');
      current.css('color', '#8f0001');
      theCount.css('font-weight','bold');
    } else {
      maximum.css('color','#666');
      theCount.css('font-weight','normal');
    }
        
  });


  

</script>
</script>
