<section class="content">
        <div class="container">
            <div class="row">
            <?php include APPPATH.'views/front/include/sidebar.php'; ?>
                <div class="col-md-12">
                    <div class="main-body">
                    <p class="alert_message" id="msg" style="display:none;"></p>
                        <div class="content-header">
                            <h2 class="company-name">Requests</h2>
                        </div>
                        <div class="content-body" id="ajax_data">
                            <?php $this->load->view('front/teacher/request_list_data.php');?>
                            

                        </div> 
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
    var checked_customer = [];
    var checked_page = [];
    function search_chk_bx(page){
        if (Array.isArray(checked_customer) && checked_customer.length) {
            var i;
            for(i=0;i<checked_customer.length;i++){//alert(i);
                $("#chk_"+checked_customer[i]).prop('checked',true);
            }
        }
        if (Array.isArray(checked_page) && checked_page.length) {
            var page_check=checked_page.includes(page.toString());
            if(page_check){
                $("#select_all_"+page).prop('checked',true);
            }
        }
    }


    function getstudentval(ee){
        if($(ee).prop("checked") == true){
            $('input[class="isChecked"]').each(function(){                
                $(this).prop("checked", "checked");
            })            
        }else{    
            $('input[class="isChecked"]').each(function(){                
                $(this).prop("checked", false);
            }) 
        }
    }

    function updateStudent(){
        var student_data = [];
        var school_id = `<?php echo $this->session->userdata('school_id'); ?>`;
        var user_id = `<?php echo $this->session->userdata('user_id'); ?>`;
      
        $('input[class="isChecked"]').each(function(){
            var studentIDs = $(this).val();
           
            if($(this).prop("checked") == true){
                student_data.push(studentIDs);
            }
        })
        let studentID = checked_customer.toString();
        if(studentID && studentID.length > 0 && user_id && school_id){
            
            $('.loader_btn').html('<span class="   fa fa-spinner fa-spin fa-lg mr-2" role="status" aria-hidden="true"></span>Loading...').attr('disabled', true);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>api/teacher-approved-student",
                data: {type:'web',request_id: studentID, school_id ,user_id},
                headers: { 'apikey': '<?php echo APP_KEY ?>' },
                success: function(response) {
                    response = JSON.parse(response);
                   
                    $('.loader_btn').html('Request Approved').attr('disabled', false);
                    if(response.status==4){
                        location.reload();
                    } 
                    if(response.status == 1 && response.msg){
                        $("#msg").html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+response.msg+'</div>');
                        $('#msg').css('display','block');
                        setTimeout(function() {
                        $('#msg').fadeOut('slow');
                        }, 3000);

                        setTimeout(function() {
                            location.reload();
                        }, 3000);
                        // location.reload();                        
                    }
                },error: function(){
                    
                    $('.loader_btn').html('Request Approved').attr('disabled', false);
                    $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close"  type="button">×</button>Some error occured in loading the data.</div>');
                    $('#msg').css('display','block');
                    setTimeout(function() {
                         $('#msg').fadeOut('slow');
                    }, 3000);
                }
            });


        }else{
            
           
        }

    }


    function filter_records(page,msg)
    { 
        $('.uk-alert').addClass("display-none");
        $('#notification_msg').html("");
        $("#loader").show();
        if(page==null) {
            page=0;
        }

        var base_url= "<?php echo base_url(); ?>";
       
        $.ajax({
            type:'GET',
            url: base_url+"student-list-request-data/"+page,
            success:function(data)
            {   
                $("#ajax_data").html(data);
                $("#loader").hide();
                 search_chk_bx(page);
               
            }
        });
    }


</script>