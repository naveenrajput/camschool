<?php
/*echo '<pre>';
print_r($teacher_video_list); die;*/
$user_type=$this->session->userdata('front_user_type');
?>
<section class="content">
    <div class="container">
        <div class="row">
        <?php include APPPATH.'views/front/include/sidebar.php'; ?>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="main-body">
                    <p class="alert_message" id="msg" style="display:none;"></p>
                    <?php if(isset($not_approved) && !empty($not_approved)){ ?>
                        <!-- <div class="alert alert-danger"><?php //echo $msg?$msg:"";?></div>   -->
                        <div class="content-header d-flex flex-wrap justify-content-between">
                            <h5 class="company-name">
                                <?php if($user_type == 3){ ?>
                                    <?php echo $grade_name; ?>
                                <?php } else { ?>
                                    <?php 
                                    if(isset($teacher_detail) && $teacher_detail[0] && $teacher_detail[0]['display_name']){ 
                                        echo $teacher_detail[0]['display_name'];
                                    }else{
                                        echo $teacher_detail[0]['first_name'].' '.$teacher_detail[0]['last_name']; 
                                    }?>
                                <?php } ?>

                            </h5>
                            <?php if($user_type == 4){ ?>
                                <a href="javascript:void(0)" class="btn btn-primary" onClick="initiateChat(<?= $teacher_detail[0]['user_id'];?>)">
                                    Chat
                                </a>
                            <?php } ?>
                        </div>
                            <div class="content-body video-detail">

                             <span class="f-12">
                                <h6 class="no-video">Your teachers haven't uploaded a video yet. you will automatically receive a notification every time a video is uploaded.</h6>
                             </span> 
                            </div>                     
                    <?php }else{ ?>
                        <div class="content-header d-flex flex-wrap justify-content-between">
                            <h5 class="company-name">
                                <?php if($user_type == 3){ ?>
                                    <?php echo $grade_name; ?>
                                <?php } else { ?>
                                    <?php 
                                    if(isset($teacher_detail) && $teacher_detail[0] && $teacher_detail[0]['display_name']){ 
                                        echo $teacher_detail[0]['display_name'];
                                    }else{
                                        echo $teacher_detail[0]['first_name'].' '.$teacher_detail[0]['last_name']; 
                                    }?>
                                <?php } ?>

                            </h5>
                            <?php if($user_type == 4){ ?>
                                <a href="javascript:void(0)" class="btn btn-primary" onClick="initiateChat(<?= $teacher_detail[0]['user_id'];?>)">
                                    Chat
                                </a>
                            <?php } ?>
                            <?php if($user_type != 4){?>
                            <div>
                                <a href="teacher-upload-video/<?php echo $grade;?>" class="btn btn-primary">Upload Video</a>
                            </div>
                            
                            <?php } ?>
                        </div>
                        <div class="content-body video-detail">
                            <?php /* ?>
                            <div class="profile-block">
                                     <?php if(isset($teacher_video_list)){ ?>
                                <div class="row">
                                    <div class="video-box col-sm-12">
                                        <div class="video-block">
                                            <?php if($user_type == 3){?>
                                                <div class="eidt-video">
                                                    <a href="teacher-edit-video/<?php echo $teacher_video_list[0]['media_slug']?$teacher_video_list[0]['media_slug']:'';?>"><i class="fa fa-pencil"></i></a>
                                                </div>
                                                <div class="delete-video">
                                                    <a href="javascript:void(0)" onclick="video_delete('<?php echo $teacher_video_list[0]['media_id']?$teacher_video_list[0]['media_id']:'';?>')"><i class="fa fa-trash"></i></a>
                                                </div>
                                            <?php  } ?>
                                        </div>

                                                                          

                                        <a href="video-fulldetail/<?php echo $teacher_video_list[0]['media_slug']; ?>">
                                            <img class="img-video" src="<?php if(isset($teacher_video_list) && $teacher_video_list[0] && $teacher_video_list[0]['large_image']){echo $teacher_video_list[0]['large_image']; }else{ echo "assets/front/images/video_thumb.jpg";}?>" />
                                            <span><?php if(isset($teacher_video_list) && $teacher_video_list[0] && $teacher_video_list[0]['duration']){echo $teacher_video_list[0]['duration']; }?>
                                                <i class="fa fa-play"></i>
                                            </span>
                                        </a>
                                    </div>
                                    <?php if(isset($teacher_video_list)){ ?>
                                    <div class="video-content col-sm-12">
                                       
                                        <a href="video-fulldetail/<?php echo $teacher_video_list[0]['media_slug']; ?>">
                                            <h3><?php if(isset($teacher_video_list) && $teacher_video_list[0] && $teacher_video_list[0]['title']){echo  mb_strimwidth($teacher_video_list[0]['title'], 0, 50, "..."); } ?></h3>
                                            <p><?php if(isset($teacher_video_list) && $teacher_video_list[0] && $teacher_video_list[0]['description']){echo mb_strimwidth($teacher_video_list[0]['description'], 0, 100, "..."); } ?></p>
                                        </a>

                                    </div>
                                    </div>
                                     <?php } 

                                 } ?>

                                

                            </div>
                            <?php */ ?>
                            <!-- //profile block -->
                            <div class="row">
                                <?php
                                    if(isset($teacher_video_list) && count($teacher_video_list)> 0){
                                        // array_splice($teacher_video_list, 0, 1);

                                    foreach($teacher_video_list as $list){ ?>
                                        <div class="timeline col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                            <div class="playlist-section">
                                                <div class="playlist-header">
                                                    <h3><?= ucfirst($list['title'])?mb_strimwidth(ucfirst($list['title']), 0, 21, "..."):"";?></h3>
                                                </div>  

                                            </div>
                                            <div class="row row-ff">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="card timeline ocean height-fixed">
                                                        <div class="video-block">
                                                        <?php if($user_type == 3){?>
                                                            <div class="eidt-video">
                                                                <a href="teacher-edit-video/<?php echo $list['media_slug']?$list['media_slug']:'';?>"><i class="fa fa-pencil"></i></a>
                                                            </div>
                                                            <div class="delete-video">
                                                                <a href="javascript:void(0)" onclick="video_delete('<?php echo $list['media_id']?$list['media_id']:'';?>')"><i class="fa fa-trash"></i></a>
                                                            </div>
                                                        <?php  } ?>
                                                            <a href="video-fulldetail/<?php echo $list['media_slug']; ?>">
                                                                <img class="card-img-top" src="<?php echo $list['thumb_image']?$list['thumb_image']:"assets/front/images/video_thumb.jpg"; ?>">
                                                                <span><?php echo $list['duration']; ?>
                                                                    <i class="fa fa-play"></i>
                                                                </span>
                                                            </a>
                                                        </div>
                                                        <a href="video-fulldetail/<?php echo $list['media_slug']; ?>">
                                                            <div class="card-block">
                                                                <?php /* <div class="card-text">
                                                                    <a href="video-fulldetail/<?php echo $list['media_slug']; ?>">
                                                                        <h3><?php echo $list['description']?mb_strimwidth($list['description'], 0, 20, "..."):""; ?></h3>
                                                                    </a>
                                                                </div> */ 
                                                                 ?>
                                                                <figure class="profile profile-inline">
                                                                    <img src="<?php echo $list['profile_image']?$list['profile_image']:"assets/front/images/video_thumb.jpg"; ?>" class="profile-avatar" alt="">
                                                                </figure>
                                                                <h3 class="card-title">
                                                                  <span class="f-12">  <?php //echo "<pre>";
                                                                    //print_r($recent_video); ?>
                                                                    
                                                                    <?php if($user_type == 3){?>
                                                                        <span class="f-12">  <?= $list['grade_display_name']; ?> </span>
                                                                    <?php }else{ ?>
                                                                    <?php
                                                                    // echo (isset($list['display_name']) && $list['display_name'])?$list['display_name']:$list['first_name'].'-'.$list['grade_display_name']; 

                                                                    if(isset($list['display_name']) && $list['display_name']){
                                                                        echo '<span class="f-12">';
                                                                        echo strlen($list['display_name'])>22?substr(ucfirst($list['display_name']),0,25).'...':ucfirst($list['display_name']);
                                                                        echo '</span>';
                                                                        echo '<span class="f-12">';
                                                                        echo $list['grade_display_name'];
                                                                        echo '</span>';
                                                                    }else{
                                                                        echo '<span class="f-12">';
                                                                        echo strlen($list['first_name'].' '.$list['last_name'])>20?substr(ucfirst($list['first_name'].' '.$list['last_name']),0,25).'...':ucfirst($list['first_name'].'-'.$list['last_name']);
                                                                        echo '</span>';
                                                                        echo '<span class="f-12">';
                                                                        echo $list['grade_display_name'];
                                                                        echo '</span>';
                                                                    }
                                                                    ?> 
                                                                    <?php } ?>
                                                                    <span class="f-11">
                                                                    <?= $list['created']; ?>
                                                                    </span>
                                                                </h3>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                        </div>



                                <?php  } } ?>
                                <!-- //playlist section -->
                                
                                <!-- //playlist section -->
                            </div>
                        </div>
                    <?php } ?>
                    <!-- //playlist section -->
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
</section>

<script>

function video_delete(media_id) {
    swal({
        title: "Confirmation",
        text: "Are you sure you want to delete this video?",
        //icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: {
            dance : {
              text : "No",
              className : 'btn btn-default add-row'
            },
            confirm: {
              text : 'Yes',
              className : 'btn btn-primary add-row'
            }
          }
    })
    .then((willDelete) => {
         if(willDelete==true){
        //if (willDelete) {
            $.ajax({
                type: 'POST',
                url: "<?php echo base_url('api/teacher-delete-video'); ?>",
                data: {media_id:media_id,type:'web'},
                dataType: 'json',
                headers: { 'apikey': '<?php echo APP_KEY ?>' },
                success:function(resp){
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    
                    if(resp.status==4){
                        location.reload();
                    } 
                    if(resp.status==0){
                        $("#msg").show();
                        $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                            setTimeout(function() {
                             $("#msg").html('');
                             $("#msg").hide();
                            }, 1000); 
                    }else{
                        swal({
                            text: resp.msg,
                            icon: "success",
                            buttons: false,
                        });
                        setTimeout(function() {
                            location.reload();
                        }, 2000); 
                    }
                    
                }
            });
        }
    });
}
</script>