<?php //echo "<pre>";print_r($details);exit;
 
foreach ($details['teacher_grade'] as $key => $value1) {
        $u_grade[] = $value1['grade'];
    }

?>
<form id="profile_form" action="" class="profile_form" action="" role="form" data-parsley-validate  role="form" data-parsley-validate enctype="multipart/form-data">
     <div class="content-body">
        <input type="hidden" name="school_id" value="<?php echo $details['school_id'] ?>">
        <div class="row">
        <div class="col-md-12 col-sm-12 form-group">
            <label>Upload a profile picture or use the CAM! avatar.</label><br>
            <div class="upload-btn-wrapper">
                <input type="file" name="profile_pic"  id="fileUpload_1" onchange="renderImage(1)" accept="image/x-png,image/gif,image/jpeg">
                <!-- <span class="upload"><i class="fa fa-camera"></i></span> -->
                <img src="<?php echo $details['profile_image']?$details['profile_image']:'assets/front/images/person.jpg';?>" id="img_src_1" class="img-fluid" height="100">
            </div>
                <div id="img_err" style="color:red;"></div>
        </div>
            <div class="col-md-6 col-sm-6 form-group">
                <label>First Name</label>

                <input class="form-control" name="first_name"  type="text" value="<?php echo $details['first_name'] ?>" maxlength="30" data-parsley-required data-parsley-required-message="Enter your first name" autocomplete="off">
            </div>
            <div class="col-md-6 col-sm-6 form-group">
                <label>Last Name</label>
                <input class="form-control" name="last_name" value="<?php echo $details['last_name'] ?>" type="text" maxlength="30" data-parsley-required data-parsley-required-message="Enter your last name" autocomplete="off">
            </div>
            <div class="col-md-6 col-sm-6 form-group">
                <label>Display Name</label>
                <input class="form-control" name="display_name"  data-parsley-required data-parsley-required-message="Enter your display name" value="<?php echo $details['display_name'] ?>" type="text" maxlength="65">
            </div>
            <div class="col-md-6 col-sm-6 form-group">
                <label>Email</label>
                 <input type=hidden name="image" id="imageinput"value="">
                <input class="form-control" type="text" name="email" value="<?php echo $details['email'] ?>" disabled>
            </div>
            <div class="col-md-6 col-sm-6 form-group">
                <label>School Name</label>
                <input class="form-control" name="school_name" value="<?php echo $details['school_name'] ?>" type="text"  disabled>
            </div>
             <div class="col-md-6 col-sm-6 form-group">
                <label>Select Grade</label>
                    <select class="form-control select2" id="grade" name= "grade[]" data-parsley-required data-parsley-errors-container="#errorGrade" data-parsley-required-message="Select your grade(s)" multiple="multiple">
                        <option  value="" >Select Grade</option>
                        <?php 
                       
                        if(!empty($school_grade)){
                            foreach($school_grade as $school_grade){ 
                              if (in_array($school_grade['grade'],$u_grade)) {
                                        $selected = 'selected';
                                    } else {
                                        $selected = '';
                                    }  
                            ?>
                             <option <?php echo $selected; ?> value="<?php echo $school_grade['grade'];?>"><?php echo $school_grade['grade_display_name'];?></option>
                        <?php } } ?>
                                            

                    </select>
                    <span id="errorGrade"></span>
                </div>
            <div class="col-sm-12 text-center mt-3">
                <button type="button" id="submit_form" attr-id="Update"  class="btn btn-primary loader_btn" onclick="return form_submit('profile_form');">Update</button>
            </div>
        </div>
    </div>
</form>
<div id="uploadimageModal" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Crop Image</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div id="image_demo" style=" margin-top:10px"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary crop_image">Crop & Upload Image</button>
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </div>
    </div>
  </div>
</div>

<script>  
$(document).ready(function(){

 $image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:200,
      height:200,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  });

  $('#fileUpload_1').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });

  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
       
       $("#img_src_1").attr("src", response); 
       $("#imageinput").val(response);
       
       $('#uploadimageModal').modal('hide')
    })
  });

});  
</script>

<script>
    $(document).ready(function() {
            $('.select2').select2({
                 placeholder: 'Select Grade',
                 multiple:true,
            });
        });
  var imgerror=0;
    function renderImage(seq) {
        $("#img_err").html('');
        //$("#loader-wrapper").show();
        var file = event.target.files[0];
        var fileReader = new FileReader();
        if(!file.type.match('image')) {
            $("#loader-wrapper").hide();
            $("#fileUpload_"+seq+"").val('');
            imgerror=1;
            $("#img_err").html("Image must be a jpg, jpeg or png.");
            return false;
        }else{
            imgerror=0;
        }
        if (file.type.match('image')) {
            var FileExt = file.name.substr(file.name.lastIndexOf('.') + 1);
            if ((FileExt.toUpperCase() != "JPG" && FileExt.toUpperCase() != "JPEG" && FileExt.toUpperCase() != "PNG")) {
                    $("#loader-wrapper").hide();
                    $("#fileUpload_"+seq+"").val('');
                    imgerror=1;
                    $("#img_err").html("File must be jpg or jpeg or png");
                    return false;
            }else{
                imgerror=0;
            }
            fileReader.onload = function() {
              $("#img_src_"+seq+"").attr('src', fileReader.result);
              $("#loader-wrapper").hide();
            };
            fileReader.readAsDataURL(file);
        } 
    }


function form_submit(id)
{   if(imgerror==0){
     submitDetailsForm(id,'<?php echo $form_action;?>');
}   }

</script>