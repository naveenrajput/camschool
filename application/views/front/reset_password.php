<style type="text/css">
  #reset-password {
    position: absolute;
    top: 46px;
    right: 30px;

}
#reset-repet {
    position: absolute;
    top: 100px;
    right: 30px;
   
}

</style>
 
<section class="content signup-page login-page">
    <div class="container">
        
        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 right-form">
            <div class="row">
                <div class="spacer20"></div>
                <form class="forgot-form" method="POST" id="forgot_form" action="<?php echo base_url(); ?>reset-password?token=<?php echo $this->input->get('token'); ?>" role="form" data-parsley-validate>
                    <div class="col-md-12 col-sm-12">
                        <p class="alert_message" id="msg" style="display:none;"></p>
                        <div class="message_box" style="display:none;"></div>
                        <?php if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger">
                              <button data-dismiss="alert" class="close" type="button">×</button>
                              <?php echo $this->session->flashdata('error') ?>
                          </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success">
                              <button data-dismiss="alert" class="close" type="button">×</button>
                              <?php echo $this->session->flashdata('success') ?>
                          </div>
                        <?php } ?>
                        <h4 class="text-center"><?php echo $page_title?$page_title:'Reset Password';?></h4>
                       
                        <div class="spacer10"></div>
                         <div class="form-group">
                           <input class="form-control" type="password" id="new_password" name="new_password" placeholder="New Password"  data-parsley-minlength="6" maxlength="30"  data-parsley-required data-parsley-required-message="Enter a new password" autocomplete="off" data-parsley-number="1" data-parsley-alphabet="1"  data-parsley-special="1"  data-parsley-uppercase="1" data-parsley-lowercase="1" data-parsley-minlength-message="Password must be at least 6 characters long">
                             <i id="reset-password" class="fa fa-eye-slash pass-eye" aria-hidden="true" onClick="viewPassword()"></i>
                            <?php echo form_error('new_password'); ?> 
                        </div>
                        <div class="form-group">
                           <input type="password" class="form-control" id="confirm_password" name="confirm_password"  placeholder="Confirm Password" data-parsley-equalto="#new_password" required data-parsley-required data-parsley-required-message="Confirm your new password" data-parsley-equalto-message="New password and the confirm password doesn't match" autocomplete="off">
                           <i id="reset-repet" class="fa fa-eye-slash pass-eye" aria-hidden="true" onClick="viewPasswordRepet()"></i>
                            <?php echo form_error('confirm_password'); ?> 
                        </div>
                    </div>
                    <div class="col-sm-12 text-center">
                        <button type="submit" id="submit_form" class="btn btn-primary">Submit</button>
                    </div>
                </form>
                <!-- <h6 class="text-center col-sm-12">Already have an account. <a href="login.php" class="link-btn">Login</a></h6> -->
            </div>
        </div>
    </div>
</section>


<script>
function viewPassword()
{
  var passwordInput = document.getElementById('new_password');
  var passStatus = document.getElementById('reset-password');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye-slash';
  }
}

function viewPasswordRepet()
{
  var passwordInput = document.getElementById('confirm_password');
  var passStatus = document.getElementById('reset-repet');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye-slash';
  }
}
</script>