    <?php $user_id=$this->session->userdata('user_id');
    $user_type=$this->session->userdata('front_user_type');
    if(isset($user_type) && $user_type == 1){ ?>
        <div class="sidebar_new">
            <div class="company-logo">
                <a href="javascript:void(0);" style="display: block;cursor: default;">
                    <?php if($user_type=$this->session->userdata('front_user_type')){ 
                            if($user_type==1){ ?>
                                <img class="img-fluid" src="assets/front/images/person.jpg" />
                            <?php }
                    } ?>
                </a>
                <a href="district-request" class="btn notfication-btn">
                    <i class="fa fa-bell"></i> Requests
                    <?php $count= userRequestCount($this->session->userdata('front_user_type'));
                     if($count){ ?>
                        <span class="count"><?php echo $count;?></span>
                    <?php } ?>
                </a>
            </div>
            <ul class="main-menu">
                <li><a href="principal-school-approved-list">Schools/Principals </a></li>
                <li><a href="broadcast-messages">Broadcast Messages </a></li>
                <li><a href="school-videos">School Videos</a></li>
                <!-- <li><a href="district/upload-csv">Upload School File</a></li> -->
                
            </ul>
        </div>
    <?php }else if(isset($user_type) && $user_type == 2){ ?>
        <div class="sidebar_new">
            <div class="company-logo" >
                <a href="javascript:void(0);" class="" style="cursor: default;">
                    <img class=" img-fluid" src="<?php echo getSchoolLogo();?>" />
                </a><br>
                <a href="principal-request" class="btn notfication-btn">
                    <i class="fa fa-bell"></i> Requests
                    <?php $count= userRequestCount($this->session->userdata('front_user_type'));
                        if($count){ ?>
                        <span class="count"><?php echo $count;?></span>
                    <?php } ?>
                </a>
            </div>

            <ul class="main-menu">
                <li><a href="teacher-approved-list" class="active">Teachers</a></li>
                <li><a href="school-upload-video">Principal's Message</a></li>
                <li><a href="teacher-broadcast-messages">Broadcast Messages </a></li>
                <li><a href="teacher-video">Teachers Videos</a></li>
            </ul>
        </div>
    <?php }else if(isset($user_type) && $user_type == 3){ ?>
         <div class="sidebar_new">
            <div class="company-logo">
                <a href="teacher-index">
                    <img class="img-fluid" src="<?php echo getSchoolLogo();?>" />
                </a>
                <a href="notifications" class="btn notfication-btn">
                    <i class="fa fa-bell"></i> Notifications
                    <?php $count= userRequestCount(8);
                        if($count){ ?>
                        <span class="count"><?php echo $count;?></span>
                    <?php } ?>
                </a>
            </div>
            <ul class="main-menu">
                <li><a href="student-request">Requests <?php $count= userRequestCount($user_type);
                        if($count){ ?>
                        <span class="count-req"><?php echo $count;?></span>
                    <?php } ?></a>
                </li>
                
                <li><a href="user-chat">Chat
                    <?php if($this->uri->segment(1)!='user-chat'){ ?>
                    <?php $unread_chat_count=$this->Common_model->getChatunreadCount($user_id);
                        if($unread_chat_count){ ?>
                        <span class="count-req"><?php echo $unread_chat_count;?></span>
                    <?php }} ?>
                </a></li>
                
                <?php $teacher_grade =$this->Common_model->getRecords('teacher_grade', 'grade,grade_display_name', array('teacher_id'=>$user_id), 'grade asc', false);
                // echo "<pre>";print_r($teacher_grade);die;
                    if(isset($teacher_grade) && !empty($teacher_grade)){
                        foreach($teacher_grade as $list){ ?>
                            <li><a href="grade-wise-video/<?php echo $list['grade']; ?>"><?php echo $list['grade_display_name'];?></a></li>
                    <?php }
                   }
                ?>
                <?php if($teacher_grade =$this->Common_model->getRecords('teacher_grade', 'grade', array('teacher_id'=>$user_id,'school_id'=>$this->session->userdata('school_id'),'grade<=' =>7), '', true)){ ?>
                    <li><a href="story-time">Story Time</a></li>
                <?php } ?>

            </ul>
        </div>

    <?php }else if(isset($user_type) && $user_type == 4){ ?>
         <div class="sidebar_new">
            <div class="company-logo">
                <a href="<?php echo base_url(); ?>parent/index">
                    <img class="img-fluid" src="<?php echo getSchoolLogo();?>" />
                </a>
                <a href="notifications" class="btn notfication-btn"><i class="fa fa-bell"></i> Notifications  <?php $count= userRequestCount(8);
                        if($count){ ?>
                        <span class="count"><?php echo $count;?></span>
                    <?php } ?></a>
            </div>
            <?php $menu = menu_teacher_by_grade_web();?>
            <ul class="main-menu">
                <li><a href="user-chat">Chat
                <?php if($this->uri->segment(1)!='user-chat'){ ?>
                    <?php $unread_chat_count=$this->Common_model->getChatunreadCount($user_id);
                        if($unread_chat_count){ ?>
                        <span class="count-req"><?php echo $unread_chat_count;?></span>
                    <?php }} ?>
                </a></li>
                <?php if(!empty($menu)) {
                    foreach ($menu['grade'] as $key => $grade) { ?>
                        <li>
                            <a role="button" data-toggle="collapse" href="#collapseExample<?php echo $key?>" aria-expanded="false"
                                aria-controls="collapseExample"><?php echo $grade['grade_name'] ?>
                                <i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="sub-menu collapse" id="collapseExample<?php echo $key?>">
                            <?php foreach ($grade['teacher'] as $key => $value) { ?>
                                <li>
                                <a href="<?php echo base_url()."teacher-wise-video/".$value['teacher_id']."/".$grade['grade_id']; ?>">
                                    <?php //echo $value['display_name']?$value['display_name']:$value['first_name']." ".$value['last_name']; ?>
                                    <?php 
                                    if($value['display_name']){
                                        echo wordwrap(ucfirst($value['display_name']),15,"<br>\n",TRUE);
                                    }else{
                                        echo wordwrap(ucwords($value['first_name']." ".$value['last_name']),15,"<br>\n",TRUE);
                                    }
                                    ?>
                                    
                                </a>
                                </li>
                            <?php } ?>                                
                            </ul>
                        </li>
                <?php } } ?>

               
                <?php if($this->Common_model->getstudentgrades($user_id)){ ?>
                    <li><a href="story-time">Story Time</a></li>
                <?php } ?>
                <!-- <li><a href="tutor">Tutors</a></li> -->
            </ul>
        </div>

    <?php } ?>
    