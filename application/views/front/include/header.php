<!DOCTYPE html>
<html lang="en">
<head>
    <!--Required meta tags-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keyword" content="<?php echo $meta_keywords?$meta_keywords:'Home';?>">
    <meta name="description" content="<?php echo $meta_desc?$meta_desc:'Home';?>">
    <meta name="author" content="<?php echo $title?$title:'Home';?>">
    <title><?php echo $title?$title:'Home';?></title>
    <base href="<?php echo base_url();?>">
    <link rel="icon" href="assets/front/images/favicon.png">
    <link rel="stylesheet" href="assets/front/css/bootstrap.css">
    <link rel="stylesheet" href="assets/front/css/custom.css">
    <link rel="stylesheet" href="assets/front/css/responsive.css">
    <link rel="stylesheet" href="assets/front/css/font-awesome.css">
    <link rel="stylesheet" href="assets/front/css/aos.css">
    <link href="assets/front/css/front_developer.css" rel="stylesheet" />
    <script src="assets/front/js/jquery.min.js"></script> 
    <script src="assets/front/js/bootstrap.min.js"></script>
    
  
     
    <script src="assets/front/js/popper.min.js"></script> 
    <script src="assets/js/parsley-min.js"></script>
    <script src="assets/js/developer.js"></script> 
    <script src="assets/js/sweetalert.min.js"></script>
    <!-- <script src="assets/front/js/socket.io.js"></script> -->
    
    <!--Google web font (Poppins)-->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">

    <link rel="stylesheet" href="assets/front/css/select2.min.css">
    <script src="assets/front/js/select2.min.js"></script> 
    <style type="text/css">
        .inactiveLink {
           pointer-events: none;
           cursor: default;
        }
        .files input {
            cursor: pointer!important;
        }

    </style>
    <script src="assets/front/js/moment.min.js"></script> 
    <script src="assets/front/js/moment-timezone.min.js"></script>
    <?php if(!$this->session->userdata('user_timezone') &&  $this->uri->uri_string()!='login'){ ?>
    <script type="text/javascript">
        // $(document).ready(function(){
            var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone; //'Asia/Kolhata' for Indian Time.
            $.post("admin/Ajax/getFrontTimeZone", {timezone: timezone}, function(data) {
            });
        // });
    </script>
    <?php } ?>
    <script>
    function openNav() {
      document.getElementById("mySidepanel").style.width = "33.33%";
      document.getElementById("mySidepanel").style.padding = "12px";
    }

    function closeNav() {
      document.getElementById("mySidepanel").style.width = "0";
      document.getElementById("mySidepanel").style.padding = "0";
    }
    </script>
</head>
<?php if($this->session->userdata('user_id')){ ?>
    <body> 
<?php } else{ ?>
    <body style="overflow: hidden;"> 
<?php } ?>
 <div id="loader-wrapper" style="display:none;">
    <div class="loader" id="loader"></div>
</div> 
<!-- Webchat Script Start -->
<?php

if ($this->uri->segment(1) == 'user-chat') { 
    $chatuser="";
    
    if ($this->session->flashdata('privatechat_user_id')) {
        $chatuser=$this->session->flashdata('privatechat_user_id');
    }
    $cam_detail = json_encode([
                  'cs_user_id'=>  $this->session->userdata('user_id'), 
                  'cs_user_type'=>  $this->session->userdata('front_user_type'), 
                  'cs_school_id'=>  $this->session->userdata('school_id'),
                  'chatuser'=>  $chatuser,
                ]);

  ?>
  
  <script src="<?php echo NODE_URL; ?>" authKey="U2FsdGVkX1+g3Od8NrmQqU52Q0w8EsFRmAfcF0rqfEfbW5NecUZQRy6RsmdqE8SEG7i46mKyFRuTC0UOem984Q+BFwYJbNbkNzY9bRrCO3Q=" csData='<?php echo $cam_detail; ?>'></script>
<?php }
?>
<!-- Webchat Script End  -->

<nav class="navbar navbar-expand-lg">
    <?php if($this->session->userdata('user_id')){ ?>
        <div class="container">
            <button class="navbar-toggler" id="toggle-menu" type="button">
                <span class="fa fa-bars text-white"></span>
            </button>
           <!--  <a class="navbar-brand max-768 inactiveLink" href="#!">
                <img src="assets/front/images/logo_v2.svg" class="img-fluid">
            </a>
            <a class="navbar-brand min-768 inactiveLink" href="#!">
                <img src="assets/front/images/logo_small.svg" class="img-fluid">
            </a> -->
             <a class="navbar-brand max-768 inactiveLink" href="#!">
                <img src="assets/front/images/logo_v3.png" class="img-fluid">
            </a>
            <a class="navbar-brand min-768 inactiveLink" href="#!">
                <img src="assets/front/images/logo_small_v3.png" class="img-fluid">
            </a>
            <div class="dropdown">
                <a href="#" class="user-name" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    <span class="hide-xs"> <?php echo getUserName();?></span>
                    <img src="<?php echo getProfilePicture();?>" />
                </a>
                <ul class="dropdown-menu w-drop" aria-labelledby="dLabel">
                    <?php if($this->session->userdata('front_user_type')==1){ ?>
                        <li><a href="district/profile">My Profile</a></li>
                    <?php }if($this->session->userdata('front_user_type')==2){ ?>
                        <li><a href="principal/profile">My Profile</a></li>
                    <?php  } if($this->session->userdata('front_user_type')==3){ ?>

                         <li><a href="teacher/profile">My Profile</a></li>

                    <?php }if($this->session->userdata('front_user_type')==4){ ?>
                         <li><a href="parent/profile">My Profile</a></li>
                          <li><a href="parent/add-student">Student Profile</a></li>
                    <?php } ?>
                    <li><a href="change-password">Change Password</a></li>

                  

                    <li><a href="logout">Logout</a></li>
                </ul>
            </div>
        </div>
    <?php } else {  ?>
        <div class="container text-center">
           <!--  <a class="navbar-brand" style="margin: 0 auto;" href="#">
                <img src="assets/front/images/logo_v2.svg">
            </a> -->
             <a class="navbar-brand" style="margin: 0 auto;" href="#">
                <img src="assets/front/images/logo_v3.png">
            </a>
        </div>
    <?php } ?>
</nav>