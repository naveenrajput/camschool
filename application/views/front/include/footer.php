<footer>
  <div class="container">
    <div class="d-flex flex-wrap justify-content-between">
      <h5 class="order-change">© <?php echo WEBSITE_NAME;?> | All Rights Reserved</h5>
      <ul>
        <li><a href="about-us">About Us</a></li>
        <li><a href="privacy-policy">Privacy Policy</a></li>
        <li><a href="terms-of-conditions">Terms of Service</a></li>
        <?php if($this->session->userdata('user_id')){ ?>
        <li><a href="chat-at-me-school-info"><?php echo WEBSITE_NAME;?> User Guide</a></li>
        <li><a href="contact-us">Contact Us</a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
</footer>
<!-- chat modal -->



<!-- <div class="modal" id="new-chat-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="chat-tabs">
          <ul class="nav nav-pills" id="pills-tab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="pills-home-tab" onclick="check(this)" data-toggle="pill" href="#pills-home" data-type="private" role="tab" aria-controls="pills-home" aria-selected="true">Private Chat</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="pills-profile-tab" data-type="group" onclick="check(this)" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Group Chat</a>
            </li>
          </ul>
        </div>
        <button type="button" class="close" onclick="hide_modal(this)" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      // end model header
      <div class="modal-body chat-people-list">
        <div class="tab-content" id="pills-tabContent">
          <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
            <div class="" id="tec_list">
            </div>
          </div>
          <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
            <div class="d-flex">
              <div class="w-50">
                <form id="create_group" style="display:none">
                  <div class="">
                    <div class="col-md-12 form-group" id="group-profile-img">
                      <label>Group Image</label>
                      // <input name="grp_image" id="grp_image" class="form-control" type="file" accept="image/*"> 
                      <div class="upload-btn-wrapper">
                        <input type="file" name="grp_image" id="grp_image" accept="image/*">
                        <span class="upload" style="padding:0 !important;">
                          <img src="assets/images/grp_img.png" class="teacher-img teacher-phl" alt="" id="img_src_3">
                        </span>
                        <p id="img_err_3"></p>
                      </div>
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Group Name</label>
                      <input name="grp_name" maxlength="25" id="grp_name" class="form-control" type="text">
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Description</label>
                      <textarea class="form-control" name="grp_description" id="grp_description" rows="2" cols="23" maxlength="250" placeholder="Write description here..."></textarea>
                    </div>
                  </div>

              </div>
              <div class="w-50" id="grp_list"></div>
            </div>
          </div>
        </div>
        <div class="modal-footer d-flex justify-content-center">
          <button type="submit" id="create_grp" class="btn btn-primary">Create Group</button>
        </div>
      </div>
      <div class="p-2">
        <p style="color:crimson; margin: auto; display:none" id="user_err">Select a user.</p>
        <p style="color:crimson; margin: auto; display:none" id="grp_name_err">Enter group name.</p>
        <p style="color:crimson; margin: auto; display:none" id="grp_desc_err">Enter group description.</p>
      </div>
    </div>

  </div>
</div> -->
</div>

<!--- Edit Group Modal Start-->
<script>
  // function readURL(input) {
  //   if (input.files && input.files[0]) {
  //     var reader = new FileReader();

  //     reader.onload = function(e) {
  //       $('#img_src_3').attr('src', e.target.result);
  //     }

  //     reader.readAsDataURL(input.files[0]); 
  // convert to base64 string
  //   }
  // }

  // $("#grp_image").change(function() {
  //   readURL(this);
  // });
</script>


<!-- <div class="modal" id="edit_group">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Group Details</h5>
        <button type="button" class="close" onclick="close_modal()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="suc_msg asfd ads" style="display: none"></div>
      <form></form>
      <form id="form_edit_group">
        <div class="modal-body">
          <div class="form-group qeqeqeq">
            <input type="hidden" id="groupId" name="groupId">
            <input type="hidden" id="grp_old_img" name="grp_old_img" />
            <label for="recipient-name" class="col-form-label">Group Name:</label>
            <input type="text" class="form-control" maxlength="25" name="groupName" id="grpname">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Description:</label>
            <textarea class="form-control" maxlength="250" name="description" id="grp_descrption"></textarea>
          </div>
          <div class="form-group">
            <img src="" id="gp_old_pic" width="300px" height="200px"><br>
            <input type="file" id="new_grp_img" name="new_grp_img" accept="image/*">
          </div>
          <div>
            <p style="color:crimson; margin: auto; display:none" id="grpname_err"> Enter Group Name.</p>
            <p style="color:crimson; margin: auto; display:none" id="grpdesc_err"> Enter Group Description.</p>
          </div>
        </div>
        <div class="modal-footer d-flex justify-content-center"> -->
          <!-- <button type="button" class="btn btn-secondary" onclick="close_modal()">Close</button> -->
          <!-- <button type="submit" class="btn btn-primary">Update Group</button>
        </div>
      </form>
    </div>
  </div>
</div> -->
<!--- Edit Group Modal End-->




<!--- Edit Group_member Modal Start-->

<!-- <div class="modal" id="edit_group_member">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Group Members</h5>
        <button type="button" class="close" onclick="close_modal_edit()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="suc_msg_members" style="display: none; text-align: center; margin-top: 10px; color: #b56f8d; "></div>
      <form id="form_edit_group_members">
        <div class="modal-body">
          <div class="" id="edit_grp_list"></div>
        </div>
        <div class="modal-footer d-flex justify-content-center">
          // <button type="button" class="btn btn-secondary" onclick="close_modal()">Close</button> 
          <button type="submit" class="btn btn-primary">Update Members</button>
        </div>
      </form>
    </div>
  </div>
</div> -->
<!--- Edit  Group_member Modal End-->

<!-- add student confirmation modal -->
<!--jQuery first, then Popper.js, then Bootstrap JS-->
<script src="assets/front/js/aos.js"></script>
<script>
  $(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll >= 200) {
      $(".navbar").addClass("fix-navbar");
    } else {
      $(".navbar").removeClass("fix-navbar");
    }
  });

  AOS.init();
</script>




<script>
  $(document).ready(function() {
    $("#toggle-menu").click(function() {
      $(".sidebar_new").toggle();
    });
  });

  /*$('#btn-one').click(function() {
    $('#btn-one').html('<span class="	fa fa-spinner fa-spin fa-lg mr-2" role="status" aria-hidden="true"></span>Loading...').attr('disabled', true);
  });*/
</script>

<script>
  $(document).ready(function() {
    $(".add-row").click(function() {
      //$("#loader-wrapper").show();
      var grade = $("#grade").val();
      var teacher = $("#teacher").val();
      // var name = $("#name").val();

      var school_id = "<?php echo  $this->session->userdata('school_id'); ?>";
      var user_id = "<?php echo  $this->session->userdata('user_id'); ?>";
      $("#student_form").parsley().validate();
      if ($("#student_form").parsley().isValid()) {
        $('#stu_add_btn').html('<span class="   fa fa-spinner fa-spin fa-lg mr-2" role="status" aria-hidden="true"></span>Loading...').attr('disabled', true);
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>api/add-student-web",
          data: {
            school_id: school_id,
            user_id: user_id,
            // fullname: name,
            grade_teacher: teacher,
            type: 'web'
          },
          headers: {
            'apikey': '<?php echo APP_KEY ?>'
          },
          success: function(response) {
            //$("#loader-wrapper").hide();
            var res = JSON.parse(response);
            $('#stu_add_btn').html('Add').attr('disabled', false);
            if (res.status == 4) {
              location.reload();
            }

            $("#success_div").html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>' + res.msg + '</div>');
            $(".content-body").animate({
              scrollTop: 0
            }, "fast");
            setTimeout(function() {
              $("#success_div").html('');
              $("#msg").html('');
              $("#msg").hide();
              location.reload();
            }, 1500);

          },
          error: function() {
            $('#stu_add_btn').html('Add').attr('disabled', false);
            //$("#loader-wrapper").hide();
          }
        });
      } else {
        //$("#loader-wrapper").hide();
        $('#stu_add_btn').html('Add').attr('disabled', false);
      }

    });


    // Find and remove selected table rows
    $(".delete-row").click(function() {
      $("table tbody").find('input[name="record"]').each(function() {
        if ($(this).is(":checked")) {
          $(this).parents("tr").remove();
        }
      });
    });
  });

  function submitDetailsForm(id, seturl = '') {
    if (seturl) {
      var form_action = seturl;
    } else {
      var form_action = "<?php if (!empty($form_action)) echo $form_action; ?>";
    }
    var last_element = form_action.split("/").pop(-1);
    // alert(last_element);
    $("#" + id).parsley().validate();
    var form = $('#' + id)[0];
    if ($("#" + id).parsley().isValid()) {
      var formData = new FormData(form);
      formData.append('type', 'web');
      //$("#loader-wrapper").show();
      $('.loader_btn').html('<span class="fa fa-spinner fa-spin fa-lg mr-2" role="status" aria-hidden="true"></span>Loading...').attr('disabled', true);
      var btn_text = $("#submit_form").attr('attr-id');
      $.ajax({
        url: form_action,
        type: 'POST',
        data: formData,
        dataType: 'json',
        // async: false,
        cache: false,
        contentType: false,
        processData: false,
        headers: {
          'apikey': '<?php echo APP_KEY ?>'
        },
        success: function(resp) {
          //$("#loader-wrapper").hide();  
          $(".content-body").animate({
            scrollTop: 0
          }, "fast");
          $("html, body").animate({
            scrollTop: 0
          }, "fast");
          $("#msg").show();
          $('.loader_btn').html(btn_text).attr('disabled', false);
          if (resp.status == 4) {
            location.reload();
          }
          if (resp.status == 0) {
            $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>' + resp.msg + '</div>');
            setTimeout(function() {
              $("#msg").html('');
              $("#msg").hide();
            }, 5000);
          } else {

            $("#msg").html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>' + resp.msg + '</div>');
            $("#" + id).parsley().reset();
            if (last_element != 'profile-update' && last_element != 'prinicpal-profile-update' && last_element != 'teacher-update-video-content') {
              $("#" + id)[0].reset();
            }
            setTimeout(function() {
              $("#msg").html('');
              $("#msg").hide();
              if (last_element == 'school-upload-video' || last_element == 'teacher-upload-video' || last_element == 'profile-update' || last_element == 'prinicpal-profile-update' || last_element == 'teacher-update-video-content') {
                location.reload();
              }
            }, 2000);
          }
        },
        error: function(err) {
          $('.loader_btn').html(btn_text).attr('disabled', false);
          $("#loader-wrapper").hide();
        }
      });
    }
  }
</script>

<?php

//if ($this->uri->segment(1) == 'user-chat') { 
  // $cam_detail = json_encode([
  //                 'cs_user_id'=>  $this->session->userdata('user_id'), 
  //                 'cs_user_type'=>  $this->session->userdata('front_user_type'), 
  //                 'cs_school_id'=>  $this->session->userdata('school_id'),
  //               ]);
  ?>
  
  <!-- <script src="<?php //echo NODE_URL; ?>" authKey="U2FsdGVkX1+g3Od8NrmQqU52Q0w8EsFRmAfcF0rqfEfbW5NecUZQRy6RsmdqE8SEG7i46mKyFRuTC0UOem984Q+BFwYJbNbkNzY9bRrCO3Q=" csData='<?php // echo $cam_detail; ?>'></script> -->

<?php // }
?>

<script>
  function back_btn() {
    $("section#chat_system_section .chat-room").css("display", "none");
  }
</script>


<script>
  // function open_list_modal_moved_to_node() {
  // // function open_list_modal() {
  //   console.log('footer function call...');
  //   $('#create_grp').css({
  //     "display": "none"
  //   });
  //   $('#new-chat-modal').show();

  //   if($("#pills-home-tab").hasClass("active")){
  //     $("#pills-home-tab").trigger("click");
  //   }
  //   if($("#pills-profile-tab").hasClass("active")){
  //     $("#pills-profile-tab").trigger("click");
  //   }
  //   // api/parent-chat-list
  //   let Teacher_list_url = '';
  //   let front_user_type = `<?php // echo $this->session->userdata('front_user_type'); ?>`
  //   if (front_user_type == `3`) {
  //     Teacher_list_url = `<?php // echo base_url(); ?>` + 'api/teacher-chat-list';
  //   }
  //   if (front_user_type == `4`) {
  //     Teacher_list_url = `<?php // echo base_url(); ?>` + 'api/parent-chat-list';
  //   }
  //   console.log('Teacher List Url......',Teacher_list_url);
  //   let user_id = `<?php // echo $this->session->userdata('user_id'); ?>`
  //   let school_id = `<?php // echo $this->session->userdata('school_id'); ?>`
  //   if (user_id && school_id) {
  //     $.ajax({
  //       url: Teacher_list_url,
  //       type: 'POST',
  //       data: {
  //         type: 'web',
  //         school_id,
  //         user_id
  //       },
  //       dataType: 'json',
  //       headers: {
  //         'apikey': '<?php // echo APP_KEY ?>'
  //       },
  //       success: function(resp) {

  //         if (resp && resp.status == 1 && resp.user_list && resp.user_list.length > 0) {

  //           var result = resp.user_list.map(val => {

  //             let profile_imgg = '';
  //             let user_type = "";
  //             if (val && val.profile_image && val.profile_image.length > 0) {
  //               profile_imgg = val.profile_image;
  //             } else {
  //               profile_imgg = 'assets/images/user_img.jpeg';
  //             }
  //             if (val.user_type == 3) {
  //               user_type = "Teacher";
  //             } else if (val.user_type == 4) {
  //               user_type = "Parental";
  //             }
  //             return ` <div class="d-flex flex-wrap justify-content-between align-self-center mb-3">
  //                         <div class="d-flex align-items-center">
  //                           <img class="mr-2" width="40" height="40" src="${profile_imgg}"
  //                             alt="phl_img">
  //                             <div calss="align-ment">
  //                             <h6 class="mt-0 mb-0">${val.display_name}</h6>
  //                             <span class="f-12 c-pink">${user_type}</span>
  //                             </div>
  //                         </div>
  //                         <div>
  //                           <a href="javascript:void(0)" onclick="add_chat(this)" data-toggle="tooltip" title="Chat" data-user_id="${val.user_id}">
  //                             <img class="img-fliud" width="30" height="30" src="assets/front/images/chat-icon.png"
  //                               alt="Generic placeholder image">
  //                           </a>
  //                           <!--tips: use $('[data-toggle="tooltip"]').tooltip() to initialize all tooltips-->
  //                         </div>
  //                       </div>`
  //           });

  //           var result1 = resp.user_list.map(val => {
  //             let profile_imgg1 = '';
  //             if (val && val.profile_image && val.profile_image.length > 0) {
  //               profile_imgg1 = val.profile_image;
  //             } else {
  //               profile_imgg1 = 'assets/images/user_img.jpeg';
  //             }
  //             if (val.user_type == 3) {
  //               user_type = "Teacher";
  //             } else if (val.user_type == 4) {
  //               user_type = "Parent";
  //             }
  //             return `<div class="d-flex flex-wrap justify-content-between align-self-center mb-3">
  //                       <div class="d-flex align-items-center">
  //                         <img class="mr-2" width="40" height="40" src="${profile_imgg1}"
  //                           alt="phl_img">
  //                         <div calss="align-ment">
  //                         <h6 class="mt-0 mb-0">${val.display_name}</h6>
  //                         <span class="f-12 c-pink">${user_type}</span>
  //                         </div>
  //                       </div>
  //                       <div>
  //                         <div class="custom-control custom-checkbox">
  //                           <input type="checkbox" class="isChecked custom-control-input" id="customCheck_${val.user_id}" value="${val.user_id}">
  //                           <label class="custom-control-label" for="customCheck_${val.user_id}"></label>
  //                         </div>
  //                       </div>
  //                     </div>`
  //           });

  //           $('#tec_list').empty();
  //           $('#grp_list').empty();
  //           $('#tec_list').append(result);
  //           $('#grp_list').append(result1);
  //         }
  //       },
  //       error: function(err) {
  //         $("#submit_form").attr('disabled', false);
  //         $("#loader-wrapper").hide();
  //       }
  //     });
  //   }
  // }

  // $('#create_group').submit(function(e) {

  //   e.preventDefault();
  //   var grp_name = $('#grp_name').val().trim();
  //   var grp_description = $('#grp_description').val().trim();


  //   var chat_userIds = [];
  //   $('.isChecked').each(function() {
  //     if ($(this).prop("checked") == true) {
  //       var chat_user_id = $(this).val()
  //       chat_userIds.push(chat_user_id);
  //     }
  //   });


  //   if (chat_userIds.length == 0) {
  //     $('#user_err').show();
  //   } else {
  //     $('#user_err').hide();
  //   }
  //   if (grp_name.length == 0) {
  //     $('#grp_name_err').show();
  //   } else {
  //     $('#grp_name_err').hide();
  //   }
  //   if (grp_description.length == 0) {
  //     $('#grp_desc_err').show();
  //   } else {
  //     $('#grp_desc_err').hide();
  //   }

  //   var user_id = `<?php //echo $this->session->userdata('user_id'); ?>`;

  //   if (chat_userIds.length > 0 && grp_name.length > 0 && grp_description.length > 0 && user_id) {
  //     chat_userIds = chat_userIds.toString();
  //     var fm_data = $('#create_group').serialize();
  //     var form = $('#create_group')[0]
  //     var formData = new FormData(form);
  //     formData.append('type', 'web');
  //     formData.append('mobile_auth_token', '123456');
  //     formData.append('user_id', user_id);
  //     formData.append('groupName', grp_name);
  //     formData.append('groupMembers', chat_userIds);
  //     formData.append('description', grp_description);

  //     $('#loader-wrapper').show();
  //     $.ajax({
  //       url: `<?php //  echo CHAT_ADD_GROUP; ?>`,
  //       type: 'POST',
  //       data: formData,
  //       dataType: 'json',
  //       cache: false,
  //       contentType: false,
  //       processData: false,
  //       success: function(resp) {

  //         if (resp && resp.status == 1) {
  //           $('#loader-wrapper').hide();
  //           window.location.reload();
  //         }
  //       },
  //       error: function(err) {
  //         $("#submit_form").attr('disabled', false);
  //         $("#loader-wrapper").hide();
  //       }
  //     });
  //   }

  // });

  // function add_chat(e) {

  //   var chat_user_id = $(e).attr('data-user_id');
  //   var user_id = `<?php //echo $this->session->userdata('user_id'); ?>`;

  //   if (user_id && chat_user_id) {
  //     // $("#loader-wrapper").show();

  //     $(e).html('<span style="color: #b4708d;" class="fa fa-spinner fa-spin fa-lg mr-2" role="status" aria-hidden="true"></span>').attr('disabled', true);


  //     $.ajax({
  //       url: `<?php //echo base_url(); ?>` + 'api/add-chat-user',
  //       type: 'POST',
  //       data: {
  //         type: 'web',
  //         user_id,
  //         chat_user_id
  //       },
  //       dataType: 'json',
  //       headers: {
  //         'apikey': '<?php // echo APP_KEY ?>'
  //       },
  //       success: function(resp) {
  //         $("#loader-wrapper").hide();
  //         if (resp.status == 1 && resp.msg == "success") {
  //           $('div#new-chat-modal.modal').hide();
  //           window.location.reload();
  //         }
  //       },
  //       error: function(err) {
  //         $("#submit_form").attr('disabled', false);
  //         $("#loader-wrapper").hide();
  //       }
  //     });
  //   }
  // }


  function check(e) {
    if ($(e).attr("data-type") == 'private') {
      $('#create_group').css({
        "display": "none"
      });
      $('#create_grp').css({
        "display": "none"
      });
    } else {
      $('#create_group').css({
        "display": "block"
      });
      $('#create_grp').css({
        "display": "block"
      });
    }
  }

  function hide_modal(e) {
    $("#new-chat-modal").css({
      "display": "none"
    });
  }


  function myFunction() {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
      a = li[i].getElementsByTagName("div")[0];
      txtValue = a.textContent || a.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display = "";
      } else {
        li[i].style.display = "none";
      }
    }
  }
</script>

<script>
  function back_btn() {
    $("section#chat_system_section .chat-room").css("display", "none");
  }

  // function checkIsGroup(e) { // remove
  //   $(e).children().find('span.badge').css("display", "none");
  //   var groupId = $(e).attr('data-receiver_group_id');
  //   if (groupId == 0) {
  //     $('#gcs_view_info').css("display", "none");
  //   } else if (groupId != 0 && groupId != '') {
  //     $('#gcs_view_info').css("display", "block");
  //   }
  // }

  function bk_btn() {
    $('.view-info').css("display", "none");
  }

  // function delete_useeInGroups(e) {
  //   if (confirm("Are you sure, you want to delete this user ?")) {

  //     var user_Id = $(e).attr('data-user_id');
  //     var group_Id = $(e).attr('data-groupid');
  //     if (group_Id && user_Id) {
  //       $.ajax({
  //         url: `<?php //echo base_url(); ?>` + 'api/delete-user-ingroup',
  //         type: 'POST',
  //         data: {
  //           type: 'web',
  //           user_Id,
  //           group_Id
  //         },
  //         dataType: 'json',
  //         headers: {
  //           'apikey': '<?php //echo APP_KEY ?>'
  //         },
  //         success: function(resp) {
  //           if (resp.status == 1 && resp.msg == "success") {
  //             // $(e).parent().parent().parent().remove();
  //             window.location.href = ""
  //           }
  //         },
  //         error: function(err) {
  //           $("#submit_form").attr('disabled', false);
  //           $("#loader-wrapper").hide();
  //         }
  //       });
  //     }
  //   }

  // }

  function delete_group_by_admin(e) {
    if (confirm("Are you sure, you want to delete this group ?")) {


      var user_Id = $(e).attr('data-userId');
      var group_Id = $(e).attr('data-groupId');
      if (group_Id && user_Id) {
        $.ajax({
          url: `<?php echo base_url(); ?>` + 'api/delete-group-byadmin',
          type: 'POST',
          data: {
            type: 'web',
            user_Id,
            group_Id
          },
          dataType: 'json',
          headers: {
            'apikey': '<?php echo APP_KEY ?>'
          },
          success: function(resp) {

            if (resp.status == 1 && resp.msg == "success") {
              window.location.href = "<?= base_url('user-chat?type=info');?>"
            }
          },
          error: function(err) {
            $("#submit_form").attr('disabled', false);
            $("#loader-wrapper").hide();
          }
        });
      }
    }
  }

  // function edit_group_by_admin(e) {
  //   console.log('edit group by admin called');

  //   $('#form_edit_group').trigger("reset");
  //   var path = `<?php // echo CHAT_GROUP_IMG_PATH; ?>`
  //   var groupName = $(e).attr('data-groupname');
  //   var description = $(e).attr('data-description');
  //   var groupImage = $(e).attr('data-groupimage');
  //   var groupId = $(e).attr('data-groupid');

  //   // memberids = memberids.split(",");

  //   if (groupId && groupId.length > 0) {
  //     $('#groupId').val(groupId);
  //   }
  //   if (groupName && groupName.length > 0) {
  //     $('#grpname').val(groupName);
  //   }
  //   if (description && description.length > 0) {
  //     $('#grp_descrption').val(description);
  //   }
  //   if (groupImage && groupImage.length > 0) {
  //     $('#grp_old_img').val(groupImage);
  //     $('#gp_old_pic').attr('src', path + groupImage)
  //   } else {
  //     $('#gp_old_pic').attr('src', 'assets/images/no_image.png')
  //   }
  //   if (groupId) {
  //     $('#edit_group').show();
  //   }
  // }

  function close_modal() {
    $('#edit_group').hide();
    $('#form_edit_group').trigger("reset");
  }

  function close_modal_edit() {
    $('#edit_group_member').hide();
    $('#form_edit_group_members').trigger("reset");
  }

  // function add_group_members(e) {
  //   alert('test');
  //   // ajax calling start 
  //   let edit_Teacher_list_url = '';
  //   let front_user_type = `<?php //echo $this->session->userdata('front_user_type'); ?>`
  //   if (front_user_type == `3`) {
  //     edit_Teacher_list_url = `<?php //echo base_url(); ?>` + 'api/teacher-chat-list';
  //   }
  //   if (front_user_type == `4`) {
  //     edit_Teacher_list_url = `<?php //echo base_url(); ?>` + 'api/parent-chat-list';
  //   }

    // let user_id = `<?php //echo $this->session->userdata('user_id'); ?>`
  //   let school_id = `<?php //echo $this->session->userdata('school_id'); ?>`
  //   var memberids = $(e).attr('data-member_ids');

  //   if (user_id && school_id) {
  //     $.ajax({
  //       url: edit_Teacher_list_url,
  //       type: 'POST',
  //       data: {
  //         type: 'web',
  //         school_id,
  //         user_id
  //       },
  //       dataType: 'json',
  //       headers: {
  //         'apikey': '<?php //echo APP_KEY ?>'
  //       },
  //       success: function(resp) {

  //         if (resp && resp.status == 1 && resp.user_list && resp.user_list.length > 0) {

  //           var result = resp.user_list.map(val => {

  //             let profile_imgg = '';
  //             let user_type = "";
  //             if (val && val.profile_image && val.profile_image.length > 0) {
  //               profile_imgg = val.profile_image;
  //             } else {
  //               profile_imgg = 'assets/images/user_img.jpeg';
  //             }
  //             if (val.user_type == 3) {
  //               user_type = "Teacher";
  //             } else if (val.user_type == 4) {
  //               user_type = "Parent";
  //             }
  //             let exist_checked = '';
  //             if (memberids.includes(val.user_id)) {
  //               exist_checked = 'checked';
  //             } else {
  //               return `<div class="d-flex flex-wrap justify-content-between align-self-center mb-3">
  //                         <div class="d-flex align-items-center">
  //                           <img class="mr-2" width="40" height="40" src="${profile_imgg}" alt="phl_img">
  //                           <div calss="align-ment">
  //                           <h6 class="mt-0 mb-0">${val.display_name}</h6>
  //                           <span class="f-12 c-pink">${user_type}</span>
  //                           </div>
  //                         </div>
  //                         <div>
  //                           <div class="custom-control custom-checkbox">
  //                             <input type="checkbox" class="isChecked_mem custom-control-input" ${exist_checked} id="customCheck_${val.user_id}" value="${val.user_id}">
  //                             <label class="custom-control-label" for="customCheck_${val.user_id}"></label>
  //                           </div>
  //                         </div>
  //                       </div>`
  //             }
  //           });

  //           $('#edit_grp_list').empty();
  //           $('#edit_grp_list').append(result);

  //         }
  //       },
  //       error: function(err) {
  //         $("#submit_form").attr('disabled', false);
  //         $("#loader-wrapper").hide();
  //       }
  //     });
  //   }
  //   // ajax calling End  
  //   $('#edit_group_member').show();
  // }

  $('#form_edit_group').submit(function(e) {

    var grpname = $('#grpname').val().trim();
    var grp_descrption = $('#grp_descrption').val().trim();

    if (grpname.length == 0) {
      $('#grpname_err').show();
      return false;
    } else {
      $('#grpname_err').hide();
    }
    if (grp_descrption.length == 0) {
      $('#grpdesc_err').show();
      return false;
    } else {
      $('#grpdesc_err').hide();
    }
    var usrId = `<?php echo $this->session->userdata('user_id'); ?>`;
    var dat = $('#form_edit_group').serialize();
    var form = $('#form_edit_group')[0]
    var formData = new FormData(form);
    formData.append('type', 'web');
    formData.append('mobile_auth_token', '123456');
    formData.append('user_id', usrId);

    e.preventDefault();
    $.ajax({
      url: `<?php echo CHAT_UPDATE_GROUP; ?>`,
      type: 'POST',
      data: formData,
      dataType: 'json',
      headers: {
        'apikey': '<?php echo APP_KEY ?>'
      },
      cache: false,
      contentType: false,
      processData: false,
      success: function(resp) {

        if (resp.status == 1 && resp.message) {
          $('#suc_msg').css("display", "block");
          $('#suc_msg').html(resp.message);
          setTimeout(function() {
            window.location.reload()
          }, 2500);
        }
      },
      error: function(err) {
        $("#submit_form").attr('disabled', false);
        $("#loader-wrapper").hide();
      }
    });
  });


  $('#form_edit_group_members').submit(function(e) {
    let user_id = `<?php echo $this->session->userdata('user_id'); ?>`
    var Add_members_userIds = [];
    $('.isChecked_mem').each(function() {
      if ($(this).prop("checked") == true) {
        var mem_user_id = $(this).val()
        Add_members_userIds.push(mem_user_id);
      }
    });
    let groupid = $('#ad_grp_mem').attr('data-groupid');
    if (Add_members_userIds && Add_members_userIds.length > 0) {
      var form = $('#form_edit_group_members')[0]
      var formData = new FormData(form);
      formData.append('type', 'web');
      formData.append('mobile_auth_token', '123456');
      formData.append('groupMembers', Add_members_userIds);
      formData.append('groupId', groupid);
      formData.append('user_id', user_id);
      e.preventDefault();
      $.ajax({
        url: `<?php echo CHAT_ADD_GROUP_MEMBERS; ?>`,
        type: 'POST',
        data: formData,
        dataType: 'json',
        headers: {
          'apikey': '<?php echo APP_KEY ?>'
        },
        cache: false,
        contentType: false,
        processData: false,
        success: function(resp) {
          if (resp.status == 1 && resp.message) {
            $('#suc_msg_members').css("display", "block");
            $('#suc_msg_members').html(resp.message);
            setTimeout(function() {
              window.location.href = "<?= base_url('user-chat?type=info');?>"
            }, 2500);
          }
        },
        error: function(err) {
          $("#submit_form").attr('disabled', false);
          $("#loader-wrapper").hide();
        }
      });
    }
  });

  function initiateChat(id) {
    var user_id = `<?php echo $this->session->userdata('user_id'); ?>`;
    $.ajax({
      url: `<?php echo base_url(); ?>` + 'api/add-chat-user',
      type: 'POST',
      data: {
        type: 'web',
        user_id: user_id,
        chat_user_id: id
      },
      dataType: 'json',
      headers: {
        'apikey': '<?php echo APP_KEY ?>'
      },
      success: function(resp) {
        if(resp.status){
          window.location.href = "<?= base_url('user-chat');?>";
        }
      },
      error: function(err) {
        $("#submit_form").attr('disabled', false);
        $("#loader-wrapper").hide();
      }
    });
  }


</script>


</body>

</html>