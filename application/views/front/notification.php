
<section class="content">
    <div class="container">
        <div class="row">
            <?php include APPPATH.'views/front/include/sidebar.php';  ?>
            <div class="col-md-12">
                <div class="main-body">
                    <div class="content-header">
                        <h2 class="company-name"><?php echo ucfirst($page_title)?ucfirst($page_title):"";?></h2>
                    </div>
                    <div class="content-body" id="notification_content">
                        
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $( document ).ready(function() {
        $("#loader-wrapper").show();
         $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>api/notification-list",
            data: {type:'web'},
            headers: { 'apikey': '<?php echo APP_KEY ?>' },
            success: function(response) { 
                try {
                    var data =$.parseJSON(response)
                    if(data.status==4){
                        location.reload();
                    }
                } catch(e) {
                    $("#loader-wrapper").hide();
                    $("#notification_content").html(response);
                }
            },error: function(){
                $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Some error occured in loading the data.</div>');
                $('#msg').css('display','block');
                    setTimeout(function() {
                    $('#msg').fadeOut('slow');
                }, 3000); 
            }
        });
    });
</script>