<style type="text/css">
    .main-body-2{
    box-shadow: 0 3px 13px 0px rgb(96 116 170 / 25%);
    background: #fff;
    border-radius: 10px; 
    margin-top: 90px;
    margin-left: 0px;
    clear: both;
}

.main-body-2 .content-header {
    background: #FCFCF2;
    padding: 20px;
    border-radius: 10px 10px 0 0;
    float: left;
    width: 100%;
    position: -webkit-sticky;
    position: sticky;
    top: 0;
    z-index: 9;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    align-content: center;
    align-items: center;
}

.main-body-2 .content-body {
    padding: 15px;
    float: left;
    width: 100%;
    height: calc(100vh - 220px);
    overflow-y: auto;
}

.main-body-2 .content-header .company-name {
    margin: 0;
    font-size: 16px;
    font-weight: 600;
    color: #102148;
    float: left;
}
</style>
<section class="content">
    <div class="container">
        <div class="row"> 

            <?php if(isset($is_sidebar)) { include APPPATH.'views/front/include/sidebar.php';}  ?>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div <?php if(isset($_SESSION['user_id'])){?> class="main-body"<?php }else{?> class="" <?php }?>>
                <!-- <div <?php /* if(isset($_SESSION['user_id'])){?>class="main-body"<?php }else{?> class="main-body-2" <?php } */?>> -->
                    <?php if(!empty($details['title'])) { ?>
                        <div class="content-header">
                            <h2 class="company-name"> <?php echo $details['title'];?> </h2>
                        </div>
                    <?php } ?>
                    <div class="content-body tutor-filter">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <?php if(!empty($details['content'])) { echo $details['content'];}else{ echo 'Not available';}?>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- <div class="spacer20"></div>
<div class="spacer20"></div> -->
