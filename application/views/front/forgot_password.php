
 
<section class="content signup-page login-page">
    <div class="container">
        
        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 right-form">
            <div class="row">
                <div class="spacer20"></div>
                <form class="forgot-form" id="forgot_form" action="" role="form" data-parsley-validate>
                    <div class="col-md-12 col-sm-12 form-group">
                        <p class="alert_message" id="msg" style="display:none;"></p>
                        <?php if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger">
                              <button data-dismiss="alert" class="close" type="button">×</button>
                              <?php echo $this->session->flashdata('error') ?>
                          </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success">
                              <button data-dismiss="alert" class="close" type="button">×</button>
                              <?php echo $this->session->flashdata('success') ?>
                          </div>
                        <?php } ?>
                        <h4 class="text-center"><?php echo $page_title?$page_title:'Forgot Password';?></h4>
                        <p class="f-14">A link to generate a new password will be emailed to you shortly. If you do not see the email in your inbox, check your SPAM folder
                            and add <a href="mailto:info@chatatmeschools.com">info@chatatmeschools.com</a> to your address book for our emails to go directly to your inbox.</p>
                        <div class="spacer10"></div>
                        <input class="form-control"  type="text" id ="email" placeholder="Email Address" type="password" data-parsley-required data-parsley-required-message="Enter your email address" data-parsley-type="email" data-parsley-type-message="Enter a valid email address" autocomplete="off" />
                    </div>
                    <div class="col-sm-12 text-center">
                        <button type="button" id="forgot_submit" class="btn btn-primary loader_btn">Submit</button>
                    </div>
                </form>
                <!-- <h6 class="text-center col-sm-12">Already have an account. <a href="login.php" class="link-btn">Login</a></h6> -->
            </div>
        </div>
    </div>
</section>

<script>
    $("#forgot_submit").on('click', function() {
        var email=$("#email").val();
        $('#forgot_form').parsley().validate();
        if ($('#forgot_form').parsley().isValid()) {
            //$("#loader-wrapper").show();
             $('.loader_btn').html('<span class="   fa fa-spinner fa-spin fa-lg mr-2" role="status" aria-hidden="true"></span>Loading...').attr('disabled', true);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>api/forgot-password",
                data: {email:email,type:'web'},
                headers: { 'apikey': '<?php echo APP_KEY ?>' },
                success: function(response) { 
                    //$("#loader-wrapper").hide();
                    $('.loader_btn').html('Submit').attr('disabled', false);
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    var res = JSON.parse(response);
                    if(res.status==1) {
                       $("#forgot_form")[0].reset();
                       $('#msg').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+res.msg+'</div>');
                        $('#msg').css('display','block');
                        setTimeout(function() {
                            $('#msg').fadeOut('slow');
                        }, 5000); 
                    } else {
                        $('#msg').html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+res.msg+'</div>');
                        $('#msg').css('display','block');
                        setTimeout(function() {
                            $('#msg').fadeOut('slow');
                        }, 3000); 
                    }
                    
                },
                error:function(err){
                  $('.loader_btn').html('Submit').attr('disabled', false);
                }
            });
        } else {
            return false;
        }
    });
    $('#email').keydown(function (e){
        if(e.keyCode == 13){
            $("#forgot_submit").click();
             return false; 
        }
    });
</script>