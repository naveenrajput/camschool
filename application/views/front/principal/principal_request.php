
    <section class="content">
        <div class="container">
            <div class="row">
            <?php include APPPATH.'views/front/include/sidebar.php'; ?>
                <div class="col-md-12">
                    <div class="main-body">
                    <p class="alert_message" id="msg" style="display:none;"></p>
                        <div class="content-header">
                            <h2 class="company-name">Requests</h2>
                        </div>
                        <div class="content-body" id="ajax_data">
                            <?php $this->load->view('front/principal/teacher_request_list_data.php');?>
                            

                            <!-- <div class="col-12 text-center">
                                <a href="javascript:void(0)" onclick="sendSchoolmsg()" class="btn btn-primary ">Send new Message</a>
                            </div> -->
                        </div> 
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
    /*$( document ).ready(function() {
        $("#loader-wrapper").show();
       var school_id = "<?php echo $this->session->userdata('school_id'); ?>";
       
       if(school_id && school_id.length > 0){
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>api/school-request-list",
                data: {type:'web',school_id },
                headers: { 'apikey': '<?php echo APP_KEY ?>' },
                success: function(response) { 
                    $("#loader-wrapper").hide();

                    console.log(response);
                    response = JSON.parse(response);

                    if(response.status == 1 && response.msg){
                        if(response && response.details.length >0 ){
                            var res = response.details.map(val => { 
                            return `<tr>
                                <td><input class="isChecked" type="checkbox" value="${val.teacher_id}"/></td>
                                    <td>${val.first_name} ${val.last_name}</td>
                                    <td>${val.grade}</td>    
                                </tr>`;
                             });    
                             $("#schoollist").append(res);
                        }else{
                            console.log('empty data');
                            $('#content_table').empty();
                            $('#content_table').html('<div class="alert alert-success">No data found</div>');
                        }
                        
                    }else{
                        console.log('empty data 123');
                            $('#content_table').empty();
                            $('#content_table').html('<div class="alert alert-success">No data found</div>');
                    }
                },error: function(){
                    console.log('error.......');
                    $("#loader-wrapper").hide();

                    $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Some error occured in loading the data.</div>');
                    $('#msg').css('display','block');
                        setTimeout(function() {
                        $('#msg').fadeOut('slow');
                    }, 3000); 
                }
            });
        }
         
    });*/

    
    var checked_customer = [];
    var checked_page = [];
    function search_chk_bx(page){
        if (Array.isArray(checked_customer) && checked_customer.length) {
            var i;
            for(i=0;i<checked_customer.length;i++){//alert(i);
                $("#chk_"+checked_customer[i]).prop('checked',true);
            }
        }
        if (Array.isArray(checked_page) && checked_page.length) {
            var page_check=checked_page.includes(page.toString());
            if(page_check){
                $("#select_all_"+page).prop('checked',true);
            }
        }
    }


    function getTeacherValue(ee){
        if($(ee).prop("checked") == true){
            $('input[class="isChecked"]').each(function(){                
                $(this).prop("checked", "checked");
            })            
        }else{    
            $('input[class="isChecked"]').each(function(){                
                $(this).prop("checked", false);
            }) 
        }
    }

    function updateTeacher(){
        var teacher_data = [];
        var school_id = `<?php echo $this->session->userdata('school_id'); ?>`;
        var user_id = `<?php echo $this->session->userdata('user_id'); ?>`;
      
        $('input[class="isChecked"]').each(function(){
            var teacherIDs = $(this).val();
           
            if($(this).prop("checked") == true){
                teacher_data.push(teacherIDs);
            }
        })
        let teacherID = checked_customer.toString();
        if(teacherID && teacherID.length > 0 && user_id && school_id){
            
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>api/school-approved-teacher",
                data: {type:'web',teacher_id: teacherID, school_id ,user_id},
                headers: { 'apikey': '<?php echo APP_KEY ?>' },
                success: function(response) {
                    response = JSON.parse(response);
                   
                    if(response.status==4){
                        location.reload();
                    } 
                    if(response.status == 1 && response.msg){
                        $("#msg").html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+response.msg+'</div>');
                        $('#msg').css('display','block');
                        setTimeout(function() {
                        $('#msg').fadeOut('slow');
                        }, 3000);

                        setTimeout(function() {
                            location.reload();
                        }, 3000);
                        // location.reload();                        
                    }
                },error: function(){
                    
                    $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close"  type="button">×</button>Some error occured in loading the data.</div>');
                    $('#msg').css('display','block');
                    setTimeout(function() {
                         $('#msg').fadeOut('slow');
                    }, 3000);
                }
            });


        }else{
            
            
        }

    }


    function filter_records(page,msg)
    { 
        $('.uk-alert').addClass("display-none");
        $('#notification_msg').html("");
        $("#loader").show();
        if(page==null) {
            page=0;
        }

        var base_url= "<?php echo base_url(); ?>";
        
        $.ajax({
            type:'GET',
            url: base_url+"teacher-list-request-data/"+page,
            success:function(data)
            {
                $("#ajax_data").html(data);
                $("#loader").hide();
                 search_chk_bx(page);
               
            }
        });
    }


</script>