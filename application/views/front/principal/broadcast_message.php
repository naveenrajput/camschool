<?php
    // echo '<pre>'; print_r($this->session->all_userdata());exit;
?>
<section class="content">
    <div class="container">
        <div class="row">
            <?php include APPPATH.'views/front/include/sidebar.php'; ?>
            <div class="col-md-12">
                <div class="main-body">
                    <p class="alert_message" id="msg" style="display:none;"></p>
                    <div class="content-header d-flex justify-content-between">

                        <div>
                            <h2 class="company-name">Broadcast Messages</h2>
                        </div>
                        <div>
                            <a href="teacher-broadcast-page" class="btn btn-primary"><span class="hide-xs">Create New
                                    Message</span> <span class="hide-lg"><i style="font-size:18px"
                                        class="fa fa-pencil"></i></span></a>
                        </div>

                    </div>

                    <div class="content-body" id="content_table">

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>

  <!-- teachers list -->
  <!-- <div class="modal" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableLabel" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-scrollable" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalCenteredLabel">Teachers List</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">×</span>
	        </button>
	      </div>
	      <div class="modal-body" id="model_body">
	        <ul>
	        	<li>msanchez@cusd.com</li>
	        	<li>msanchez@cusd.com</li>
	        	<li>msanchez@cusd.com</li>
	        	<li>msanchez@cusd.com</li>
	        	
	        </ul>
	      </div>
	     
	    </div>
	  </div>
	</div> -->

<script>
    $( document ).ready(function() {
        //$("#loader-wrapper").show();
       
        var user_id = `<?php echo $this->session->userdata('user_id'); ?>`;
    

        if(user_id && user_id.length > 0){

            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>api/broadcast-msg-list",
                data: {type:'web',user_id },
                headers: { 'apikey': '<?php echo APP_KEY ?>' },
                success: function(response) { 
                  

                    if(response.status==4){
                        location.reload();
                    }    
                    if(response){
                        $('#content_table').empty();
                        $('#content_table').html(response);
                    }


                },error: function(){
                    
                    $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Some error occured in loading the data.</div>');
                    $('#msg').css('display','block');
                        setTimeout(function() {
                        $('#msg').fadeOut('slow');
                    }, 3000); 
                }
            });
        }
         
    });

    function deleteBroadMsg(e,notification_id){
        
        var user_id = `<?php echo $this->session->userdata('user_id'); ?>`;
        if(notification_id != "" && notification_id && user_id){
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>api/broadcast-msg-delete",
                data: {type:'web',user_id,notification_id },
                headers: { 'apikey': '<?php echo APP_KEY ?>' },
                success: function(response) {     
                    response = JSON.parse(response);
                    
                    if(response && response.status == 1 && response.msg){
                        $(e).parent().parent().remove();
                        $("#msg").html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Broadcast message deleted successfully.</div>');
                        $('#msg').css('display','block');
                            setTimeout(function() {
                            $('#msg').fadeOut('slow');
                        }, 3000);
                    }
                },error: function(){
                    
                    $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Some error occured in loading the data.</div>');
                    $('#msg').css('display','block');
                        setTimeout(function() {
                        $('#msg').fadeOut('slow');
                    }, 3000); 
                }
            });
        }
    }

    function modelopen(e){
        
        var emails = $(e).attr('data-emails');
        var name = $(e).attr('data-name');
        if(emails){
            emails = emails.split(" ");
            $('#model_body').empty();
            var res = emails.map((val)=>{
                return `<li>${val}</li>`;
            })
            var ul = '<ul>'
            for(var i=0; i< res.length; i++){
                ul += res[i];
            }
            var list = ul+'</ul>'
            $('#model_body').html(list);
            
            $('#exampleModalCenteredLabel').text(name);

            
        }
        
    }

</script>