
    <section class="content">
        <div class="container">
            <div class="row">
            <?php include APPPATH.'views/front/include/sidebar.php'; ?>
                <div class="col-md-12">
                    <div class="main-body">
                    <p class="alert_message" id="msg" style="display:none;"></p>
                        <div class="content-header">
                            <h2 class="company-name">Requests</h2>
                        </div>
                        <div class="content-body" id="content_table">
                            <table class="table table-responsive-sm table-responsive-md table-hover" id="schoollist">
                                <tr>
                                    <th><input type="checkbox" onchange="getTeacherValue(this)"/></th>
                                    <th>Teacher Name</th>
                                    <th>Grade</th>

                                </tr>
                            </table>
                            <div class="col-12 text-center">
                                <a href="javascript:void(0)" onclick="updateTeacher()" class="btn btn-primary ">Request Approved</a> 
                                <!-- // district-administration-compose -->
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
    $( document ).ready(function() {
        $("#loader-wrapper").show();
       var school_id = "<?php echo $this->session->userdata('school_id'); ?>";
       
       if(school_id && school_id.length > 0){
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>api/school-request-list",
                data: {type:'web',school_id },
                headers: { 'apikey': '<?php echo APP_KEY ?>' },
                success: function(response) { 
                    $("#loader-wrapper").hide();

                   
                    response = JSON.parse(response);

                    if(response.status == 1 && response.msg){
                        if(response && response.details.length >0 ){
                            var res = response.details.map(val => { 
                            return `<tr>
                                <td><input class="isChecked" type="checkbox" value="${val.teacher_id}"/></td>
                                    <td>${val.first_name} ${val.last_name}</td>
                                    <td>${val.grade}</td>    
                                </tr>`;
                             });    
                             $("#schoollist").append(res);
                        }else{
                           
                            $('#content_table').empty();
                            $('#content_table').html('<div class="alert alert-success">No data found</div>');
                        }
                        
                    }else{
                
                            $('#content_table').empty();
                            $('#content_table').html('<div class="alert alert-success">No data found</div>');
                    }
                },error: function(){
                    
                    $("#loader-wrapper").hide();

                    $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Some error occured in loading the data.</div>');
                    $('#msg').css('display','block');
                        setTimeout(function() {
                        $('#msg').fadeOut('slow');
                    }, 3000); 
                }
            });
        }
         
    });

    function getTeacherValue(ee){
        if($(ee).prop("checked") == true){
            $('input[class="isChecked"]').each(function(){                
                $(this).prop("checked", "checked");
            })            
        }else{    
            $('input[class="isChecked"]').each(function(){                
                $(this).prop("checked", false);
            }) 
        }
    }

    function updateTeacher(){
        var teacher_data = [];
        var school_id = `<?php echo $this->session->userdata('school_id'); ?>`;
        var user_id = `<?php echo $this->session->userdata('user_id'); ?>`;
      
        $('input[class="isChecked"]').each(function(){
            var teacherIDs = $(this).val();
           
            if($(this).prop("checked") == true){
                teacher_data.push(teacherIDs);
            }
        })
        let teacherID = teacher_data.toString();
        if(teacherID && teacherID.length > 0 && user_id && school_id){
            
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>api/school-approved-teacher",
                data: {type:'web',teacher_id: teacherID, school_id ,user_id},
                headers: { 'apikey': '<?php echo APP_KEY ?>' },
                success: function(response) {
                    response = JSON.parse(response);
                   
                    if(response.status == 1 && response.msg){
                        $("#msg").html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+response.msg+'</div>');
                        $('#msg').css('display','block');
                        setTimeout(function() {
                        $('#msg').fadeOut('slow');
                        }, 3000);

                        setTimeout(function() {
                            location.reload();
                        }, 3000);
                        // location.reload();                        
                    }
                },error: function(){
                   
                    $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close"  type="button">×</button>Some error occured in loading the data.</div>');
                    $('#msg').css('display','block');
                    setTimeout(function() {
                         $('#msg').fadeOut('slow');
                    }, 3000);
                }
            });


        }else{
            
           
        }

    }


</script>