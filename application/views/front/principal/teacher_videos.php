<section class="content">
    <div class="container">
        <div class="row">
        <?php include APPPATH.'views/front/include/sidebar.php'; ?>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="main-body">
                    <div class="message_box" id="msg" style="display:none;"></div>
                        <div class="content-header">
                            <h2 class="company-name mt-2 mb-2">Teachers Videos</h2>
                            <div class="d-flex flex-wrap justify-content-between align-content-center">
                                <button type="" class="openbtn" onclick="openNav()">
                                    <img src="assets/front/images/filter.svg" width="30" alt="filter-icon">
                                </button>  
                                <div id="mySidepanel" class="sidepanel">
                                    <div class="filter-header p-2">
                                        <h6>Filter By</h6>
                                        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                    </div>
                                    <div class="mb-3">
                                        <select class="form-control" id="gradelist" onchange="getTeacherList(this)">
                                            <option value="">Select Grade</option>
                                        </select>
                                    </div>
                                    <div class="mb-3">                           
                                        <select class="form-control" id="teacherList">
                                            <option value="">Select Teacher</option>
                                            <?php if (!empty($teachers_list)) {
                                                foreach ($teachers_list as $row) { ?>
                                                    <option value="<?php echo $row['teacher_id'] ?>"><?php echo ucwords($row['name']); ?></option>
                                                <?php }
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="text-center">
                                        <a onclick="getallvideos();" attr-id="Submit" class="btn btn-primary loader_btn">Search</a>
                                        
                                    </div>
                                     <div class="text-center">
                                         <a href="javascript:void(0)" onclick="reset()"><u>Reset</u></a>
                                         <!-- <button onclick="reset()"  class="btn btn-primary ">Resdfdet</button> -->
                                      
                                       
                                    </div>
                            </div>
                        </div>
                    </div>

                    <div class="content-body">
                        <div class="timeline">
                            <div class="row row-ff" id="videoList">

                            </div>
                        </div>

                       <!--  <div class="col-md-12 text-center" >
                            <a href="javascript:void(0)"  id="moreVideos" onclick="getMoreVideos()" class="btn btn-primary ">See More</a>
                        </div> -->
                        <input type="hidden" id="page" value="0">
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- <div class="spacer20"></div>
<div class="spacer20"></div> -->

<script>
function reset() {
    
    $('#teacherList').prop('selectedIndex',0);
    $('#gradelist').prop('selectedIndex',0);
    $('#teacherList').empty().append(`<option value="">Select Teacher</option>`);
     <?php if (!empty($teachers_list)) {
        foreach ($teachers_list as $row) { ?>
            $('#teacherList').append('<option value="<?php echo $row['teacher_id'] ?>"><?php echo $row['name'] ?></option>');
        <?php }
    } ?>
    // getGradeList();
    getallvideos();
}
getGradeList();
function getGradeList(){
    var schooId = `<?php if($school_id){ echo $school_id; }else{echo '';} ?>`;
   
    if(schooId && schooId.length>0){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>api/grade-list",
            data: {type:'web',school_id: schooId},
            headers: { 'apikey': '<?php echo APP_KEY ?>' },
            success: function(response) {
                response = JSON.parse(response);
               
                if(response.status==4){
                    location.reload();
                } 
                // return false
                if(response.status == 1 && response.msg == 'Grade list' && response.details.length>0){
                    // $('#gradelist').empty('<option value="">Select Grade</option>');

                    // $('#teacherList').empty().append(`<option value="">Select Teacher</option>`);

                   var res = response.details.map((val) => {
                        return `<option value="${val.grade}">${val.grade_display_name}</option>`;
                    });    
                    $('#gradelist').append(res);
                }else{
                    
                    $('#gradelist').empty().append(`<option value="">Select Grade</option>`);
                }
            },error: function(){
                
                $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close"  type="button">×</button>Some error occured in loading the data.</div>');
                $('#msg').css('display','block');
                setTimeout(function() {
                     $('#msg').fadeOut('slow');
                }, 3000);
            }
        });
    }
}

function getTeacherList(e){
    $('#teacherList').empty().append(`<option value="">Select Teacher</option>`);
    $('#teacherList').attr('disabled',true);
    var gradeID = $(e).val().trim();
    var school_id =  `<?php if($school_id){ echo $school_id; }else{echo '';} ?>`;
    var user_id = `<?php echo $this->session->userdata('user_id'); ?>`;
    
    if(gradeID && gradeID.length>0 && school_id && user_id){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>api/teacher-by-grade",
            data: {type:'web',school_id: school_id, grade:gradeID,user_id  },
            headers: { 'apikey': '<?php echo APP_KEY ?>' },
            success: function(response) {
                response = JSON.parse(response);
                $('#teacherList').removeAttr('disabled');
                // $('#teacherList').empty()
                if(response.status == 1 && response.details && response.details.grade && response.details.grade[0] && response.details.grade[0].teacher ){
                   var res = response.details.grade[0].teacher.map((val) => {
                    let Dpname ='';
                       // if((val.display_name).length >0){
                       //      Dpname = val.display_name;
                       // }else{
                            // Dpname = val.first_name+' '+val.last_name;
                            Dpname = (val.first_name+' '+val.last_name).replace(/(^\w|\s\w)/g, m => m.toUpperCase());;
                       // }
                        return `<option value="${val.user_id}">${Dpname}</option>`;
                    });    
                    $('#teacherList').append(res);
                }else{
                   
                    $('#teacherList').empty().append(`<option value="">Select Teacher</option>`);
                }
            },error: function(){
                $('#teacherList').removeAttr('disabled');
                $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close"  type="button">×</button>Some error occured in loading the data.</div>');
                $('#msg').css('display','block');
                setTimeout(function() {
                     $('#msg').fadeOut('slow');
                }, 3000);
            }
        });
    }else{
        $('#teacherList').removeAttr('disabled');
    }
} 
    
function getMoreVideos(){
    var pageno = parseInt($('#page').val());
    pageno = pageno+1
    $('#page').val(pageno);    
    getallvideos();
}
getallvideos();

function getallvideos(tect,grade){
   // $("#loader-wrapper").show();
    var user_type = `<?php echo $this->session->userdata('front_user_type'); ?>`;
    var user_id = `<?php echo $this->session->userdata('user_id'); ?>`;
    var school_id = `<?php if($school_id){ echo $school_id; }else{echo '';} ?>`;
    if(tect != 1){
    var teacher_id = $('#teacherList').val().trim() ? $('#teacherList').val().trim() : "";
    }else{  var teacher_id =''; }
    if(grade != 1){
    var grade = $('#gradelist').val().trim() ? $('#gradelist').val().trim() : "";
     }else{  var grade =''; }
    
    var page = $('#page').val();
    var limit = 10
   
    var data = {type:'web',user_id,user_type,school_id, teacher_id,grade, page,limit  };

    $('.loader_btn').html('<span class="   fa fa-spinner fa-spin fa-lg mr-2" role="status" aria-hidden="true"></span>Loading...').attr('disabled', true);
    
    if(user_type && user_id){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>api/all-media",
            data: data,
            dataType: 'JSON',
            headers: { 'apikey': '<?php echo APP_KEY ?>' },
            success: function(response) { 
               // $("#loader-wrapper").hide();
                // return false
                closeNav();
                $('.loader_btn').html('Search').attr('disabled', false);
              
                if(response && response.status == 1 && response.msg == "Video List" ){
                    if($('#moreVideos').css('display') == 'none'){
                        
                        $('#moreVideos').show();
                    }
                    if(page !=0){
                       $('#videoList').append(response.html)
                    }else{
                        $('#videoList').empty();
                        $('#videoList').html(response.html);
                    }
                   
                }else{
                    if(response && response.status == 0 && response.msg == "No record found" ){
                        $('#moreVideos').hide();
                        $('#videoList').empty();
                        $('#videoList').html(response.html);

                        //$("#msg").html('<div class="alert alert-warning"><button data-dismiss="alert" class="close" type="button">×</button>No more videos</div>');
                        $('#msg').css('display','block');
                            setTimeout(function() {
                            $('#msg').fadeOut('slow');
                        }, 3000);
                    }
                }
                
            },error: function(){
                $('.loader_btn').html('Submit').attr('disabled', false);
                $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Some error occured in loading the data.</div>');
                $('#msg').css('display','block');
                    setTimeout(function() {
                    $('#msg').fadeOut('slow');
                }, 3000); 
            }
        });
    }
};

</script>