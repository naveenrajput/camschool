<?php //echo "<pre>";print_r($details);exit;?>
<form id="profile_form" action="" role="form" data-parsley-validate enctype="multipart/form-data">
    <div class="content-body">
        <div class="row">

            <div class="col-md-12 col-sm-12 form-group">
                <label>Upload a profile picture or use the CAM! avatar.</label><br>
                <div class="upload-btn-wrapper">
                    <input type="file" name="profile_pic" id="fileUpload_1" onchange="renderImage(1)" accept="image/x-png,image/gif,image/jpeg">
                    <span class="upload"><i class="fa fa-camera"></i></span>
                    <img src="<?php echo $details['profile_image']?$details['profile_image']:'assets/front/images/person.jpg';?>" id="img_src_1" class="img-fluid" height="100">
                </div>
                    <div id="img_err" style="color:red;"></div>
            </div>

            <div class="col-md-6 col-sm-6 form-group">
                <label>First Name</label>
                <input class="form-control" name="first_name" id="first_name" value="<?php echo ucwords($details['first_name']) ?>" type="text" maxlength="30" data-parsley-required data-parsley-required-message="Enter your first name" autocomplete="off">
            </div>

            <div class="col-md-6 col-sm-6 form-group">
                <label>Last Name</label>
                <input class="form-control" id="last_name"  name="last_name" value="<?php echo ucwords($details['last_name']) ?>" type="text" maxlength="30" data-parsley-required data-parsley-required-message="Enter your last name" autocomplete="off">
            </div>

            <div class="col-md-6 col-sm-6 form-group">
                <label>Display Name</label>
                <input class="form-control" id="display_name"  name="display_name" value="<?php echo ucwords($details['display_name']) ?>" type="text" maxlength="65" data-parsley-required data-parsley-required-message="Enter your display name" autocomplete="off">
            </div>

            <div class="col-md-6 col-sm-6 form-group">
                <label>Email</label>
                <input type=hidden name="image" id="imageinput" value="">
                <input class="form-control" id="email"  name="email" value="<?php echo $details['email'] ?>" type="text" disabled data-parsley-required data-parsley-required-message="Enter your email address" autocomplete="off">
            </div>                                                  

            <div class="col-md-6 col-sm-6 form-group">
                <label>Phone Number</label>
                <input class="form-control" type="text" value="<?php echo $details['phone']?$details['phone']:'';?>" id="phone" name="phone" placeholder="Phone Number" maxlength="12" data-parsley-required data-parsley-required-message="Enter your phone number" data-parsley-minlength="10" data-parsley-minlength-message="Phone number must be at least 10 digits long." autocomplete="off">
            </div>

            <div class="col-md-6 col-sm-6 form-group"></div>
            
            <div class="col-md-4 col-sm-6 form-group"></div>
            
            <div class="col-md-12 col-sm-12 saprator mt-1 mb-2"></div>

            <div class="col-md-6 col-sm-6 form-group">
                <label>Upload the school's logo</label><br>
                <div class="upload-btn-wrapper">
                    <input type="file" id="fileUpload_2"name="school_logo" onchange="renderImage(2)" accept="image/x-png,image/gif,image/jpeg">
                    <input type="hidden" id="schoollogo" name="croplogo">
                    <!-- <span class="upload"><i class="fa fa-camera"></i></span> -->
                   <img src="<?php echo $details['logo']?$details['logo']:'assets/front/images/phl_school_2x.svg';?>" id="img_src_2" class="img-fluid" height="100">
                    
                </div>
            </div>

            <div class="col-md-6 col-sm-6 form-group">                    
                <div class="col-md-12 col-sm-12 p-0 form-group">
                    <label>School Name</label>
                    <input class="form-control" id="school_name"  name="school_name" value="<?php echo ucfirst($details['school_name']) ?>" type="text" data-parsley-required data-parsley-required-message="Enter your school's name" autocomplete="off" maxlength="100" >
                </div>                                                

                <div class="col-md-12 col-sm-12 p-0 form-group">
                    <label>School Address</label>
                    <textarea class="form-control" id="address"  name="address" rows="2" data-parsley-required data-parsley-required-message="Enter your school's address" autocomplete="off" maxlength="200"><?php echo ucfirst($details['address']) ?></textarea>
                </div>  
            </div>

          
            <div class="col-sm-12 text-center mt-3">
                <button type="button" id="submit_form" attr-id="Update" class="btn btn-primary loader_btn" onclick="return form_submit('profile_form');">Update</button>
            </div>
            
        </div>
    </div>
</form>
<div id="uploadimageModal" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Crop Image</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div id="image_demo" style=" margin-top:10px"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary crop_image">Crop & Upload Image</button>
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </div>
    </div>
  </div>
</div>
<div id="uploadimageModal2" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Crop Image</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div id="image_demo1" style=" margin-top:10px"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary crop_image2">Crop & Upload Image</button>
       <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </div>
    </div>
  </div>
</div>
<script src="assets/front/js/jquerymask.min.js"></script>

<script>  
$(document).ready(function(){

 $image_crop1 = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:200,
      height:200,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  }); 
 $image_crop = $('#image_demo1').croppie({
    enableExif: true,
    viewport: {
      width:200,
      height:200,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  });

  $('#fileUpload_1').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop1.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });

  $('.crop_image').click(function(event){
    $image_crop1.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
       
       $("#img_src_1").attr("src", response); 
       $("#imageinput").val(response);
       
       $('#uploadimageModal').modal('hide')
    })
  });

});  


//school logo 

$('#fileUpload_2').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal2').modal('show');
  });

  $('.crop_image2').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
       
       $("#img_src_2").attr("src", response); 
       $("#schoollogo").val(response);
       
       $('#uploadimageModal2').modal('hide')
    })
  });

 
</script>
<script>

    $('#phone').mask('000-000-0000');

  var imgerror=0;
    function renderImage(seq) {
        $("#img_err").html('');
        //$("#loader-wrapper").show();
        var file = event.target.files[0];
        var fileReader = new FileReader();
        if(!file.type.match('image')) {
            $("#loader-wrapper").hide();
            $("#fileUpload_"+seq+"").val('');
            imgerror=1;
            $("#img_err").html("Image must be a jpg, jpeg or png.");
            return false;
        }else{
            imgerror=0;
        }
        if (file.type.match('image')) {
            var FileExt = file.name.substr(file.name.lastIndexOf('.') + 1);
            if ((FileExt.toUpperCase() != "JPG" && FileExt.toUpperCase() != "JPEG" && FileExt.toUpperCase() != "PNG")) {
                    $("#loader-wrapper").hide();
                    $("#fileUpload_"+seq+"").val('');
                    imgerror=1;
                    $("#img_err").html("File must be jpg or jpeg or png");
                    return false;
            }else{
                imgerror=0;
            }
            fileReader.onload = function() {
              $("#img_src_"+seq+"").attr('src', fileReader.result);
              $("#loader-wrapper").hide();
            };
            fileReader.readAsDataURL(file);
        } 
    }


function form_submit(id)
{   if(imgerror==0){
     submitDetailsForm(id,'<?php echo $form_action;?>');
}   }

</script>