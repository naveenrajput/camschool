    <section class="content">
        <div class="container">
            <div class="row">
            <?php include APPPATH.'views/front/include/sidebar.php'; ?>               
            
                <div class="col-md-12">
                    <div class="main-body">
                    <p class="alert_message" id="msg" style="display:none;"></p>
                        <div class="content-header">
                             <h2 class="company-name"><?php echo ucfirst('teachers');?></h2>
                             <a href="javascript:void(0)" onclick="sendTeachermsg()" class="btn btn-primary ">Send New Message</a>
                        </div>
                        <div class="content-body" id="ajax_data">
                            <?php $this->load->view('front/principal/teacher_list_data.php');?>
                            

                            <!-- <div class="col-12 text-center">
                                <a href="javascript:void(0)" onclick="sendSchoolmsg()" class="btn btn-primary ">Send new Message</a>
                            </div> -->
                        </div>                            
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
   

    function getteacherval(ee){
        // return false
        if($(ee).prop("checked") == true){
            $('input[class="isChecked"]').each(function(){                
                $(this).prop("checked", "checked");
            })            
        }else{    
            $('input[class="isChecked"]').each(function(){                
                $(this).prop("checked", false);
            }) 
        }
    }


  
    var checked_customer = [];
    var checked_page = [];
    function search_chk_bx(page){
        if (Array.isArray(checked_customer) && checked_customer.length) {
            var i;
            for(i=0;i<checked_customer.length;i++){//alert(i);
                $("#chk_"+checked_customer[i]).prop('checked',true);
            }
        }
        if (Array.isArray(checked_page) && checked_page.length) {
            var page_check=checked_page.includes(page.toString());
            if(page_check){
                $("#select_all_"+page).prop('checked',true);
            }
        }
    }


    function filter_records(page,msg)
    { 
        $('.uk-alert').addClass("display-none");
        $('#notification_msg').html("");
        $("#loader").show();
        if(page==null) {
            page=0;
        }

        var base_url= "<?php echo base_url(); ?>";
        
        $.ajax({
            type:'GET',
            url: base_url+"teacher-approved-list-data/"+page,
            success:function(data)
            {
                $("#ajax_data").html(data);
                $("#loader").hide();
                 search_chk_bx(page);
               
            }
        });
    }

      function sendTeachermsg(){
       
        var URL = "<?php echo base_url().'teacher-broadcast-page';?>"
        var teacher_data = [];
        var school_id = `<?php echo $this->session->userdata('school_id'); ?>`;
        var user_id = `<?php echo $this->session->userdata('user_id'); ?>`;
        
        $('input[class="isChecked"]').each(function(){
            var teacher_ids = $(this).val();
            var teacher_email = $(this).attr('data-email');
            var teacher_name  = $(this).attr('data-name');
            if($(this).prop("checked") == true){
                teacher_data.push({
                    name: teacher_name,
                    email: teacher_email,
                    id: teacher_ids
                });
            
            }
        });
        

        if(checked_customer && checked_customer.length > 0 && school_id && user_id){

            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>approved-teacher-sendmsg",
                data: {type:'web',teacher_data: checked_customer, school_id ,user_id},
                headers: { 'apikey': '<?php echo APP_KEY ?>' },
                success: function(response) {
                    response = JSON.parse(response);
                    if(response.status==4){
                        location.reload();
                    } 
                    if(response && response.status == 1 && response.msg && response.details){    
                        window.location.href = URL
                    }
                },error: function(){
                    
                    $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close"  type="button">×</button>Some error occured in loading the data.</div>');
                    $('#msg').css('display','block');
                    setTimeout(function() {
                         $('#msg').fadeOut('slow');
                    }, 3000);
                }
            });
        }else{   
            $("html, body").animate({ scrollTop: 0 }, "fast");  
            $("#msg").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close"  type="button">×</button>Select the recipients</div>');
            $('#msg').css('display','block');
            setTimeout(function() {
                 $('#msg').fadeOut('slow');
            }, 5000);        
           
        }

    }


</script>