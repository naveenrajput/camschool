
<section class="content">
    <div class="container">
        <div class="row">         
            <?php include APPPATH.'views/front/include/sidebar.php';  ?>
            <div class="col-md-12">
                <div class="main-body">
                    <div class="content-header">
                        <h2 class="company-name"><?php echo $page_title?$page_title:"";?></h2>
                    </div>
                    <div class="content-body">
                         <p class="alert_message" id="msg" style="display:none;"></p>
                        <form id="support_form" action="" role="form" data-parsley-validate enctype="multipart/form-data" >
                            <div class="form-group">
                                <label>Name</label>
                                <input class="form-control" type="text" name="name" id="first_name"  maxlength="60" data-parsley-required data-parsley-required-message="Please enter name." value="<?php echo getUserName();?>"/>
                            </div>
                         
                            <div class="form-group">
                                <label>Email</label>
                                 <input class="form-control"  type="text" name="email" id="email" data-parsley-required data-parsley-required-message="Please enter email." data-parsley-type="email" data-parsley-type-message="Please enter valid email." value="<?php echo $this->session->userdata('user_email');?>"/>
                            </div>

                            <div class="form-group">
                                <label>Subject</label>
                                <input class="form-control"  type="text" name="subject" id="subject" maxlength="100"  data-parsley-required data-parsley-required-message="Please enter subject."/>
                            </div>

                            <div class="form-group">
                                    <label>Message</label>
                                <textarea class="form-control" rows="6"  type="text" name="message" id="message" maxlength="600"  data-parsley-required data-parsley-required-message="Please enter message."></textarea>
                            </div>

                            <div class="form-group text-center">
                                <button type="button" id="submit_form" attr-id="Submit" class="btn btn-primary loader_btn" onclick="return form_submit('support_form');" >Submit</button>
                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
function form_submit(id)
{
    submitDetailsForm(id,'<?php echo $form_action;?>');
}
</script>   
