<link rel="stylesheet" href="<?php echo base_url();?>assets/front/css/styles.css">
<section class="section-div py-5 aboutus-section border">
    <div class="container">
        <h2 class="aboutus-title"><?php echo $details['title'];?></h2>
        <?php echo $details['content'];?>
    </div>
</section>