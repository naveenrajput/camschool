<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/developer.css" />
<style type="text/css">
	body {
	    overflow-X: hidden;
	    padding-top: 0px !important;
	    background-color: transparent !important;
	}
</style>
<div class="main_container notificatio_page_main"> 
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 about_page_sec"> 
						<?php echo $details['content'];?>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>
