<link rel="stylesheet" href="<?php echo base_url();?>assets/front/css/styles.css">
<section class="section-div py-5 aboutus-section border">
   <section class="Material-contact-section section-padding section-dark">
      <div class="container">
         
         <div class="row">

            <!-- Section Titile -->
            <div class="col-md-6">
               <h2 class="aboutus-title"><?php echo $details['title'];?></h2>
               <?php echo $details['content'];?>
            </div>
            
         </div>
      </div>
   </section>
</section>
