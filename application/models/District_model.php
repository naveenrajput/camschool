<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class District_model extends CI_Model {

    public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function getSchoolList($districtId,$approve,$isAppCheck=""){
        try{
            $page=trim($this->input->post('page'));
            $limit=trim($this->input->post('limit'));
            if(!$page){
                $page=0;
            }
            if(!$limit){
                $limit=APP_PAGE_LIMIT;
            }
            $offset=$limit*$page;


           
            if($approve==1){
                $sel="s.id,s.name as school_name,first_name,users.last_name,users.display_name";
            }else{
                $sel="s.id,s.name,users.user_id as user_id,users.first_name,users.last_name,users.display_name,users.is_approve,users.phone,users.email";
            }

            $this->db->select($sel);
            $this->db->from('school s');
            $this->db->join('district d','d.id=s.district_id');
            if($approve==1){
                $this->db->join('users','users.school_id=s.id AND users.is_approve =1 and users.email_verified=1 AND users.user_type = 2','left');
            }else{
                $this->db->join('users','users.school_id=s.id AND users.is_approve in (2,3) AND `users`.`user_type` = 2','left');
            }
            if (!empty($districtId)) {
                $this->db->where('s.district_id', $districtId);
            }
            if(!isset($_SESSION['broadcast_email'])){    
              
                if($approve==1){

                    $this->db->where('users.is_approve', $approve);
                }else{
                    if (!empty($isAppCheck)) {
                        $this->db->where('users.is_approve', 3);
                    } else {
                       /* $this->db->group_start();
                        $this->db->where('users.is_approve', 2);
                        $this->db->or_where('users.is_approve', 3);
                        $this->db->group_end();*/
                    }
                }
            }
            // $this->db->where('users.email_verified', 1);
            // $this->db->where('users.user_type', 2);
            // $this->db->where('users.is_deleted', 0);
            // $this->db->order_by('users.created','DESC');
            // $this->db->order_by('s.id','DESC');
            $this->db->order_by('s.id','asc');
            
            if($this->input->post('type')=='Mobile'){
                // $this->db->limit($limit,$offset);
            }
            $query = $this->db->get();
            // echo $this->db->last_query();exit;
            return $query->result_array();
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function getDetailBySlug($slug){
        try{    
            $this->db->select('M.*, IFNULL(C.duration,0) as current_duration');
            $this->db->from('media M');
            $this->db->join('continue_watch_media C','C.media_id = M.id','left');
            $this->db->where('media_slug',$slug);
            $query = $this->db->get();
            //echo $this->db->last_query();die; exit;
            return $query->row_array();
           
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }


}