<?php 

class Common_model extends CI_Model 
{
	function __construct() {
     	parent::__construct();
    }
	
	public function check_login() {
		
		if($this->session->userdata('admin_id')) {

			$admin_id = $this->session->userdata('admin_id');

			if($user_data = $this->Common_model->getRecords('admin','role_id,admin_id,user_type,parent_id,status,is_deleted',array('admin_id'=>$admin_id,'is_deleted' => 0,'status' => 'Active'),'',true)){
	        	if($user_data['user_type'] =='SuperAdmin'){
			        return true;
	           	}
		    }else{
				$this->session->sess_destroy();
				redirect(base_url()."admin/login");
			}
		}
		else{
			redirect('admin/login');
	   }
  	}

	public function role_check($user_id) {
		$role=$this->session->userdata('role_id');
		if($role==4) {
			if($this->Common_model->getRecords('users','user_id',array('user_id'=>$user_id,'checker_id'=>$this->session->userdata('admin_id')),'',true)) {
				return true;
			}else {
				return false;
				//redirect('pages/page_not_found');
			}
		}
	}

	public function paginate($url,$total_row,$yes='') {
		$this->load->library('pagination');
		$config = array();
		$config["base_url"] = $url;
		$config["total_rows"] = $total_row;
		$config["per_page"] = ADMIN_LIMIT;
		// $config['use_page_numbers'] = TRUE;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		if($yes=='') {
			$config["uri_segment"] = 4;	
		}elseif ($yes=='seg5') {
			$config["uri_segment"] = 5;
		}else{
			$config["uri_segment"] = 3;
		}
		
		$config['enable_query_strings']=TRUE;
		$config['reuse_query_string'] = TRUE;
 		$config['num_links'] = 2;
		//$config['page_query_string'] = TRUE;
		$config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='active'><a href='javascript:void(0)'>";
		$config['cur_tag_close'] = "</a></li>";
		$config['next_tag_open'] = "<li class='next'>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li class='prev'>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";

		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}
	
	public function frontPaginate($url,$total_row,$segment) {
		$this->load->library('pagination');
		$config = array();
		$config["base_url"] = $url;
		$config["total_rows"] = $total_row;
		$config["per_page"] = FRONT_LIMIT;
		// $config['use_page_numbers'] = TRUE;
		// $choice = $config["total_rows"] / $config["per_page"];
		// $config["num_links"] = round($choice);
		$config["uri_segment"] = $segment;	
		
		$config['enable_query_strings']=TRUE;
		$config['reuse_query_string'] = TRUE;
		$config['num_links'] = 3;
		//$config['page_query_string'] = TRUE;
		$config['full_tag_open'] = '<div class="pagination pager pull-left" style="display:block !important">';
		$config['full_tag_close'] = '</div>';
		$config['num_tag_open'] = "<div class='pageNumbers'>";
		$config['num_tag_close'] = '</div>';
		$config['cur_tag_open'] = "<div class='pageNumbers'><a href='javascript:void(0);' class='active'>";
		$config['cur_tag_close'] = "</a></div>";
		$config['prev_tag_open'] = "<div class='previousPage'>";
		$config['prev_link'] = "‹";
		$config['prev_tag_close'] = "</div>";
		$config['next_tag_open'] = "<div class='nextPage'>";
		$config['next_link'] = "›";
		$config['next_tag_close'] = "</div>";
		$config['first_tag_open'] = "<div class='firstPage'>";
		$config['first_link'] = "«";
		$config['first_tag_close'] = "</div>";
		$config['last_tag_open'] = "<div class='lastPage'>";
		$config['last_link'] = "»";
		$config['last_tag_close'] = "</div>";

		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}

	public function getUniqueGrade($parent_id){
        $sel='CAST(SG.grade AS UNSIGNED) as grade ,group_concat(distinct(SG.teacher_grade_id)) as teacher_grade_id';
        $this->db->select($sel);
        $this->db->from('student S');
        $this->db->join('student_grade SG','S.id=SG.student_id');
        $this->db->where('S.parent_id',$parent_id);
        $this->db->group_by('SG.grade');
        $this->db->order_by('grade');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getTeacherDetailsByGrade($teacher_grade_id){
        $sel='U.user_id, U.display_name,U.first_name,U.last_name,U.profile_image,TG.grade,TG.grade_display_name,TG.teacher_id';
        $this->db->select($sel);
        $this->db->from('teacher_grade  TG');
        $this->db->join('users U','U.user_id =TG.teacher_id');
        $this->db->where('U.is_deleted','0');
        $this->db->where_IN('TG.id',$teacher_grade_id,FALSE);
        $query = $this->db->get();
        return $query->result_array();
    }
   
     function get_report_list($limit,$offset,$is_userlist="",$all=""){

        if($this->input->get('school_name')){
            $school_name=$this->input->get('school_name');
            $this->db->where("S.name", trim($school_name));
        }
        if($this->input->get('media_title')){
            $title=$this->input->get('media_title');
            $this->db->like("M.title", trim($title));
        }if($this->input->get('report_by')){
            $display_name=$this->input->get('report_by');
            $this->db->group_start();
            $this->db->like("U.display_name", trim($display_name));
            $this->db->or_like("U.first_name", trim($display_name));
            $this->db->or_like("U.last_name", trim($display_name));
            $this->db->group_end();
           
        }
       $sel='U.display_name,U.first_name,U.last_name,CR.id AS reportId,CR.comment_id,CR.user_id,CR.reason,CR.created,S.name,S.id,M.title,C.comment,C.is_delete,C.user_id AS cid,C.media_id,Us.first_name AS fname,Us.last_name AS lname';
        $this->db->select($sel);
        $this->db->from('comment_report CR');
        $this->db->join('users U','U.user_id =CR.user_id');
        $this->db->join('school S','S.id =U.school_id'); 
        $this->db->join('comments C','C.id =CR.comment_id');
        $this->db->join('media M','M.id =C.media_id');
        $this->db->join('users Us','Us.user_id =C.user_id');
        $this->db->order_by('CR.id','Desc');
        //$this->db->limit($limit, $offset);
       if ($all!='') {
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        } else {
            if ($limit==0 && $offset==0) {
                $query = $this->db->get();
               
                if ($query->num_rows() > 0) {
                    return $query->num_rows();
                } else return false;
            }else{
                $this->db->limit($limit,$offset);
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    return $query->result_array();
                } else return false;
            }
        }
    }  
    function get_single_report($data){

       $sel='U.display_name,U.first_name,U.last_name,CR.id AS reportId,CR.comment_id,CR.user_id,CR.reason,CR.created,S.name,S.id,M.title,C.comment,C.is_delete,C.user_id AS cid,C.media_id,Us.first_name AS fname,Us.last_name AS lname';
        $this->db->select($sel);
        $this->db->from('comment_report CR');
        $this->db->join('users U','U.user_id =CR.user_id');
        $this->db->join('school S','S.id =U.school_id'); 
        $this->db->join('comments C','C.id =CR.comment_id');
        $this->db->join('media M','M.id =C.media_id');
        $this->db->join('users Us','Us.user_id =C.user_id');
        $this->db->where('CR.id', $data);
        $this->db->order_by('CR.id','Desc');
        $query = $this->db->get();
        return $query->row_array();
    }
	public function getDropdownList($table,$col1,$col2,$title="",$where="",$group="") {

		$this->db->select($col1);
		$this->db->select($col2);
		if($where != "") {	
			$this->db->where($where);
		}
		if($group != "") {
		 $this->db->group_by($group); 
		}
		$this->db->order_by($col2,'asc');
		$query= $this->db->get($table);
		$query_result = $query->result_array();
		$return = array();
	    if( is_array( $query_result ) && count( $query_result ) > 0 ) {
	    	if($title !=""){
	    		$return[''] = 'Select '.ucfirst($title);
	    	}else{
	        	$return[''] = 'Select '.ucfirst($col1);
	        }
	        foreach($query_result as $row) {
	            $return[$row[$col1]] = $row[$col2];
	        }
	    }
	    return $return;
	}
	
	public function getRecords($table, $fields="", $condition="", $orderby="", $single_row=false,$groupby="",$offset=-1,$limit=10) {

		
		if($fields != "") {
			$this->db->select($fields);
		}

		if($groupby != "") {
			$this->db->group_by($groupby); 
		}
		 
		if($orderby != "") {
			$this->db->order_by($orderby); 
		}

		if($offset>-1) {
			$this->db->limit($limit,$offset);
		}
		
		if($condition != "") {
			$rs = $this->db->get_where($table,$condition);
		} else {
			$rs = $this->db->get($table);
		}
		
		if($single_row) {  
			return $rs->row_array();
		}
		return $rs->result_array();
	}

	public function group_By_Records($table_name='' , $field_name = '', $id='' ,$is_deleted='') {
        $this->db->select("$field_name,count(*) as total");
        if($is_deleted)
        {
        	$this->db->where('is_deleted',0);
        }
        $this->db->group_by($field_name);
        if($id)
        	$this->db->having($field_name,$id);
        $query = $this->db->get($table_name);
        return $query->result_array();
    }

    public function is_deleteable_comma_seperated($table_name='' , $field_name = '', $id=''){
        $this->db->select('id');
        $this->db->where("FIND_IN_SET('$id', $table_name.$field_name) !=", 0);
        $query = $this->db->get($table_name);
        return $query->result_array();
    }
	// this function returns table data.
	public function getFieldValue($table, $fields="", $condition="") {
		if($fields != "") {
			$this->db->select($fields);
		}

		if($condition != "") {
			$rs = $this->db->get_where($table,$condition);
		} else {
			$rs = $this->db->get_where($table);
		}
		
		$result = $rs->row_array();
		return $result[$fields];
	}

	public function addEditRecords($table_name, $data_array, $where='') {
		if($table_name && is_array($data_array)) {
			$columns = $this->getTableFields($table_name);
			foreach($columns as $coloumn_data)
				$column_name[]=$coloumn_data['Field'];
					  
			foreach($data_array as $key=>$val) {
				if(in_array(trim($key),$column_name)) {
					$data[$key] = $val;
				}
			 }

			if($where == "") {	
				$query = $this->db->insert_string($table_name, $data);
				$this->db->query($query);
				return  $this->db->insert_id();
			} else {
				$query = $this->db->update_string($table_name, $data, $where);
				$this->db->query($query);
				return  $this->db->affected_rows();
			}
		}			
	}

	function getNumRecords($table, $fields="", $condition="") {
		if($fields != "") {
			$this->db->select($fields);
		}
		if($condition != "") {
			$rs = $this->db->get_where($table,$condition);
		} else {
			$rs = $this->db->get($table);
		}		
		return $rs->num_rows();
	}
	
	// function for deleting records by condition.
	function deleteRecords($table, $where) { 
		$this->db->delete($table, $where);
		return $this->db->affected_rows();
	}

	// this function is used to get all the fields of a table.
	function getTableFields($table_name) {
		$query = "SHOW COLUMNS FROM $table_name";
		$rs = $this->db->query($query);
		return $rs->result_array();
	}

	// This function is used to set up mail configuration..
	function setMailConfig() {
		$this->load->library('email');
		$config['smtp_host'] = SMTP_HOST;
		$config['smtp_user'] = SMTP_USER;
		$config['smtp_pass'] = SMTP_PASS;
		$config['smtp_port'] = SMTP_PORT;
		$config['smtp_crypto'] = 'ssl';
		$config['validate'] = 'TRUE';
		$config['protocol'] = PROTOCOL;
		$config['mailpath'] = MAILPATH;
		$config['mailtype'] = MAILTYPE;
		$config['charset'] = CHARSET;
		$config['wordwrap'] = WORD_WRAP;
		$config['smtp_timeout'] = 300;
		$config['newline'] = "\r\n";
		$this->email->set_crlf( "\r\n" );

		$this->email->initialize($config);
	}

	function sendEmail($to_email,$subject,$body,$from_email) {
		
		$headers  = 'MIME-Version: 1.0' . "\r\n";
	 	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	 	$this->setMailConfig();
	 	$this->email->set_newline("\r\n");
		$this->email->from($from_email); 
		$this->email->to($to_email,'test');
		$this->email->subject($subject); 
		$this->email->message($body); 
		return $this->email->send();
	}

	public function check_user_login() {
		if ($this->input->is_ajax_request()) {
			if($this->session->userdata('user_id')) {
				return true;
			} else {
				display_output('4','Session expired');
			}
		}else{
			if($this->session->userdata('user_id')) {
				return true;
			} else {
				redirect('');
			}
		}
		
	}

	function defaultEmailSend($to_email,$subject,$body,$from_email) {
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= "From: ".$from_email."\r\n";
		if(mail($to_email,$subject,$body,$headers)){
			return true;
		} else {
			return false;
		}
	}

	public function remember_me() {
        $result = $this->common_model->getRecords('admin','*',array('admin_id ' => $_COOKIE['remember_me']),'',true);
        $this->session->set_userdata('admin_auth',$result);
    }

    public function groupByRecords($table_name='' , $field_name = '', $id='') {
        $this->db->select("$field_name,count(*) as total");
        $this->db->group_by($field_name);
		if($id)
			$this->db->having($field_name,$id);        
        $query = $this->db->get($table_name);
        return $query->result_array();
    }

    public function groupBytotal($table_name='' , $name = '', $field_name = '', $id='') {
        $this->db->select("count($name) as total");
        $this->db->where('is_deleted','0');
        $this->db->where($field_name,$id);
        $query = $this->db->get($table_name);
        return $query->row_array();
    }

    public function getRecordsimg($table, $fields="", $condition="", $orderby="", $single_row=false,$limit=-1) {
		if($fields != "") {
			$this->db->select($fields);
		}
		 
		if($orderby != "") {
			$this->db->order_by($orderby); 
		}

		if($limit>-1) {
			$this->db->limit(5,$limit);
		}
		
		if($condition != "") {
			$rs = $this->db->get_where($table,$condition);
		} else {
			$rs = $this->db->get($table);
		}
		//echo $this->db->last_query(); exit;
		
		if($single_row) {
			return $rs->row_array();
		}
		return $rs->result_array();
	}

	

    public function getPermissionsadd() {

		$type = $this->session->userdata('user_type');
		if($type=='Super Admin'){
			$type='Admin';
		}

		$menuarray=array();
		$sections = $this->Common_model->getRecords('sections','*',array('type'=>$type,'status'=>'Active','parent_id'=>'0'),'sort_order asc',false); 
		foreach ($sections as $key => $value) {
			array_push($menuarray,$value);
			$subsections = $this->Common_model->getRecords('sections','*',array('type'=>$type,'status'=>'Active','parent_id'=>$value['id']),'sort_order asc',false); 
			if(!empty($subsections)){
				foreach ($subsections as $value1) {
					array_push($menuarray,$value1);
				}
			}

		}
		return $menuarray;
		
    }

	public function getrolePermissions($role_id) {

		$type = $this->session->userdata('user_type');
		if($type=='Super Admin'){
			$type='Admin';
		}

		$menuarray=array();

		$this->db->select('i.*,s.id as section_id,s.name,s.parent_id');
		$this->db->from('sections s');
		$this->db->join('role_permissions i','s.id = i.section_id and i.role_id='.$role_id,'left');       
		// $this->db->where('i.role_id',$role_id);
		$this->db->where('s.type',$type);
		$this->db->where('s.parent_id',0);
		$this->db->where('s.status','Active');
		$this->db->order_by('s.sort_order');
		$query = $this->db->get();
		$sections= $query->result_array();

		foreach ($sections as $key => $value) {
			array_push($menuarray,$value);

			$this->db->select('i.*,s.id as section_id,s.name,s.parent_id');
			$this->db->from('sections s');
			$this->db->join('role_permissions i','s.id = i.section_id and i.role_id='.$role_id,'left'); 
			$this->db->where('s.type',$type);
			$this->db->where('s.parent_id',$value['section_id']);
			$this->db->order_by('s.sort_order');
			$query = $this->db->get();
			$subsections= $query->result_array();

			if(!empty($subsections)){
				foreach ($subsections as $value1) {
					array_push($menuarray,$value1);
				}
			}
		}

		return $menuarray;

    }
    public function get_admin_list($offset=0) 
	{
	 	/*$roles_reci = 	get_sub_role_id($this->session->userdata('role_id'));
	 	$roles_reci  = ltrim($roles_reci, ",");
		if($this->session->userdata('user_type')!='Super Admin') {
			if(!empty($roles_reci)) { 
		 	 	$where .= "admin.role_id IN ($roles_reci) AND admin.is_deleted=0 AND admin.admin_id!=".$this->session->userdata('admin_id');
		 	} else {
		 		$where .= "admin.is_deleted=0 AND admin.admin_id!=".$this->session->userdata('admin_id');
		 	}
		} else {
			$where="admin.admin_id!=0 AND admin.is_deleted=0";
		}*/
		$admin_id=$this->session->userdata('admin_id');
		$where="admin.parent_id='".$admin_id."' AND admin.is_deleted=0";

		if($this->input->get('name')) {
			$name=$this->input->get('name');
			$where.=" and admin.fullname LIKE '%".$this->db->escape_like_str(trim($name))."%' ";
		} 
		if($this->input->get('role_name')) {
			$role_name=$this->input->get('role_name');
			$where.=" and admin.role_id =".$role_name;
		} 
		if($this->input->get('username')) {
			$username=$this->input->get('username');
			$where.=" and admin.username LIKE '%".$this->db->escape_like_str(trim($username))."%' ";
		}
		if($this->input->get('email')){
			$email=$this->input->get('email');
			$where.=" and admin.email LIKE '%".$this->db->escape_like_str(trim($email))."%' ";
		}
		if($this->input->get('mobile')){
			$mobile=$this->input->get('mobile');
			$where.=" and admin.mobile LIKE '%".$this->db->escape_like_str(trim($mobile))."%' ";
		}
		if($this->input->get('role')){
			$role=$this->input->get('role');
			$where.=" and admin.role_id = '".$role."' ";
		} 
		$where.=" and roles.hide = 0 ";
		$limit=ADMIN_LIMIT;
	 	$sql="SELECT admin.*,roles.name as role_name 
		FROM  admin inner join roles on roles.role_id = admin.role_id WHERE $where ORDER BY admin_id DESC
		limit $offset,$limit";
		$query=$this->db->query($sql);

		if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
	}	
    public function get_admin_total() 
	{
		/*$roles_reci = 	get_sub_role_id($this->session->userdata('role_id'));
	 	$roles_reci  = ltrim($roles_reci, ",");
		if($this->session->userdata('user_type')!='Super Admin') {
			if(!empty($roles_reci)) {
		 	 	$where .= "admin.role_id IN ($roles_reci) AND admin.is_deleted=0 AND admin.admin_id!=".$this->session->userdata('admin_id');
		 	} else {
		 		$where .= "admin.is_deleted=0 AND admin.admin_id!=".$this->session->userdata('admin_id');
		 	}
		} else {
			$where=" admin.admin_id!=0 and admin.is_deleted=0";
		}*/
		$admin_id=$this->session->userdata('admin_id');
		$where="admin.parent_id='".$admin_id."' AND admin.is_deleted=0";

		if($this->input->get('name')) {
			$name=$this->input->get('name');
			$where.=" and admin.fullname LIKE '%".$this->db->escape_like_str(trim($name))."%' ";
		} 
		if($this->input->get('role_name')){
			$role_name=$this->input->get('role_name');
			$where.=" and admin.role_id =".$role_name;
		} 
		if($this->input->get('username')){
			$username=$this->input->get('username');
			$where.=" and admin.username LIKE '%".$this->db->escape_like_str(trim($username))."%' ";
		}
		if($this->input->get('email')){
			$email=$this->input->get('email');
			$where.=" and admin.email = '".$this->db->escape_like_str(trim($email))."' ";
		}
		if($this->input->get('mobile')){
			$mobile=$this->input->get('mobile');
			$where.=" and admin.mobile = '".$this->db->escape_like_str(trim($mobile))."' ";
		}
		if($this->input->get('role')){
			$role=$this->input->get('role');
			$where.=" and admin.role_id = '".$role."' ";
		}

		$limit=ADMIN_LIMIT;
		$sql="SELECT admin.*,roles.name as role_name FROM admin inner join roles on roles.role_id = admin.role_id	
		WHERE $where
		";
		$query=$this->db->query($sql);
		
		if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else return false;
	}	
 	public function get_role_list($offset=0) {
 		$type = $this->session->userdata('user_type');
 		$admin_id=$this->session->userdata('admin_id');
        if($type=='Super Admin'){
            $type='Admin';
        }

		$where="type ='".$type."' and hide=0";

		if($type!='Admin'){
        	
        	$where.=" and created_by ='".getParentAdminId($admin_id)."'";
        }

		if($this->input->get('name')){
			$name=$this->input->get('name');
			$where.=" and roles.name LIKE '%".$this->db->escape_like_str(trim($name))."%' ";
		} 
		 
		$limit=ADMIN_LIMIT;
	 	$sql="SELECT *
		FROM  roles 
		WHERE $where
		ORDER BY parent_id ASC
		limit $offset,$limit";
		$query=$this->db->query($sql);
		
		if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
	}	

	public function get_role_total() {
		$type = $this->session->userdata('user_type');
		$admin_id=$this->session->userdata('admin_id');
        if($type=='Super Admin'){
            $type='Admin';
        }
		$where="type ='".$type."'  and hide=0";

        if($type!='Admin'){
        	
        	$where.=" and created_by ='".getParentAdminId($admin_id)."'";
        }

		if($this->input->get('name')) {
			$name=$this->input->get('name');
			$where.=" and roles.name LIKE '%".$this->db->escape_like_str(trim($name))."%' ";
		} 
		 
		$limit=ADMIN_LIMIT;
		$sql="SELECT *
		FROM  roles
		WHERE $where
		";
		$query=$this->db->query($sql);
		
		if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else return false;
	}	 

	/*public function check_permission($section_id,$user_id,$action,$redirect='') {
		if(empty($redirect)) {
		 	$this->db->select('ia.*');
	        $this->db->from('user_access ia'); 
	  		$this->db->where('ia.admin_id',$user_id);
	  		$this->db->where('ia.section_id',$section_id);
	  		$this->db->where('ia.'.$action,'1');
	       	$query = $this->db->get();
	        return ;
		} else {

		}
	}*/
	
	public function common_info($primary_table,$join_table,$clause,$where="",$single_row=false, $orderby="") {
        $this->db->select(''.$primary_table.'.*');
        $this->db->from($primary_table);
        if($where != "") {
	        $this->db->where($where);
        }
        if($orderby != "") {
			$this->db->order_by($orderby); 
		}
        $this->db->join($join_table,$clause, 'left');
        $query= $this->db->get();
        if($single_row) {  
            return $query->row_array();
        }
        return $query->result_array();
    }

    public function check_user_exist($user_id) {
        $this->db->select('user_id');
        $this->db->from('users');
        $this->db->where('user_id',$user_id,false);
        $query= $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }

    public function push_notification_send($user_id,$notification_data,$record_id,$alert_message,$description_message,$extra_msg_for_title='',$created_by='1') {
    	//$log=$this->getRecords('admin','device_type,device_id',array('admin_id'=>$user_id,'is_deleted'=>0),'',true);
    	$log=$this->getRecords('users','device_type,device_id',array('user_id'=>$user_id,'send_notification'=>1),'',true);
    	//echo '<pre>';print_r($log);
        //$badge_count = $this->badge_count($user_id,'users','user_id');
        $request_status="";
        if(isset($notification_data['status'])){
        	$request_status=$notification_data['status'];
        }
        $iosarray = array(
            //'alert' => $alert_message.' :- '.$description_message,
            'alert' => $alert_message,
            // 'title' => $alert_message.$extra_msg_for_title,
            'type'  => $notification_data['type'],
            'request_status'  => $request_status,
            'user_id' => $user_id,
            'record_id' => $record_id,
            // 'badge' =>  $badge_count,
            'sound' => 'default',
            'msg' => $description_message
        );

        $andarray = array(
            'message'   =>$alert_message.$extra_msg_for_title,
            'type'      =>$notification_data['type'],
            'user_id' => $user_id,
            'record_id' => $record_id,
            'title'     => 'Notification',
            'request_status'  => $request_status,
            'msg' => $description_message
        );
        if(!empty($log)){
            if(!empty($log['device_id'])){
            	$device_type  = strtoupper($log['device_type']);
                if($device_type == 'ANDROID'){
                    $referrer = androidNotification($log['device_id'],$andarray);
                }
                if($device_type == 'IOS'){
                    $referrer = iosNotification($log['device_id'],$iosarray);
                }
            }
        } 
        $add_data =array(
        	'user_id' => $user_id,
        	'created_by' =>$created_by,
        	'notification_type'=>$notification_data['type'],
        	'notification_title'=>$alert_message.$extra_msg_for_title, 
        	'notification_description'=>$description_message,
        	'creation_datetime' => date("Y-m-d H:i:s"),
        	'notification_sent_datetime' => date("Y-m-d H:i:s"),
        	'record_id'=>$record_id
        ); 
        $this->addEditRecords('notifications',$add_data); 
    }

 	public function get_notification_list($offset=0) {
		
		$where="";
		$where .= "is_deleted =0";
		if($this->input->get('title')) {
			$title=$this->input->get('title');
			$where.=" and broadcast_notification.title LIKE '%".$this->db->escape_like_str(trim($title))."%' ";
		} 
		if($this->input->get('content')){
			$content=$this->input->get('content');
			$where.=" and broadcast_notification.content LIKE '%".$this->db->escape_like_str(trim($content))."%' ";
		} 
		$limit=ADMIN_LIMIT;
		$sql="SELECT * From broadcast_notification WHERE $where ORDER BY notification_id DESC limit $offset,$limit";


		$query=$this->db->query($sql);
		
		if ($query->num_rows() > 0) {
             return $query->result_array();
        } else return false;
	}

	public function get_notification_total() {
		
		$where="";
		$where .= "is_deleted =0";
		if($this->input->get('title')) {
			$title=$this->input->get('title');
			$where.=" and broadcast_notification.title LIKE '%".$this->db->escape_like_str(trim($title))."%' ";
		} 
		if($this->input->get('content')){
			$content=$this->input->get('content');
			$where.=" and broadcast_notification.content LIKE '%".$this->db->escape_like_str(trim($content))."%' ";
		} 
		$limit=ADMIN_LIMIT;
		$sql="SELECT * From broadcast_notification WHERE $where";
		$query=$this->db->query($sql);
		
		if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else return false;
	}

    public function badge_count($id,$table,$list) {
        $where = array($list =>$id);
        $badge_count=$this->getFieldValue($table,'badge_count',$where); 
        $badge_count=$badge_count+1;
        $update_data = array('badge_count' =>$badge_count);
        $this->addEditRecords($table,$update_data,$where);
        return $badge_count;
    }
    public function ajaxPagination($total_row,$url,$per_page) {
		$this->load->library("pagination");
		$config = array();
        $config["base_url"] = $url;
        $config["total_rows"] = $total_row;
        $config["per_page"] = ADMIN_LIMIT;
        $config["uri_segment"] = 5;
       	$config['enable_query_strings']=TRUE;
		$config['reuse_query_string'] = TRUE;
 		$config['num_links'] = 2;
		//$config['page_query_string'] = TRUE;
		$config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='active'><a href='javascript:void(0)'>";
		$config['cur_tag_close'] = "</a></li>";
		$config['next_tag_open'] = "<li class='next'>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li class='prev'>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
        $this->pagination->initialize($config);
    }
    /****************************Comment section ****************************************/
    public function getTotalComments($data){
    	$this->db->select("count(c.id) as comment_count");
        $this->db->from('comments c');
        $this->db->join('users u','u.user_id = c.user_id');
        $this->db->where('u.is_approve', 3);
        $this->db->where('u.is_deleted', 0);
	    $this->db->where('u.status', 1); 
	     $this->db->where('c.is_delete', 0);
	    if(!empty($data['media_id']))
	    	$this->db->where('c.media_id', $data['media_id']);
	    
	    if(isset($data['request_type']) && $data['request_type'] == 'previous'){
			$parentIds = implode(",", $data['parent_id']);
			$this->db->where('c.parent_id = (SELECT parent_id FROM comments WHERE id IN ('.$parentIds.'))');
		}else if(!empty($data['parent_id'])){
	    	$parentIds = implode(",", $data['parent_id']);
	    	$this->db->where('(c.parent_id IN ('.$parentIds.'))');
		}
	    
  		// if(!empty($data['user_id']))
	   //  	$this->db->where('c.user_id',$data['user_id']);

	    $this->db->order_by('c.created','DESC');

	    if(!empty($data['limit'])){
	    	$this->db->limit($data['limit'], $data['offset']);
	    }

       	$query = $this->db->get();
        return $query->row_array();
    }
     public function getMediaComments($data){
    	$addFields = $userComment = "";
    	$userId = ($data['user_id'] > 0) ? $data['user_id']: 0;
    	if(empty($data['limit'])){
    		$addFields = ", (SELECT count(id) FROM comments WHERE parent_id = com_id) as reply_count";
    	}
    	$this->db->select("c.id,c.id as com_id, c.parent_id, c.media_id, c.user_id, c.comment_type, c.comment, c.created as comment_publish, m.media_type, m.duration, m.thumb_image, m.media_slug, m.title, m.description, m.views, m.format, m.large_image, m.media_name, m.status,  m.comments, m.created_as, m.likes, m.dislikes, u.first_name, u.last_name, u.display_name,u.user_type,u.profile_image, (SELECT count(id) FROM comment_like WHERE comment_id = c.id AND user_id = ".$userId.") as comment_like,
    		(SELECT count(id) FROM comment_dislike WHERE comment_id = c.id AND user_id = ".$userId.") as comment_dislike,
    		(SELECT count(id) FROM comment_dislike WHERE comment_id = c.id) as dislike_count,
    		(SELECT count(id) FROM comment_like WHERE comment_id = c.id) as like_count,
    		(SELECT count(id) FROM comment_report WHERE comment_id = c.id) as report_count,
    		(SELECT count(id) FROM comment_report WHERE comment_id = c.id AND user_id = ".$userId.") as report_by,
    		$addFields");
        $this->db->from('comments c');
        $this->db->join('users u','u.user_id = c.user_id');
        $this->db->join('media m','m.id = c.comment', 'left');
        $this->db->where('u.is_approve', 3);
        $this->db->where('u.is_deleted', 0);
	    $this->db->where('u.status', 1);
	    $this->db->where('c.is_delete', 0);
	    if(!empty($data['media_id']))
	    	$this->db->where('c.media_id',$data['media_id']);
	    
	    if(isset($data['request_type']) && $data['request_type'] == 'previous'){
			$parentIds = implode(",", $data['parent_id']);
			$this->db->where('c.parent_id = (SELECT parent_id FROM comments WHERE id IN ('.$parentIds.'))');
		}else if(!empty($data['parent_id'])){
	    	$parentIds = implode(",", $data['parent_id']);
	    	$this->db->where('(c.parent_id IN ('.$parentIds.'))');
		}
	    
	    $this->db->order_by('c.created','DESC');
	    if(!empty($data['limit'])){
	    	$this->db->limit($data['limit'], $data['offset']);
	    }
       	$query = $this->db->get();
        return $query->result_array();	
    }	

    public function getstudentgrades($parent_id) {

    	$this->db->select('SG.grade');
        $this->db->from('student S');
        $this->db->join('student_grade SG','SG.student_id=S.id');
        $this->db->join('teacher_grade TG','TG.id=SG.teacher_grade_id');
        $this->db->where('S.parent_id', $parent_id);
        $this->db->group_start();
	        $this->db->where('SG.is_approve', 2);
	        $this->db->or_where('SG.is_approve', 1);
        $this->db->group_end();
        $this->db->where('TG.grade<=', 7);	
        // $this->db->where('TG.grade<=', 6);	
        // $this->db->where('TG.grade >', 6);
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        return $query->result_array();
    }

    public function mediaNotifyUsers($teacher_grade_id) {

    	$this->db->select('s.parent_id');
        $this->db->from('student_grade sg');
        $this->db->join('student s','s.id=sg.student_id');
        $this->db->where('sg.teacher_grade_id', $teacher_grade_id);
        $this->db->where('sg.is_approve', 2);
        $this->db->group_by('s.parent_id');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getStoryCheck($recordId,$type='') {
    	$this->db->select('g.grade');
    	if ($type=='student') {
	        $this->db->from('student');
        	$this->db->join('student_grade g','g.student_id=student.id');
	        $this->db->where('student.parent_id', $recordId);
	        $this->db->where('g.is_approve', 2);
    	} else {
	        $this->db->from('users');
        	$this->db->join('teacher_grade g','g.teacher_id=users.user_id');
	        $this->db->where('users.user_id', $recordId);
	        $this->db->where('users.user_type', 3);
    	}
        $this->db->where('g.grade<=', 7);
        // $this->db->where('g.grade<=', 6);
        $query = $this->db->get();
        // echo $this->db->last_query();exit();
        return $query->result_array();
    }
    public function getEmailNotificationList(){
        try{
            $this->db->select('e.*,u.user_type as to_user_type');
            $this->db->from('email_notification e');
            $this->db->join('users u','u.user_id=e.user_id','left');
            $this->db->where('e.status', 0);
            $this->db->order_by('e.id','ASC');
            $query = $this->db->get();
            //echo $this->db->last_Query();exit();
            return $query->result_array();
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function getChatunreadCount($user_id){
        try{
        	
            $this->db->select('count(is_read) as unreadcount');
            $this->db->from('chat_message_recipients');
            $this->db->where('is_read', 0);
            $this->db->where('is_active', 1);
            $this->db->where('deleted', 0);
            $this->db->where('recipient_id', $user_id);
            $query = $this->db->get();
            $res=$query->row_array();
            // echo $this->db->last_query();
            return $res['unreadcount'];
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function ranges($grade){
        switch ($grade) {
            case 'Pre-Kindergarten':
            case 'PK':
            case 'pk':
                return 0;
            case 'Kindergarten':
            case 'K':
            case 'k':
                return 1;
            break;
            case '1st Grade':
            case '1st grade':
            case '1':
                return 2;
            break;
            case '2nd Grade':
            case '2nd grade':
            case '2':
                return 3;
            break;
            case '3rd Grade':
            case '3rd grade':
            case '3':
                return 4;
            break;
            case '4th Grade':
            case '4th grade':
            case '4':
                return 5;
            break;
            case '5th Grade':
            case '5th grade':
            case '5':
                return 6;
            break;
            case '6th Grade':
            case '6th grade':
            case '6':
                return 7;
            break;
            case '7th Grade':
            case '7th grade':
            case '7':
                return 8;
            break;
            case '8th Grade':
            case '8th grade':
            case '8':
                return 9;
            break;
            case '9th Grade':
            case '9th grade':
            case '9':
                return 10;
            break;
            case '10th Grade':
            case '10th grade':
            case '10':
                return 11;
            break;
            case '11th Grade':
            case '11th grade':
            case '11':
                return 12;
            break;
            case '12th Grade':
            case '12th grade':
            case '12':
                return 13;
            break;
        }
    }    
    	
}

	