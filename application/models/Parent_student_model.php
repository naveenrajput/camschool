<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parent_student_model extends CI_Model {

    public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();
    }


    public function getTeacherByGrade($grade,$school_id){
        try{
            $this->db->select('U.user_id,U.first_name,U.last_name,U.display_name,TG.id as teacher_grade_id,TG.grade');
            $this->db->from('teacher_grade TG');
            $this->db->join('users U','TG.teacher_id= U.user_id');
            $this->db->where('U.school_id', $school_id);
            $this->db->where('U.user_type', 3);
            /*$this->db->group_start();
            $this->db->where('U.is_approve', 2);
            $this->db->or_where('U.is_approve', 3);
            $this->db->group_end();*/
            $this->db->where('U.is_approve', 3);
            
            $this->db->where('U.is_deleted', 0);
            $this->db->where('TG.grade', $grade);
            $this->db->where('TG.school_id', $school_id);
           // $this->db->order_by('U.','DESC');
           // $this->db->group_by('TG.teacher_id');
            $query = $this->db->get();
            return $query->result_array();
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }
    public function getStudentDetail($student_id){
        try{
            $this->db->select('SG.grade,SG.teacher_grade_id,SG.is_approve,U.first_name,U.last_name,U.display_name,TG.grade_display_name');
            $this->db->from('student_grade SG');
            $this->db->join('teacher_grade TG','TG.id = SG.teacher_grade_id');
            $this->db->join('users U','U.user_id=TG.teacher_id');
            $this->db->where('SG.student_id', $student_id);
            $this->db->where('SG.is_deleted', 0);
            $this->db->order_by('SG.grade','ASC');
            $query = $this->db->get();
            return $query->result_array();
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }
    public function getSchoolVideo($school_id){
        try{
            $this->db->select('S.*,U.display_name,U.first_name,U.last_name,U.profile_image,SC.name,SC.address');
            $this->db->from('school_introductory_video S');
            $this->db->join('users U','U.user_id=S.user_id');
            $this->db->join('school SC','U.school_id=SC.id');
            $this->db->where('S.status',1);
            $this->db->where('S.school_id',$school_id);
            $this->db->where('S.is_deleted', 0);
            $this->db->where('U.is_deleted', 0);
            $query = $this->db->get();
            return $query->result_array();
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function getGradeDetails($student_id){
        $this->db->select('DISTINCT(SG.grade)');
        $this->db->from('student_grade SG');
        $this->db->where('SG.student_id',$student_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTeacherDetails($teacher_ids){
        foreach ($teacher_ids as $key => $value) {
          $teacher_id[] = $value['teacher_grade_id'];
        }
        $this->db->select('DISTINCT(TG.teacher_id),U.first_name,U.last_name');
        $this->db->from('teacher_grade TG');
        $this->db->join('users U','TG.teacher_id=U.user_id');
        $this->db->where_in('TG.id',$teacher_id);
        $this->db->where('U.is_deleted', 0);
        $query = $this->db->get();
       return $query->result_array();
       
    }

    public function getAllVideos($user_id,$user_type="",$school_id="",$teacher_id="",$grade="")
    {
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=APP_PAGE_LIMIT;
        }
        if(isset($user_type) && !empty($user_type)){
            if($user_type==1){
                $district_id = $this->Common_model->getFieldValue('users','district_id',array("user_id"=>$user_id));
                $this->db->where('m.district_id', $district_id);
            }
        }
        if(isset($school_id) && !empty($school_id)){
            $this->db->where('m.school_id', $school_id);
        }
        if(isset($teacher_id) && !empty($teacher_id)){
            $this->db->where('m.user_id', $teacher_id);
            //$this->db->where_IN('m.user_id', $teacher_id,False);

        }
        if(isset($grade)){
            $this->db->where('m.grade', $grade);
            //$this->db->where_IN('m.grade', $grade,False);
        }

        $offset=$limit*$page;
        $this->db->select('m.id as media_id,m.school_id,m.grade,m.grade_display_name,m.user_id,m.grade,m.grade_display_name,m.media_type,m.format,m.title,m.media_slug,m.description,m.thumb_image,m.large_image,m.media_name,m.duration,m.created,u.display_name,u.profile_image,u.first_name,u.last_name');
        $this->db->from('media m');
        $this->db->join('users u','u.user_id = m.user_id');
        $this->db->where('m.status', 1);
        $this->db->where('m.media_type', 1);
        if(empty($user_type)){
            $this->db->where('m.user_id', $teacher_id);
        }
        $this->db->where('m.created_as', 1);
        $this->db->where('m.is_deleted', 0);
        $this->db->where('u.is_deleted', 0);
        $this->db->order_by('m.id','DESC');
        $this->db->limit($limit,$offset);
        $query = $this->db->get();
        //echo $this->db->last_query();die;
        return $query->result_array();
        
    }

    public function getTeacherName($teacher_grade_ids){ 
        $this->db->select("if((U.display_name IS NULL OR U.display_name = ''), concat(U.first_name, U.last_name),`U`.`display_name`) as name");
        $this->db->from('teacher_grade TG');
        $this->db->join('users U','TG.teacher_id=U.user_id');
        $this->db->where('U.is_deleted', 0);
        $this->db->where('TG.id',$teacher_grade_ids);
        $query = $this->db->get();
       return $query->row_array();
    }
    public function CheckstudentGrade($parent_id,$name,$teacher_grade_id){ 
        $this->db->select('S.id');
        $this->db->from('student S');
        $this->db->join('student_grade SG','SG.student_id=S.id');
        $this->db->where('S.parent_id',$parent_id);
        $this->db->where('S.student_name',$name);
        $this->db->where('S.is_deleted',0);
        $this->db->where('SG.is_deleted',0);
        // $this->db->where('SG.is_approve!=',2);
        $this->db->where('SG.teacher_grade_id',$teacher_grade_id);
        $query = $this->db->get();
       return $query->row_array();
    }

    public function getStudentRecords($user_id){
        $this->db->select("S.id,SG.teacher_grade_id,SG.is_approve,if((U.display_name IS NULL OR U.display_name = ''), concat(U.first_name, U.last_name),`U`.`display_name`) as name");
        $this->db->from('student S');
        $this->db->join('student_grade SG','SG.student_id = S.id','left');
        $this->db->join('teacher_grade TG','TG.id = SG.teacher_grade_id','left');
        $this->db->join('users U','U.user_id=TG.teacher_id','left');
        $this->db->where('S.parent_id', $user_id);
        $this->db->where('S.is_deleted', 0);
        $this->db->where('U.is_deleted', 0);
        $query = $this->db->get();
        return $query->result_array();
    }    
    
}