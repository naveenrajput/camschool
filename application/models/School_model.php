<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class School_model extends CI_Model {

    public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();
    }


    public function getTeacherList($school_id,$approve){
        try{

            $page=trim($this->input->post('page'));
            $limit=trim($this->input->post('limit'));
            if(!$page){
                $page=0;
            }
            if(!$limit){
                $limit=APP_PAGE_LIMIT;
            }

            $offset=$limit*$page;
            if($approve==1){
                $sel="U.user_id as teacher_id,U.first_name, U.last_name, GROUP_CONCAT(TG.grade_display_name ORDER BY TG.grade ASC SEPARATOR ', ') as grade";
            }else{
                $sel="U.user_id as teacher_id,U.first_name, U.last_name, GROUP_CONCAT(TG.grade_display_name ORDER BY TG.grade ASC SEPARATOR ', ') as grade,if((U.display_name IS NULL OR U.display_name = ''), concat(U.first_name,' ',U.last_name),U.display_name) as display_name,U.is_approve,U.phone,U.email";
            }

            $this->db->select($sel);
            $this->db->from('users U');
            $this->db->join('teacher_grade TG','TG.teacher_id= U.user_id');
           
            if($approve==1){
                $this->db->where('U.is_approve', $approve);
            }else{
                $this->db->group_start();
                $this->db->where('U.is_approve', 2);
                $this->db->or_where('U.is_approve', 3);
                $this->db->group_end();
            }
            $this->db->where('U.school_id', $school_id);
            $this->db->where('U.user_type', 3);
            $this->db->where('U.is_deleted', 0);
            $this->db->where('U.email_verified', 1);
            $this->db->order_by('U.created','DESC');
            $this->db->group_by('TG.teacher_id');
            // $this->db->limit($limit,$offset);
            $query = $this->db->get();
            //echo $this->db->last_query();exit;
            return $query->result_array();
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function getTeacherListForFilter($school_id){
        try{

            // $sel="U.user_id as teacher_id,if((U.display_name IS NULL OR U.display_name = ''), concat(U.first_name, U.last_name),`U`.`display_name`) as name";
            $sel="U.user_id as teacher_id,U.display_name, concat(U.first_name,' ', U.last_name) as name";
            $this->db->select($sel);
            $this->db->from('users U');
            $this->db->where('U.is_approve', 3);
            $this->db->where('U.school_id', $school_id);
            $this->db->where('U.user_type', 3);
            $this->db->where('U.is_deleted', 0);
            $this->db->where('U.email_verified', 1);
            $this->db->order_by('U.created','DESC');
            $query = $this->db->get();
            return $query->result_array();
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }


     public function getTeacherListWeb($school_id,$approve,$limit,$page){
        try{
           
            if(!$page){
                $page=0;
            }
            if($limit==0){
                $limit=0;
            }else{
                $limit=APP_PAGE_LIMIT;
            }
            $offset=$page;
            if($approve==1){
                $sel="U.user_id as teacher_id,U.first_name, U.last_name, GROUP_CONCAT(TG.grade_display_name ORDER BY TG.grade ASC SEPARATOR ', ') as grade";
            }else{
                $sel="U.user_id as teacher_id,U.first_name, U.last_name, GROUP_CONCAT(TG.grade_display_name ORDER BY TG.grade ASC SEPARATOR ', ') as grade,U.display_name,U.is_approve,U.phone,U.email";
            }
            $this->db->select($sel);
            $this->db->from('users U');
            $this->db->join('teacher_grade TG','TG.teacher_id= U.user_id');
           
            if($approve==1){
                $this->db->where('U.is_approve', $approve);
            }else{
                $this->db->group_start();
                $this->db->where('U.is_approve', 2);
                $this->db->or_where('U.is_approve', 3);
                $this->db->group_end();
            }
            $this->db->where('U.school_id', $school_id);
            $this->db->where('U.user_type', 3);
            $this->db->where('U.is_deleted', 0);
            $this->db->where('U.email_verified', 1);
            $this->db->order_by('U.created','DESC');
            $this->db->group_by('TG.teacher_id');

            if($limit==0 && $offset==0){
            $query = $this->db->get();
            // echo $this->db->last_query();exit;
            return $query->num_rows(); 
            }else{
            $this->db->limit($limit,$offset);
            $query = $this->db->get(); 
            //echo $this->db->last_query();
            return $query->result_array();
            }
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }
    public function getTeacherListForMail($school_id,$approve){
        try{

            if($approve==1){
                $sel="U.user_id as teacher_id,U.first_name, U.last_name, GROUP_CONCAT(TG.grade_display_name ORDER BY TG.grade ASC) as grade";
            }else{
                $sel="U.user_id as teacher_id,U.first_name, U.last_name, GROUP_CONCAT(TG.grade_display_name ORDER BY TG.grade ASC) as grade,U.display_name,U.is_approve,U.phone,U.email";
            }
            $this->db->select($sel);
            $this->db->from('users U');
            $this->db->join('teacher_grade TG','TG.teacher_id= U.user_id');

            if(!isset($_SESSION['broadcast_email'])){    
                if($approve==1){
                    $this->db->where('U.is_approve', $approve);
                }else{
                    // $this->db->group_start();
                    // $this->db->where('U.is_approve', 2);
                    // $this->db->or_where('U.is_approve', 3);
                    // $this->db->group_end();
                    $this->db->where('U.is_approve', 3);
                }
            }
            $this->db->where('U.school_id', $school_id);
            $this->db->where('U.user_type', 3);
            $this->db->where('U.is_deleted', 0);
            $this->db->order_by('U.created','DESC');
            $this->db->group_by('TG.teacher_id');
           
            $query = $this->db->get();
            //echo $this->db->last_query();exit;
            return $query->result_array();
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }


    public function get_broadcast_email($broadcast_email){
        $this->db->select('first_name as name,email,user_id as id');
        $this->db->from('users');
        $this->db->where_in('user_id',$broadcast_email);
        $query = $this->db->get(); 
            //echo $this->db->last_query();
        return $result =  $query->result_array();
        
    }


}