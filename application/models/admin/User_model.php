<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

    public function __construct()
    {
        parent:: __construct();
    }

    public function get_user_list($limit,$offset,$user_type,$is_userlist="",$all="") {
        if($this->input->get('first_name')){
            $first_name=$this->input->get('first_name');
            $this->db->like("users.first_name", trim($first_name));
        }
        if($this->input->get('last_name')){
            $last_name=$this->input->get('last_name');
            $this->db->like("users.last_name", trim($last_name));
        }
        if($this->input->get('email')){
            $email=$this->input->get('email');
            $this->db->like("users.email", trim($email));
        }
        if($this->input->get('phone')){
            $phone=$this->input->get('phone');
            $this->db->like("users.phone", trim($phone));
        }
        if($this->input->get('is_approve')){
            $is_approve=$this->input->get('is_approve');
            $this->db->where("users.is_approve", trim($is_approve));
        }
        if($this->input->get('status')){
            $status=$this->input->get('status');
            if($status==3){
                $this->db->where("users.status", trim($status));
                $this->db->where("users.email_verified", 1);
            }else{
                $this->db->where("users.status", trim($status));
            }
           
            
        }
        if($this->input->get('prospect_status')){
            $prospect_status=$this->input->get('prospect_status');
            $this->db->where("users.prospect_status", trim($prospect_status));
        }
        if($this->input->get('status')){
          /*  $status=$this->input->get('status');
            $this->db->where("users.status", trim($status));*/
            $status=$this->input->get('status');
            if($status==3){
              $this->db->group_start();
              $this->db->where("users.status", trim($status));
              $this->db->or_where("users.email_verified", 2);
              $this->db->group_end();
            }else if($status==1){
              $this->db->group_start();
              $this->db->where("users.status", trim($status));
              $this->db->where("users.email_verified", 1);
              $this->db->group_end();
            }else{
              $this->db->where("users.status", trim($status));
            }
           
        }
        if($this->input->get('state')){
            $state=$this->input->get('state');
            $this->db->where("S.state_id", trim($state));
        }
        if($this->input->get('district_name')){
            $district_name=$this->input->get('district_name');
            $this->db->like("D.name", trim($district_name));
        }
        if($is_userlist==1){
            $this->db->where("users.prospect_status", 2);
        }else{
            $this->db->where("users.is_approve", 1);
        }
        
        $this->db->select('users.*,S.state_name,D.name as district_name,D.ask_question');
        $this->db->from('users');
        $this->db->join('states S','users.state_id=S.state_id');
        $this->db->join('district D','users.district_id=D.id');
        $this->db->where('users.user_type',$user_type);
        $this->db->where('users.is_deleted','0');
        $this->db->order_by('user_id','Desc');
        
        // $this->db->limit($limit, $offset);
        // $query=$this->db->get();

        // if($limit==0 && $offset==0){ 
        //     return $query->num_rows();
        // } else {
        //     if ($query->num_rows() > 0) {
        //         return $query->result_array();
        //     } else return false;
        // }

       if ($all!='') {
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        } else {
            if ($limit==0 && $offset==0) {
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    return $query->num_rows();
                } else return false;
            }else{
                $this->db->limit($limit,$offset);
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    return $query->result_array();
                } else return false;
            }
        }




    }

    public function get_user_detail($id) {
       
        $this->db->select('users.*,S.state_name,D.name as district_name,D.ask_question');
        $this->db->from('users');
        $this->db->join('states S','users.state_id=S.state_id');
        $this->db->join('district D','users.district_id=D.id');
        $this->db->where('users.is_deleted','0');
        $this->db->where('users.user_id',$id);
        $query=$this->db->get();
        return $query->row_array();
    }


    public function getPrincipalList($limit,$offset,$user_type,$all='') {
        if($this->input->get('first_name')){
            $first_name=$this->input->get('first_name');
            $this->db->like("users.first_name", trim($first_name));
        }
        if($this->input->get('last_name')){
            $last_name=$this->input->get('last_name');
            $this->db->like("users.last_name", trim($last_name));
        }
        if($this->input->get('email')){
            $email=$this->input->get('email');
            $this->db->like("users.email", trim($email));
        }
        if($this->input->get('phone')){
            $phone=$this->input->get('phone');
            $this->db->like("users.phone", trim($phone));
        }
        if($this->input->get('status')){
          /*  $status=$this->input->get('status');
            $this->db->where("users.status", trim($status));*/
            $status=$this->input->get('status');
            if($status==3){
              $this->db->group_start();
              $this->db->where("users.status", trim($status));
              $this->db->or_where("users.email_verified", 2);
              $this->db->group_end();
            }else if($status==1){
              $this->db->group_start();
              $this->db->where("users.status", trim($status));
              $this->db->where("users.email_verified", 1);
              $this->db->group_end();
            }else{
              $this->db->where("users.status", trim($status));
            }
           
        }
      
        if($this->input->get('state')){
            $state=$this->input->get('state');
            $this->db->where("S.state_id", trim($state));
        }if($this->input->get('district_name')){
            $district_name=$this->input->get('district_name');
            $this->db->like("D.name", trim($district_name));
        }
        if($this->input->get('school_name')){
            $school_name=$this->input->get('school_name');
            $this->db->like("SC.name", trim($school_name));
        }
       
        $this->db->select('users.*,S.state_name,D.name as district_name,D.ask_question,SC.name as school_name');
        $this->db->from('users');
        $this->db->join('states S','users.state_id=S.state_id');
        $this->db->join('district D','users.district_id=D.id');
        $this->db->join('school SC','users.school_id=SC.id');
        $this->db->where('users.user_type',$user_type);
        $this->db->where('users.is_deleted','0');
        $this->db->order_by('user_id','Desc');
       
        /*$this->db->limit($limit, $offset);
        $query=$this->db->get();

        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }*/

        if ($all!='') {
            $query = $this->db->get();

            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        } else {
            if ($limit==0 && $offset==0) {
                $query = $this->db->get();
                // echo $this->db->last_query();die;
                if ($query->num_rows() > 0) {
                    return $query->num_rows();
                } else return false;
            }else{
                $this->db->limit($limit,$offset);
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    return $query->result_array();
                } else return false;
            }
        }        
    }

    public function getPrincipalDetail($id) {
        $this->db->select('users.*,S.state_name,D.name as district_name,SC.name as school_name');
        $this->db->from('users');
        $this->db->join('states S','users.state_id=S.state_id');
        $this->db->join('district D','users.district_id=D.id');
        $this->db->join('school SC','users.school_id=SC.id');
        $this->db->where('users.is_deleted','0');
        $this->db->where('users.user_id',$id);
        $query=$this->db->get();
        return $query->row_array();
    }



    public function getTeacherList($limit,$offset,$user_type,$all='') {
        if($this->input->get('first_name')){
            $first_name=$this->input->get('first_name');
            $this->db->like("users.first_name", trim($first_name));
        }
        if($this->input->get('last_name')){
            $last_name=$this->input->get('last_name');
            $this->db->like("users.last_name", trim($last_name));
        }
        if($this->input->get('email')){
            $email=$this->input->get('email');
            $this->db->like("users.email", trim($email));
        }
        if($this->input->get('phone')){
            $phone=$this->input->get('phone');
            $this->db->like("users.phone", trim($phone));
        }
        if($this->input->get('status')){
            $status=$this->input->get('status');
            $this->db->where("users.status", trim($status));
        }
       if($this->input->get('status')){
          /*  $status=$this->input->get('status');
            $this->db->where("users.status", trim($status));*/
            $status=$this->input->get('status');
            if($status==3){
              $this->db->group_start();
              $this->db->where("users.status", trim($status));
              $this->db->or_where("users.email_verified", 2);
              $this->db->group_end();
            }else if($status==1){
              $this->db->group_start();
              $this->db->where("users.status", trim($status));
              $this->db->where("users.email_verified", 1);
              $this->db->group_end();
            }else{
              $this->db->where("users.status", trim($status));
            }
           
        }
        if($this->input->get('state')){
            $state=$this->input->get('state');
            $this->db->where("S.state_id", trim($state));
        }        
        if($this->input->get('district_name')){
            $district_name=$this->input->get('district_name');
            $this->db->like("D.name", trim($district_name));
        }
        if($this->input->get('school_name')){
            $school_name=$this->input->get('school_name');
            $this->db->like("SC.name", trim($school_name));
        }        
        
        $this->db->select('users.*,S.state_name,D.name as district_name,SC.name as school_name');
        $this->db->from('users');
        $this->db->join('states S','users.state_id=S.state_id');
        $this->db->join('district D','users.district_id=D.id');
        $this->db->join('school SC','users.school_id=SC.id');
        $this->db->where('users.user_type',$user_type);
        $this->db->where('users.is_deleted','0');
        $this->db->order_by('user_id','Desc');
        /*$this->db->limit($limit, $offset);
        $query=$this->db->get();

        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }*/
        if ($all!='') {
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        } else {
            if ($limit==0 && $offset==0) {
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    return $query->num_rows();
                } else return false;
            }else{
                $this->db->limit($limit,$offset);
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    return $query->result_array();
                } else return false;
            }
        }         
    }

    public function getTeacherDetail($id) {
        $this->db->select('users.*,S.state_name,D.name as district_name,SC.name as school_name');
        $this->db->from('users');
        $this->db->join('states S','users.state_id=S.state_id');
        $this->db->join('district D','users.district_id=D.id');
        $this->db->join('school SC','users.school_id=SC.id');        
        $this->db->where('users.is_deleted','0');
        $this->db->where('users.user_id',$id);
        $query=$this->db->get();
        return $query->row_array();
    }



    public function getParentList($limit,$offset,$user_type,$all='') {
        if($this->input->get('first_name')){
            $first_name=$this->input->get('first_name');
            $this->db->like("users.first_name", trim($first_name));
        }
        if($this->input->get('last_name')){
            $last_name=$this->input->get('last_name');
            $this->db->like("users.last_name", trim($last_name));
        }
        if($this->input->get('email')){
            $email=$this->input->get('email');
            $this->db->like("users.email", trim($email));
        }
        if($this->input->get('phone')){
            $phone=$this->input->get('phone');
            $this->db->like("users.phone", trim($phone));
        }
        if($this->input->get('status')){
          /*  $status=$this->input->get('status');
            $this->db->where("users.status", trim($status));*/
            $status=$this->input->get('status');
            if($status==3){
              $this->db->group_start();
              $this->db->where("users.status", trim($status));
              $this->db->or_where("users.email_verified", 2);
              $this->db->group_end();
            }else if($status==1){
              $this->db->group_start();
              $this->db->where("users.status", trim($status));
              $this->db->where("users.email_verified", 1);
              $this->db->group_end();
            }else{
              $this->db->where("users.status", trim($status));
            }
           
        }
        if($this->input->get('state')){
            $state=$this->input->get('state');
            $this->db->where("S.state_id", trim($state));
        }if($this->input->get('district_name')){
            $district_name=$this->input->get('district_name');
            $this->db->like("D.name", trim($district_name));
        }
        if($this->input->get('school_name')){
            $school_name=$this->input->get('school_name');
            $this->db->like("SC.name", trim($school_name));
        }        
        
        $this->db->select('users.*,S.state_name,D.name as district_name,SC.name as school_name');
        $this->db->from('users');
        $this->db->join('states S','users.state_id=S.state_id');
        $this->db->join('district D','users.district_id=D.id');
        $this->db->join('school SC','users.school_id=SC.id');        
        $this->db->where('users.user_type',$user_type);
        $this->db->where('users.is_deleted','0');
        $this->db->order_by('user_id','Desc');
        /*$this->db->limit($limit, $offset);
        $query=$this->db->get();

        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }*/
        if ($all!='') {
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        } else {
            if ($limit==0 && $offset==0) {
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    return $query->num_rows();
                } else return false;
            }else{
                $this->db->limit($limit,$offset);
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    return $query->result_array();
                } else return false;
            }
        }         
    }

    public function getParentDetail($id) {
        $this->db->select('users.*,S.state_name,D.name as district_name,SC.name as school_name');
        $this->db->from('users');
        $this->db->join('states S','users.state_id=S.state_id');
        $this->db->join('district D','users.district_id=D.id');
        $this->db->join('school SC','users.school_id=SC.id');        
        $this->db->where('users.is_deleted','0');
        $this->db->where('users.user_id',$id);
        $query=$this->db->get();
        return $query->row_array();
    }


    public function getStudentList($limit,$offset,$parentId,$all='') {
        if($this->input->get('student_name')){
            $student_name=$this->input->get('student_name');
            $this->db->like("st.student_name", trim($student_name));
        }
        if($this->input->get('first_name')){
            $first_name=$this->input->get('first_name');
            $this->db->like("users.first_name", trim($first_name));
        }
        if($this->input->get('last_name')){
            $last_name=$this->input->get('last_name');
            $this->db->like("users.last_name", trim($last_name));
        }
        if($this->input->get('email')){
            $email=$this->input->get('email');
            $this->db->like("users.email", trim($email));
        }
        if($this->input->get('status')){
            $status=$this->input->get('status');
            $this->db->where("st.status", trim($status));
        }
        if($this->input->get('state')){
            $state=$this->input->get('state');
            $this->db->where("S.state_id", trim($state));
        }if($this->input->get('district_name')){
            $district_name=$this->input->get('district_name');
            $this->db->like("D.name", trim($district_name));
        }
        if($this->input->get('school_name')){
            $school_name=$this->input->get('school_name');
            $this->db->like("SC.name", trim($school_name));
        }        
        
        $this->db->select('st.id,st.student_name,st.status,users.first_name,users.last_name,users.email,users.phone,users.email_verified,S.state_name,D.name as district_name,SC.name as school_name');
        $this->db->from('student st');
        $this->db->join('users','users.user_id=st.parent_id','left');
        $this->db->join('states S','users.state_id=S.state_id');
        $this->db->join('district D','users.district_id=D.id');
        $this->db->join('school SC','users.school_id=SC.id');        
        $this->db->where('st.parent_id',$parentId);
        $this->db->where('st.is_deleted','0');
        $this->db->order_by('st.id','Desc');
        /*$this->db->limit($limit, $offset);
        $query=$this->db->get();

        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }*/
        if ($all!='') {
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        } else {
            if ($limit==0 && $offset==0) {
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    return $query->num_rows();
                } else return false;
            }else{
                $this->db->limit($limit,$offset);
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    return $query->result_array();
                } else return false;
            }
        }         
    }


    public function broadcastList($limit,$offset) {
        if($this->input->get('subject')){
            $subject=$this->input->get('subject');
            $this->db->like("bn.subject", trim($subject));
        }
        if($this->input->get('user_name')){
            $user_name=trim($this->input->get('user_name'));
            $this->db->like("admin.username", trim($user_name));
        }
        if($this->input->get('user_type')){

            $user_type=$this->input->get('user_type');
            $this->db->like("bn.user_type", trim($user_type));
        }
        
        $this->db->select('bn.notification_id,bn.subject,bn.message,bn.user_type,bn.sent_by,bn.created,admin.username');
        $this->db->from('broadcast_notification bn');
        $this->db->join("admin","admin.admin_id=bn.created_by","left");
        $this->db->where('bn.sent_by',1);
        $this->db->where('bn.is_deleted',0);
        $this->db->order_by('bn.notification_id','desc');

        if ($limit==0 && $offset==0) {
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            } else return false;
        }else{
            $this->db->limit($limit,$offset);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }


    public function getBroadcastUserList($limit,$offset,$notyID) {
        if($this->input->get('email')){
            $email=$this->input->get('email');
            $this->db->like("email", trim($email));
        }
        
        $this->db->select('id,notification_id,email,user_id,created');
        $this->db->from('broadcast_emails');
        $this->db->where('notification_id',$notyID);
        $this->db->order_by('id','desc');

        if ($limit==0 && $offset==0) {
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            } else return false;
        }else{
            $this->db->limit($limit,$offset);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }









}//model end

?>