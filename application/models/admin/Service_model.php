<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service_model extends CI_Model {

    public function __construct()
    {
        parent:: __construct();
    }

    public function get_service_list($limit,$offset) {
        if($this->input->get('title')){
            $title=$this->input->get('title');
            $this->db->like("title", trim($title));
        }
         if($this->input->get('status')){
            $status=$this->input->get('status');
            $this->db->where('status',$status);
        }
        $this->db->select('*');
        $this->db->from('service_category');
        $this->db->where('is_deleted','0');
        $this->db->order_by('id','Desc');
        $this->db->limit($limit, $offset);
        $query=$this->db->get();

        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }
    public function get_measurement_unit_list($limit,$offset) {
        if($this->input->get('title')){
            $title=$this->input->get('title');
            $this->db->like("title", trim($title));
        }
         if($this->input->get('status')){
            $status=$this->input->get('status');
            $this->db->where('status',$status);
        }
        $this->db->select('*');
        $this->db->from('measurement_unit');
        $this->db->where('is_deleted','0');
        $this->db->order_by('id','Desc');
        $this->db->limit($limit, $offset);
        $query=$this->db->get();

        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }


      public function getUserService($offset=0,$limit=0) 
    {
        $where="services.id !=0 and services.is_deleted=0";
        if($this->input->get('username')){
            $username=$this->input->get('username');
            $where.=" and users.fullname LIKE '%".$this->db->escape_like_str(trim($username))."%' ";
        }  
        if($this->input->get('user_id')){
            $user_id=$this->input->get('user_id');
            $where.=" and services.user_id = '".$this->db->escape_like_str(trim($user_id))."' ";
        }
        if($this->input->get('category_title')){
            $category_title=$this->input->get('category_title'); 
             $where.=" and service_category.title LIKE '%".$this->db->escape_like_str(trim($category_title))."%' ";
        } 
         
        if($this->input->get('services_title')){
            $services_title=$this->input->get('services_title'); 
            $where.=" and services.service_title LIKE '%".$this->db->escape_like_str(trim($services_title))."%' ";
        } 
          
        
        if($this->input->get('status')){
            $status=$this->input->get('status');
           $where.=" and services.status='".$this->db->escape_like_str(trim($status))."'";
        }


        $this->db->select("services.*,users.fullname,service_category.title");
        $this->db->from('services');
        $this->db->join('users','users.user_id=services.user_id'); 
        $this->db->join('service_category','service_category.id=services.category_id');  
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        }  
        $this->db->order_by('services.id','DESC');
        if ($limit!=0) {
            $this->db->limit($limit,$offset);
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }else{
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            } else return false;
        }        
       
    }   


    public function getUserServicedetails($category_id) 
    {
        $where="services.id !=0 and services.is_deleted=0 and services.id=".$category_id; 

        $this->db->select("services.*,users.fullname,service_category.title");
        $this->db->from('services');
        $this->db->join('users','users.user_id=services.user_id'); 
        $this->db->join('service_category','service_category.id=services.category_id');  
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        }  
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;       
       
    }     

    public function getCategoryItem($category_id) 
    {
        $where=" services_item.service_id=".$category_id; 

        $this->db->select("services_item.*,measurement_unit.title as measurement_title");
        $this->db->from('services_item');
        $this->db->join('measurement_unit','measurement_unit.id=services_item.unit');  
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        }   
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
       
    }

    public function getServiceOffer($id,$offset=0,$limit=0) 
    {
        $where="offer.id !=0 and offer.is_deleted=0 and offer.service_id=".$id;
     
        if($this->input->get('offer_name')){
            $offer_name=$this->input->get('offer_name');
            $where.=" and offer.offer_name LIKE '%".$this->db->escape_like_str(trim($offer_name))."%' ";
        }   
        
        if($this->input->get('date_rang')){
            $date_rang=$this->input->get('date_rang');
            $split_date=explode(" - ",$date_rang);
            $start_date= date('Y-m-d',strtotime($split_date[0]));
            $end_date=date('Y-m-d',strtotime($split_date[1])); 
            $where.=" and date(offer.start_date) >='".$start_date."' and date(offer.end_date) <='".$end_date."' ";
        } 

        
        if($this->input->get('status')){
            $status=$this->input->get('status');
           $where.=" and offer.status='".$this->db->escape_like_str(trim($status))."'";
        } 
        $this->db->select("offer.*");
        $this->db->from('offer');
      
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        } 
        $this->db->order_by('offer.id','DESC');
        if ($limit!=0) {
            $this->db->limit($limit,$offset);
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }else{
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            } else return false;
        }        
       
    }    
    
}//model end

?>