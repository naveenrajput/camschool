<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Other_model extends CI_Model {

	public function __construct()
	{
		parent:: __construct();
	}


    public function get_log_list_bkp($limit,$offset=0) {

        $where="action_id !=0";
        if($this->input->get('admin_name')){
            $admin_name=$this->input->get('admin_name');
            $where.=" and admin.username LIKE '%".$this->db->escape_like_str(trim($admin_name))."%' ";
        } 
        if($this->input->get('username')){
            $username=$this->input->get('username');
            $where.=" and users.username LIKE '%".$this->db->escape_like_str(trim($username))."%' ";
        } 
        if($this->input->get('action_date')){
            $action_date=$this->input->get('action_date');
            $split_date=explode(" - ",$action_date);
            $start_date= date('Y-m-d',strtotime($split_date[0]));
            $end_date=date('Y-m-d',strtotime($split_date[1]));
            $where.=" and date(al.created) >='".$start_date."' and date(al.created) <='".$end_date."' ";
        } 

        $this->db->select('al.*');
        $this->db->from('action_log al');
        $this->db->join('users','users.user_id=al.performer_id AND perform_by="User"','left');
        $this->db->join('admin','admin.admin_id=al.performer_id AND perform_by="Admin"','left');        
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        }
        $this->db->limit($limit,$offset);
        $this->db->order_by('action_id','desc');
        $query= $this->db->get();
        
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }     
    public function get_log_list($limit,$offset=0) {
            
        $timezone = $this->session->userdata['admin_timezone'];

        $where="action_id !=0";
        if($this->input->get('admin_name')){
            $admin_name=$this->input->get('admin_name');
            $where.=" and admin.username LIKE '%".$this->db->escape_like_str(trim($admin_name))."%' ";
        } 
        if($this->input->get('username')){
            $username=$this->input->get('username');
            $where.=" and users.staging_id LIKE '%".$this->db->escape_like_str(trim($username))."%' ";
        } 
        if($this->input->get('action_date')){
            $action_date=$this->input->get('action_date');
            $split_date=explode(" - ",$action_date);

            $start_date= date('Y-m-d',strtotime($split_date[0]));
            $end_date=date('Y-m-d',strtotime($split_date[1]));

            // $where.=" and date(al.created) >='".$start_date."' and date(al.created) <='".$end_date."' ";
            $where.=" and date(CONVERT_TZ(al.created,'UTC','".$timezone."')) >='".$start_date."' and date(CONVERT_TZ(al.created,'UTC','".$timezone."')) <='".$end_date."' ";
        } 

        if($this->input->get('keyword')){
            $keyword=$this->input->get('keyword');
            $where.=" and al.description LIKE '%".$this->db->escape_like_str(trim($keyword))."%' ";
        } 

        $this->db->select('al.*');
        $this->db->from('action_log al');
        $this->db->join('users','users.user_id=al.performer_id AND perform_by="User"','left');
        $this->db->join('admin','admin.admin_id=al.performer_id AND perform_by="Admin"','left');        
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        }
        $this->db->order_by('action_id','desc');
        
        if($limit==0 && $offset==0){ 
            $query= $this->db->get();
            return $query->num_rows();
        } else {
            $this->db->limit($limit,$offset);
            $query= $this->db->get();
            // echo $this->db->last_query();exit();
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }     

    public function getOffers($offset=0,$limit=0) 
    {
       $where="offer_id !=0 and is_deleted=0";
       if($this->input->get('title')){
            $title=$this->input->get('title');
            $where.=" and title LIKE '%".$this->db->escape_like_str(trim($title))."%' ";
        }
        if($this->input->get('code')){
            $code=$this->input->get('code');
            $where.=" and code LIKE '%".$this->db->escape_like_str(trim($code))."%' ";
        }
        if($this->input->get('type')){
            $type=$this->input->get('type');
            $where.=" and type LIKE '%".$this->db->escape_like_str(trim($type))."%' ";
        }

        if($this->input->get('date_rang')){
            $date_rang=$this->input->get('date_rang');
            $split_date=explode(" - ",$date_rang);
            $start_date= date('Y-m-d',strtotime($split_date[0]));
            $end_date=date('Y-m-d',strtotime($split_date[1]));
            $where.=" and start_date >='".$start_date.' 00:00:00'."' and end_date <='".$end_date.' 00:00:00'."' ";
        } 
        
        if($this->input->get('status')){
            $status=$this->input->get('status');
           $where.=" and status='".$this->db->escape_like_str(trim($status))."'";
        }


        $this->db->select('*');
        $this->db->from('offers');
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        }

        $this->db->order_by('offer_id','DESC');
        if ($limit!=0) {
            $this->db->limit($limit,$offset);
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }else{
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            } else return false;
        }        
       
    }       


  

    public function getReviews($offset=0,$limit=0) 
    {
        $where="rv.id !=0 and rv.is_deleted=0 and events.is_deleted=0";
        if($this->input->get('event_number')){
            $event_number=$this->input->get('event_number');
            $where.=" and events.event_number LIKE '%".$this->db->escape_like_str(trim($event_number))."%' ";
        }  
        if($this->input->get('event_title')){
            $event_title=$this->input->get('event_title');
            $where.=" and events.event_title LIKE '%".$this->db->escape_like_str(trim($event_title))."%' ";
        }
        if($this->input->get('sender_name')){
            $sender_name=$this->input->get('sender_name');
            $where.=" and CONCAT(sender.fullname) LIKE '%".$this->db->escape_like_str(trim($sender_name))."%' ";
        }
        if($this->input->get('receiver_name')){
            $receiver_name=$this->input->get('receiver_name');
            $where.=" and CONCAT(receiver.fullname) LIKE '%".$this->db->escape_like_str(trim($receiver_name))."%' ";
        } 
        if($this->input->get('sender_id')){
            $sender_id=$this->input->get('sender_id');
            $where.=" and sender.user_id = '".$this->db->escape_like_str(trim($sender_id))."' ";
        }
        if($this->input->get('receiver_id')){
            $receiver_id=$this->input->get('receiver_id');
            $where.=" and receiver.user_id = '".$this->db->escape_like_str(trim($receiver_id))."' ";
        }
        if($this->input->get('rating')){
            $rating=$this->input->get('rating');
            $where.=" and rv.rating = '".$this->db->escape_like_str(trim($rating))."' ";
        }
        
        if($this->input->get('date_rang')){
            $date_rang=$this->input->get('date_rang');
            $split_date=explode(" - ",$date_rang);
            $start_date= date('Y-m-d',strtotime($split_date[0]));
            $end_date=date('Y-m-d',strtotime($split_date[1]));
            // $where.=" and rv.created >='".$start_date.' 00:00:00'."' and rv.created <='".$end_date.' 00:00:00'."' ";
            $where.=" and date(rv.created) >='".$start_date."' and date(rv.created) <='".$end_date."' ";
        } 

        
        if($this->input->get('status')){
            $status=$this->input->get('status');
           $where.=" and rv.status='".$this->db->escape_like_str(trim($status))."'";
        }


        $this->db->select("events.event_number,rv.id,rv.rating,rv.review,rv.status,rv.is_deleted,rv.created,concat(sender.fullname) as sender_name,concat(receiver.fullname) as receiver_name,events.event_title,events.id as event_id");
        $this->db->from('reviews rv');
        $this->db->join('users sender','sender.user_id=rv.sender_id');
        $this->db->join('users receiver','receiver.user_id=rv.receiver_id');
        $this->db->join('events','events.id=rv.event_id');
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        }

        $this->db->order_by('rv.id','DESC');
        if ($limit!=0) {
            $this->db->limit($limit,$offset);
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }else{
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            } else return false;
        }        
    }       



    public function getReviewsdetail($review_id) 
    {
        $where="rv.id !=0 and rv.is_deleted=0 and events.is_deleted=0 and rv.id=".$review_id; 
        $this->db->select("rv.id,rv.rating,rv.review,rv.status,rv.is_deleted,rv.created,concat(sender.fullname) as sender_name,concat(receiver.fullname) as receiver_name,events.event_title,events.id as event_id");
        $this->db->from('reviews rv');
        $this->db->join('users sender','sender.user_id=rv.sender_id');
        $this->db->join('users receiver','receiver.user_id=rv.receiver_id');
        $this->db->join('events','events.id=rv.event_id');
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        } 
       
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
               
    }  

/*    public function getReviewsDetail($id) 
    {
        $this->db->select("rv.id,rv.private_review,rv.rating,rv.review,rv.is_private,rv.status,rv.is_deleted,rv.created,concat(sender.first_name,' ',sender.last_name) as sender_name,concat(receiver.first_name,' ',receiver.last_name) as receiver_name,job.job_title,job.id as job_id");
        $this->db->from('reviews rv');
        $this->db->join('users sender','sender.user_id=rv.sender_id');
        $this->db->join('users receiver','receiver.user_id=rv.receiver_id');
        $this->db->join('job','job.id=rv.job_id');
        $this->db->where('rv.id',$id);
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
        
       
    }     */  


    public function getSections($offset=0,$limit=0) {
        $this->db->select('*');
        $this->db->from('sections');
        if ($limit!=0) {
            $this->db->limit($limit,$offset);
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }else{
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            } else return false;
        }
    }

    /*Not in use*/
    public function get_offers_list($offset=0) 
    {
       $where="offer_id !=0 and is_deleted=0";
       if($this->input->get('title')){
            $title=$this->input->get('title');
            $where.=" and title LIKE '%".$this->db->escape_like_str(trim($title))."%' ";
        }
        if($this->input->get('code')){
            $code=$this->input->get('code');
            $where.=" and code LIKE '%".$this->db->escape_like_str(trim($code))."%' ";
        }
        if($this->input->get('type')){
            $type=$this->input->get('type');
            $where.=" and type LIKE '%".$this->db->escape_like_str(trim($type))."%' ";
        }

        if($this->input->get('date_rang')){
            $date_rang=$this->input->get('date_rang');
            $split_date=explode(" - ",$date_rang);
            $start_date= date('Y-m-d',strtotime($split_date[0]));
            $end_date=date('Y-m-d',strtotime($split_date[1]));
            $where.=" and start_date >='".$start_date.' 00:00:00'."' and end_date <='".$end_date.' 00:00:00'."' ";
        } 
        
        if($this->input->get('status')){
            $status=$this->input->get('status');
           $where.=" and status='".$this->db->escape_like_str(trim($status))."'";
        }

        $limit=ADMIN_LIMIT;
        $sql="SELECT *
        FROM  `offers` 
        WHERE $where
        ORDER BY offer_id DESC
        limit $offset,$limit";
        $query=$this->db->query($sql);
        // echo $this->db->last_query();
      if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
        
    }   

    /*Not in use*/
    public function get_offers_total() 
    {
        $where="offer_id !=0 and is_deleted=0";
       if($this->input->get('title')){
            $title=$this->input->get('title');
            $where.=" and title LIKE '%".$this->db->escape_like_str(trim($title))."%' ";
        }
        if($this->input->get('code')){
            $code=$this->input->get('code');
            $where.=" and code LIKE '%".$this->db->escape_like_str(trim($code))."%' ";
        }
        if($this->input->get('type')){
            $type=$this->input->get('type');
            $where.=" and type LIKE '%".$this->db->escape_like_str(trim($type))."%' ";
        }

        if($this->input->get('date_rang')){
             $date_rang=$this->input->get('date_rang');
            $split_date=explode(" - ",$date_rang);
            $start_date= date('Y-m-d',strtotime($split_date[0]));
            $end_date=date('Y-m-d',strtotime($split_date[1]));
            $where.=" and date(start_date) >='".$start_date."' and date(end_date) <='".$end_date."' ";
        } 
        
        if($this->input->get('status')){
            $status=$this->input->get('status');
           $where.=" and status='".$this->db->escape_like_str(trim($status))."'";
        }

        $limit=ADMIN_LIMIT;
        $sql="SELECT *
        FROM  `offers` 
        WHERE $where
        ORDER BY offer_id DESC";
        $query=$this->db->query($sql);
        
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else return false;

    }

    public function getSubscription($offset=0,$limit=0) 
    {
       $where="is_deleted=0";
        
        if($this->input->get('title')){
            $title=$this->input->get('title');
           // $where.=" and title='".$this->db->escape_like_str(trim($title))."'";

            $where.=" and title LIKE '%".$this->db->escape_like_str(trim($title))."%' ";
        }

        if($this->input->get('amount_type')){
            $amount_type=$this->input->get('amount_type');
           $where.=" and amount_type='".$this->db->escape_like_str(trim($amount_type))."'";
        }

        if($this->input->get('status')){
            $status=$this->input->get('status');
           $where.=" and status='".$this->db->escape_like_str(trim($status))."'";
        }



        $this->db->select('*');
        $this->db->from('subscriptions');
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        }

        $this->db->order_by('id','DESC');
        if ($limit!=0) {
            $this->db->limit($limit,$offset);
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }else{
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            } else return false;
        }        
       
    }   

    public function getWalletHistory($offset=0,$limit=0) 
    {  
       
        if($this->input->get('status')){
            $status=$this->input->get('status');
            $this->db->like("AW.status", trim($status));
        }
        if($this->input->get('amount')){
            $amount=$this->input->get('amount');
            $this->db->like("AW.amount", trim($amount));
        }
        if($this->input->get('transaction_type')){
            $transaction_type=$this->input->get('transaction_type');
            $this->db->like("AW.transaction_type", trim($transaction_type));
        }

        if($this->input->get('pay_from')){
            $pay_from=$this->input->get('pay_from');
            if($pay_from == 'Admin'){
                $this->db->where("AW.pay_from", trim($pay_from));
            }else{
                 $this->db->like("pay_from.first_name", trim($pay_from));  
            }
               
        }

        if($this->input->get('pay_to')){
            $pay_to=$this->input->get('pay_to');
            if($pay_to == 'Admin'){
                $this->db->where("AW.pay_to", trim($pay_to));
            }else{
                 $this->db->like("pay_to.first_name", trim($pay_to));  
            }
               
        }
        

        if($this->input->get('date')){
            $date=$this->input->get('date');
            $dates=explode(' - ',$date);
            
            $start_date=$dates[0];
            $array_date1 = explode("-", $dates[0]);  
            $fdate = $array_date1[1] . "-" . $array_date1[0] . "-" . $array_date1[2];

            $end_datee=$dates[1];
            $array_date2 = explode("-", $dates[1]);
            $edate = $array_date2[1] . "-" . $array_date2[0] . "-" . $array_date2[2];
            
           
            $this->db->where('date(AW.created) >=', date('Y-m-d',strtotime($fdate))); 
            $this->db->where('date(AW.created) <=', date('Y-m-d',strtotime($edate)));
        }

        $this->db->select('AW.*,concat(pay_from.first_name," ",pay_from.last_name) AS pay_from,concat(pay_to.first_name," ",pay_to.last_name) AS pay_to,J.job_id');
        $this->db->from('admin_wallet_history AW');
        $this->db->join('users pay_from','AW.pay_from=pay_from.user_id','LEFT');
        $this->db->join('users pay_to','AW.pay_to=pay_to.user_id','LEFT');
        $this->db->join('job J','AW.job_id=J.id','LEFT');
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        }

        $this->db->order_by('AW.id','DESC');
        if ($limit!=0) {
            $this->db->limit($limit,$offset);
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }else{
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            } else return false;
        }        
       
    } 

    public function getWithdrawRequest($offset=0,$limit=0) 
    {
       
        if($this->input->get('name')){
            $name=$this->input->get('name');
            $this->db->like("concat(U.first_name,' ',U.last_name)", trim($name));
        }

        if($this->input->get('user_type')){
            $user_type=$this->input->get('user_type');
            $this->db->where('U.user_type', $this->input->get('user_type'));
        }

        if($this->input->get('status')){
            $status=$this->input->get('status');
            $this->db->like("WR.status", trim($status));
        }
        if($this->input->get('amount')){
            $amount=$this->input->get('amount');
            $this->db->like("WR.amount", trim($amount));
        }

        if($this->input->get('transaction_id')){
            $transaction_id=$this->input->get('transaction_id');
            $this->db->like("WR.transaction_id", trim($transaction_id));
        }
        if($this->input->get('user_id')!='') {
            $this->db->where('WR.user_id', $this->input->get('user_id'));
        }
        
      
        if($this->input->get('date')){
            $date=$this->input->get('date');
            $dates=explode(' - ',$date);
            
            $start_date=$dates[0];
            $array_date1 = explode("-", $dates[0]);  
            $fdate = $array_date1[1] . "-" . $array_date1[0] . "-" . $array_date1[2];

            $end_datee=$dates[1];
            $array_date2 = explode("-", $dates[1]);
            $edate = $array_date2[1] . "-" . $array_date2[0] . "-" . $array_date2[2];
            
           
            $this->db->where('date(WR.created) >=', date('Y-m-d',strtotime($fdate))); 
            $this->db->where('date(WR.created) <=', date('Y-m-d',strtotime($edate)));
        }

        $this->db->select('WR.*,concat(U.first_name," ",U.last_name) AS name,U.user_type');
        $this->db->from('withdraw_request WR');
        $this->db->join('users U','WR.user_id=U.user_id','LEFT');
        $this->db->order_by('WR.id','DESC');
        if ($limit!=0) {
            $this->db->limit($limit,$offset);
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }else{
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            } else return false;
        }        
       
    }         
    
    public function getReports($start_date,$end_date,$type){
          
        $daysLeft = abs(strtotime($start_date) - strtotime($end_date));

        if($type == "this_week" || $type == "this_month"){
            $days = $daysLeft/(60 * 60 * 24);
            for($i =0;$i<$days;$i++){
                $date[] =  date('Y-m-d', strtotime($start_date . '+'.$i.' day'));
            }
        
        }
        if($type == "last_week" || $type == "last_month" || $type == "today"){
            $days = $daysLeft/(60 * 60 * 24)+1;
            for($i =0;$i<$days;$i++){
                $date[] =  date('Y-m-d', strtotime($start_date . '+'.$i.' day'));
            }
        }

        if($type == "last_year" || $type == "this_year"){
           
            $years = floor($daysLeft / (365*60*60*24));  
            $days = floor(($daysLeft - $years * 365*60*60*24)/(30*60*60*24)) + 1;
            for($i =0;$i<$days;$i++){
                $date[] =  date('Y-m', strtotime($start_date . '+'.$i.' month'));
            }
        }


        if($type == ""){
            $days = $daysLeft/(60 * 60 * 24);
            for($i =0;$i<$days;$i++){
                $date[] =  date('Y-m-d', strtotime($start_date . '+'.$i.' day'));
            }
        
        }

 
        $size = sizeof($date);
        for($i=0;$i<$size;$i++){

        $this->db->select('sum(admin_commission) as admin_commission,DAY(created) as day,month(created) as month,DATE(created) as date');
        $this->db->from('admin_wallet_history');
        $this->db->like('created', $date[$i]); 
        $this->db->where('transaction_type',"Credit");
        if($type == "this_week"){
            $this->db->group_by('DATE(created)');
        }
        if($type == "last_week"){
            $this->db->group_by('DATE(created)');
        }
        if($type == "last_month"){
            $this->db->group_by('DATE(created)');
        }
        if($type == "this_month"){
             $this->db->group_by('DATE(created)');
        }
        if($type == "this_year"){
            $this->db->group_by('month(created)');
        }
        if($type == "last_year"){
             $this->db->group_by('month(created)');
        }
        if($type == "today"){
            $this->db->group_by('DATE(created)');
        }
        if($type == ""){
            $this->db->group_by('DATE(created)');
        }


        $query = $this->db->get(); 
        
            if(!empty($query->result_array())){
                $full_data[$i] =  $query->row_array(); 
            }else{
               $full_data[$i]  = array(
                'admin_commission' => "0", 
                'day' => date('d',strtotime($date[$i])),
                'month' => date('m',strtotime($date[$i])), 
                'date' => $date[$i], 
               );  
            }
        }

        return $full_data;

          
      }

    public function getTotalAmount($start_date,$end_date){
        $this->db->select('sum(admin_commission) as admin_commission');
        $this->db->from('admin_wallet_history');
        $this->db->where('transaction_type',"Credit");
        $this->db->where('created >=', date('Y-m-d',strtotime($start_date))); 
        $this->db->where('created <=', date('Y-m-d',strtotime($end_date)));
        $query = $this->db->get();  
        if($query->num_rows() > 0) {
            return $query->row_array();
        } else return 0;
      
    }

    public function get_offer_used($limit,$offset,$offer_id) {

      
        if($this->input->get('name')){
            $name=$this->input->get('name');
            $this->db->like("concat(U.first_name,' ',U.last_name)", trim($name));
        }
           if($this->input->get('email')){
            $email=$this->input->get('email');
            $this->db->like("CH.redeem_user_email", trim($email));
        }

    
        
        if($this->input->get('date')){
            $date=$this->input->get('date');
            $dates=explode(' - ',$date);
            
            $start_date=$dates[0];
            $array_date1 = explode("-", $dates[0]);  
            $fdate = $array_date1[1] . "-" . $array_date1[0] . "-" . $array_date1[2];

            $end_datee=$dates[1];
            $array_date2 = explode("-", $dates[1]);
            $edate = $array_date2[1] . "-" . $array_date2[0] . "-" . $array_date2[2];
            
           
            $this->db->where('CH.created >=', date('Y-m-d',strtotime($fdate))); 
            $this->db->where('CH.created <=', date('Y-m-d',strtotime($edate)));

        }
        

         
        $this->db->select('CH.*,concat(U.first_name," ",U.last_name) as name');
        $this->db->from('coupon_history CH');
        $this->db->join('users U','CH.redeem_user_id=U.user_id','left');
        $this->db->where('CH.offer_id',$offer_id);
        $this->db->order_by('CH.id','desc');
        $this->db->limit($limit, $offset);
        $query=$this->db->get();
        //echo $this->db->last_query();exit;
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }
    

    public function getInvoiceDetails($job_id,$contract_id){
        $this->db->select('C.*,SC.name as service_category,concat(applied.first_name," ",applied.last_name) as freelancer_name,applied.email as freelancer_email,concat(posted.first_name," ",posted.last_name) as individual_name,posted.email as individual_email,JPD.penalty_type,JPD.freelancer_penalty,JPD.indivisible_penalty,JPD.freelancer_get,JPD.indivisible_get');
        $this->db->from('contract C');
        $this->db->join('service_category SC','SC.id=C.service_catagory','left');
        $this->db->join('users posted','C.posted_by=posted.user_id','left');
        $this->db->join('users applied','C.applied_by=applied.user_id','left');
        $this->db->join('job_payment_details JPD','C.id=JPD.contract_id','left');
        $this->db->where('C.id',$contract_id);
        $query =$this->db->get();
        //echo $this->db->last_query();die;
        $result = $query->row_array();  
        return $result;  
    }

    public function paymentHistoryInvoice($job_id){
        
        $contract_id = $this->Common_model->getFieldValue('contract','id',array('job_id'=>$job_id));
        $data['job_id'] = $this->Common_model->getFieldValue('job','job_id',array('id'=>$job_id));
        $data['job_details'] = $this->getInvoiceDetails($job_id,$contract_id);

        $check_invoice = $this->Common_model->getFieldValue('job_invoice','id',array('job_id'=>$job_id));
        if($check_invoice){
           return true;
        }
        
        define('_MPDF_PATH',APPPATH.'/third_party/mpdf/');
        include(APPPATH.'/third_party/mpdf/mpdf.php');

        /***************individual*******************/
 
        $html= $this->load->view('admin/freelancer_job_payment_invoice',$data,true);
        
    
        $html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');

     
        $mpdf=new mPDF('','A4','','',20,15,25,25,10,10); 
     
        $mpdf->SetProtection(array('print'));
 
        $mpdf->autoScriptToLang = true;
        $mpdf->autoLangToFont = true;
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($html);
    
        $pdf_name = rand()."_".time().".pdf"; 
        $pdf_name = str_replace(" ","_",$pdf_name);
        $pdfFilePath_f = 'resources/invoice/MF'.$pdf_name;

        //F for file and D for download
        $mpdf->Output($pdfFilePath_f, "F");
        //echo $mpdf->Output(); 

        /***************individual end*******************/
        
        /***************Freelancer*******************/
 
        $html_i = $this->load->view('admin/individual_job_payment_invoice',$data,true);
        
    
        $html_i = mb_convert_encoding($html_i, 'UTF-8', 'UTF-8');

     
        $mpdf=new mPDF('','A4','','',20,15,25,25,10,10); 
     
        $mpdf->SetProtection(array('print'));
 
        $mpdf->autoScriptToLang = true;
        $mpdf->autoLangToFont = true;
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($html_i);
    
        $pdf_name = rand()."_".time().".pdf"; 
        $pdf_name = str_replace(" ","_",$pdf_name);
        $pdfFilePath_i = 'resources/invoice/MI'.$pdf_name;

        //F for file and D for download
        $mpdf->Output($pdfFilePath_i, "F");
        //echo $mpdf->Output(); 

        /***************Freelancer end*******************/

        $insert_data = array(
            'job_id' => $job_id,
            'individual_invoice' => $pdfFilePath_i,
            'freelancer_invoice' => $pdfFilePath_f,
            'created' => date("Y-m-d H:i:s"),

        );
        if($last_id = $this->Common_model->addEditRecords('job_invoice', $insert_data)){

            $update_data = array(
                'invoice_id' => "M0000".$last_id,
            );

            $this->Common_model->addEditRecords('job_invoice', $update_data,array('id'=>$last_id));
        }


    }

}/* model end*/

?>