<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

    public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function getDistrictList($stateId=''){
        try{
            $this->db->select('d.id,d.name');
            $this->db->from('district d');
            $this->db->join('users','users.district_id=d.id');
            if (!empty($stateId)) {
                $this->db->where('d.state_id', $stateId);
            }
            $this->db->where('users.is_approve', 3);
            $this->db->where('users.user_type', 1);
            $this->db->where('users.is_deleted', 0);
            $this->db->order_by('d.name','ASC');
            $query = $this->db->get();
            return $query->result_array();
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function getSchoolList($districtId=''){
        try{
            $this->db->select('s.id,s.name');
            $this->db->from('school s');
            $this->db->join('district d','d.id=s.district_id');
            $this->db->join('users','users.school_id=s.id');
            if (!empty($districtId)) {
                $this->db->where('s.district_id', $districtId);
            }
            $this->db->where('users.is_approve', 3);
            $this->db->where('users.user_type', 2);
            $this->db->where('users.is_deleted', 0);
            $this->db->where('s.is_deleted', 0);
            $this->db->order_by('s.name','ASC');
            $query = $this->db->get();
            return $query->result_array();
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function getGradeList($schoolId=''){
        try{
            $this->db->select('sg.id,sg.grade,sg.grade_display_name');
            $this->db->from('school_grade sg');
            // $this->db->join('district d','d.id=s.district_id');
            // $this->db->join('users','users.school_id=s.id');
            if (!empty($schoolId)) {
                $this->db->where('sg.school_id', $schoolId);
            }
            // $this->db->where('users.is_approve', 3);
            // $this->db->where('users.user_type', 2);
            $this->db->order_by('sg.id','ASC');
            $query = $this->db->get();
            return $query->result_array();
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function getUserDetail($userId,$user_type=""){
        try{
            $sel="u.user_id,u.title,u.display_name,u.user_type,u.first_name,u.last_name,u.email,u.profile_image,u.state_id,u.district_id,u.is_approve,u.email_verified,u.is_public,u.status,d.name as district_name,s.state_name,u.phone,s.state_name,u.unread_notification,u.notification_flag";
            if($user_type!=1){
                $sel.=",sch.name as school_name,sch.id as school_id,sch.logo,sch.address";
            }
            $this->db->select($sel);
            $this->db->from('users u');
            $this->db->join('district d','d.id=u.district_id','left');
            $this->db->join('states s','s.state_id=u.state_id','left');
            if($user_type!=1){
                $this->db->join('school sch','sch.id=u.school_id','left');
            }
            $this->db->where('u.user_id', $userId);
            $this->db->where('u.is_deleted', 0);
            $query = $this->db->get();
            return $query->row_array();
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function getBroadcastMessageList($user_id){
        try{
            $page=trim($this->input->post('page'));
            $limit=trim($this->input->post('limit'));
            if(!$page){
                $page=0;
            }
            if(!$limit){
                $limit=1000;
            }
            $offset=$limit*$page;
            $this->db->select('b.notification_id,b.subject,b.message,b.created');
            $this->db->from('broadcast_notification b');
            $this->db->where('b.created_by', $user_id);
            $this->db->where('b.is_deleted', 0);
            $this->db->where('b.sent_by', 0);
            $this->db->order_by('b.created','desc');
            $this->db->limit($limit,$offset);
            $query = $this->db->get();
            return $query->result_array();
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }
    public function getTeacherRequestCount($teacher_id){
        try{
            $this->db->select('S.id');
            $this->db->from('teacher_grade TG');
            $this->db->join('student_grade S','S.teacher_grade_id=TG.id');
            $this->db->join('student stu','stu.id=S.student_id');
            $this->db->where('S.is_approve', 1);
            $this->db->where('stu.status', 1);
            $this->db->where('TG.teacher_id', $teacher_id);
            $this->db->where('S.is_deleted','0');
            $this->db->where('stu.is_deleted','0');
            $this->db->order_by('S.created','desc');
            $query = $this->db->get();
            return $query->num_rows();
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function TeacherGradeUpdateRelavantRecordDelete($school_id,$user_id,$grade,$teacher_grade_id){
        try{
            $this->Common_model->deleteRecords('teacher_grade',array('school_id'=>$school_id,'teacher_id'=>$user_id,'grade'=>$grade));
            // $this->Common_model->deleteRecords('student_grade',array('teacher_grade_id'=>$teacher_grade_id));            
            // $this->Common_model->addEditRecords('student_grade',array('is_deleted'=>1),array('teacher_grade_id'=>$teacher_grade_id));
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }
     public function DeleteGradeGroup($school_id,$user_id,$grade){
        try{
            $this->Common_model->addEditRecords('chat_groups', array('deleted'=>1),array('created_by'=>$user_id,'grade'=>$grade,'school_id'=>$school_id));
            
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function getEmailNotificationList(){
        try{
            $this->db->select('e.*,u.user_type as to_user_type');
            $this->db->from('email_notification e');
            $this->db->join('users u','u.user_id=e.user_id','left');
            $this->db->where('e.status', 0);
            $this->db->order_by('e.id','ASC');
            $query = $this->db->get();
            //echo $this->db->last_Query();exit();
            return $query->result_array();
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

}