<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teacher_model extends CI_Model { 

    public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function getStudentList($teacher_id,$school_id,$approve){
        try{
            $page=trim($this->input->post('page'));
            $limit=trim($this->input->post('limit'));
            if(!$page){
                $page=0;
            }
            if(!$limit){
                $limit=APP_PAGE_LIMIT;
            }
            $offset=$limit*$page;
            $sel="stu.id,stu.student_name,SG.id as request_id,SG.teacher_grade_id,U.first_name,U.last_name,TG.grade_display_name as grade";
            $this->db->select($sel);
            $this->db->from('teacher_grade TG');
            $this->db->join('student_grade SG','TG.id=SG.teacher_grade_id');
            $this->db->join('student stu','stu.id=SG.student_id');
            $this->db->join('users U','U.user_id=stu.parent_id');
            $this->db->where('TG.teacher_id', $teacher_id);
            $this->db->where('TG.school_id', $school_id);
            $this->db->where('SG.is_deleted', 0);
            $this->db->where('SG.is_approve', $approve);
            $this->db->where('U.user_type', 4);
            $this->db->where('U.is_deleted', 0);
            $this->db->order_by('SG.created','DESC');
            $this->db->limit($limit,$offset);
            $query = $this->db->get();
            //echo $this->db->last_query();exit;
            return $query->result_array();
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

     public function getStudentListWeb($teacher_id,$school_id,$approve,$limit,$page){
        try{
           
             if(!$page){
                $page=0;
            }
            if($limit==0){
                $limit=0;
            }else{
                $limit=APP_PAGE_LIMIT;
            }
            $offset=$limit*$page;
            $sel="stu.id,stu.student_name,SG.id as request_id,SG.teacher_grade_id,U.first_name,U.last_name,TG.grade_display_name as grade";
            $this->db->select($sel);
            $this->db->from('teacher_grade TG');
            $this->db->join('student_grade SG','TG.id=SG.teacher_grade_id');
            $this->db->join('student stu','stu.id=SG.student_id');
            $this->db->join('users U','U.user_id=stu.parent_id');
            $this->db->where('TG.teacher_id', $teacher_id);
            $this->db->where('TG.school_id', $school_id);
            $this->db->where('SG.is_deleted', 0);
            $this->db->where('SG.is_approve', $approve);
            $this->db->where('U.user_type', 4);
            $this->db->where('U.is_deleted', 0);
            $this->db->order_by('SG.created','DESC');
            if($limit==0 && $offset==0){
            $query = $this->db->get();
            //echo $this->db->last_query();exit;
            return $query->num_rows(); 
            }else{
            $this->db->limit($limit,$offset);
            $query = $this->db->get(); 
            //echo $this->db->last_query();
            return $query->result_array();
            }
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function getAllVideos($user_id,$user_type="",$school_id="",$teacher_id="",$grade="")
    {
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=APP_PAGE_LIMIT;
        }
        
        if(isset($user_type) && !empty($user_type)){
            if($user_type==1){
                $district_id = $this->Common_model->getFieldValue('users','district_id',array("user_id"=>$user_id));
                $this->db->where('m.district_id', $district_id);
            }
        }
        if(isset($school_id) && !empty($school_id)){
            $this->db->where('m.school_id', $school_id);
        }
        if(isset($teacher_id) && !empty($teacher_id)){
            $this->db->where('m.user_id', $teacher_id);
            //$this->db->where_IN('m.user_id', $teacher_id,False);

        }
        if(isset($grade) && $grade!=""){
            $this->db->where('m.grade', $grade);
            //$this->db->where_IN('m.grade', $grade,False);
        }

        $offset=$limit*$page;
        $this->db->select('m.id as media_id,m.school_id,school.name as school_name,m.grade,m.grade_display_name,m.user_id,m.grade,m.grade_display_name,m.media_type,m.format,m.title,m.media_slug,m.description,m.thumb_image,m.large_image,m.media_name,m.duration,m.created,u.display_name,u.profile_image,u.first_name,u.last_name,u.user_type');
        $this->db->from('media m');
        $this->db->join('users u','u.user_id = m.user_id');
        $this->db->join('school','school.id = m.school_id');
        $this->db->where('m.status', 1);
        $this->db->where('m.media_type', 1);
        if(empty($user_type)){
            $this->db->where('m.user_id', $user_id);
        }
        $this->db->where('m.created_as', 1);
        $this->db->where('m.is_deleted', 0);
        $this->db->where('u.is_deleted', 0);
        $this->db->order_by('m.id','DESC');
        $this->db->limit($limit,$offset);
        $query = $this->db->get();
        // echo $this->db->last_query();exit;
        return $query->result_array();
        
    }

    public function getVideosHome($user_id,$user_type="",$school_id="",$teacher_id="",$grade="")
    {
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=3;
        }
        if(isset($user_type) && !empty($user_type)){
            if($user_type==1){
                $district_id = $this->Common_model->getFieldValue('users','district_id',array("user_id"=>$user_id));
                $this->db->where('m.district_id', $district_id);
            }
        }
        if(isset($school_id) && !empty($school_id)){
            $this->db->where('m.school_id', $school_id);
        }
        if(isset($teacher_id) && !empty($teacher_id)){
            $this->db->where('m.user_id', $teacher_id);
            //$this->db->where_IN('m.user_id', $teacher_id,False);

        }
        if(isset($grade) && $grade!=""){
            $this->db->where('m.grade', $grade);
            //$this->db->where_IN('m.grade', $grade,False);
        }

        $offset=$limit*$page;
        $this->db->select('m.id as media_id,m.school_id,m.grade,m.grade_display_name,m.user_id,m.grade,m.grade_display_name,m.media_type,m.format,m.title,m.media_slug,m.description,m.thumb_image,m.large_image,m.media_name,m.duration,m.created,u.display_name,u.profile_image,u.first_name,u.last_name');
        $this->db->from('media m');
        $this->db->join('users u','u.user_id = m.user_id');
        $this->db->where('m.status', 1);
        $this->db->where('m.media_type', 1);
        if(empty($user_type)){
            $this->db->where('m.user_id', $user_id);
        }
        $this->db->where('m.created_as', 1);
        $this->db->where('m.is_deleted', 0);
        $this->db->where('u.is_deleted', 0);
        $this->db->order_by('m.id','DESC');
        $this->db->limit($limit,$offset);
        $query = $this->db->get();
        return $query->result_array();
        
    }

    public function getRecentVideo($is_home,$is_continue,$user_id="",$teacher_id="",$school_id="")
    {
        if($is_home==1){
            $page=0;
            $limit=3;
        }else{
            $page=trim($this->input->post('page'));
            $limit=trim($this->input->post('limit'));
            if(!$page){
                $page=0;
            }
            if(!$limit){
                $limit=APP_PAGE_LIMIT;
            }
        }
        if(isset($school_id) && !empty($school_id)){
            $this->db->where('m.school_id', $school_id);
        }
        //echo $teacher_id;exit;
        if(isset($teacher_id) && !empty($teacher_id) && $teacher_id!=""){
            $this->db->where_IN('m.teacher_grade_id', $teacher_id,False);

        }
        $today=date("Y-m-d");
        $last_three_days=date("Y-m-d", strtotime("- 3 days"));
        $offset=$limit*$page;
        $this->db->select('m.id as media_id,m.school_id,m.grade,m.grade_display_name,m.user_id,m.grade,m.grade_display_name,m.media_type,m.format,m.title,m.media_slug,m.description,m.thumb_image,m.large_image,m.media_name,m.duration,m.created,u.display_name,u.profile_image,u.first_name,u.last_name');
        if($is_continue==1){
            $this->db->select('c.duration as current_duration');
        }
        $this->db->from('media m');
        $this->db->join('users u','u.user_id = m.user_id');
        if($is_continue==1){
            $this->db->join('continue_watch_media c','c.media_id = m.id and c.user_id='.$user_id.'');
        }
        $st_time = date("Y-m-d H:i:s", strtotime("-36 hours"));
        $this->db->where('m.status', 1);
        $this->db->where('m.media_type', 1);
        $this->db->where('m.created_as', 1);
        $this->db->where('m.is_deleted', 0);
        $this->db->where('u.is_deleted', 0);
        $this->db->where('m.created >=', $st_time);
        // $this->db->where('m.created <=', $new_time);
        $this->db->order_by('m.id','DESC');
        $this->db->limit($limit,$offset);
        $query = $this->db->get();
        //print_r($this->db->last_query($query));die;
        return $query->result_array();
        
    }

     public function getRecentVideoCount($is_home="",$is_continue,$user_id="",$teacher_id="",$school_id="")
    {
        if($is_home==1){
            $page=0;
            $limit=3;
        }else{
            $page=trim($this->input->post('page'));
            $limit=trim($this->input->post('limit'));
            if(!$page){
                $page=0;
            }
            if(!$limit){
                $limit=APP_PAGE_LIMIT;
            }
        }
        if(isset($school_id) && !empty($school_id)){
            $this->db->where('m.school_id', $school_id);
        }
        //echo $teacher_id;exit;
        if(isset($teacher_id) && !empty($teacher_id) && $teacher_id!=""){
            $this->db->where_IN('m.teacher_grade_id', $teacher_id,False);

        }
        $offset=$limit*$page;
        $this->db->select('m.id as media_id,m.school_id,m.grade,m.grade_display_name,m.user_id,m.grade,m.grade_display_name,m.media_type,m.format,m.title,m.media_slug,m.description,m.thumb_image,m.large_image,m.media_name,m.duration,m.created,u.display_name,u.profile_image,u.first_name,u.last_name');
        if($is_continue==1){
            $this->db->select('c.duration as current_duration');
        }
        $this->db->from('media m');
        $this->db->join('users u','u.user_id = m.user_id');
        if($is_continue==1){
            $this->db->join('continue_watch_media c','c.media_id = m.id and c.user_id='.$user_id.'');
        }
        $this->db->where('m.status', 1);
        $this->db->where('m.media_type', 1);
        $this->db->where('m.created_as', 1);
        $this->db->where('u.is_deleted', 0);
        $this->db->where('m.is_deleted', 0);
        $this->db->order_by('m.id','DESC');
        $this->db->limit($limit,$offset);
        $query = $this->db->get();
        return $query->result_array();
        
    }


   
    public function getUploadDetail($id){
       
        $sel='m.id, m.school_id,school.name as school_name,m.grade,m.grade_display_name,m.user_id, m.media_type, m.format, m.title, m.media_slug, m.description, m.thumb_image, m.large_image, m.media_name, m.duration, (SELECT count(id) FROM comments WHERE media_id = m.id AND parent_id = 0) as comments, m.created_as, m.views, m.likes, m.created,u.first_name,u.last_name,u.display_name,u.profile_image,IFNULL(C.duration,0) as current_duration';
        $this->db->select($sel);
        $this->db->from('media m');
        $this->db->join('users u','u.user_id = m.user_id');
        $this->db->join('school','school.id = m.school_id');
        $this->db->join('continue_watch_media C','C.media_id = m.id','left');
        $this->db->where('m.status', 1);
        $this->db->where('m.is_deleted', 0);
        $this->db->where('u.is_deleted', 0);
        $this->db->where('m.id',$id);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function getUploadShoolDetail($id){
       
        $sel='m.id, m.school_id,school.name as school_name,m.user_id, m.media_type, m.format, m.title, m.media_slug, m.description, m.thumb_image, m.large_image, m.media_name, m.duration, m.views,m.created,u.first_name,u.last_name,u.display_name,u.profile_image';
        $this->db->select($sel);
        $this->db->from('school_introductory_video m');
        $this->db->join('users u','u.user_id = m.user_id');
        $this->db->join('school','school.id = m.school_id');
        $this->db->where('m.status', 1);
        $this->db->where('m.is_deleted', 0);
        $this->db->where('u.is_deleted', 0);
        $this->db->where('m.id',$id);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function CheckTeacherApprovedStatus($grade,$teacher_id,$parent_id){
        $sel='S.id,is_approve';
        $this->db->select($sel);
        $this->db->from('student S');
        $this->db->join('student_grade SG','SG.student_id=S.id');
        $this->db->join('teacher_grade TG','TG.id=SG.teacher_grade_id');
        $this->db->where('TG.grade', $grade);
        $this->db->where('TG.teacher_id', $teacher_id);
        $this->db->where('S.parent_id', $parent_id);
        $this->db->where('SG.is_approve', 2);
        $this->db->where('S.is_deleted', 0);
        $this->db->where('SG.is_deleted', 0);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function getUniqueGrade($parent_id){
        $sel='CAST(SG.grade AS UNSIGNED) as grade,group_concat(distinct(SG.teacher_grade_id)) as teacher_grade_id';
        $this->db->select($sel);
        $this->db->from('student S');
        $this->db->join('student_grade SG','S.id=SG.student_id');
        $this->db->where('S.parent_id',$parent_id);
        $this->db->group_by('grade');
        $this->db->order_by('grade');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTeacherDetailsByGrade($teacher_grade_id){
        $sel='U.display_name,U.first_name,U.last_name,U.profile_image,TG.grade,TG.grade_display_name,TG.teacher_id';
        $this->db->select($sel);
        $this->db->from('teacher_grade  TG');
        $this->db->join('users U','U.user_id =TG.teacher_id');
        $this->db->where('U.is_deleted', 0);
        $this->db->where_IN('TG.id',$teacher_grade_id,FALSE);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getUniqueGradeCommaSeprated($parent_id){
        $sel='group_concat(distinct(SG.grade)) as grade';
        $this->db->select($sel);
        $this->db->from('student S');
        $this->db->join('student_grade SG','S.id=SG.student_id');
        $this->db->where('S.parent_id',$parent_id);
        //$this->db->group_by('SG.grade');
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        return $query->row_array();
    }
    public function getApprovedTeachers($parent_id){
        $sel='group_concat(distinct(SG.teacher_grade_id)) as teacher_grade_id';
        $this->db->select($sel);
        $this->db->from('student S');
        $this->db->join('student_grade SG','S.id=SG.student_id');
        $this->db->where('S.parent_id',$parent_id);
        $this->db->where('SG.is_approve',2);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getTeacherChatList($user_id,$school_id,$keyword=""){
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=APP_PAGE_LIMIT;
        }
        $offset=$limit*$page;
        $sel="SELECT U.user_id, concat(U.first_name,' ',U.last_name) as display_name, U.user_type,U.profile_image  
        FROM `teacher_grade` tg 
        inner join student_grade SG ON SG.teacher_grade_id=tg.id 
        inner join student S ON S.id= SG.student_id 
        left join users U ON U.user_id=S.parent_id 
        WHERE tg.teacher_id=$user_id  and tg.school_id=$school_id and SG.is_approve=2 and U.school_id= $school_id and U.is_deleted=0 and U.is_approve=3  and U.status=1 ";
        if($keyword!=""){
            $sel.=" and concat(U.first_name,' ',U.last_name) like '%$keyword%'";
        }
        $sel.=" group by parent_id ";
        $sel.="UNION ";
        $sel.="SELECT  U.user_id, U.display_name as display_name, U.user_type,U.profile_image 
        from users U 
        where U.user_type=3 and U.is_deleted=0 and U.is_approve=3 and U.school_id=$school_id  and U.status=1 and U.user_id!=$user_id";
        if($keyword!=""){
            $sel.="  and U.display_name like '%$keyword%'";
        }
        // $sel.=" order by display_name Asc ";
        $sel.=" order by user_type Asc ";
        if($limit>0){
            $sel.="limit $offset , $limit";
        }
        $rs = $this->db->query($sel);
        //echo $this->db->last_query();exit; 
        if($limit>0){
            return $rs->result_array();
        }else{
            return $rs->num_rows();
        }
    }
    public function getParentChatList($user_id,$school_id,$grades,$keyword=""){
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=APP_PAGE_LIMIT;
        }
        $offset=$limit*$page;
        $sel="SELECT U.user_id, U.display_name as display_name, U.user_type,U.profile_image 
        FROM student S 
        JOIN student_grade SG ON S.id=SG.student_id 
        JOIN teacher_grade tg ON tg.id=SG.teacher_grade_id 
        LEFT JOIN users U ON U.user_id=tg.teacher_id 
        WHERE S.parent_id =$user_id  AND SG.is_approve = 2 and U.school_id= $school_id and U.is_deleted=0 and U.is_approve=3  and U.status=1 ";
        if($keyword!=""){
            $sel.="  and U.display_name like '%$keyword%'";
        }
        $sel.=" group by tg.teacher_id ";

        $sel.="UNION ";
        //$sel.="SELECT  U.user_id,concat(U.first_name,' ',U.last_name) as display_name,  U.user_type,U.profile_image from users U where U.user_type=4 and U.is_deleted=0 and U.is_approve=3 and U.school_id=$school_id  and U.status!=2 and U.is_public=1";
        $sel.="SELECT  U.user_id,concat(U.first_name,' ',U.last_name) as display_name,U.user_type,U.profile_image 
        from users U 
        inner join student S ON S.parent_id=U.user_id 
        inner join student_grade SG ON SG.student_id=S.id 
        where U.user_type=4 and U.is_deleted=0 and U.is_approve=3 and U.school_id=$school_id  and U.status=1 and U.is_public=1 and  S.is_deleted=0 and U.user_id!=$user_id";
        if($grades!=""){
            $sel.=" and SG.grade IN ($grades)";
        } 
        
        if($keyword!=""){
            $sel.=" and concat(U.first_name,' ',U.last_name) like '%$keyword%'";
        }
        $sel.=" group by U.user_id";
        // $sel.=" order by display_name Asc ";
        $sel.=" order by user_type Asc ";
        if($limit>0){
            $sel.=" limit $offset , $limit";
        }
        $rs = $this->db->query($sel); 
        if($limit>0){
            // echo $this->db->last_query();exit;
            return $rs->result_array();
        }else{
            return $rs->num_rows();
        }
    }
    public function getParentByRequest($request_id){
        $sel='S.student_name,S.parent_id,SG.grade';
        $this->db->select($sel);
        $this->db->from('student_grade SG');
        $this->db->join('student S','SG.student_id = S.id');
        $this->db->where('SG.id',$request_id);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function getParentByTeacherGradeId($teacher_grade_id){
        $sel='S.parent_id,U.email,U.first_name,U.last_name';
        $this->db->select($sel);
        $this->db->from('student_grade SG');
        $this->db->join('student S','SG.student_id = S.id');
        $this->db->join('users U','U.user_id = S.parent_id');
        $this->db->where('SG.teacher_grade_id',$teacher_grade_id);
        $this->db->where('SG.is_approve',2);
        $this->db->where('U.is_deleted',0);
        $this->db->group_by('S.parent_id');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAllVideosCount($user_id,$user_type="",$school_id="",$teacher_id="",$grade="")
    {
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=APP_PAGE_LIMIT;
        }
        
        if(isset($user_type) && !empty($user_type)){
            if($user_type==1){
                $district_id = $this->Common_model->getFieldValue('users','district_id',array("user_id"=>$user_id));
                $this->db->where('m.district_id', $district_id);
            }
        }
        if(isset($school_id) && !empty($school_id)){
            $this->db->where('m.school_id', $school_id);
        }
        if(isset($teacher_id) && !empty($teacher_id)){
            $this->db->where('m.user_id', $teacher_id);
            //$this->db->where_IN('m.user_id', $teacher_id,False);

        }
        if(isset($grade) && $grade!=""){
            $this->db->where('m.grade', $grade);
            //$this->db->where_IN('m.grade', $grade,False);
        }

        $offset=$limit*$page;
        $this->db->select('count(*) as count');
        $this->db->from('media m');
        $this->db->join('users u','u.user_id = m.user_id');
        $this->db->where('m.status', 1);
        $this->db->where('m.media_type', 1);
        if(empty($user_type)){
            $this->db->where('m.user_id', $user_id);
        }
        $this->db->where('m.created_as', 1);
        $this->db->where('m.is_deleted', 0);
        $this->db->where('u.is_deleted', 0);
        $this->db->order_by('m.id','DESC');
        
        $query = $this->db->get();
        // echo $this->db->last_query();exit;
        return $query->result_array();
        
    }
    

   
}