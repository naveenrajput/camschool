<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
   /* $CI = get_instance();

    // You may need to load the model if it hasn't been pre-loaded
    $CI->load->model('Auth_model');
    $CI->load->model('common_model');
    $CI->load->model('Teacher_model');*/

function apiAuthentication(){
    $h_key = getallheaders();

    $apiKey = "";
    if(isset($h_key['Apikey']))
    {
        $apiKey=$h_key['Apikey'];
    }
    if(isset($h_key['apikey']))
    {
        $apiKey=$h_key['apikey'];
    }

    if(APP_KEY !== $apiKey){ //check header key for authorization 
        return false;
    }else{
        return true;
    }
}
function check_user_status() 
{
    $ci =& get_instance();
    $type  =  $ci->input->post('type');
    if(!isset($type) || empty($type)){
        display_output('0','Please enter type.');
    }
    if(!in_array($type, array('Mobile','web','chat_user'))){
        display_output('0','Please enter valid type.');
    }
    if($type=='Mobile'){
        $user_id  =  $ci->input->post('user_id');
        $mobile_auth_token  =  $ci->input->post('mobile_auth_token');
        if(empty($user_id)){
            display_output('0','Please enter user id.');
        }
        if(empty($mobile_auth_token)){
            display_output('0','Enter the mobile number.');
        }

        $where['user_id'] = $user_id;
        if($user_data = $ci->Common_model->getRecords('users','mobile_auth_token,status,is_deleted',$where,'',true)) {
            
            if($user_data['is_deleted']==1 || $user_data['status']=='2'){

                display_output('4','Your account is no longer active. Email accounts@chatatmeschools.com to have your account reinstated.');
            }else{
                if(empty($user_data['mobile_auth_token'])) {
                      display_output('4','You logged out from this device.');  
                } else {
                    if($user_data['mobile_auth_token']!=$mobile_auth_token)
                    {
                        display_output('4','You logged out from this device.'); 
                    }
                }
            }
        }else{
            display_output('4','Details not found. Please contact to admin.');
        }
    }else{
        if($type!='chat_user'){
            if ($ci->input->is_ajax_request()) {
                if($ci->session->userdata('user_id')) {
                    // return true;
                    $where['user_id'] = $ci->session->userdata('user_id');
                    if($user_data = $ci->Common_model->getRecords('users','status,is_deleted',$where,'',true)) {
                        
                        if($user_data['is_deleted']==1 || $user_data['status']=='2'){

                            display_output('4','Your account is no longer active. Email accounts@chatatmeschools.com to have your account reinstated.');
                        }
                        return true;
                    }else{
                        display_output('4','Details not found. Please contact to admin.');
                    }
                } else {
                    display_output('4','You logged out from this device.');
                }
            }else{
                if($ci->session->userdata('user_id')) {
                    // return true;
                    $where['user_id'] = $ci->session->userdata('user_id');
                    if($user_data = $ci->Common_model->getRecords('users','status,is_deleted',$where,'',true)) {
                        if($user_data['is_deleted']==1 || $user_data['status']=='2'){
                            redirect('');
                        }
                        return true;
                    }else{
                        redirect('');
                    }
                } else {
                    redirect('');
                }
            }
        }
    }
    
}
function check_web_user_status($chk_redirect="") 
{    
    $ci =& get_instance();
    if($ci->session->userdata('user_id')) {
        $user_id = $ci->session->userdata('user_id');       
        $user_data = $ci->Common_model->getRecords('users','display_name,user_type,is_approve,status,is_deleted,school_id',array('user_id'=>$user_id),'',true);
        // if($user_data['status'] != 1 || $user_data['is_deleted'] == 1) {
        if($user_data['is_deleted'] == 1) {
           
            $ci->session->sess_destroy();
            redirect('');
           
        }
        if($user_data['status'] == 2){
               
            $ci->session->sess_destroy();
            redirect('');
        }
        if($user_data['is_approve']==2) {
            if(!$chk_redirect){
                if ($user_data['user_type']==1) {
                    if($user_data['status']!=1){//changepw
                        redirect('change-password');
                    }
                } 
                if ($user_data['user_type']==2) {  
                    if($user_data['status']!=1){
                       redirect('change-password');//changepw
                    }

                    if(empty($user_data['display_name'])) {
                        redirect('principal/profile');
                    }
                    if($chk_school = $ci->Common_model->getRecords('school','id',array('id'=>$user_data['school_id'],'name!='=>"",'address!='=>""),'',true,'','',1)){
                        redirect('principal/profile');
                    }
                }
                if ($user_data['user_type']==3) {  
                    if($user_data['status']!=1){
                       redirect('change-password');//changepw
                    }
                    if(empty($user_data['display_name'])) {
                        redirect('teacher/profile');
                    }
                }
                if ($user_data['user_type']==4) {

                    if($chk_student = $this->Common_model->getRecords('student','parent_id',array('parent_id'=>$userData['user_id'],'is_deleted'=>0),'',true,'','',1)){
                        redirect('parent/index');
                    }else{
                        redirect('parent/profile');
                    }  
                }
            }
            
        }
        return true;
    } else {
        redirect('');
    }
}
if (!function_exists('userRedirection')){
    function userRedirection($user_type){
        switch ($user_type) {
            case '1':
                redirect('principal-school-approved-list');
            break;
            case '2':
                redirect('teacher-approved-list');
            break;
            case '3':
                redirect('student-request');
            break;
            case '4':
                redirect('parent/profile');
            break;
            default:
               redirect(base_url());
            break;
        }
    }
}
if (!function_exists('userRequestCount')){
    function userRequestCount($user_type){
        $ci =& get_instance();
        switch ($user_type) {
            case '1':
                $where=array('is_approve'=>1,'is_deleted'=>0,'email_verified'=>1,'user_type'=>2,'district_id'=>$ci->session->userdata('district_id'));
                return $request_count=$ci->Common_model->getNumRecords('users',array('user_id'),$where);
            break;
            case '2':
                 $where=array('is_approve'=>1,'is_deleted'=>0,'email_verified'=>1,'user_type'=>3,'school_id'=>$ci->session->userdata('school_id'));
                return $request_count=$ci->Common_model->getNumRecords('users',array('user_id'),$where);
            break;
            case '3':
                $user_id=$ci->session->userdata('user_id');
               
                return $request_count=$ci->Auth_model->getTeacherRequestCount($user_id);
                    
            break;
            case '8'://for notification count
                $user_id=$ci->session->userdata('user_id');
                /*$unread_notification = $ci->Common_model->getFieldValue('users','unread_notification',array('user_id'=>$user_id,'is_deleted'=>0));*/

                $st_time=date("Y-m-d H:i:s");
                $new_time = date("Y-m-d H:i:s", strtotime("-24 hours"));
                
               $where = "read='0' and to_user=$user_id AND is_deleted =0 AND created_by !=$user_id and created_datetime between '$new_time' and '$st_time'";
               
                $unread_notification = $ci->Common_model->getNumRecords('notification','id',$where);

                return $unread_notification;
            break;
           
        }
    }
}

if (!function_exists('display_output')){
    function display_output($status,$msg,$data=array()){
        $response =  array('status'=>$status,'msg'=>$msg);
        if(!empty($data)){
           $response= array_merge($response,$data);
        }
        echo json_encode($response); 
        exit;
    }
}

if (!function_exists('is_used')){
    function is_used($table,$is_used,$id){
        //get main CodeIgniter object
        $ci =& get_instance();
        $res =array();
        $res = $ci->Common_model->getRecords($table,$id,array($id=>$is_used),'',true);
        return $res[$id];
    }
}


if (!function_exists('passwordValidate')){
    function passwordValidate($password){
        if(preg_match("/^(?=.*\d)(?=.*[a-zA-Z])(?=.*[-_!@#$]).{8,30}$/", $password)) {
            return true;
        } else {
           return false; 
        }
    }
}

if (!function_exists('getAdminEmail')){
    function getAdminEmail(){
        //get main CodeIgniter object
        $ci =& get_instance();
        $res =array();
        if($res = $ci->Common_model->getRecords('admin','email',array('admin_id' => 1),'',true)){
            return $res['email'];
        }else{
            return ADMIN_EMAIL;
        }
        
    }
}
if (!function_exists('getNotificationEmail')){
    function getNotificationEmail(){
        //get main CodeIgniter object
        $ci =& get_instance();
        $res =array();
        $res = $ci->Common_model->getRecords('email_setting','email',array('type' => 'notification'),'',true);
        return $res['email'];
    }
}

if (!function_exists('getParentAdminId')){
    function getParentAdminId($id){
        //get main CodeIgniter object
        $ci =& get_instance();
        $res =array();
        $res = $ci->Common_model->getRecords('admin','admin_id,parent_id',array('admin_id'=>$id),'',true);
        if($res['parent_id']>0){
            return $res['parent_id'];
        }
        return $id;
    }
}

if (!function_exists('getAdminIdfromTable')){
    function getAdminIdfromTable($table,$id){
        //get main CodeIgniter object
        $ci =& get_instance();
        $res =array();
        $res = $ci->Common_model->getRecords($table,'admin_id',array('id'=>$id),'',true);
        
        return $res['admin_id'];
    }
}

if (!function_exists('getSuperAdminDetail')){
    function getSuperAdminDetail(){
        //get main CodeIgniter object
        $ci =& get_instance();
        $res =array();
        $res = $ci->Common_model->getRecords('admin','email,admin_id,fullname',array('user_type'=>'Super Admin'),'',true);
        return $res;
    }
}
if (!function_exists('getActualId')){
    function getActualId($table){
        //get main CodeIgniter object
        $ci =& get_instance();
        $res =array();
        $loggedin_user_id=$ci->session->userdata('admin_id');
        
        $res = $ci->Common_model->getRecords('admin','admin_id,parent_id',array('admin_id'=>$loggedin_user_id),'',true);
        if($res['parent_id']>0){
            $admin_id=$res['parent_id'];
        }else{
            $admin_id=$loggedin_user_id;
        }
        $details = $ci->Common_model->getRecords($table,'id',array('admin_id'=>$admin_id),'',true);
        return  $details['id'];
        
    }
}

if (!function_exists('actionLog')) {
        function actionLog($table_name,$record_id,$action,$description,$perform_by,$performer_id) {
            $ci =& get_instance();
            $insert_data = array(
                'table_name'    => $table_name,
                'record_id'     => $record_id,
                'action'        => $action,
                'description'   => $description,
                'perform_by'    => $perform_by,
                'performer_id'  => $performer_id,
                'ip_address'    => $ci->input->ip_address(),
                'created'=>date('Y-m-d H:i:s')
            );
            
            if($ci->Common_model->addEditRecords('action_log',$insert_data)) {
                return true;
            } else {
                return false;
            }            
        } //end send notification on android
    }
    


if (!function_exists('getNameEmailAddress')){
    function getNameEmailAddress($id){
        //get main CodeIgniter object
        $ci =& get_instance();
        $res =array();
        $res = $ci->Common_model->getRecords('users','email,fullname,user_type,user_id,mobile,staging_id,status,is_deleted',array('user_id' => $id),'',true);
        return $res;
    }
}
if (!function_exists('getDeletePath')){
    function getDeletePath($path) {
        $filename = explode("/",$path);
        $total =count($filename);
        if($total==5){
            $result = $filename[$total-2]."/".$filename[$total-1]; 
        }else{
            $result = $filename[$total-1];
        }
       

        return $result;
    }
}
if (!function_exists('getStatesList')){
    function getStatesList($country_id){
        //get main CodeIgniter object
        $ci =& get_instance();
        $res =array();
        if($country_id) {
            $res = $ci->Common_model->getRecords('states','id,name',array('country_id'=>$country_id),'',false);
        }
        return $res;
    }
}

if (!function_exists('getCitiesList')){
    function getCitiesList($state_id){
        //get main CodeIgniter object
        $ci =& get_instance();
        $res =array();
        if($state_id) {
           $res = $ci->Common_model->getRecords('cities','id,name',array('state_id'=>$state_id),'',false);
        }
        return $res;
    }
}


if (!function_exists('commonImageUpload')){
    function commonImageUpload($upload_path,$allowed_types,$file,$width,$height) 
    {
        $ci =& get_instance();
        $config['upload_path'] = $upload_path;
        $config['allowed_types'] = $allowed_types;
        $config['encrypt_name'] = TRUE;
        $ci->load->library('upload', $config);
        $ci->upload->initialize($config);
        
        if (!$ci->upload->do_upload($file)) {
            return array('status'=>0,'msg'=>$ci->upload->display_errors("<p class='inputerror'>","</p>"));        
        } else {
            $upload_data=$ci->upload->data();
           
            $img=$upload_data['file_name'];
            // if($upload_data['file_type']!='image/svg+xml'){
            //     $img = uniqid(time()).$upload_data['file_ext'];
            //     $config['image_library'] = 'gd2';
            //     $config['source_image'] = $upload_data['full_path'];
            //     $config['new_image'] = $upload_path.$img;
            //     $config['quality'] = 100;
            //     $config['maintain_ratio'] = FALSE;
            //     $config['width']         = $width;
            //     $config['height']       = $height;

            //     $ci->load->library('image_lib', $config);

            //     $ci->image_lib->resize();
            //     $ci->image_lib->clear();
            //     unlink($upload_data['full_path']);
            // }
            return array('status'=>1,'image_path'=>$upload_path.$img);
        }
    }
}

if (!function_exists('commonDocumentUpload')){
    function commonDocumentUpload($upload_path,$allowed_types,$file,$width,$height) 
    {
        $ci =& get_instance();
        $config['upload_path'] = $upload_path;
        $config['allowed_types'] = $allowed_types;
        $new_name = uniqid(time()).$_FILES["image"]['name'];
        $config['file_name'] = $new_name;
        $ci->load->library('upload', $config);
        $ci->upload->initialize($config);
        
        if (!$ci->upload->do_upload($file)) {
            return array('status'=>0,'msg'=>$ci->upload->display_errors("<p class='inputerror'>","</p>"));        
        } else {
            $upload_data=$ci->upload->data();
            $img=$upload_data['file_name'];
            // if($upload_data['file_type']!='application/pdf'){
            if($upload_data['file_ext']!='.pdf' && $upload_data['file_ext']!='.docx' && $upload_data['file_ext']!='.doc'){
                $img = uniqid(time()).$upload_data['file_ext'];
                $config['image_library'] = 'gd2';
                $config['source_image'] = $upload_data['full_path'];
                $config['new_image'] = $upload_path.$img;
                $config['quality'] = 100;
                $config['maintain_ratio'] = FALSE;
                $config['width']         = $width;
                $config['height']       = $height;

                $ci->load->library('image_lib', $config);

                $ci->image_lib->resize();
                $ci->image_lib->clear();
                unlink($upload_data['full_path']);
            }
            return array('status'=>1,'image_path'=>$upload_path.$img);
        }
    }
}
if (!function_exists('multiImageUpload')){
    function multiImageUpload($upload_path,$allowed_types,$file,$width,$height,$filename) 
    {
        $ci =& get_instance();

        $config['upload_path'] = $upload_path;
        $config['allowed_types'] = $allowed_types;
        $config['encrypt_name'] = TRUE;
        // $new_name = uniqid(time()).$filename;
        // $config['file_name'] = $new_name;
        $ci->load->library('upload', $config); 
        $ci->upload->initialize($config);
        $ci->load->library('image_lib');
        
        if (!$ci->upload->do_upload($file)) {
            //echo $ci->upload->display_errors(); exit;
            return array('status'=>0,'msg'=>$ci->upload->display_errors("<p class='inputerror'>","</p>"));        
        } else {
            $upload_data=$ci->upload->data();
            $img =$upload_data['file_name'];
            compress($upload_path.$img);
            /*if($upload_data['file_ext']!='.pdf' && $upload_data['file_ext']!='.docx' && $upload_data['file_ext']!='.doc'){
                $img = $filename;
                //$img = uniqid(time()).$upload_data['file_ext'];
                $config['image_library'] = 'gd2';
                $config['source_image'] = $upload_data['full_path'];
                $config['new_image'] = $upload_path.$img;
                $config['quality'] = 100;
                $config['maintain_ratio'] = TRUE;
                $config['width']         = $width;
                $config['height']       = $height;

                $ci->image_lib->initialize($config);
                $ci->image_lib->resize();
                $ci->image_lib->clear();
                unlink($upload_data['full_path']);
            }*/
            return array('status'=>1,'image_path'=>$upload_path.$img);
        }
    }
}


if (!function_exists('getProfilePicture')){
    function getProfilePicture(){
        $ci =& get_instance();
        $res=array();
        $id = $ci->session->userdata('user_id');
        $res = $ci->Common_model->getRecords('users','profile_image,user_type', array('user_id'=>$id), '', true);
        if(!empty($res['profile_image'])){
            return $res['profile_image'];
        }else {
            return $res['profile_image']='assets/front/images/person.jpg';
        }
    }
}
if (!function_exists('getSchoolLogo')){
    function getSchoolLogo(){
        $ci =& get_instance();
        $res=array();
        $school_id = $ci->session->userdata('school_id');
        $res = $ci->Common_model->getRecords('school','logo', array('id'=>$school_id), '', true);
        if(!empty($res['logo'])){
            return $res['logo'];
        }else {
            return $res['logo']='assets/front/images/phl_school.svg';
        }
    }
}


if (!function_exists('getUserInfo')){
    function getUserInfo($id,$table,$match_col,$find_col){
        //get main CodeIgniter object
        $ci =& get_instance();
        $res=array();
        if($res = $ci->Common_model->getRecords($table, $find_col, array($match_col=>$id), '', true)) {
           return $res[$find_col];
        } else {
            return 0;
        }
    }
}



if (!function_exists('user_username')){
    function user_username($id,$username){
        //get main CodeIgniter object
        $ci =& get_instance();
        if($ci->Common_model->getRecords('users', 'user_id', array('user_id!='=>$id,'username'=>$username), '', true)) {
            return 1;
        } else {
            return 0;
        }
    }
}
if (!function_exists('user_details')){
    function user_details($id){
        //get main CodeIgniter object
        $res=array();
        $ci =& get_instance();
        if($res=$ci->Common_model->getRecords('users', 'first_name,last_name,display_name', array('user_id'=>$id), '', true)) {
            return $res;
        } else {
            return 0;
        }
    }
}



if (!function_exists('getCommaName')){
    function getCommaName($table_name,$ids,$get_column_name,$compare_column_name){
        //get main CodeIgniter object
        $ci =& get_instance();
    
        $record_array = array();
         $ids = explode(',', $ids);
         foreach ($ids as $key => $id) {
            $record =  $ci->Common_model->getRecords($table_name,"$get_column_name",array($compare_column_name=>$id,'is_deleted'=>0),'',true);
            if(!empty($record))
            { 
                $record_array[$key] = ucfirst($record[$get_column_name]);
            }
         }
         // echo "<pre>";print_r($record_array);die;
         if(!empty($record_array))
         {
            return implode(',',$record_array);      
         }else
         {
            return $record_array;
         } 
    }
}


if (!function_exists('menu_teacher_by_grade_web')){
    function menu_teacher_by_grade_web(){
        $ci =& get_instance();
        $user_id = $ci->session->userdata('user_id');
        if($user_id) {
            $result=array();
            $result_grade['grade']=array();
            if($teacher_grade_details=$ci->Common_model->getUniqueGrade($user_id)){
                foreach ($teacher_grade_details as $key => $details) {
                   
                    $teacherInfo=$ci->Common_model->getTeacherDetailsByGrade($details['teacher_grade_id']);
                    if($teacherInfo){
                        $result_grade['grade'][$key]['grade_name']=DISPLAY_GRADE_NAME[$details['grade']];
                        $result_grade['grade'][$key]['grade_id'] = $details['grade'];
                        $result_grade['grade'][$key]['teacher'] = $teacherInfo;
                    }
                    $result=$result_grade;
                }
            }
            if(!empty($result)){
               return $result;
            }else {
               return false;
            }
        }  
    }
}

if (!function_exists('check_permission')){
    function check_permission($section_id,$action,$redirect=''){
       
        $ci =& get_instance(); 
        $user_id = $ci->session->userdata('admin_id');
    

        if($admin_role_id = $ci->Common_model->getRecords('admin', 'role_id,user_type', array('admin_id='=>$user_id), '', true)) {
            if($a=$ci->Common_model->getRecords('role_permissions', '*', array('role_id='=>$admin_role_id['role_id'],'section_id'=>$section_id,$action=>'1'), '', true)) { 
           
                return true;
            }else{
              
                if($admin_role_id['user_type']!='Super Admin')
                {
                    if(!empty($redirect))
                    { 
                        redirect(base_url().'admin/not_authorized');
                    }else{
                        return false;
                    }   
                }else{
                    return true;
                }
            }  
        }  
    }
}


function display_output($status,$msg,$data=array()){
    $response =  array('status'=>$status,'msg'=>$msg);
    if(!empty($data)){
       $response= array_merge($response,$data);
    }
    echo json_encode($response); 
    exit;
}
function getroles($parent_id = 0, $prefix='',$count=0,$select='',$full='')
{ 
    $ci =& get_instance();
    if($parent_id!=0){
        $prefix++;  
    }
    $type = $ci->session->userdata('user_type');
    if($type=='Super Admin'){
        $type='Admin';
    }

    $roles = $ci->Common_model->getRecords('roles','*',array('parent_id'=>$parent_id,'hide'=>'0','type'=>$type,'status'=>'Active'),'parent_id ASC',false);
    $role = '';
    $testing = 0;
    $countt= count($roles);
    if(count($roles) > 0) {
        foreach ($roles as $row) {
                  
            if($select==$row['role_id'])
            {
                $selected= 'selected';
            }else{
                $selected= '';
            }
            if($prefix < 2 || $full!=''){
                $role .= "<option ".$selected." value=".$row['role_id'].">  ";
                for($i=1;$i <=$prefix ;$i++){
                    $role .= " - ";
                    $count++;
                }
                $role .= ucfirst($row['name']);
                $role .= getroles($row['role_id'],$prefix,$count,$select,$full);
                $role .= "</option>";
            
            }
        }
    }
    return $role;
} 

if (!function_exists('filterData')){
 function filterData(&$str)
    {
        $str = preg_replace("/\t/", "\\t", $str);
        $str = preg_replace("/\r?\n/", "\\n", $str);
        if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
    }
}

if (!function_exists('get_sub_role_id')){  
    function get_sub_role_id($parent_id = 0,$prefix=0)
    {
        $ci =& get_instance();
        $test='';
        if($parent_id!=0){
            $prefix++;  
        }
        $categories = $ci->Common_model->getRecords('roles','*',array('parent_id'=>$parent_id,'status'=>'Active','hide'=>'0'),'parent_id ASC',false); 
        if(count($categories) > 0) {
            foreach ($categories as $row) {
                $test .=','.$row['role_id'];    
                $test .=   get_sub_role_id($row['role_id'],$prefix); 
            }
        } 
        return $test;
    }
}


if (!function_exists('get_sidemenu')){
    function get_sidemenu()
    {
        $ci =& get_instance(); 
        $type = $ci->session->userdata('user_type');
        
        // $type='Admin';
        $menuarray=array();
        if($type=='Super Admin'){

            if($type=='Super Admin'){
                $type='Admin';
            }

            $sections = $ci->Common_model->getRecords('sections','*',array('type'=>$type,'status'=>'Active'),'',false); 
            if(count($sections) > 0) {
                $parentarr = array_filter($sections, function ($var) {
                    return ($var['parent_id'] == '0');
                });
                $keys = array_column($parentarr, 'sort_order');
                array_multisort($keys, SORT_ASC, $parentarr);

                foreach ($parentarr as $key => $value) 
                {
                    array_push($menuarray,$value);
                    $submenu = array_filter($sections, function ($var) use ($value) {
                        return ($var['parent_id'] == $value['id']);
                    });
                    $subkeys = array_column($submenu, 'sort_order');

                    array_multisort($subkeys, SORT_ASC, $submenu);
                    $menuarray[$key]['children']=$submenu;
                }
                
            } 
        }else{
            if($type=='Admin'){
                $check_section_id=1;
            }

            $role_id = $ci->session->userdata('role_id');
            if(!$ci->Common_model->getRecords('role_permissions','view',array('role_id'=>$role_id,'section_id'=>$check_section_id,'view'=>1),'',true)){
                $sections1[]=array('id' => 253,
                            'role_id' =>  $role_id,
                            'section_id' => 1,
                            'add' => 1,
                            'edit' => 1,
                            'delete' => 1,
                            'view' => 1,
                            'created' => date('Y-m-d H:i:s'),
                            'modified' => date('Y-m-d H:i:s'),
                            'name' => 'Dashboard',
                            'parent_id' => 0,
                            'icon' => 'fa fa-dashboard',
                            'link' => 'admin/dashboard');
            }
            $ci->db->select('i.*,s.id as section_id,s.name,s.parent_id,s.icon,s.link');
            $ci->db->from('sections s');
            $ci->db->join('role_permissions i','s.id = i.section_id','left');       
            $ci->db->where('i.role_id',$role_id);
            $ci->db->where('i.view',1);
            $ci->db->where('s.type',$type);
            $ci->db->where('s.parent_id',0);
            $ci->db->where('s.status','Active');
            $ci->db->order_by('s.sort_order');
            $query = $ci->db->get();

            $sections= $query->result_array();
            if(isset($sections1) && !empty($sections1)){
              $sections=array_merge($sections1,$sections);  
            }
          
            foreach ($sections as $key => $value) {
                array_push($menuarray,$value);
                $ci->db->select('i.*,s.id as section_id,s.name,s.parent_id,s.icon,s.link');
                $ci->db->from('sections s');
                $ci->db->join('role_permissions i','s.id = i.section_id','left'); 
                $ci->db->where('i.role_id',$role_id);
                $ci->db->where('i.view',1);
                $ci->db->where('s.type',$type);
                $ci->db->where('s.status','Active');
                $ci->db->where('s.parent_id',$value['section_id']);
                $ci->db->order_by('s.sort_order');
                $query = $ci->db->get();
                $subsections= $query->result_array();

                if(!empty($subsections)){
                    
                    $menuarray[$key]['children']=$subsections;
                    
                }
            }
        }
       /*echo "<pre>";
         print_r($menuarray);
        die;*/
        return $menuarray;
    }
}

if (!function_exists('configAjaxPagination')){
    function configAjaxPagination($total_records,$b_url,$selector,$per_page,$function) {
        $config['target']      = $selector;
        $config['base_url']    = $b_url;
        $config['total_rows']  = $total_records;
        $config['per_page']    = $per_page;
        $config['function']    = $function;
        $config["uri_segment"] = 4;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['cur_tag_open'] = "<li class='active'><a href='javascript:void(0);'>";
        $config['cur_tag_close'] = '</a></li>';
        return $config;
        //$ci->ajax_pagination->initialize($config);
    }
}

if (!function_exists('configFrontAjaxPagination')){
    function configFrontAjaxPagination($total_records,$b_url,$selector,$per_page,$function,$seg='') {
        $config['target']      = $selector;
        $config['base_url']    = $b_url;
        $config['total_rows']  = $total_records;
        $config['per_page']    = $per_page;
        $config['function']    = $function;
        $config["uri_segment"] = $seg?$seg:2;
        $config['num_tag_open'] = '<span class="num_tag_open">';
        $config['num_tag_close'] = '</span>';
        $config['first_tag_open'] = '<span class="first_tag_open">';
        $config['first_tag_close'] = '</span>';
        $config['last_tag_open'] = '<span class="first_tag_open">';
        $config['last_tag_close'] = '</span>';
        $config['full_tag_open'] = '<div class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</div>';
        $config['cur_tag_open'] = "<span><a class='paginate_button current' href='javascript:void(0);'>";
        $config['cur_tag_close'] = '</a></span>';
        return $config;
        //$this->ajax_pagination->initialize($config);
    }
}

if (!function_exists('getSetting')){
    function getSetting($table,$field) {
        $ci =& get_instance();
        $getInfo = array();
        $getInfo = $ci->Common_model->getRecords($table,$field,'','',true);
        return $getInfo[$field];
    }
}

// Function to create slug
if (!function_exists('create_slug_with_id')) {
    function create_slug_with_id($str,$id) {
        $str = trim($str);
        $encode_key = "57";
        $id = base64_encode($encode_key."_".$id);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $str);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", '-', $clean);
        $clean = $clean."-".$id;
        return $clean;
    }
}

// Function to create slug
if (!function_exists('create_slug')){
    function create_slug($str) {
        $str = trim($str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $str);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", '-', $clean);
        return $clean;
    }
}

if (!function_exists('get_timeago')){
    function get_timeago($ptime)
    {
    
        $estimate_time = time() - $ptime;

        if( $estimate_time < 1 )
        {
            return 'less than 1 second ago';
        }
        $condition = array( 
                    12 * 30 * 24 * 60 * 60  =>  'year',
                    30 * 24 * 60 * 60       =>  'month',
                    24 * 60 * 60            =>  'day',
                    60 * 60                 =>  'hour',
                    60                      =>  'minute',
                    1                       =>  'second'
        );

        foreach( $condition as $secs => $str )
        {
            $d = $estimate_time / $secs;

            if( $d >= 1 )
            {
                $r = round( $d );
                return '' . $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
            }
        }
    }

}
if (!function_exists('shortNumber')){
    function shortNumber($num){
        $units = ['', 'K', 'M', 'B', 'T', 'Q', 'S'];
        for ($i = 0; $num >= 1000; $i++) {
            $num /= 1000;
        }
        return round($num, 1) . $units[$i];
    }
}
function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ' : '';
}







if (!function_exists('androidNotification')) {
    function androidNotification($deviceToken,$message_arr='') 
    {
        if(!empty($deviceToken))
        {   
            $deviceToken = array($deviceToken);
            $andarray = array(
                'msg' => $message_arr
            );
           
           // $url = 'https://android.googleapis.com/gcm/send'; 
            $url = 'https://fcm.googleapis.com/fcm/send';

            $fields = array(
                'registration_ids' => $deviceToken,
               
                //'data' => $message_arr
                'data' => $andarray
            );
            /*$headers = array(
                'Authorization: key=AAAA4OwNvnk:APA91bGNxzUbIIeOIMYvY4su_gTptZtFg4sxdqN7EgyEIG_dLc1L0nmWiiLJIesHIz210POz087oGXMGpNyfSSCGwZbGhMkRe44_XYlS7oXeA50WlIAzrci4EuMZk7AYvW6KI9JmKckc',
                'Content-Type: application/json'
            );*/
            $headers = array(
                'Authorization: key=AAAA8iugTL8:APA91bFAlCxuHnijyQ3n-iL6RUD_DJLeNjZIFHNUzFjN2vjzBADBv0pYoJ0J7nMVj616-XdJ5rd8cabCVGpYZsYQ_65wh5tWTiSclxvvxy_st_FG1ygCZKYL2UKj_PJA4c8cEHjHN2Hl',
                'Content-Type: application/json'
            );
            // Open connection
            $ch = curl_init();
            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Execute post
            $result = curl_exec($ch);
           
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }
            // Close connection
            curl_close($ch);
            json_encode($result);
            return true;
        }else{
            return false;
        }
        
    } //end send notification on android
}
if (!function_exists('getCitiesList')){
    function getCitiesList($state_id){
        //get main CodeIgniter object
        $ci =& get_instance();
        $res =array();
        if($state_id) {
           $res = $ci->Common_model->getRecords('us_cities','id,title',array('state_id'=>$state_id),'',false);
        }
        return $res;
    }
}
if (!function_exists('convertFromHoursToMinutes')){
function convertFromHoursToMinutes($string)
    {
        //echo $string;
        // Separate hours from minutes
        $split = explode(':', $string);
      // print_r($split);
        // Transform hours into minutes
        $hoursToMinutes = $split[0] * 60;

        $total = $hoursToMinutes + (int)$split[1];

        return $total;
    }
}
if (!function_exists('DayCount')){
    function DayCount($day, $start, $end)
    {        
        //get the day of the week for start and end dates (0-6)
        $w = array(date('w', $start), date('w', $end));

        //get partial week day count
        if ($w[0] < $w[1])
        {            
            $partialWeekCount = ($day >= $w[0] && $day <= $w[1]);
        }else if ($w[0] == $w[1])
        {
            $partialWeekCount = $w[0] == $day;
        }else
        {
            $partialWeekCount = ($day >= $w[0] || $day <= $w[1]);
        }

        //first count the number of complete weeks, then add 1 if $day falls in a partial week.
        return floor( ( $end-$start )/60/60/24/7) + $partialWeekCount;
    }
}
if (!function_exists('hoursRange')){
    function hoursRange( $lower = 0, $upper = 86400, $step = 3600, $format = '' ) {
        $times = array();

        if ( empty( $format ) ) {
            $format = 'H:i';
        }

        foreach ( range( $lower, $upper, $step ) as $increment ) {
            $increment = gmdate( 'H:i', $increment );

            list( $hour, $minutes ) = explode( ':', $increment );

            $date = new DateTime( $hour . ':' . $minutes );

            $times[(string) $increment] = $date->format( $format );
        }

        return $times;
    }
}

 if (!function_exists('getallheaders')) {
    function getallheaders() {
    $headers = [];
    foreach ($_SERVER as $name => $value) {
        if (substr($name, 0, 5) == 'HTTP_') {
            $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
        }
    }
    return $headers;
    }
}
function get_time_posted($date1,$date2)
{
    $diff = abs(strtotime($date2) - strtotime($date1)); 

    $years   = floor($diff / (365*60*60*24)); 
    $months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24)); 
    $days    = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

    $hours   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60)); 

    $minuts  = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60); 

    $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60)); 

    if($days>0){
        return $days.'d';
    }else if($hours>0){
        return $hours.'h';
    }else if($minuts>0){
        return $minuts.'m';
    }else{
        return 'Just now';
    }
}
function get_time_diffrence($date1,$date2,$type)
{
    $diff = abs(strtotime($date2) - strtotime($date1)); 

    $years   = floor($diff / (365*60*60*24)); 
    $months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24)); 
    $days    = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

    $hours   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60)); 

    $minuts  = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60); 

    $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60)); 
    switch ($type) {
        case 'years':
            return $years;
            break;
        case 'months':
            return $months;
            break;
        case 'days':
            return $days;
            break;
        case 'hours':
            return $hours;
            break;
        case 'minuts':
            return $minuts;
            break;
        case 'seconds':
            return $seconds;
            break;
    }
}

function get_number_to_albhabet($number)
{
    $alpha='';
    foreach(explode('-', $number) as $num){
        $list=array('A' => 0,'B' => 1,'C' => 2,'D' => 3,'E' => 4,'F' => 5,'G' => 6,'H' => 7,'I' => 8,'J' => 9);
        
        $arr_num=str_split ($num);
        foreach($arr_num as $data)
        {
            $alpha.=array_search($data,$list);
        }
        $alpha.="-";
    }
    return rtrim($alpha,'-');
}

function get_albhabet_to_number($modal){
    $numb='';
    foreach(explode('-', $modal) as $str){
        $list=array(0 =>'A',1 => 'B',2 => 'C',3 => 'D',4 => 'E',5 => 'F',6 => 'G',7 => 'H',8 => 'I',9 => 'J');
        
        $arr_num=str_split ($str);
        foreach($arr_num as $data)
        {
            $numb.=array_search($data,$list);
        }
        $numb.="-";
    }
    return rtrim($numb,'-');

}

function get_albhabet_to_number_mask($modal){
    $numb='';
    foreach(explode('-', $modal) as $str){
        $list=array(0 =>'A',1 => 'B',2 => 'C',3 => 'D',4 => 'E',5 => 'F',6 => 'G',7 => 'H',8 => 'I',9 => 'J');
        
        $arr_num=str_split ($str);
        foreach($arr_num as $data)
        {
            $numb.=array_search($data,$list);
        }
        $numb.="-";
    }
    $numb=rtrim($numb,'-');
    return '***-**-'.substr($numb, -4);

}
//to get distance using latlong
function calculate_distance($lat1, $lon1, $lat2, $lon2, $unit='M') {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  $result=0;
  if ($unit == "K") {
      $result= ($miles * 1.609344);
  } else if ($unit == "N") {
      $result= ($miles * 0.8684);
  } else {
      $result= $miles;
  }
  return round($result,2);
}

function isDate($string) {
    $today=date('Y-m-d' ,strtotime("+ 1 day"));
    $check_date=date('Y-m-d',strtotime(str_replace('/','-', $string)));
    $matches = array();
    $pattern = '/^([0-9]{1,2})\\/([0-9]{1,2})\\/([0-9]{4})$/';
   // $pattern = '/^([0-9]{1,2})\\/([0-9]{1,2})\\/([0-9]{4})$/';
    if (!preg_match($pattern, $string, $matches)) return false;
    if (!checkdate($matches[2], $matches[1], $matches[3])) return false;
    if(strtotime($today) >= strtotime($check_date))return false;
    return true;
}

 if (!function_exists('convertGMTToLocalTimezone')) {
    function convertGMTToLocalTimezone($gmttime,$showTime=FALSE,$only_time=FALSE,$timezoneRequired='',$fulltime=FALSE)
    {
        $ci =& get_instance();
        $system_timezone = date_default_timezone_get();
        date_default_timezone_set("GMT");
        $gmt = date("Y-m-d h:i:s A");
        if($admin_timezone = $ci->session->userdata('admin_timezone')) {
            $local_timezone = $admin_timezone;
        }elseif($front_timezone=$ci->session->userdata('user_timezone')){
            $local_timezone = $front_timezone;
        }elseif($timezoneRequired){
            $local_timezone =$timezoneRequired;
        }
        else {
           $local_timezone = 'America/Toronto'; 
        }
        //echo $admin_timezone;exit;
        date_default_timezone_set($local_timezone);
        $local = date("Y-m-d h:i:s A");

        date_default_timezone_set($system_timezone);
        $diff = (strtotime($local) - strtotime($gmt));
        
        $date = new DateTime($gmttime);
        $date->modify("+$diff seconds");
        if($only_time==FALSE){
             if ($showTime==FALSE) {
                $timestamp = $date->format(DATETIMEFORMATE);
            }else{
                $timestamp = $date->format(DATETIMEFORMATE." h:i A");
            }
        }else{
               $timestamp = $date->format("h:i A");
        }
        if($fulltime == TRUE){
            $timestamp = $date->format("m-d-Y • h:i A");
        }
       
        return $timestamp;
    }
}
if (!function_exists('convertLocalTimezoneToGMT')) {
    function convertLocalTimezoneToGMT($gmttime,$timezoneRequired,$showTime=FALSE,$only_time=FALSE)
    {
       //echo $timezoneRequired;exit;
        $system_timezone = date_default_timezone_get();
        $local_timezone = $timezoneRequired;
        date_default_timezone_set($local_timezone);
        $local = date("Y-m-d h:i:s A");
        date_default_timezone_set("GMT");
        $gmt = date("Y-m-d h:i:s A");
        date_default_timezone_set($system_timezone);
        $diff = (strtotime($gmt) - strtotime($local));
        $date = new DateTime($gmttime);
        $date->modify("+$diff seconds");
        if($only_time==FALSE){
            if ($showTime==FALSE) {
                $timestamp = $date->format(DATETIMEFORMATE);
            }else{
                $timestamp = $date->format(DATETIMEFORMATE." h:i A");
            }
        }else{
               $timestamp = $date->format("h:i A");
        }
        return $timestamp;
    }
}

if (!function_exists('getAdminUsername')){
    function getAdminUsername($admin_id){
        //get main CodeIgniter object
        $ci =& get_instance();
        $res =array();
        $res = $ci->Common_model->getRecords('admin','username',array('admin_id' => $admin_id),'',true);
        return $res['username'];
    }
}

if (!function_exists('getFrontUsertype')){
    function getFrontUsertype($user_id){
        //get main CodeIgniter object
        $ci =& get_instance();
        $res =array();
        $res = $ci->Common_model->getRecords('users','user_type',array('user_id' => $user_id),'',true);
        return getUserType($res['user_type']);

        //return $res['user_type'];
    }
}

if (!function_exists('removeNotification_bkp')){
    function removeNotification_bkp($user_id,$type,$record_id,$is_child='') {
        $ci =& get_instance(); 
        if($is_child==1){
            $where = array(
                'created_by'=> $user_id,
                'notification_type'=>$type,
                'record_id'=>$record_id
            );  
        }else{
            $where = array(
                //'created_by'=> $user_id,
                'user_id'=> $user_id,
                'notification_type'=>$type,
                'record_id'=>$record_id
            );   
        } 
        
        $ci->Common_model->deleteRecords('notifications',$where);
        
    }
}

///////////////////////s3 code////////////////////////////////////
if (!function_exists('createImage')) {
    function createImage($srcfile,$tmp_path,$size,$duration,$mobile_type="web",$rotation=-1){
        
        $large = time().'_'.$size.".jpg";
        $large_image = $tmp_path.$large;
        $small = time()."_300x171.jpg";
        $small_image = $tmp_path.$small;
        $command =  'ffprobe -v quiet -print_format json -show_format -show_streams '.$srcfile;
        $media_data = json_decode(shell_exec($command));
        //echo '<br>';echo "Portrait <pre>";print_r($media_data);exit;
        $smallthumbPath = $tmp_path."small_thumb_".time().".jpg";
        $thumbPath = $tmp_path."thumb_rotate_".time().".jpg";
        $stretchPath = $tmp_path."stretched_thumb_".time().".jpg";
        $stretchPath1 = $tmp_path."stretched_thumb1_".time().".jpg";
        $bg_image = "assets/images/transparent_800x450.png";
        $cropped = $tmp_path."cropped".time().".jpg";
        
        if($duration >3) {
            $interval = 3;
        } else {
            $interval = 1;
        }
        $images = array();
        $rotate = 0;
        $transpose = 0;

        if($mobile_type=="android") {
            $rotate = $rotation;
        } else {
            foreach($media_data->streams as $list) {
                if(isset($list->tags->rotate) && !empty($list->tags->rotate)) {
                    $rotate = $list->tags->rotate;
                }
            }
        }
        if($rotate>0 && $rotate!=180) {
            if($mobile_type=="android") {
                if($rotate == 90 || $rotate == 270) {
                   $command = "ffmpeg -i $srcfile -ss $interval -vframes 1 $thumbPath"; 
                    shell_exec($command); 
                }
            } else {
                // if($rotate == 90) {
                //     $transpose = '-vf transpose=1';
                // } 
                // else if($rotate == 270) {
                //     $transpose = '-vf transpose=2';
                // }
                if($rotate == 90) {
                    $transpose = '';
                } 
                else if($rotate == 270) {
                    $transpose = '';
                }
                $command = "ffmpeg -i $srcfile -ss $interval -vframes 1 $transpose $thumbPath"; 
                shell_exec($command); 
            }
            // if($rotate == 90) {
            //     $transpose = '-vf transpose=1';
            // } 
            // else if($rotate == 270) {
            //     $transpose = '-vf transpose=3';
            // }
        
            // $command = "ffmpeg -i $srcfile -ss $interval -vframes 1 $transpose $thumbPath"; 
            // shell_exec($command); 
            $command1 = "ffmpeg -i $thumbPath -s '253x450' $smallthumbPath";
            shell_exec($command1);
            $command2 = "ffmpeg -i $thumbPath -filter:v 'crop=in_w:0.56*in_w' $cropped";
            shell_exec($command2);
            $command3 = "ffmpeg -i $cropped -s '800x450' $stretchPath";
            shell_exec($command3);
            $command4 = "ffmpeg -i $stretchPath -i $bg_image -filter_complex '[0:v][1:v]overlay=0:0' $stretchPath1";
            shell_exec($command4);
            $command5 = "ffmpeg -i $stretchPath1 -i $smallthumbPath -filter_complex '[0:v][1:v]overlay=main_w/2-overlay_w/2:main_h/2-overlay_h/2' $large_image";
            shell_exec($command5);
            $command6 = "ffmpeg -i $large_image -s '300x171' $small_image";
            shell_exec($command6);
            unlink($cropped);
            unlink($smallthumbPath);
            unlink($thumbPath);
            unlink($stretchPath);
            unlink($stretchPath1);
        } else {
            if($rotate == 180) {
                // $transpose = '-vf "vflip,hflip"';
                // $command = "ffmpeg -i $srcfile -an -ss $interval -vframes 1 $transpose -y -s '800x450' $large_image";
                // shell_exec($command); 
                // $command1 = "ffmpeg -i $large_image -s '300x171' $small_image";
                // shell_exec($command1);
                if($mobile_type=="android") {
                    $command = "ffmpeg -i $srcfile -an -ss $interval -vframes 1 -y -s '800x450' $large_image";
                    //exit;
                    shell_exec($command); 
                } else {
                    // $transpose = '-vf "vflip,hflip"';
                    // $command = "ffmpeg -i $srcfile -an -ss $interval -vframes 1 $transpose -y -s '800x450' $large_image";
                    $command = "ffmpeg -i $srcfile -an -ss $interval -f mjpeg -t 1 -r 1 -vframes 1 -y -s '800x450' $large_image";
                    shell_exec($command); 
                }
                $command1 = "ffmpeg -i $large_image -s '300x171' $small_image";
                shell_exec($command1);
            } else {
                $command = "ffmpeg -i $srcfile -an -ss $interval -f mjpeg -t 1 -r 1 -vframes 1 -y -s '800x450' $large_image";
                shell_exec($command); 
                $command1 = "ffmpeg -i $large_image -s '300x171' $small_image";
                shell_exec($command1);
            }
        }
        $images[0] = $large;
        $images[1] = $small; 
        $images[2] = $rotate; 
        return $images;
    }

    if (!function_exists('compress')){       
        function compress($source, $quality=80) {
            $info = getimagesize($source);
            if ($info['mime'] == 'image/jpeg') 
                $image = imagecreatefromjpeg($source);
            elseif ($info['mime'] == 'image/gif') 
                $image = imagecreatefromgif($source);
            elseif ($info['mime'] == 'image/png') 
                $image = imagecreatefrompng($source);
            $destination = $source;
            imagejpeg($image, $destination, $quality);
            return $destination;
        }
    }

    if (!function_exists('compress_other_path')){       
        function compress_other_path($source, $destination, $quality=80) {
            $info = getimagesize($source);
            if ($info['mime'] == 'image/jpeg') 
                $image = imagecreatefromjpeg($source);
            elseif ($info['mime'] == 'image/gif') 
                $image = imagecreatefromgif($source);
            elseif ($info['mime'] == 'image/png') 
                $image = imagecreatefrompng($source);
            
            imagejpeg($image, $destination, $quality);
            return $destination;
        }
    }

    if (!function_exists('check_server_unique')){
    function check_server_unique($table, $matched_col, $matched_value, $matched_id="", $matched_id_value="",$param)
    {
        $ci =& get_instance();
        if(isset($matched_col) && isset($table) && isset($matched_value) ) {
            if((isset($matched_id) && !empty($matched_id)) && isset($matched_id_value) && !empty($matched_id_value)) {
                
                $field = $matched_id;
                if($ci->Common_model->getRecords($table, $matched_col, array("$field!=" =>$matched_id_value,$matched_col=>$matched_value,'is_deleted'=>0), '', true)) {
                    echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">'.$param.' already exist.</div>')); exit;  
                }
            } else {
                if($ci->Common_model->getRecords($table, $matched_col, array($matched_col=>$matched_value,'is_deleted'=>0), '', true)) {
                    echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">'.$param.' already exist.</div>')); exit;      
                }
            }
        }
    }
}
if (!function_exists('front_check_server_unique')){
    function front_check_server_unique($table, $matched_col, $matched_value, $matched_id="", $matched_id_value="",$param)
    {
        $ci =& get_instance();
        if(isset($matched_col) && isset($table) && isset($matched_value) ) {
            if((isset($matched_id) && !empty($matched_id)) && isset($matched_id_value) && !empty($matched_id_value)) {
                
                $field = $matched_id;
                if($ci->Common_model->getRecords($table, $matched_col, array("$field!=" =>$matched_id_value,$matched_col=>$matched_value,'is_deleted'=>0), '', true)) {
                    display_output('0',$param.' already exist.'); 
                }
            } else {
                if($ci->Common_model->getRecords($table, $matched_col, array($matched_col=>$matched_value,'is_deleted'=>0), '', true)) {
                    display_output('0',$param.' already exist.');      
                }
            }
        }
    }
}

if (!function_exists('test_input')) {
   function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        //$data = htmlspecialchars($data);
        return $data;
    }
}

    
    if (!function_exists('generateRandomString')) {
        function generateRandomString($length = 12) {
            $characters = '0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHIJKMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }
    }



    if (!function_exists('getTotalCount')) {
        function getTotalCount($table,$col,$cond){
            $table = $table;
            $col = $col;
            $cond = $cond;
            $ci =& get_instance();
            $res = $ci->Common_model->getNumRecords($table,$col,$cond,'',true);
            return $res;
        }
    }

  
    if (!function_exists('countNotification')) {
        function countNotification() {
        $ci =& get_instance(); 
        $user_id = $ci->session->userdata('user_id'); 
        $ci->db->select('n.notification_id'); 
        $ci->db->from('notifications n'); 
        $ci->db->where('n.user_id',$user_id); 
        $ci->db->where('n.is_read',0);   
        $query=$ci->db->get();    
        return $query->num_rows();    
        }
    }
    if (!function_exists('checkFollowStatus')){
        function checkFollowStatus($follow_to,$followed_by=''){
            //get main CodeIgniter object
            $ci =& get_instance();
            $res =array();
            if(!$followed_by){
                $follow_to=base64_decode($follow_to);
                $followed_by=$ci->session->userdata('user_id');
            }
            
            $where=array('follow_to'=>$follow_to,'followed_by'=>$followed_by);
            if($ci->Common_model->getRecords('follow_users','id',$where,'',true)){
                return 1; 
            }else{
                return 0;
            }
            
        }
    }
    if (!function_exists('convert_duration')){
        function convert_duration($sec) {
            $sec= (double)$sec;
            $s = $sec%60;
            $m = floor(($sec%3600)/60);
            $h = floor(($sec%86400)/3600);
            $duration = "$h:$m:$s";
            return date("H:i:s",strtotime($duration));
        }
    }
    if (!function_exists('getUserName')){
        function getUserName($user_id=""){
            //get main CodeIgniter object
            $ci =& get_instance();
            $res =array();
            if(!isset($user_id) || empty($user_id)){
                $user_id=$ci->session->userdata('user_id');
            }
            $res = $ci->Common_model->getRecords('users',"group_concat(first_name,' ',last_name) as fullname,display_name",array('user_id' => $user_id),'',true);
            if(!empty($res['display_name'])){
                return ucfirst($res['display_name']);
            }else{
                return ucfirst($res['fullname']);
            }
            
        }
    }
    if (!function_exists('addNotification')) {
    function addNotification ($created_by,$to_userid,$type,$message,$record_id,$main_media_id,$not_type='all') {
        if(SEND_NOTIFICATION) {
            $ci =& get_instance();  
            $send_notification=true;
            if($not_type!='all'){
                if($not_type=='video'){
                    $send_notification=false;
                    if($ci->Common_model->getRecords('users', 'unread_notification', array('user_id'=>$to_userid,'notification_flag'=>0), '', true)){
                        $send_notification=true;
                    }
                }
                if($not_type=='comment'){
                    $send_notification=false;
                    if($ci->Common_model->getRecords('users', 'unread_notification', array('user_id'=>$to_userid,'notification_flag_comment'=>0), '', true)){
                        $send_notification=true;
                    }
                }
            }
            if($send_notification){
                $date= date('Y-m-d H:i:s');
                $insert_data = array(
                    'created_by'=>  $created_by,
                    'to_user'=> $to_userid, 
                    'type'=>$type,
                    'message'=>$message,
                    'record_id'=>$record_id,
                    'main_media_id'=>$main_media_id,
                    'created_datetime'=>$date,
                );
                $ci =& get_instance();      
                $notification = $ci->Common_model->addEditRecords('notification',$insert_data);
                if(!$notification) {
                    $err = array('data'=> array('status'=>'0','message'=>'Some error occured!!'));
                    echo json_encode($err);
                } else {
                    if($to_userid > 0) {
                        if($res = $ci->Common_model->getRecords('users', 'unread_notification', array('user_id'=>$to_userid), '', true)) {
                            $total_notiication = $res['unread_notification']+1;
                            $ci->Common_model->addEditRecords('users',array('unread_notification'=>$total_notiication),array('user_id'=>$to_userid));
                        }
                    } 
                }
            }
        }
    }
}

if (!function_exists('removeNotification')){
    function removeNotification ($created_by,$type,$record_id,$main_media_id) {
        $ci =& get_instance();    
        $where = array(
            'created_by'=> $created_by,
            'type'=>$type,
            'record_id'=>$record_id,
            'main_media_id'=>$main_media_id,
        ); 

        // if($notification = $ci->Common_model->getRecords('notification','to_user',$where,'', false)) {
        //     //echo "<pre>";print_r($notification);exit;'`set_row`- 1'
        //     foreach($notification as $list) {
        //         if($unread_notification = $ci->Common_model->getFieldValue('users','unread_notification',array('user_id'=>$list['to_user']))) {
        //             //echo $unread_notification;
        //             $unread_notification = $unread_notification-1;
        //             $ci->Common_model->addEditRecords('users',array('unread_notification'=>$unread_notification),array('user_id'=>$list['to_user']));
        //             //echo $ci->db->last_query();exit;
        //         }
        //     }
        // }
        $update_data = array(
            'is_deleted'=> 1,
            'deleted_datetime'=> date('Y-m-d H:i:s'),
        );
        $ci->Common_model->addEditRecords('notification',$update_data,$where);
        $ci->db->last_query();
    }
}




if (!function_exists('removeNotificationCount')){
    function removeNotificationCount ($user_id,$created_by,$type,$record_id,$main_media_id) {
        $ci =& get_instance();   
        // $where = array('user_id'=> $user_id); 
        // if($unread_notification = $ci->Common_model->getFieldValue('users','unread_notification',$where)) {
        //     //echo $unread_notification;
        //     $unread_notification = $unread_notification-1;
        //     $ci->Common_model->addEditRecords('users',array('unread_notification'=>$unread_notification),$where);
        //     //echo $ci->db->last_query();exit;
        // }

        $nwhere = array(
            'created_by'=> $created_by,'to_user'=> $user_id,'type'=>$type,'read'=>'0','record_id'=>$record_id,'main_media_id'=>$main_media_id);

        //$is_read = $ci->Common_model->getFieldValue('notification','read',$nwhere);
        $read_count = $ci->Common_model->getRecords('notification','read',$nwhere,'',true);
       // echo '1H'.$ci->db->last_query();
        if(!empty($read_count)){
            $is_read=$read_count['read'];
        }else{
            $is_read=0;
        }
        
        if($is_read=='0') {  
            //echo "if";
            $where = array('user_id'=> $user_id); 
            $unread_notification = $ci->Common_model->getRecords('users','unread_notification',$where,'',true);
           
           
            if(!empty($unread_notification['unread_notification'])) {
                
                //$unread_notification['unread_notification']=$unread_notification;
               // echo $ci->db->last_query();exit;
                //echo $unread_notification;
                $unread_notification = $unread_notification['unread_notification']-1;
                $ci->Common_model->addEditRecords('users',array('unread_notification'=>$unread_notification),$where);
                //echo '3 H'.$ci->db->last_query();exit;
            }
        } 
        // else {
        //     echo "else";exit;
        // }
        
    }
}
if (!function_exists('sendNotification')) {
    function sendNotification($user_id,$message,$not_type='all') {
        if(SEND_NOTIFICATION) {
            $ci =& get_instance(); 
            $send_notification=false;
            if($not_type!='all'){
                if($not_type=='video'){
                    $send_notification=false;
                    if($device_detail = $ci->Common_model->getRecords('users','device_id,device_type',array('user_id' =>$user_id,'notification_flag'=>0,'is_deleted'=>0),'', false)) {
                        $send_notification=true;
                    }
                }
                if($not_type=='comment'){
                    $send_notification=false;
                    if($device_detail = $ci->Common_model->getRecords('users','device_id,device_type',array('user_id' =>$user_id,'notification_flag_comment'=>0,'is_deleted'=>0),'', false)) {
                        $send_notification=true;
                    }
                }
            }else{
                if($device_detail = $ci->Common_model->getRecords('users','device_id,device_type',array('user_id' =>$user_id,'is_deleted'=>0),'', false)) {
                    $send_notification=true;
                }
            }
            if($send_notification){ 
            
                foreach($device_detail as $user_record) {
                    if(!empty($user_record['device_id'])) {
                        if(!empty($user_record['device_type'])) {
                            if($user_record['device_type']=="android") {
                                androidNotification($user_record['device_id'],$message);
                            } else if($user_record['device_type']=="ios") {
                                iosNotification($user_id,$user_record['device_id'],$message);
                            }
                        } 
                    }
                }
            }
        }
    }
}

if (!function_exists('iosNotification')) {
    function iosNotification($user_id,$deviceToken,$message)
    {
        return true;
        if(!empty($deviceToken)) {
            if($deviceToken !='NA') {
                // private key's passphrase here:
               // $passphrase = IOS_PUSH_PATH;
                // $passphrase = '12345678';
                $passphrase = '1234';
                $ctx = stream_context_create();
                
                if(DEV_MODE==TRUE) {
                    $pem = "pem/CAM2SchoolNewCert.pem";
                    $url = 'ssl://gateway.sandbox.push.apple.com:2195';
                } else {
                    $pem = "pem/CAM2SchoolNewCert.pem";
                    $url = 'ssl://gateway.push.apple.com:2195';
                }

                stream_context_set_option($ctx, 'ssl', 'local_cert', $pem);
                stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
                $fp = stream_socket_client($url, $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

                // Open a connection to the APNS server
                
                $ci =& get_instance();
                if(!$fp) {
                   return false;
                   // exit("Failed to connect: $err $errstr" . PHP_EOL);
                }

                $badge = 0;
                $badge1 = 0;
                if($res = $ci->Common_model->getRecords('users', 'unread_notification', array('user_id'=>$user_id), '', true)) {
                    $badge = $res['unread_notification'];
                }
               
                $bgarr=array('recipient_id'=>$user_id,'is_read'=>0,'is_active'=>1,'deleted'=>0);
                if($res = $ci->Common_model->getRecords('chat_message_recipients', 'count(is_read) as count',$bgarr, '', true)) {
                    $badge1 = $res['count'];
                }
                $badge = $badge+$badge1;
                $body['aps'] = array(
                    'alert' => $message,
                    'badge' => (int)$badge,
                    'sound' => 'default',
                );

                // // Encode the payload as JSON
                $payload = json_encode($body);

                // Build the binary notification
                $msg = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
                $result = fwrite($fp, $msg, strlen($msg));
                // Send it to the server
                if (!$result) {
                    return false;
                } else {
                    fclose($fp);
                    return true;
                }
                // Close the Connection to the Server.
            } else {
                return false;
            }
        } else {
            return false;
        }
    } //end send notification on ios
}
if (!function_exists('getLocalTime')){
    function getLocalTime($pst_date,$format) {
        $default_date = new DateTime($pst_date, new DateTimeZone(date_default_timezone_get()));
        if(!empty(LOCAL_TIMEZONE) && LOCAL_TIMEZONE!='Undefined')
        {
            $default_date->setTimezone(new DateTimeZone(LOCAL_TIMEZONE)); 

        }
        return $default_date->format($format);
    }
}
if (!function_exists('getImageFromVideo')){
    function getImageFromVideo($duration,$media_type,$media_name,$mobile_type,$rotation,$s3dir) {
        $duration = convert_duration($duration);
        $s3thumbPath='';
        $s3largeimagePath='';
        $res=array();
        if($media_type==1) {
            $s3 = new S3(AMAZON_ID, AMAZON_KEY); 
            if(!is_dir(MEDIA_THUMB_TEMP_PATH)){
                mkdir(MEDIA_THUMB_TEMP_PATH);
            }
            $s3dir = $s3dir.'/';
            $images = createImage($media_name,MEDIA_THUMB_TEMP_PATH,"800x450",$duration,$mobile_type,$rotation);
            //echo '<pre>';print_r($images);exit;
            $thumb_image = $images[1];
            $large_image = $images[0];
            $thumb_imagePath = MEDIA_THUMB_TEMP_PATH.$thumb_image;
            $large_imagePath = MEDIA_THUMB_TEMP_PATH.$large_image;

            //upload images on the amazon s3
            
            if($thumb_imagePath !='') {
                //store thumb image on the amazon s3 and delete temp file
                $save_path = $s3dir.$thumb_image;
                $s3->putObjectFile($thumb_imagePath, AMAZON_BUCKET, $save_path, S3::ACL_PUBLIC_READ);
                $s3thumbPath=AMAZON_PATH.$save_path;
                $res['s3thumbPath']=$s3thumbPath;
                if(file_exists($thumb_imagePath)) {
                    unlink($thumb_imagePath);
                }
            }

            if($large_imagePath !='') {
                //store thumb image on the amazon s3 and delete temp file
                $save_path = $s3dir.$large_image;
                $s3->putObjectFile($large_imagePath, AMAZON_BUCKET, $save_path, S3::ACL_PUBLIC_READ);
                $s3largeimagePath=AMAZON_PATH.$save_path;
                $res['s3largeimagePath']=$s3largeimagePath;
                if(file_exists($large_imagePath)) {
                    unlink($large_imagePath);
                }
            }
        }
        return $res;

    }
}
if (!function_exists('get_duration')){
     function get_duration($srcFile) {
        // Change the path according to your server.
        if(!empty($srcFile)) {
            $command = "ffmpeg -i $srcFile -vstats 2>&1";
            $output = shell_exec($command);
            $regex_duration = "/Duration: ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}).([0-9]{1,2})/";
            $duration=0;
            if (preg_match($regex_duration, $output, $regs)) {
                $hours = $regs [1] ? $regs [1] : null;
                $mins = $regs [2] ? $regs [2] : null;
                $secs = $regs [3] ? $regs [3] : null;
                $ms = $regs [4] ? $regs [4] : null;
                //$duration = $hours.':'. $mins.':'. $secs.':'. $ms; 
                $duration = ($hours*3600) + ($mins*60) + $secs + ($ms/1000);
                //$duration = gmdate("H:i:s", $duration);
            }
            return $duration;
        }
    } 
}  
    
}