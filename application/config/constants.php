<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code 
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code 



define('USER_PROFILE_PATH', 'resources/images/profile/');
define('SCHOOL_LOGO_PATH', 'resources/images/school/');
define('DISTRICT_CSV_PATH', 'resources/district_csv/');
define('EXCEL_PATH', 'resources/excel/');


define('ADMIN_LIMIT', 100);
define('FRONT_LIMIT', 30);
define('COMMENT_LIMIT',5);
define('CHILD_COMMENT_LIMIT',5);
define('INNER_CHILD_COMMENT_LIMIT',2);


define('ADMIN_CURRENCY','$');
define('ADMIN_CURRENCY_CODE','USD');

define('DATETIMEFORMATE',"m-d-Y");


define('ADMIN_EMAIL','hypdev01@gmail.com');

define('SMTP_HOST','smtp.gmail.com');
//define('SMTP_HOST','ssl://smtp.googlemail.com');
define('SMTP_USER', 'hypdev01@gmail.com');
define('SMTP_PASS', 'Test@123');
define('SMTP_PORT', '465'); 
define('PROTOCOL', 'smtp');
define('MAILPATH', '/usr/sbin/sendmail');
define('MAILTYPE', 'html');
define('CHARSET', 'utf-8');
define('WORD_WRAP', TRUE);
define('SMTP_TIMEOUT',300);
define('enc_value','1345');

define('FROM_NAME','CAM! Schools');
define('WEBSITE_NAME','CAM! Schools');
define('WEBSITE_EMAIL_NAME','Chat At Me! Schools');
define('SITE_TITLE', 'CAM! Schools');
// define('SITE_TITLE', 'Chat At Me! Schools');

define('APP_KEY','m55ypUJOuvDIpVcHPbnalieGTXLxeOcm');

# use it
define('DEV_MODE',true);

define('GOOGLE_API_KEY','AIzaSyBLV-ZJ-ZcSVG4z63Ohe0n4yFDkxrbHvBo');
define('GOOGLE_API_KEY_LATLONG','AIzaSyB_jfNvLIu7C6yw_LdYzWRKOf4lvDiMWXg');


define('AMAZON_BUCKET','cam-school-staging');
define('AMAZON_ID','AKIAVEQVROWLMEGEBPPT');
define('AMAZON_KEY','EaikYWZVSY6B6m7yTXjrrqN06LqVrll/gBQOCxTP');
define('AMAZON_PATH',"https://".AMAZON_BUCKET.".s3.amazonaws.com/");
//define('AMAZON_PATH',"https://cam-school-staging.s3-us-west-2.amazonaws.com/");

define('SEND_EMAIL',TRUE);
define('SEND_NOTIFICATION',TRUE);



define('MEDIA_PATH','resources/media/');
define('MEDIA_THUMB_PATH','resources/media/media_thumbs/');
define('MEDIA_THUMB_TEMP_PATH','resources/media/temp/');

define('GOOGLE_PLAY','https://play.google.com');
define('APPLE_ITUNE','https://play.google.com');


define('APP_PAGE_LIMIT',100);


/*define("DISPLAY_GRADE_NAME", array("0"=>"Kindergarten", "1" =>"1st Grade", "2"=>"2nd Grade", "3"=>"3rd Grade", "4"=>"4th Grade", "5"=>"5th Grade", "6"=>"6th Grade", "7"=>"7th Grade", "8"=>"8th Grade", "9"=>"9th Grade", "10"=>"10th Grade", "11"=>"11th Grade", "12"=>"12th Grade"));
define('SCHOOL_GRADE', array(array('id' => "1",'grade'=> "0",'grade_display_name' => DISPLAY_GRADE_NAME[0]),
							 array('id' => "2",'grade'=> "1",'grade_display_name' => DISPLAY_GRADE_NAME[1]),
							 array('id' => "3",'grade'=> "2",'grade_display_name' => DISPLAY_GRADE_NAME[2]),
							 array('id' => "4",'grade'=> "3",'grade_display_name' => DISPLAY_GRADE_NAME[3]),
							 array('id' => "5",'grade'=> "4",'grade_display_name' => DISPLAY_GRADE_NAME[4]),
							 array('id' => "6",'grade'=> "5",'grade_display_name' => DISPLAY_GRADE_NAME[5]),
							 array('id' => "7",'grade'=> "6",'grade_display_name' => DISPLAY_GRADE_NAME[6]),
							 array('id' => "8",'grade'=> "7",'grade_display_name' => DISPLAY_GRADE_NAME[7]),
							 array('id' => "9",'grade'=> "8",'grade_display_name' => DISPLAY_GRADE_NAME[8]),
							 array('id' => "10",'grade'=> "9",'grade_display_name' => DISPLAY_GRADE_NAME[9]),
							 array('id' => "11",'grade'=> "10",'grade_display_name' => DISPLAY_GRADE_NAME[10]),
							 array('id' => "12",'grade'=> "11",'grade_display_name' => DISPLAY_GRADE_NAME[11]),
							 array('id' => "13",'grade'=> "12",'grade_display_name' => DISPLAY_GRADE_NAME[12]),
                       ));*/

define("DISPLAY_GRADE_NAME", array(
								"0"=>"Pre-Kindergarten", 
								"1"=>"Kindergarten", 
								"2" =>"1st Grade", 
								"3"=>"2nd Grade",
								"4"=>"3rd Grade",
								"5"=>"4th Grade",
								"6"=>"5th Grade",
								"7"=>"6th Grade",
								"8"=>"7th Grade",
								"9"=>"8th Grade",
								"10"=>"9th Grade", 
								"11"=>"10th Grade", 
								"12"=>"11th Grade", 
								"13"=>"12th Grade"
							));

define('SCHOOL_GRADE', array(array('id' => "1",'grade'=> "0",'grade_display_name' => DISPLAY_GRADE_NAME[0]),
							array('id' => "2",'grade'=> "1",'grade_display_name' => DISPLAY_GRADE_NAME[1]),
							array('id' => "3",'grade'=> "2",'grade_display_name' => DISPLAY_GRADE_NAME[2]),
							array('id' => "4",'grade'=> "3",'grade_display_name' => DISPLAY_GRADE_NAME[3]),
							array('id' => "5",'grade'=> "4",'grade_display_name' => DISPLAY_GRADE_NAME[4]),
							array('id' => "6",'grade'=> "5",'grade_display_name' => DISPLAY_GRADE_NAME[5]),
							array('id' => "7",'grade'=> "6",'grade_display_name' => DISPLAY_GRADE_NAME[6]),
							array('id' => "8",'grade'=> "7",'grade_display_name' => DISPLAY_GRADE_NAME[7]),
							array('id' => "9",'grade'=> "8",'grade_display_name' => DISPLAY_GRADE_NAME[8]),
							array('id' => "10",'grade'=> "9",'grade_display_name' => DISPLAY_GRADE_NAME[9]),
							array('id' => "11",'grade'=> "10",'grade_display_name' => DISPLAY_GRADE_NAME[10]),
							array('id' => "12",'grade'=> "11",'grade_display_name' => DISPLAY_GRADE_NAME[11]),
							array('id' => "13",'grade'=> "12",'grade_display_name' => DISPLAY_GRADE_NAME[12]),
							array('id' => "14",'grade'=> "13",'grade_display_name' => DISPLAY_GRADE_NAME[13]),
                       	));


   
// define('NODE_URL','http://localhost/cam-school-chat/public/js/chat_system.js');
// define('NODE_URL','http://3.238.88.19/camschool_chat/public/js/chat_system.js');
define('NODE_URL','http://www.techopium.com/cam-school/public/js/chat_system.js');

define('recaptcha_site_key','6Lfp9fwZAAAAAMCc-_g_1EZH-Z0r3SLfSMrqvdxi');
define('recaptcha_secret_key','6Lfp9fwZAAAAAMCc-_g_1EZH-Z0r3SLfSMrqvdxi');
define('recaptcha_lang','en');