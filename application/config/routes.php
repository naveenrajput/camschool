<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*  
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html 
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Welcome'; //admin login view  

$route['404_override'] = 'page_not_found';
$route['translate_uri_dashes'] = FALSE;

$route['app/terms_of_service_mobile']	= 	'Static_pages/terms_and_conditions';
$route['app/privacy_policy_mobile'] 		= 	'Static_pages/privacy_policy';
$route['app/about_us'] 		= 	'Static_pages/about_us';
$route['app/chat_at_me_school_info_district'] 		= 	'Static_pages/chat_at_me_school_info_district';
$route['app/chat_at_me_school_info_school'] 		= 	'Static_pages/chat_at_me_school_info_school';
$route['app/chat_at_me_school_info_teacher'] 		= 	'Static_pages/chat_at_me_school_info_teacher';
$route['app/chat_at_me_school_info_parent'] 		= 	'Static_pages/chat_at_me_school_info_parent';

$route['about-us'] 		= 	'Static_pages/web_about_us';
$route['terms-of-conditions'] 		= 	'Static_pages/web_terms_and_conditions';
$route['privacy-policy'] 		= 	'Static_pages/web_privacy_policy';
$route['chat-at-me-school-info'] 		= 	'Static_pages/web_chat_at_me_school_info';


$route['admin'] 		 		= 'admin/admin';
$route['admin/login'] 		 		= 'admin/admin';
$route['admin/logout'] 				= 'admin/admin/logout';
$route['admin/forgot_password'] 	= 'admin/admin/forgot_password';
$route['admin/reset_password'] 		= 'admin/admin/reset_password';

$route['admin/edit_profile'] 		= 'admin/admin/edit_profile';
$route['admin/change_password'] 	= 'admin/admin/change_password';

$route['admin/dashboard'] 			= 'admin/Dashboard';
$route['admin/report'] 			    = 'admin/Report/list';
$route['admin/report/(:num)'] 		= 'admin/Report/list/$1';

$route['admin/report/detail/(:num)'] = 	'admin/Report/detail/$1';
$route['admin/filter'] 		         = 'admin//Report/Reportfilter';

$route['admin/subadmin/list'] 			= 'admin/admin/admin_list';
$route['admin/subadmin/list/:num'] 		= 'admin/admin/admin_list/:num';
$route['admin/subadmin/add'] 			= 'admin/admin/add_admin';
$route['admin/subadmin/edit/(:num)'] 	= 'admin/admin/edit_admin/$1';
$route['admin/subadmin/edit'] 			= 'admin/admin/edit_admin';

$route['admin/role_list'] 			= 'admin/admin/role_list';
$route['admin/role_list/:num'] 		= 'admin/admin/role_list/:num';
$route['admin/add_role'] 			= 'admin/admin/add_role';
$route['admin/edit_role/(:num)'] 	= 'admin/admin/edit_role/$1';
$route['admin/edit_role'] 			= 'admin/admin/edit_role';

$route['admin/contact/list'] 		= 'admin/Support/contact_list';
$route['admin/contact/list/(:num)'] = 'admin/Support/contact_list/$1';
$route['admin/contact/add_contact'] = 'admin/Support/add_contact';
$route['admin/contact/reply'] 		= 'admin/Support/contact_reply';
$route['admin/contact/reply/(:num)']= 'admin/Support/contact_reply/$1';



$route['admin/pages/list'] 				= 'admin/Cmscontent/pages_list';
$route['admin/pages/list/:num'] 		= 'admin/Cmscontent/pages_list/:num';
$route['admin/pages/add'] 				= 'admin/Cmscontent/add_page';
$route['admin/pages/edit/(:num)']   	= 'admin/Cmscontent/edit_page/$1';
$route['admin/pages/edit'] 				= 'admin/Cmscontent/edit_page';

$route['admin/states/list'] 			= 'admin/Cmscontent/states_list';
$route['admin/states/list/:num'] 		= 'admin/Cmscontent/states_list/:num';
$route['admin/states/add'] 				= 'admin/Cmscontent/add_states';
$route['admin/states/edit/(:num)']   	= 'admin/Cmscontent/edit_states/$1';
$route['admin/states/edit'] 			= 'admin/Cmscontent/edit_states';

$route['admin/cities/list'] 			= 'admin/Cmscontent/cities_list';
$route['admin/cities/list/:num'] 		= 'admin/Cmscontent/cities_list/:num';
$route['admin/cities/add'] 				= 'admin/Cmscontent/add_cities';
$route['admin/cities/edit/(:num)']   	= 'admin/Cmscontent/edit_cities/$1';
$route['admin/cities/edit'] 			= 'admin/Cmscontent/edit_cities';


$route['admin/contact_us/list'] 			= 'admin/Cmscontent/contact_us_form';
$route['admin/contact_us/list/:num'] 		= 'admin/Cmscontent/contact_us_form/:num';
$route['admin/contact_us/detail/(:num)'] 		= 'admin/Cmscontent/contact_details/$1';



$route['admin/prospect/list']    		= 	'admin/User/prospect_list';
$route['admin/prospect/list/:num'] 		= 	'admin/User/prospect_list/:num';
$route['admin/prospect/export']    		= 	'admin/User/prospect_export';

$route['admin/user/list']    				= 	'admin/User/users_list';
$route['admin/user/import-file']    	    = 	'admin/User/import_file';
$route['admin/user/list/:num'] 				= 	'admin/User/users_list/:num';
$route['admin/user/add'] 					= 	'admin/User/add_user';
$route['admin/user/edit'] 					= 	'admin/User/edit_user';
$route['admin/user/upload/(:num)'] 		    = 	'admin/User/upload_user/$1';
$route['admin/user/edit/(:num)']   			= 	'admin/User/edit_user/$1';
$route['admin/district/export']				= 	'admin/User/district_export';
$route['admin/district/check_broadcast']	= 	'admin/User/check_broadcast';
$route['admin/district/broadcast_message']	= 	'admin/User/broadcast_message';
$route['admin/broadcast/list']				= 	'admin/User/broadcast_list';
$route['admin/broadcast/list/(:num)']		= 	'admin/User/broadcast_list/$1';
$route['admin/broadcast_user/list/(:num)']	= 	'admin/User/broadcast_user_list/$1';
$route['admin/broadcast_user/list/(:num)/(:num)']	= 	'admin/User/broadcast_user_list/$1/$2';
$route['admin/deleteall']    		            = 	'admin/User/DeleteAllUser';

$route['admin/principal/list']    		= 	'admin/User/principal_list';
$route['admin/principal/list/:num'] 	= 	'admin/User/principal_list/:num';
$route['admin/principal/edit/(:num)']   = 	'admin/User/edit_principal/$1';
$route['admin/principal/export']		= 	'admin/User/principal_export';


$route['admin/teacher/list']    		= 	'admin/User/teacher_list';
$route['admin/teacher/list/:num'] 		= 	'admin/User/teacher_list/:num';
$route['admin/teacher/edit/(:num)']   	= 	'admin/User/edit_teacher/$1';
$route['admin/teacher/export']			= 	'admin/User/teacher_export';

$route['admin/parent/list']    			= 	'admin/User/parent_list';
$route['admin/parent/list/:num'] 		= 	'admin/User/parent_list/:num';
$route['admin/parent/edit/(:num)']   	= 	'admin/User/edit_parent/$1';
$route['admin/parent/export']			= 	'admin/User/parent_export';

$route['admin/student/list/(:num)']			= 	'admin/User/student_list/$1';
$route['admin/student/list/(:num)/(:num)']	= 	'admin/User/student_list/$1/$2';


$route['admin/not_authorized'] 	= 'admin/Admin/not_authorized';
$route['admin/prohibit_dashboard'] 	= 'admin/Dashboard/prohibit_dashboard';
 
 
//  Api Routing 

$route['api/state-list'] 					= 	'Auth/state_list';
$route['api/district-list'] 				= 	'Auth/district_list';
$route['api/school-list'] 					= 	'Auth/school_list';
$route['api/grade-list'] 					= 	'Auth/grade_list';
$route['api/signup'] 						= 	'Auth/signup';
$route['api/email-verification'] 			= 	'Auth/email_verification';
$route['api/resend-email-verification'] 	= 	'Auth/resend_email_verification';
$route['api/login'] 						= 	'Auth/login';
$route['api/logout'] 						= 	'Auth/logout';
$route['api/change-password']				= 	'Auth/change_password';
$route['api/forgot-password']				= 	'Auth/forgot_password';
$route['api/reset-password']				= 	'Auth/reset_password';
$route['api/get-user-profile']				= 	'Auth/get_user_profile';
$route['api/profile-update']				= 	'Auth/profile_update';
$route['api/prinicpal-profile-update']		= 	'front/Front_principal/profile_update';
$route['api/teacher-profile-update']		= 	'front/Front_teacher/profile_update';
$route['api/contact-us']				    = 	'Auth/contact_us';
$route['api/broadcast-msg']				    = 	'Auth/broadcast_msg';
$route['api/broadcast-msg-list']		    = 	'Auth/broadcast_msg_list';
$route['api/broadcast-msg-delete']		    = 	'Auth/broadcast_msg_delete';


$route['api/district-request-list']		    = 	'District/district_request_list';
$route['api/district-approved-school']	    = 	'District/school_approved_by_district';
$route['api/approved-school-list-by-district']	= 'District/approved_school_by_district';

$route['api/school-request-list']		    = 	'School/school_request_list';
$route['api/school-approved-teacher']		= 	'School/teacher_approved_by_school';
$route['api/approved-teacher-list-by-school']= 	'School/approved_teacher_by_school';
$route['api/school-detail-update']          = 	'School/school_detail_update';
$route['api/school-upload-video']          = 	'School/school_upload_video';

$route['api/teacher-by-grade']          = 	'Parent_student/teacher_by_grade';
$route['api/add-student']         		= 	'Parent_student/add_student';
$route['api/add-student-web']         	= 	'Parent_student/add_student_web';
$route['api/student-detail']          	= 	'Parent_student/student_detail';
$route['api/request-canceled-by-student']   = 	'Parent_student/request_canceled_by_student';
$route['api/menu-teacher-by-grade']   = 	'Parent_student/menu_teacher_by_grade';
$route['api/home']          			= 	'Parent_student/home';
$route['api/add-continue-watch-video']      = 	'Parent_student/add_continue_watch_detail';
$route['api/all-recent-continue-video']      = 	'Parent_student/all_recent_continue_video';


$route['api/teacher-request-list']		    = 	'Teacher/teacher_request_list';
$route['api/teacher-approved-student']		 = 	'Teacher/student_approved_by_teacher';
$route['api/approved-student-list-by-teacher'] = 	'Teacher/approved_student_by_teacher';
$route['api/teacher-upload-video']    = 	'Teacher/teacher_upload_video';
$route['api/teacher-update-video-content']    = 	'Teacher/teacher_update_video_content';
$route['api/teacher-delete-video']    = 	'Teacher/teacher_delete_video';
$route['api/teacher-my-upload']    = 	'Teacher/teacher_my_upload';


/*************common for all ********************/
$route['api/video-details']    = 	'Common/video_details';
$route['api/get-comments']    = 	'Common/get_comments';
$route['api/all-media']   	 = 	'Common/all_media';
$route['api/add-comment']    	= 	'Common/comment';
$route['api/comment-reply']    	= 	'Common/comment_reply';
$route['api/comment-like-unlike']   = 	'Common/comment_like_unlike';
$route['api/comment-report']    	= 	'Common/comment_report';
$route['api/media-like-unlike']    	= 	'Common/media_like_unlike';
$route['api/notification-setting']    	= 	'Common/notification_setting';
$route['api/notification-list']    	= 	'Common/notification_list';
$route['api/convert-image-from-media']    	= 	'Common/ConvertImageFromMedia';

/*************chat list ********************/

$route['api/teacher-chat-list']    	= 	'Common/teacher_chat_list';
$route['api/parent-chat-list']    	= 	'Common/parent_chat_list';

//Cron 
$route['send-email-notification']	   = 	'Cron/send_pending_email';
$route['import-csv-data']	   	       = 	'Cron/import_csv_data';
$route['teacher-video-delete']	       = 	'Cron/teacher_video_delete';
$route['send-otp-email']	       		= 	'Cron/send_otp_email';
//Web Routing

$route['signup'] 						= 	'front/Front_Auth/sign_up';  
$route['login'] 						= 	'front/Front_Auth/login';  
$route['logout'] 						= 	'front/Front_Auth/logout';  
$route['verify-email/(:any)/(:any)'] 	= 	'front/Front_Auth/verify_email/$1/$1';
$route['change-password']				= 	'front/Front_Auth/change_password';  
$route['forgot-password']				= 	'front/Front_Auth/forgot_password';  
$route['reset-password']				= 	'front/Front_Auth/reset_password';  
$route['contact-us']						= 	'front/Front_Auth/support';  
$route['notifications']					= 	'front/Front_Auth/notification';  
$route['download-media/(:any)']			= 	'front/Front_Auth/download_media/(:any)';  


$route['district/profile'] 				= 	'front/Front_district/profile';  
$route['district/upload-csv'] 			= 	'front/Front_district/upload_csv';  
$route['district/import-file'] 			= 	'front/Front_district/import_file'; 

$route['principal/profile'] 			= 	'front/Front_principal/profile'; 

//District
$route['district-request']          =   'front/Front_district/district_request';
$route['principal-school-approved-list'] = 'front/Front_district/approved_school_request';

$route['approved-school-sendmsg'] = 'front/Front_district/approved_school_sendmsg';
$route['approved-school-request'] = 'front/Front_district/approved_school_request';
$route['school-broadcast-page'] = 'front/Front_district/school_broadcast_page';
$route['broadcast-messages'] = 'front/Front_district/broadcast_messages';

$route['school-videos'] = 'front/Front_district/school_videos';
$route['video-fulldetail/(:any)'] = 'front/Front_district/video_fulldetail/$1';

//princelpal

$route['principal-request']     =   'front/Front_principal/principal_request';
$route['principal-request/(:any)']     =   'front/Front_principal/principal_request/$1';
$route['teacher-list-request-data'] = 'front/Front_principal/teacher_list_request_data';
$route['teacher-list-request-data/(:any)'] = 'front/Front_principal/teacher_list_request_data/$1';

$route['teacher-approved-list'] = 'front/Front_principal/approved_teacher_request';
$route['teacher-approved-list'] = 'front/Front_principal/approved_teacher_request';
$route['teacher-approved-list-data'] = 'front/Front_principal/approved_teacher_request_data';
$route['teacher-approved-list/(:any)'] = 'front/Front_principal/approved_teacher_request/$1';
$route['teacher-approved-list-data/(:any)'] = 'front/Front_principal/approved_teacher_request_data/$1';
$route['approved-teacher-sendmsg'] = 'front/Front_principal/approved_teacher_sendmsg';
$route['teacher-broadcast-page'] = 'front/Front_principal/teacher_broadcast_page';
$route['teacher-broadcast-messages'] = 'front/Front_principal/teacher_broadcast_messages';

$route['broadcast_details'] = 'front/Front_principal/broadcast_details';
$route['broadcast_details/(:num)'] = 'front/Front_principal/broadcast_details/$1';
$route['school-upload-video'] = 'front/Front_principal/school_upload_video';
$route['teacher-video'] = 'front/Front_principal/teacher_videos';

//teacher
$route['teacher/profile'] 			= 	'front/Front_teacher/profile';

$route['student-request']           =   'front/Front_teacher/student_request';
$route['student-request/(:any)']     =   'front/Front_teacher/student_request/$1';
$route['grade-wise-video/(:num)']     =   'front/Front_teacher/grade_wise_video/$1';
$route['teacher-upload-video'] 		= 'front/Front_teacher/teacher_upload_video';
$route['teacher-upload-video/(:num)'] 		= 'front/Front_teacher/teacher_upload_video/$1';
$route['teacher-upload-video/(:num)/(:num)'] 		= 'front/Front_teacher/teacher_upload_video/$1/$1';
$route['teacher-upload-video/(:num)/(:num)/(:num)'] 		= 'front/Front_teacher/teacher_upload_video/$1/$2/$3';
$route['teacher-edit-video/(:any)'] 		= 'front/Front_teacher/teacher_edit_video/$1';


$route['student-list-request-data'] = 'front/Front_teacher/student_list_request_data';
$route['student-list-request-data/(:any)'] = 'front/Front_teacher/student_list_request_data/$1';

$route['teacher-index'] 			= 	'front/Front_teacher/home';  
$route['teacher/recently-video'] 		= 	'front/Front_teacher/recently_video'; 
$route['teacher/continue-video'] 		= 	'front/Front_teacher/continue_video';

 //parent
$route['parent/profile'] 		= 	'front/Front_parent/profile'; 
$route['parent/add-student'] 	= 	'front/Front_parent/Add_Student';
$route['parent/index'] 			= 	'front/Front_parent/home'; 
$route['teacher-wise-video/(:num)']     =   'front/Front_parent/teacher_wise_video/$1';
$route['teacher-wise-video/(:num)/(:num)']  =   'front/Front_parent/teacher_wise_video/$1/$2';
$route['principal-video/(:num)'] 			= 	'front/Front_parent/principal_video/$1'; 
$route['story-time'] 		= 	'front/Front_parent/story_time'; 
$route['tutor'] 		= 	'front/Front_parent/tutor'; 

$route['parent/recently-video'] 		= 	'front/Front_parent/recently_video'; 
$route['parent/continue-video'] 		= 	'front/Front_parent/continue_video';

$route['user-chat'] 		            = 	'front/Front_teacher/chatView';
$route['api/add-chat-user'] 		    = 	'Common/add_chat_user';
$route['api/delete-user-ingroup'] 		= 	'Common/delete_user_ingroup';
$route['api/delete-group-byadmin'] 		= 	'Common/delete_group_byadmin';